package com.blackmaria;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;
import android.widget.RadioButton;

public class CustomAutoRadionButton extends RadioButton {

    public CustomAutoRadionButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomAutoRadionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomAutoRadionButton(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto-condensed.regular.ttf");
        //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/robos.ttf");
        setTypeface(tf);
    }


}