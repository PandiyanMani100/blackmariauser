package com.blackmaria

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.blackmaria.R
import com.google.android.gms.instantapps.InstantApps
import kotlinx.android.synthetic.main.activity_instant.*

class InstantActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instant)
        showInstallPrompt()
    }


    private fun showInstallPrompt()
    {
      /*  val postInstall = Intent(Intent.ACTION_VIEW)
            .addCategory(Intent.CATEGORY_BROWSABLE)
            .setPackage("com.blackmaria")

        // The request code is passed to startActivityForResult().
        InstantApps.showInstallPrompt(this, postInstall, REQUEST_CODE, *//* referrer= *//* "" +
                "")
        InstantApps.getPackageManagerCompat(this).isInstantApp*/
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
        }
    }

    companion object {
        const val REQUEST_CODE = 1
    }
}
