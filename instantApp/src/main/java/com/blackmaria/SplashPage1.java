package com.blackmaria;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

/**
 * Created by user14 on 4/18/2017.
 */

public class SplashPage1 extends Activity {
    Handler handler;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Intent i = new Intent(SplashPage1.this, BookingScreenActivity.class);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        handler=new Handler();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                removerunnable();
                handler.postDelayed(runnable, SPLASH_TIME_OUT);
            }
        }, SPLASH_TIME_OUT);
    }



    public void removerunnable()
    {
        if (runnable != null) {
            handler.removeCallbacks(runnable);
        }
    }

}

