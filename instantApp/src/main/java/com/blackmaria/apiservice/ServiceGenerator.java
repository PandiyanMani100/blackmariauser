package com.blackmaria.apiservice;

import android.content.Context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.TimeUnit;

import kotlin.jvm.internal.Intrinsics;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ServiceGenerator {
    @NotNull
    private Context context;
    @NotNull
    private OkHttpClient.Builder httpClient;
    @NotNull
    private Retrofit.Builder builder;

    @NotNull
    public final Context getContext() {
        return this.context;
    }

    public final void setContext(@NotNull Context var1) {
        this.context = var1;
    }

    @NotNull
    public final OkHttpClient.Builder getHttpClient() {
        return this.httpClient;
    }

    public final void setHttpClient(@NotNull OkHttpClient.Builder var1) {
        Intrinsics.checkParameterIsNotNull(var1, "<set-?>");
        this.httpClient = var1;
    }

    @NotNull
    public final Retrofit.Builder getBuilder() {
        return this.builder;
    }

    public final void setBuilder(@NotNull Retrofit.Builder var1) {
        Intrinsics.checkParameterIsNotNull(var1, "<set-?>");
        this.builder = var1;
    }

    public final CoreClient createService(@Nullable Class serviceClass) {
        Retrofit retrofit = this.builder.client(this.httpClient.build()).build();
        return (CoreClient) retrofit.create(serviceClass);
    }

    public ServiceGenerator(@NotNull Context c) {
        Intrinsics.checkParameterIsNotNull(c, "c");
        this.context = c;
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder var10001 = (new OkHttpClient.Builder()).connectTimeout(2000L, TimeUnit.SECONDS).readTimeout(200L, TimeUnit.SECONDS);
        this.httpClient = var10001;
        Retrofit.Builder var3 = (new Retrofit.Builder())
                .baseUrl("https://maps.googleapis.com/maps/api/directions/json/")
                .addConverterFactory((GsonConverterFactory.create()) );
        this.builder = var3;
    }
}
