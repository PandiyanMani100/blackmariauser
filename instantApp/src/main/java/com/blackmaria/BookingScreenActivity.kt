package com.blackmaria

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.constraintlayout.widget.Group
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import com.blackmaria.route.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import java.util.*

class BookingScreenActivity : FragmentActivity(), OnMapReadyCallback, PlaceSearchList,RouteListeners {
    private var mRoute = FindRoute(this)
    private lateinit var permissionsList: ArrayList<String>
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private lateinit var autoCompleteTextView: AutoCompleteTextView
    private lateinit var okBtn: Button
    private lateinit var rideBtn: Button

    private lateinit var firstGrp: Group
    private lateinit var secondGrp: Group
    private lateinit var placesList: ArrayList<PlacesDetail>
    private lateinit var listener: OnLocationSearched
    private lateinit var mapFragment:SupportMapFragment
    private lateinit var placesAutoCompleteAdapter:PlacesAutoCompleteAdapter
    private var selectedPlacesDetail:PlacesDetail?=null
    lateinit var currentLanLng :LatLng
    lateinit var dropLanLng :LatLng
    var BOOKING_PAGE:Int=1
//    private lateinit var placesAutoCompleteAdapter:ArrayAdapter<PlacesDetail>

    private var currentLatitude = 0.0
    private var currentLongtitude = 0.0

    private var mMap: GoogleMap? = null

    private val textWatcher = object : TextWatcher {

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (s != null && s.length>=2 ) {

                listener.onLocationSearched(s.toString())
            }

        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun afterTextChanged(s: Editable) {

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_screen)
        initView()
    }

    private fun initView(){
        placesList=ArrayList()
        permissionsList = ArrayList<String>()
        permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION)
        permissionsList.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment!!.getMapAsync(this)
        if (checkPermissions()) {
            getCurrentLocation()
        }

        autoCompleteTextView=findViewById<AutoCompleteTextView>(R.id.loc_search)



        okBtn=findViewById(R.id.ok_btn)
        rideBtn=findViewById(R.id.ride_btn)
        firstGrp=findViewById(R.id.grp_view)
        secondGrp=findViewById(R.id.grp_view1)
        autoCompleteTextView=findViewById<AutoCompleteTextView>(R.id.loc_search)
        autoCompleteTextView.threshold=1
        listener= GooglePlaceRepository(this, this)
        autoCompleteTextView.addTextChangedListener(textWatcher)
        placesAutoCompleteAdapter=PlacesAutoCompleteAdapter(this,android.R.layout.simple_list_item_1,placesList)
        autoCompleteTextView.setAdapter(placesAutoCompleteAdapter)
        autoCompleteTextView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, pos, p3 ->
            if (placesAutoCompleteAdapter.getItem(pos) != null)
                listener.onItemClicked(placesAutoCompleteAdapter.getItem(pos)!!)
        }
        okBtn.setOnClickListener(object :View.OnClickListener{
            override fun onClick(p0: View?) {
                if (selectedPlacesDetail!=null && autoCompleteTextView.text.toString() != ""){

                    moveToNext()
                }
                else{
                    Toast.makeText(this@BookingScreenActivity,"Enter Drop Off Location",Toast.LENGTH_SHORT).show()
                }
            }

        })
        rideBtn.setOnClickListener(object :View.OnClickListener{
            override fun onClick(p0: View?) {
                showInstallPrompt()
            }

        })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
//        getCurrentLocation()

    }


    private fun checkPermissions():Boolean{
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            makeRequest(permissionsList)
            return false
        }
        return true
    }

    private fun makeRequest(permissionsList: ArrayList<String>) {
        val permissionStringList = arrayOfNulls<String>(permissionsList.size)
        for (i in permissionsList.indices) {
            permissionStringList[i] = permissionsList[i]
        }
        if (permissionStringList.isNotEmpty()) ActivityCompat.requestPermissions(
            this@BookingScreenActivity,
            permissionStringList,
            1000
        ) else {
           getCurrentLocation()
        }
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1000) {
            if (checkPermissions())
            {
                getCurrentLocation()
            }
        }

    }

    private  fun getCurrentLocation(){
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.lastLocation.addOnCompleteListener { task ->
            if (task.isSuccessful && task.result != null) {
                if (servicesConnected()) {
                    currentLatitude = task.result.latitude
                    currentLongtitude = task.result.longitude
                    if (currentLatitude!=0.0&&currentLongtitude!=0.0) {
                        val currentLocation = LatLng(currentLatitude, currentLongtitude)
                        val markerOptions=MarkerOptions().position(currentLocation).title("Current Location")
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(markerOptions.position, 16F))
                        mMap!!.addMarker(markerOptions)
                        generateRandomMarkers()
                    }
                }
            }
        }
    }
    private fun servicesConnected(): Boolean {
        val resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        return ConnectionResult.SUCCESS == resultCode
    }

    override fun setPlaceList(placeDetailResult: ArrayList<PlacesDetail>?) {
        placesList.clear()
        if (placeDetailResult != null) {
            placesList.addAll(placeDetailResult)
        }
        placesAutoCompleteAdapter=PlacesAutoCompleteAdapter(this,android.R.layout.simple_list_item_1,placesList)
        autoCompleteTextView.setAdapter(placesAutoCompleteAdapter)
    }

    fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager: InputMethodManager = activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText()) {
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus()!!.getWindowToken(),
                    0
            )
        }
    }
    override fun setPlaceDetail(placeDetail: PlacesDetail) {
        hideSoftKeyboard(this)
        selectedPlacesDetail=placeDetail
        currentLanLng=LatLng(currentLatitude,currentLongtitude)
//        val dropLanLng=LatLng(currentLatitude+5,currentLongtitude+5)
        dropLanLng=LatLng(selectedPlacesDetail!!.latitude,selectedPlacesDetail!!.longtitute)
        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(dropLanLng, 16F))
        val height = 150
        val width = 150
        val bitmapdraw: BitmapDrawable = resources.getDrawable(R.drawable.pickuptoofar) as BitmapDrawable
        val b: Bitmap = bitmapdraw.getBitmap()
        val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)

        mMap!!.addMarker(MarkerOptions().position(dropLanLng).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)))

       }
    private fun moveBackToHome(){
        mMap!!.clear()
        selectedPlacesDetail=null
        autoCompleteTextView.setText("")
        BOOKING_PAGE=1
        firstGrp.visibility=View.VISIBLE
        secondGrp.visibility=View.GONE
        getCurrentLocation()
    }
    private fun moveToNext(){
        BOOKING_PAGE=2
        firstGrp.visibility=View.GONE
        secondGrp.visibility=View.VISIBLE
        val latLngList:ArrayList<LatLng> = ArrayList()
        latLngList.add(currentLanLng)
        latLngList.add(dropLanLng)
        val m = arrayOfNulls<Marker>(2)
        mMap!!.clear()
        m[0] = mMap!!.addMarker(MarkerOptions().position(currentLanLng).icon(BitmapDescriptorFactory.fromBitmap(createStoreMarker(1))))
        m[1] = mMap!!.addMarker(MarkerOptions().position(dropLanLng).icon(BitmapDescriptorFactory.fromBitmap(createStoreMarker(2))))
        mRoute.setUpPolyLine(mMap!!, applicationContext,currentLanLng ,dropLanLng, latLngList, true)
       /* val draw_route_asyncTask = GetRouteTask(mapFragment,currentLanLng, dropLanLng)
        draw_route_asyncTask.execute()*/
    }

    private fun createStoreMarker(type:Int): Bitmap? {
        val markerLayout = layoutInflater.inflate(R.layout.marker_layout, null)
        val markerImage = markerLayout.findViewById(R.id.marker_image) as ImageView
        val markerRating = markerLayout.findViewById(R.id.marker_text) as TextView
        markerImage.setImageResource(R.drawable.red)
        if(type==1) {
            markerRating.text = "PICKUP"
        }
        else{
            markerRating.text = "DROP OFF"
        }
        markerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
        markerLayout.layout(0, 0, markerLayout.measuredWidth, markerLayout.measuredHeight)
        val bitmap = Bitmap.createBitmap(markerLayout.measuredWidth, markerLayout.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        markerLayout.draw(canvas)
        return bitmap
    }

    override fun routeDrawnPickToDrop(time: Double?, dist: Double?) {
        generateRandomMarkers()
        val builder = LatLngBounds.Builder()
        builder.include(dropLanLng)
        builder.include(currentLanLng)
        var bounds = builder.build()
        mMap!!.setOnMapLoadedCallback { mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 40)) }
        //future purposes
    }


    override fun onBackPressed() {
        if (BOOKING_PAGE==2){
            moveBackToHome()
        }
        else{
            super.onBackPressed()
        }
    }


    fun generateRandomMarkers() { //set your own minimum distance here
        val minimumDistanceFromMe = 70
        //set your own maximum distance here
        val maximumDistanceFromMe = 500
        //set number of markers you want to generate in Map/
        val markersToGenerate = 4
        for (position in 1..markersToGenerate) {
            val coordinates = generateRandomCoordinates(minimumDistanceFromMe, maximumDistanceFromMe)
            val height = 100
            val width = 100
            val bitmapdraw: BitmapDrawable = resources.getDrawable(R.drawable.whitecarr) as BitmapDrawable
            val b: Bitmap = bitmapdraw.getBitmap()
            val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)
            mMap!!.addMarker(MarkerOptions().position(LatLng(coordinates.latitude, coordinates.longitude)).title("mid point").icon(BitmapDescriptorFactory.fromBitmap(smallMarker)))
        } // end FOR loop
    }

    fun generateRandomCoordinates(min: Int, max: Int): LatLng { // Get the Current Location's longitude and latitude
        val currentLong: Double = currentLongtitude
        val currentLat: Double = currentLatitude
        // 1 KiloMeter = 0.00900900900901° So, 1 Meter = 0.00900900900901 / 1000
        val meterCord = 0.00900900900901 / 1000
        //Generate random Meters between the maximum and minimum Meters
        val r = Random()
        val randomMeters = r.nextInt(max + min)
        //then Generating Random numbers for different Methods
        val randomPM = r.nextInt(6)
        //Then we convert the distance in meters to coordinates by Multiplying number of meters with 1 Meter Coordinate
        val metersCordN = meterCord * randomMeters.toDouble()
        //here we generate the last Coordinates
        return when (randomPM) {
            0 -> {
                LatLng(currentLat + metersCordN, currentLong + metersCordN)
            }
            1 -> {
                LatLng(currentLat - metersCordN, currentLong - metersCordN)
            }
            2 -> {
                LatLng(currentLat + metersCordN, currentLong - metersCordN)
            }
            3 -> {
                LatLng(currentLat - metersCordN, currentLong + metersCordN)
            }
            4 -> {
                LatLng(currentLat, currentLong - metersCordN)
            }
            else -> {
                LatLng(currentLat - metersCordN, currentLong)
            }
        }
    }

    private fun showInstallPrompt() {
        val intent = Intent(this, SignUpPage::class.java)
        startActivity(intent)
        finish()
    }

}