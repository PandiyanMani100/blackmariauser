package com.blackmaria;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by user14 on 11/17/2016.
 */

public class SignUpPage extends AppCompatActivity  {

    ObjectAnimator objectanimator;
    private TextView Rl_DriveMe;
    LinearLayout signup;
    ImageView movim;

    int p=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.carmoving);
        initialize();
        objectanimator = ObjectAnimator.ofFloat(movim,"x",200);
        objectanimator.setDuration(2000);
        objectanimator.start();
        Rl_DriveMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
    }

    private void initialize() {
        Rl_DriveMe = (TextView) findViewById(R.id.signup_page_drive_me_layout);
        TextView signuptext = (TextView) findViewById(R.id.signuptext);
        signup= (LinearLayout) findViewById(R.id.signup);
        movim = (ImageView) findViewById(R.id.movim);
    }



}

