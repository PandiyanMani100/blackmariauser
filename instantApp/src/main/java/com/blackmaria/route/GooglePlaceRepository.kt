package com.blackmaria.route

import android.content.Context
import android.widget.Filter
import android.widget.Filterable
import com.blackmaria.R
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Tasks
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FetchPlaceResponse
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.PlacesClient
import java.util.*
import java.util.concurrent.TimeUnit


class GooglePlaceRepository(val mContext: Context, val listener: PlaceSearchList) : OnLocationSearched, Filterable {

    private var mResultList: ArrayList<PlacesDetail>? = null

    private lateinit var placesClient: PlacesClient
    private lateinit var token: AutocompleteSessionToken


    init {
        Places.initialize(mContext,mContext.getString(R.string.google_maps_key))
        placesClient = Places.createClient(mContext)
        token = AutocompleteSessionToken.newInstance()
    }
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val results = FilterResults()
                if (constraint != null) {
                    mResultList = getAutocomplete(constraint)
                    if (mResultList != null) {
                        results.values = mResultList
                        results.count = mResultList?.size ?: 0

                    }
                }
                return results
            }

            override fun publishResults(constraint: CharSequence, results: FilterResults?) {
            }
        }
    }

    override fun onLocationSearched(queryString: String) {
        this.filter.filter(queryString)
    }

    override fun onItemClicked(placesDetail: PlacesDetail) {

        val placeId = placesDetail.placeId.toString()
        val placeFields = Arrays.asList(Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val crequest = FetchPlaceRequest.builder(placeId, placeFields).build()
        placesClient.fetchPlace(crequest).addOnSuccessListener {
            if (it != null) {
                val places = it.place
                places.latLng?.let { latLng ->

                    listener.setPlaceDetail(PlacesDetail().apply {
                        location_name = places.address.toString()
                        label_name = places.name.toString()
                        latitude = latLng.latitude
                        longtitute = latLng.longitude
                        this.placeId = placeId
                    })
                }
            }
        }.addOnFailureListener {
            it.printStackTrace()

        }


    }

    private fun getAutocomplete(constraint: CharSequence): ArrayList<PlacesDetail>? {
        val resultList = ArrayList<PlacesDetail>()
        val request = FindAutocompletePredictionsRequest.builder().let {
            it.setSessionToken(token)
            it.setQuery(constraint.toString())
            it.build()
        }

        val task = placesClient.findAutocompletePredictions(request).addOnSuccessListener { response ->
            for (prediction in response.autocompletePredictions) {
                resultList.add(PlacesDetail().apply {
                    setLabel_name(prediction.getPrimaryText(null).toString())
                    setLocation_name(prediction.getFullText(null).toString())
                    setPlaceId(prediction.placeId)
                   /* var latLng :LatLng?=getPlacesLatLng(prediction.placeId)
                    if (latLng!=null){
                        setLatitude(latLng.latitude)
                        setLongtitute(latLng.longitude)
                    }*/
                    setPlaceType(0)
                })
            }



            if (resultList.count() > 0)
                listener.setPlaceList(resultList)
            else
                listener.setPlaceList(null)

        }.addOnFailureListener { exception ->
            exception.printStackTrace()
            resultList.clear()
            listener.setPlaceList(null)
        }
        try {
            Tasks.await(task, 5, TimeUnit.SECONDS)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return resultList
    }

    private fun getPlacesLatLng(placeId:String): LatLng? {

// Specify the fields to return.
        val placeFields = listOf(Place.Field.ID, Place.Field.NAME)

// Construct a request object, passing the place ID and fields array.
        val request = FetchPlaceRequest.newInstance(placeId, placeFields)
        var placeLatLng:LatLng? =null
        placesClient.fetchPlace(request)
                .addOnSuccessListener { response: FetchPlaceResponse ->
                    placeLatLng = response.place.latLng!!

                }.addOnFailureListener { exception: Exception ->
                    if (exception is ApiException) {
                        val statusCode = exception.statusCode
                        placeLatLng=null
                    }
                }

        return placeLatLng
    }


}