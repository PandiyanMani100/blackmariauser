package com.blackmaria.route



interface OnLocationSearched {
    fun onLocationSearched(queryString: String)
    fun onItemClicked(placesDetail: PlacesDetail)
}