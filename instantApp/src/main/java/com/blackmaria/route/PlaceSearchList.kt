package com.blackmaria.route

import java.util.*

interface PlaceSearchList {
    fun setPlaceList(placeDetailResult: ArrayList<PlacesDetail>?)
    fun setPlaceDetail(placeDetail: PlacesDetail)
}