package com.blackmaria.route;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;


import com.blackmaria.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by developer on 14/3/17.
 */

/*public class PlacesAutoCompleteAdapter extends ArrayAdapter<PlacesAutoCompleteAdapter.PredictionHolder> {

    private static final String TAG = "PlacesAutoCompleteAdapter";
    private ArrayList<PlacesDetail> mResultList;
    private Context mContext;

    public PlacesAutoCompleteAdapter(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public PredictionHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.support_simple_spinner_dropdown_item, viewGroup, false);
        return new PredictionHolder(convertView);
    }

    @Override
    public void onBindViewHolder(PredictionHolder mPredictionHolder, final int i) {
        PlacesDetail placesDetail = null;
        if (mResultList != null)
            placesDetail = mResultList.get(i);



    }

    public void submitList(ArrayList<PlacesDetail> mResultList) {
        this.mResultList = mResultList;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return mResultList != null ? mResultList.size() : 0;
    }

    public PlacesDetail getItem(int position) {
        return mResultList != null ? mResultList.get(position) : null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.support_simple_spinner_dropdown_item, viewGroup, false);
        return convertView;
    }

    class PredictionHolder extends RecyclerView.ViewHolder {
        private TextView tvPlaceName, tvAddress;
        private ImageView imgPlaceType;

        PredictionHolder(View itemView) {
            super(itemView);

        }

    }
}*/

public class PlacesAutoCompleteAdapter extends ArrayAdapter<PlacesDetail> {
    private LayoutInflater layoutInflater;
    List<PlacesDetail> mCustomers;

/*    private Filter mFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((PlacesDetail)resultValue).getLocation_name();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                ArrayList<PlacesDetail> suggestions = new ArrayList<PlacesDetail>();
                for (PlacesDetail customer : mCustomers) {
                    // Note: change the "contains" to "startsWith" if you only want starting matches
                    if (customer.getLocation_name().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(customer);
                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                addAll((ArrayList<PlacesDetail>) results.values);
            } else {
                // no filter, add entire original list back in
                addAll(mCustomers);
            }
            notifyDataSetChanged();
        }
    };*/

    public PlacesAutoCompleteAdapter(Context context, int textViewResourceId, List<PlacesDetail> customers) {
        super(context, textViewResourceId, customers);
        // copy all the customers into a master list
        mCustomers = new ArrayList<PlacesDetail>(customers.size());
        mCustomers.addAll(customers);
        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(android.R.layout.simple_list_item_1, null);
        }

        PlacesDetail customer = getItem(position);

        TextView name = (TextView) view.findViewById(android.R.id.text1);
        name.setText(customer.getLocation_name());

        return view;
    }

    @Nullable
    @Override
    public PlacesDetail getItem(int position) {
        return mCustomers.get(position);
    }

    /*@Override
    public Filter getFilter() {
        return mFilter;
    }*/

    public void submitList(ArrayList<PlacesDetail> mResultList) {
        this.mCustomers = mResultList;
        notifyDataSetChanged();
    }
}
