package com.blackmaria;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.widgets.CustomTextViewForm;
import com.blackmaria.widgets.TextRCBold;

import java.util.HashMap;

/**
 * Created by Muruganantham on 3/9/2018.
 */

@SuppressLint("Registered")
public class Reload_Transfer_PushNotification extends ActivityHockeyApp implements View.OnClickListener {
    private ImageView closeIcon;
    private TextView creditDebitType;
    private CustomTextViewForm payviaTv;
    private CustomTextViewForm phoneNumber;
    private TextRCBold walletAmount;
    private CustomTextViewForm transfererName;
    private CustomTextViewForm transferDate;
    private CustomTextViewForm transferTime;
    private ImageView pushCloseIc;
    private ImageView pushWalletIc;
    private ImageView pushBookIc;
    private ImageView pushReportIc;
    private String sDriveID = "", basicUserProfileUpdateStatus = "", UserProfileUpdateStatus = "", UserID = "";
    private SessionManager sessionManager;
    private String currency_code = "", Amount = "";
    String user_id = "", Str_action="",paymenttype = "", payment_via = "", from_number = "", from = "", date = "", time = "", trans_id = "";
    private CustomTextViewForm transactionNumber,networkfee,ampountreceived,accno;
    private LinearLayout banksuccess;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transfer_pushnotication);
        Initialization();
    }

    private void Initialization() {
        sessionManager = new SessionManager(this);
        closeIcon = (ImageView) findViewById(R.id.close_icon);
        creditDebitType = (TextView) findViewById(R.id.credit_debit_type);
        payviaTv = (CustomTextViewForm) findViewById(R.id.payvia_tv);
        phoneNumber = (CustomTextViewForm) findViewById(R.id.phone_number);
        walletAmount = (TextRCBold) findViewById(R.id.wallet_amount);
        transfererName = (CustomTextViewForm) findViewById(R.id.transferer_name);
        transferDate = (CustomTextViewForm) findViewById(R.id.transfer_date);
        transferTime = (CustomTextViewForm) findViewById(R.id.transfer_time);
        transactionNumber = (CustomTextViewForm) findViewById(R.id.transaction_number);
        pushCloseIc = (ImageView) findViewById(R.id.push_close_ic);
        pushWalletIc = (ImageView) findViewById(R.id.push_wallet_ic);
        pushBookIc = (ImageView) findViewById(R.id.push_book_ic);
        pushReportIc = (ImageView) findViewById(R.id.push_report_ic);


        networkfee=(CustomTextViewForm)findViewById(R.id.networkfee);
        ampountreceived=(CustomTextViewForm)findViewById(R.id.ampountreceived);
        accno=(CustomTextViewForm)findViewById(R.id.accno);
        banksuccess=(LinearLayout)findViewById(R.id.banksuccess);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_USERID);

        HashMap<String, String> info = sessionManager.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> user1 = sessionManager.getUserBasicProfile();
        basicUserProfileUpdateStatus = user1.get(SessionManager.BASIC_PROFILE);

        HashMap<String, String> user2 = sessionManager.getUserCompleteProfile();
        UserProfileUpdateStatus = user2.get(SessionManager.COMPLETE_PROFILE);
        Intent i=getIntent();
        Str_action=i.getStringExtra("Str_action");
        user_id = i.getStringExtra("user_id");
        currency_code = i.getStringExtra("Currencycode");
        Amount = i.getStringExtra("Amount");
        paymenttype = i.getStringExtra("paymenttype");
        payment_via = i.getStringExtra("payment_via");
        from_number = i.getStringExtra("from_number");
        from = i.getStringExtra("from");
        date = i.getStringExtra("date");
        time = i.getStringExtra("time");
        trans_id = i.getStringExtra("trans_id");

        if (Str_action.equalsIgnoreCase(Iconstant.wallet_credit)) {
            payviaTv.setText(payment_via);
            phoneNumber.setText(from_number);
            transfererName.setText(from);
            transferDate.setText(date);
            transferTime.setText(time);
            transactionNumber.setText(getResources().getString(R.string.trasn_number)+trans_id);
            walletAmount.setText(currency_code + " " + Amount);
            banksuccess.setVisibility(View.GONE);
        }else if(Str_action.equalsIgnoreCase(Iconstant.bank_success)){
            payviaTv.setText(payment_via);
            phoneNumber.setText(from_number);
            transfererName.setText(from);
            transferDate.setText(date);
            transferTime.setText(time);
            transactionNumber.setText(getResources().getString(R.string.trasn_number)+trans_id);


            walletAmount.setText(currency_code + " " + Amount);
            accno.setText("ACCOUNT NO");
            networkfee.setText("0");
            ampountreceived.setTag(Amount);

            banksuccess.setVisibility(View.VISIBLE);
        }else if(Str_action.equalsIgnoreCase(Iconstant.wallet_success)){
            payviaTv.setText(payment_via);
            phoneNumber.setText(from_number);
            transfererName.setText(from);
            transferDate.setText(date);
            transferTime.setText(time);
            transactionNumber.setText(getResources().getString(R.string.trasn_number)+trans_id);
            walletAmount.setText(currency_code + " " + Amount);
            banksuccess.setVisibility(View.GONE);
        }

        closeIcon.setOnClickListener(this);
        pushCloseIc.setOnClickListener(this);
        pushWalletIc.setOnClickListener(this);
        pushBookIc.setOnClickListener(this);
        pushReportIc.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == closeIcon) {

            Intent intent = new Intent(Reload_Transfer_PushNotification.this, Navigation_new.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == pushCloseIc) {
            Intent intent = new Intent(Reload_Transfer_PushNotification.this, Navigation_new.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == pushWalletIc) {
            Intent intent = new Intent(Reload_Transfer_PushNotification.this, WalletMoneyPage1.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == pushBookIc) {
            Intent intent = new Intent(Reload_Transfer_PushNotification.this, Navigation_new.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == pushReportIc) {
            Intent intent = new Intent(Reload_Transfer_PushNotification.this, Complaint_Page_new.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }
}
