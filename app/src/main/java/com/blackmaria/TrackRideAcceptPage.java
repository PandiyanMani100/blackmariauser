package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.android.volley.Request;
import com.blackmaria.adapter.DriverProfileAdapter;
import com.blackmaria.pojo.DriverProfilePojo;
import com.blackmaria.pojo.Trackrideaccept_pojo;
import com.blackmaria.subclass.ActivitySubClass;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
//import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.danikula.videocache.ProxyCacheException;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by user14 on 12/20/2016.
 */

public class TrackRideAcceptPage extends ActivitySubClass {

    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "", selectdCarmage;
    private LinearLayout freecall_id;
    private TextView Tv_driverCarType, Tv_VechileNo, Tv_Profile;
    private RelativeLayout driver_detailpage;
    private RelativeLayout Rl_TrackDriver;

    private String driverID = "", driverName = "", driverImage = "", driverRating = "",
            driverTime = "", rideID = "", driverMobile = "", driverCar_no = "", driverCar_model = "", flag = "";
    private ImageView driver_image, trackdriver_carphoto;
    public static Activity trackRideAcceptPage_class;

    private RefreshReceiver refreshReceiver;
    private Dialog profile_dialog;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private ArrayList<DriverProfilePojo> itemList;
    private boolean hasData = false;
    private DriverProfileAdapter adapter;
    LanguageDb mhelper;
    String Str_DriverName = "", Str_DriverId = "", Str_DriverImg = "", etaArrive = "", sDriverReview = "", sTotalReviewCount = "";
    private TextView etaTime_Tv, tv_drivername;
    private String Str_phone_number = "";
    final int PERMISSION_REQUEST_CODE = 111;
    final int PERMISSION_REQUEST_CODES = 222;

    private VideoView videoView;
    private SpinKitView spinKitView;
    private ImageView homeAnimateImage;
    int stopPosition = 0;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {

                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(TrackRideAcceptPage.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(TrackRideAcceptPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", rideID);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(TrackRideAcceptPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", rideID);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }

                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_track_layout);
        mhelper =new LanguageDb(this);
        trackRideAcceptPage_class = TrackRideAcceptPage.this;

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.app.timerPage.finish");
        sendBroadcast(broadcastIntent);
        Intent broadcastIntent1 = new Intent();
        broadcastIntent1.setAction("com.package.finish.BookingPage");
        sendBroadcast(broadcastIntent1);
        initialize();

        Rl_TrackDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Move to ride Detail page
                Intent i = new Intent(TrackRideAcceptPage.this, TrackRidePage.class);
                i.putExtra("ride_id", rideID);
                startActivity(i);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();

            }
        });
        freecall_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Str_phone_number.equalsIgnoreCase("")) {
//                    HashMap<String, String> phone = session.getPhoneMaskingStatus();
                    String sPhoneMaskingStatus = session.getPhoneMaskingStatus();

                    if (sPhoneMaskingStatus.equalsIgnoreCase("Yes")) {
                        postRequest_PhoneMasking(Iconstant.phoneMasking_url);
                    } else {
                        if (Build.VERSION.SDK_INT >= 23) {
                            // Marshmallow+
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_phone_number));
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_phone_number));
                            startActivity(intent);
                        }
                    }

                } else {
                    Alert(getkey("action_error"),getkey("inavid_call"));
                }
            }
        });

        driver_detailpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileDialog();
            }
        });
    }

    private void initialize() {

        session = new SessionManager(TrackRideAcceptPage.this);
        cd = new ConnectionDetector(TrackRideAcceptPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        itemList = new ArrayList<DriverProfilePojo>();

        videoView = findViewById(R.id.videoView);
        spinKitView = findViewById(R.id.spin_kit1);
        homeAnimateImage = findViewById(R.id.home_animate_image);

        Tv_VechileNo = (TextView) findViewById(R.id.trackdriver_carnumber_textview);
        Tv_driverCarType = (TextView) findViewById(R.id.trackdriver_driver_cartype_textview);
        freecall_id = (LinearLayout) findViewById(R.id.freecall_id);
        Rl_TrackDriver = (RelativeLayout) findViewById(R.id.trackdriver_bottom_layout);
        Tv_Profile = (TextView) findViewById(R.id.trackdriver_profile_textview);
        Tv_Profile.setText(getkey("viewprofile"));
        driver_image = (ImageView) findViewById(R.id.driver_image);
        etaTime_Tv = (TextView) findViewById(R.id.eta_time);
        driver_detailpage = (RelativeLayout) findViewById(R.id.driver_detailpage);
        tv_drivername = findViewById(R.id.trackdriver_name);

        TextView ontheway = findViewById(R.id.ontheway);
        ontheway.setText(getkey("ontheway"));

        TextView tracme = findViewById(R.id.tracme);
        tracme.setText(getkey("trackvehicle"));

        HashMap<String, String> carImage = session.getSelectedCarCatImage();
        selectdCarmage = carImage.get(SessionManager.SELECTED_CAR_IMAGE);
//        trackdriver_carphoto = (ImageView) findViewById(R.id.trackdriver_carphoto);
//        try {
//            Picasso.with(getApplicationContext()).load(String.valueOf(selectdCarmage)).into(trackdriver_carphoto);
//        } catch (Exception e) {
//
//        }

//        ImageView Rl_drawer = (ImageView) findViewById(R.id.drawer_icon);
//        /*GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(Rl_drawer);
//        Glide.with(this).load(R.drawable.alertgif).into(imageViewTarget);*/
//        final Animation animation = new AlphaAnimation(1, 0);
//        animation.setDuration(500);
//        animation.setInterpolator(new LinearInterpolator());
//        animation.setRepeatCount(Animation.INFINITE);
//        animation.setRepeatMode(Animation.REVERSE);
//        Rl_drawer.startAnimation(animation);
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        Intent intent = getIntent();
        if (intent != null) {
            flag = intent.getStringExtra("flag");

            if (flag.equalsIgnoreCase("2")) {
                Str_phone_number = intent.getStringExtra("mobile_number");
                driverID = intent.getStringExtra("driverID");
                rideID = intent.getStringExtra("rideID");
                driverImage = intent.getStringExtra("driverImage");
                driverCar_no = intent.getStringExtra("driverCar_no");
                driverCar_model = intent.getStringExtra("driverCar_model");
                freecall_id.setVisibility(View.VISIBLE);
                driver_detailpage.setVisibility(View.GONE);
            } else {
                freecall_id.setVisibility(View.GONE);
                driver_detailpage.setVisibility(View.VISIBLE);
                driverID = intent.getStringExtra("driverID");
                driverName = intent.getStringExtra("driverName");
                tv_drivername.setText(driverName);
                driverImage = intent.getStringExtra("driverImage");
                driverRating = intent.getStringExtra("driverRating");
                driverTime = intent.getStringExtra("driverTime");
                rideID = intent.getStringExtra("rideID");
                driverMobile = intent.getStringExtra("driverMobile");
                driverCar_no = intent.getStringExtra("driverCar_no");
                driverCar_model = intent.getStringExtra("driverCar_model");
            }

        }
        try {
            Picasso.with(TrackRideAcceptPage.this)
                    .load(driverImage)
                    .placeholder(R.drawable.no_user_img)
                    .resize(50, 50)// optional
                    .error(R.drawable.no_user_img)      // optional
                    .into(driver_image);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Tv_VechileNo.setText(driverCar_no);
        Tv_driverCarType.setText(driverCar_model);

        if (isInternetPresent) {
            DriverProfileRequest(Iconstant.driverProfilePage_url);
        } else {
            alert(getkey("alert_nointernet"), getkey("alert_nointernet_message"));
        }

        videoView.setVisibility(View.VISIBLE);
        try {
            String uriPath = "android.resource://com.blackmaria/" + R.raw.ontheway;
            PlayVideos(uriPath);
        } catch (ProxyCacheException e) {
            e.printStackTrace();
        }

    }

    private void DriverProfileRequest(String Url) {
        dialog = new Dialog(TrackRideAcceptPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverID);
        jsonParams.put("ride_id", rideID);

        System.out.println("--------------DriverProfileRequestUrl-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(TrackRideAcceptPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverProfileRequest response-------------------" + response);
                String status = "", sTitle_text = "", sSubtitle_text = "", sRideStatus = "";


                try {
                    JSONObject object = new JSONObject(response);

                    Type listType = new TypeToken<Trackrideaccept_pojo>() {
                    }.getType();
                    Trackrideaccept_pojo trackpojo = new GsonBuilder().create().fromJson(object.toString(), listType);

                    if (object.length() > 0) {
                        status = trackpojo.getStatus();
                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {
                                etaArrive = trackpojo.getResponse().getEta_arrive();
                                etaTime_Tv.setText(getkey("in") +" "+ etaArrive);
                                sTotalReviewCount = trackpojo.getResponse().getTotal_review_count();

                                JSONObject profile_jsonObject = jsonObject.getJSONObject("driver_profile");
                                if (profile_jsonObject.length() > 0) {
                                    Str_DriverName = trackpojo.getResponse().getDriver_profile().getDriver_name();
                                    tv_drivername.setText(Str_DriverName);
                                    Str_DriverId = trackpojo.getResponse().getDriver_profile().getDriver_id();
                                    Str_DriverImg = trackpojo.getResponse().getDriver_profile().getDriver_image();
                                    sDriverReview = trackpojo.getResponse().getDriver_profile().getDriver_review();

                                    if (profile_jsonObject.has("phone_number")) {
                                        session.setKeyDriverPhonenumber(trackpojo.getResponse().getDriver_profile().getDriver_phone());
                                    }
                                }


                                Object contentArray_check = jsonObject.get("driver_array_view");
                                if (contentArray_check instanceof JSONArray) {
                                    JSONArray contentArray = jsonObject.getJSONArray("driver_array_view");
                                    if (contentArray.length() > 0) {
                                        itemList.clear();
                                        itemList.addAll(trackpojo.getResponse().getDriver_array_view());
//                                        for (int i = 0; i < contentArray.length(); i++) {
//                                            JSONObject content_jsonObject = contentArray.getJSONObject(i);
//                                            DriverProfilePojo pojo = new DriverProfilePojo();
//                                            pojo.setTitle(content_jsonObject.getString("title"));
//                                            pojo.setValue(content_jsonObject.getString("value"));
//
//                                            itemList.add(pojo);
//                                        }
                                        hasData = true;
                                    } else {
                                        hasData = false;
                                    }
                                } else {
                                    hasData = false;
                                }

//                                if (trackpojo.getResponse().getPhoto_video_type().equalsIgnoreCase("mp4")) {
//
//
//                                } else if (trackpojo.getResponse().getPhoto_video_type().equalsIgnoreCase("gif")) {
//                                    videoView.setVisibility(View.GONE);
//                                    homeAnimateImage.setVisibility(View.VISIBLE);
//                                    Glide.with(TrackRideAcceptPage.this)
//                                            .load(trackpojo.getResponse().getPhoto_video())
//                                            .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
//                                            .into(new SimpleTarget<Drawable>() {
//                                                @Override
//                                                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                                                    homeAnimateImage.setImageDrawable(resource);
//                                                }
//                                            });
//                                } else {
//                                    videoView.setVisibility(View.GONE);
//                                    homeAnimateImage.setVisibility(View.VISIBLE);
//                                    Picasso.with(TrackRideAcceptPage.this).load(String.valueOf(trackpojo.getResponse().getPhoto_video())).into(homeAnimateImage);
//                                }
                            }
                        } else {
                            String sResponse = object.getString("response");
                            alert(getkey("action_error"), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void profileDialog() {

        final MaterialDialog dialog = new MaterialDialog(TrackRideAcceptPage.this);
        View view = LayoutInflater.from(TrackRideAcceptPage.this).inflate(R.layout.driver_profile_dialog, null);

        final TextView Tv_close = (TextView) view.findViewById(R.id.driver_profile_close_textview);
        CustomTextView Tv_driver_name = (CustomTextView) view.findViewById(R.id.driver_profile_driver_name);
        ExpandableHeightListView overview_listView = (ExpandableHeightListView) view.findViewById(R.id.driver_profile_expandable_listview);
        RoundedImageView Iv_profileImage = (RoundedImageView) view.findViewById(R.id.driver_profile_imageview);
        RatingBar Rb_driver_rating = (RatingBar) view.findViewById(R.id.driver_profile_ratingbar);
//        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
        TextView driver_profile_driver_profile_lable = (TextView) view.findViewById(R.id.driver_profile_driver_profile_lable);
        driver_profile_driver_profile_lable.setText(getkey("profile_label_driverprofile"));

        Tv_close.setText(getkey("profile_label_close"));
        Drawable drawable = Rb_driver_rating.getProgressDrawable();
        drawable.setColorFilter(Color.parseColor("#FF9900"), PorterDuff.Mode.SRC_ATOP);
        CustomTextView Tv_listempty = (CustomTextView) view.findViewById(R.id.driver_profile_list_empty_textview);

        Tv_driver_name.setText(Str_DriverName.toUpperCase());
        Picasso.with(TrackRideAcceptPage.this)
                .load(Str_DriverImg)
                .placeholder(R.drawable.no_user_img)   // optional
                .error(R.drawable.no_user_img)      // optional
                .into(Iv_profileImage);
        Rb_driver_rating.setRating(Float.parseFloat(sDriverReview));


        if (hasData) {
            Tv_listempty.setVisibility(View.GONE);
            adapter = new DriverProfileAdapter(TrackRideAcceptPage.this, itemList);
            overview_listView.setAdapter(adapter);
        } else {
            Tv_listempty.setVisibility(View.VISIBLE);
        }


        Tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.setBackground(getResources().getDrawable(R.drawable.transparant_bg));
        dialog.setView(view).show();
    }

    //--------------Alert Method-----------
    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TrackRideAcceptPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("==============accept page finishd========");
        unregisterReceiver(refreshReceiver);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }

    //----------------------------Run time permission for call--------------------------------------------

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODES);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_phone_number));
                    startActivity(intent);
                }
                break;


        }
    }

    //---------------------------------------------------------------------
    private void postRequest_PhoneMasking(String Url) {

        dialog = new Dialog(TrackRideAcceptPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", rideID);
        jsonParams.put("user_type", "user");

        System.out.println("-------------PhoneMasking Url----------------" + Url);
        mRequest = new ServiceRequest(TrackRideAcceptPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------PhoneMasking Response----------------" + response);

                String Str_status = "", SResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    SResponse = object.getString("response");
                    if (Str_status.equalsIgnoreCase("1")) {
                        Alert(getkey("action_success"), SResponse);
                    } else {
                        Alert(getkey("action_error"), SResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(TrackRideAcceptPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    private void PlayVideos(String videourl) throws ProxyCacheException {
        spinKitView.setVisibility(View.VISIBLE);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) videoView.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        videoView.setLayoutParams(params);
        videoView.setOnInfoListener(onInfoToPlayStateListener);

        String uriPath = videourl;
        Uri uri = Uri.parse(uriPath);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        MediaController mediaController = null;
        mediaController = new MediaController(this);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
            if (mediaController != null) {
                mediaController.setAnchorView(videoView);
            }
        mediaController.setVisibility(View.GONE);
        videoView.setMediaController(mediaController);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                spinKitView.setVisibility(View.GONE);
                mp.setLooping(true);
            }
        });
        videoView.start();

    }

    public void onResume() {
        super.onResume();

        videoView.seekTo(stopPosition);
        videoView.start(); //Or use resume() if it doesn't work. I'm not sure

    }

    @Override
    public void onPause() {
        super.onPause();
        /*if(dialog!=null){
            dialog.dismiss();
        }*/

        stopPosition = videoView.getCurrentPosition(); //stopPosition is an int
        videoView.pause();
        //dialogDismiss();
    }

    private final MediaPlayer.OnInfoListener onInfoToPlayStateListener = new MediaPlayer.OnInfoListener() {

        @Override
        public boolean onInfo(MediaPlayer mp, int videoReqCode, int extra) {
            if (MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START == videoReqCode) {
                spinKitView.setVisibility(View.GONE);
            }
            if (MediaPlayer.MEDIA_INFO_BUFFERING_START == videoReqCode) {
                spinKitView.setVisibility(View.VISIBLE);
            }
            if (MediaPlayer.MEDIA_INFO_BUFFERING_END == videoReqCode) {
                spinKitView.setVisibility(View.GONE);
            }
            return false;
        }
    };

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
