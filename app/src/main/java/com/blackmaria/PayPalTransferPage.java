package com.blackmaria;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.TextRCBold;
import com.devspark.appmsg.AppMsg;

import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by user129 on 2/28/2018.
 */
public class PayPalTransferPage extends ActivityHockeyApp {

    private CustomEdittext Ed_paypal_id;
    private RelativeLayout Rl_cancel, Rl_ok;
    private String UserID = "";
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private SmoothProgressBar loading;
    private String withdrawel_satus = "", payment_code = "", sSecurePin = "", Samount = "",Swithdrawel_status_txt="",Scurrency="";
    private ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paypal_transfer_page);
        initization();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PayPalTransferPage.this, WalletMoneyPage1.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        Rl_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PayPalTransferPage.this, WalletMoneyPage1.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        Rl_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CloseKeyboard(Ed_paypal_id);

                if (Ed_paypal_id.getText().toString().length() == 0) {

                    erroredit(Ed_paypal_id, getResources().getString(R.string.enterpaidid));

                } else {


                    if (withdrawel_satus.equalsIgnoreCase("1")) {

                        PaypalConfirmFullDialog();

                    } else {

                        Alert(getResources().getString(R.string.action_error), Swithdrawel_status_txt);
                    }

                }
            }
        });


    }


    private void initization() {
        session = new SessionManager(PayPalTransferPage.this);
        cd = new ConnectionDetector(PayPalTransferPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        sSecurePin = session.getSecurePin();
        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        img_back = (ImageView) findViewById(R.id.img_back);
        Ed_paypal_id = (CustomEdittext) findViewById(R.id.ed_paypal_id);
        Rl_cancel = (RelativeLayout) findViewById(R.id.paypal_transf_cancel);
        Rl_ok = (RelativeLayout) findViewById(R.id.paypal_transf_ok);

        Intent in = getIntent();
        withdrawel_satus = in.getStringExtra("withdrawel_status");
        payment_code = in.getStringExtra("payment_code");
        Samount = in.getStringExtra("amount");
        Swithdrawel_status_txt=in.getStringExtra("Swithdrawel_status_txt");
        Scurrency=in.getStringExtra("currency");

    }


    private void PaypalConfirmFullDialog() {

        final Dialog dialog = new Dialog(PayPalTransferPage.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.paypal_confirm_fullscreen_dialog);

        TextRCBold paypal_amount = (TextRCBold) dialog.findViewById(R.id.paypal_amount);
        TextRCBold paypal_id = (TextRCBold) dialog.findViewById(R.id.paypal_id_txt);

        final RelativeLayout Rl_cancel = (RelativeLayout) dialog.findViewById(R.id.paycal_cancel);

        final RelativeLayout Rl_confirm = (RelativeLayout) dialog.findViewById(R.id.paypal_confirm);

        loading=(SmoothProgressBar)dialog.findViewById(R.id.paypal_loading_progressbar);

        paypal_amount.setText(Scurrency+" "+Samount);
        paypal_id.setText(Ed_paypal_id.getText().toString().trim());


        Rl_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();


            }
        });


        Rl_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent in=new Intent(PayPalTransferPage.this, CloudTransferAmountPinEnter1.class);
                in.putExtra("flag","paypal_withdrawal");
                in.putExtra("amount",Samount);
                in.putExtra("payment_code",payment_code);
                in.putExtra("paypal_id",Ed_paypal_id.getText().toString().trim());
                startActivity(in);
                finish();
                Ed_paypal_id.setText("");



            }
        });


        dialog.show();

    }









    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(PayPalTransferPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(PayPalTransferPage.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    //--------------Close KeyBoard Method-----------
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(PayPalTransferPage.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

}
