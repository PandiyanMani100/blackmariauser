package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.HomeRateCardAdapter;
import com.blackmaria.adapter.HomeRateCardFareSummaryAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.RateCard_CardDisplayPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;



public class Home_RatecardPage extends ActivityHockeyApp {
    private SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ExpandableHeightListView Lv_ratecard_detail, Lv_radecard_fare_info;
    private ServiceRequest rateCard_mRequest;
    private Dialog dialog;
    LanguageDb mhelper;
    ArrayList<RateCard_CardDisplayPojo> rate_itemList;
    ArrayList<RateCard_CardDisplayPojo> dertails_itemList;
    private boolean isDataAvailable = false;
    private String ScurrencySymbol = "";
    private HomeRateCardAdapter details_adapter;
    private HomeRateCardFareSummaryAdapter fare_adapter;
    private ImageView back1, ratecard_car_imageview;
    private TextView tripdate, vehicletype, kilometer, rideamount, eta_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ratecard_page_constrain);
        mhelper  = new LanguageDb(this);
        initialize();
        back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    private void initialize() {

        session = new SessionManager(Home_RatecardPage.this);
        cd = new ConnectionDetector(Home_RatecardPage.this);
        isInternetPresent = cd.isConnectingToInternet();

        rate_itemList = new ArrayList<RateCard_CardDisplayPojo>();
        dertails_itemList = new ArrayList<RateCard_CardDisplayPojo>();

        TextView tripsumm = findViewById(R.id.tripsumm);
        tripsumm.setText(getkey("trip_summary_lable"));

        TextView diatancelabel = findViewById(R.id.diatancelabel);
        diatancelabel.setText(getkey("sdistance"));

        TextView estimatedfarelable = findViewById(R.id.estimatedfarelable);
        estimatedfarelable.setText(getkey("estimated_fare"));

        TextView etalabel = findViewById(R.id.etalabel);
        etalabel.setText(getkey("etaa"));
        TextView mintues = findViewById(R.id.mintues);
        mintues.setText(getkey("minutes"));

        TextView fareinfo = findViewById(R.id.fareinfo);
        fareinfo.setText(getkey("fare_information"));

        TextView estimatedfaretripinfo = findViewById(R.id.estimatedfaretripinfo);
        estimatedfaretripinfo.setText(getkey("estimated_fare_and_trip_information"));

        TextView finalfare = findViewById(R.id.finalfare);
        finalfare.setText(getkey("final_fare_may_different_from_estimated_fare"));

        TextView secondfinalfare = findViewById(R.id.secondfinalfare);
        secondfinalfare.setText(getkey("subject_to_distance_and_time_while_on_trip"));

        TextView kilometerlabel = findViewById(R.id.kilometerlabel);
        kilometerlabel.setText(getkey("homepage_km"));



        eta_time = findViewById(R.id.eta_time);
        kilometer = findViewById(R.id.kilometer);
        rideamount = findViewById(R.id.rideamount);
        tripdate = findViewById(R.id.tripdate);
        vehicletype = findViewById(R.id.vehicletype);
        back1 = findViewById(R.id.back_imageview1);
        ratecard_car_imageview = findViewById(R.id.ratecard_car_imageview);
        Lv_ratecard_detail = (ExpandableHeightListView) findViewById(R.id.ratecard_details_listView);
        Lv_radecard_fare_info = (ExpandableHeightListView) findViewById(R.id.ratecard_fare_info_listView);

        Intent intent = getIntent();
        HashMap<String, String> jsonParams = (HashMap<String, String>) intent.getSerializableExtra("jsonParam");

        Lv_ratecard_detail.setExpanded(true);
        Lv_radecard_fare_info.setExpanded(true);
        if (isInternetPresent) {
            rateCard_displayRequest(Iconstant.get_ratecard_url, jsonParams);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Home_RatecardPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-----------------------Rate Card Display Post Request-----------------
    private void rateCard_displayRequest(String Url, final HashMap<String, String> jsonParams) {
        dialog = new Dialog(Home_RatecardPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-------------Rate Card Url----------------" + Url);
        System.out.println("-------------Rate Card jsonParams----------------" + jsonParams);

        rateCard_mRequest = new ServiceRequest(Home_RatecardPage.this);
        rateCard_mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Rate Card Response----------------" + response);

                String Sstatus = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            tripdate.setText(getkey("trip_date") + response_object.getString("trip_date"));
                            vehicletype.setText( getkey("vehicle_colon") + response_object.getString("vehicle_name"));
                            kilometer.setText(response_object.getString("distance"));
                            rideamount.setText(response_object.getString("eta_fare"));
                            eta_time.setText(response_object.getString("eta_time"));

                            RequestOptions options = new RequestOptions()
                                    .centerCrop()
                                    .placeholder(R.drawable.car_img)
                                    .error(R.drawable.car_img)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL);
                            Glide.with(Home_RatecardPage.this).load(response_object.getString("cat_image")).apply(options).into(ratecard_car_imageview);

                            ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(response_object.getString("currency"));
                            Object check_triparr_object = response_object.get("tripArr");
                            if (check_triparr_object instanceof JSONArray) {
                                JSONArray triparr_array = response_object.getJSONArray("tripArr");
                                if (triparr_array.length() > 0) {
                                    dertails_itemList.clear();
                                    for (int i = 0; i < triparr_array.length(); i++) {
                                        JSONObject standard_object = triparr_array.getJSONObject(i);
                                        RateCard_CardDisplayPojo stdrate_pojo = new RateCard_CardDisplayPojo();
                                        stdrate_pojo.setRate_title(standard_object.getString("title"));
                                        stdrate_pojo.setRate_value(standard_object.getString("value"));
                                        stdrate_pojo.setRate_currencySymbol(ScurrencySymbol);
                                        dertails_itemList.add(stdrate_pojo);
                                    }
                                }
                            }

                            Object check_ratecard_object = response_object.get("ratecard");
                            if (check_ratecard_object instanceof JSONArray) {

                                JSONArray ratecard_array = response_object.getJSONArray("ratecard");
                                if (ratecard_array.length() > 0) {
                                    rate_itemList.clear();
                                    for (int j = 0; j < ratecard_array.length(); j++) {
                                        JSONObject extra_object = ratecard_array.getJSONObject(j);
                                        RateCard_CardDisplayPojo rate_pojo = new RateCard_CardDisplayPojo();
                                        rate_pojo.setRate_title(extra_object.getString("title"));
                                        rate_pojo.setRate_value(ScurrencySymbol + " " + extra_object.getString("value"));
                                        rate_pojo.setRate_currencySymbol(ScurrencySymbol);
                                        rate_itemList.add(rate_pojo);
                                    }
                                }
                            }
                            isDataAvailable = true;

                        } else {
                            isDataAvailable = false;
                        }
                    } else {
                        isDataAvailable = false;
                    }


                    if (Sstatus.equalsIgnoreCase("1") && isDataAvailable) {


                        if (dertails_itemList.size() > 0) {
                            details_adapter = new HomeRateCardAdapter(Home_RatecardPage.this, dertails_itemList);
                            Lv_ratecard_detail.setAdapter(details_adapter);
                        }

                        if (rate_itemList.size() > 0) {
                            fare_adapter = new HomeRateCardFareSummaryAdapter(Home_RatecardPage.this, rate_itemList);
                            Lv_radecard_fare_info.setAdapter(fare_adapter);
                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}
