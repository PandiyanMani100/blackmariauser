package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.danikula.videocache.ProxyCacheException;
import com.github.ybq.android.spinkit.SpinKitView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


/**
 * Created by PremKumar on 9/23/2015.
 */
public class TimerPage extends ActivityHockeyApp {/**/
    int seconds = 0;
    private String retry = "";
    private String rideID = "";
    private String userID = "";
    LanguageDb mhelper;
    BroadcastReceiver updateReciver;
    private SessionManager sessionManager;

    private RelativeLayout Rl_cancelRide;
    //    private ProgressBar cancel_progressbar;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private Toast toast;
    private PkDialog mdialog;
    Handler mHandler;
    int count = 0;
    TextView tv_countreduce, tv_resend;
    private ImageView Iv_loading;
    Animation rotate;
    VideoView simpleVideoView;
    private Dialog dialog;
    private int seondsreduce = 0;
    private String photovideo = "", photovideotype = "";

    private SpinKitView spinKitView;
    private ImageView homeAnimateImage;

    int stopPosition = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timer_layout);
        mhelper = new LanguageDb(this);
        try {
            initialize();
            // Receiving the data from broadcast
            IntentFilter filter = new IntentFilter();
            filter.addAction("com.app.pushnotification.RideAccept");
            filter.addAction("com.app.timerPage.finish");
            updateReciver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    System.out.println("----------message--------------" + intent.getStringExtra("message"));

                    if (intent.getAction().equalsIgnoreCase("com.app.pushnotification.RideAccept")) {
//                        if (intent.getStringExtra("Action").equalsIgnoreCase("ride_confirmed")) {

                        sessionManager.setCouponCode("", "");
                        sessionManager.setReferralCode("", "");

                        Intent i = new Intent(TimerPage.this, TrackRideAcceptPage.class);
                        i.putExtra("driverID", intent.getStringExtra("driverID"));
                        i.putExtra("driverName", intent.getStringExtra("driverName"));
                        i.putExtra("driverImage", intent.getStringExtra("driverImage"));
                        i.putExtra("driverRating", intent.getStringExtra("driverRating"));
                        i.putExtra("driverTime", intent.getStringExtra("driverTime"));
                        i.putExtra("rideID", intent.getStringExtra("rideID"));
                        i.putExtra("driverMobile", intent.getStringExtra("driverMobile"));
                        i.putExtra("driverCar_no", intent.getStringExtra("driverCar_no"));
                        i.putExtra("driverCar_model", intent.getStringExtra("driverCar_model"));
                        i.putExtra("flag", "1");
                        startActivity(i);
                        mHandler.removeCallbacks(mRunnable);
                        //timer.cancel();

                        if (mRequest != null) {
                            mRequest.cancelRequest();
                        }

                        finish();
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                        }
                    } else if (intent.getAction().equalsIgnoreCase("com.app.timerPage.finish")) {
                        finish();
                    }
                }
            };
            registerReceiver(updateReciver, filter);


            Rl_cancelRide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mdialog = new PkDialog(TimerPage.this);
                    mdialog.setDialogTitle(getkey("timer_label_alert_cancel_ride"));
                    mdialog.setDialogMessage(getkey("timer_label_alert_cancel_ride_message"));
                    mdialog.setPositiveButton(getkey("timer_label_alert_yes"), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mdialog.dismiss();
                            cd = new ConnectionDetector(TimerPage.this);
                            isInternetPresent = cd.isConnectingToInternet();

                            if (isInternetPresent) {
                                toast.cancel();
                                DeleteRideRequest(Iconstant.delete_ride_url);
                            } else {
                                toast.setText(getkey("alert_nointernet_message"));
                                toast.show();
                            }
                        }
                    });
                    mdialog.setNegativeButton(getkey("timer_label_alert_no"), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mdialog.dismiss();
                        }
                    });
                    mdialog.show();
                }
            });
        } catch (NullPointerException e) {

        } catch (Exception e) {
        }

    }

    private void initialize() {
        toast = new Toast(TimerPage.this);
        sessionManager = new SessionManager(TimerPage.this);

        sessionManager.setTimerPage("available");

         simpleVideoView = (VideoView) findViewById(R.id.simpleVideoView);
        simpleVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.radar));
        simpleVideoView.start();
        simpleVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        Rl_cancelRide = findViewById(R.id.timer_page_cancel_layout);

        spinKitView = findViewById(R.id.spin_kit1);

        TextView tvpleasewait = findViewById(R.id.tvpleasewait);
        tvpleasewait.setText(getkey("plswait"));

        TextView tv_authenticate = findViewById(R.id.tv_authenticate);
        tv_authenticate.setText(getkey("connectingvehicles"));

        TextView secondss = findViewById(R.id.secondss);
        secondss.setText(getkey("seconds"));

        TextView can = findViewById(R.id.can);
        can.setText(getkey("cancel_lable"));


        homeAnimateImage = findViewById(R.id.home_animate_image);
//        cancel_progressbar = (ProgressBar) findViewById(R.id.timer_page_cancel_progressbar);
//        Iv_loading = (ImageView) findViewById(R.id.rotate_animation_imageview);
        tv_countreduce = findViewById(R.id.tv_countreduce);
        tv_resend = findViewById(R.id.tv_resend);
        // load the animation
//        rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_image);
//        Iv_loading.startAnimation(rotate);
//        Typeface face = Typeface.createFromAsset(getAssets(),
//                "fonts/vladimir.ttf");
//
//        locatingText.setTypeface(face);
//        rotate.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation anim) {
//                System.out.println("-------anim.getRepeatCount() onAnimationStart-----------" + anim.getDuration());
//            }
//
//            @Override
//            public void onAnimationEnd(Animation anim) {
//                System.out.println("-------anim.getRepeatCount() onAnimationEnd-----------" + anim.getDuration());
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation anim) {
//                System.out.println("-------anim.getRepeatCount() onAnimationRepeat-----------" + anim.getDuration());
//            }
//        });

        HashMap<String, String> userDetail = sessionManager.getUserDetails();
        userID = userDetail.get(SessionManager.KEY_USERID);

        Intent intent = getIntent();
        seconds = Integer.parseInt(intent.getStringExtra("Time")) + 1;
        retry = intent.getStringExtra("retry_count");
        rideID = intent.getStringExtra("ride_ID");
        if (intent.hasExtra("Str_photo")) {
            photovideo = intent.getStringExtra("Str_photo");
            photovideotype = intent.getStringExtra("Str_photo_type");

//            if (photovideotype.equalsIgnoreCase("mp4")) {
               // videoView.setVisibility(View.VISIBLE);


//            }

//            if (photovideotype.equalsIgnoreCase("mp4")) {
//                videoView.setVisibility(View.VISIBLE);
//                try {
//                    PlayVideos(photovideo);
//                } catch (ProxyCacheException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (photovideotype.equalsIgnoreCase("gif")) {
//                videoView.setVisibility(View.GONE);
//                homeAnimateImage.setVisibility(View.VISIBLE);
//                Glide.with(this)
//                        .load(photovideo)
//                        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
//                        .into(new SimpleTarget<Drawable>() {
//                            @Override
//                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                                homeAnimateImage.setImageDrawable(resource);
//                            }
//                        });
//            } else {
//                videoView.setVisibility(View.GONE);
//                homeAnimateImage.setVisibility(View.VISIBLE);
//                Picasso.with(this).load(String.valueOf(photovideo)).into(homeAnimateImage);
//            }
        }
        seondsreduce = seconds;
        tv_countreduce.setText(String.valueOf(seondsreduce));


        if (retry != null && retry.length() > 0) {
            retry = "2";
            tv_resend.setText("Second");
        } else {
            retry = "1";
            tv_resend.setText("First");
        }

        System.out.println("Seconds " + seconds);

        mHandler = new Handler();
        mHandler.post(mRunnable);


    }


    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (count < seconds) {
                count++;
                tv_countreduce.setText(String.valueOf(seondsreduce--));
                if(tv_countreduce.getText().toString().endsWith("0"))
                {
                    checkrideacceptornot(Iconstant.track_your_ride_url);
                }
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);

                if (mRequest != null) {
                    mRequest.cancelRequest();
                }

                Intent returnIntent = new Intent();
                returnIntent.putExtra("Accepted_or_Not", "not");
                returnIntent.putExtra("Retry_Count", retry);
                setResult(RESULT_OK, returnIntent);
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            }
        }
    };


    //-------------------Delete Ride Post Request----------------

    private void DeleteRideRequest(String Url) {

        dialog = new Dialog(TimerPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Rl_cancelRide.setVisibility(View.GONE);
//        cancel_progressbar.setVisibility(View.VISIBLE);

        System.out.println("--------------Timer Delete Ride url-------------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", userID);
        jsonParams.put("ride_id", rideID);

        mRequest = new ServiceRequest(TimerPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String status = object.getString("status");
                        String response_value = object.getString("response");
                        if (status.equalsIgnoreCase("1")) {

                            Rl_cancelRide.setVisibility(View.VISIBLE);
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            try {
//                            cancel_progressbar.setVisibility(View.GONE);
                                mHandler.removeCallbacks(mRunnable);
                            } catch (Exception e) {

                            }

                            try {
                                final JSONObject jsonObject = new JSONObject(response_value);
                                final PkDialog mDialog = new PkDialog(TimerPage.this);
                                mDialog.setDialogTitle(getkey("timer_sorry"));
                                mDialog.setDialogMessage(jsonObject.getString("message"));
                                mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            if (jsonObject.getString("message").equalsIgnoreCase("ride confirmed")) {
                                                mDialog.dismiss();
                                                Intent broadcastIntent = new Intent();
                                                broadcastIntent.setAction("com.app.pushnotification.RideAccept");
                                                broadcastIntent.putExtra("driverID", jsonObject.getJSONObject("driver_profile").getString("driver_id"));
                                                broadcastIntent.putExtra("driverName", jsonObject.getJSONObject("driver_profile").getString("driver_name"));
                                                broadcastIntent.putExtra("driverImage", jsonObject.getJSONObject("driver_profile").getString("driver_image"));
                                                broadcastIntent.putExtra("driverRating", jsonObject.getJSONObject("driver_profile").getString("driver_review"));
//                                            broadcastIntent.putExtra("driverTime", jsonObject.getJSONObject("driver_profile").getString("driverTime"));
                                                broadcastIntent.putExtra("rideID", jsonObject.getString("ride_id"));
                                                broadcastIntent.putExtra("driverMobile", jsonObject.getJSONObject("driver_profile").getString("phone_number"));
                                                broadcastIntent.putExtra("driverCar_no", jsonObject.getJSONObject("driver_profile").getString("vehicle_number"));
                                                broadcastIntent.putExtra("driverCar_model", jsonObject.getJSONObject("driver_profile").getString("vehicle_model"));
                                                broadcastIntent.putExtra("flag", "1");
                                                sendBroadcast(broadcastIntent);

                                            } else {
                                                mDialog.dismiss();
                                                System.out.println("==================cancel 1================");
                                                Intent returnIntent = new Intent();
                                                returnIntent.putExtra("Accepted_or_Not", "Cancelled");
                                                returnIntent.putExtra("Retry_Count", retry);
                                                setResult(RESULT_OK, returnIntent);
                                                finish();
                                                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                                mDialog.show();
                            } catch (Exception e) {
                                Alert(getkey("action_error"), response_value);
                            }

                        } else {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            Intent intent = new Intent(TimerPage.this, Navigation_new.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.exit, R.anim.enter);
//                            toast.setText(response_value);
//                            toast.show();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                Rl_cancelRide.setVisibility(View.VISIBLE);
                if (dialog != null) {
                    dialog.dismiss();
                }
//                cancel_progressbar.setVisibility(View.GONE);
            }

            @Override
            public void onErrorListener() {
                Rl_cancelRide.setVisibility(View.VISIBLE);
                if (dialog != null) {
                    dialog.dismiss();
                }
//                cancel_progressbar.setVisibility(View.GONE);
            }
        });
    }


    private void checkrideacceptornot(String Url) {




        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", userID);
        jsonParams.put("ride_id", rideID);

        mRequest = new ServiceRequest(TimerPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject response_object = object.getJSONObject("response");
                            if (response_object.length() > 0) {

                                Object check_driver_profile_object = response_object.get("driver_profile");
                                if (check_driver_profile_object instanceof JSONObject) {
                                    JSONObject driver_profile_object = response_object.getJSONObject("driver_profile");
                                    if (driver_profile_object.length() > 0) {

                                        System.out.println("==================Ride Requet================"+response);
                                        sessionManager.setCouponCode("", "");
                                        sessionManager.setReferralCode("", "");
                                        Intent i = new Intent(getApplicationContext(), TrackRideAcceptPage.class);
                                        i.putExtra("driverID", driver_profile_object.getString("driver_id"));
                                        i.putExtra("driverName", driver_profile_object.getString("driver_name"));
                                        i.putExtra("driverImage", driver_profile_object.getString("driver_image"));
                                        i.putExtra("driverRating", driver_profile_object.getString("driver_review"));
                                        i.putExtra("driverTime", driver_profile_object.getString("min_pickup_duration"));
                                        i.putExtra("rideID", rideID);
                                        i.putExtra("driverMobile", driver_profile_object.getString("phone_number"));
                                        i.putExtra("driverCar_no", driver_profile_object.getString("vehicle_number"));
                                        i.putExtra("driverCar_model", driver_profile_object.getString("vehicle_model"));
                                        i.putExtra("flag", "1");
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);


                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {

            }
        });
    }



    @Override
    public void onDestroy() {
        //timer.cancel();
        if(simpleVideoView!=null)
        {
            simpleVideoView.stopPlayback();
        }
        if (mdialog != null) {
            mdialog.dismiss();
        }
        if (mRequest != null) {
            mRequest.cancelRequest();
        }
        sessionManager.setTimerPage("");
        mHandler.removeCallbacks(mRunnable);
        unregisterReceiver(updateReciver);
        super.onDestroy();
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(TimerPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(TimerPage.this, Navigation_new.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);

            }
        });
        mDialog.show();

    }




    public void onResume() {
        super.onResume();



    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private final MediaPlayer.OnInfoListener onInfoToPlayStateListener = new MediaPlayer.OnInfoListener() {

        @Override
        public boolean onInfo(MediaPlayer mp, int videoReqCode, int extra) {
            if (MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START == videoReqCode) {
                spinKitView.setVisibility(View.GONE);
            }
            if (MediaPlayer.MEDIA_INFO_BUFFERING_START == videoReqCode) {
            //    spinKitView.setVisibility(View.VISIBLE);
            }
            if (MediaPlayer.MEDIA_INFO_BUFFERING_END == videoReqCode) {
                spinKitView.setVisibility(View.GONE);
            }
            return false;
        }
    };

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
