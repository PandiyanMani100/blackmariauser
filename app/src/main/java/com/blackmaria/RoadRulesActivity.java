package com.blackmaria;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.blackmaria.hockeyapp.ActivityHockeyApp;

/**
 * Created by user144 on 8/30/2017.
 */

public class RoadRulesActivity extends ActivityHockeyApp {
    private RelativeLayout RL_home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.road_rules_new);
        RL_home = (RelativeLayout) findViewById(R.id.RL_home);
        RL_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter,R.anim.exit);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter,R.anim.exit);
    }
}
