package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.ComplaintsPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomEdittextCambrialItalic;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 5/15/2017.
 */
public class CreateNew_Complaint extends ActivityHockeyApp {
    private RefreshReceiver refreshReceiver;
    private RelativeLayout ewalletHeaderLayout;
    private ImageView walletPageHomeImageview;
    LanguageDb mhelper;
    private RoundedImageView complaintPageUserImage;
    private TextView username;
    Spinner spinnerCustom;
    private TextView userLocation;
    private RelativeLayout complaintTextLyt;
    private RelativeLayout homeComplaintSubjectPinnerLayout;
    private Spinner genderSpinner;
    private TextView ratecardCityTextview;
    private RelativeLayout complaintTextviewLyt;
    private CustomEdittext tvComplaint;
    private RelativeLayout complaintSubmitLyt;
    private TextView resetId;
    private TextView submitId;
    private RelativeLayout complaintInsertyLyt;
    private RelativeLayout complaintDateTimeLayout;
    private TextView dateTv;
    private TextView timeTv;
    private TextView view_ride;
    private String user_name = "", user_image = "", location_name = "", today_datetime = "", rideId = "", valuesFrom = "";
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private SessionManager session;
    private String UserID = "";
    private ArrayList<String> complaint_itemList1;
    private ArrayList<String> complaint_itemList2;
    private int Complaint_request_code = 555;
    private CustomEdittextCambrialItalic insert_ride_id;
    private String options_id = "", options_name = "";
    private ArrayList<ComplaintsPojo> complaint_itemList;
    private Dialog success_dialog;


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CreateNew_Complaint.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CreateNew_Complaint.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            } else if (intent.getAction().equals("com.package.ACTION_CLASS_complaint_replay_notification")) {
                if (intent.getExtras() != null) {
                    String ticketId = (String) intent.getExtras().get("ticket_id");
                    String userID = (String) intent.getExtras().get("user_id");
                    String message = (String) intent.getExtras().get("message");
                    String dateTime = (String) intent.getExtras().get("date_time");

                    Intent intent1 = new Intent(CreateNew_Complaint.this, ComplaintPageViewDetailPage.class);
                    intent1.putExtra("ticket_id", ticketId);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_new_complaint_ticket_constrain);
        mhelper =  new LanguageDb(this);
        Initialize();
        complaintSubject();

        walletPageHomeImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(CreateNew_Complaint.this, Complaint_Page_new.class);
//                startActivity(intent);
                hideKeyboard(CreateNew_Complaint.this);
                onBackPressed();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        view_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateNew_Complaint.this, RideList.class);
                intent.putExtra("Class", "Complaint");
                startActivityForResult(intent, Complaint_request_code);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        resetId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insert_ride_id.setText("");
                tvComplaint.setText("");
                options_id= "";
                if(complaint_itemList1.size() > 0)
                {
                    spinnerCustom.setSelection(0);
                }
            }
        });
        submitId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (insert_ride_id.getText().toString().trim().length() == 0) {
                    Alert(getkey("action_error"), getkey("action_enter_crncomplaint"));
                } else if ((options_id.equals("")) && (options_id.equals(""))) {
                    Alert(getkey("action_error"), getkey("select_complaint_list"));
                } else if (tvComplaint.getText().toString().trim().length() == 0) {
                    Alert(getkey("action_error"), getkey("action_enter_complaint"));
                } else {
                    cd = new ConnectionDetector(CreateNew_Complaint.this);
                    isInternetPresent = cd.isConnectingToInternet();

                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", UserID);//"584aa27ccae2aa741a00002f");
                    jsonParams.put("ride_id", insert_ride_id.getText().toString().trim());
                    jsonParams.put("option_id", options_id);
                    jsonParams.put("option_name", options_name);
                    jsonParams.put("comments", tvComplaint.getText().toString());
                    System.out.println("-------------Complaint Submit jsonParams----------------" + jsonParams);
                    if (isInternetPresent) {
                        postRequestComplaintSubmitURL(Iconstant.complaint_submit_url, jsonParams);
                    } else {
                        Alert(getkey("action_error"), getkey("alert_nointernet"));
                    }
                }
            }
        });
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void Initialize() {

        cd = new ConnectionDetector(CreateNew_Complaint.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(CreateNew_Complaint.this);
         spinnerCustom = (Spinner) findViewById(R.id.complaintsubject);
        walletPageHomeImageview = (ImageView) findViewById(R.id.backbtn);
        complaintPageUserImage = (RoundedImageView) findViewById(R.id.complaint_page_user_image);
//        username = (TextView) findViewById(R.id.username);
//        userLocation = (TextView) findViewById(R.id.user_location);
//        complaintTextLyt = (RelativeLayout) findViewById(R.id.complaint_text_lyt);
        homeComplaintSubjectPinnerLayout = (RelativeLayout) findViewById(R.id.home_complaint_subject_pinner_layout);
        genderSpinner = (Spinner) findViewById(R.id.complaintsubject);
        ratecardCityTextview = (TextView) findViewById(R.id.ratecard_city_textview);
        ratecardCityTextview.setText(getkey("select_subject"));
        complaintTextviewLyt = (RelativeLayout) findViewById(R.id.complaint_textview_lyt);
        tvComplaint =  findViewById(R.id.tv_complaint);
        tvComplaint.setHint(getkey("messages_500_characters"));
        complaintSubmitLyt = (RelativeLayout) findViewById(R.id.complaint_submit_lyt);
        resetId = (TextView) findViewById(R.id.reset_id);
        resetId.setText(getkey("reset"));

       /* TextView ssub = (TextView) findViewById(R.id.ssub);
        ssub.setText(getkey("complaint_has_been_submitted"));
        TextView comre = (TextView) findViewById(R.id.comre);
        comre.setText(getkey("complaint_reports_messages"));
        TextView balinnc = (TextView) findViewById(R.id.balinnc);
        balinnc.setText(getkey("blackmaria_inc"));*/


        submitId = (TextView) findViewById(R.id.submit_id);
        submitId.setText(getkey("sendnow"));
        complaintInsertyLyt = (RelativeLayout) findViewById(R.id.complaint_inserty_lyt);



        TextView writecomplaint= (TextView) findViewById(R.id.writecomplaint);
        writecomplaint.setText(getkey("write_complaint"));

//        complaintDateTimeLayout = (RelativeLayout) findViewById(R.id.complaint_date_time_layout);
//        dateTv = (TextView) findViewById(R.id.date_tv);
//        timeTv = (TextView) findViewById(R.id.time_tv);
        view_ride =  findViewById(R.id.view_ride);
        view_ride.setText(getkey("lookup"));
        insert_ride_id =  findViewById(R.id.insert_ride_id);
        insert_ride_id.setHint(getkey("enter_booking_number"));
        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        Intent intent = getIntent();
        user_name = intent.getStringExtra("user_name");
        user_image = intent.getStringExtra("user_image");
        location_name = intent.getStringExtra("location_name");
        today_datetime = intent.getStringExtra("today_datetime");
        complaint_itemList1 = intent.getStringArrayListExtra("complaintsubjects");
        complaint_itemList2 = intent.getStringArrayListExtra("complaintoptionid");
        valuesFrom = intent.getStringExtra("valuesfrom");

        if (valuesFrom.equalsIgnoreCase("RideDetailPage")) {
            rideId = intent.getStringExtra("ride_id");
            insert_ride_id.setText(rideId);
        }


//        username.setText(user_name);
//        userLocation.setText(location_name);
//        dateTv.setText(today_datetime);
        Picasso.with(CreateNew_Complaint.this)
                .load(user_image)
                .placeholder(R.drawable.no_user_img)   // optional
                .error(R.drawable.no_user_img)      // optional
                .into(complaintPageUserImage);
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new CreateNew_Complaint.RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);


    }

    private void complaintSubject() {


        com.blackmaria.adapter.CustomSpinnerAdapter customSpinnerAdapter = new com.blackmaria.adapter.CustomSpinnerAdapter(CreateNew_Complaint.this, complaint_itemList1);
        spinnerCustom.setAdapter(customSpinnerAdapter);

        spinnerCustom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(ratecardCityTextview.getWindowToken(), 0);
                return false;
            }
        });
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println(" complaint subject spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                ratecardCityTextview.setText(item.toString());
                cd = new ConnectionDetector(CreateNew_Complaint.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (!item.equalsIgnoreCase("Select Subject")) {
                    if (isInternetPresent) {
                        int subPostion = position;
                        options_id = complaint_itemList2.get(subPostion);
                        options_name = item;
                        System.out.println("-------------Sselected_subject-----------------" + item + options_id);
                    } else {
                        Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                    }
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ratecardCityTextview.setText("Select Subject");
            }
        });

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(CreateNew_Complaint.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<String> asr;

        public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
            this.asr = asr;
            activity = context;
        }


        public int getCount() {
            return asr.size();
        }

        public Object getItem(int i) {
            return asr.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(CreateNew_Complaint.this);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(16);
            txt.setGravity(Gravity.CENTER);
            txt.setText(asr.get(position));
            txt.setBackground(getResources().getDrawable(R.drawable.spinner_drop_down_background));
            txt.setTextColor(Color.parseColor("#ffffff"));
            return txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(CreateNew_Complaint.this);
            txt.setGravity(Gravity.CENTER);
            txt.setPadding(10, 0, 0, 0);
            txt.setTextSize(14);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt.setText(asr.get(i));
            txt.setTextColor(Color.parseColor("#ffffff"));
            txt.setVisibility(View.INVISIBLE);
            return txt;
        }

    }

    private void postRequestComplaintSubmitURL(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(CreateNew_Complaint.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        mRequest = new ServiceRequest(CreateNew_Complaint.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Complaint Submit Page Response----------------" + response);

                String Sstatus = "", ticket_no = "", Scurrency_code = "", user_image = "", Scurrentbalance = "", Smessage = "", Scurrency = "", user_name = "", user_location = "", user_id = "", member_since = "", Date = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject jobject = object.getJSONObject("response");
                        Smessage = jobject.getString("message");
                        ticket_no = jobject.getString("ticket_no");

                        successDialog(ticket_no, tvComplaint.getText().toString());
                        dialogDismiss();
                    } else {
                        Smessage = object.getString("response");
                        Alert(getkey("action_error"), Smessage);
                        dialogDismiss();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogDismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void sendBroadcast() {
        Intent local = new Intent();
        local.setAction("com.app.MilesPagePage.refreshhomePage");
        local.putExtra("refresh", "yes");
        sendBroadcast(local);
    }

    private void successDialog(String ticket_no, String smessage) {

        success_dialog = new Dialog(CreateNew_Complaint.this, R.style.DialogSlideAnim3);
        success_dialog.getWindow();
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        success_dialog.setContentView(R.layout.complaint_submit_popup);
        success_dialog.setCanceledOnTouchOutside(true);
//        success_dialog.getWindow().getAttributes().windowAnimations =R.style.Animations_categories_filter;
        success_dialog.show();
        success_dialog.getWindow().setGravity(Gravity.CENTER);

        TextView Tv_ticketNo = (TextView) success_dialog.findViewById(R.id.ticketnumber);
        TextView Tv_message = (TextView) success_dialog.findViewById(R.id.complaint_message);
        TextView subject = (TextView) success_dialog.findViewById(R.id.subject);
        TextView bookingnumber = (TextView) success_dialog.findViewById(R.id.bookingnumber);
        TextView today_datetime1 = (TextView) success_dialog.findViewById(R.id.dateandtime);
        ImageView img_close = (ImageView) success_dialog.findViewById(R.id.image_close);

        Tv_ticketNo.setText(getkey("ticket_no_lable")+" : " + ticket_no);
        Tv_message.setText(smessage);
        subject.setText(getkey("subjects") + options_name);
        bookingnumber.setText(getkey("book_no")+ insert_ride_id.getText().toString().trim());
        today_datetime1.setText(getkey("open_daate")+" " + today_datetime);

        TextView ssub = (TextView) success_dialog.findViewById(R.id.ssub);
        ssub.setText(getkey("complaint_has_been_submitted"));
        TextView comre = (TextView) success_dialog.findViewById(R.id.comre);
        comre.setText(getkey("complaint_reports_messages"));
        TextView balinnc = (TextView) success_dialog.findViewById(R.id.balinnc);
        balinnc.setText(getkey("blackmaria_inc"));

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBroadcast();
//                Intent intent = new Intent(CreateNew_Complaint.this, Complaint_Page_new.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((requestCode == Complaint_request_code && resultCode == Activity.RESULT_OK && data != null)) {
            String ride_id = data.getStringExtra("ride_id");
            insert_ride_id.setText(ride_id);

        }


        super.onActivityResult(requestCode, resultCode, data);
    }

   /* @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
