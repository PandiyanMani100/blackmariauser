package com.blackmaria.iconstant;

public interface Iconstant {

    //Local Url
   /* String Url = "http://192.168.1.244/cabily/customization/blackmaria/";
    String BaseUrl = "http://192.168.1.244/cabily/customization/blackmaria/v11/";*/

    // Staging Url
    String Url = "https://app.blackmaria.co/";
    String BaseUrl = "https://app.blackmaria.co/v11/";

   /* String Url = "https://blackmaria.casperon.co/";
    String BaseUrl = "https://blackmaria.casperon.co/v11/";*/

    String getlocationlist = BaseUrl + "api/v6/app/driver/get-location-list";
    String Verifymobile = BaseUrl + "api/v6/app/verify/mobile";
    String Verifyemail = BaseUrl + "api/v6/app/verify/email";

    String getcompanynames = BaseUrl + "api/v6/app/get-company-list";
    // Live Url
//    String Url = "https://www.blackmaria.co/";
//    String BaseUrl = "https://www.blackmaria.co/v10/";
    String termsurl = "https://global.blackmaria.co/term-of-services";
    String privacyurl = "https://global.blackmaria.co/privacy-statements";
    String facebookurl = "https://global.blackmaria.co/facebook-policy";
    String enduserurl = "https://global.blackmaria.co/end-user";
    String getAppInfo = BaseUrl + "api/v6/get-app-info";

    String getlanguaage = BaseUrl + "api/v6/language/get";


    String faqlist = BaseUrl + "api/v6/faq/list";
    String closeaccountpaymentslist = BaseUrl + "v6/api/v1/app/account-close-payment";
    String closeaccount_request = BaseUrl + "v6/api/v1/app/account-close-request";
    String setUserLocation = BaseUrl + "api/v6/app/set-user-geo";
    String refered_friendslist = BaseUrl + "api/v6/user/friend/list";
    String single_friendsview = BaseUrl + "api/v6/user/friend/trips";
    String fastpayhome = BaseUrl + "api/v6/app/wallet/summery";
    String profilesafe = BaseUrl + "api/v6/app/business/profile";
    String businessprofile = BaseUrl + "api/v6/app/business/profile";
    String sendnotification = BaseUrl + "api/v6/app/chat/push-chat-message";
    String fastpay_findfriend = BaseUrl + "api/v6/app/fastpay/find-friend";
    String fastpay_amounttransfer = BaseUrl + "api_v6/user/fastpay_amount_transfer";
    String loginurl = BaseUrl + "v6/api/v1/app/check-user";
    String waitingtime_api = BaseUrl + "app/driver/info";
    String uploadimageforchat = BaseUrl + "app/upload/chat/image";
    String loginurl_new = BaseUrl + "v6/api/v1/app/user/login";
    String register_otp_url = BaseUrl + "v6/api/v1/app/register";
    String register_otp_resend_url = BaseUrl + "v6/api/v1/app/resend-otp";
    String get_profile_detail_url = BaseUrl + "v6/api/v1/app/get-profile";
    String Edit_profile_image_url = BaseUrl + "v6/api/v1/app/upload-profilepic";
    String Update_profile_image_url = BaseUrl + "v6/api/v1/app/update/image";
    String profile_sos_remove_url = BaseUrl + "v6/api/v1/app/emergency-delete";
    String profile_sos_add_url = BaseUrl + "v6/api/v1/app/emergency-add";
    String Edit_profile_url = BaseUrl + "v6/api/v1/app/edit-profile";
    String Edit_profile_mobileNo_url = BaseUrl + "v6/api/v1/app/mobilenumber-change";
    String BookMyRide_url = BaseUrl + "api/v6/app/get-map-view";
    String delete_ride_url = BaseUrl + "api/v6/app/delete-ride";
    String help_profile = BaseUrl + "v6/api/v1/app/help-profile";
    String mileage_dashboard = BaseUrl + "api/v6/app/mileage-dashboard";
    String recent_rides = BaseUrl + "v6/api/v1/app/recent/rides";
    String forgetpin = BaseUrl + "v6/api/v1/app/forgot/pin";
    String updatepattern = BaseUrl + "api/v6/app/update-pattern";
    String getpattern = BaseUrl + "api/v6/app/get-pattern";
    String updatepincode = BaseUrl + "v6/api/v1/app/change/pin";
    String getvehicledetails = BaseUrl + "api/v6/app/get-vehicle-details";

    String place_search_url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=geocode||establishment&key=AIzaSyD_lpM4Zq2VVNf5nj_2_TK8xqThyfmqrUg&input=";
    String place_search_url1 = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=geocode&key=AIzaSyD_lpM4Zq2VVNf5nj_2_TK8xqThyfmqrUg&input=";
    String GetAddressFrom_LatLong_url = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyD_lpM4Zq2VVNf5nj_2_TK8xqThyfmqrUg&placeid=";
    String Routekey = "AIzaSyD_lpM4Zq2VVNf5nj_2_TK8xqThyfmqrUg";


    String get_coupon_list_url = BaseUrl + "api/v6/app/user/coupon-list";
    String add_coupon_code_url = BaseUrl + "api/v6/app/user/addcoupon-code";
    String apply_coupon_code_url = BaseUrl + "api/v6/app/user/apply-coupon";
    String add_referral_code_url = BaseUrl + "api/v6/app/user/referral-code";

    String confirm_ride_url = BaseUrl + "api/v6/app/book-ride";
    String estimate_price_url = BaseUrl + "api/v6/app/get-eta";
    String get_ratecard_url = BaseUrl + "api/v6/app/get-ratecard";
    String get_homepage_url = BaseUrl + "api/v6/app/user-dashboard";
    String updatelanguagee = BaseUrl + "api/v6/language/update";

    String retryurl = BaseUrl + "api/v6/app/rebook-ride";

    String wallet_money_url = BaseUrl + "api/v6/app/get-money-page";

    String wallet_money_transaction_url = BaseUrl + "api/v6/app/get-trans-list";
    String wallet_money_stripe_webview_url = BaseUrl + "v6/mobile/wallet-recharge/payform?user_id=";
    String wallet_money_paypal_webview_url = BaseUrl + "v6/mobile/wallet-recharge/paypal-process?user_id=";
    String wallet_add_money_url = BaseUrl + "v6/mobile/wallet-recharge/stripe-process";
    String myrides_url = BaseUrl + "api/v6/app/my-rides";
    String myride_details_url = BaseUrl + "api/v6/app/view-ride";
    String mileage_url = BaseUrl + "api/v6/app/mileage-list";
    String mileage_reward_url = BaseUrl + "api/v6/app/mileage-reward";
    String mileage_reward_success_url = BaseUrl + "api/v6/app/redeem-reward";
    String track_your_ride_url = BaseUrl + "api/v6/track-driver";
    String sos_alert_url = BaseUrl + "api/v6/app/user/alert-emergency-contact";
    String cancel_myride_reason_url = BaseUrl + "api/v6/app/cancellation-reason";
    String cancel_myride_url = BaseUrl + "api/v6/app/cancel-ride";
    String logout_url = BaseUrl + "api/v6/app/logout";
    String getfareBreakUpURL = BaseUrl + "v6/app/get-fare-breakup";
    String driverProfilePage_url = BaseUrl + "v6/api/v1/app/driver-profile";
    String favoritelist_display_url = BaseUrl + "api/v6/app/favourite/display";
    String favoritelist_add_url = BaseUrl + "api/v6/app/favourite/add";
    String favoritelist_edit_url = BaseUrl + "api/v6/app/favourite/edit";
    String favoritelist_delete_url = BaseUrl + "api/v6/app/favourite/remove";
    String dynamic_page_url = BaseUrl + "api/v6/app/get-page-details";
    String invite_earn_friends_url = BaseUrl + "api/v6/app/get-invites";
    String app_availability_url = BaseUrl + "api/v6/xmpp-status";
    String Earnings_list_url = BaseUrl + "v6/api/v1/app/earning-list";
    String Earnings_withdraw_url = BaseUrl + "v6/api/v1/app/withdraw-amount";
    String saveplustransfer = BaseUrl + "api/v6/app/amount/transfer";
    String saveplustransfer_list = BaseUrl + "api/v6/app/get-trans-saveplus-list";
    String findaccount = BaseUrl + "api/v6/app/find/account";
    String saveplus_withdraw = BaseUrl + "api/v6/app/saveplus/withdraw";
    String check_refrrelcode = BaseUrl + "v6/api/v1/app/check-referral-code";


    String complaint_list_url = BaseUrl + "api/v6/app/support-list";

    String complaint_list = BaseUrl + "api/v6/app/ticket-list";

    String complaint_submit_url = BaseUrl + "api/v6/app/submit-ticket";
    String view_ticket_url = BaseUrl + "api/v6/app/view-ticket";
    String post_comment_url = BaseUrl + "api/v6/app/reply-comment";
    String close_ticket_url = BaseUrl + "api/v6/app/close-ticket";
    String ratecard_select_city_url = BaseUrl + "v6/api/v1/app/get-location";
    String ratecard_select_cartype_url = BaseUrl + "v6/api/v1/app/get-category";
    String ratecard_display_url = BaseUrl + "api/v6/app/get-ratecard-page";
    String share_trip_url = BaseUrl + "api/v6/track-driver/share-my-ride";
    String myride_rating_url = BaseUrl + "api/v6/app/review/options-list";
    String myride_rating_submit_url = BaseUrl + "api/v6/app/review/submit";
    String get_cabs_url = BaseUrl + "v6/api/v1/app/get-cabs";
    String get_arrival_url = BaseUrl + "v6/api/v1/app/get-arrival";
    String phoneMasking_url = BaseUrl + "api/v6/app/phone-call";
    String get_basic_profile_url = BaseUrl + "v6/api/v1/app/basic-profile";
    /*new profile*/
/*    String userprofile_update_user_info_url = BaseUrl + "v6/api/v1/app/user-profile";
    String userprofile_update_login_info_url = BaseUrl + "v6/api/v1/app/user-login";
    String userprofile_update_sos_info_url = BaseUrl + "v6/api/v1/app/user-sos-contact";*/
    String driver_signup_web_url = Url + "driver/signup";
    String DefaultCardDetails = BaseUrl + "api/v6/app/default-card";
    String myride_details_inVoiceEmail_url = BaseUrl + "api/v1/app/mail-invoice";
    String paymentList_url = BaseUrl + "api/v1/app/payment-list";
    String makepayment_cash_url = BaseUrl + "api/v1/app/payment/by-cash";
    String makepayment_wallet_url = BaseUrl + "api/v1/app/payment/by-wallet";
    String makepayment_autoDetect_url = BaseUrl + "api/v1/app/payment/by-auto-detect";
    String makepayment_Get_webview_mobileId_url = BaseUrl + "api/v1/app/payment/by-gateway";
    String makepayment_webview_url = BaseUrl + "api/v1/mobile/proceed-payment?mobileId=";


    String myride_DashBoard = BaseUrl + "api/v6/user/mytrip-dashboard";
    String mesggaeg_url = BaseUrl + "api/v6/app/message-list";
    /*cloud money*/
    String referal_url = BaseUrl + "api/v6/app/user/referral-code";
    String referal_code_submit_url = BaseUrl + "api/v6/app/user/submit-code";

    String earningdshboard_url = BaseUrl + "v6/api/v1/app/earning-dashboard";
    String cloudmoney_withdrawl_url = BaseUrl + "v6/api/v1/app/earning-withdraw";
    String cloudmoney_withdrawl_anount = BaseUrl + "v6/api/v1/app/withdraw-amount";
    String cashback_home = BaseUrl + "api/v6/app/cashback";
    String cashback_Apply_url = BaseUrl + "api/v6/app/apply-cashback";
    String dashboardimage_url = BaseUrl + "v6/api/v1/app/upload-dashboardimage";
    //   -------------mailes url----------------------
    String miles_home_url = BaseUrl + "api/v6/app/mileage-dashboard";
    String miles_reward_list_url = BaseUrl + "api/v6/app/mileage-reward";
    String miles_rewards_apply_url = BaseUrl + "api/v6/app/redeem-reward";
    String miles_rewards_apply_milageurl = BaseUrl + "api/v6/app/apply-mileage";
    String complementry_waiting = BaseUrl + "v6/api/v1/app/complementary-time";
    String creditcard_stripe = BaseUrl + "api/v6/app/create_stripe_customer";
    String DeleteCardDetails = BaseUrl + "api/v6/app/delete-cards";
    String Privacy_policy = BaseUrl + "v6/api/v1/app/update-privacy";
    String Dynamic_content_Url = BaseUrl + "api/v6/app/get-page-details";
    String Ewallet_withdrawal_url = BaseUrl + "api/v6/app/user/wallet-withdraw";
    String Ewallet_withdrawal_confirm_url = BaseUrl + "api/v6/app/user/wallet-withdraw-amount";
    /*new profile page*/
    String CloseMyAccount_Url = BaseUrl + "v6/api/v1/app/account-close";
    String userprofile_update_login_info_url = BaseUrl + "v6/api/v1/app/user-login";
    String userprofile_update_sos_info_url = BaseUrl + "v6/api/v1/app/user-sos-contact";
    String userprofile_update_user_info_url = BaseUrl + "v6/api/v1/app/user-profile";

    String card_list_url = BaseUrl + "api/v6/app/list-cards";
    //message page update
    String message_update_list = BaseUrl + "api/v6/app/update-message";
    String message_delete = BaseUrl + "api/v6/app/delete-message";
    String deactivate_account_statement_url = BaseUrl + "v6/api/v1/app/account-statement";
    String menupage_ratinglist_home = BaseUrl + "api/v6/app/review/user-review";
    String menupage_ratinglist = BaseUrl + "api/v6/app/review/review-list";
    String forgot_pin_url = BaseUrl + "v6/api/v1/app/forgot-pin";
    String getPhantomCar_url = BaseUrl + "api/v6/app/get-phantom-car";
    String bookhotel_url = "https://www.booking.com/?aid=1153818";
    String flightBooking_url = "http://flights.blackmaria.co";

    //New Cloudmoney Page Urls

    String getXendit_bank_save_url = BaseUrl + "api/v6/app/save-bank-xendit";
    String getXendit_bank_saved_detail = BaseUrl + "api/v6/app/get-bank-xendit";

    //New Cloudmoney reload Page Urls
    String new_cloudmoney_reload_dashboard_url = BaseUrl + "api/v6/app/get-money-page";
    String Xendit_Card_Url = BaseUrl + "api/v6/app/xendit-card-payment";
    String Xendit_Bank_Url = BaseUrl + "api/v6/app/xendit-bank-payment";

    String find_Friend_Account_transfer_url = BaseUrl + "api/v6/app/wallet-transfer";
    String redeem_code_url = BaseUrl + "api/v6/app/apply-redeem-code";
    String XenditCard_Ride_Payment = BaseUrl + "api/v6/app/xendit-card-ridepayment";
    String XenditCard_TOKEN_UPDATE = BaseUrl + "api/v6/app/xendit-token-update";
    String cloudmoney_crossbordervalue_Url = BaseUrl + "api/v6/app/cross-border";
    String redeem_code_url1 = BaseUrl + "api/v6/app/free-redeem-code";
    /*DRIVER REGISTRATION URLs*/
    String DriverReg_Locationwise_Carcategory_URL = BaseUrl + "api/v6/app/get-available-category";
    String DriverReg_upload_Image_URl = BaseUrl + "api/v6/app/upload-image";
    String DriverReg_applyReferal_URl = BaseUrl + "api/v6/app/apply-referral-code";
    String DriverReg_getCompanyList_URl = BaseUrl + "api/v6/app/get-company-list";
    String DriverReg_getCountryList_URl = BaseUrl + "api/v6/app/get-country-list";
    String DriverReg_UploadVehicleImage_URl = BaseUrl + "api/v6/app/upload-image-vehicle";
    String DriverReg_VehicleList_URl = BaseUrl + "api/v6/app/get-vehicle-list";
    String DriverReg_MarkerList_URl = BaseUrl + "api/v6/app/get-maker-list";
    String DriverReg_ModelList_URl = BaseUrl + "api/v6/app/get-model-list";
    String DriverReg_YearList_URl = BaseUrl + "api/v6/app/get-year-list";
    String DriverRegistration_URl = BaseUrl + "api/v6/app/register";
    String DriverRegistration_VerifyMobile_URl = BaseUrl + "api/v6/app/send-otp-driver";

    // localdb
    String GetRideDetail = BaseUrl + "app/ride/view";
    String GetRideDetail_view = BaseUrl + "app/view-ride";

    /*book ride payment selectList urls*/

    String User_Booking_PaymentChoose_Url = BaseUrl + "api/v6/app/payment-method";
    String User_Activiate_Referal = BaseUrl + "v6/api/v1/app/update-referral";
    String User_Otp_LoginUpdate = BaseUrl + "v6/api/v1/app/contact-update-otp";
    String User_Email_LoginUpdate = BaseUrl + "v6/api/v1/app/change/email";

    String UserEndRide = BaseUrl + "api/v6/app/end-ride";

    /*find nearest place */
    String app_facebook_post_url = BaseUrl + "";
    String register_url = BaseUrl + "v6/api/v1/app/check-user";
    String facebook_register_url = BaseUrl + "api/v1/app/social-login";
    String social_check_url = BaseUrl + "api/v1/app/social-check";
    String forgot_password_url = BaseUrl + "api/v1/app/user/reset-password";
    String reset_password_url = BaseUrl + "api/v1/app/user/update-reset-password";
    String couponCode_apply_url = BaseUrl + "api/v1/app/apply-coupon";
    String changePassword_url = BaseUrl + "api/v1/app/user/change-password";
    String profile_edit_userName_url = BaseUrl + "api/v1/app/user/change-name";
    String profile_edit_mobileNo_url = BaseUrl + "api/v1/app/user/change-mobile";
    String emergencycontact_add_url = BaseUrl + "api/v1/app/user/set-emergency-contact";
    String emergencycontact_view_url = BaseUrl + "api/v1/app/user/view-emergency-contact";
    String emergencycontact_delete_url = BaseUrl + "api/v1/app/user/delete-emergency-contact";
    String emergencycontact_send_message_url = BaseUrl + "api/v1/app/user/alert-emergency-contact";
    String myride_details_track_your_ride_url = BaseUrl + "api/v3/track-driver";
    String tip_add_url = BaseUrl + "api/v1/app/apply-tips";
    String tip_remove_url = BaseUrl + "api/v1/app/remove-tips";
    String phone_masking_url = BaseUrl + "api/v4/app/phone-call";
    String message_masking_url = BaseUrl + "api/v4/app/send-text";
    String create_stripe_customer = BaseUrl + "app/connect-card";//http://192.168.1.251:8081/dectar/customization/scoopmerides/v7/app/connect-card
    String Strip_check = BaseUrl + "app/get-card-status";// http://192.168.1.251:8081/dectar/customization/scoopmerides/v7/app/get-card-status
    String ride_cancel_url = BaseUrl + "api/v6/driver/check-cancel-ride";
    //    http://192.168.1.251:8081/dectar/customization/scoopmerides/v9/api/v6/driver/check-cancel-ride
    String Makepayment_url = BaseUrl + "api/v6/auto_charge/pay-ride-amount";
    //    http://192.168.1.251:8081/dectar/customization/scoopmerides/v9/api/v6/auto_charge/pay-ride-amount
    String updateAppStatus_url = BaseUrl + "api/v5/app/app-status";

    String rechargecheck_url = BaseUrl + "v6/mobile/check/recharge";


    //----------------------UserAgent---------------------
    String cabily_userAgent = "cabily2k15android";
    String cabily_IsApplication = "1";
    String cabily_AppLanguage = "en";
    String cabily_AppType = "android";
    String cabily_AppToken = "";

    /*HELP AND TERMS AND CONDITION PAGE URLs*/
    String about_url = "http://blackmaria.co";//Url + "pages/about-us";
    String booking_url = "https://blackmaria.co/global//faq-booking/";//Url + "pages/booking";
    String earning_url = "https://blackmaria.co/global/earnings-commmision/";//Url + "pages/earningcommision";
    String bidding_url = Url + "pages/bidding";
    String miles_url = Url + "pages/miles";
    String referal_urls = Url + "pages/referral";
    String ewallet_url = Url + "pages/e-wallet";
    String sos_url = Url + "pages/sos";
    String account_url = Url + "pages/account";
    String help_url = Url + "pages/help";

    String termsandcomndition_url = Url + "pages/terms-and-conditions";
    String userpolicy_url = Url + "pages/userpolicy";
    String biddfare_url = Url + "pages/bidfare-terms";
    String loyaltypgm_url = Url + "pages/loyalty-program";
    String referealearn_url = Url + "pages/ride-earn-referral";
    String termsnadconditon_url = Url + "pages/sos-terms-conditions";
    String privacy_and_policy_url = Url + "pages/privacy-and-policy";


    //-----------------PushNotification Key--------------------
    String PushNotification_AcceptRide_Key = "ride_confirmed";
    String PushNotification_CabArrived_Key = "cab_arrived";
    String ACTION_TAG_CHATMESSAGE = "blackmariachat";
    String PushNotification_RideCancelled_Key = "ride_cancelled";
    String PushNotification_RideCompleted_Key = "ride_completed";
    String PushNotification_RequestPayment_Key = "requesting_payment";
    String PushNotification_RequestPayment_makepayment_Stripe_Key = "make_payment";
    String PushNotification_PaymentPaid_Key = "payment_paid";
    String pushNotificationBeginTrip = "trip_begin";
    String pushNotificationBeginTripReturn = "return";
    String pushNotificationMultistop = "multistop";
    String pushNotificationDriverLoc = "driver_loc";
    String PushNotification_declined_Key = "driver_declined";
    String PushNotification_ride_later_confirmed = "ride_later_confirmed";
    String PushNotification_ride_completed_thankyou = "thank_you";
    String complementry_time_closed = "comp_closed";
    String referalcredit_amount = "referral_credit";


    String drop_user_key = "drop_user";


    String Drop_Waiting_time_closed_key = "close_waiting_time";
    String wallet_success = "wallet_success";
    String bank_success = "bank_success";
    String rating_submitted = "rating_submitted";
    String wallet_credit = "wallet_credit";
    String user_wallet_credited = "user_wallet_credited";
    String referral_info = "referral_info";
    String coupon_info = "coupon_info";
    String complaint_reply = "complaint_reply";
    String complaint_closed = "complaint_closed";
    String withdrawal_success = "withdrawal_success";

    /*Ride Accept Key*/
    String DriverID = "key1";
    String DriverName = "key2";
    String DriverEmail = "key3";
    String DriverImage = "key4";
    String DriverRating = "key5";
    String DriverLat = "key6";
    String DriverLong = "key7";
    String DriverTime = "key8";
    String RideID = "key9";
    String DriverMobile = "key10";
    String DriverCar_No = "key11";
    String DriverCar_Model = "key12";
    String UserLat = "key14";
    String UserLong = "key15";
    String VechileImage = "key16";

    String Push_Message = "message";
    String Push_Action = "action";
    String rideID = "rideID";
    String latitude = "latitude";
    String longitude = "longitude";
    String Bearing = "bearing";
    String ride_id = "ride_id";
    String isContinousRide = "isContinousRide";
    /*Ride Arrived Key*/
    String UserID_Arrived = "key1";
    String RideID_Arrived = "key2";
    String Push_Message_Arrived = "message";
    String Push_Action_Arrived = "action";

    /*Ride Cancelled Key*/
    String RideID_Cancelled = "key1";
    String free_waitingtime = "key2";
    String drop_waitingtime = "key2";
    String Ride_type = "key3";
    String Push_Message_Cancelled = "message";
    String Push_Action_Cancelled = "action";

    /*Ride Completed Key*/
    String Push_Message_Completed = "message";
    String Push_Action_Completed = "action";
    /*Ride Complementrytime closed Key*/
    String Push_Complementrytime_Completed = "message";
    String Push_ComplementrytimeAction_Completed = "action";
    /*Request Payment Key*/
    String Push_Message_Request_Payment = "message";
    String Push_Action_Request_Payment = "action";
    String CurrencyCode_Request_Payment = "key1";
    String TotalAmount_Request_Payment = "key2";
    String TravelDistance_Request_Payment = "key3";
    String Duration_Request_Payment = "key4";
    String WaitingTime_Request_Payment = "key5";
    String RideID_Request_Payment = "key6";
    String UserID_Request_Payment = "key7";
    String TipStatus_Request_Payment = "key8";
    String TipAmount_Request_Payment = "key9";
    String DriverName_Request_Payment = "key10";
    String DriverImage_Request_Payment = "key11";
    String DriverRating_Request_Payment = "key12";
    String Driver_Latitude_Request_Payment = "key13";
    String Driver_Longitude_Request_Payment = "key14";
    String UserName_Request_Payment = "key15";
    String User_Latitude_Request_Payment = "key16";
    String User_Longitude_Request_Payment = "key17";
    String subTotal_Request_Payment = "key18";
    String coupon_Request_Payment = "key19";
    String serviceTax_Request_Payment = "key20";
    String Total_Request_Payment = "key21";
    String BaseFare = "key22";

    String Payment_type = "key23";
    String xendit_public_key = "key24";
    String currencyconverionValue = "key25";


    String Make_Payment = "key1";
    String drop_lat = "key3";
    String drop_lan = "key4";


    /*Payment Paid Key*/
    String Push_Message_Payment_paid = "message";
    String Push_Action_Payment_paid = "action";
    String RideID_Payment_paid = "key1";
    String UserID_Payment_paid = "key2";


    /*Ads Key*/
    String pushNotification_Ads = "ads";
    String Ads_title = "key1";
    String Ads_Message = "key2";
    String Ads_image = "key3";

    /*Ads Key*/
    String complaint_replay_notification = "reply_notification";
    String ticketId = "key1";
    String userId = "key2";
    String replay_message = "key3";
    String dateTime = "key4";


    /*Declined Key*/
    String push_Message_Declined = "message";
    String Push_Action_Declined = "action";
    String UserID_Declined = "key1";
    String RideID_Declined = "key2";
    String category_Declined = "key3";
    String couponcode_Declined = "key5";
    String pickupaddress_Declined = "key6";
    String pickupLat_Declined = "key7";
    String pickupLon_Declined = "key8";
    String pickupdate_Declined = "key9";
    String pickuptime_Declined = "key10";
    String dropaddress_Declined = "key11";
    String dropLat_Declined = "key12";
    String dropLon_Declined = "key13";

    //not works
    String xendit_bank_pay_url = BaseUrl + "api/v6/app/xendit-bank-payment";
    String xendit_card_pay_url = BaseUrl + "api/v6/app/xendit-card-payment";
    String driver_wallet_to_wallet_transfer_url = BaseUrl + "api/v6/app/provider/amount-transfer";
    String find_Friend_Account_url = BaseUrl + "api/v6/app/find-friend";
    String wallet_withdrawal_dashboard_url = BaseUrl + "api/v6/app/user/wallet-withdraw";
    String getPaymentOPtion_url_new = BaseUrl + "api/v6/app/get-bank-info";
    String wallet_withdraw_amount_url = BaseUrl + "api/v6/app/user/wallet-withdraw-amount";
    String paypalsuccessapi = BaseUrl + "wallet-recharge-user/payment/details";
}
