package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.MessageListviewAdapter1;
import com.blackmaria.newdesigns.Notification_subpage;
import com.blackmaria.pojo.MessagePojo;
import com.blackmaria.pojo.adsupdatepojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class InfoPage extends AppCompatActivity implements View.OnClickListener, MessageListviewAdapter1.MessageView {

    private ImageView Iv_back;

    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;
    private Boolean isInternetPresent = false;
    LanguageDb mhelper;
    String perPage = "10";
    String mode = "";
    private int currentPage = 1;
    private String UserId = "";
    private boolean isDataPresent = false;
    ArrayList<MessagePojo> itemlist_all;
    ArrayList<MessagePojo> itemlist_all_copy;
    String[] mStrings;
    private boolean isTransactionAvailable = false;
    MessageListviewAdapter1 adapter;
    ImageView Iv_previous, Iv_next;
    int statusselect = 0;
    private ListView mMessageList;
    TextView empty_text, thisweek, thismonth, thisyear, selectall, unreadcount, delete;
    private String notificationId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information_page_constrain);
        EventBus.getDefault().register(this);
        mhelper=new LanguageDb(this);

        initialize();
        Iv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Iv_previous.setVisibility(View.VISIBLE);
                currentPage = currentPage + 1;
                postData();
            }
        });

        Iv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage - 1;
                postData();

            }
        });

        thisweek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mode = "week";
                postData();
            }
        });

        thismonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mode = "month";
                postData();
            }
        });

        thisyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mode = "year";
                postData();
            }
        });

        selectall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(statusselect == 0)
                {
                    selectall.setText(getkey("unselectall"));
                    statusselect = 1;
                    itemlist_all_copy = new ArrayList<>();
                    itemlist_all_copy.addAll(itemlist_all);
                    itemlist_all.clear();
                    for (int i = 0; i <= itemlist_all_copy.size() - 1; i++) {
                        MessagePojo pojo = new MessagePojo();
                        pojo.setDate(itemlist_all_copy.get(i).getDate());
                        pojo.setIMage(itemlist_all_copy.get(i).getIMage());
                        pojo.setTitle(itemlist_all_copy.get(i).getTitle());
                        pojo.setDescription(itemlist_all_copy.get(i).getDescription());
                        pojo.setNotification_id(itemlist_all_copy.get(i).getNotification_id());
                        pojo.setIschecked(true);
                        itemlist_all.add(pojo);
                    }
                    adapter = new MessageListviewAdapter1(InfoPage.this, 1, itemlist_all, InfoPage.this);
                    mMessageList.setAdapter(adapter);
                }
                else
                {
                    selectall.setText(getkey("select_all"));
                    statusselect = 0;
                    itemlist_all_copy = new ArrayList<>();
                    itemlist_all_copy.addAll(itemlist_all);
                    itemlist_all.clear();
                    for (int i = 0; i <= itemlist_all_copy.size() - 1; i++) {
                        MessagePojo pojo = new MessagePojo();
                        pojo.setDate(itemlist_all_copy.get(i).getDate());
                        pojo.setIMage(itemlist_all_copy.get(i).getIMage());
                        pojo.setTitle(itemlist_all_copy.get(i).getTitle());
                        pojo.setDescription(itemlist_all_copy.get(i).getDescription());
                        pojo.setNotification_id(itemlist_all_copy.get(i).getNotification_id());
                        pojo.setIschecked(false);
                        itemlist_all.add(pojo);
                    }
                    adapter = new MessageListviewAdapter1(InfoPage.this, 1, itemlist_all, InfoPage.this);
                    mMessageList.setAdapter(adapter);
                }

            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemlist_all.size() > 0) {
                    boolean isselected = false;
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("id", UserId);
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i <= itemlist_all.size() - 1; i++) {
                        if (itemlist_all.get(i).isIschecked()) {
                            strings.add(itemlist_all.get(i).getNotification_id());
                            isselected = true;
                        }
                    }
                    for (int j = 0; j <= strings.size() - 1; j++) {
                        jsonParams.put("notification_id[" + j + "]", strings.get(j));
                    }

                    if (isselected) {
                        if (isInternetPresent) {
                            postRequest_MessageUpdate(Iconstant.message_update_list, jsonParams);
                        } else {
                            Alert(getkey("action_error"), getkey("alert_nointernet"));
                        }
                    } else {
                        Alert(getkey("action_error"), getkey("kindly_choose_delete"));
                    }
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        postData();
    }

    @Subscribe()
    public void getinfoupdate(adsupdatepojo obj) {
        if (obj.isIsupdate()) {
            postData();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initialize() {

        sessionManager = new SessionManager(InfoPage.this);
        cd = new ConnectionDetector(InfoPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        HashMap<String, String> user = sessionManager.getUserDetails();
        UserId = user.get(SessionManager.KEY_USERID);
        itemlist_all = new ArrayList<MessagePojo>();

        empty_text = (TextView) findViewById(R.id.emptytext);
        empty_text.setText(getkey("no_messages_available"));
        mMessageList = (ListView) findViewById(R.id.message_listview);
        Iv_previous = (ImageView) findViewById(R.id.img_prev);
        Iv_next = (ImageView) findViewById(R.id.img_next);
        Iv_back = (ImageView) findViewById(R.id.img_back);

        TextView txt_page_title = (TextView) findViewById(R.id.txt_page_title);
        txt_page_title.setText(getkey("home_message"));

        TextView viewmessage = (TextView) findViewById(R.id.viewmessage);
        viewmessage.setText(getkey("view_message"));

        TextView txt_label_blackmaria = (TextView) findViewById(R.id.txt_label_blackmaria);
        txt_label_blackmaria.setText(getkey("blackmaria_inc"));

        thisweek = findViewById(R.id.thisweek);
        thismonth = findViewById(R.id.thismonth);
        thismonth.setText(getkey("this_month"));
        thisyear = findViewById(R.id.thisyear);
        thisyear.setText(getkey("this_year"));
        selectall = findViewById(R.id.selectall);
        selectall.setText(getkey("select_all"));
        unreadcount = findViewById(R.id.unreadcount);
        delete = findViewById(R.id.delete);
        delete.setText(getkey("delete"));
        Iv_back.setOnClickListener(this);

        mode = "";

        thisweek.setText(getkey("this_week"));

    }

    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", UserId);
        jsonParams.put("type", "notification");
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        jsonParams.put("mode", mode);
        if (isInternetPresent) {
            postRequest_walletMoney(Iconstant.mesggaeg_url, jsonParams);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));
        }
    }

    private void postData2(final String notifiId) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", UserId);
        jsonParams.put("notification_id", notifiId);

        if (isInternetPresent) {
            postRequest_MessageUpdate(Iconstant.message_update_list, jsonParams);
        } else {
            Alert(getkey("action_error"),getkey("alert_nointernet"));
        }
    }

    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == Iv_back) {
                Intent in = new Intent(InfoPage.this, Navigation_new.class);
                startActivity(in);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else {
            Alert(getkey("alert_nointernet"), getkey("alert_nointernet_message"));
        }

    }

    private void postRequest_MessageUpdate(String Url, HashMap<String, String> jsonParams) {
        selectall.setText(getkey("select_all"));
        statusselect = 0;
        final Dialog dialog = new Dialog(InfoPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));


        System.out.println("-------------Message  Url----------------" + Url);


        mRequest = new ServiceRequest(InfoPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Message  Response----------------" + response);

                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "", ScurrencySymbol = "", Str_total_amount = "", Str_total_transaction = "";

                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        String Sresponse = object.getString("response");
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        postData();

                    } else {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        String Sresponse = object.getString("response");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });


    }

    //-----------------------wallet Money Post Request-----------------
    private void postRequest_walletMoney(String Url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(InfoPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));


        System.out.println("-------------Message  Url----------------" + Url);


        mRequest = new ServiceRequest(InfoPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Message  Response----------------" + response);

                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "", ScurrencySymbol = "", Str_total_amount = "", Str_total_transaction = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");

                        if (response_object.length() > 0) {
                            String totlanotification = response_object.getString("total_record");
                            sessionManager.setInfoBadge(totlanotification);
                            Object check_trans_object = response_object.get("notification");
                            itemlist_all.clear();
                            if (check_trans_object instanceof JSONArray) {
                                JSONArray trans_array = response_object.getJSONArray("notification");
                                mStrings = new String[trans_array.length()];
                                if (trans_array.length() > 0) {
                                    for (int i = 0; i < trans_array.length(); i++) {
                                        JSONObject trans_object = trans_array.getJSONObject(i);
                                        MessagePojo pojo = new MessagePojo();
                                        pojo.setDate(trans_object.getString("date"));
                                        pojo.setIMage(trans_object.getString("image"));
                                        pojo.setTitle(trans_object.getString("title"));
                                        pojo.setDescription(trans_object.getString("description"));
                                        pojo.setNotification_id(trans_object.getString("notification_id"));
                                        pojo.setIschecked(false);
                                        mStrings[i] = trans_object.getString("title");
                                        itemlist_all.add(pojo);
                                    }

                                    unreadcount.setText(String.valueOf(itemlist_all.size()));
                                    isTransactionAvailable = true;
                                } else {
                                    unreadcount.setText(String.valueOf(itemlist_all.size()));
                                    isTransactionAvailable = false;
                                    Iv_next.setVisibility(View.GONE);
                                }
                            } else {
                                isTransactionAvailable = false;
                                Iv_next.setVisibility(View.GONE);
                            }
                            currentPage = Integer.parseInt(response_object.getString("current_page"));
                            str_NextPage = response_object.getString("next_page");
                            if (str_NextPage.equalsIgnoreCase("")) {
                                Iv_next.setVisibility(View.GONE);
                            } else {
                                Iv_next.setVisibility(View.VISIBLE);
                            }
                            perPage = response_object.getString("perPage");
                        }
                    }

                    if (currentPage == 1) {
                        Iv_previous.setVisibility(View.GONE);
                    } else {
                        Iv_previous.setVisibility(View.VISIBLE);
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        if (isTransactionAvailable) {
                            adapter = new MessageListviewAdapter1(InfoPage.this, 1, itemlist_all, InfoPage.this);
                            mMessageList.setAdapter(adapter);
                            empty_text.setVisibility(View.GONE);
                            mMessageList.setVisibility(View.VISIBLE);
                            //  Iv_next.setVisibility(View.VISIBLE);
                        } else {
                            empty_text.setVisibility(View.VISIBLE);
                            mMessageList.setVisibility(View.GONE);
                            //  Iv_next.setVisibility(View.GONE);
                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }


            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

    }


    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(InfoPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onClickview(int position, MessagePojo pojo) {
        Intent i = new Intent(InfoPage.this, Notification_subpage.class);
        i.putExtra("image", itemlist_all.get(position).getIMage());
        i.putExtra("message", itemlist_all.get(position).getDescription());
        i.putExtra("id", itemlist_all.get(position).getNotification_id());
        startActivity(i);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}


