package com.blackmaria.api_request;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.blackmaria.R;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.AppController;
import com.blackmaria.volley.VolleyMultipartRequest;
import com.blackmaria.widgets.PkDialog;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MultipartRequest {

    private Context context;
    private ServiceListener mServiceListener;
    private StringRequest stringRequest;
    private SessionManager session;
    private AppInfoSessionManager headerSessionManager;
    private String driverID = "", gcmID = "", language_code = "", Agent_Name = "";
    private VolleyMultipartRequest multipartRequest;
    private byte[] array = null;
    private String nameofimage = "";

    public interface ServiceListener {
        void onCompleteListener(String response);

        void onErrorListener();
    }

    public MultipartRequest(Context context, String nameofimage) {
        this.nameofimage = nameofimage;
        this.context = context;
        session = new SessionManager(context);

        HashMap<String, String> user = session.getUserDetails();
        driverID = user.get(SessionManager.KEY_DRIVERID);
        gcmID = user.get(SessionManager.KEY_GCM_ID);

        headerSessionManager = new AppInfoSessionManager(context);
        HashMap<String, String> header = headerSessionManager.getAppInfo();
        Agent_Name = header.get(headerSessionManager.KEY_APP_IDENTITY);
        language_code = Iconstant.cabily_AppLanguage;
        gcmID = user.get(SessionManager.KEY_GCM_ID);
    }

    public void cancelRequest() {
        if (stringRequest != null) {
            stringRequest.cancel();
        }
    }

    public void makeServiceRequest(final String url, int method, final byte array[], ServiceListener listener) {

        this.mServiceListener = listener;

        multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                System.out.println("------------- image response-----------------" + response.data);

                String resultResponse = new String(response.data);
                System.out.println("-------------  response-----------------" + resultResponse);
                try {

                    mServiceListener.onCompleteListener(resultResponse);
                    JSONObject object = new JSONObject(resultResponse);

                    if (object.has("is_dead") || object.has("is_out")) {

                        String message = object.getString("message");

                        final PkDialog mDialog = new PkDialog(context);
                        mDialog.setDialogTitle(context.getResources().getString(R.string.action_session_expired_title));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(context.getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                session.logoutUser();



                            }
                        });
                        mDialog.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
               /* params.put("user_id", UserID);
                System.out.println("user_id---------------" + UserID);*/
                return params;
            }

            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                if(nameofimage.equalsIgnoreCase("blackmariavehicle.jpg")){
                    params.put("vehicle_image", new DataPart(nameofimage, array));
                }

               else if(nameofimage.equalsIgnoreCase("road_tax.jpg")){
                    params.put("road_tax", new DataPart(nameofimage, array));
                }
                else if(nameofimage.equalsIgnoreCase("insurance_copy.jpg")){
                    params.put("insurance_copy", new DataPart(nameofimage, array));
                }
                else if(nameofimage.equalsIgnoreCase("driving_license.jpg")){
                    params.put("driving_license", new DataPart(nameofimage, array));
                }




                else{
                    params.put("image", new DataPart(nameofimage, array));
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authkey", Agent_Name);
                headers.put("isapplication", Iconstant.cabily_IsApplication);
                headers.put("applanguage", language_code);
                headers.put("apptype", Iconstant.cabily_AppType);
                headers.put("driverid", driverID);
                headers.put("apptoken", gcmID);

                System.out.println("------------Headers-----" + headers);
                return headers;
            }

        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(multipartRequest);
    }


}
