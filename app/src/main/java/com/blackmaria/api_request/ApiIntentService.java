package com.blackmaria.api_request;


import android.app.IntentService;
import android.content.Intent;


import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

public class ApiIntentService extends IntentService implements ApIServices.completelisner {

    private String url = "";

    public ApiIntentService() {
        super("MyService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        HashMap<String, String> hashMap = (HashMap<String, String>) intent.getSerializableExtra("params");
        url = intent.getStringExtra("url");
        ApIServices apIServices = new ApIServices(getApplicationContext(), ApiIntentService.this);
        apIServices.dooperation(url, hashMap);
    }

    @Override
    public void sucessresponse(String val) {
        if (url.equalsIgnoreCase(Iconstant.getvehicledetails)) {
            SessionManager sessionManager = SessionManager.getInstance(this);
            sessionManager.setvehicleinfo(val);
        }

        EventBus.getDefault().post(val);
    }

    @Override
    public void errorreponse() {

    }

    @Override
    public void jsonexception() {

    }
}
