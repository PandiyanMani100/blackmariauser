package com.blackmaria.api_request;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.blackmaria.R;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.AppController;
import com.blackmaria.widgets.PkDialog;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ServiceRequest {
    private Context context;
    private ServiceListener mServiceListener;
    private StringRequest stringRequest;
    SessionManager session;
    AppInfoSessionManager headerSessionManager;
    private String driverID = "", gcmID = "", language_code = "", Agent_Name = "";

    public interface ServiceListener {
        void onCompleteListener(String response);

        void onErrorListener();
    }

    public ServiceRequest(Context context) {
        this.context = context;
        session = new SessionManager(context);

        HashMap<String, String> user = session.getUserDetails();
        driverID = user.get(SessionManager.KEY_DRIVERID);
        gcmID = user.get(SessionManager.KEY_GCM_ID);

        headerSessionManager = new AppInfoSessionManager(context);
        HashMap<String, String> header = headerSessionManager.getAppInfo();
        Agent_Name = header.get(headerSessionManager.KEY_APP_IDENTITY);
        language_code = Iconstant.cabily_AppLanguage;
    }

    public void cancelRequest() {
        if (stringRequest != null) {
            stringRequest.cancel();
        }
    }

    public void makeServiceRequest(final String url, int method, final HashMap<String, String> param, ServiceListener listener) {

        this.mServiceListener = listener;
        System.out.println("---------Url--------" + url);

        stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mServiceListener.onCompleteListener(response);

                    System.out.println("---------Response--------" + response);

                    JSONObject object = new JSONObject(response);

                    if (object.has("is_dead") || object.has("is_out")) {
                        System.out.println("-----------is dead----------------");

                        String message = object.getString("message");

                        final PkDialog mDialog = new PkDialog(context);
                        mDialog.setDialogTitle(context.getResources().getString(R.string.action_session_expired_title));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(context.getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                session.logoutUser();

                                //Stop the xmpp service

                            }
                        });
                        mDialog.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        Toast.makeText(context, context.getResources().getString(R.string.network_slow), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(context,context.getResources().getString(R.string.authfail) , Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                         Toast.makeText(context, context.getResources().getString(R.string.servererror), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof NetworkError) {
                        Toast.makeText(context, context.getResources().getString(R.string.networerror), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(context, context.getResources().getString(R.string.paresrerror), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mServiceListener.onErrorListener();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                System.out.println("---------Params--------" + param);
                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authkey", Agent_Name);
                headers.put("isapplication", Iconstant.cabily_IsApplication);
                headers.put("applanguage", language_code);
                headers.put("apptype", Iconstant.cabily_AppType);
                headers.put("driverid", "");
                headers.put("apptoken", gcmID);

                System.out.println("------------Headers-----" + headers);

                return headers;
            }
        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

}
