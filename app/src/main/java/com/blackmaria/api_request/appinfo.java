package com.blackmaria.api_request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class appinfo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("info")
        Info InfoObject;


        // Getter Methods

        public Info getInfo() {
            return InfoObject;
        }

        // Setter Methods

        public void setInfo(Info infoObject) {
            this.InfoObject = infoObject;
        }

        public class Info {
            public String getDriver_review() {
                return driver_review;
            }

            public void setDriver_review(String driver_review) {
                this.driver_review = driver_review;
            }

            @SerializedName("driver_review")
            private String driver_review;

            public String getRide_earn() {
                return ride_earn;
            }

            public void setRide_earn(String ride_earn) {
                this.ride_earn = ride_earn;
            }

            @SerializedName("ride_earn")
            private String ride_earn;

            public String getVehicle_type() {
                return vehicle_type;
            }

            public void setVehicle_type(String vehicle_type) {
                this.vehicle_type = vehicle_type;
            }

            @SerializedName("vehicle_type")
            private String vehicle_type;

            public String getIcon_car_image() {
                return icon_car_image;
            }

            public void setIcon_car_image(String icon_car_image) {
                this.icon_car_image = icon_car_image;
            }

            @SerializedName("icon_car_image")
            private String icon_car_image;

            @SerializedName("site_contact_mail")
            private String site_contact_mail;
            @SerializedName("customer_service_number")
            private String customer_service_number;
            @SerializedName("server_mode")
            private String server_mode;
            @SerializedName("site_mode")
            private String site_mode;
            @SerializedName("site_mode_string")
            private String site_mode_string;
            @SerializedName("site_url")
            private String site_url;
            @SerializedName("xmpp_host_url")
            private String xmpp_host_url;
            @SerializedName("xmpp_host_name")
            private String xmpp_host_name;
            @SerializedName("facebook_id")
            private String facebook_id;
            @SerializedName("google_plus_app_id")
            private String google_plus_app_id;
            @SerializedName("driver_google_ios_key")
            private String driver_google_ios_key;
            @SerializedName("driver_google_android_key")
            private String driver_google_android_key;
            @SerializedName("driver_google_ios_server_key")
            private String driver_google_ios_server_key;
            @SerializedName("driver_google_android_server_key")
            private String driver_google_android_server_key;
            @SerializedName("user_google_ios_key")
            private String user_google_ios_key;
            @SerializedName("user_google_android_key")
            private String user_google_android_key;
            @SerializedName("user_google_ios_server_key")
            private String user_google_ios_server_key;
            @SerializedName("user_google_android_server_key")
            private String user_google_android_server_key;
            @SerializedName("cashback_mode")
            private String cashback_mode;
            @SerializedName("phantom_car")
            private String phantom_car;
            @SerializedName("app_identity_name")
            private String app_identity_name;
            @SerializedName("about_content")
            private String about_content;
            @SerializedName("user_image")
            private String user_image;
            @SerializedName("lang_code")
            private String lang_code;
            @SerializedName("with_pincode")
            private String with_pincode;
            @SerializedName("phone_masking_status")
            private String phone_masking_status;

            public ArrayList<EmergencyNumbers> getEmergencyNumbers() {
                return emergencyNumbers;
            }

            public void setEmergencyNumbers(ArrayList<EmergencyNumbers> emergencyNumbers) {
                this.emergencyNumbers = emergencyNumbers;
            }

            @SerializedName("emergencyNumbers")
            ArrayList< EmergencyNumbers > emergencyNumbers = new ArrayList< EmergencyNumbers >();
            @SerializedName("user_guide")
            private String user_guide;
            @SerializedName("profile_complete")
            private String profile_complete;
            @SerializedName("terms_condition")
            private String terms_condition;


            // Getter Methods

            public class EmergencyNumbers{
                @SerializedName("title")
                private String title;
                @SerializedName("number")
                private String number;


                // Getter Methods

                public String getTitle() {
                    return title;
                }

                public String getNumber() {
                    return number;
                }

                // Setter Methods

                public void setTitle(String title) {
                    this.title = title;
                }

                public void setNumber(String number) {
                    this.number = number;
                }
            }

            public String getSite_contact_mail() {
                return site_contact_mail;
            }

            public String getCustomer_service_number() {
                return customer_service_number;
            }

            public String getServer_mode() {
                return server_mode;
            }

            public String getSite_mode() {
                return site_mode;
            }

            public String getSite_mode_string() {
                return site_mode_string;
            }

            public String getSite_url() {
                return site_url;
            }

            public String getXmpp_host_url() {
                return xmpp_host_url;
            }

            public String getXmpp_host_name() {
                return xmpp_host_name;
            }

            public String getFacebook_id() {
                return facebook_id;
            }

            public String getGoogle_plus_app_id() {
                return google_plus_app_id;
            }

            public String getDriver_google_ios_key() {
                return driver_google_ios_key;
            }

            public String getDriver_google_android_key() {
                return driver_google_android_key;
            }

            public String getDriver_google_ios_server_key() {
                return driver_google_ios_server_key;
            }

            public String getDriver_google_android_server_key() {
                return driver_google_android_server_key;
            }

            public String getUser_google_ios_key() {
                return user_google_ios_key;
            }

            public String getUser_google_android_key() {
                return user_google_android_key;
            }

            public String getUser_google_ios_server_key() {
                return user_google_ios_server_key;
            }

            public String getUser_google_android_server_key() {
                return user_google_android_server_key;
            }

            public String getCashback_mode() {
                return cashback_mode;
            }

            public String getPhantom_car() {
                return phantom_car;
            }

            public String getApp_identity_name() {
                return app_identity_name;
            }

            public String getAbout_content() {
                return about_content;
            }

            public String getUser_image() {
                return user_image;
            }

            public String getLang_code() {
                return lang_code;
            }

            public String getWith_pincode() {
                return with_pincode;
            }

            public String getPhone_masking_status() {
                return phone_masking_status;
            }

            public String getUser_guide() {
                return user_guide;
            }

            public String getProfile_complete() {
                return profile_complete;
            }

            public String getTerms_condition() {
                return terms_condition;
            }

            // Setter Methods

            public void setSite_contact_mail(String site_contact_mail) {
                this.site_contact_mail = site_contact_mail;
            }

            public void setCustomer_service_number(String customer_service_number) {
                this.customer_service_number = customer_service_number;
            }

            public void setServer_mode(String server_mode) {
                this.server_mode = server_mode;
            }

            public void setSite_mode(String site_mode) {
                this.site_mode = site_mode;
            }

            public void setSite_mode_string(String site_mode_string) {
                this.site_mode_string = site_mode_string;
            }

            public void setSite_url(String site_url) {
                this.site_url = site_url;
            }

            public void setXmpp_host_url(String xmpp_host_url) {
                this.xmpp_host_url = xmpp_host_url;
            }

            public void setXmpp_host_name(String xmpp_host_name) {
                this.xmpp_host_name = xmpp_host_name;
            }

            public void setFacebook_id(String facebook_id) {
                this.facebook_id = facebook_id;
            }

            public void setGoogle_plus_app_id(String google_plus_app_id) {
                this.google_plus_app_id = google_plus_app_id;
            }

            public void setDriver_google_ios_key(String driver_google_ios_key) {
                this.driver_google_ios_key = driver_google_ios_key;
            }

            public void setDriver_google_android_key(String driver_google_android_key) {
                this.driver_google_android_key = driver_google_android_key;
            }

            public void setDriver_google_ios_server_key(String driver_google_ios_server_key) {
                this.driver_google_ios_server_key = driver_google_ios_server_key;
            }

            public void setDriver_google_android_server_key(String driver_google_android_server_key) {
                this.driver_google_android_server_key = driver_google_android_server_key;
            }

            public void setUser_google_ios_key(String user_google_ios_key) {
                this.user_google_ios_key = user_google_ios_key;
            }

            public void setUser_google_android_key(String user_google_android_key) {
                this.user_google_android_key = user_google_android_key;
            }

            public void setUser_google_ios_server_key(String user_google_ios_server_key) {
                this.user_google_ios_server_key = user_google_ios_server_key;
            }

            public void setUser_google_android_server_key(String user_google_android_server_key) {
                this.user_google_android_server_key = user_google_android_server_key;
            }

            public void setCashback_mode(String cashback_mode) {
                this.cashback_mode = cashback_mode;
            }

            public void setPhantom_car(String phantom_car) {
                this.phantom_car = phantom_car;
            }

            public void setApp_identity_name(String app_identity_name) {
                this.app_identity_name = app_identity_name;
            }

            public void setAbout_content(String about_content) {
                this.about_content = about_content;
            }

            public void setUser_image(String user_image) {
                this.user_image = user_image;
            }

            public void setLang_code(String lang_code) {
                this.lang_code = lang_code;
            }

            public void setWith_pincode(String with_pincode) {
                this.with_pincode = with_pincode;
            }

            public void setPhone_masking_status(String phone_masking_status) {
                this.phone_masking_status = phone_masking_status;
            }

            public void setUser_guide(String user_guide) {
                this.user_guide = user_guide;
            }

            public void setProfile_complete(String profile_complete) {
                this.profile_complete = profile_complete;
            }

            public void setTerms_condition(String terms_condition) {
                this.terms_condition = terms_condition;
            }
        }
    }
}


