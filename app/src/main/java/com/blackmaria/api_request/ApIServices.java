package com.blackmaria.api_request;


import android.content.Context;

import com.android.volley.Request;

import com.blackmaria.utils.SessionManager;
import com.xendit.AuthenticationCallback;
import com.xendit.Models.Authentication;
import com.xendit.Models.Card;
import com.xendit.Models.Token;
import com.xendit.Models.XenditError;
import com.xendit.TokenCallback;
import com.xendit.Xendit;

import java.util.HashMap;

public class ApIServices {
    public Context context;
    completelisner completelisner;
    Xenditinterface xenditinterface;
    private SessionManager session;
    private String number = "", sContact_mail = "", app_identity_name = "", Language_code = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
    private boolean isAppInfoAvailable = false;


    public ApIServices(Context context, completelisner completelisner) {
        this.context = context;
        this.completelisner = completelisner;
        session = SessionManager.getInstance(context);
    }

    public ApIServices(Context context, Xenditinterface xenditinterface, Context contexts) {
        this.context = context;
        this.xenditinterface = xenditinterface;
        session = SessionManager.getInstance(context);
    }

    public interface completelisner {
        void sucessresponse(String val);

        void errorreponse();

        void jsonexception();

    }

    public interface Xenditinterface{
        void Xenditerrorresponse(String msg);
        void Xenditsucessresponse(String val);
        void sucessresponse(String val);
        void errorreponse();
    }

    public void imageupload(String url, byte[] array, String driverimagename){
        MultipartRequest mRequest = new MultipartRequest(context,driverimagename);
        mRequest.makeServiceRequest(url, Request.Method.POST, array, new MultipartRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String sStatus = "", sResponse = "", SUser_image = "", Smsg = "";
                try {
                    completelisner.sucessresponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                completelisner.errorreponse();
            }
        });
    }

    public void dooperation(String Url, HashMap<String, String> jsonParams) {
        ServiceRequest mRequest = new ServiceRequest(context);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                try {
                    completelisner.sucessresponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                completelisner.errorreponse();
            }
        });
    }

    public void Xendtidooperation(String Url, HashMap<String, String> jsonParams) {
        ServiceRequest mRequest = new ServiceRequest(context);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                try {
                    xenditinterface.sucessresponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                xenditinterface.errorreponse();
            }
        });
    }


    public void CreateXenditToken(Card card, boolean isMultipleUse, Xendit xendit, boolean shouldAuthenticate, String insertAmount, String CURRENCYCONVERSIONKEY_KEY){
        TokenCallback callback = new TokenCallback() {
            @Override
            public void onSuccess(Token token) {
                if (token != null) {
                    if (token.getStatus().equalsIgnoreCase("VERIFIED")) {
                        xenditinterface.Xenditsucessresponse(token.getId());
                    }
                }
            }

            @Override
            public void onError(XenditError xenditError) {
                xenditinterface.Xenditerrorresponse(xenditError.getErrorCode() + xenditError.getErrorMessage());
            }
        };


        if (isMultipleUse) {
            xendit.createMultipleUseToken(card, callback);
        } else {
            Double amount = 0.0;
            amount = Double.parseDouble(insertAmount) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
            amount = (Double) Math.ceil(amount);
            xendit.createSingleUseToken(card, amount, shouldAuthenticate, callback);
        }
    }

    public void CreateAuthenticationId(final String tokenId, final Xendit xendit, final Double amount){
        xendit.createAuthentication(tokenId, amount, new AuthenticationCallback() {
            @Override
            public void onSuccess(Authentication authentication) {
                xenditinterface.Xenditsucessresponse(authentication.getId());
            }

            @Override
            public void onError(XenditError xenditError) {
                xenditinterface.Xenditerrorresponse(xenditError.getErrorCode()+xenditError.getErrorMessage());
            }
        });
    }



}
