package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.volley.Request;
import com.blackmaria.adapter.MenuHomeRateAdapter;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.MenuHomeratingPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 8/30/2017.
 */

public class MenuUserratingHOme extends ActivityHockeyApp implements View.OnClickListener {

    private SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    LanguageDb mhelper;


    private ImageView backImag;//ratingPageProfilephoto;
    private TextView ratingPageDriverCartypeTextview, viewAllReviews;
    private View dummyview5;
    private RatingBar myrideRatingSingleRatingbar;
    private TextView myrideRatingSingleTitle;
    private RatingBar myrideRatingSingleRatingbar4;
    private TextView myrideRatingSingleTitle1;
    private RatingBar myrideRatingSingleRatingbar1;
    private TextView myrideRatingSingleTitle2;
    private RatingBar myrideRatingSingleRatingbar3;
    private RoundedImageView ratingPageProfilephoto1,img_page_logo;
    private TextView dateName;
    private TextView comments;
    private TextView infoBtn;
    private ListView menuHomeListView;
    MenuHomeRateAdapter adapter;
    private String UserID = "";
    ArrayList<MenuHomeratingPojo> itemlist;
    private boolean isDataAvailable = false;
    private String rideId1 = "";
    private RefreshReceiver refreshReceiver;
    private TextView lastcomment_text;
    private ConstraintLayout lastcomment_ll;
    private String type = "";

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
        System.out.println("************ GC Memory cleared***********");
    }

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(MenuUserratingHOme.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(MenuUserratingHOme.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_rating_home_new_constrain);
        mhelper=new LanguageDb(this);
        Initialize();

        if (getIntent().hasExtra("frompage")) {
            type = getIntent().getStringExtra("frompage");
        }

    }

    private void Initialize() {
        session = new SessionManager(MenuUserratingHOme.this);
        cd = new ConnectionDetector(MenuUserratingHOme.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemlist = new ArrayList<MenuHomeratingPojo>();
        backImag = (ImageView) findViewById(R.id.back_imag);
        ratingPageDriverCartypeTextview = findViewById(R.id.rating_page_driver_cartype_textview);
        ratingPageDriverCartypeTextview.setText(getkey("rating_score"));
        myrideRatingSingleRatingbar = (RatingBar) findViewById(R.id.myride_rating_single_ratingbar);
        ratingPageProfilephoto1 = findViewById(R.id.rating_page_profilephoto1);
        img_page_logo = findViewById(R.id.img_page_logo);
        viewAllReviews = findViewById(R.id.view_all_reviews);
        viewAllReviews.setText(getkey("all_comments"));
        lastcomment_text = (TextView) findViewById(R.id.noreview_tv);
        lastcomment_text.setText(getkey("no_reviews_yet"));
        lastcomment_ll = findViewById(R.id.noreviews_available);
        infoBtn = (TextView) findViewById(R.id.info_btn);
        infoBtn.setText(getkey("trip_infos"));
        dateName = (TextView) findViewById(R.id.date_name);
        dateName.setText(getkey("na"));
        comments = (TextView) findViewById(R.id.comments);
        comments.setText(getkey("na"));
        menuHomeListView = (ListView) findViewById(R.id.menu_rating_home_list);

        TextView lastcommente= (TextView) findViewById(R.id.lastcommente);
        lastcommente.setText(getkey("lastcomment"));

        backImag.setOnClickListener(this);
        viewAllReviews.setOnClickListener(this);
        infoBtn.setOnClickListener(this);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        registerReceiver(refreshReceiver, intentFilter);

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        if (isInternetPresent) {
            postRequest_MenuratingHome(Iconstant.menupage_ratinglist_home);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }
    }

    @Override
    public void onClick(View v) {
        if (v == viewAllReviews) {
            Intent intent = new Intent(MenuUserratingHOme.this, MenuRatingListActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (v == infoBtn) {
            Intent intent = new Intent(MenuUserratingHOme.this, RideDetailPage.class);
            intent.putExtra("ride_id", rideId1);
            intent.putExtra("user_id", UserID);
            intent.putExtra("class", "MenuUserratingHOme");

            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (v == backImag) {
//            if (type.equalsIgnoreCase("Home")) {
//                Intent intent = new Intent(MenuUserratingHOme.this, Navigation_new.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);
//            } else {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
//            }
        }
    }


    //-----------------------Rating List Post Request-----------------
    private void postRequest_MenuratingHome(String Url) {
        dialog = new Dialog(MenuUserratingHOme.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));


        System.out.println("-------------MenuratingHome  Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);


        mRequest = new ServiceRequest(MenuUserratingHOme.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String userName = "", userEmail = "", userGender = "", userAge = "", userImage = "", userAvgReview = "", driverName = "", driveromments = "", DriverImage = "", reviewDate1 = "";
                Log.e("rateing", response);

                System.out.println("-------------MenuratingHome Response----------------" + response);

                String Sstatus = "";
                String SRating_status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject jsonObj = object.getJSONObject("response");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject driver_profile_object = jsonObj.getJSONObject("user_profile");

                        if (driver_profile_object.length() > 0) {
                            userName = driver_profile_object.getString("user_name");
                            userEmail = driver_profile_object.getString("email");
                            userGender = driver_profile_object.getString("gender");
                            userAge = driver_profile_object.getString("age");
                            userImage = driver_profile_object.getString("user_image");
                            userAvgReview = driver_profile_object.getString("avg_review");
                            if(!userImage.equals("")){
                                try {
                                    Picasso.with(MenuUserratingHOme.this)
                                            .load(userImage)
                                            .into(img_page_logo);
                                }catch (IllegalArgumentException e){}
                            }

                        }

                        Object intervention = jsonObj.get("last_comment_review");
                        if (intervention instanceof JSONArray) {
                            // It's an array
                            //viewAllReviews.setVisibility(View.GONE);
                            lastcomment_ll.setVisibility(View.GONE);
                            lastcomment_text.setVisibility(View.VISIBLE);

                        } else if (intervention instanceof JSONObject) {
                            // It's an object
                            JSONObject objLastreview = jsonObj.getJSONObject("last_comment_review");
                            if (objLastreview.length() > 0) {
                                driverName = objLastreview.getString("driver_name");
                                driveromments = objLastreview.getString("comments");
                                DriverImage = objLastreview.getString("image");
                                rideId1 = objLastreview.getString("ride_id");
                                reviewDate1 = objLastreview.getString("review_date");
                            }
                            lastcomment_ll.setVisibility(View.VISIBLE);
                            lastcomment_text.setVisibility(View.GONE);
                        }

                        ArrayList<String>  title =new ArrayList<String>();
                        JSONArray payment_array = jsonObj.getJSONArray("reason_review");
                        if (payment_array.length() > 0) {
                            itemlist.clear();
                            title.clear();
                            for (int i = 0; i < payment_array.length(); i++) {
                                JSONObject reason_object = payment_array.getJSONObject(i);

                                if(title.contains(reason_object.getString("title")))
                                {

                                }
                                else
                                {
                                    title.add(reason_object.getString("title"));
                                    MenuHomeratingPojo pojo = new MenuHomeratingPojo();
                                    pojo.SetRatingTitle(reason_object.getString("title"));
                                    pojo.getRatingAvarage(reason_object.getString("avg_ratting"));
                                    pojo.SetRatingCount(reason_object.getString("total_count"));
                                    itemlist.add(pojo);
                                }

                            }
                            isDataAvailable = true;
                        } else {
                            isDataAvailable = false;
                        }
                    }

                    if (Sstatus.equalsIgnoreCase("1") && isDataAvailable) {

                        if (SRating_status.equalsIgnoreCase("1")) {

                            Toast.makeText(getApplicationContext(),  getkey("allready_submited"), Toast.LENGTH_SHORT).show();

                        } else {

                            if (!DriverImage.equalsIgnoreCase("")) {
                                Picasso.with(MenuUserratingHOme.this)
                                        .load(DriverImage)
                                        .into(ratingPageProfilephoto1);

                                myrideRatingSingleRatingbar.setRating(Float.parseFloat(userAvgReview));
                            }

                            dateName.setText(driverName + " on " + reviewDate1);
                            if(driveromments.startsWith("Optional"))
                            {
                                comments.setText(getkey("nocomments"));
                            }
                            else
                            {
                                comments.setText(driveromments);
                            }


                            adapter = new MenuHomeRateAdapter(MenuUserratingHOme.this, itemlist);
                            menuHomeListView.setAdapter(adapter);
                        }

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(MenuUserratingHOme.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
