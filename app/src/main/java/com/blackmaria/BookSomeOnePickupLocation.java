package com.blackmaria;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.InterFace.CallBack;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.GeocoderHelper;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.List;
import java.util.Locale;

/**
 * Created by user144 on 7/26/2017.
 */

public class BookSomeOnePickupLocation extends ActivityHockeyApp implements GoogleApiClient.ConnectionCallbacks {

    private RelativeLayout bookMyRidePickupAddressLayout;
    private ImageView bookNavigationGreenDotImageView;
    private TextView tvYouare;
    private CustomTextView bookMyRidePickupAddressTextView;
    private ImageView bookMyRideFavImageview;
    private TextView okBtn;
    private TextView text1;
    private ImageView homeImage;
    private ImageView roadimage;
    LanguageDb mhelper;
    private double SpickupLatitude, SpickupLongitude;
    private String SpickupLocation = "";
    private String StrPickupLocation, StrPickupLatitude, StrPickupLongitude;

    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    GPSTracker gps;
    private int placeSearch_request_code = 201;
    //-----Declaration For Enabling Gps-------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 299;
    private String bookingReference = "", someOneNmame = "", someOneGender = "", mode = "", CategoryID = "", mobileNumber,CountryCodeStr;
    private RefreshReceiver refreshReceiver;
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
 /*Intent intent1 = new Intent(BookingPage2.this, BookingPage2.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(BookSomeOnePickupLocation.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(BookSomeOnePickupLocation.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.someone_booking_pickup);
        mhelper = new LanguageDb(this);
        initialize();

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookSomeOnePickupLocation.this, BookingPage3.class);
                intent.putExtra("pickupAddress", StrPickupLocation);
                intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                intent.putExtra("destinationAddress", "");
                intent.putExtra("destinationLatitude","");
                intent.putExtra("destinationLongitude", "");
                intent.putExtra("CategoryID", CategoryID);

                intent.putExtra("book_ref", bookingReference);
                intent.putExtra("someOneNmame",someOneNmame);
                intent.putExtra("someOneGender",someOneGender);
                intent.putExtra("mode", mode);
                intent.putExtra("page", "SomeOnePickup");
                intent.putExtra("intentCall", "SomeOnePickup");

                intent.putExtra("mobileNumber", mobileNumber);
                intent.putExtra("CountryCode", CountryCodeStr);

                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter,R.anim.exit);

            }
        });
        bookMyRidePickupAddressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SpickupLocation.length() > 0) {
                    Intent intent = new Intent(BookSomeOnePickupLocation.this, DropLocationSelect.class);
                    intent.putExtra("page","bookSomeOne");
                    startActivityForResult(intent, placeSearch_request_code);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Alert(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.home_label_invalid_pickUp));
                }
            }
        });
        homeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    private void initialize() {
        gps = new GPSTracker(BookSomeOnePickupLocation.this);
        bookMyRidePickupAddressLayout = (RelativeLayout) findViewById(R.id.book_my_ride_pickup_address_layout);
        bookNavigationGreenDotImageView = (ImageView) findViewById(R.id.book_navigation_green_dot_imageView);
        tvYouare = (TextView) findViewById(R.id.tv_youare);
        bookMyRidePickupAddressTextView = (CustomTextView) findViewById(R.id.book_my_ride_pickup_address_textView);
        bookMyRideFavImageview = (ImageView) findViewById(R.id.book_my_ride_fav_imageview);
        okBtn = (TextView) findViewById(R.id.ok_btn);
        okBtn.setText(getkey("ok_lable"));

        tvYouare.setText(getkey("action_text_currentarea_location"));
        TextView eee= (TextView) findViewById(R.id.eee);
        eee.setText(getkey("set_pickup_lable_new"));

        homeImage = (ImageView) findViewById(R.id.home_image);

        text1= (TextView) findViewById(R.id.text1);
        text1.setText(getkey("we_take_you_wherever_you_need_lable"));

        Intent intent = getIntent();
        bookingReference = intent.getStringExtra("book_ref");
        someOneNmame = intent.getStringExtra("someOneNmame");
        someOneGender = intent.getStringExtra("someOneGender");
        mode = intent.getStringExtra("mode");
        CategoryID = intent.getStringExtra("CategoryID");


        mobileNumber= intent.getStringExtra("mobileNumber");
        CountryCodeStr= intent.getStringExtra("CountryCode");



        if (gps.canGetLocation() && gps.isgpsenabled()) {
            SpickupLatitude = gps.getLatitude();
            SpickupLongitude = gps.getLongitude();
            SpickupLocation =/* getCompleteAddressString(SpickupLatitude,SpickupLongitude);//*/new GeocoderHelper().fetchCityName(BookSomeOnePickupLocation.this, SpickupLatitude,SpickupLongitude,callBack );//
            StrPickupLocation=SpickupLocation;
            bookMyRidePickupAddressTextView.setText(SpickupLocation);

        } else {
            enableGpsService();
        }
// -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        registerReceiver(refreshReceiver, intentFilter);

    }

    //-------------Method to get Complete Address------------
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(BookSomeOnePickupLocation.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                Log.e("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Current loction address", "Cannot get Address!");
        }
        return strAdd;
    }

    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(BookSomeOnePickupLocation.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(BookSomeOnePickupLocation.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(BookSomeOnePickupLocation.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("ok_lable"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == placeSearch_request_code && resultCode == Activity.RESULT_OK && data != null)) {
            StrPickupLatitude = data.getStringExtra("Selected_Latitude");
            StrPickupLongitude = data.getStringExtra("Selected_Longitude");
            StrPickupLocation = data.getStringExtra("Selected_Location");

            System.out.println("-----------SdestinationLatitude book3----------------" + StrPickupLatitude);
            System.out.println("-----------SdestinationLongitude book3----------------" + StrPickupLongitude);
            System.out.println("-----------SdestinationLocation book3----------------" + StrPickupLocation);

            bookMyRidePickupAddressTextView.setText(StrPickupLocation);
        }

    }
    CallBack callBack=new CallBack() {

        @Override

        public void onComplete(String LocationName)
        {
            StrPickupLocation=LocationName;
            SpickupLocation=LocationName;
            bookMyRidePickupAddressTextView.setText(SpickupLocation);
            System.out.println("-------------------addreess----------------0"+LocationName);
        }

        @Override

        public void onError(String errorMsg) {


        }

        @Override
        public void onCompleteAddress(String message) {

        }

    };

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}

