package com.blackmaria;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.newdesigns.adapter.SlidingImage_Adapter_mileagereward;
import com.blackmaria.newdesigns.Mileagereward_success;
import com.blackmaria.newdesigns.slider.CirclePageIndicator;
import com.blackmaria.pojo.MilesRewardsPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by user144 on 5/17/2017.
 */
public class MilesRedeemRewards extends FragmentActivityHockeyApp implements SlidingImage_Adapter_mileagereward.redeemRewards, View.OnClickListener {

    private LinearLayout Ll_miles_share_facebook, Ll_miles_share_twitter, Ll_miles_share_googlepuls;
    private TextView Tv_Km;
    private ImageView Iv_back_press, Iv_home;
    private TextView Tv_no_rewards_txt;
    private ArrayList<String> imgList;
    private RelativeLayout Rl_tandc;
    private int currImage = 0;
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private Dialog dialog;
    LanguageDb mhelper;
    private String UserID = "", Currenycode = "";
    private ArrayList<MilesRewardsPojo> milesrewardsList;
    private boolean isRewardsAvailable = false;
    private RefreshReceiver refreshReceiver;
    private String Sdistance_txt = "", Sdistance = "";
    private CustomTextView rewardsTapRedeemLable;
    private ListView rewardlistview;
    private ViewPager mPager;


    private String Miles_sharelink = "", Miles_shareMessage = "", Miles_shareSubject = "", Miles_shareImage = "", facebook = "", google = "";
    private boolean isdataPresent = false;

    final int PERMISSION_REQUEST_CODE = 111;
    final int GOOGLE_PLUS_SHARE_REQUEST_CODE = 222;


    public static final int REQUEST_CODE_FACEBOOK_LOGIN = 111;
    public static final int REQUEST_CODE_FACEBOOK_SHARE = 112;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(MileageDetails.this, Fragment_HomePage.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(MilesRedeemRewards.this, MyRideRating.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(MilesRedeemRewards.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    private void init_slidebanner(ArrayList<MilesRewardsPojo> booking_banner_arr) {
        mPager.setAdapter(new SlidingImage_Adapter_mileagereward(this, booking_banner_arr, MilesRedeemRewards.this));
        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);
        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);


        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.miles_list_view_constrain);
        mhelper = new LanguageDb(this);
        initiaztion();
        facebookSDKInitialize();
//
//        Iv_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MilesRedeemRewards.this, Navigation_new.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);
//            }
//        });

        Iv_back_press.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MilesRedeemRewards.this, MilesHome.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
//        Rl_tandc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TermsAndConditionsPopUp();
//            }
//        });
    }


    private void initiaztion() {
        imgList = new ArrayList<String>();
        session = new SessionManager(MilesRedeemRewards.this);
        cd = new ConnectionDetector(MilesRedeemRewards.this);
        isInternetPresent = cd.isConnectingToInternet();
        milesrewardsList = new ArrayList<MilesRewardsPojo>();
        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        mPager = findViewById(R.id.pager);
        Ll_miles_share_facebook = (LinearLayout) findViewById(R.id.invite_earn_fb_layout);
        Ll_miles_share_twitter = (LinearLayout) findViewById(R.id.invite_earn_twitter_layout);
        Ll_miles_share_googlepuls = (LinearLayout) findViewById(R.id.invite_earn_googleplus_layout);


//        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/digital-7.ttf");
//        Tv_miles_km.setTypeface(face);


        Ll_miles_share_facebook.setOnClickListener(this);
        Ll_miles_share_twitter.setOnClickListener(this);
        Ll_miles_share_googlepuls.setOnClickListener(this);

//        Tv_Km = (TextView) findViewById(R.id.mailes_rewards_km);
        Iv_back_press = (ImageView) findViewById(R.id.mailes_rewards_back);
//        Iv_home = (ImageView) findViewById(R.id.mailes_rewards_home_icon);

        Tv_no_rewards_txt = findViewById(R.id.no_rewards_lable);
        Tv_no_rewards_txt.setText(getkey("no_rewards_available"));

        TextView howtoredeem = findViewById(R.id.howtoredeem);
        howtoredeem.setText(getkey("how_to_redeem"));
        TextView howtoredeemcontent = findViewById(R.id.howtoredeemcontent);
        howtoredeemcontent.setText(getkey("howtoredeem"));
        TextView share_miles_tv = findViewById(R.id.share_miles_tv);
        share_miles_tv.setText(getkey("share_us_get_free_miles"));


//        Rl_tandc = (RelativeLayout) findViewById(R.id.miles_terms_and_condition);
//        rewardsTapRedeemLable = (CustomTextView) findViewById(R.id.rewards_tap_redeem_lable);
//        rewardlistview = (ListView) findViewById(R.id.rewardlistview);
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        if (isInternetPresent) {
            PostRequestMilesHome(Iconstant.miles_home_url);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));
        }


    }

    private void PostRequestForMilesRewards(String Url) {

        dialog = new Dialog(MilesRedeemRewards.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------MilesRewards url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        System.out.println("-----------MilesRewards jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(MilesRedeemRewards.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {


            @Override
            public void onCompleteListener(String response) {

                System.out.println("-----------MilesRewards response--------------" + response);

                String Sstatus = "", Sexpiry_on = "", currency = "";

                try {

                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject response_object = object.getJSONObject("response");

                        if (response_object.length() > 0) {

                            currency = response_object.getString("currency");
                            Currenycode = CurrencySymbolConverter.getCurrencySymbol(currency);
                            Sdistance_txt = response_object.getString("distance_txt");
                            Sdistance = response_object.getString("distance");
                            Sexpiry_on = response_object.getString("expiry_on");


                            Object check_rewards_object = response_object.get("rewards");

                            if (check_rewards_object instanceof JSONArray) {

                                JSONArray rewards_array = response_object.getJSONArray("rewards");

                                if (rewards_array.length() > 0) {

                                    milesrewardsList.clear();

                                    for (int i = 0; i < rewards_array.length(); i++) {

                                        JSONObject rewards_object = rewards_array.getJSONObject(i);
                                        MilesRewardsPojo milesrewards = new MilesRewardsPojo();
                                        milesrewards.setMiles_distance(rewards_object.getString("distance_range"));
                                        milesrewards.setReward_id(rewards_object.getString("reward_id"));
                                        milesrewards.setGift_name(rewards_object.getString("gift_name"));
                                        milesrewards.setCoupon_value(rewards_object.getString("coupon_value"));
                                        milesrewards.setGift_image(rewards_object.getString("gift_image"));
                                        milesrewards.setValid_to(rewards_object.getString("valid_to"));
                                        milesrewards.setValid_to_second(rewards_object.getString("valid_to_second"));
                                        milesrewards.setReward_type(rewards_object.getString("reward_type"));
                                        milesrewardsList.add(milesrewards);
                                    }
                                    isRewardsAvailable = true;

                                } else {

                                    isRewardsAvailable = false;
                                }
                            }

                        } else {

                            isRewardsAvailable = false;
                        }
                    } else {

                        isRewardsAvailable = false;

                    }


                    if (Sstatus.equalsIgnoreCase("1")) {
                        init_slidebanner(milesrewardsList);
//                        adapter = new RewardListAdapter(MilesRedeemRewards.this, milesrewardsList, MilesRedeemRewards.this);
//                        rewardlistview.setAdapter(adapter);

//                        Tv_Km.setText(Sdistance_txt);
                        if (isRewardsAvailable) {
                        } else {
//                            rewardsTapRedeemLable.setVisibility(View.GONE);
                            Tv_no_rewards_txt.setVisibility(View.VISIBLE);
                        }


                    } else {

                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }


                } catch (JSONException e) {

                    e.printStackTrace();

                    dialogDismiss();

                }


                dialogDismiss();


            }

            @Override
            public void onErrorListener() {

                dialogDismiss();

            }
        });


    }

    private void PostRequestMilesHome(String Url) {

        final Dialog dialog = new Dialog(MilesRedeemRewards.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------MilesHome url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);


        System.out.println("-----------MilesHome jsonParams--------------" + jsonParams);


        ServiceRequest mRequest = new ServiceRequest(MilesRedeemRewards.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------MilesHome reponse-------------------" + response);

                String Sstatus = "", Stotal_distance = "", Smileage_expiry = "", Sdistance_unit = "", Smileage_timeline = "", Sfree_mileage = "",
                        Ssubject = "", Smessage = "", Surl = "", Salertmsg = "";
                dialog.dismiss();
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsondata = object.getJSONObject("response");

                        if (jsondata.length() > 0) {

                            Stotal_distance = jsondata.getString("total_distance");
                            Smileage_expiry = jsondata.getString("mileage_expiry");
                            Sdistance_unit = jsondata.getString("distance_unit");
                            Miles_shareImage = jsondata.getString("mileage_timeline");
                            Sfree_mileage = jsondata.getString("free_mileage");
                            Miles_shareSubject = jsondata.getString("subject");
                            Miles_shareMessage = jsondata.getString("message");
                            facebook = jsondata.getString("facebook");
                            google = jsondata.getString("google");
                            Miles_sharelink = jsondata.getString("url");

                            isdataPresent = true;

                            if (isInternetPresent) {
                                PostRequestForMilesRewards(Iconstant.miles_reward_list_url);
                            } else {
                                Alert(getkey("action_error"), getkey("alert_nointernet"));
                            }
                        }
                    } else {
                        Salertmsg = object.getString("response");
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });


    }


    private void PostRequestForApplyRewards(String Url, int pos) {

        Dialog dialog = new Dialog(MilesRedeemRewards.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------MilesRewardsApply url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("reward_id", milesrewardsList.get(pos).getReward_id());
        jsonParams.put("distance", /*milesrewardsList.get(currImage).getMiles_distance()*/Sdistance);


        System.out.println("-----------MilesRewardsApply jsonParams--------------" + jsonParams);

        ServiceRequest mRequest = new ServiceRequest(MilesRedeemRewards.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {


            @Override
            public void onCompleteListener(String response) {

                System.out.println("-----------MilesRewardsApply response--------------" + response);

                String Sstatus = "", Sremaining_distance = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject data_reponse_object = object.getJSONObject("response");
                        Sremaining_distance = data_reponse_object.getString("remaining_distance");
                    }


                } catch (JSONException e) {


                    e.printStackTrace();

                }

                dialogDismiss();
//                if (Sstatus.equalsIgnoreCase("1")) {
//
////                    Tv_Km.setText(Sremaining_distance);
//                    Sdistance = Sremaining_distance;
//                    Alerter.create(MilesRedeemRewards.this)
//                            .setTitle("REDEEM SUCCESS")
//                            .setText("NOTIFICATION SENT TO YOUR EMAIL")
//                            .setContentGravity(Gravity.CENTER)
//                            .hideIcon()
//                            .setBackgroundColor(R.color.drive_and_earn_dark_blue_bg)
//                            .show();
//
//                } else {
//
//                    Alerter.create(MilesRedeemRewards.this)
//                            .setTitle("SORRY")
//                            .setText("NOT ENOUGH MILES CREDIT")
//                            .setContentGravity(Gravity.CENTER)
//                            .hideIcon()
//                            .setBackgroundColor(R.color.miles_list_alert_nagtive)
//                            .show();
//                }
            }

            @Override
            public void onErrorListener() {

                dialogDismiss();


            }
        });


    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(MilesRedeemRewards.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }

    private void TermsAndConditionsPopUp() {

        final Dialog dialog = new Dialog(MilesRedeemRewards.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.miles_rewards_terms_and_condions);
        final ImageView closeImage = (ImageView) dialog.findViewById(R.id.closeimage);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onClickRedeemList(int postion) {

        Intent i = new Intent(MilesRedeemRewards.this, Mileagereward_success.class);
        i.putExtra("rewardid", milesrewardsList.get(postion).getReward_id());
        i.putExtra("Sdistance", Sdistance);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {

    }

    //----------Share Image and Text on Twitter Method--------
    protected void shareTwitter(String text, Uri image) {
        boolean isAppInstalled = appInstalledOrNot("com.twitter.android");

        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_STREAM, image);
            intent.setType("image/jpeg");
            intent.setPackage("com.twitter.android");

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                Alert(getkey("alert_label_title"), getkey("invite_earn_label_twitter_not_installed"));
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_twitter_not_installed"), "https://play.google.com/store/apps/details?id=com.twitter.android");
        }
    }

    protected void shareInstagramelink(String text, Uri image) {
        boolean isAppInstalled = appInstalledOrNot("com.instagram.android");

        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            // intent.putExtra(Intent.EXTRA_STREAM, "");
            intent.setType("image/jpeg");
            intent.setPackage("com.instagram.android");

            // Add the URI to the Intent.
            intent.putExtra(Intent.EXTRA_STREAM, image);

            try {
                //startActivity(intent);
                startActivity(Intent.createChooser(intent, "Share to"));
            } catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"), getkey("invite_earn_label_instagram_not_installed"), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_instagram_not_installed"), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
        }
    }


    @Override
    public void onClick(View v) {


        if (v == Ll_miles_share_facebook) {

            if (isInternetPresent) {
                try {
                    if (isFBLoggedIn()) {
                        ShareDialog();
                    } else {
                        LoginManager.getInstance().logInWithPublishPermissions(
                                MilesRedeemRewards.this,
                                Arrays.asList("publish_actions"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
            }


        } else if (v == Ll_miles_share_twitter) {

            if (facebook.equalsIgnoreCase("1")) {

                Uri imageUri = null;
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.miles_timeline_post), null, null));
                } catch (NullPointerException e) {
                }
                shareTwitter(Miles_shareMessage, imageUri);
            } else {
                Toast.makeText(MilesRedeemRewards.this, getkey("alreadyusermoiles"), Toast.LENGTH_SHORT).show();
            }


        } else if (v == Ll_miles_share_googlepuls) {

            if (google.equalsIgnoreCase("1")) {
                Uri imageUri = null;
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.miles_timeline_post), null, null));
                } catch (NullPointerException e) {
                }

//                    shareGPlus(Miles_shareMessage, imageUri);
                shareInstagramelink(Miles_shareMessage, imageUri);

            } else {
                Toast.makeText(MilesRedeemRewards.this, getkey("alreadyusermoiles"), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void Alert1(String title, String alert, final String url) {

        final PkDialog mDialog = new PkDialog(MilesRedeemRewards.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        mDialog.show();

    }


    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case GOOGLE_PLUS_SHARE_REQUEST_CODE:

                if (resultCode == RESULT_OK) {
                    PostapplyMilesHome(Iconstant.miles_rewards_apply_milageurl, "google");
                    // Google Plus Share Success
                }

                break;

        }

        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();


        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                ShareDialog();

            }

            @Override
            public void onCancel() {
                Toast.makeText(MilesRedeemRewards.this, getkey("logincancel"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(MilesRedeemRewards.this,  getkey("loginerror"), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void ShareDialog() {

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                System.out.println("Result = " + result.getPostId());
//                Toast.makeText(MilesHome.this, "FB Share success", Toast.LENGTH_SHORT).show();

                if (isInternetPresent) {
                    PostapplyMilesHome(Iconstant.miles_rewards_apply_milageurl, "facebook");
                } else {
                    Alert(getkey("alert_label_title"),getkey("alert_nointernet"));
                }

            }

            @Override
            public void onCancel() {
                Toast.makeText(MilesRedeemRewards.this, getkey("fb_shared"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(MilesRedeemRewards.this,  getkey("fb_error"), Toast.LENGTH_SHORT).show();
            }
        }, REQUEST_CODE_FACEBOOK_SHARE);


        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(Miles_sharelink))
                .build();

        shareDialog.show(content);


    }

    private void sendBroadcast() {
        Intent local = new Intent();
        local.setAction("com.app.MilesPagePage.refreshhomePage");
        local.putExtra("refresh", "yes");
        sendBroadcast(local);
    }

    public boolean isFBLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }


    private void PostapplyMilesHome(String Url, String media) {

        final Dialog dialog = new Dialog(MilesRedeemRewards.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------MILESAPPLY URL--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("media", media);

        System.out.println("-----------MILESAPPLY jsonParams--------------" + jsonParams);


        ServiceRequest mRequest = new ServiceRequest(MilesRedeemRewards.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------MilesHome reponse-------------------" + response);
                if (dialog != null) {
                    dialog.dismiss();
                }
                String Sstatus = "", Salertmsg = "", Smilage = "", SdistanceUnit = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        sendBroadcast();
                        //  JSONObject jsondata = object.getJSONObject("response");
                        Salertmsg = object.getString("response");
                        Smilage = object.getString("mileage");
                        SdistanceUnit = object.getString("distance_unit");
//                        Tv_miles_km.setText(Smilage + " " + SdistanceUnit);
                    } else {
                        Salertmsg = object.getString("response");
                    }
                } catch (JSONException e) {

                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    Alert(getkey("action_success"), Salertmsg);
                } else {
                    Alert(getkey("action_error"), Salertmsg);

                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });


    }


    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}



