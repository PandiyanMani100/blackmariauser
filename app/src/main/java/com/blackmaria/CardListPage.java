package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.CardDetailsAdapter;
import com.blackmaria.pojo.CardDetailsPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by user144 and prabu on 7/17/2017.
 */

public class CardListPage extends Activity {

    private ArrayList<CardDetailsPojo> cardDetailsList;
    private boolean isCardDetailsAvailable = false;
    private CardDetailsAdapter cardDetailsAdapter;
    private int cardPosition;
    private ImageView Back_press;
    private ListView Lv_CardDetails;
    private ConnectionDetector cd;
    private boolean isInternetPresent;
    private TextView addcard_tv;
    private String UserID = "";
    private SessionManager session;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private ServiceRequest stripRequest;
    private String card_id = "";
    private String customer_id = "";
    private SmoothProgressBar stripSmoothbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_stripelist_new_constrain);

        intialize();

        Back_press.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        addcard_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CardListPage.this, NewProfile_Page_CardEdit.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);

            }
        });
    }

    private void intialize() {
        session = new SessionManager(CardListPage.this);
        cd = new ConnectionDetector(CardListPage.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> info = session.getUserDetails();
        String tipstime = info.get(SessionManager.KEY_PHONENO);
        UserID = info.get(SessionManager.KEY_USERID);
        stripSmoothbar = (SmoothProgressBar) findViewById(R.id.profile_loading_progressbar1);
        cardDetailsList = new ArrayList<CardDetailsPojo>();
        Back_press = (ImageView) findViewById(R.id.cardinfo_back_press);
        Lv_CardDetails = (ListView) findViewById(R.id.card_info_card_details_listview);
        addcard_tv = (TextView) findViewById(R.id.addcard_tv);

        if (isInternetPresent) {
            PostRequest(Iconstant.card_list_url);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
        }

    }


    private void PostRequest(String Url) {
        stripSmoothbar.setVisibility(View.VISIBLE);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        System.out.println("-----------cardList getdata jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(CardListPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------cardlist getdata reponse-------------------" + response);

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject jsonObject = object.getJSONObject("response");

                        Object check_object = jsonObject.get("cards");
                        if (check_object instanceof JSONObject) {
                            JSONObject jsonObject1 = jsonObject.getJSONObject("cards");

                            Object check_object1 = jsonObject1.get("result");
                            if (check_object1 instanceof JSONArray) {

                                JSONArray cardDetailArray = jsonObject1.getJSONArray("result");
                                if (cardDetailArray.length() > 0) {
                                    cardDetailsList.clear();
                                    for (int j = 0; j < cardDetailArray.length(); j++) {
                                        JSONObject driver_object = cardDetailArray.getJSONObject(j);
                                        CardDetailsPojo cardDetailsPojo = new CardDetailsPojo();

                                        cardDetailsPojo.setCard_id(driver_object.getString("card_id"));
                                        cardDetailsPojo.setCard_number(driver_object.getString("card_number"));
                                        cardDetailsPojo.setCard_type(driver_object.getString("card_type").toUpperCase());
                                        cardDetailsPojo.setCustomer_id(driver_object.getString("customer_id"));
                                        cardDetailsPojo.setExp_month(driver_object.getString("exp_month"));
                                        cardDetailsPojo.setExp_year(driver_object.getString("exp_year"));
                                        cardDetailsPojo.setCard_default(driver_object.getString("card_default"));

                                        cardDetailsList.add(cardDetailsPojo);
                                    }
                                    isCardDetailsAvailable = true;

                                } else {
                                    isCardDetailsAvailable = false;
                                }
                            } else {
                                isCardDetailsAvailable = false;
                            }
                        }
                        stripSmoothbar.setVisibility(View.GONE);

                    } else {
                        Smessage = object.getString("response");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    stripSmoothbar.setVisibility(View.GONE);
                }
                stripSmoothbar.setVisibility(View.GONE);

                if (Sstatus.equalsIgnoreCase("1")) {

                    if (isCardDetailsAvailable) {
                        Lv_CardDetails.setVisibility(View.VISIBLE);
                        cardDetailsAdapter = new CardDetailsAdapter(CardListPage.this, cardDetailsList);
                        Lv_CardDetails.setAdapter(cardDetailsAdapter);
                    } else {
                        Lv_CardDetails.setVisibility(View.GONE);
                    }

                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                stripSmoothbar.setVisibility(View.GONE);
            }
        });
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(CardListPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void defaultCard(String cardId, int position) {
        card_id = cardId;
        cardPosition = position;
        if (isInternetPresent) {
            DefaultCard(Iconstant.DefaultCardDetails);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }

    }


    private void DefaultCard(String url) {
        stripSmoothbar.setVisibility(View.VISIBLE);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("card_id", card_id);
        System.out.println("=========DefaultCard URL===========" + url);
        System.out.println("=========DefaultCard Params===========" + jsonParams);

        stripRequest = new ServiceRequest(CardListPage.this);
        stripRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("=========DefaultCard Response===========" + response);
                String Sstatus = "", Sresponse = "";

                try {
                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        stripSmoothbar.setVisibility(View.GONE);


                        AlertCardAdded(getResources().getString(R.string.action_success), Sresponse);

                    } else {
                        dialog.dismiss();
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

                dialog.dismiss();

            }
        });
    }


    public void deleteCard(String cardId, String customerId, int position) {
        card_id = cardId;
        customer_id = customerId;
        cardPosition = position;
        AlertCardDelete("Alert", "Do you want delete card?");
    }


    //--------------Alert Method-----------
    private void AlertCardDelete(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CardListPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetPresent) {
                    DeleteCard(Iconstant.DeleteCardDetails);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }

                mDialog.dismiss();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.action_cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    private void DeleteCard(String url) {
        dialog = new Dialog(CardListPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        stripSmoothbar.setVisibility(View.VISIBLE);
        TextView loading = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        loading.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("card_id", card_id);
        jsonParams.put("customer_id", customer_id);
       /*if (card_id.length() > 0) {
            jsonParams.put("card_id", card_id);
        }*/
        System.out.println("=========Delete Card URL===========" + url);
        System.out.println("=========Delete Card Params===========" + jsonParams);

        stripRequest = new ServiceRequest(CardListPage.this);
        stripRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("=========Delete Card Response===========" + response);
                String Sstatus = "", Sresponse = "";

                try {
                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        stripSmoothbar.setVisibility(View.GONE);
                        AlertCardAdded(getResources().getString(R.string.action_success), Sresponse);

                    } else {
                        stripSmoothbar.setVisibility(View.GONE);
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }

                    dialog.dismiss();

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

                stripSmoothbar.setVisibility(View.GONE);

            }
        });
    }

    //--------------Alert Method-----------
    private void AlertCardAdded(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CardListPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Refresh_page();
            }
        });

        mDialog.show();

    }

    public void Refresh_page() {

        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            PostRequest(Iconstant.card_list_url);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }

    }

    private void sendBroadcast() {
        Intent local = new Intent();
        local.setAction("com.app.PaymentListPage.refreshPage");
        local.putExtra("refresh", "yes");
        sendBroadcast(local);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sendBroadcast();
        finish();
    }


}
