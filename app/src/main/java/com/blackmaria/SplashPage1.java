package com.blackmaria;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.utils.Locationservices;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.volley.ServiceRequest;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

/**
 * Created by user14 on 4/18/2017.
 */

public class SplashPage1 extends Activity {
    private SessionManager session;
    private ServiceRequest mRequest;
    private LanguageDb mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        mHelper=new LanguageDb(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        session = new SessionManager(SplashPage1.this);
        AppCenter.start(getApplication(), "369ad6aa-0faf-4cce-8e66-197e29dcef1e",
                Analytics.class, Crashes.class);


        postRequest_Language(Iconstant.getlanguaage);






    }


    //-----------------------App Information Post Request-----------------
    private void postRequest_Language(String Url)
    {
        String languageToLoad  = session.getCurrentlanguage(); // your language
        HashMap<String, String> user = session.getUserDetails();
        String userID = user.get(SessionManager.KEY_USERID);
        System.out.println("-------------Splash App Information Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "user");
        jsonParams.put("id", userID);
        jsonParams.put("app_type", "AU");
        jsonParams.put("lang_code", languageToLoad);

        mRequest = new ServiceRequest(SplashPage1.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response)
            {
                try
                {
                    JSONObject object = new JSONObject(response);
                    String Sstatus = object.getString("status");
                    if(Sstatus.equals("1"))
                    {
                        deleteDatabase(LanguageDb.DATABASE_NAME);
                        JSONObject jsonObject = object.getJSONObject("response");
                        JSONObject language_key = jsonObject.getJSONObject("language_key");
                        Iterator<String> iter = language_key.keys();
                        while (iter.hasNext())
                        {
                            String key = iter.next();
                            try
                            {
                                Object value = language_key.get(key);
                                System.out.println("Keysss--"+key+"--"+value.toString());
                                mHelper.saveData(key,value.toString());
                            }
                            catch (JSONException e)
                            {
                            }
                        }
                        mHelper.closedatabase();
                        startService(new Intent(SplashPage1.this, Locationservices.class));
                        Intent i;
                        i = new Intent(SplashPage1.this, FetchingDataActivity.class);
                        startActivity(i);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                    else
                    {
                        startService(new Intent(SplashPage1.this, Locationservices.class));
                        Intent i;
                        i = new Intent(SplashPage1.this, FetchingDataActivity.class);
                        startActivity(i);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


               /* System.out.println("--------Language Information jsonParams--------" + response);
                String languageToLoad  = session.getCurrentlanguage(); // your language
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());

                startService(new Intent(SplashPage1.this, Locationservices.class));
                Intent i;
                i = new Intent(SplashPage1.this, FetchingDataActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);*/
            }

            @Override
            public void onErrorListener()
            {
                System.out.println("--------Language Information jsonParams--------failed");
                startService(new Intent(SplashPage1.this, Locationservices.class));
                Intent i;
                i = new Intent(SplashPage1.this, FetchingDataActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


    }



}

