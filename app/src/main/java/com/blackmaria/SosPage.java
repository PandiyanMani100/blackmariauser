package com.blackmaria;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.InterFace.CallBack;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.GeocoderHelper;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Prem Kumar and Anitha on 3/13/2017.
 */

public class SosPage extends Activity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    AppInfoSessionManager appInfoSessionManager;
    private RelativeLayout Tv_back;
    private CustomTextView Tv_PickupAddress;
    private ImageView Iv_callSos;
    private SessionManager session;
    private String sSelectedPanicNumber = "";
    final int PERMISSION_REQUEST_CODE = 111;
    GPSTracker gps;
    String UserID = "";
    private double SpickupLatitude, SpickupLongitude;
    private String SpickupLocation = "";
    private RefreshReceiver refreshReceiver;
    //-----Declaration For Enabling Gps-------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 299;
    private RelativeLayout sosPageLayout;
    private ServiceRequest mRequest;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    String strCallassist = "+6565 898914";
    final int PERMISSION_REQUEST_CODES = 222;

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(MileageDetails.this, Fragment_HomePage.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(SosPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(SosPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sos_page);
        initialize();
        sosPageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        SOS();
                    }
                }, 100);
            }
        });
    }

    private void initialize() {
        appInfoSessionManager = new AppInfoSessionManager(SosPage.this);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        Tv_back = (RelativeLayout) findViewById(R.id.sos_page_headerBar_back_textView);
        Iv_callSos = (ImageView) findViewById(R.id.sos_page_sosCall_imageView);
        Tv_PickupAddress = (CustomTextView) findViewById(R.id.book_my_ride_pickup_address_textView);
        sosPageLayout = (RelativeLayout) findViewById(R.id.sos_page_myContact_layout);
        gps = new GPSTracker(SosPage.this);
        session = new SessionManager(SosPage.this);
        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> app = appInfoSessionManager.getAppInfo();
        strCallassist = app.get(appInfoSessionManager.KEY_CUSTOMER_NUMBER);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        Tv_back.setOnClickListener(this);
        Iv_callSos.setOnClickListener(this);
        if (gps.canGetLocation() && gps.isgpsenabled()) {
            SpickupLatitude = gps.getLatitude();
            SpickupLongitude = gps.getLongitude();
            SpickupLocation = new GeocoderHelper().fetchCityName(SosPage.this, SpickupLatitude, SpickupLongitude, callBack);//
            Tv_PickupAddress.setText(SpickupLocation);

        } else {
            enableGpsService();
        }


    }

    @Override
    public void onClick(View view) {
        if (view == Tv_back) {
            overridePendingTransition(R.anim.exit, R.anim.enter);
            finish();
        } else if (view == Iv_callSos) {
            panic();
        }
    }

    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(SosPage.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SosPage.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    //-------------Method to get Complete Address------------
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(SosPage.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                Log.e("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Current loction address", "Cannot get Address!");
        }
        return strAdd;
    }

    private void SOS() {
        System.out.println("----------------muruga sos-----------------");
        final Dialog dialog = new Dialog(SosPage.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.sos_popup1);

        ImageView bkng_closeImge = (ImageView) dialog.findViewById(R.id.label_close_imageview);
        bkng_closeImge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        CustomTextView cnformTv = (CustomTextView) dialog.findViewById(R.id.tv_call_emergency);
        cnformTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", UserID);
                    jsonParams.put("latitude", SpickupLatitude + "");
                    jsonParams.put("longitude", SpickupLongitude + "");
                    postRequest_Sos(Iconstant.sos_alert_url, jsonParams);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }
            }
        });
        dialog.show();
    }

    private void SOS1() {
        System.out.println("----------------muruga sos-----------------");
        final Dialog dialog = new Dialog(SosPage.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(R.layout.sos_popup2);
        dialog.show();
        TextView tv_callassist = (TextView) dialog.findViewById(R.id.tv_callassist);
        tv_callassist.setText(strCallassist);
        ImageView bkng_closeImge = (ImageView) dialog.findViewById(R.id.label_close_imageview);
        bkng_closeImge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        RelativeLayout callAssistance = (RelativeLayout) dialog.findViewById(R.id.lyt_assistant_call);
        callAssistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + strCallassist));
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + strCallassist));
                    startActivity(intent);
                }
                dialog.dismiss();
            }
        });

    }

    private void panic() {
        System.out.println("----------------panic method-----------------");
        View view = View.inflate(SosPage.this, R.layout.sos_panic_dialog, null);
        final MaterialDialog dialog = new MaterialDialog(SosPage.this);
        dialog.setContentView(view).setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                }
        ).show();
        Button call_police = (Button) view.findViewById(R.id.call_police_button);
        Button call_fire = (Button) view.findViewById(R.id.call_fireservice_button);
        Button call_ambulance = (Button) view.findViewById(R.id.call_ambulance_button);
        call_police.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();


                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getPolice()));
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getPolice()));
                    startActivity(intent);
                }
            }
        });

        call_fire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getFire()));
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getFire()));
                    startActivity(intent);
                }
            }
        });
        call_ambulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getAmbulance()));
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getAmbulance()));
                    startActivity(intent);
                }

            }
        });

    }

    private void postRequest_Sos(String Url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(SosPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));
        System.out.println("-------------Sos alert Url----------------" + Url);
        System.out.println("-------------Sos alert jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(SosPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Sos alert Response----------------" + response);
                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    String Sresponse = object.getString("response");
                    if ("1".equalsIgnoreCase(Sstatus)) {

                        Alert(getResources().getString(R.string.action_success), Sresponse);
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        SOS1();
                        //Alert(getResources().getString(R.string.action_error), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(SosPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.ok_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    CallBack callBack = new CallBack() {

        @Override

        public void onComplete(String LocationName) {
            System.out.println("-------------------addreess----------------0" + LocationName);
            Tv_PickupAddress.setText(LocationName);
            SpickupLocation = LocationName;

        }

        @Override

        public void onError(String errorMsg) {


        }

        @Override
        public void onCompleteAddress(String message) {

        }

    };

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sSelectedPanicNumber));
                    startActivity(intent);
                }
                break;

        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODES);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            Intent intent = new Intent(SosPage.this, Navigation_new.class);
            startActivity(intent);
            overridePendingTransition(R.anim.exit, R.anim.enter);
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }
}
