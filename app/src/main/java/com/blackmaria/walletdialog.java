package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.newdesigns.Pin_activitynew;
import com.blackmaria.newdesigns.saveplus.Checkpattern;
import com.blackmaria.newdesigns.saveplus.Confirmpatern_activity;
import com.blackmaria.newdesigns.saveplus.Scanqrcode;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.volley.ServiceRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.apache.commons.codec.language.bm.Lang;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class walletdialog extends BottomSheetDialogFragment {

    private SessionManager session;

    LanguageDb mhelper;
    String saveplusida,codepath;

    public static walletdialog newInstance() {
        return new walletdialog();
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setCancelable(true);
        getDialog().setTitle("Title");

        View view = inflater.inflate(R.layout.walletpopup, null, false);

        session=new SessionManager(getActivity());
        mhelper = new LanguageDb(getActivity());

        final TextView balance = view.findViewById(R.id.balance);
        balance.setText(getkey("balancesmall") +" :"+ session.getssavedcurnecy());

        final TextView moneyin = view.findViewById(R.id.moneyin);
        final TextView moneyout = view.findViewById(R.id.moneyout);
        final TextView viewdetails = view.findViewById(R.id.viewdetails);
        viewdetails.setText(getkey("view_details"));

        final TextView moneyinn = view.findViewById(R.id.moneyinn);
        moneyinn.setText(getkey("money_in"));

        final TextView moneyoutt = view.findViewById(R.id.moneyoutt);
        moneyoutt.setText(getkey("money_out"));


        JSONObject object = null;
        try {
            object = new JSONObject(session.getsaveplusbalance());
            JSONObject response_object = object.getJSONObject("response");

            JSONObject total = response_object.getJSONObject("total");
            String wallet_credit = total.getString("wallet_credit");
            String wallet_spend = total.getString("wallet_spend");

            moneyin.setText(session.getsaveplusidd() + " " + wallet_credit);
            moneyout.setText(session.getsaveplusidd() + " " + wallet_spend);


            ImageView closedialog = (ImageView)view.findViewById(R.id.closedialog);
            closedialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            viewdetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();

                    HashMap<String, String> getpatterns = session.getpattern();
                    String patterns = getpatterns.get(SessionManager.KEY_PATTERN);
                    Intent intents;
                    if (!patterns.equalsIgnoreCase(""))
                    {
                        updatepattern(Iconstant.getpattern);
                    } else {
                        ArrayList<String> ad = new ArrayList<String>();
                        ad.add("wallconfisss");
                        EventBus.getDefault().post(ad);
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return view;
    }


    private void updatepattern(String Url) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> info = session.getUserDetails();
        String UserID = info.get(SessionManager.KEY_USERID);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);


        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        JSONObject jsonObject = object.getJSONObject("response");
                        if (jsonObject.has("pattern_code")) {
                            session.setpattern(jsonObject.getString("pattern_code"));
                            if(!jsonObject.getString("pattern_code").equals(""))
                            {
                                ArrayList<String> ad = new ArrayList<String>();
                                ad.add("wallet");
                                ad.add(codepath);
                                ad.add(saveplusida);
                                EventBus.getDefault().post(ad);
                            }
                            else
                            {
                                ArrayList<String> ad = new ArrayList<String>();
                                ad.add("wallconfi");
                                EventBus.getDefault().post(ad);
                            }
                        }
                        else
                        {
                            ArrayList<String> ad = new ArrayList<String>();
                            ad.add("wallconfi");
                            EventBus.getDefault().post(ad);


                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }
    public interface Communicator {
        public void message(String data);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}