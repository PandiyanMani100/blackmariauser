package com.blackmaria;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;

/**
 * Created by Prem Kumar and Anitha on 11/6/2015.
 */
public class PushNotificationAlert_new extends Activity {
    TextView Tv_ok, Tv_title;
    RelativeLayout Rl_ok;
    TextView message;
    private String Str_message, Str_action, SrideId_intent, IsNormalOrreturnMulti = "", FreeWaitingTime = "";
    private SessionManager sessionManager;
    String currency_code = "", Amount = "";
    String user_id = "", paymenttype = "", payment_via = "", from_number = "", from = "", date = "", time = "", trans_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pushnotification_alert_new);
        initialize();

        Tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//|| Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCompleted_Key)
                if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCancelled_Key) || Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCompleted_Key) || Str_action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key)) {
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction("com.pushnotification.updateBottom_view");
                    broadcastIntent.putExtra("rideStatus", Str_action);
                    broadcastIntent.putExtra("ride_id", SrideId_intent);
                    sendBroadcast(broadcastIntent);

                }

                if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_PaymentPaid_Key)) {
                    System.out.println("--------------------------------------niramjan------------------alert----------------payment paid");
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction("com.pushnotification.updateBottom_view");
                    broadcastIntent.putExtra("rideStatus", Iconstant.PushNotification_PaymentPaid_Key);
                    broadcastIntent.putExtra("ride_id", SrideId_intent);
                    broadcastIntent.putExtra("payment", "success");
                    sendBroadcast(broadcastIntent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }


                if (Str_action.equalsIgnoreCase(Iconstant.wallet_success)) {


//                    Intent intent = new Intent(PushNotificationAlert_new.this, Reload_Transfer_PushNotification.class);
//                    intent.putExtra("Str_action", Str_action);
//                    intent.putExtra("user_id", user_id);
//                    intent.putExtra("Amount", Amount);
//                    intent.putExtra("Currencycode", currency_code);
//                    intent.putExtra("paymenttype", paymenttype);
//                    intent.putExtra("payment_via", payment_via);
//                    intent.putExtra("from_number", from_number);
//                    intent.putExtra("from", from);
//                    intent.putExtra("date", date);
//                    intent.putExtra("time", time);
//                    intent.putExtra("trans_id", trans_id);
//
//                    startActivity(intent);
//                    finish();

//                    Intent intent = new Intent(PushNotificationAlert_new.this, Navigation_new.class);
//                    intent.putExtra("Str_action",Str_action);
//                    intent.putExtra("user_id", user_id);
//                    intent.putExtra("Amount", Amount);
//                    intent.putExtra("Currencycode", currency_code);
//                    intent.putExtra("paymenttype", paymenttype);
//                    intent.putExtra("payment_via", payment_via);
//                    intent.putExtra("from_number", from_number);
//                    intent.putExtra("from", from);
//                    intent.putExtra("date", date);
//                    intent.putExtra("time", time);
//                    intent.putExtra("trans_id", trans_id);
//                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    //         overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


                }
                if (Str_action.equalsIgnoreCase(Iconstant.bank_success)) {

                    Intent intent = new Intent(PushNotificationAlert_new.this, Navigation_new.class);
//                    intent.putExtra("Str_action",Str_action);
//                    intent.putExtra("user_id", user_id);
//                    intent.putExtra("Amount", Amount);
//                    intent.putExtra("Currencycode", currency_code);
//                    intent.putExtra("paymenttype", paymenttype);
//                    intent.putExtra("payment_via", payment_via);
//                    intent.putExtra("from_number", from_number);
//                    intent.putExtra("from", from);
//                    intent.putExtra("date", date);
//                    intent.putExtra("time", time);
//                    intent.putExtra("trans_id", trans_id);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                    Intent intent = new Intent(PushNotificationAlert_new.this, Reload_Transfer_PushNotification.class);
//                    intent.putExtra("Str_action", Str_action);
//                    intent.putExtra("user_id", user_id);
//                    intent.putExtra("Amount", Amount);
//                    intent.putExtra("Currencycode", currency_code);
//                    intent.putExtra("paymenttype", paymenttype);
//                    intent.putExtra("payment_via", payment_via);
//                    intent.putExtra("from_number", from_number);
//                    intent.putExtra("from", from);
//                    intent.putExtra("date", date);
//                    intent.putExtra("time", time);
//                    intent.putExtra("trans_id", trans_id);
//                    startActivity(intent);
//                    finish();
                    //   overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//
                }
                if (Str_action.equalsIgnoreCase(Iconstant.wallet_credit)) {

//                    Intent intent = new Intent(PushNotificationAlert_new.this, Navigation_new.class);
//                    intent.putExtra("Str_action",Str_action);
//                    intent.putExtra("user_id", user_id);
//                    intent.putExtra("Amount", Amount);
//                    intent.putExtra("Currencycode", currency_code);
//                    intent.putExtra("paymenttype", paymenttype);
//                    intent.putExtra("payment_via", payment_via);
//                    intent.putExtra("from_number", from_number);
//                    intent.putExtra("from", from);
//                    intent.putExtra("date", date);
//                    intent.putExtra("time", time);
//                    intent.putExtra("trans_id", trans_id);
//                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }


                if (Str_action.equalsIgnoreCase(Iconstant.complementry_time_closed) || Str_action.equalsIgnoreCase(Iconstant.Drop_Waiting_time_closed_key)) {
                    sessionManager.setWaitedTime(0, 0, "", "");
                    Intent intent = new Intent(PushNotificationAlert_new.this, TrackRidePage.class);
                    intent.putExtra("ride_id", SrideId_intent);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                } else if (Str_action.equalsIgnoreCase(Iconstant.referalcredit_amount)) {
                    Intent intent = new Intent(PushNotificationAlert_new.this, CloudMoneyHomePage.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                } else {
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }

            }
        });
    }

    private void initialize() {
        sessionManager = new SessionManager(PushNotificationAlert_new.this);
        Tv_ok = (TextView) findViewById(R.id.pushnotification_alert_ok_textview);
        Tv_title = (TextView) findViewById(R.id.pushnotification_alert_messge_label);
        message = (TextView) findViewById(R.id.pushnotification_alert_messge_textview);
//        Rl_ok = (RelativeLayout) findViewById(R.id.pushnotification_alert_ok_layout);

        Intent intent = getIntent();
        Str_message = intent.getStringExtra("message");
        Str_action = intent.getStringExtra("Action");
        if (getIntent().getExtras().containsKey("RideID")) {
            SrideId_intent = intent.getStringExtra("RideID");
        }


        if (intent.hasExtra("Currencycode")) {
            currency_code = intent.getStringExtra("Currencycode");
        }
        if (intent.hasExtra("Amount")) {
            Amount = intent.getStringExtra("Amount");
        }


        if (getIntent().getExtras().containsKey("FreeWaitingTime")) {
            FreeWaitingTime = intent.getStringExtra("FreeWaitingTime");
        }
        if (getIntent().getExtras().containsKey("IsNormalOrreturnMulti")) {
            IsNormalOrreturnMulti = intent.getStringExtra("IsNormalOrreturnMulti");
        }

        if (Str_action.equalsIgnoreCase(Iconstant.wallet_credit) || Str_action.equalsIgnoreCase(Iconstant.wallet_success) || Str_action.equalsIgnoreCase(Iconstant.bank_success)) {

            user_id = intent.getStringExtra("user_id");
            paymenttype = intent.getStringExtra("paymenttype");
            payment_via = intent.getStringExtra("payment_via");
            from_number = intent.getStringExtra("from_number");
            from = intent.getStringExtra("from");
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            trans_id = intent.getStringExtra("trans_id");
        }

        message.setText(Str_message);

        if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCancelled_Key)) {
            //  Tv_title.setText(getResources().getString(R.string.pushnotification_alert_label_ride_cancelled_sorry));
            System.out.println("pushnotification");
        } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_CabArrived_Key)) {
            // Tv_title.setText(getResources().getString(R.string.pushnotification_alert_label));
        } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCompleted_Key) || Str_action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key)) {
            // Tv_title.setText(getResources().getString(R.string.pushnotification_alert_label_ride_completed_thanks));
        } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_PaymentPaid_Key)) {
            // Tv_title.setText(getResources().getString(R.string.pushnotification_alert_label_ride_arrived_success));
        } else if (Str_action.equalsIgnoreCase(Iconstant.complementry_time_closed)) {
            // Tv_title.setText(getResources().getString(R.string.pushnotification_alert_label_ride_arrived_success));
        }

    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }
}
