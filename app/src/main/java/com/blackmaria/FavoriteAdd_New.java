package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.adapter.PlaceSearchAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.InterFace.CallBack;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.PkDialogtryagain;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

import static android.view.View.GONE;

/**
 * Created by user144 on 1/2/2018.
 */

@SuppressLint("Registered")
public class FavoriteAdd_New extends ActivityHockeyApp implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "";
    private String SselectedAddress = "", Slatitude = "", Slongitude = "", Stitle = "", SlocationKey = "", SidentityKey = "";

    private ImageView Rl_back /*Rl_find_address*/;
    private Button Rl_save;
    //    private EditText Et_name;
    private EditText Tv_address;
    private ImageView currentLocation_image;
    private ServiceRequest mRequest, editRequest;
    private GoogleMap googleMap;
    GPSTracker gps;
    static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
    Dialog dialog;

    //    private RelativeLayout Rl_alert;
//    private TextView Tv_alert;
    private boolean isAddressAvailable = false;
    LanguageDb mhelper;
    private RefreshReceiver refreshReceiver;
    private String sPage = "";
    String strAdd = "";
    String address = "";
    //-----Declaration For Enabling Gps-------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 299;

    private String sLatitude = "";
    private String sLongitude = "";

    EditText Et_Did_location_name;
    MapFragment mapFragment;
    public static final int FavoriteRequestCode = 6666;
    private TextView book_my_ride_set_destination_textview;
    private boolean dropclick = true;
    private SmoothProgressBar loading_spinner;
    private ListView listview;
    ArrayList<String> itemList_location = new ArrayList<String>();
    ArrayList<String> itemList_placeId = new ArrayList<String>();
    private boolean isdataAvailable = false;
    private PlaceSearchAdapter PlaceSearchAdapter;
    private String Sselected_location = "", placeId = "";
    private String Slocation = "";

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {

                        if (getkey("profile_label_menu").equalsIgnoreCase(sPage)) {
                            FavoriteList.favoriteList_class.finish();
                        } else {
                            FavoriteList.favoriteList_class.finish();
                            BookingPage2.BookingPage2_class.finish();
                        }

                        Intent intent1 = new Intent(FavoriteAdd_New.this, MyRideRating.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        if (getkey("profile_label_menu").equalsIgnoreCase(sPage)) {
                            FavoriteList.favoriteList_class.finish();
                        } else {
                            FavoriteList.favoriteList_class.finish();
                            BookingPage2.BookingPage2_class.finish();
                        }
                        Intent intent1 = new Intent(FavoriteAdd_New.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favourite_add_new_constrain);
        mhelper = new LanguageDb(this);
        initialize();
        initializeMap();

        Rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Rl_back.getWindowToken(), 0);

                onBackPressed();
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });

        currentLocation_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(FavoriteAdd_New.this);
                isInternetPresent = cd.isConnectingToInternet();
                gps = new GPSTracker(FavoriteAdd_New.this);

                if (gps.isgpsenabled() && gps.canGetLocation()) {

                    double Dlatitude = gps.getLatitude();
                    double Dlongitude = gps.getLongitude();

                    // Move the camera to last position with a zoom level
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                } else {
                    Toast.makeText(FavoriteAdd_New.this, getkey("gpsnotenable"), Toast.LENGTH_LONG).show();
                }
            }
        });

        Rl_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Slocation.length() > 0 && !Slocation.equalsIgnoreCase(getkey("favorite_add_label_gettingAddress"))) {

                    SaveFavoriteLocationDialog();

                } else {

                    Alert(getkey("alert_label_title"), getkey("favorite_add_label_invalid_address"));

                }


            }
        });


//        Et_name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
//                    CloseKeyboard(Et_name);
//                }
//                return false;
//            }
//        });


        book_my_ride_set_destination_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tv_address.requestFocus();
                InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(Tv_address, 0);
                Tv_address.setText(book_my_ride_set_destination_textview.getText().toString());
                book_my_ride_set_destination_textview.setVisibility(GONE);
                Tv_address.setVisibility(View.VISIBLE);
                Tv_address.setFocusable(true);

            }
        });
        Tv_address.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (Tv_address.getText().toString().trim().length() == 0) {
                        book_my_ride_set_destination_textview.setText("");
                        Slocation = "";
                        System.out.println("================Muruga fav address empty=============");
                    }
                }
                return false;
            }
        });
/* Tv_address.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (Tv_address.getText().toString().trim().length() == 0) {
                        book_my_ride_set_destination_textview.setText("");
                        Slocation = "";
                        System.out.println("================Muruga fav address empty=============");
                    }
                }
                return false;
            }
        });*/
        Tv_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                System.out.println("------------------dropclick------------------" + dropclick);
                cd = new ConnectionDetector(FavoriteAdd_New.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }
                    String data = Tv_address.getText().toString().toLowerCase().replace("%", "").replace(" ", "%20").trim();
                    CitySearchRequest(Iconstant.place_search_url + data);
                } else {

                }

            }
        });


        Tv_address.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(Tv_address, 0);
            }
        }, 200);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dropclick = false;
                Sselected_location = itemList_location.get(position);

                cd = new ConnectionDetector(FavoriteAdd_New.this);
                isInternetPresent = cd.isConnectingToInternet();
                listview.setVisibility(GONE);
                CloseKeyboard(Tv_address);
                if (isInternetPresent) {
                    placeId = itemList_placeId.get(position);
                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId.get(position), "1");
                } else {

                }

            }
        });


    }

    private void initialize() {
        session = new SessionManager(FavoriteAdd_New.this);
        cd = new ConnectionDetector(FavoriteAdd_New.this);
        isInternetPresent = cd.isConnectingToInternet();
        gps = new GPSTracker(FavoriteAdd_New.this);

        Rl_back = findViewById(R.id.favorite_add_header_back_layout);
        Rl_save = (Button) findViewById(R.id.favorite_add_header_save_layout);


        ImageView clear = (ImageView) findViewById(R.id.clear);



        Rl_save.setText(getkey("save"));
//        Et_name = (EditText) findViewById(R.id.favorite_add_name_edittext);
        Tv_address = (EditText) findViewById(R.id.location_search_editText);
        Tv_address.setHint(getkey("enter_your_favourite_places"));
//        Rl_alert = (RelativeLayout) findViewById(R.id.favorite_add_alert_layout);
//        Tv_alert = (TextView) findViewById(R.id.favorite_add_alert_textView);
        currentLocation_image = (ImageView) findViewById(R.id.favorite_add_current_location_imageview);

        book_my_ride_set_destination_textview = (TextView) findViewById(R.id.book_my_ride_set_destination_textview1);
        loading_spinner = (SmoothProgressBar) findViewById(R.id.timerpage_loading_progressbar);
        listview = (ListView) findViewById(R.id.location_search_listView_booking);

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tv_address.setText("");
                book_my_ride_set_destination_textview.setText("");
            }
        });

        book_my_ride_set_destination_textview.setHint(getkey("enter_your_favourite_places"));
       TextView title = (TextView) findViewById(R.id.title);
        title.setText(getkey("add_places"));

        TextView myreferalcode = (TextView) findViewById(R.id.myreferalcode);
        myreferalcode.setText(getkey("favorites"));

        //    Rl_find_address = (RelativeLayout) findViewById(R.id.find_address_btn);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);


        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        Intent intent = getIntent();
        SselectedAddress = intent.getStringExtra("Intent_Address");
        Slatitude = intent.getStringExtra("Intent_Latitude");
        Slongitude = intent.getStringExtra("Intent_Longitude");
        SidentityKey = intent.getStringExtra("Intent_IdentityKey");
        if (SidentityKey.equalsIgnoreCase("Edit")) {
            Stitle = intent.getStringExtra("Intent_Title");
            SlocationKey = intent.getStringExtra("Intent_LocationKey");

//            Et_name.setText(Stitle);
        }
        if (intent.hasExtra("str_page")) {
            sPage = intent.getStringExtra("str_page");
        }

    }

    private void initializeMap() {
        if (googleMap == null) {
            //googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.book_my_ride3_mapview)).getMap();
            mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.favorite_add_mapview));
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap arg) {
                    loadMap(arg);
                }
            });
        }


    }

    public void loadMap(GoogleMap arg) {
        googleMap = arg;
        if (CheckPlayService()) {
            // Changing map type
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            // Showing / hiding your current location
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            // Enable / Disable zooming controls
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            // Enable / Disable my location button
            // Enable / Disable Compass icon
            googleMap.getUiSettings().setCompassEnabled(false);
            // Enable / Disable Rotate gesture
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);

            if (getkey("profile_label_menu").equalsIgnoreCase(sPage) || "booking3".equalsIgnoreCase(sPage)) {
                if (gps.canGetLocation() && gps.isgpsenabled()) {
                    double Dlatitude = gps.getLatitude();
                    double Dlongitude = gps.getLongitude();

                    // Move the camera to last position with a zoom level
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                } else {
                    enableGpsService();
                }
            } else {

                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(Slatitude), Double.parseDouble(Slongitude))).zoom(17).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }


            //New Chsnges

            GoogleMap.OnCameraChangeListener mOnCameraChangeListener = new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    double latitude = cameraPosition.target.latitude;
                    double longitude = cameraPosition.target.longitude;

                    cd = new ConnectionDetector(FavoriteAdd_New.this);
                    isInternetPresent = cd.isConnectingToInternet();

                    Log.e("latitude--on_camera_change---->", "" + latitude);
                    Log.e("longitude--on_camera_change---->", "" + longitude);

                    if (latitude != 0.0) {
                        googleMap.clear();

                        Slatitude = String.valueOf(latitude);
                        Slongitude = String.valueOf(longitude);

                        if (isInternetPresent) {

                            if (Slatitude != null && Slongitude != null) {
                                if (!placeId.equalsIgnoreCase("")) {
                                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + placeId, "2");
                                } else {
                                    FavoriteAdd_New.GetCompleteAddressAsyncTask asyncTask = new FavoriteAdd_New.GetCompleteAddressAsyncTask();
                                    asyncTask.execute();
                                }
                            } else {  }

                        } else {

                        }
                    }
                }
            };
            if (CheckPlayService()) {
                if (googleMap != null) {
                    googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            String tittle = marker.getTitle();

                            Log.e("tittle--on_camera_change---->", "" + tittle);

                            return true;
                        }
                    });
                }
            } else {
                Alert(getkey("alert_label_title"),  getkey("install_googleplay_view_location"));
            }


        } else {

            final PkDialog mDialog = new PkDialog(FavoriteAdd_New.this);
            mDialog.setDialogTitle(getkey("alert_label_title"));
            mDialog.setDialogMessage(getkey("action_unable_to_create_map"));
            mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();
        }
    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(FavoriteAdd_New.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-----------Check Google Play Service--------
    private boolean CheckPlayService() {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(FavoriteAdd_New.this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        }
        return true;
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        runOnUiThread(new Runnable() {
            public void run() {
                final Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionStatusCode, FavoriteAdd_New.this, REQUEST_CODE_RECOVER_PLAY_SERVICES);
                if (dialog == null) {
                    Toast.makeText(FavoriteAdd_New.this,  getkey("incompitable"), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    //-------------AsyncTask to get Complete Address------------
    public class GetCompleteAddressAsyncTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Tv_address.setVisibility(GONE);
            Tv_address.setText(getkey("favorite_add_label_gettingAddress"));
            book_my_ride_set_destination_textview.setText(getkey("favorite_add_label_gettingAddress"));
        }

        @Override
        protected String doInBackground(Void... params) {
            //address = new GeocoderHelper().fetchCityName(FavoriteAdd_New.this, Double.parseDouble(Slatitude), Double.parseDouble(Slongitude), callBack);////getCompleteAddressString(dLatitude, dLongitude);
            //  Tv_address.setFocusable(false);

            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(FavoriteAdd_New.this, Locale.getDefault());

                addresses = geocoder.getFromLocation(Double.parseDouble(Slatitude), Double.parseDouble(Slongitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                String addresss = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                strAdd = addresss;
                address = addresss;
            } catch (IOException ex) {
                ex.printStackTrace();
                Log.e("My Current loction address", "Canont get Address!");
                isAddressAvailable = false;
            }

            if (!strAdd.equalsIgnoreCase("")) {
                isAddressAvailable = true;
            }

            return address;
        }

        @Override
        protected void onPostExecute(String address) {
            //      Tv_address.setFocusable(false);
            if (isAddressAvailable) {
//                Rl_alert.setVisibility(View.GONE);
                book_my_ride_set_destination_textview.setVisibility(View.VISIBLE);
                book_my_ride_set_destination_textview.setText(strAdd);
//                Tv_address.setVisibility(View.GONE);
                Slocation = address;
            } else {
//                Rl_alert.setVisibility(View.VISIBLE);

                book_my_ride_set_destination_textview.setVisibility(View.VISIBLE);
                book_my_ride_set_destination_textview.setText("");
//                Tv_address.setVisibility(View.GONE);

            }
        }
    }

    CallBack callBack = new CallBack() {
        @Override
        public void onComplete(String LocationName) {
            strAdd = LocationName;
            System.out.println("-------------------addreess--------------favourite" + LocationName);
        }

        @Override
        public void onError(String errorMsg) {
        }

        @Override
        public void onCompleteAddress(String message) {

        }
    };


    //-----------------------Favourite Save Post Request-----------------
    private void postRequest_FavoriteSave(String Url) {
        dialog = new Dialog(FavoriteAdd_New.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_saving"));


        System.out.println("-------------Favourite Save Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("title", Et_Did_location_name.getText().toString());
        jsonParams.put("latitude", Slatitude);
        jsonParams.put("longitude", Slongitude);
        jsonParams.put("address", Slocation);

        System.out.println("-------------Favourite Save jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(FavoriteAdd_New.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Favourite Save Response----------------" + response);
                String Sstatus = "", Smessage = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");
//
//                    // close keyboard
//                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                    mgr.hideSoftInputFromWindow(Et_name.getWindowToken(), 0);

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Intent local = new Intent();
                        local.setAction("com.favoriteList.refresh");
                        sendBroadcast(local);

                        final PkDialog mDialog = new PkDialog(FavoriteAdd_New.this);
                        mDialog.setDialogTitle(getkey("action_success"));
                        mDialog.setDialogMessage(getkey("ad_fvt_loc"));
                        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                finish();
                                overridePendingTransition(R.anim.enter, R.anim.exit);
                            }
                        });
                        mDialog.show();

                    } else {
                        Alert(getkey("alert_label_title"), Smessage);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //-----------------------Favourite List Edit Post Request-----------------
//    private void postRequest_FavoriteEdit(String Url) {
//        dialog = new Dialog(FavoriteAdd_New.this);
//        dialog.getWindow();
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.custom_loading);
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.show();
//
//        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);

//
//        System.out.println("-------------Favourite Edit Url----------------" + Url);
//
//        HashMap<String, String> jsonParams = new HashMap<String, String>();
//        jsonParams.put("user_id", UserID);
//        jsonParams.put("title", Et_name.getText().toString());
//        jsonParams.put("latitude", Slatitude);
//        jsonParams.put("longitude", Slongitude);
//        jsonParams.put("address", Tv_address.getText().toString());
//        jsonParams.put("location_key", SlocationKey);
//
//        editRequest = new ServiceRequest(FavoriteAdd_New.this);
//        editRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
//            @Override
//            public void onCompleteListener(String response) {
//
//                System.out.println("-------------Favourite Edit Response----------------" + response);
//
//                String Sstatus = "", Smessage = "";
//
//                try {
//                    JSONObject object = new JSONObject(response);
//                    Sstatus = object.getString("status");
//                    Smessage = object.getString("message");
//
//                    // close keyboard
//                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                    mgr.hideSoftInputFromWindow(Et_name.getWindowToken(), 0);
//
//                    if (Sstatus.equalsIgnoreCase("1")) {
//                        Intent local = new Intent();
//                        local.setAction("com.favoriteList.refresh");
//                        sendBroadcast(local);
//
//                        final PkDialog mDialog = new PkDialog(FavoriteAdd_New.this);
//
//                        mDialog.setDialogMessage(Smessage);
//
//                    } else {
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                dialog.dismiss();
//            }
//
//            @Override
//            public void onErrorListener() {
//                dialog.dismiss();
//            }
//        });
//    }


    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(FavoriteAdd_New.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(FavoriteAdd_New.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            // close keyboard
            InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            mgr.hideSoftInputFromWindow(Rl_back.getWindowToken(), 0);

            onBackPressed();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }


    public void SaveFavoriteLocationDialog() {


        final Dialog dialog = new Dialog(FavoriteAdd_New.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.save_favourite_dialog_constrain);

        final TextView Tv_Dig_saveLoaction_address = dialog.findViewById(R.id.save_fav_location_address);
        final TextView Btn_Dig_save = dialog.findViewById(R.id.save_fav_bottom_layout);
        final ImageView iv_close = (ImageView) dialog.findViewById(R.id.image_close);
        Et_Did_location_name = dialog.findViewById(R.id.fav_loc_name);

        final TextView myreferalcode = dialog.findViewById(R.id.myreferalcode);
        myreferalcode.setText(getkey("favourites"));
        final TextView fav_location_name_lable = dialog.findViewById(R.id.fav_location_name_lable);
        fav_location_name_lable.setText(getkey("enter_place_name"));
        Et_Did_location_name.setHint(getkey("eg_cafe"));
        Btn_Dig_save.setText(getkey("add_favourites"));




        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Tv_Dig_saveLoaction_address.setText(Slocation);


        Btn_Dig_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Btn_Dig_save.getWindowToken(), 0);

                cd = new ConnectionDetector(FavoriteAdd_New.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {

                    if (!Et_Did_location_name.getText().toString().equalsIgnoreCase("")) {
                        postRequest_FavoriteSave(Iconstant.favoritelist_add_url);
                    } else {
                        erroredit(Et_Did_location_name, getkey("profile_label_alert_favelocartionname"));
                    }
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }

            }
        });


        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // remove background dim
        dialog.getWindow().setDimAmount(0);

        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == RESULT_OK) {
            if (requestCode == FavoriteRequestCode) {
                String sAddress = data.getStringExtra("Selected_Location");
                sLatitude = data.getStringExtra("Selected_Latitude");
                sLongitude = data.getStringExtra("Selected_Longitude");
                Tv_address.setText(sAddress);
                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(sLatitude), Double.parseDouble(sLongitude))).zoom(17).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            }
        }
    }


    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(FavoriteAdd_New.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    private void LatLongRequest(String Url, final String flag) {

        dialog = new Dialog(FavoriteAdd_New.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("--------------LatLong url-------------------" + Url);

        mRequest = new ServiceRequest(FavoriteAdd_New.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------LatLong  reponse-------------------" + response);
                String status = "", SLatitude1 = "", SLongitude1 = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONObject place_object = object.getJSONObject("result");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_object.length() > 0) {
                                JSONObject geometry_object = place_object.getJSONObject("geometry");
                                if (geometry_object.length() > 0) {
                                    JSONObject location_object = geometry_object.getJSONObject("location");
                                    if (location_object.length() > 0) {
                                        SLatitude1 = location_object.getString("lat");
                                        SLongitude1 = location_object.getString("lng");
                                        isdataAvailable = true;
                                    } else {
                                        isdataAvailable = false;
                                    }
                                } else {
                                    isdataAvailable = false;
                                }
                            } else {
                                isdataAvailable = false;
                            }
                        } else {
                            isdataAvailable = false;
                        }
                    }

                    if (isdataAvailable) {


                        Slocation = Sselected_location;
                        Slatitude = SLatitude1;
                        Slongitude = SLongitude1;


                        System.out.println("=========set value1=================");
                        book_my_ride_set_destination_textview.setText(Slocation);
                        Tv_address.setText("");
                        book_my_ride_set_destination_textview.setVisibility(View.VISIBLE);
                        Tv_address.setVisibility(View.GONE);
                        if (flag.equalsIgnoreCase("1")) {
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(Slatitude), Double.parseDouble(Slongitude))).zoom(17).build();
                            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            listview.setVisibility(GONE);
                        } else if (flag.equalsIgnoreCase("2")) {
                            placeId = "";
                        }


                        dialog.dismiss();
                    } else {
                        dialog.dismiss();

                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-------------------Search Place Request----------------
    private void CitySearchRequest(String Url) {
        listview.setVisibility(View.VISIBLE);
        loading_spinner.setVisibility(View.VISIBLE);
        System.out.println("--------------Search city url-------------------" + Url);

        mRequest = new ServiceRequest(FavoriteAdd_New.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Search city  reponse-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONArray place_array = object.getJSONArray("predictions");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_array.length() > 0) {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                for (int i = 0; i < place_array.length(); i++) {
                                    JSONObject place_object = place_array.getJSONObject(i);
                                    itemList_location.add(place_object.getString("description"));
                                    itemList_placeId.add(place_object.getString("place_id"));
                                }
                                isdataAvailable = true;
                            } else {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                isdataAvailable = false;
                            }
                        } else {
                            itemList_location.clear();
                            itemList_placeId.clear();
                            isdataAvailable = false;
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                loading_spinner.setVisibility(View.INVISIBLE);

                PlaceSearchAdapter = new PlaceSearchAdapter(FavoriteAdd_New.this, itemList_location);
                listview.setAdapter(PlaceSearchAdapter);
                PlaceSearchAdapter.notifyDataSetChanged();
                if(itemList_location.size()==0){
                    listview.setVisibility(GONE);
                }
            }

            @Override
            public void onErrorListener() {
                loading_spinner.setVisibility(View.INVISIBLE);

            }
        });

    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}

