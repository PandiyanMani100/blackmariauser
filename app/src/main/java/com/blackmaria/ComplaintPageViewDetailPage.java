package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.ComplaintPageViewDetailAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.ViewComplaintPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 3/9/2018.
 */

@SuppressLint("Registered")
public class ComplaintPageViewDetailPage extends ActivityHockeyApp implements ComplaintPageViewDetailAdapter.ReplyCloseInterface {
    private CustomTextView tv_reply;
    private ImageView back;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "";
    private ServiceRequest mRequest;
    LanguageDb mhelper;
    private Dialog dialog;
    private ListView listview;
    private ArrayList<ViewComplaintPojo> itemlist;
    private ComplaintPageViewDetailAdapter adapter;
    private boolean isCommentAvailable = false;
    private String Sticket_no = "", Sticket_date = "", Ssubject = "", Sbooking_no = "", Sticket_status = "";
    private String SticketId = "";
    private TextView ticket_status, tv_ticket_no, ticketdate, complaint_sub;
    //tv_you_wrote,tv_blackmaria_wrote

    private RefreshReceiver refreshReceiver;
    private Dialog success_dialog;
    private String phone_number = "";
    final int PERMISSION_REQUEST_CODE = 111;
    final int PERMISSION_REQUEST_CODES = 222;
    private TextView callDriver;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(ComplaintPageViewDetailPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(ComplaintPageViewDetailPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            } else if (intent.getAction().equals("com.package.ACTION_CLASS_complaint_replay_notification")) {
                if (intent.getExtras() != null) {
                    String ticketId = (String) intent.getExtras().get("ticket_id");
                    String userID = (String) intent.getExtras().get("user_id");
                    String message = (String) intent.getExtras().get("message");
                    String dateTime = (String) intent.getExtras().get("date_time");

                    if (ticketId.equalsIgnoreCase(SticketId)) {

                        ViewComplaintPojo pojo = new ViewComplaintPojo();
                        pojo.setComment_by("BLACKMARIA");
                        pojo.setComments(message);
                        pojo.setComment_date(dateTime);
                        pojo.settype("admin");
                        itemlist.add(pojo);
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }
                        listview.setSelection(listview.getAdapter().getCount()-1);

                    } else {

                        if (isInternetPresent) {
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("user_id", userID);
                            jsonParams.put("ticket_id", ticketId);
                            postRequest_viewTicket(Iconstant.view_ticket_url, jsonParams);
                        } else {
                            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                        }

                    }

                }

            }


        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complaint_reply_close_constrain);
        mhelper= new LanguageDb(this);
        initialize();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(ComplaintPageViewDetailPage.this, Complaint_Page_new.class);
                startActivity(intent);*/
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });

//        callDriver.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (phone_number != null) {
////                    HashMap<String, String> phone = session.getPhoneMaskingStatus();
//                    String sPhoneMaskingStatus = session.getPhoneMaskingStatus();
////                    sPhoneMaskingStatus = "No";
//                    if (sPhoneMaskingStatus.equalsIgnoreCase("Yes")) {
//                        postRequest_PhoneMasking(Iconstant.phoneMasking_url);
//                    } else {
//                        if (Build.VERSION.SDK_INT >= 23) {
//                            // Marshmallow+
//
//                                Intent callIntent = new Intent(Intent.ACTION_CALL);
//                                callIntent.setData(Uri.parse("tel:" + phone_number));
//                                startActivity(callIntent);
//
//                        } else {
//                            Intent callIntent = new Intent(Intent.ACTION_CALL);
//                            callIntent.setData(Uri.parse("tel:" + phone_number));
//                            startActivity(callIntent);
//                        }
//                    }
//
//                } else {
//                  }
//            }
//        });
//

    }

    private void initialize() {
        session = new SessionManager(ComplaintPageViewDetailPage.this);
        cd = new ConnectionDetector(ComplaintPageViewDetailPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemlist = new ArrayList<ViewComplaintPojo>();

        back = (ImageView) findViewById(R.id.complaintback);
        listview = (ListView) findViewById(R.id.view_complaint_page_comments_listview);
        tv_ticket_no = (TextView) findViewById(R.id.view_complaint_page_ticket_no_textview);
//        ticket_status = (TextView) findViewById(R.id.ticket_status);
        ticketdate = (TextView) findViewById(R.id.ticketdate);
        complaint_sub = (TextView) findViewById(R.id.complaint_sub);
//        callDriver = (TextView) findViewById(R.id.value_reporttext);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.package.ACTION_CLASS_complaint_replay_notification");
        registerReceiver(refreshReceiver, intentFilter);

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        HashMap<String, String> code = session.getcomplaintUserDetail();
        String uname = code.get(SessionManager.COMPLAINT_U_NAME);
        String uimage = code.get(SessionManager.COMPLAINT_U_IMAGE);
        String ulocation = code.get(SessionManager.COMPLAINT_U_LOCATION);

        Intent intent = getIntent();
        SticketId = intent.getStringExtra("ticket_id");

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ticket_id", SticketId);

        if (isInternetPresent) {
            postRequest_viewTicket(Iconstant.view_ticket_url, jsonParams);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }
    }

    private void postRequest_PhoneMasking(String Url) {

        dialog = new Dialog(ComplaintPageViewDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Sbooking_no);
        jsonParams.put("user_type", "user");

        System.out.println("-------------PhoneMasking Url----------------" + Url);
        mRequest = new ServiceRequest(ComplaintPageViewDetailPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------PhoneMasking Response----------------" + response);

                String Str_status = "", SResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    SResponse = object.getString("response");
                    if (Str_status.equalsIgnoreCase("1")) {
                        Alert(getkey("action_success"), SResponse);
                    } else {
                        Alert(getkey("action_error"), SResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-----------------------viewTicket Post Request-----------------
    private void postRequest_viewTicket(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(ComplaintPageViewDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));
        System.out.println("-------------viewTicket Url----------------" + Url);

        System.out.println("-------------viewTicket jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(ComplaintPageViewDetailPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------viewTicket Response----------------" + response);
                String Sstatus = "", driver_image = "", user_image = "";
                ;
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {


                            JSONObject details_object = response_object.getJSONObject("details");
                            if (details_object.length() > 0) {
                                Sticket_no = details_object.getString("ticket_no");
                                Sticket_date = details_object.getString("ticket_date");
                                Ssubject = details_object.getString("subject");
                                Sbooking_no = details_object.getString("booking_no");
//                                Sticket_status = details_object.getString("ticket_status");

                                driver_image = details_object.getString("driver_image");
                                user_image = details_object.getString("user_image");
                                phone_number = details_object.getString("phone_number");


                                JSONArray comment_by_array = details_object.getJSONArray("comments");
                                if (comment_by_array.length() > 0) {
                                    itemlist.clear();
                                    for (int i = 0; i < comment_by_array.length(); i++) {
                                        JSONObject comment_by_object = comment_by_array.getJSONObject(i);
                                        ViewComplaintPojo pojo = new ViewComplaintPojo();
                                        pojo.setComment_by(comment_by_object.getString("comment_by"));
                                        pojo.setComments(comment_by_object.getString("comments"));
                                        pojo.setComment_date(comment_by_object.getString("comment_date"));
                                        pojo.settype(comment_by_object.getString("type"));
                                        itemlist.add(pojo);
                                    }
                                    isCommentAvailable = true;
                                } else {
                                    isCommentAvailable = false;
                                }

                            }

                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {
//                        ticket_status.setText(Sticket_status.toUpperCase());
                        tv_ticket_no.setText(getkey("ticket_no_lable")+ " :" + Sticket_no);
                        ticketdate.setText(getkey("createdat")+" " + Sticket_date);
                        complaint_sub.setText(getkey("subjects")+Ssubject.toUpperCase());
                        if (isCommentAvailable) {
                            adapter = new ComplaintPageViewDetailAdapter(ComplaintPageViewDetailPage.this, itemlist, ComplaintPageViewDetailPage.this, driver_image, user_image);
                            listview.setAdapter(adapter);
                            listview.setSelection(listview.getAdapter().getCount()-1);

                        }
                    }
                } catch (JSONException e) {
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //-----------------------postComment Post Request-----------------
    private void postRequest_postComment(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(ComplaintPageViewDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));
        System.out.println("-------------viewTicket Url----------------" + Url);
        System.out.println("-------------postComment jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(ComplaintPageViewDetailPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------postComment Response----------------" + response);
                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    String Sresponse = object.getString("message");
                    Alert1(getkey("action_success"), Sresponse);

                } catch (JSONException e) {
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-----------------------closeCase Post Request-----------------
    private void postRequest_closeCase(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(ComplaintPageViewDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));
        System.out.println("-------------closeCase Url----------------" + Url);
        System.out.println("-------------closeCase jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(ComplaintPageViewDetailPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------closeCase Response----------------" + response);
                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    String Sresponse = object.getString("message");


                    successDialog(SticketId, Sresponse);

                } catch (JSONException e) {
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void sendBroadcast() {


        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_CLASS_complaint_refreshhomePageComplaintlist");
        sendBroadcast(broadcastIntent);

    }

    private void successDialog(String ticket_no, String smessage) {

        success_dialog = new Dialog(ComplaintPageViewDetailPage.this);
        success_dialog.getWindow();
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        success_dialog.setContentView(R.layout.complaint_success_alert);
        success_dialog.setCanceledOnTouchOutside(true);
//        success_dialog.getWindow().getAttributes().windowAnimations =R.style.Animations_categories_filter;
        success_dialog.show();
        success_dialog.getWindow().setGravity(Gravity.CENTER);

        CustomTextView Tv_ticketNo = (CustomTextView) success_dialog.findViewById(R.id.complaint_alert_ticket_no_textview);
        CustomTextView Tv_message = (CustomTextView) success_dialog.findViewById(R.id.complaint_alert_success_message_textview);
        ImageView Tv_ok = (ImageView) success_dialog.findViewById(R.id.complaint_alert_ok_textview);
        ImageView closeInage = (ImageView) success_dialog.findViewById(R.id.image_close);
        Tv_ticketNo.setText(getkey("ticket_no") + ":" + ticket_no);
//        Tv_message.setText(smessage);

        closeInage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBroadcast();
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        Tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ComplaintPageViewDetailPage.this, Navigation_new.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });


    }


    private void ReplyComplaint() {

        final Dialog replyDialog = new Dialog(ComplaintPageViewDetailPage.this, R.style.DialogSlideAnim);
        replyDialog.getWindow();
        replyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        replyDialog.setContentView(R.layout.replycomplaint_popup);
        replyDialog.setCanceledOnTouchOutside(true);
        replyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        replyDialog.show();

        final EditText Et_message = (EditText) replyDialog.findViewById(R.id.view_complaint_page_replay_message_edittext);
        TextView Tv_ok = (TextView) replyDialog.findViewById(R.id.view_complaint_page_replay_message_send_imageview);
        ImageView closeInage = (ImageView) replyDialog.findViewById(R.id.close_im);


        Et_message.setHint(getkey("messaeg_lable"));
        Tv_ok.setText(getkey("send"));
        closeInage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replyDialog.dismiss();
            }
        });


        Tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                cd = new ConnectionDetector(ComplaintPageViewDetailPage.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {


                    String sMessage = Et_message.getText().toString();
                    if (sMessage != null && sMessage.length() > 0) {
                        try {

                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("user_id", UserID);//"5858fa30cae2aac80b000054");
                            jsonParams.put("ticket_id", SticketId);
                            jsonParams.put("comment", sMessage);

                            postRequest_postComment(Iconstant.post_comment_url, jsonParams);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        replyDialog.dismiss();
                        Et_message.getText().clear();
                    } else {
                        AlertError(getkey("alert_label_title"), getkey("pls_renter_msg"));
                    }
                } else {
                    AlertError(getkey("alert_label_title"), getkey("alert_nointernet"));
                }


            }
        });


    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(ComplaintPageViewDetailPage.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(ComplaintPageViewDetailPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onReplyToDriver() {
        ReplyComplaint();
    }

    @Override
    public void onCloseIssue() {
        cd = new ConnectionDetector(ComplaintPageViewDetailPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", UserID);
            jsonParams.put("ticket_id", Sticket_no);
            postRequest_closeCase(Iconstant.close_ticket_url, jsonParams);

        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }
    }

    //--------------Alert Method-----------
    private void Alert1(String title, String alert) {
        final PkDialog mDialog = new PkDialog(ComplaintPageViewDetailPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(ComplaintPageViewDetailPage.this, ComplaintPageViewDetailPage.class);
                intent.putExtra("ticket_id", Sticket_no);
                startActivity(intent);
                finish();


            }
        });
        mDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ComplaintPageViewDetailPage.this, Complaint_Page_new.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODES);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone_number));
                    startActivity(intent);
                }
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
