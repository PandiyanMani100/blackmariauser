package com.blackmaria;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.DowloadPdfActivity;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.widgets.PkDialog;

/**
 * Created by GANESH on 29-08-2017.
 */

public class HelpAndAssistance extends ActivityHockeyApp implements View.OnClickListener {

    private RelativeLayout RL_home, RL_booking, RL_earnings, RL_rulesOfRoad, RL_userGuides;
    private LinearLayout complaint_layout;
    private ConnectionDetector cd;
    private Dialog dialog;
    View includedLayout;
    private boolean isReceiversRegistered = false, isAlertShowing = false;
    DownloadManager downloadManager;
    private long myDownloadReference;
    private BroadcastReceiver receiverDownloadComplete, receiverNotificationClicked;
    DownloadManager.Request request;
    SessionManager session;
private String page="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_and_assistance);

        initialize();

    }

    private void initialize() {
        session = new SessionManager(HelpAndAssistance.this);
        cd = new ConnectionDetector(HelpAndAssistance.this);
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        RL_home = (RelativeLayout) findViewById(R.id.RL_home);
        RL_booking = (RelativeLayout) findViewById(R.id.RL_booking);
        RL_earnings = (RelativeLayout) findViewById(R.id.RL_earnings);
        RL_rulesOfRoad = (RelativeLayout) findViewById(R.id.RL_rules_of_road);
        RL_userGuides = (RelativeLayout) findViewById(R.id.RL_user_guides);
        complaint_layout = (LinearLayout) findViewById(R.id.complaint_layout);
page=getIntent().getStringExtra("pagecm");
        RL_home.setOnClickListener(this);
        RL_booking.setOnClickListener(this);
        RL_earnings.setOnClickListener(this);
        RL_rulesOfRoad.setOnClickListener(this);
        RL_userGuides.setOnClickListener(this);
        complaint_layout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == RL_home) {
                Intent newInt=null;
                newInt=new Intent(HelpAndAssistance.this,Navigation_new.class);
                /*if(page.equalsIgnoreCase("1")){
                    newInt=new Intent(HelpAndAssistance.this,Complaint_Page_new.class);
                }else{
                    newInt=new Intent(HelpAndAssistance.this,Navigation_new.class);
                }*/
                startActivity(newInt);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_booking) {
                String url = Iconstant.booking_url;
                ;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_earnings) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Iconstant.earning_url));
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_rulesOfRoad) {
                Intent rulesIntent = new Intent(HelpAndAssistance.this, RoadRulesActivity.class);
                startActivity(rulesIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            } else if (view == RL_userGuides) {
                String pdfUrl = session.getUserGuidePdf();
                if (pdfUrl.length() > 0) {
                    new DowloadPdfActivity(HelpAndAssistance.this, pdfUrl);
                   // downloadFile(pdfUrl);
                }
            } else if (view == complaint_layout) {
                Intent intent = new Intent(HelpAndAssistance.this, Complaint_Page_new.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit, R.anim.enter);
                finish();
            }
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(HelpAndAssistance.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void downloadFile(String docLink) {
        if (cd.isConnectingToInternet()) {
            Uri uri = Uri.parse(docLink);
//            Calendar cal = Calendar.getInstance();
//            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            String docName = "IncomeStatement_" + System.currentTimeMillis();
            request = new DownloadManager.Request(uri);
            /*MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(docLink));
            System.out.println("MimeString = "+mimeString);
            request.setMimeType(mimeString);*/
            request.setMimeType("application/pdf");
            request.setDescription("Downloading" + " " + docName);
            request.setTitle(docName);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, docName);
            request.setVisibleInDownloadsUi(true);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
//            request.setShowRunningNotification(true);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
            myDownloadReference = downloadManager.enqueue(request);
            Toast.makeText(HelpAndAssistance.this, getString(R.string.downloading), Toast.LENGTH_SHORT).show();
            IntentFilter filter = new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED);
            receiverNotificationClicked = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String extraId = DownloadManager.EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS;
                    long references[] = intent.getLongArrayExtra(extraId);
                    for (long reference : references) {
                        // do something with the download file
                    }
                }
            };
            IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
            receiverDownloadComplete = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                    if (myDownloadReference == reference) {
                        DownloadManager.Query query = new DownloadManager.Query();
                        query.setFilterById(reference);
                        Cursor cursor = downloadManager.query(query);
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                        int status = cursor.getInt(columnIndex);
                        int fileNameIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME);
                        String filePath = cursor.getString(fileNameIndex);
                        int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
                        int reason = cursor.getInt(columnReason);
                        switch (status) {
                            case DownloadManager.STATUS_SUCCESSFUL:
                                //  RL_tapToDownload.setClickable(true);
//                                Toast.makeText(DriveAndEarnStatementDownload.this, getString(R.string.download_success), Toast.LENGTH_SHORT).show();
                                String action = intent.getAction();
                                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                                    AlertDownloadCompleted(getResources().getString(R.string.action_success), ("COMPLETED"), intent);
                                }
                                break;
                            case DownloadManager.STATUS_FAILED:
                                // RL_tapToDownload.setClickable(true);
                                Toast.makeText(HelpAndAssistance.this, getString(R.string .succeess_alert), Toast.LENGTH_SHORT).show();
                                break;
                            case DownloadManager.STATUS_PAUSED:
                                Toast.makeText(HelpAndAssistance.this, getString(R.string.download_paused), Toast.LENGTH_SHORT).show();
                                break;
                            case DownloadManager.STATUS_PENDING:
                                Toast.makeText(HelpAndAssistance.this, getString(R.string.download_pending), Toast.LENGTH_SHORT).show();
                                break;
                            case DownloadManager.STATUS_RUNNING:
//                                Toast.makeText(DriveAndEarnStatementDownload.this, getString(R.string.downloading), Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }
            };
            registerReceiver(receiverNotificationClicked, filter);
            registerReceiver(receiverDownloadComplete, intentFilter);
            isReceiversRegistered = true;
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(HelpAndAssistance.this, Navigation_new.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    private void AlertDownloadCompleted(String title, String alert, final Intent intent) {
        final PkDialog mDialog = new PkDialog(HelpAndAssistance.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.timer_label_alert_yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(downloadId);
                downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                Cursor c = downloadManager.query(query);
                if (c.moveToFirst()) {
                    int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                    if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                        String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                        //TODO : Use this local uri and launch intent to open file

//                        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
//                        String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(sDownloadLink));

                        Uri uri = Uri.parse(uriString);
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setDataAndType(uri, "application/pdf");
//                        intent.setDataAndType(uri, mimeString);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);

                    }
                }

            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.timer_label_alert_no), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                isAlertShowing = false;
            }
        });

        if (!isAlertShowing) {
            mDialog.show();
            isAlertShowing = true;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (receiverNotificationClicked != null && isReceiversRegistered) {
            unregisterReceiver(receiverNotificationClicked);
        }
        if (receiverDownloadComplete != null && isReceiversRegistered) {
            unregisterReceiver(receiverDownloadComplete);
        }
    }
}