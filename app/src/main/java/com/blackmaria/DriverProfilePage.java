package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.DriverProfileAdapter;
import com.blackmaria.adapter.DriverProfileReviewAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.DriverProfilePojo;
import com.blackmaria.pojo.DriverProfileReviewPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.blackmaria.R.id.myprofile_reviews_textview;

/**
 * Created by Prem Kumar and Anitha on 12/22/2016.
 */

public class DriverProfilePage extends ActivityHockeyApp implements View.OnClickListener{

    private CustomTextView  Tv_driverName, Tv_reviewCount;
    private RoundedImageView Iv_profileImage;
    private RatingBar Rb_driverRating;
    private LinearLayout Ll_review, Ll_overView;
    private RelativeLayout Rl_close;

    private ServiceRequest mRequest;
    Dialog dialog;

    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;
    private String sUserId = "", sRideId = "";

    private ExpandableHeightListView overview_listView,review_listView;
    private ArrayList<DriverProfilePojo> itemList;
    private ArrayList<DriverProfileReviewPojo> reviewItemlist;
    private boolean hasData = false,hasreviewData = false;
    private DriverProfileAdapter adapter;
    private DriverProfileReviewAdapter review_adapter;
    private RelativeLayout Rl_back;

    private RefreshReceiver refreshReceiver;
    private CustomTextView Tv_listempty;
    private CustomTextView Tv_overview,Tv_review;
    private Dialog review_dialog;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        TrackRideAcceptPage.trackRideAcceptPage_class.finish();
                        Intent intent1 = new Intent(DriverProfilePage.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        TrackRideAcceptPage.trackRideAcceptPage_class.finish();
                        Intent intent1 = new Intent(DriverProfilePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", sRideId);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus) ){
                        TrackRideAcceptPage.trackRideAcceptPage_class.finish();
                        Intent intent1 = new Intent(DriverProfilePage.this, FareBreakUp.class);
                        intent1.putExtra("ratingflag", "2");
                        intent1.putExtra("RideID", sRideId);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_profile_page);
        initialize();

        Rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        Ll_overView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Ll_review.setBackgroundColor(getResources().getColor(R.color.white_color));
                Ll_overView.setBackgroundColor(getResources().getColor(R.color.blue_color));
                Tv_overview.setTextColor(getResources().getColor(R.color.white_color));
                Tv_review.setTextColor(getResources().getColor(R.color.blue_color));

                if (hasData) {
                    Tv_listempty.setVisibility(View.GONE);
                    adapter = new DriverProfileAdapter(DriverProfilePage.this, itemList);
                    overview_listView.setAdapter(adapter);
                }else{
                    Tv_listempty.setVisibility(View.VISIBLE);
                }

            }
        });
        Ll_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ll_review.setBackgroundColor(getResources().getColor(R.color.blue_color));
                Ll_overView.setBackgroundColor(getResources().getColor(R.color.white_color));
                Tv_overview.setTextColor(getResources().getColor(R.color.blue_color));
                Tv_review.setTextColor(getResources().getColor(R.color.white_color));

                if (hasreviewData) {
                    Tv_listempty.setVisibility(View.GONE);

                    review_adapter = new DriverProfileReviewAdapter(DriverProfilePage.this, reviewItemlist);
                    overview_listView.setAdapter(review_adapter);
                }else{
                    Tv_listempty.setVisibility(View.VISIBLE);
                }

            }
        });




    }






    private void initialize() {

        sessionManager = new SessionManager(DriverProfilePage.this);
        cd = new ConnectionDetector(DriverProfilePage.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemList = new ArrayList<DriverProfilePojo>();
        reviewItemlist = new ArrayList<DriverProfileReviewPojo>();

        Rl_back = (RelativeLayout) findViewById(R.id.myprofile_header_back_layout);
        Tv_driverName = (CustomTextView) findViewById(R.id.myprofile_drivername);
        Tv_reviewCount = (CustomTextView) findViewById(myprofile_reviews_textview);
        overview_listView = (ExpandableHeightListView) findViewById(R.id.myprofile_overview_expandable_listView);
        review_listView = (ExpandableHeightListView) findViewById(R.id.myprofile_review_expandable_listView);
        Tv_listempty = (CustomTextView) findViewById(R.id.myprofile_review_list_empty_textview);


        Iv_profileImage = (RoundedImageView) findViewById(R.id.myprofile_profileimg);
        Rb_driverRating = (RatingBar) findViewById(R.id.myprofile_ratingbar);
        Ll_review = (LinearLayout) findViewById(R.id.myprofile_reviews_layout);
        Ll_overView = (LinearLayout) findViewById(R.id.myprofile_overview_layout);
        Tv_overview = (CustomTextView) findViewById(R.id.myprofile_overview_textview);
        Tv_review = (CustomTextView) findViewById(R.id.myprofile_reviews_textview);

        overview_listView.setExpanded(true);
        review_listView.setExpanded(true);
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        HashMap<String, String> user = sessionManager.getUserDetails();
        sUserId = user.get(SessionManager.KEY_USERID);

        Intent intent = getIntent();
        if (intent != null) {
            sRideId = intent.getStringExtra("ride_id");
        }

        if (isInternetPresent) {
            DriverProfileRequest(Iconstant.driverProfilePage_url);
        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
    }

    @Override
    public void onClick(View view) {

        if(view==Ll_review)
        {

        }else if(view==Ll_overView)
        {

        }

    }


    //--------------Alert Method-----------
    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(DriverProfilePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void DriverProfileRequest(String Url) {

        dialog = new Dialog(DriverProfilePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sUserId);
        jsonParams.put("ride_id", sRideId);

        System.out.println("--------------DriverProfileRequestUrl-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(DriverProfilePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverProfileRequest response-------------------" + response);
                String status = "", sTitle_text = "", sSubtitle_text = "", sRideStatus = "";
                String Str_DriverName = "", Str_DriverId = "", Str_DriverImg = "", sDriverReview = "", sTotalReviewCount = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                sTotalReviewCount = jsonObject.getString("total_review_count");

                                JSONObject profile_jsonObject = jsonObject.getJSONObject("driver_profile");
                                if (profile_jsonObject.length() > 0) {
                                    Str_DriverName = profile_jsonObject.getString("driver_name");
                                    Str_DriverId = profile_jsonObject.getString("driver_id");
                                    Str_DriverImg = profile_jsonObject.getString("driver_image");
                                    sDriverReview = profile_jsonObject.getString("driver_review");
                                }


                                Object contentArray_check = jsonObject.get("driver_array_view");
                                if (contentArray_check instanceof JSONArray) {

                                    JSONArray contentArray = jsonObject.getJSONArray("driver_array_view");
                                    if (contentArray.length() > 0) {
                                        itemList.clear();
                                        for (int i = 0; i < contentArray.length(); i++) {
                                            JSONObject content_jsonObject = contentArray.getJSONObject(i);
                                            DriverProfilePojo pojo = new DriverProfilePojo();
                                            pojo.setTitle(content_jsonObject.getString("title"));
                                            pojo.setValue(content_jsonObject.getString("value"));

                                            itemList.add(pojo);
                                        }

                                        hasData = true;
                                    } else {
                                        hasData = false;
                                    }
                                } else {
                                    hasData = false;
                                }


                                Object review_arr_check = jsonObject.get("review_arr");
                                if (review_arr_check instanceof JSONArray) {

                                    JSONArray review_arr = jsonObject.getJSONArray("review_arr");
                                    if (review_arr.length() > 0) {
                                        reviewItemlist.clear();
                                        for (int i = 0; i < review_arr.length(); i++) {
                                            JSONObject review_arr_jsonObject = review_arr.getJSONObject(i);
                                            DriverProfileReviewPojo driverProfileReviewPojo = new DriverProfileReviewPojo();
                                            driverProfileReviewPojo.setRide_id(review_arr_jsonObject.getString("ride_id"));
                                            driverProfileReviewPojo.setUser_name(review_arr_jsonObject.getString("user_name"));
                                            driverProfileReviewPojo.setUser_image(review_arr_jsonObject.getString("user_image"));
                                            driverProfileReviewPojo.setComment(review_arr_jsonObject.getString("comment"));
                                            driverProfileReviewPojo.setDate(review_arr_jsonObject.getString("date"));
                                            driverProfileReviewPojo.setRating(review_arr_jsonObject.getString("rating"));
                                            reviewItemlist.add(driverProfileReviewPojo);
                                        }

                                        hasreviewData = true;
                                    } else {
                                        hasreviewData = false;
                                    }
                                } else {
                                    hasreviewData = false;
                                }






                            }
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (status.equalsIgnoreCase("1")) {

                    Tv_driverName.setText(Str_DriverName);
                    Picasso.with(DriverProfilePage.this)
                            .load(Str_DriverImg)
                            .placeholder(R.drawable.no_user_img)   // optional
                            .error(R.drawable.no_user_img)      // optional
                            .into(Iv_profileImage);

                    Tv_reviewCount.setText(getResources().getString(R.string.profile_label_reviews) + " (" + sTotalReviewCount + ") ");
                    Rb_driverRating.setRating(Float.parseFloat(sDriverReview));


                    if (hasData) {
                        Tv_listempty.setVisibility(View.GONE);
                        adapter = new DriverProfileAdapter(DriverProfilePage.this, itemList);
                        overview_listView.setAdapter(adapter);
                    }else{
                        Tv_listempty.setVisibility(View.VISIBLE);
                    }


                }


                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    public void reviewDialog(String sName,String sImage,String sDate,String sRate,String sCommant) {

        review_dialog = new Dialog(DriverProfilePage.this);
        review_dialog.getWindow();
        review_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        review_dialog.setContentView(R.layout.driver_review_dialog);
        review_dialog.setCanceledOnTouchOutside(true);
//        redeem_dialog.getWindow().getAttributes().windowAnimations =R.style.Animations_categories_filter;
        review_dialog.show();
        review_dialog.getWindow().setGravity(Gravity.CENTER);

        CustomTextView Tv_close = (CustomTextView) review_dialog.findViewById(R.id.driver_review_dialog_close_textview);
        CustomTextView Tv_commant = (CustomTextView) review_dialog.findViewById(R.id.driver_review_dialog_user_commant_textview);
        CustomTextView Tv_date = (CustomTextView) review_dialog.findViewById(R.id.driver_review_dialog_user_rating_date_textview);
        CustomTextView Tv_name = (CustomTextView) review_dialog.findViewById(R.id.driver_review_dialog_username_textview);
        RoundedImageView user_img = (RoundedImageView) review_dialog.findViewById(R.id.driver_review_dialog_user_imageview);
        RatingBar user_rating = (RatingBar) review_dialog.findViewById(R.id.driver_review_dialog_user_ratingbar);

        Tv_commant.setText(sCommant);
        Tv_date.setText(sDate);
        Tv_name.setText(sName);
        user_rating.setRating(Float.parseFloat(sRate));
        Picasso.with(DriverProfilePage.this)
                .load(sImage)
                .into(user_img);

        Tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                review_dialog.dismiss();
            }
        });

    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }
}
