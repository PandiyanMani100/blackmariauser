package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.adapter.DriverProfileAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.DriverProfilePojo;
import com.blackmaria.pojo.Driver_profile;
import com.blackmaria.pojo.Trackrideafteraccept_pojo;
import com.blackmaria.pojo.Waitingtimeurl;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;

import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by user144 on 26-07-2017.
 */

public class WaitingTimePage extends ActivityHockeyApp {
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private String sUserId = "", sRideId = "";
    long min = 0, sec = 0;
    boolean isContinuing = false;

    private TextView Tv_waitingTime;
    private Button Btn_close;
    private String ride_id = "",Str_driver_image="";
    private SessionManager sessionManager;
    private Handler customHandler = new Handler();
    private Calendar calendar;
    DecimalFormat precision = new DecimalFormat("00");
    private Dialog dialog;
    private ServiceRequest mRequest;
    private String strFreeWaitingTime = "", IsNormalOrreturnMulti = "";
    private String Usergivenwaitingtime = "";
    TextView freeWaitingTimeTv, tv_carmodel;
    private RelativeLayout calldriver, cmplemntrydrivercall, continueTrip, finishTrip, driverinfo, message;

    final int PERMISSION_REQUEST_CODE = 111;
    final int PERMISSION_REQUEST_CODES = 222;
    LanguageDb mhelper;

    private String Str_phone_number = "";
    //    private SmoothProgressBar profile_loading_progressbar;
    private TextView waitingtime_text, tv_charges;
    private Trackrideafteraccept_pojo trackpojo;
    public static Activity activity;
    private RoundedImageView driverimg, trackdriver_carphoto;
    private TextView tv_carno, tv_carnumber;

    private int freewaitingfromresponse = 0, chargeperminute = 0;
    private String currencycode = "";

    private ArrayList<DriverProfilePojo> itemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newwaitingtime_layout);
        mhelper= new LanguageDb(this);
        activity = WaitingTimePage.this;

        initialize();

        Btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertSuccess("",getkey("close_timer"));
            }
        });


        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ConfirmRideRequest(Iconstant.track_your_ride_url, "");
            }
        });


        calldriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Str_phone_number.equalsIgnoreCase("")) {
//                    HashMap<String, String> phone = sessionManager.getPhoneMaskingStatus();
                    String sPhoneMaskingStatus = sessionManager.getPhoneMaskingStatus();

                    if (sPhoneMaskingStatus.equalsIgnoreCase("Yes")) {
                        postRequest_PhoneMasking(Iconstant.phoneMasking_url);
                    } else {
                        if (Build.VERSION.SDK_INT >= 23) {
                            // Marshmallow+
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_phone_number));
                            startActivity(intent);
//                            }
                        } else {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_phone_number));
                            startActivity(intent);
                        }
                    }

                } else {
                    Alert(getkey("action_error"), getkey("inavid_call"));
                }
            }
        });

        finishTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FinsihTripAlertSuccess("",getkey("close_timer"));

            }
        });
        continueTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertSuccess("", getkey("close_timer"));
            }
        });

        driverinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileDialog();
            }
        });
    }

    private void ConfirmRideRequest(String Url, final String try_value) {
        dialog = new Dialog(WaitingTimePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView loading = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        loading.setText(getkey("action_pleasewait"));

        System.out.println("--------------Confirm Ride url----bpp---------------" + Url);

        HashMap<String, String> info = sessionManager.getUserDetails();
        String UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", sRideId);

        mRequest = new ServiceRequest(WaitingTimePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------Confirm Ride reponse-------------------" + response);

                trackpojo = new Trackrideafteraccept_pojo();

                try {
                    JSONObject object = new JSONObject(response);
                    Type listType = new TypeToken<Trackrideafteraccept_pojo>() {
                    }.getType();

                    try {
                        trackpojo = new GsonBuilder().create().fromJson(object.toString(), listType);
                    } catch (JsonSyntaxException e) {
                        System.out.println("sout" + e);
                    }

                    if (object.length() > 0) {
                        String status = object.getString("status");
                        if (status.equalsIgnoreCase("1")) {
                            JSONObject response_object = object.getJSONObject("response");
                            if (response_object.length() > 0) {
                                Object check_driver_profile_object = response_object.get("driver_profile");
                                if (check_driver_profile_object instanceof JSONObject) {
                                    JSONObject driver_profile_object = response_object.getJSONObject("driver_profile");
                                    if (driver_profile_object.length() > 0) {
                                        sessionManager.setDriverprofile(driver_profile_object.toString());
                                        String driver_fcm_token="";
                                        Str_driver_image = trackpojo.getResponse().getDriver_profile().getDriver_image();
                                        String Str_driver_name = trackpojo.getResponse().getDriver_profile().getDriver_name();
                                        String Str_driver_id = trackpojo.getResponse().getDriver_profile().getDriver_id();
                                        String Str_phone_number = trackpojo.getResponse().getDriver_profile().getPhone_number();
                                        sessionManager.setKeyDriverPhonenumber(Str_phone_number);

                                        Object notification_details = driver_profile_object.get("notification_details");
                                        if (notification_details instanceof JSONObject) {
                                            JSONObject notification_detailsjson = driver_profile_object.getJSONObject("notification_details");
                                            if (notification_detailsjson.length() > 0) {
                                                driver_fcm_token = notification_detailsjson.getString("driver_fcm_token");
                                            }
                                        }

                                        Intent intent = new Intent(WaitingTimePage.this,MessagesActivity.class);
                                        intent.putExtra("name",Str_driver_name);
                                        intent.putExtra("userimage",Str_driver_image);
                                        intent.putExtra("rideid",ride_id);
                                        intent.putExtra("useridtosend",Str_driver_id);
                                        intent.putExtra("userphonenumber",Str_phone_number);
                                        intent.putExtra("driver_fcm_token",driver_fcm_token);
                                        intent.putExtra("jsutcallfinish","1");
                                        startActivity(intent);

                                    }
                                }
                            }
                        } else {
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void initialize() {
        sessionManager = new SessionManager(WaitingTimePage.this);
        cd = new ConnectionDetector(WaitingTimePage.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemList = new ArrayList<>();
        HashMap<String, String> user = sessionManager.getUserDetails();
        sUserId = user.get(SessionManager.KEY_USERID);
        strFreeWaitingTime = sessionManager.getFreeWaitingTimen();
        Tv_waitingTime = (TextView) findViewById(R.id.txt_waiting_time);
        calldriver = (RelativeLayout) findViewById(R.id.calldriver);
        cmplemntrydrivercall = (RelativeLayout) findViewById(R.id.cmplemntrydrivercall);
        Btn_close = (Button) findViewById(R.id.btn_close);
        freeWaitingTimeTv = (TextView) findViewById(R.id.free_waitingtime_tv);
        tv_carmodel = (TextView) findViewById(R.id.tv_carmodel);
//        profile_loading_progressbar = (SmoothProgressBar) findViewById(R.id.profile_loading_progressbar);
        waitingtime_text = (TextView) findViewById(R.id.waitingtime_text);
        waitingtime_text.setText(getkey("vehicleiswaiting"));
        tv_charges = (TextView) findViewById(R.id.free_waitingtime_charges);

        TextView addwait = (TextView) findViewById(R.id.addwait);
        addwait.setText(getkey("additionalchargestimeup"));

        TextView freec = (TextView) findViewById(R.id.freec);
        freec.setText(getkey("freecall"));
        TextView fremesc = (TextView) findViewById(R.id.fremesc);
        fremesc.setText(getkey("freemessage"));

        TextView carn = (TextView) findViewById(R.id.carn);
        carn.setText(getkey("carnos"));



        continueTrip = (RelativeLayout) findViewById(R.id.continiue_trip);
        finishTrip = (RelativeLayout) findViewById(R.id.finish_trip);
        driverinfo = (RelativeLayout) findViewById(R.id.driverinfo);
        message = (RelativeLayout) findViewById(R.id.message);
        driverimg = (RoundedImageView) findViewById(R.id.driverimg);
//        trackdriver_carphoto = (ImageView) findViewById(R.id.trackdriver_carphoto);
        tv_carno = (TextView) findViewById(R.id.tv_carno);
        tv_carnumber = (TextView) findViewById(R.id.tv_carnumber);


        getIntentData();
        strFreeWaitingTime = sessionManager.getFreeWaitingTimen();

        Str_phone_number = sessionManager.getKeyDriverPhonenumber();
        if (IsNormalOrreturnMulti.equalsIgnoreCase("return") || IsNormalOrreturnMulti.equalsIgnoreCase("multistop")) {
            freeWaitingTimeTv.setText(getkey("youhave") +" "+ strFreeWaitingTime +" "+getkey("minfreewait"));
            if (strFreeWaitingTime.equalsIgnoreCase("0")) {
                waitingtime_text.setText(getkey("wait_service_timeup"));
            }
            tv_charges.setText(getkey("fees") + currencycode + " " + "0.00");
            tv_charges.setVisibility(View.VISIBLE);
        } else {
            freeWaitingTimeTv.setText(getkey("youhave")+" " + strFreeWaitingTime +" "+ getkey("minfreewait"));
            tv_charges.setVisibility(View.INVISIBLE);
        }


        ImageView Rl_drawer = (ImageView) findViewById(R.id.drawer_icon);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);




        if (isContinuing) {
            try {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        HashMap<String, String> waitedTimeMap = sessionManager.getWaitedTime();
                        String waitedTime = waitedTimeMap.get(SessionManager.KEY_CALENDAR_TIME);

                        // String minutes1 = waitedTimeMap.get(SessionManager.KEY_WAITED_MINS);
                        // String seconds1 = waitedTimeMap.get(SessionManager.KEY_WAITED_SECS);

                        System.out.println("==============Muruga session waitingtime==========" + waitedTime);

                        if (waitedTime.isEmpty()) {
                            String updatedTime = precision.format(min) + ":" + precision.format(sec);
                            calendar = Calendar.getInstance();
                            sessionManager.setWaitedTime(min, sec, updatedTime, String.valueOf(calendar.getTime()));
                        }
                        if (!waitedTime.equalsIgnoreCase("")) {

                            setSecodds(waitedTime);

                            if (sec > 0) {
                                // sec = 0;
                                // min = min + 1;

                                if (min >= Integer.parseInt(strFreeWaitingTime)) {

//                                    if (!IsNormalOrreturnMulti.equalsIgnoreCase("")) {
//                                        continueTrip.setVisibility(View.VISIBLE);
//                                        finishTrip.setVisibility(View.VISIBLE);
//                                    } else {
//                                        continueTrip.setVisibility(View.GONE);
//                                        finishTrip.setVisibility(View.GONE);
//                                    }
//                                    cmplemntrydrivercall.setVisibility(View.VISIBLE);
//                                    tv_charges.setVisibility(View.VISIBLE);
//                                    profile_loading_progressbar.setVisibility(View.GONE);
                                    waitingtime_text.setText(getkey("wait_service_timeup"));
                                } else {
//                                    cmplemntrydrivercall.setVisibility(View.VISIBLE);
//                                    tv_charges.setVisibility(View.VISIBLE);
//                                    profile_loading_progressbar.setVisibility(View.VISIBLE);
                                    if (IsNormalOrreturnMulti.equalsIgnoreCase("return") || IsNormalOrreturnMulti.equalsIgnoreCase("multistop")) {
                                        if (sessionManager.getComplementStatus().equalsIgnoreCase("onprocess")) {
                                            waitingtime_text.setText(getkey("waititme_activated"));
                                        } else {
                                            waitingtime_text.setText(getkey("waitimefornext"));
                                        }
                                    } else {
                                        waitingtime_text.setText(getkey("waihilecwaiting"));
                                    }
                                }
                            }
                        }

                    }
                }, 100);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            Log.d("", "");
        }
        customHandler.postDelayed(updateTimerThread, 1000);

        updateUIservice(Iconstant.waitingtime_api);
    }

    private void updateUIservice(String Url) {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", sRideId);
        mRequest = new ServiceRequest(WaitingTimePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println(".........." + response);
                String Sstatus = "", message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Type listType = new TypeToken<Waitingtimeurl>() {
                    }.getType();
                    Waitingtimeurl obj = new GsonBuilder().create().fromJson(object.toString(), listType);

                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        try {
                            tv_carno.setText(obj.getResponse().getVehicle_no());
                            tv_carmodel.setText(obj.getResponse().getVehicle_model());
                            tv_carnumber.setText(obj.getResponse().getVehicle_no());
                            freewaitingfromresponse = Integer.parseInt(obj.getResponse().getWaiting_time().getFree_min());
                            chargeperminute = Integer.parseInt(obj.getResponse().getWaiting_time().getPer_min());
                            currencycode = obj.getResponse().getCurrency();
                            Glide.with(WaitingTimePage.this).load(obj.getResponse().getDriver_image()).into(driverimg);
                        } catch (NumberFormatException e) {
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("FreeWaitingTime")) {
                strFreeWaitingTime = intent.getStringExtra("FreeWaitingTime");
                if (strFreeWaitingTime.equalsIgnoreCase("0")) {
                } else {
                    sessionManager.setFreeWaitingTime(strFreeWaitingTime);
                }
            }
            if (intent.hasExtra("IsNormalOrreturnMulti")) {
                IsNormalOrreturnMulti = intent.getStringExtra("IsNormalOrreturnMulti");
            }

            sRideId = intent.getStringExtra("ride_id");
            if (intent.hasExtra("isContinue")) {
                isContinuing = getIntent().getBooleanExtra("isContinue", false);
            }
        }

        if(intent.hasExtra("arivepage")){
            driverinfo.setVisibility(View.VISIBLE);
            cmplemntrydrivercall.setVisibility(View.VISIBLE);
            message.setVisibility(View.VISIBLE);

        }else{
            if (!IsNormalOrreturnMulti.equalsIgnoreCase("")) {
                driverinfo.setVisibility(View.GONE);
                cmplemntrydrivercall.setVisibility(View.GONE);
                message.setVisibility(View.GONE);

                continueTrip.setVisibility(View.VISIBLE);
                finishTrip.setVisibility(View.VISIBLE);
                cmplemntrydrivercall.setVisibility(View.VISIBLE);
                tv_charges.setVisibility(View.VISIBLE);
            }
        }

    }

    private Runnable updateTimerThread = new Runnable() {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void run() {
            HashMap<String, String> waitedTimeMap = sessionManager.getWaitedTime();
            String waitedTime = waitedTimeMap.get(SessionManager.KEY_CALENDAR_TIME);
            if (waitedTime.isEmpty()) {
                String updatedTime = precision.format(min) + ":" + precision.format(sec);
                calendar = Calendar.getInstance();
                sessionManager.setWaitedTime(min, sec, updatedTime, String.valueOf(calendar.getTime()));
            }
            setSecodds(waitedTime);
            //sec = sec + 1;
            if (true) {
                // sec = 0;
                //min = min + 1;
                try {
                    if (min >= Integer.parseInt(strFreeWaitingTime)) {
//                        if (!IsNormalOrreturnMulti.equalsIgnoreCase("")) {
//                            continueTrip.setVisibility(View.VISIBLE);
//                            finishTrip.setVisibility(View.VISIBLE);
//                        } else {
//                            continueTrip.setVisibility(View.GONE);
//                            finishTrip.setVisibility(View.GONE);
//                        }
//                        cmplemntrydrivercall.setVisibility(View.VISIBLE);
//                        tv_charges.setVisibility(View.VISIBLE);
//                    profile_loading_progressbar.setVisibility(View.GONE);
                        waitingtime_text.setText(getkey("wait_service_timeup"));
                    } else {
//                        cmplemntrydrivercall.setVisibility(View.VISIBLE);
//                        tv_charges.setVisibility(View.VISIBLE);
//                    profile_loading_progressbar.setVisibility(View.VISIBLE);

                        if (IsNormalOrreturnMulti.equalsIgnoreCase("return") || IsNormalOrreturnMulti.equalsIgnoreCase("multistop")) {

                            if (sessionManager.getComplementStatus().equalsIgnoreCase("onprocess")) {
                                waitingtime_text.setText(getkey("waititme_activated"));
                            } else {
                                waitingtime_text.setText(getkey("waitimefornext"));
                            }
                        } else {
                            waitingtime_text.setText(getkey("waihilecwaiting"));
                        }
                    }
                }catch (NumberFormatException e){}
            }
//            if (Integer.parseInt(strFreeWaitingTime) == 0) {
//                continueTrip.setVisibility(View.VISIBLE);
//                finishTrip.setVisibility(View.VISIBLE);
//                cmplemntrydrivercall.setVisibility(View.VISIBLE);
//                tv_charges.setVisibility(View.VISIBLE);
//            }
            String updatedTime = precision.format(min) + ":" + precision.format(sec);
            Tv_waitingTime.setText(updatedTime);
            int minss = Math.round(min);

            //charger update
            if (freewaitingfromresponse != 0 && minss != 0) {
                try {
                    int mins = Math.round(min);
                    int finalcharges = (mins - freewaitingfromresponse) * chargeperminute;
                    tv_charges.setText(getkey("fees") + currencycode + " " + finalcharges);
                } catch (NumberFormatException e) {
                    System.out.println("" + e);
                } catch (NullPointerException e) {
                    System.out.println("" + e);
                }
            }

            calendar = Calendar.getInstance();

//            /sessionManager.setWaitedTime(0, 0, "0", String.valueOf(calendar.getTime()));

            customHandler.postDelayed(updateTimerThread, 1000);
        }
    };


    private void setSecodds(String waitedTime) {
        if (waitedTime == null || waitedTime.isEmpty())
            return;
        Date waitingTimeDate = new Date(waitedTime);
        Date currentDate = Calendar.getInstance().getTime();
        long diff = currentDate.getTime() - waitingTimeDate.getTime();

        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

/*                            sec = seconds + Long.parseLong(seconds1);
                            min = minutes + Long.parseLong(minutes1);*/

        sec = seconds % 60;
        min = minutes;
//                    sec = Long.parseLong(seconds1);
//                    min = Long.parseLong(minutes1);

    }

    private void postRequest_CitySelect(String Url) {
        dialog = new Dialog(WaitingTimePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-------------End Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", sUserId);
        jsonParams.put("ride_id", sRideId);
        mRequest = new ServiceRequest(WaitingTimePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------End Response----------------" + response);

                String Sstatus = "", message = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject resobject = object.getJSONObject("response");
                        message = resobject.getString("message");
                        final PkDialog mDialog = new PkDialog(WaitingTimePage.this);
                        mDialog.setDialogTitle(getkey("action_success"));
                        mDialog.setDialogMessage(message);
                        mDialog.setCancelOnTouchOutside(false);
                        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                Intent intent1 = new Intent(WaitingTimePage.this, FareBreakUp.class);
                                intent1.putExtra("RideID", sRideId);
                                intent1.putExtra("ratingflag", "1");
                                startActivity(intent1);
                                overridePendingTransition(R.anim.enter, R.anim.exit);
                                finish();

                            }
                        });
                        mDialog.show();
                    } else {
                        message = object.getString("response");
                        Alert("Alert", message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (customHandler != null) {
            customHandler.removeCallbacks(updateTimerThread);
            customHandler = null;
        }

    }

    //--------------Alert Function------------------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WaitingTimePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.show();
    }


    private void AlertSuccess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WaitingTimePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (customHandler != null) {
                    customHandler.removeCallbacks(updateTimerThread);
                    customHandler = null;
                }
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                sessionManager.setContinueTrip("yes");

                Intent intent = new Intent(WaitingTimePage.this, TrackRidePage.class);
                intent.putExtra("ride_id", sRideId);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
            }
        });
        mDialog.setNegativeButton(getkey("action_cancel"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void FinsihTripAlertSuccess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WaitingTimePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetPresent) {
                    postRequest_CitySelect(Iconstant.UserEndRide);
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }
                mDialog.dismiss();

            }
        });
        mDialog.setNegativeButton(getkey("action_cancel"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void postRequest_PhoneMasking(String Url) {

        dialog = new Dialog(WaitingTimePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", sRideId);
        jsonParams.put("user_type", "user");

        System.out.println("-------------PhoneMasking Url----------------" + Url);
        mRequest = new ServiceRequest(WaitingTimePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------PhoneMasking Response----------------" + response);

                String Str_status = "", SResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    SResponse = object.getString("response");
                    if (Str_status.equalsIgnoreCase("1")) {
                        Alert(getkey("action_success"), SResponse);
                    } else {
                        Alert(getkey("action_error"), SResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODES);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_phone_number));
                    startActivity(intent);
                }
        }
    }

    private void profileDialog() {
        DriverProfileAdapter adapter;
        final MaterialDialog dialog = new MaterialDialog(WaitingTimePage.this);
        View view = LayoutInflater.from(WaitingTimePage.this).inflate(R.layout.driver_profile_dialog, null);

        final TextView Tv_close = (TextView) view.findViewById(R.id.driver_profile_close_textview);
        CustomTextView Tv_driver_name = (CustomTextView) view.findViewById(R.id.driver_profile_driver_name);
        ExpandableHeightListView overview_listView = (ExpandableHeightListView) view.findViewById(R.id.driver_profile_expandable_listview);
        RoundedImageView Iv_profileImage = (RoundedImageView) view.findViewById(R.id.driver_profile_imageview);
        RatingBar Rb_driver_rating = (RatingBar) view.findViewById(R.id.driver_profile_ratingbar);
//        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
        TextView driver_profile_driver_profile_lable = (TextView) view.findViewById(R.id.driver_profile_driver_profile_lable);
        driver_profile_driver_profile_lable.setText(getkey("profile_label_driverprofile"));

        Tv_close.setText(getkey("profile_label_close"));


        Drawable drawable = Rb_driver_rating.getProgressDrawable();
        drawable.setColorFilter(Color.parseColor("#FF9900"), PorterDuff.Mode.SRC_ATOP);
        CustomTextView Tv_listempty = (CustomTextView) view.findViewById(R.id.driver_profile_list_empty_textview);

        if(!sessionManager.getDriverprofile().equalsIgnoreCase("")){
            JSONObject object = null;
            try {
                object = new JSONObject(sessionManager.getDriverprofile());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Type listType = new TypeToken<Driver_profile>() {
            }.getType();

            try {
                Driver_profile trackpojo = new GsonBuilder().create().fromJson(object.toString(), listType);
                Tv_driver_name.setText(trackpojo.getDriver_name());
                Picasso.with(WaitingTimePage.this)
                        .load(trackpojo.getDriver_image())
                        .placeholder(R.drawable.no_user_img)   // optional
                        .error(R.drawable.no_user_img)      // optional
                        .into(Iv_profileImage);
                Rb_driver_rating.setRating(Float.parseFloat(trackpojo.getDriver_review()));
                itemList = trackpojo.getDriver_array_view();
            } catch (JsonSyntaxException e) {
                System.out.println("sout" + e);
            }

            if (itemList.size() > 0) {
                Tv_listempty.setVisibility(View.GONE);
                adapter = new DriverProfileAdapter(WaitingTimePage.this, itemList);
                overview_listView.setAdapter(adapter);
            } else {
                Tv_listempty.setVisibility(View.VISIBLE);
            }
        }else{
            Toast.makeText(WaitingTimePage.this,getkey("profilenotupadted"),Toast.LENGTH_SHORT).show();
        }

        Tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.setBackground(getResources().getDrawable(R.drawable.transparant_bg));
        dialog.setView(view).show();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}