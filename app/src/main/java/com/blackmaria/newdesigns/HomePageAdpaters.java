package com.blackmaria.newdesigns;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.InterFace.Slider;
import com.blackmaria.pojo.Homepage_pojo;
import com.blackmaria.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;


public class HomePageAdpaters extends RecyclerView.Adapter<HomePageAdpaters.ViewHolder>{
    private ArrayList<Homepage_pojo.Booking_banner_arr> listdata;
    Context ctx;
    private Slider slider;
    final Animation animation = new AlphaAnimation(1, 0);


    // RecyclerView recyclerView;
    public HomePageAdpaters(ArrayList<Homepage_pojo.Booking_banner_arr> listdata, Context ctx1,Slider slider) {
        this.listdata = listdata;
        this.ctx = ctx1;
        this.slider = slider;
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slider.onComplete(position,listdata.get(position));
            }
        });
        holder.ridetype.setText(listdata.get(position).getName());
        if(position == 0)
        {
            holder.firrstview.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.firrstview.setVisibility(View.GONE);
        }

        RequestOptions options = new RequestOptions()

                .fitCenter()

                .diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(ctx).load(listdata.get(position).getFile()).apply(options).into(holder.imageView);


        /*RequestOptions options = new RequestOptions()

                .fitCenter()

                .diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(ctx).load(listdata.get(position).getFile()).apply(options).into(holder.imageView);*/
    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatImageView imageView;
        ImageView Rl_drawer;
        public TextView ridetype;
        View firrstview;
        public RelativeLayout relativeLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (AppCompatImageView) itemView.findViewById(R.id.imageView);
            this.ridetype = (TextView) itemView.findViewById(R.id.ridetype);
            this.Rl_drawer = (ImageView) itemView.findViewById(R.id.loading);
            this.firrstview= (View) itemView.findViewById(R.id.firrstview);
        }
    }
}
