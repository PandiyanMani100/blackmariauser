package com.blackmaria.newdesigns;

import android.content.Intent;
import android.os.CountDownTimer;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.Navigation_new;
import com.blackmaria.R;
import com.blackmaria.utils.RoundedImageView;
import com.squareup.picasso.Picasso;

public class Thanksrating extends AppCompatActivity {

    private ImageView iv_close;
    private RoundedImageView iv_usericon;
    private TextView username, count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanksrating);

        init();
    }

    private void init() {
        iv_close = findViewById(R.id.iv_close);
        iv_usericon = findViewById(R.id.iv_usericon);
        username = findViewById(R.id.username);
        count = findViewById(R.id.count);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Thanksrating.this, Navigation_new.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        if (getIntent().hasExtra("driverimg")) {
            username.setText(getIntent().getStringExtra("drivername"));
            Picasso.with(Thanksrating.this)
                    .load(getIntent().getStringExtra("driverimg"))
                    .into(iv_usericon);
        }

        count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Thanksrating.this, Navigation_new.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                count.setText(" " + millisUntilFinished / 1000);
            }


            public void onFinish() {
                Intent intent = new Intent(Thanksrating.this, Navigation_new.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }

        }.start();


    }
}
