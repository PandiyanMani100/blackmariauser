package com.blackmaria.newdesigns;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.blackmaria.newdesigns.saveplus.Confirmpatern_activity;
import com.blackmaria.R;
import com.itsxtt.patternlock.PatternLockView;

import java.util.ArrayList;

public class Pattern_activity extends AppCompatActivity {

    private PatternLockView patternLockView;
    private ImageView iv_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_activity);

        patternLockView = findViewById(R.id.patternLockView);
        iv_back = findViewById(R.id.booking_back_imgeview);

        patternLockView.setOnPatternListener(new PatternLockView.OnPatternListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(ArrayList<Integer> ids) {

                System.out.println("progress........... " + ids);
            }

            @Override
            public boolean onComplete(ArrayList<Integer> ids) {
                /*
                 * A return value required
                 * if the pattern is not correct and you'd like change the pattern to error state, return false
                 * otherwise return true
                 */
                System.out.println("completed........... " + ids);
                return isPatternCorrect(ids);
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

    }

    private boolean isPatternCorrect(ArrayList<Integer> ids) {
        boolean value = false;
        if (ids.size() == 5) {
            value = true;
            Intent ii = new Intent(Pattern_activity.this, Confirmpatern_activity.class);
            ii.putIntegerArrayListExtra("pattern",ids);
            startActivity(ii);
        }
        return value;
    }


}
