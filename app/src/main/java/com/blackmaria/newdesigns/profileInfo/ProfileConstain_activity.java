package com.blackmaria.newdesigns.profileInfo;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.util.IOUtils;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.blackmaria.LanguageDb;
import com.blackmaria.adapter.CustomSpinnerAdapter;
import com.blackmaria.CardListPage;
import com.blackmaria.NewProfile_Page_CardEdit;
import com.blackmaria.pojo.WalletMoneyPojo;
import com.blackmaria.R;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.AppController;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.volley.VolleyMultipartRequest;
import com.blackmaria.widgets.Emailchange;
import com.blackmaria.widgets.Mobilenochange;
import com.blackmaria.widgets.PkDialogtryagain;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.devspark.appmsg.AppMsg;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ProfileConstain_activity extends AppCompatActivity implements View.OnClickListener {

    //login details
    private TextView editprofile, mylogin_dialog_form_country_code_textview, mylogin_dialog_form_pincode_edittext, updatelogin;
    private EditText mylogin_dialog_form_emailid_edittext, mylogin_dialog_form_mobile_no_edittext;

    //profile details
    private TextView editpersonaldetails, updateprofile, gender_textview, myprofile_dialog_form_country_edittext;
    private EditText myprofile_dialog_form_name_edittext, myprofile_dialog_form_age_edittext, myprofile_dialog_form_city_edittext, myprofile_dialog_form_state_edittext, myprofile_dialog_form_postcode_edittext;
    private Spinner profile_page_gende_spinner;

    //account details
    private TextView editaccount, addcard_number_textview, updateaccount, accounttype, selectbanktext, etaccountnumber, editcard, card_number_textview, card_exp_year_textview, card_exp_cvv_textview;
    private Spinner selectbank;
    private LinearLayout expyearcvv;
    private int selectbanktext_postion = 0;
    private ArrayList<WalletMoneyPojo> paymentcardlist;
    private ArrayList<String> paymentcardNamelist;
    private TextView tv_coupon_count;

    //emergencydetails
    private TextView editemergencydetails, SOS_dialog_form_country_edittext, sosmylogin_dialog_form_country_code_textview, updatesos;
    private EditText SOS_dialog_form_city_edittext, SOS_dialog_form_name_edittext, SOS_dialog_form_phone_edittext, SOS_dialog_form_email_edittext, SOS_dialog_form_relation_edittext;
    private AutoCompleteTextView SOS_dialog_form_address_edittext;

    private ConstraintLayout logindetails, accountdetails, emergencycontact;

    //headview
    private TextView profile, bankcard, emergency;
    private RoundedImageView iv_userbg;
    private BroadcastReceiver updateReceiver;
    private String UserID = "", sAuthKey = "", gcmID = "", globalmobilenumber = "", globalemail = "";
    private ArrayList<String> genderlist = new ArrayList<>();


    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private AppInfoSessionManager appInfoSessionManager;
    final int PERMISSION_REQUEST_CODE = 111;
    private int mobno_change_request_code = 200;
    private Uri camera_FileUri;
    Bitmap bitMapThumbnail;
    TextView chnagepin;
    private byte[] byteArray;
    private int REQUEST_TAKE_PHOTO = 1;
    private File destination;
    private int galleryRequestCode = 2;
    private static final String IMAGE_DIRECTORY_NAME = "blackmaria";
    private CountryPicker picker, picker_countrychoose_profile, picker_countrychoose_emergency, picker_countrychoose_emergency_country;
    private ImageView back;
    private boolean isemailchange = false;

LanguageDb mhelper;
    String uploaded_file_name = "";
    private String s3_bucket_access_key = "";
    private String s3_bucket_secret_key = "";
    private String s3_bucket_name = "";
    private AmazonS3Client s3Client;
    private BasicAWSCredentials credentials;
    TransferObserver uploadObserver;

    String File_names = "";
    String Extension = "";

    File imageRoot;
    private Uri mImageCaptureUri, outputUri;
    String appDirectoryName = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_constain_activity);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        mhelper = new LanguageDb(this);
        initView();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.PaymentListPage.refreshPage");
        updateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent != null) {
                    Refresh_page();
                }
            }
        };
        registerReceiver(updateReceiver, filter);

        s3BucketInit();
    }


    private void s3BucketInit() {

        s3_bucket_name = session.getbucketname();
        s3_bucket_access_key = session.getaccesskey();
        s3_bucket_secret_key = session.getsecretkey();

        getApplicationContext().startService(new Intent(getApplicationContext(), TransferService.class));
        credentials = new BasicAWSCredentials(s3_bucket_access_key, s3_bucket_secret_key);
        s3Client = new AmazonS3Client(credentials);
        s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
    }
    private void initView() {
        //login details
        editprofile = findViewById(R.id.editprofile);
        editprofile.setText(getkey("edita"));
        back = findViewById(R.id.RL_home);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        editprofile.setOnClickListener(this);
        iv_userbg = findViewById(R.id.iv_userbg);
        mylogin_dialog_form_country_code_textview = findViewById(R.id.mylogin_dialog_form_country_code_textview);
        mylogin_dialog_form_country_code_textview.setOnClickListener(this);
        mylogin_dialog_form_pincode_edittext = findViewById(R.id.mylogin_dialog_form_pincode_edittext);
        mylogin_dialog_form_pincode_edittext.setHint(getkey("profile_lable_pincode"));
        updatelogin = findViewById(R.id.updatelogin);
        updatelogin.setText(getkey("update_login"));
        updatelogin.setOnClickListener(this);
        mylogin_dialog_form_emailid_edittext = findViewById(R.id.mylogin_dialog_form_emailid_edittext);
        mylogin_dialog_form_mobile_no_edittext = findViewById(R.id.mylogin_dialog_form_mobile_no_edittext);



        TextView logindetailss = findViewById(R.id.logindetsails);
        logindetailss.setText(getkey("login_details"));

        TextView uploadphoto = findViewById(R.id.uploadphoto);
        uploadphoto.setText(getkey("uploadphoto"));

        TextView personaldetails = findViewById(R.id.personaldetails);
        personaldetails.setText(getkey("personal_details"));

        TextView acco = findViewById(R.id.acco);
        acco.setText(getkey("account_details"));

        TextView cardinfotext = findViewById(R.id.cardinfotext);
        cardinfotext.setText(getkey("cardinfo"));

        TextView emecon = findViewById(R.id.emecon);
        emecon.setText(getkey("emergency_contact"));

        TextView contactaddress = findViewById(R.id.contactaddress);
        contactaddress.setText(getkey("emergency_contact_address"));




        //profile details
        editpersonaldetails = findViewById(R.id.editpersonaldetails);
        chnagepin = findViewById(R.id.chnagepin);
        chnagepin.setText(getkey("change_pin_code"));
        editpersonaldetails.setOnClickListener(this);
        editpersonaldetails.setText(getkey("edit_tas"));
        updateprofile = findViewById(R.id.updateprofile);
        gender_textview = findViewById(R.id.gender_textview);
        gender_textview.setText(getkey("gender_lable"));
        updateprofile.setOnClickListener(this);
        updateprofile.setText(getkey("update_profile"));
        myprofile_dialog_form_name_edittext = findViewById(R.id.myprofile_dialog_form_name_edittext);
        myprofile_dialog_form_name_edittext.setHint(getkey("name_txt"));
        myprofile_dialog_form_city_edittext = findViewById(R.id.myprofile_dialog_form_city_edittext);
        myprofile_dialog_form_city_edittext.setHint(getkey("city_town"));
        myprofile_dialog_form_state_edittext = findViewById(R.id.myprofile_dialog_form_state_edittext);
        myprofile_dialog_form_state_edittext.setHint(getkey("state"));
        myprofile_dialog_form_postcode_edittext = findViewById(R.id.myprofile_dialog_form_postcode_edittext);
        myprofile_dialog_form_postcode_edittext.setHint(getkey("postcode"));
        myprofile_dialog_form_country_edittext = findViewById(R.id.myprofile_dialog_form_country_edittext);
        myprofile_dialog_form_country_edittext.setOnClickListener(this);
        myprofile_dialog_form_country_edittext.setHint(getkey("countrynew"));
        profile_page_gende_spinner = findViewById(R.id.profile_page_gende_spinner);
        genderlist.clear();
        genderlist.add(getkey("gender"));
        genderlist.add(getkey("male"));
        genderlist.add(getkey("female"));
        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(ProfileConstain_activity.this, genderlist);
        profile_page_gende_spinner.setAdapter(customSpinnerAdapter);
        profile_page_gende_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                gender_textview.setText(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        chnagepin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(ProfileConstain_activity.this);
                View view = getLayoutInflater().inflate(R.layout.changepindialog, null);
                bottomSheetDialog.setContentView(view);
                bottomSheetDialog.setCancelable(true);

                final TextView changepincode = (TextView)view.findViewById(R.id.changepincode);
                changepincode.setText(getkey("change_pin_code"));

                final TextView enternewpincode = (TextView)view.findViewById(R.id.enternewpincode);
                enternewpincode.setText(getkey("enter_new_pin_code"));

                final TextView pincon = (TextView)view.findViewById(R.id.pincon);
                pincon.setText(getkey("pinconfirm"));

                final EditText oldpin = (EditText)view.findViewById(R.id.oldpin);
                oldpin.setHint(getkey("enter_ol_pin"));
                final EditText newpin = (EditText)view.findViewById(R.id.newpin);
                LinearLayout layout_booking_confirm=(LinearLayout)view.findViewById(R.id.layout_booking_confirm);
                layout_booking_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(oldpin.getText().toString().equals(""))
                        {
                            Toast.makeText(getApplicationContext(),getkey("enter_ol_pin"),Toast.LENGTH_LONG).show();
                        }
                        else if(newpin.getText().toString().equals(""))
                        {
                            Toast.makeText(getApplicationContext(),getkey("enter_newpin"),Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            isInternetPresent = cd.isConnectingToInternet();
                            try {
                                if (isInternetPresent) {
                                    bottomSheetDialog.dismiss();
                                    PostRequestnewpincode(Iconstant.updatepincode,oldpin.getText().toString(),newpin.getText().toString());

                                } else {
                                    Alert(getkey("action_error"), getkey("alert_nointernet"));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });


                ImageView closedialog = (ImageView)view.findViewById(R.id.closedialog);
                closedialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                    }
                });

                bottomSheetDialog.show();

            }
        });



        myprofile_dialog_form_age_edittext = findViewById(R.id.myprofile_dialog_form_age_edittext);
        myprofile_dialog_form_age_edittext.setHint(getkey("age_txt"));
        //account details
        editaccount = findViewById(R.id.editaccount);
        editaccount.setText(getkey("add_bank"));
        tv_coupon_count = findViewById(R.id.tv_coupon_count);
        tv_coupon_count.setText(getkey("profile_lable"));
        expyearcvv = findViewById(R.id.expyearcvv);
        addcard_number_textview = findViewById(R.id.addcard_number_textview);
        addcard_number_textview.setOnClickListener(this);
        addcard_number_textview.setText(getkey("addcardd"));
        updateaccount = findViewById(R.id.updateaccount);
        updateaccount.setOnClickListener(this);
        updateaccount.setText(getkey("update_bank"));
        editaccount.setOnClickListener(this);
        etaccountnumber = findViewById(R.id.etaccountnumber);
        etaccountnumber.setHint(getkey("accountnumber"));
        editcard = findViewById(R.id.editcard);
        editcard.setOnClickListener(this);
        card_number_textview = findViewById(R.id.card_number_textview);
        card_exp_year_textview = findViewById(R.id.card_exp_year_textview);
        card_exp_cvv_textview = findViewById(R.id.card_exp_cvv_textview);
        accounttype = findViewById(R.id.accounttype);
        accounttype.setHint(getkey("account_holder_name"));
        selectbank = findViewById(R.id.selectbank);
        selectbanktext = findViewById(R.id.selectbanktext);
        selectbanktext.setHint(getkey("selectbank"));

        selectbank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                selectbanktext_postion = position;
                selectbanktext.setText(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //emergencydetails
        editemergencydetails = findViewById(R.id.editemergencydetails);
        editemergencydetails.setOnClickListener(this);
        SOS_dialog_form_city_edittext = findViewById(R.id.SOS_dialog_form_city_edittext);
        SOS_dialog_form_city_edittext.setHint(getkey("city_town"));
        SOS_dialog_form_country_edittext = findViewById(R.id.SOS_dialog_form_country_edittext);
        SOS_dialog_form_country_edittext.setOnClickListener(this);
        SOS_dialog_form_country_edittext.setHint(getkey("country"));
        SOS_dialog_form_name_edittext = findViewById(R.id.SOS_dialog_form_name_edittext);
        SOS_dialog_form_name_edittext.setHint(getkey("name_txt"));
        sosmylogin_dialog_form_country_code_textview = findViewById(R.id.sosmylogin_dialog_form_country_code_textview);
        sosmylogin_dialog_form_country_code_textview.setOnClickListener(this);
        SOS_dialog_form_phone_edittext = findViewById(R.id.SOS_dialog_form_phone_edittext);
        SOS_dialog_form_phone_edittext.setHint(getkey("phone"));
        SOS_dialog_form_email_edittext = findViewById(R.id.SOS_dialog_form_email_edittext);
        SOS_dialog_form_email_edittext.setHint(getkey("email"));
        SOS_dialog_form_relation_edittext = findViewById(R.id.SOS_dialog_form_relation_edittext);
        SOS_dialog_form_relation_edittext.setHint(getkey("relationship"));
        SOS_dialog_form_address_edittext = findViewById(R.id.SOS_dialog_form_address_edittext);
        SOS_dialog_form_address_edittext.setHint(getkey("address"));
        updatesos = findViewById(R.id.updatesos);
        updatesos.setText(getkey("update"));
        updatesos.setOnClickListener(this);

        //contrainlayouts
        logindetails = findViewById(R.id.logindetails);
        accountdetails = findViewById(R.id.accountdetails);
        emergencycontact = findViewById(R.id.emergencycontact);

        //headview
        profile = findViewById(R.id.profile);
        bankcard = findViewById(R.id.bankcard);
        emergency = findViewById(R.id.emergency);

        profile.setText(getkey("profile"));
        bankcard.setText(getkey("bankcard"));
        emergency.setText(getkey("emergency"));

        emergency.setOnClickListener(this);
        bankcard.setOnClickListener(this);
        profile.setOnClickListener(this);
        iv_userbg.setOnClickListener(this);

        session = new SessionManager(ProfileConstain_activity.this);
        appInfoSessionManager = new AppInfoSessionManager(ProfileConstain_activity.this);
        cd = new ConnectionDetector(ProfileConstain_activity.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        HashMap<String, String> app = appInfoSessionManager.getAppInfo();
        sAuthKey = app.get(appInfoSessionManager.KEY_APP_IDENTITY);

        HashMap<String, String> user = session.getUserDetails();
        gcmID = user.get(SessionManager.KEY_GCM_ID);
        picker = CountryPicker.newInstance(getkey("select_country_lable"));
        picker_countrychoose_emergency_country = CountryPicker.newInstance(getkey("select_country_lable"));
        picker_countrychoose_profile = CountryPicker.newInstance(getkey("select_country_lable"));
        picker_countrychoose_emergency = CountryPicker.newInstance(getkey("select_country_lable"));

        Refresh_page();


        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                mylogin_dialog_form_country_code_textview.setText(dialCode);
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(mylogin_dialog_form_mobile_no_edittext.getWindowToken(), 0);
            }
        });

        picker_countrychoose_emergency.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker_countrychoose_emergency.dismiss();
                sosmylogin_dialog_form_country_code_textview.setText(dialCode);
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(mylogin_dialog_form_mobile_no_edittext.getWindowToken(), 0);
            }
        });

        picker_countrychoose_profile.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker_countrychoose_profile.dismiss();
                myprofile_dialog_form_country_edittext.setText(name);
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(mylogin_dialog_form_mobile_no_edittext.getWindowToken(), 0);
            }
        });

        picker_countrychoose_emergency_country.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker_countrychoose_emergency_country.dismiss();
                SOS_dialog_form_country_edittext.setText(name);
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(mylogin_dialog_form_mobile_no_edittext.getWindowToken(), 0);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.updateaccount:
                BankUpdate();
                break;
            case R.id.SOS_dialog_form_country_edittext:
                picker_countrychoose_emergency_country.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
            case R.id.sosmylogin_dialog_form_country_code_textview:
                picker_countrychoose_emergency.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
            case R.id.editemergencydetails:
                SOS_dialog_form_name_edittext.setEnabled(true);
                sosmylogin_dialog_form_country_code_textview.setEnabled(true);
                SOS_dialog_form_phone_edittext.setEnabled(true);
                SOS_dialog_form_email_edittext.setEnabled(true);
                SOS_dialog_form_relation_edittext.setEnabled(true);
                SOS_dialog_form_address_edittext.setEnabled(true);
                SOS_dialog_form_city_edittext.setEnabled(true);
                SOS_dialog_form_country_edittext.setEnabled(true);
                updatesos.setVisibility(View.VISIBLE);

                break;
            case R.id.updatesos:
                SosUpdate();
                break;
            case R.id.myprofile_dialog_form_country_edittext:
                picker_countrychoose_profile.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
            case R.id.addcard_number_textview:
                startActivity(new Intent(ProfileConstain_activity.this, NewProfile_Page_CardEdit.class));
//                finish();
                break;
            case R.id.editcard:
                Intent intent = new Intent(ProfileConstain_activity.this, CardListPage.class);
                startActivity(intent);
                break;
            case R.id.editaccount:
                etaccountnumber.setEnabled(true);
                selectbank.setEnabled(true);
                accounttype.setEnabled(true);
                updateaccount.setVisibility(View.VISIBLE);

//                Intent in = new Intent(ProfileConstain_activity.this, NewProfile_Page_BankEdit.class);
//                startActivity(in);
//                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.mylogin_dialog_form_country_code_textview:
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
            case R.id.editpersonaldetails:
                profile_page_gende_spinner.setEnabled(true);
                myprofile_dialog_form_name_edittext.setEnabled(true);
                myprofile_dialog_form_age_edittext.setEnabled(true);
                myprofile_dialog_form_city_edittext.setEnabled(true);
                myprofile_dialog_form_state_edittext.setEnabled(true);
                myprofile_dialog_form_postcode_edittext.setEnabled(true);
                myprofile_dialog_form_country_edittext.setEnabled(true);

                updateprofile.setClickable(true);
                updateprofile.setEnabled(true);
                updateprofile.setVisibility(View.VISIBLE);


                break;
            case R.id.updateprofile:

                ProfileUpdate();

                break;
            case R.id.editprofile:

                //edit true
                mylogin_dialog_form_emailid_edittext.setEnabled(true);
                mylogin_dialog_form_mobile_no_edittext.setEnabled(true);
                mylogin_dialog_form_pincode_edittext.setEnabled(true);
                mylogin_dialog_form_country_code_textview.setEnabled(true);
                updatelogin.setClickable(true);
                updatelogin.setEnabled(true);
                updatelogin.setVisibility(View.VISIBLE);
                break;
            case R.id.updatelogin:
                //update login details
                //edit false
                OtpUpdate();


                break;
            case R.id.profile:
                logindetails.setVisibility(View.VISIBLE);
                profile.setBackgroundColor(getResources().getColor(R.color.profilebg));
                accountdetails.setVisibility(View.GONE);
                bankcard.setBackgroundColor(getResources().getColor(R.color.white));
                emergencycontact.setVisibility(View.GONE);
                emergency.setBackgroundColor(getResources().getColor(R.color.white));
                tv_coupon_count.setText(getkey("profile_lable"));
                break;
            case R.id.bankcard:
                logindetails.setVisibility(View.GONE);
                profile.setBackgroundColor(getResources().getColor(R.color.white));
                accountdetails.setVisibility(View.VISIBLE);
                bankcard.setBackgroundColor(getResources().getColor(R.color.profilebg));
                emergencycontact.setVisibility(View.GONE);
                emergency.setBackgroundColor(getResources().getColor(R.color.white));
                tv_coupon_count.setText(getkey("bank_card"));
                break;
            case R.id.emergency:
                logindetails.setVisibility(View.GONE);
                profile.setBackgroundColor(getResources().getColor(R.color.white));
                accountdetails.setVisibility(View.GONE);
                bankcard.setBackgroundColor(getResources().getColor(R.color.white));
                emergencycontact.setVisibility(View.VISIBLE);
                emergency.setBackgroundColor(getResources().getColor(R.color.profilebg));
                tv_coupon_count.setText(getkey("emer"));
                break;
            case R.id.iv_userbg:
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()) {
                        requestPermission();
                    } else {
                        chooseimage();
                    }
                } else {
                    chooseimage();
                }
                break;
        }
    }


    public void Refresh_page() {

        isInternetPresent = cd.isConnectingToInternet();
        try {
            if (isInternetPresent) {
                PostRequest(Iconstant.get_profile_detail_url);
//                PostRquestXenditBankList(Iconstant.getPaymentOPtion_url_new);
            } else {
                Alert(getkey("action_error"), getkey("alert_nointernet"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void OtpUpdate() {
        try {

            if (mylogin_dialog_form_emailid_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"), getkey("profile_label_alert_email"));
            } else if (mylogin_dialog_form_country_code_textview.getText().toString().equalsIgnoreCase("code")) {
                AlertError(getkey("action_error"), getkey("login_page_alert_country_code"));
            } else if (mylogin_dialog_form_mobile_no_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"), getkey("login_page_alert_phone"));
            } else if (!isValidPhoneNumber(mylogin_dialog_form_mobile_no_edittext.getText().toString())) {
                AlertError(getkey("action_error"), getkey("pls_enterphone"));
            } else if (mylogin_dialog_form_pincode_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"), getkey("pincodecnt_epmty"));
            } else {
                cd = new ConnectionDetector(ProfileConstain_activity.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (!mylogin_dialog_form_mobile_no_edittext.getText().toString().equalsIgnoreCase(globalmobilenumber)) {

                    if (isInternetPresent) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("user_id", UserID);
                        jsonParams.put("country_code", mylogin_dialog_form_country_code_textview.getText().toString());
                        jsonParams.put("phone_number", mylogin_dialog_form_mobile_no_edittext.getText().toString());
                        isemailchange = false;
                        PostRequest_updateOtp_info(Iconstant.User_Otp_LoginUpdate, jsonParams);
                    } else {
                        Alert(getkey("action_error"),getkey("alert_nointernet"));
                    }
                } else if (!mylogin_dialog_form_emailid_edittext.getText().toString().equalsIgnoreCase(globalemail)) {
                    if (isInternetPresent) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("user_id", UserID);
                        jsonParams.put("email", mylogin_dialog_form_emailid_edittext.getText().toString());
                        isemailchange = true;
                        PostRequest_updateOtp_info(Iconstant.User_Email_LoginUpdate, jsonParams);
                    } else {
                        Alert(getkey("action_error"), getkey("alert_nointernet"));
                    }
                } else {
                    LoginUpdate();
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PostRequest_updateOtp_info(String Url, final HashMap<String, String> jsonParams) {

        final Dialog dialog = new Dialog(ProfileConstain_activity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ServiceRequest mRequest = new ServiceRequest(ProfileConstain_activity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------PostRequest_updateOtp_info reponse-------------------" + response);
                String Sstatus = "", Smessage = "", otp_status = "", globalemail = "", globalmobilenumber = "", Up_home_passcode = "", otp = "", Up_login_phone_no = "", Up_login_country_code = "", Up_home_pho_no = "";
                try {
                    dialog.dismiss();
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (isemailchange) {
                        if (Sstatus.equalsIgnoreCase("1")) {
                            Smessage = object.getString("response");
                            globalemail = object.getJSONObject("user_profile").getString("email");
                            otp_status = object.getJSONObject("user_profile").getString("passcode_show");
                            otp = object.getJSONObject("user_profile").getString("email_passcode");
                        } else {
                            Smessage = object.getString("response");
                        }
                    } else {
                        if (Sstatus.equalsIgnoreCase("1")) {
                            Smessage = object.getString("response");
                            globalmobilenumber = object.getString("phone_number");
                            Up_login_country_code = object.getString("country_code");
                            otp_status = object.getString("otp_status");
                            otp = object.getString("otp");
                            session.setOtpstatusAndPin(otp_status, otp);
                        } else {
                            Smessage = object.getString("response");
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();

                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    if (isemailchange) {
                        if (otp_status.equalsIgnoreCase("1")) {
                            Emailchange(otp);
                        } else {
                            Emailchange("");
                        }
                    } else {
                        if (otp_status.equalsIgnoreCase("development")) {
                            Mobilechange(otp);
                        } else {
                            Mobilechange("");
                        }
                    }


                } else {
                    Alert(getkey("action_error"), Smessage);

                }

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void PostRequest(String Url) {
        final Dialog dialog = new Dialog(ProfileConstain_activity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        System.out.println("-----------Profilepage getdata jsonParams--------------" + jsonParams);

        ServiceRequest mRequest = new ServiceRequest(ProfileConstain_activity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Profilepage getdata reponse-------------------" + response);


                String Sstatus = "", key1_sos_name = "", key1_sos_email = "", key1_sos_mobileno = "", key1_sos_relationship = "", key1_sos_address = "",
                        key2_sos_name = "", key2_sos_email = "", key2_sos_mobileno = "", key2_sos_relationship = "", key2_sos_address = "";

                try {
                    dialog.dismiss();

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject jsonObject = object.getJSONObject("response");
                        JSONObject user_profile_jsonObject = jsonObject.getJSONObject("user_profile");
                        JSONArray user_login_info = jsonObject.getJSONArray("user_login_info");
                        if (user_login_info.length() > 0) {
                            mylogin_dialog_form_emailid_edittext.setText(user_profile_jsonObject.getString("email"));
                            mylogin_dialog_form_mobile_no_edittext.setText(user_profile_jsonObject.getString("phone_number"));
                            globalmobilenumber = user_profile_jsonObject.getString("phone_number");
                            globalemail = user_profile_jsonObject.getString("email");
                            mylogin_dialog_form_pincode_edittext.setText(user_profile_jsonObject.getString("country_code"));
                            mylogin_dialog_form_country_code_textview.setText(user_profile_jsonObject.getString("country_code"));
                            Picasso.with(ProfileConstain_activity.this).load(user_profile_jsonObject.getString("user_image")).error(R.drawable.new_no_user_img).placeholder(R.drawable.new_no_user_img).into(iv_userbg);
                            //edit false
                            mylogin_dialog_form_emailid_edittext.setEnabled(false);
                            mylogin_dialog_form_mobile_no_edittext.setEnabled(false);
                            mylogin_dialog_form_pincode_edittext.setEnabled(false);
                            mylogin_dialog_form_country_code_textview.setEnabled(false);

                            updatelogin.setClickable(false);
                            updatelogin.setEnabled(false);
                        }

                        if (user_profile_jsonObject.length() > 0) {
                            myprofile_dialog_form_name_edittext.setText(user_profile_jsonObject.getString("user_name"));
                            if (!user_profile_jsonObject.getString("gender").equalsIgnoreCase("")) {
                                if (user_profile_jsonObject.getString("gender").equalsIgnoreCase("Male")) {
                                    profile_page_gende_spinner.setSelection(1);
                                } else {
                                    profile_page_gende_spinner.setSelection(2);
                                }
                            }

                            myprofile_dialog_form_age_edittext.setText(user_profile_jsonObject.getString("age"));
                            myprofile_dialog_form_city_edittext.setText(user_profile_jsonObject.getString("locality"));
                            myprofile_dialog_form_state_edittext.setText(user_profile_jsonObject.getString("state"));
                            myprofile_dialog_form_postcode_edittext.setText(user_profile_jsonObject.getString("pincode"));
                            myprofile_dialog_form_country_edittext.setText(user_profile_jsonObject.getString("country"));

                            profile_page_gende_spinner.setEnabled(false);
                            myprofile_dialog_form_name_edittext.setEnabled(false);
                            myprofile_dialog_form_age_edittext.setEnabled(false);
                            myprofile_dialog_form_city_edittext.setEnabled(false);
                            myprofile_dialog_form_state_edittext.setEnabled(false);
                            myprofile_dialog_form_postcode_edittext.setEnabled(false);
                            myprofile_dialog_form_country_edittext.setEnabled(false);

                            updateprofile.setClickable(false);
                            updateprofile.setEnabled(false);
                            updateprofile.setVisibility(View.INVISIBLE);
                        }

                        Object check_object1 = jsonObject.get("card_details");
                        if (check_object1 instanceof JSONArray) {

                            JSONArray cardDetailArray = jsonObject.getJSONArray("card_details");
                            if (cardDetailArray.length() > 0) {
                                editcard.setText(getkey("view_all"));
                                editcard.setVisibility(View.VISIBLE);
                                expyearcvv.setVisibility(View.VISIBLE);
                                card_number_textview.setVisibility(View.VISIBLE);
                                for (int j = 0; j < cardDetailArray.length(); j++) {
                                    JSONObject driver_object = cardDetailArray.getJSONObject(j);
                                    card_number_textview.setText(driver_object.getString("card_number"));
                                    card_exp_year_textview.setText(driver_object.getString("exp_month") + "/" + driver_object.getString("exp_year"));
                                    card_exp_cvv_textview.setText("****");
                                }
                            } else {
                                card_number_textview.setVisibility(View.GONE);
                                editcard.setVisibility(View.INVISIBLE);
                                expyearcvv.setVisibility(View.GONE);
                            }
                        }
                        paymentcardlist = new ArrayList<>();
                        paymentcardNamelist = new ArrayList<>();
                        Object bank_listcheck_object = jsonObject.get("bank_list");
                        if (bank_listcheck_object instanceof JSONArray) {
                            JSONArray payment_list_jsonArray = jsonObject.getJSONArray("bank_list");
                            if (payment_list_jsonArray.length() > 0) {
                                paymentcardlist.clear();
                                paymentcardNamelist.clear();
                                WalletMoneyPojo wmpojos = new WalletMoneyPojo();
                                wmpojos.setXenditBankName("");
                                wmpojos.setXenditBankCode("");
                                wmpojos.setXendiActiveImage("");
                                paymentcardlist.add(wmpojos);
                                paymentcardNamelist.add(mhelper.getvalueforkey("selectbank"));
                                for (int i = 0; i < payment_list_jsonArray.length(); i++) {
                                    JSONObject payment_obj = payment_list_jsonArray.getJSONObject(i);
                                    WalletMoneyPojo wmpojo = new WalletMoneyPojo();
                                    wmpojo.setXenditBankName(payment_obj.getString("name"));
                                    wmpojo.setXenditBankCode(payment_obj.getString("code"));
                                    wmpojo.setXendiActiveImage(payment_obj.getString("image"));
                                    paymentcardlist.add(wmpojo);
                                    paymentcardNamelist.add(payment_obj.getString("name"));
                                }
                            }
                        }

                        CustomSpinnerAdapter customSpinnerbank = new CustomSpinnerAdapter(ProfileConstain_activity.this, paymentcardNamelist);
                        selectbank.setAdapter(customSpinnerbank);

                        Object check_object2 = jsonObject.get("bankingArr");
                        if (check_object2 instanceof JSONObject) {
                            JSONObject bankDetailObj = jsonObject.getJSONObject("bankingArr");
                            if (bankDetailObj.length() > 0) {
                                for (int i = 0; i <= paymentcardNamelist.size() - 1; i++) {
                                    if (paymentcardNamelist.get(i).equalsIgnoreCase(bankDetailObj.getString("bank_name"))) {
                                        selectbank.setSelection(i);
                                        break;
                                    }
                                }
                                etaccountnumber.setText(bankDetailObj.getString("acc_number"));
                                accounttype.setText(bankDetailObj.getString("acc_holder_name"));

                                etaccountnumber.setEnabled(false);
                                accounttype.setEnabled(false);
                                selectbank.setEnabled(false);
                                updateaccount.setVisibility(View.INVISIBLE);
                            } else {
                                etaccountnumber.setEnabled(true);
                                selectbank.setEnabled(true);
                                accounttype.setEnabled(true);
                                updateaccount.setVisibility(View.VISIBLE);
                            }
                        }


                        Object check_object = jsonObject.get("emergency_contact");
                        if (check_object instanceof JSONObject) {

                            JSONObject emergency_contact_jsonObject = jsonObject.getJSONObject("emergency_contact");
                            if (emergency_contact_jsonObject.length() > 0) {

                                if (emergency_contact_jsonObject.has("key_1")) {
                                    JSONObject emergency_contactkey_1_jsonObject = emergency_contact_jsonObject.getJSONObject("key_1");
                                    if (emergency_contactkey_1_jsonObject.length() > 0) {
                                        updatesos.setVisibility(View.INVISIBLE);
                                        SOS_dialog_form_name_edittext.setEnabled(false);
                                        sosmylogin_dialog_form_country_code_textview.setEnabled(false);
                                        SOS_dialog_form_phone_edittext.setEnabled(false);
                                        SOS_dialog_form_email_edittext.setEnabled(false);
                                        SOS_dialog_form_relation_edittext.setEnabled(false);
                                        SOS_dialog_form_address_edittext.setEnabled(false);
                                        SOS_dialog_form_city_edittext.setEnabled(false);
                                        SOS_dialog_form_country_edittext.setEnabled(false);
                                        SOS_dialog_form_name_edittext.setText(emergency_contactkey_1_jsonObject.getString("name"));
                                        sosmylogin_dialog_form_country_code_textview.setText(emergency_contactkey_1_jsonObject.getString("code"));
                                        SOS_dialog_form_phone_edittext.setText(emergency_contactkey_1_jsonObject.getString("mobile"));
                                        SOS_dialog_form_email_edittext.setText(emergency_contactkey_1_jsonObject.getString("email"));
                                        SOS_dialog_form_relation_edittext.setText(emergency_contactkey_1_jsonObject.getString("relationship"));
                                        SOS_dialog_form_address_edittext.setText(emergency_contactkey_1_jsonObject.getString("address"));
                                        SOS_dialog_form_city_edittext.setText(emergency_contactkey_1_jsonObject.getString("city"));
                                        SOS_dialog_form_country_edittext.setText(emergency_contactkey_1_jsonObject.getString("country"));

                                        if (emergency_contact_jsonObject.has("key_2")) {
                                            JSONObject emergency_contactkey_2_jsonObject = emergency_contact_jsonObject.getJSONObject("key_2");
                                            if (emergency_contactkey_2_jsonObject.length() > 0) {
                                                SOS_dialog_form_name_edittext.setText(emergency_contactkey_2_jsonObject.getString("name"));
                                                sosmylogin_dialog_form_country_code_textview.setText(emergency_contactkey_2_jsonObject.getString("code"));
                                                SOS_dialog_form_phone_edittext.setText(emergency_contactkey_2_jsonObject.getString("mobile"));
                                                SOS_dialog_form_email_edittext.setText(emergency_contactkey_2_jsonObject.getString("email"));
                                                SOS_dialog_form_relation_edittext.setText(emergency_contactkey_2_jsonObject.getString("relationship"));
                                                SOS_dialog_form_address_edittext.setText(emergency_contactkey_2_jsonObject.getString("address"));
                                                SOS_dialog_form_city_edittext.setText(emergency_contactkey_2_jsonObject.getString("city"));
                                                SOS_dialog_form_country_edittext.setText(emergency_contactkey_2_jsonObject.getString("country"));
                                            }
                                        }
                                    } else {
                                        SOS_dialog_form_name_edittext.setEnabled(true);
                                        sosmylogin_dialog_form_country_code_textview.setEnabled(true);
                                        SOS_dialog_form_phone_edittext.setEnabled(true);
                                        SOS_dialog_form_email_edittext.setEnabled(true);
                                        SOS_dialog_form_relation_edittext.setEnabled(true);
                                        SOS_dialog_form_address_edittext.setEnabled(true);
                                        SOS_dialog_form_city_edittext.setEnabled(true);
                                        SOS_dialog_form_country_edittext.setEnabled(true);
                                        updatesos.setVisibility(View.VISIBLE);
                                    }
                                }
                            }

                        }


                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();

                }


                if (Sstatus.equalsIgnoreCase("1")) {
                } else {
                    Alert(getkey("action_error"), "");
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void PostRequestnewpincode(String Url,String oldpin,String newpin) {
        final Dialog dialog = new Dialog(ProfileConstain_activity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("pin", oldpin);
        jsonParams.put("new_pin", newpin);
        System.out.println("-----------Profilepage getdata jsonParams--------------" + jsonParams);

        ServiceRequest mRequest = new ServiceRequest(ProfileConstain_activity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Profilepage getdata reponse-------------------" + response);

                dialog.dismiss();
                String Sstatus = "", key1_sos_name = "", key1_sos_email = "", key1_sos_mobileno = "", key1_sos_relationship = "", key1_sos_address = "",
                        key2_sos_name = "", key2_sos_email = "", key2_sos_mobileno = "", key2_sos_relationship = "", key2_sos_address = "";

                try {


                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    response = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Alert(getkey("action_success"), response);
                    } else {
                        Alert(getkey("action_error"), response);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();

                }



            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void Alert(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(ProfileConstain_activity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void Emailchange(String otp) {

        final Emailchange mDialog = new Emailchange(ProfileConstain_activity.this);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(otp, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog.getenteredotp().equalsIgnoreCase(session.getOtpPin())) {
                    mDialog.dismiss();
//                    LoginUpdate();
                } else {
                    Alert(getkey("alert_label_title"), getkey("enterdinvalidotp"));
                }

            }
        });
        mDialog.show();
    }

    private void Mobilechange(String otp){
        final Mobilenochange mDialog = new Mobilenochange(ProfileConstain_activity.this);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(otp, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog.getenteredotp().equalsIgnoreCase(session.getOtpPin())) {
                    mDialog.dismiss();
                    LoginUpdate();
                } else {
                    Alert(getkey("alert_label_title"),getkey("enterdinvalidotp"));
                }

            }
        });
        mDialog.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (updateReceiver != null) {
            unregisterReceiver(updateReceiver);
            updateReceiver = null;
        }
    }


    private void takePicture() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        camera_FileUri = getOutputMediaFileUri(1);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, camera_FileUri);
//        // start the image capture Intent
//        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
        try {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                //Create a file to store the image
                imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), appDirectoryName);
            }
            if (imageRoot != null) {
                String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                destination = new File(imageRoot, name + ".jpg");
                Uri photoURI = FileProvider.getUriForFile(this, "com.provider", destination);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                startActivityForResult(pictureIntent,
                        REQUEST_TAKE_PHOTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, galleryRequestCode);
    }

    public String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", camera_FileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // get the file url
        camera_FileUri = savedInstanceState.getParcelable("file_uri");
    }

    private void onCaptureImageResult(Intent data) {
        try {
            String imagePath = destination.getAbsolutePath();
            camera_FileUri = Uri.fromFile(new File(imagePath));
            outputUri = mImageCaptureUri;

//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);

            UCrop.Options Uoptions = new UCrop.Options();
            Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
            Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
            Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));


            try {
                UCrop.of(camera_FileUri, camera_FileUri)
                        .withAspectRatio(4, 4)
                        .withMaxResultSize(8000, 8000)
                        .start(ProfileConstain_activity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onCroppedImageResult(Intent data) {

        final Uri resultUri = UCrop.getOutput(data);

        Log.d("Crop success", "" + resultUri);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
            Bitmap thumbnail = bitmap;
            final String picturePath = resultUri.getPath();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            File curFile = new File(picturePath);
            try {
                ExifInterface exif = new ExifInterface(curFile.getPath());
                int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationInDegrees = exifToDegrees(rotation);

                Matrix matrix = new Matrix();
                if (rotation != 0f) {
                    matrix.preRotate(rotationInDegrees);
                }
                thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (IOException ex) {
                Log.e("TAG", "Failed to get Exif data", ex);
            }

            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            byteArray = byteArrayOutputStream.toByteArray();

            if (isInternetPresent) {
                bitMapThumbnail = thumbnail;
                iv_userbg.setImageBitmap(thumbnail);

                if(session.getimagestatus().equals("1"))
                {
                    uploadToS3Bucket(curFile);
                }
                else
                {
                    UploadDriverImage(Iconstant.Edit_profile_image_url);
                }


            } else {
                Alert(getkey("alert_nointernet"), getkey("alert_nointernet_message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void uploadToS3Bucket(File fileUri)
    {
        String[] path_split;
        if (fileUri != null) {

            final Dialog dialog = new Dialog(ProfileConstain_activity.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            uploaded_file_name = "";
            String path = String.valueOf(fileUri);
            String file_name_full = path.substring(path.lastIndexOf("/") + 1);
            path_split = file_name_full.split(".");
            if (path_split.length == 2) {
                File_names = path_split[0];
                Extension = path_split[1];

            } else {
                path_split = file_name_full.split(".jpg");
                if (path_split.length == 1) {
                    File_names = path_split[0];
                }
            }

            String file_name_upload = File_names;
            //current time stamp
            Long tsLong = System.currentTimeMillis() / 1000;
            String str_current_ts = tsLong.toString();
            String concat_image_name = file_name_upload + "" + str_current_ts;
            final String fileName = md5Encryption(concat_image_name) + "." + "jpg";


            TransferUtility transferUtility =
                    TransferUtility.builder()
                            .context(getApplicationContext())
                            .defaultBucket(s3_bucket_name)
                            .s3Client(s3Client)
                            .build();
            uploaded_file_name = fileName;

            uploadObserver = transferUtility.upload("images/users/" + fileName, fileUri, CannedAccessControlList.PublicRead);
            // Attach a listener to the observer to get state update and progress notifications
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    // Handle a completed upload.
                    if (TransferState.COMPLETED == state) {
                        String path = uploadObserver.getAbsoluteFilePath();
                        session.setUserImageUpdate(path);
                        session.setUserImageName(fileName);
                        dialog.dismiss();
                        UploadDriverImage(Iconstant.Edit_profile_image_url);
                    } else if (TransferState.FAILED == state) {
                        // dialog.dismiss();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                    Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
                }

                @Override
                public void onError(int id, Exception ex) {
                    // Handle errors
                    ex.printStackTrace();
                    uploaded_file_name = "";

                    dialog.dismiss();
                }

            });

            // If you prefer to poll for the data, instead of attaching a
            // listener, check for the state and progress in the observer.
            if (TransferState.COMPLETED == uploadObserver.getState()) {
                // Handle a completed upload.
                Log.d("completed upload", "completed upload");
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onCroppedImageResult(data);
            } else if (requestCode == galleryRequestCode) {

                try {
                    Uri selectedImage = data.getData();
                    if (selectedImage.toString().startsWith("content://com.sec.android.gallery3d.provider")) {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        final String picturePath = c.getString(columnIndex);
                        c.close();

                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                        Bitmap thumbnail = bitmap; //getResizedBitmap(bitmap, 600);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        File curFile = new File(picturePath);

                        String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                        curFile = storeImage(thumbnail,name);

                        UCrop.Options Uoptions = new UCrop.Options();
                        Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));


                        try {
                            UCrop.of(Uri.fromFile(curFile),Uri.fromFile(curFile))
                                    .withAspectRatio(4, 4)
                                    .withMaxResultSize(8000, 8000)
                                    .start(ProfileConstain_activity.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();

                        int columnIndex = c.getColumnIndex(filePath[0]);
                        final String picturePath = c.getString(columnIndex);
                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                        Bitmap thumbnail = bitmap; //getResizedBitmap(bitmap, 600);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        File curFile = new File(picturePath);
                        String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                        curFile = storeImage(thumbnail,name);
                        UCrop.Options Uoptions = new UCrop.Options();
                        Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));


                        try {
                            UCrop.of(Uri.fromFile(curFile),Uri.fromFile(curFile))
                                    .withAspectRatio(4, 4)
                                    .withMaxResultSize(8000, 8000)
                                    .start(ProfileConstain_activity.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } catch (OutOfMemoryError | NullPointerException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == mobno_change_request_code && data != null) {
                String Scountrycode = data.getStringExtra("countryCode");
                String SmobNo = data.getStringExtra("phoneNo");
                String Smessage = data.getStringExtra("message");
                Alert(getkey("action_success"), Smessage);
            }

        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private void UploadDriverImage(String url) {

        final Dialog dialog = new Dialog(ProfileConstain_activity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                System.out.println("------------- image response-----------------" + response.data);
                dialog.dismiss();

                String resultResponse = new String(response.data);
                System.out.println("-------------  response-----------------" + resultResponse);
                String sStatus = "", sResponse = "", SUser_image = "", Smsg = "", name = "";
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    Smsg = jsonObject.getString("response");
                    if (sStatus.equalsIgnoreCase("1")) {
                        SUser_image = jsonObject.getString("image_url");
//                        name = jsonObject.getString("picture_name");
                        iv_userbg.setImageBitmap(bitMapThumbnail);
                        if(!SUser_image.equals("")){
                            session.setUserImageUpdate(SUser_image);
                        }

//                        session.setUserImageName(name);
                        Alert(getkey("action_success"), Smsg);
                    } else {
                        Alert(getkey("action_error"), Smsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserID);
                if(session.getimagestatus().equals("1"))
                {
                    params.put("user_image", session.getUserImageName());
                }
                System.out.println("user_id---------------" + UserID);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                if(!session.getimagestatus().equals("1"))
                {
                    params.put("user_image", new DataPart("blackmaria.jpg", byteArray));
                }
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authkey", sAuthKey);
                headers.put("User-agent", Iconstant.cabily_userAgent);
                headers.put("isapplication", Iconstant.cabily_IsApplication);
                headers.put("applanguage", Iconstant.cabily_AppLanguage);
                headers.put("apptype", Iconstant.cabily_AppType);
                headers.put("userid", UserID);
                headers.put("apptoken", gcmID);
                System.out.println("------------header---------" + headers);
                return headers;
            }

        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }

    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseimage();
                } else {
                    finish();
                }
                break;
        }
    }

    private void chooseimage() {
        final Dialog photo_dialog = new Dialog(ProfileConstain_activity.this);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        photo_dialog.setCanceledOnTouchOutside(true);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);


        TextView titles = (TextView) photo_dialog
                .findViewById(R.id.titles);
        TextView cameratext = (TextView) photo_dialog
                .findViewById(R.id.camera);
        TextView galleryy = (TextView) photo_dialog
                .findViewById(R.id.galleryy);

        titles.setText(getkey("takephoto"));
        cameratext.setText(getkey("camra"));
        galleryy.setText(getkey("existingcamera"));

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                photo_dialog.dismiss();
            }
        });
    }

    private void LoginUpdate() {
        try {
            cd = new ConnectionDetector(ProfileConstain_activity.this);
            isInternetPresent = cd.isConnectingToInternet();
            if (mylogin_dialog_form_emailid_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"), getkey("profile_label_alert_email"));
            } else if (mylogin_dialog_form_country_code_textview.getText().toString().equalsIgnoreCase("code")) {
                AlertError(getkey("action_error"), getkey("login_page_alert_country_code"));
            } else if (mylogin_dialog_form_mobile_no_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"), getkey("pls_enter_mobile_num"));
            } else if (!isValidPhoneNumber(mylogin_dialog_form_mobile_no_edittext.getText().toString())) {
                AlertError(getkey("action_error"), getkey("profile_lable_error_mobile"));
            } else if (mylogin_dialog_form_pincode_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"),getkey("pin_cnt_empy"));
            } else {
                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("mode", "update");
                    jsonParams.put("user_id", UserID);
                    jsonParams.put("email", mylogin_dialog_form_emailid_edittext.getText().toString());
                    jsonParams.put("country_code", mylogin_dialog_form_country_code_textview.getText().toString());
                    jsonParams.put("phone_number", mylogin_dialog_form_mobile_no_edittext.getText().toString());
                    jsonParams.put("passcode", session.getSecurePin());

                    PostRequest_updatelogin_info(Iconstant.userprofile_update_login_info_url, jsonParams);

                } else {
                    Alert(getkey("action_error"), getkey("alert_nointernet"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(ProfileConstain_activity.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }


    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }


    private void PostRequest_updatelogin_info(String Url, final HashMap<String, String> jsonParams) {

        final Dialog dialog = new Dialog(ProfileConstain_activity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        ServiceRequest mRequest = new ServiceRequest(ProfileConstain_activity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------updateProfile reponse-------------------" + response);
                String Sstatus = "", Smessage = "", Up_login_user_name = "", Up_home_passcode = "", Up_home_email_id = "", Up_login_phone_no = "", Up_login_country_code = "", Up_home_pho_no = "";
                try {
                    dialog.dismiss();
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");
                        JSONObject jsonObject_userprofile = object.getJSONObject("user_profile");
                        if (jsonObject_userprofile.length() > 0) {
                            globalmobilenumber = jsonObject_userprofile.getString("phone_number");
                            Up_login_country_code = jsonObject_userprofile.getString("country_code");
                            globalemail = jsonObject_userprofile.getString("email");
                            Up_home_passcode = jsonObject_userprofile.getString("passcode");
                            session.setSecurePin(Up_home_passcode);

                        }
                    } else {
                        Smessage = object.getString("response");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();

                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    Alert(getkey("action_success"), Smessage);
                    mylogin_dialog_form_emailid_edittext.setEnabled(false);
                    mylogin_dialog_form_mobile_no_edittext.setEnabled(false);
                    mylogin_dialog_form_pincode_edittext.setEnabled(false);
                    mylogin_dialog_form_country_code_textview.setEnabled(false);

                    updatelogin.setClickable(false);
                    updatelogin.setEnabled(false);
                    updatelogin.setVisibility(View.INVISIBLE);
                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void ProfileUpdate() {
        try {
            if (myprofile_dialog_form_name_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"), getkey("profile_label_alert_username"));
            } else if (profile_page_gende_spinner.getSelectedItem().equals("Gender")) {
                AlertError(getkey("action_error"), getkey("profile_label_alert_gender1"));
            } else if (myprofile_dialog_form_age_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"), getkey("profile_label_alert_age"));
            } else if (myprofile_dialog_form_city_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"),getkey("profile_label_alert_cityname"));
            } else if (myprofile_dialog_form_state_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"), getkey("profile_label_alert_state"));
            } else if (myprofile_dialog_form_postcode_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"),getkey("profile_label_alert_pincode"));
            } else if (myprofile_dialog_form_country_edittext.getText().toString().length() == 0) {
                AlertError(getkey("action_error"),getkey("profile_label_alert_countryname"));
            } else {
                cd = new ConnectionDetector(ProfileConstain_activity.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", UserID);
                    jsonParams.put("user_name", myprofile_dialog_form_name_edittext.getText().toString());
                    jsonParams.put("email", mylogin_dialog_form_emailid_edittext.getText().toString());
                    jsonParams.put("gender", profile_page_gende_spinner.getSelectedItem().toString());
                    jsonParams.put("age", myprofile_dialog_form_age_edittext.getText().toString());
                    jsonParams.put("locality", myprofile_dialog_form_city_edittext.getText().toString());
                    jsonParams.put("district", myprofile_dialog_form_city_edittext.getText().toString());
                    jsonParams.put("state", myprofile_dialog_form_state_edittext.getText().toString());
                    jsonParams.put("pincode", myprofile_dialog_form_postcode_edittext.getText().toString());
                    jsonParams.put("country", myprofile_dialog_form_country_edittext.getText().toString());
                    jsonParams.put("mode", "update");

                    PostRequest_updateProfile(Iconstant.userprofile_update_user_info_url, jsonParams);

                } else {
                    Alert(getkey("action_error"), getkey("alert_nointernet"));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void PostRequest_updateProfile(String Url, final HashMap<String, String> jsonParams) {

        final Dialog dialog = new Dialog(ProfileConstain_activity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        System.out.println("--------------updateProfile-------------------" + Url);
        System.out.println("--------------updateProfile jsonParams-------------------" + jsonParams);

        ServiceRequest mRequest = new ServiceRequest(ProfileConstain_activity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                dialog.dismiss();
                String Sstatus = "", Smessage = "", Up_user_name = "", Up_email = "", Up_unique_code = "", Up_gender = "", Up_age = "", Up_locality = "", Up_state = "", Up_district = "", Up_pincode = "", Up_country = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");

                    } else {
                        Smessage = object.getString("message");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialog.dismiss();
                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    myprofile_dialog_form_name_edittext.setEnabled(false);
                    profile_page_gende_spinner.setEnabled(false);
                    myprofile_dialog_form_age_edittext.setEnabled(false);
                    myprofile_dialog_form_city_edittext.setEnabled(false);
                    myprofile_dialog_form_state_edittext.setEnabled(false);
                    myprofile_dialog_form_postcode_edittext.setEnabled(false);
                    myprofile_dialog_form_country_edittext.setEnabled(false);

                    updateprofile.setClickable(false);
                    updateprofile.setEnabled(false);
                    updateprofile.setVisibility(View.INVISIBLE);
                    Alert(getkey("profile_changes"), Smessage);
                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void SosUpdate() {
        try {


            if (SOS_dialog_form_name_edittext.getText().toString().length() == 0) {
                erroredit(SOS_dialog_form_name_edittext, getkey("profile_label_alert_username"));
            } else if (!isValidEmail(SOS_dialog_form_email_edittext.getText().toString().trim().replace(" ", ""))) {
                erroredit(SOS_dialog_form_email_edittext, getkey("profile_label_alert_email"));
            } else if (sosmylogin_dialog_form_country_code_textview.getText().toString().trim().length() == 0) {
                AlertError(getkey("info_lable"), getkey("profile_label_alert_countrycode"));
            } else if (!isValidPhoneNumber(SOS_dialog_form_phone_edittext.getText().toString())) {
                erroredit(SOS_dialog_form_phone_edittext, getkey("profile_lable_error_mobile"));
            } else if (SOS_dialog_form_relation_edittext.getText().toString().length() == 0) {
                erroredit(SOS_dialog_form_relation_edittext, getkey("profile_label_alert_relationship"));
            } else if (SOS_dialog_form_address_edittext.getText().toString().length() == 0) {
                erroredit(SOS_dialog_form_address_edittext, getkey("profile_label_alert_address"));
            } else if (SOS_dialog_form_city_edittext.getText().toString().length() == 0) {
                erroredit(SOS_dialog_form_city_edittext, getkey("profile_label_alert_cityname"));
            } else if (SOS_dialog_form_country_edittext.getText().toString().length() == 0) {
                AlertError("Error", getkey("profile_label_alert_countryname"));
            } else {

                cd = new ConnectionDetector(ProfileConstain_activity.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {


                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", UserID);
                    jsonParams.put("mode", "update");

                    jsonParams.put("edit_index", "key_1");
                    jsonParams.put("emergency_conatct[key_1][em_name]", SOS_dialog_form_name_edittext.getText().toString());
                    jsonParams.put("emergency_conatct[key_1][em_email]", SOS_dialog_form_email_edittext.getText().toString());
                    jsonParams.put("emergency_conatct[key_1][em_mobile]", SOS_dialog_form_phone_edittext.getText().toString());
                    jsonParams.put("emergency_conatct[key_1][em_mobile_code]", sosmylogin_dialog_form_country_code_textview.getText().toString());
                    jsonParams.put("emergency_conatct[key_1][em_realtionship]", SOS_dialog_form_relation_edittext.getText().toString());
                    jsonParams.put("emergency_conatct[key_1][em_address]", SOS_dialog_form_address_edittext.getText().toString());
                    jsonParams.put("emergency_conatct[key_1][em_city]", SOS_dialog_form_city_edittext.getText().toString());
                    jsonParams.put("emergency_conatct[key_1][em_state]", "");
                    jsonParams.put("emergency_conatct[key_1][em_postcode]", "");
                    jsonParams.put("emergency_conatct[key_1][em_country]", SOS_dialog_form_country_edittext.getText().toString());


                    System.out.println("-----------sos jsonParams--------------" + jsonParams);
                    PostRequest_updatesos_info(Iconstant.userprofile_update_sos_info_url, jsonParams, "");
                } else {
                    Alert(getkey("action_error"), getkey("alert_nointernet"));
                }


            }


        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void PostRequest_updatesos_info(String Url, final HashMap<String, String> jsonParams, final String sos_key) {

        final Dialog dialog = new Dialog(ProfileConstain_activity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ServiceRequest mRequest = new ServiceRequest(ProfileConstain_activity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
//                Log.e("registr", response);
                System.out.println("--------------updateProfile reponse-------------------" + response);

                String Sstatus = "", Smessage = "", Up_login_user_name = "", Up_login_phone_no = "", Up_login_country_code = "", Up_home_pho_no = "";


                String key1_sos_name = "", key1_sos_email = "", key1_sos_mobileno = "", key1_sos_relationship = "", key1_sos_address = "",
                        key2_sos_name = "", key2_sos_email = "", key2_sos_mobileno = "", key2_sos_relationship = "", key2_sos_address = "";
                try {
                    dialog.dismiss();
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject jsonObject = object.getJSONObject("response");
                        Smessage = jsonObject.getString("message");

                    } else {
                        Smessage = object.getString("response");
                    }
                } catch (JSONException e) {
                    dialog.dismiss();
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    SOS_dialog_form_name_edittext.setEnabled(false);
                    sosmylogin_dialog_form_country_code_textview.setEnabled(false);
                    SOS_dialog_form_phone_edittext.setEnabled(false);
                    SOS_dialog_form_email_edittext.setEnabled(false);
                    SOS_dialog_form_relation_edittext.setEnabled(false);
                    SOS_dialog_form_address_edittext.setEnabled(false);
                    SOS_dialog_form_city_edittext.setEnabled(false);
                    SOS_dialog_form_country_edittext.setEnabled(false);
                    updatesos.setVisibility(View.INVISIBLE);
                    Alert(getkey("action_success"), Smessage);
                } else {
                    updatesos.setVisibility(View.VISIBLE);
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(ProfileConstain_activity.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    private void BankUpdate() {

        if (accounttype.getText().toString().trim().length() <= 0) {
            AlertError(getkey("action_error"), getkey("valid_card_holder_name"));
        } else if (etaccountnumber.getText().toString().trim().length() <= 0) {
            AlertError(getkey("action_error"),getkey("valid_card_no_enter"));
        } else if (selectbanktext.getText().toString().trim().equals("Select Bank")) {
            AlertError(getkey("action_error"), getkey("valid_bank"));
        } else {
            String bankCode = paymentcardlist.get(selectbanktext_postion).getXenditBankCode();
            String bankName = paymentcardlist.get(selectbanktext_postion).getXenditBankName();

            if (isInternetPresent) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);
                jsonParams.put("acc_holder_name", accounttype.getText().toString());
                jsonParams.put("acc_number", etaccountnumber.getText().toString());
                jsonParams.put("bank_name", bankName);
                jsonParams.put("bank_code", bankCode);
                PostRquestXenditBanDetailSave(Iconstant.getXendit_bank_save_url, jsonParams);
            } else {

                Alert(getkey("action_error"),getkey("alert_nointernet"));
            }
        }
    }

    private void PostRquestXenditBanDetailSave(String Url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(ProfileConstain_activity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ServiceRequest mRequest = new ServiceRequest(ProfileConstain_activity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                String Sstatus = "", Smessage = "", acc_holder_name = "", acc_number = "", bank_name = "", bank_code = "";
                try {
                    dialog.dismiss();
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");
                        if (jsonObject.has("banking")) {

                            JSONObject banking = jsonObject.getJSONObject("banking");
                            if (banking.length() > 0) {
                                acc_holder_name = banking.getString("acc_holder_name");
                                acc_number = banking.getString("acc_number");
                                bank_name = banking.getString("bank_name");
                                bank_code = banking.getString("bank_code");
                            }
                        }

                    } else {
                        Smessage = object.getString("response");
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialog.dismiss();

                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    etaccountnumber.setEnabled(false);
                    selectbank.setEnabled(false);
                    accounttype.setEnabled(false);
                    updateaccount.setVisibility(View.INVISIBLE);
                    Alert(getkey("action_success"),  getkey("bank_details_updated"));
                } else {
                    etaccountnumber.setEnabled(true);
                    selectbank.setEnabled(true);
                    accounttype.setEnabled(true);
                    updateaccount.setVisibility(View.VISIBLE);
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });


    }

    public String md5Encryption(String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void createFile(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
    private File storeImage(Bitmap image,String imagename) {
        File myDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), appDirectoryName);
        myDir.mkdirs();

        String fname = imagename+".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

            if (file.exists ())
            {
                System.out.println("sucess");
            }
            else
            {
                System.out.println("failed");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }
}
