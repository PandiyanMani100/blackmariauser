package com.blackmaria.newdesigns;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.MyDialogFragment;
import com.blackmaria.Saveplusdialog;
import com.blackmaria.adapter.CarListAdapter;
import com.blackmaria.adapter.CustomSpinnerAdapter;
import com.blackmaria.adapter.HelpSpinnerAdapter;

import com.blackmaria.Commonwebviewpage;
import com.blackmaria.Complaint_Page_new;
import com.blackmaria.FareBreakUp;
import com.blackmaria.hockeyapp.FragmentHockeyApp;
import com.blackmaria.InfoPage;
import com.blackmaria.InterFace.CallBack;
import com.blackmaria.InterFace.Slider;
import com.blackmaria.MilesHome;
import com.blackmaria.Navigation_new;

import com.blackmaria.newdesigns.saveplus.Checkpattern;
import com.blackmaria.newdesigns.saveplus.Confirmpatern_activity;
import com.blackmaria.newdesigns.saveplus.Saveplus_manual;
import com.blackmaria.newdesigns.saveplus.Saveplus_sender_sendmoney;
import com.blackmaria.newdesigns.saveplus.Scanqrcode;

import com.blackmaria.newdesigns.sos.Sosdragmap_screen;
import com.blackmaria.newdesigns.view.Recharge.Card.RechargecardpaymentConstrain;
import com.blackmaria.newdesigns.fastwallet.Fastpaypincheck;
import com.blackmaria.pojo.BookingPojo1;
import com.blackmaria.pojo.Homepage_pojo;
import com.blackmaria.pojo.bookinpage;
import com.blackmaria.pojo.clickdisabledashboardpojo;
import com.blackmaria.pojo.setuserimageindashboard;
import com.blackmaria.R;
import com.blackmaria.RideListHomePage;
import com.blackmaria.SearchByNearHome;
import com.blackmaria.SelectVehicleCategoryPage;
import com.blackmaria.SignUpPage;
import com.blackmaria.SplashPage1;
import com.blackmaria.TrackRidePage;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.GeocoderHelper;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.WaitingTimePage;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.walletdialog;
import com.blackmaria.welcomeplusdialog;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.PkDialogtryagain;
import com.blackmaria.xmpp.XmppService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;


import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.danikula.videocache.HttpProxyCache;
import com.devspark.appmsg.AppMsg;
import com.github.ybq.android.spinkit.SpinKitView;


import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;


import me.relex.circleindicator.CircleIndicator2;

import static android.content.Context.CLIPBOARD_SERVICE;

public class Homepage_fragment extends FragmentHockeyApp implements View.OnClickListener {

LanguageDb mhelper;
    private ImageView iv_menu;
    LinearLayout rl_findplaces,tv_help;
    private RelativeLayout  rl_showmelayout, rl_fastwallet, rl_saveplus, rl_trips, rl_km, rl_sos, rl_comp, rl_message;
    private TextView nearyby_btn,tv_username, tv_sample, tv_near_vehicles, tv_dollar, tv_walletamount, tv_tripsount, tv_kilometer, tv_km, tv_comp_noti, tv_message_noti;
    private SpinKitView spinKitView, spin_kit1;
    private Button showme;
    ImageView newprofilepick;
    private SessionManager session;
    CircleIndicator2 indicator;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    HashMap<String, String> info;
    private ServiceRequest mRequest;
    final int PERMISSION_REQUEST_CODE = 111;
    BroadcastReceiver updateReceiver, updateReceiver1;
    private RefreshReceiver refreshReceiver;
    HttpProxyCache proxyCache;
    private boolean isFirstime = true;
    IntentFilter filter, fileter2;
    final Handler handler = new Handler();
    ShapeableImageView setbac;
    private Context Mcontext;
    private PkDialog wholedialog;
    private Dialog trackdialog;
    LinearLayoutManager layoutManager;
    private String UserID = "", Username = "", UserProfileUpdateStatus = "", basicUserProfileUpdateStatus = "", homeAnimImage = "";
    private String RideID = "", onGoingTxt = "", Ssupport_no = "", driverRequestMode = "", onGoingRideId = "", currencyconversion = "", xendit_key = "", payment_mode = "", ride_status = "", ride_amount = "", currency = "", insertAmount = "";
    private String completeStatus = "", CompleteStage = "", waitingtime;
    private String gender = "", relationship = "";


    int stopPosition = 0;
    RecyclerView recyclerView;
    //imageslider

    private int currentPage = 0;
    private int NUM_PAGES = 0;
    private final Integer[] IMAGES = {R.drawable.cars_home, R.drawable.cars_home, R.drawable.cars_home, R.drawable.cars_home};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private Timer swipeTimer;
    private boolean isTimerstarted = false;
    private GPSTracker gpsTracker;
    ImageView leftarrow;
    private ArrayList<BookingPojo1> driver_list;
    private Homepage_pojo homepage;
    private String SpickupLocation = "", Slattitude = "", Slogitude = "";
    private boolean isemergencyexist = false;
    private boolean isdraweropen = false;
    public View parentView;


    Slider slider = new Slider() {
        @Override
        public void onComplete(int position, Homepage_pojo.Booking_banner_arr booking_banner_arr) {
            if (isdraweropen) {
                if (position == 0) {
                    if (isTimerstarted) {
                        swipeTimer.cancel();
                    }

                    session.setContinueTrip("no");

                    if (!isemergencyexist) {
                        help_popup();
                    } else {
                        Intent intent = new Intent(getActivity(), SelectVehicleCategoryPage.class);
                        intent.putExtra("pickupAddress", SpickupLocation);
                        intent.putExtra("pickupLatitude", String.valueOf(Slattitude));
                        intent.putExtra("pickupLongitude", String.valueOf(Slogitude));
                        intent.putExtra("destinationAddress", "");
                        intent.putExtra("destinationLatitude", "");
                        intent.putExtra("destinationLongitude", "");
                        intent.putExtra("page", "BookingPage1");
                        intent.putExtra("intentCall", "normalBooking");
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                } else {
                    String url = booking_banner_arr.getUrl();
                    if (url.length() > 0) {



                        Intent intent = new Intent(getActivity(), Commonwebviewpage.class);                        intent.putExtra("pickupAddress", SpickupLocation);
                        intent.putExtra("url",url);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

                       /* try {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        } catch (ActivityNotFoundException e) {

                        }*/
                    } else {
                        Alert("Error", getkey("link_not_provided"));
                    }

                }
            }
        }
    };


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(getActivity(), Navigation_new.class);
                        getActivity().startActivity(intent1);
                        getActivity().finish();
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(getActivity(), FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        getActivity().startActivity(intent1);
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                        getActivity().finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(getActivity(), FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        getActivity().startActivity(intent1);
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                        getActivity().finish();
                    }
                }
            }
        }
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        parentView = inflater.inflate(R.layout.newhomepagedesign, container, false);
        mhelper = new LanguageDb(getActivity());
        initView(parentView);


        return parentView;
    }



    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager)getActivity(). getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getActivity().getCurrentFocus()) ? null : getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init_slidebanner(View view, ArrayList<Homepage_pojo.Booking_banner_arr> booking_banner_arr) {


        ArrayList<bookinpage> version = new ArrayList<bookinpage>();
        for (int i = 0; i < IMAGES.length; i++)
        {
            ImagesArray.add(IMAGES[i]);
            bookinpage ee = new bookinpage();
            ee.setName("");
            ee.setImage(""+IMAGES[i]);
            version.add(ee);
        }


          layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        HomePageAdpaters adapter = new HomePageAdpaters(booking_banner_arr,getActivity(),slider);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
      /*  SnapHelper helper = new LinearSnapHelper();
        try {
            recyclerView.setOnFlingListener(null);
            helper.attachToRecyclerView(recyclerView);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }*/








        NUM_PAGES = IMAGES.length;
        if(booking_banner_arr.size() > 0)
        {
            indicator.setVisibility(View.VISIBLE);
            indicator.createIndicators(booking_banner_arr.size(),0);
            recyclerView.addOnScrollListener(onScrollListener);

        }




    }



    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();


            currentPage = firstVisibleItemPosition;

            System.out.println("-->"+currentPage);
            if(firstVisibleItemPosition == 0)
            {
                //.setVisibility(View.INVISIBLE);

            }
            else  if(firstVisibleItemPosition == (homepage.getBooking_banner_arr().size()-1))
            {

                //leftarrow.setVisibility(View.VISIBLE);
            }
            else
            {

               // leftarrow.setVisibility(View.VISIBLE);
            }
        }
    };

    private void initView(final View view) {
        Mcontext = view.getContext();
        gpsTracker = new GPSTracker(Mcontext);
        Slattitude = String.valueOf(gpsTracker.getLatitude());
        Slogitude = String.valueOf(gpsTracker.getLongitude());
        SpickupLocation = new GeocoderHelper().fetchCityName(Mcontext, gpsTracker.getLatitude(), gpsTracker.getLongitude(), callBack);
        indicator= view.findViewById(R.id.indicator);
        newprofilepick= view.findViewById(R.id.newprofilepick);
        iv_menu = view.findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(this);
        rl_findplaces = view.findViewById(R.id.rl_findplaces);
        nearyby_btn = view.findViewById(R.id.nearyby_btn);
        nearyby_btn.setText(getkey("booking_page_findplaces"));
        leftarrow= view.findViewById(R.id.leftarrow);
        setbac= view.findViewById(R.id.setbac);
        TextView  whereeveryougo= view.findViewById(R.id.whereeveryougo);
        whereeveryougo.setText(getkey("wherever_you_go"));

        TextView  weareready= view.findViewById(R.id.weareready);
        weareready.setText(getkey("we_re_ready"));

        TextView  helpp= view.findViewById(R.id.helpp);
        helpp.setText(getkey("help_text"));

        TextView  savepll= view.findViewById(R.id.savepll);
        savepll.setText(getkey("saveplus"));

        TextView  walletetxt= view.findViewById(R.id.walletetxt);
        walletetxt.setText(getkey("wallet"));

        TextView  mytripsss= view.findViewById(R.id.mytripsss);
        mytripsss.setText(getkey("mytrip_txt"));

        TextView  emee= view.findViewById(R.id.emee);
        emee.setText(getkey("emergency"));
        TextView  comcom= view.findViewById(R.id.comcom);
        comcom.setText(getkey("complaint_lable"));
        TextView  mesmes= view.findViewById(R.id.mesmes);
        mesmes.setText(getkey("messagesla"));



         recyclerView = (RecyclerView) view.findViewById(R.id.homereylerview);
        setbac.setImageResource(R.drawable.app_background_new);


       /* float radius = getResources().getDimension(R.dimen.radius_top_right);
        float radius2 = getResources().getDimension(R.dimen.radius_bottom_right);
        float radius3 = getResources().getDimension(R.dimen.radius_bottom_left);
        imageView.setShapeAppearanceModel(imageView.getShapeAppearanceModel()
                .toBuilder()
                .setTopRightCorner(CornerFamily.ROUNDED, radius)
                .setBottomRightCorner(CornerFamily.ROUNDED, radius2)
                .setBottomLeftCorner(CornerFamily.ROUNDED, radius3)
                //.setBottomLeftCornerSize(radius4)

                .build());*/



        nearyby_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<String> mylist = new ArrayList<>();
                for (int i = 0; i < homepage.getDrivers().size(); i++) {
                    mylist.add(homepage.getDrivers().get(i).getBrand_image());
                }

                Intent searchNearByIntent = new Intent(Mcontext, SearchByNearHome.class);
                searchNearByIntent.putExtra("Savailable_count", homepage.getAvailable_count());
                searchNearByIntent.putExtra("pickuplocation", SpickupLocation);
                searchNearByIntent.putExtra("pickuplat", Slattitude);
                searchNearByIntent.putExtra("pickuplon", Slogitude);
                searchNearByIntent.putStringArrayListExtra("list", mylist);
                startActivity(searchNearByIntent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        rl_findplaces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<String> mylist = new ArrayList<>();
                for (int i = 0; i < homepage.getDrivers().size(); i++) {
                    mylist.add(homepage.getDrivers().get(i).getBrand_image());
                }


                Intent searchNearByIntent = new Intent(Mcontext, SearchByNearHome.class);
                searchNearByIntent.putExtra("Savailable_count", homepage.getAvailable_count());
                searchNearByIntent.putExtra("pickuplocation", SpickupLocation);
                searchNearByIntent.putExtra("pickuplat", Slattitude);
                searchNearByIntent.putExtra("pickuplon", Slogitude);
                searchNearByIntent.putStringArrayListExtra("list", mylist);
                startActivity(searchNearByIntent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        rl_showmelayout = view.findViewById(R.id.rl_showmelayout);
        rl_showmelayout.setOnClickListener(this);
        rl_fastwallet = view.findViewById(R.id.rl_wallet);
        rl_fastwallet.setOnClickListener(this);
        rl_saveplus = view.findViewById(R.id.rl_saveplus);
        rl_saveplus.setOnClickListener(this);

        rl_trips = view.findViewById(R.id.rl_trips);
        rl_trips.setOnClickListener(this);
        rl_km = view.findViewById(R.id.rl_km);
        rl_km.setOnClickListener(this);
        rl_sos = view.findViewById(R.id.rl_sos);
        rl_sos.setOnClickListener(this);
        rl_comp = view.findViewById(R.id.rl_complaint);
        rl_comp.setOnClickListener(this);
        rl_message = view.findViewById(R.id.rl_message);
        rl_message.setOnClickListener(this);
        tv_username = view.findViewById(R.id.username);
        tv_sample = view.findViewById(R.id.tv_sample);
        tv_near_vehicles = view.findViewById(R.id.near_vehicles);
//        tv_dollar = view.findViewById(R.id.tv_doller);

        tv_walletamount = view.findViewById(R.id.tv_walletamount);
        tv_tripsount = view.findViewById(R.id.tv_tripsount);
        tv_kilometer = view.findViewById(R.id.tv_kilometer);
        tv_km = view.findViewById(R.id.tv_km);

        tv_comp_noti = view.findViewById(R.id.tv_comp_noti);
        tv_message_noti = view.findViewById(R.id.tv_message_noti);
        tv_help = view.findViewById(R.id.tv_help);
        tv_help.setOnClickListener(this);

        spinKitView = view.findViewById(R.id.spin_kit);
        spin_kit1 = view.findViewById(R.id.spin_kit1);
       // homeAnimateImage = view.findViewById(R.id.home_animate_image);

        showme = view.findViewById(R.id.showme);
        showme.setOnClickListener(this);

        driver_list = new ArrayList<>();

        session = new SessionManager(getActivity());
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        Username = info.get(SessionManager.KEY_USERNAME);
        homeAnimImage = info.get(SessionManager.KEY_HOMEUSERIMAGE);
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 0 && timeOfDay < 12) {
            tv_username.setText(getkey("good_morning_lable") + " " + Username);
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            tv_username.setText(getkey("good_afternoon_lable") + " " + Username);
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            tv_username.setText(getkey("good_evening_lable") + " " + Username);
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            tv_username.setText(getkey("good_night_lable") + " " + Username);
        }
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        getActivity().registerReceiver(refreshReceiver, intentFilter);


        filter = new IntentFilter();
        filter.addAction("com.app.PaymentListPage.refreshhomedashBoardImage");
        updateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    info = session.getUserDetails();
                    homeAnimImage = info.get(SessionManager.KEY_HOMEUSERIMAGE);
                    if (!homeAnimImage.equalsIgnoreCase("") && !homeAnimImage.equalsIgnoreCase(null)) {
                        String Imsge = homeAnimImage.substring(homeAnimImage.lastIndexOf("."));
                        if (Imsge.equalsIgnoreCase(".gif")) {
                            System.out.println("=====================homeAnimImage gif==============" + Imsge + " " + homeAnimImage);
                           /* GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(homeAnimateImage);
                            Glide.with(Homepage_fragment.this).load(homeAnimImage).into(imageViewTarget);*/
                            Glide.with(Homepage_fragment.this)
                                    .load(homeAnimImage)
                                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                                    .into(new SimpleTarget<Drawable>() {
                                        @Override
                                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                         //   homeAnimateImage.setImageDrawable(resource);
                                        }
                                    });


                        } else {
                            System.out.println("=====================homeAnimImage==============" + homeAnimImage);
                           // Picasso.with(getActivity()).load(String.valueOf(homeAnimImage)).into(homeAnimateImage);

                        }
                    }
                }
            }
        };
        getActivity().registerReceiver(updateReceiver, filter);
        fileter2 = new IntentFilter();
        fileter2.addAction("com.app.MilesPagePage.refreshhomePage");
        updateReceiver1 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    if (isInternetPresent) {
                        PostRequest(Iconstant.get_homepage_url, view);
                    } else {
                        Alert(getkey("action_error"), getkey("alert_nointernet"));
                    }
                }
            }
        };
        getActivity().registerReceiver(updateReceiver1, fileter2);

        isdraweropen = true;
        rl_saveplus.setEnabled(true);
        rl_fastwallet.setEnabled(true);
        rl_trips.setEnabled(true);
        rl_km.setEnabled(true);
        rl_sos.setEnabled(true);
        rl_comp.setEnabled(true);
        rl_message.setEnabled(true);
        tv_help.setEnabled(true);
        rl_showmelayout.setEnabled(true);
        iv_menu.setEnabled(true);
        nearyby_btn.setEnabled(true);

        rl_findplaces.setEnabled(true);

    }

    private void welcomepack_popup() {

        session.setwelcomemeessage(Username,currency + " " +homepage.getWallet_amount(),homepage.getCoupen_count(),homepage.getTot_distance(),homepage.getDistance_unit());


        welcomeplusdialog addPhotoBottomDialogFragment =
                welcomeplusdialog.newInstance();
        addPhotoBottomDialogFragment.show(getActivity().getSupportFragmentManager(),
                "add_photo_dialog_fragment");
    }

    private void qrcode_popup() {

        session.setsavevalue(currency,currency+homepage.getSaveplus_balance(),homepage.getSaveplus_id(),homepage.getQrcode_path());
        Saveplusdialog addPhotoBottomDialogFragment =
                Saveplusdialog.newInstance();
        addPhotoBottomDialogFragment.show(getActivity().getSupportFragmentManager(),
                "add_photo_dialog_fragment");
    }


    private void help_popup() {
        final Dialog dialog = new Dialog(getActivity(), R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.help_assist_layout);

        final CountryPicker picker = CountryPicker.newInstance(getkey("select_country_lable"));


        final TextView Ok_Profilechange = dialog.findViewById(R.id.updateNow);
        Ok_Profilechange.setText(getkey("update_now"));
        final TextView Tv_countryCode = dialog.findViewById(R.id.txt_country_code);

        final TextView estimate_amounts = dialog.findViewById(R.id.estimate_amounts);
        estimate_amounts.setText(getkey("help_us_to_assist_you"));

        final TextView image_Wallet = dialog.findViewById(R.id.image_Wallet);
        image_Wallet.setText(getkey("help_assist_content"));

        final TextView emeco = dialog.findViewById(R.id.emeco);
        emeco.setText(getkey("emergencycontact"));


        final EditText userName = dialog.findViewById(R.id.et_name);
        userName.setHint(getkey("name_txt"));
        final EditText number = dialog.findViewById(R.id.et_number);
        number.setHint(getkey("edit_mobilenumber"));
        final EditText userEmail = dialog.findViewById(R.id.et_email);
        userEmail.setHint(getkey("email"));
        final EditText user_age_et = dialog.findViewById(R.id.et_age);
        user_age_et.setHint(getkey("age_txt"));
        final TextView closeimage = dialog.findViewById(R.id.nothanks);
        closeimage.setText(getkey("later"));
        final ImageView close = dialog.findViewById(R.id.close);

        Tv_countryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getActivity().getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                Tv_countryCode.setText(dialCode);
                CloseKeyBoard();
            }
        });

        final Spinner spgender = (Spinner) dialog.findViewById(R.id.sp_gender);
        Spinner sprelationship = (Spinner) dialog.findViewById(R.id.sp_relationship);
        // Spinner Drop down elements
        ArrayList<String> spinnerItems = new ArrayList<>();
        spinnerItems.add(getkey("gender"));
        spinnerItems.add(getkey("male"));
        spinnerItems.add(getkey("female"));
        spinnerItems.add(getkey("others"));

        HelpSpinnerAdapter customSpinnerAdapter = new HelpSpinnerAdapter(getActivity(), spinnerItems);
        spgender.setAdapter(customSpinnerAdapter);
        spgender.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // close keyboard
                return false;
            }
        });
        spgender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                gender = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                gender = getkey("gender");

            }
        });

        ArrayList<String> spinnerItems_relation = new ArrayList<>();
        spinnerItems_relation.add(getkey("sos_dialog_form_relationship_label"));
        spinnerItems_relation.add(getkey("father_txt"));
        spinnerItems_relation.add(getkey("mother_txt"));
        spinnerItems_relation.add(getkey("sister_txt"));
        spinnerItems_relation.add(getkey("brother_txt"));
        spinnerItems_relation.add(getkey("cousin_txt"));

        HelpSpinnerAdapter customSpinnerAdapter_relation = new HelpSpinnerAdapter(getActivity(), spinnerItems_relation);
        sprelationship.setAdapter(customSpinnerAdapter_relation);
        sprelationship.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // close keyboard
                return false;
            }
        });

        sprelationship.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                relationship = item;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                relationship = "RELATION";
            }
        });

        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_age_et.getText().toString().length() == 0) {
                    erroredit(user_age_et, getkey("profile_label_alert_validage"));
                } else if (user_age_et.getText().toString().trim().equalsIgnoreCase("0")) {
                    erroredit(user_age_et,getkey("profile_label_alert_validage"));
                } else if (userName.getText().toString().length() == 0) {
                    erroredit(userName, getkey("profile_label_alert_username"));
                } else if (number.getText().toString().length() == 0) {
                    erroredit(number, getkey("profile_lable_error_mobile"));
                } else if (!isValidEmail(userEmail.getText().toString().trim().replace(" ", ""))) {
                    erroredit(userEmail, getkey("profile_label_alert_email"));
                } else if (gender.equalsIgnoreCase("GENDER")) {
                    Alert(getkey("alert_label_title"), getkey("profile_label_alert_validgender"));
                } else if (relationship.equalsIgnoreCase("RELATION")) {
                    Alert(getkey("alert_label_title"), getkey("profile_label_alert_validrelation"));
                } else {
                    if (isInternetPresent) {
                        String username = userName.getText().toString().trim().replace(" ", "");
                        String userEmail1 = userEmail.getText().toString().trim().replace(" ", "");
                        String age = user_age_et.getText().toString().trim().replace(" ", "");
                        helpprofile_URL(Iconstant.help_profile, username, userEmail1, gender, age, Tv_countryCode.getText().toString(), number.getText().toString(), relationship);
                    } else {
                        Alert(getkey("action_error"), getkey("alert_nointernet"));
                    }
                    dialog.dismiss();
                }
            }
        });
        closeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(user_age_et);
                dialog.dismiss();
                Intent intent = new Intent(getActivity(), SelectVehicleCategoryPage.class);
                intent.putExtra("pickupAddress", SpickupLocation);
                intent.putExtra("pickupLatitude", String.valueOf(Slattitude));
                intent.putExtra("pickupLongitude", String.valueOf(Slogitude));
                intent.putExtra("destinationAddress", "");
                intent.putExtra("destinationLatitude", "");
                intent.putExtra("destinationLongitude", "");
                intent.putExtra("page", "BookingPage1");
                intent.putExtra("intentCall", "normalBooking");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(user_age_et);
                dialog.dismiss();
                Intent intent = new Intent(getActivity(), SelectVehicleCategoryPage.class);
                intent.putExtra("pickupAddress", SpickupLocation);
                intent.putExtra("pickupLatitude", String.valueOf(Slattitude));
                intent.putExtra("pickupLongitude", String.valueOf(Slogitude));
                intent.putExtra("destinationAddress", "");
                intent.putExtra("destinationLatitude", "");
                intent.putExtra("destinationLongitude", "");
                intent.putExtra("page", "BookingPage1");
                intent.putExtra("intentCall", "normalBooking");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void CloseKeyBoard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    private void helpprofile_URL(String help_profile, String username, String userEmail1, String gender, String age, String code, String number, String relationship) {
        final Dialog dialog = new Dialog(Mcontext);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("gender", gender);
        jsonParams.put("age", age);
        jsonParams.put("emergency_conatct[0][em_mobile_code]", code);
        jsonParams.put("emergency_conatct[0][em_mobile]", number);
        jsonParams.put("emergency_conatct[0][em_email]", userEmail1);
        jsonParams.put("emergency_conatct[0][em_name]", username);
        jsonParams.put("emergency_conatct[0][em_realtionship]", relationship);
        jsonParams.put("emergency_conatct[0][em_address]", "");
        jsonParams.put("emergency_conatct[0][em_city]", "");
        jsonParams.put("emergency_conatct[0][em_state]", "");
        jsonParams.put("emergency_conatct[0][em_country]", "");
        jsonParams.put("emergency_conatct[0][em_pincode]", "");


        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(help_profile, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println(".........." + response);
                String Sstatus = "", message = "";
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Alerthelp(getkey("action_success"), jsonObject.getString("response"));
                    } else {
                        Alert(getkey("action_success"), jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });

    }


    private void fastpayhome(String Url) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);

                dialog.dismiss();
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1"))
                    {

                        JSONObject response_object = object.getJSONObject("response");

                        session.setsavevalue(currency+homepage.getWallet_amount(),response,currency,"");
                        walletdialog addPhotoBottomDialogFragment =
                                walletdialog.newInstance();
                        addPhotoBottomDialogFragment.show(getActivity().getSupportFragmentManager(),
                                "add_photo_dialog_fragment");




                    }
                    else
                        {
                        HashMap<String, String> getpatterns = session.getpattern();
                        String patterns = getpatterns.get(SessionManager.KEY_PATTERN);
                        Intent intents;
                        if (!patterns.equalsIgnoreCase(""))
                        {
                            updatepattern(Iconstant.getpattern);
                        } else {
                            intents = new Intent(getActivity(), Pin_activitynew.class);
                            startActivity(intents);
                            getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void walletpopup()
    {
        fastpayhome(Iconstant.fastpayhome);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_showmelayout:
                showCarList_Dialog();
                break;
            case R.id.showme:
                showCarList_Dialog();
                break;

            case R.id.tv_help:
                getActivity().startActivity(new Intent(getActivity(), Helpfaq.class));
                break;
            case R.id.iv_menu:
                Navigation_new.openDrawer();
                break;
            case R.id.rl_saveplus:
                qrcode_popup();
                break;
            case R.id.rl_wallet:

                walletpopup();
                break;


            case R.id.rl_trips:
                Intent trips = new Intent(getActivity(), RideListHomePage.class);
                //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                trips.putExtra("Class", "Home");
                startActivity(trips);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.rl_km:
                Intent km = new Intent(getActivity(), MilesHome.class);
//                Intent km = new Intent(getActivity(), RequestTimer.class);
//                Intent km = new Intent(getActivity(), WalletMoneyPage1.class);
//
// Intent km = new Intent(getActivity(), Fastpaybusiness_setting.class);
                //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(km);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.rl_sos:
//                Intent sos = new Intent(getActivity(), SosPage.class);
                Intent sos = new Intent(getActivity(), Sosdragmap_screen.class);
                startActivity(sos);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.rl_complaint:
                Intent complaint = new Intent(getActivity(), Complaint_Page_new.class);
                complaint.putExtra("frompage", "Homepage");
                startActivity(complaint);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

                break;
            case R.id.rl_message:
                Intent message = new Intent(getActivity(), InfoPage.class);
                startActivity(message);
                getActivity().overridePendingTransition(R.anim.slideup, R.anim.exit);
                break;

        }
    }

    private void Alert(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void Alertlogout(String title, String alert) {
        if (wholedialog != null) {
            wholedialog.dismiss();
        }
        wholedialog = new PkDialog(getActivity());
        wholedialog.setDialogTitle(title);
        wholedialog.setDialogMessage(alert);
        wholedialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wholedialog.dismiss();
                session.logoutUser();




                getActivity().stopService(new Intent(getActivity(), XmppService.class));
                postRequest_AppInformation(Iconstant.getAppInfo);
                Intent intent = new Intent(getActivity(), SplashPage1.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                getActivity().finish();
            }
        });
        wholedialog.show();
    }



    //-----------------------App Information Post Request-----------------
    private void postRequest_AppInformation(String Url) {
        System.out.println("-------------Splash App Information Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "user");
        jsonParams.put("id", "");
        jsonParams.put("lat", "");
        jsonParams.put("lon", "");
        System.out.println("--------App Information jsonParams--------" + jsonParams);
        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Splash App Information Response----------------" + response);
                String Str_status = "", SuserImage = "", userGuide = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                session.setCustomerServiceNo(sCustomerServiceNumber);
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");

                                if(info_object.has("s3_status"))
                                {
                                    if(info_object.getString("s3_status").equals("1"))
                                    {
                                        JSONObject s3_info = info_object.getJSONObject("s3_info");

                                        String access_key = s3_info.getString("access_key");
                                        String secret_key = s3_info.getString("secret_key");
                                        String bucket_name = s3_info.getString("bucket_name");
                                        String bucket_url = s3_info.getString("bucket_url");
                                        session.setAmazondetails(info_object.getString("s3_status"),access_key,secret_key,bucket_name,bucket_url);
                                    }
                                    else
                                    {
                                        session.setImagestatus("0");
                                    }
                                }
                            } else {

                            }

                           /* sPendingRideId= response_object.getString("pending_rideid");
                            sRatingStatus= response_object.getString("ride_status");*/

                        } else {

                        }
                    } else {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
//                Toast.makeText(context, Iconstant.Url, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Alerthelp(String title, String alert) {
        if (wholedialog != null) {
            wholedialog.dismiss();
        }
        wholedialog = new PkDialog(getActivity());
        wholedialog.setDialogTitle(title);
        wholedialog.setDialogMessage(alert);
        wholedialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wholedialog.dismiss();
                Intent intent = new Intent(getActivity(), SelectVehicleCategoryPage.class);
                intent.putExtra("pickupAddress", SpickupLocation);
                intent.putExtra("pickupLatitude", String.valueOf(Slattitude));
                intent.putExtra("pickupLongitude", String.valueOf(Slogitude));
                intent.putExtra("destinationAddress", "");
                intent.putExtra("destinationLatitude", "");
                intent.putExtra("destinationLongitude", "");
                intent.putExtra("page", "BookingPage1");
                intent.putExtra("intentCall", "normalBooking");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        wholedialog.show();
    }

    private void Alert_tryagin(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("yes"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                Intent i = new Intent(getActivity(), Saveplus_manual.class);
                startActivity(i);
            }
        });
        mDialog.setNegativeButton("Try again", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                Intent i = new Intent(getActivity(), Saveplus_sender_sendmoney.class);
                startActivity(i);
            }
        });
        mDialog.show();
    }







    private void UpdateProfilePopUp2(final String needAssistance) {

        final Dialog dialog = new Dialog(getActivity(), R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup2);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_tv);

        final CustomEdittext userName = (CustomEdittext) dialog.findViewById(R.id.name_title);
        final CustomEdittext userEmail = (CustomEdittext) dialog.findViewById(R.id.email_title);
        final CustomEdittext user_age_et = (CustomEdittext) dialog.findViewById(R.id.user_age_et);
        final ImageView closeimage = (ImageView) dialog.findViewById(R.id.img_close);
        CustomTextView Profilechange = (CustomTextView) dialog.findViewById(R.id.profile_text1);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        Spinner spinnerCustom = (Spinner) dialog.findViewById(R.id.gender_spinner1);
        final TextView Tv_city = (TextView) dialog.findViewById(R.id.ratecard_city_textview);
        // Spinner Drop down elements
        ArrayList<String> spinnerItems = new ArrayList<>();
        spinnerItems.add(getkey("gender"));
        spinnerItems.add(getkey("male"));
        spinnerItems.add(getkey("female"));
        spinnerItems.add(getkey("others"));

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(), spinnerItems);
        spinnerCustom.setAdapter(customSpinnerAdapter);
        spinnerCustom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // close keyboard
                return false;
            }
        });
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                cd = new ConnectionDetector(getActivity());
                isInternetPresent = cd.isConnectingToInternet();
                Tv_city.setText(item);
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Tv_city.setText(getkey("seletc_gender"));
            }
        });
        user_age_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    user_age_et.setCursorVisible(false);
                    user_age_et.clearFocus();
                    InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(user_age_et.getWindowToken(), 0);
                }
                return false;
            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userName.getText().toString().length() == 0) {
                    erroredit(userName, getkey("profile_label_alert_username"));
                } else if (!isValidEmail(userEmail.getText().toString().trim().replace(" ", ""))) {
                    erroredit(userEmail, getkey("profile_label_alert_email"));
                } else if (Tv_city.getText().toString().trim().equalsIgnoreCase("SELECT GENDER")) {
                    AlertError(getkey("alert_label_title"), getkey("profile_label_alert_validgender"));
                } else if (user_age_et.getText().toString().length() == 0) {
                    erroredit(user_age_et, getkey("profile_label_alert_validage"));
                } else if (user_age_et.getText().toString().trim().equalsIgnoreCase("0")) {
                    erroredit(user_age_et, getkey("profile_label_alert_validage"));
                } else {
                    if (isInternetPresent) {
                        String username = userName.getText().toString().trim().replace(" ", "");
                        String userEmail1 = userEmail.getText().toString().trim().replace(" ", "");
                        String gender = Tv_city.getText().toString().trim().replace(" ", "");
                        String age = user_age_et.getText().toString().trim().replace(" ", "");
                        postrequestUpdateProfile(Iconstant.get_basic_profile_url, username, userEmail1, gender, needAssistance, age);
                    } else {
                        Alert(getkey("action_error"), getkey("alert_nointernet"));
                    }
                    dialog.dismiss();
                }
            }
        });
        closeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(user_age_et);
                dialog.dismiss();
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(getActivity(), msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }


    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getCustomerServiceNo()));
                    startActivity(intent);
                }
                break;


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        CloseKeyBoard();


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isInternetPresent) {
                    PostRequest(Iconstant.get_homepage_url, parentView);
                } else {
                    Alert(getkey("action_error"), getkey("alert_nointernet"));
                }
            }
        }, 100);
    }

    @Override
    public void onPause() {
        super.onPause();
        /*if(dialog!=null){
            dialog.dismiss();
        }*/
        if (spinKitView.getVisibility() == View.VISIBLE) {
            spinKitView.setVisibility(View.GONE);
        }

        //dialogDismiss();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:

                AlertExit(getkey("alert"), getkey("exit_alert"));

                return true;
        }
        return false;
    }

    //--------------Alert Method-----------
    private void AlertExit(String title, String alert) {

        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                getActivity().finish();
            }
        });
        mDialog.setNegativeButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void Alert1(String title, String alert, final String status) {

        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);

        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")) {
                    Intent intent = new Intent(getActivity(), Navigation_new.class);
                    // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
//                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                    mDialog.dismiss();
                } else {
                    mDialog.dismiss();
                }

            }
        });
        mDialog.show();
    }

    @Subscribe
    public void redirect(ArrayList<String> pojo)
    {
       if(pojo.get(0).equals("Checkpa"))
       {
           Intent intent = new Intent(getActivity(), Checkpattern.class);
           intent.putExtra("codepath",pojo.get(1));
           intent.putExtra("saveplusid",pojo.get(2));
           startActivity(intent);

       }
       else if(pojo.get(0).equals("confi"))
       {
           Intent intent = new Intent(getActivity(), Confirmpatern_activity.class);
           startActivity(intent);

       }
       else if(pojo.get(0).equals("wallet"))
       {
           Intent intent = new Intent(getActivity(), Checkpattern.class);
           intent.putExtra("codepath",pojo.get(1));
           intent.putExtra("saveplusid",pojo.get(2));
           startActivity(intent);

       }
       else if(pojo.get(0).equals("wallconfi"))
       {
           Intent intent = new Intent(getActivity(), Confirmpatern_activity.class);
           startActivity(intent);

       }
       else if(pojo.get(0).equals("wallconfisss"))
       {

           Intent intents = new Intent(getActivity(), Pin_activitynew.class);
           startActivity(intents);
           getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

       }
    }

    @Subscribe
    public void dashboarddrawerlitener(clickdisabledashboardpojo pojo) {
        if (pojo.isIsenabled()) {
            isdraweropen = false;
            rl_saveplus.setEnabled(false);
            rl_fastwallet.setEnabled(false);
            rl_trips.setEnabled(false);
            rl_km.setEnabled(false);
            rl_sos.setEnabled(false);
            rl_comp.setEnabled(false);
            rl_message.setEnabled(false);
            tv_help.setEnabled(false);
            rl_showmelayout.setEnabled(false);
            iv_menu.setEnabled(false);
            nearyby_btn.setEnabled(false);

            rl_findplaces.setEnabled(false);
        } else {
            isdraweropen = true;
            rl_saveplus.setEnabled(true);
            rl_fastwallet.setEnabled(true);
            rl_trips.setEnabled(true);
            rl_km.setEnabled(true);
            rl_sos.setEnabled(true);
            rl_comp.setEnabled(true);
            rl_message.setEnabled(true);
            tv_help.setEnabled(true);
            rl_showmelayout.setEnabled(true);
            iv_menu.setEnabled(true);
            nearyby_btn.setEnabled(true);

            rl_findplaces.setEnabled(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (spinKitView.getVisibility() == View.VISIBLE) {
            spinKitView.setVisibility(View.GONE);
        }
        if (isTimerstarted) {
            swipeTimer.cancel();
        }
        if (proxyCache != null) {
            proxyCache.shutdown();
        }
        //dialogDismiss();
        if (updateReceiver != null) {
            getActivity().unregisterReceiver(updateReceiver);
            updateReceiver = null;
        }
        if (updateReceiver1 != null) {
            getActivity().unregisterReceiver(updateReceiver1);
            updateReceiver1 = null;
        }
        getActivity().unregisterReceiver(refreshReceiver);
    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    private void postrequestUpdateProfile(String Url, String name, String email, String gender, String needAssistance, String Age) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("user_name", name);
        jsonParams.put("email", email);
        jsonParams.put("gender", gender);
        jsonParams.put("age", Age);
        jsonParams.put("assistance", needAssistance);

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                dialog.dismiss();
                System.out.println("--------------Basic profile reponse-------------------" + response);
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");
                        Alert1(getkey("action_success"), Smessage, Sstatus);

                    } else {
                        Smessage = object.getString("response");
                        Alert1(getkey("action_error"), Smessage, Sstatus);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void PostRequest(String Url, final View view) {

        final Dialog dialog = new Dialog(Mcontext);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        // spinKitView.setVisibility(View.VISIBLE);
        System.out.println("-----------HomePage url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("latitude", String.valueOf(gpsTracker.getLatitude()));
        jsonParams.put("longitude", String.valueOf(gpsTracker.getLongitude()));
        System.out.println("-----------HomePage jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                dialog.dismiss();
                System.out.println("--------------HomePage reponse-------------------" + response);
                Log.e("dsf",response);

                String user_location = "", info_badge = "", referalStatus = "", onGoingRide = "", roaming_status = "", roaming_mode = "", Sstatus = "", totalTicketCount = "", dashboard_image = "", Smessage = "", Swallet_amount = "", Sbid_fare = "", Sride_min = "", ScurrencySymbol = "", Sprofile_complete = "", totalDistance, closedTicketCount = "", openTicketCount = "", userActive = "";
                homepage = new Homepage_pojo();
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.has("is_out")) {
                        if (object.getString("is_out").toLowerCase().equals("yes")) {
                            Alertlogout(getkey("sorry"), object.getString("message").toString());
                        }
                    }

                    Type listType = new TypeToken<Homepage_pojo>() {
                    }.getType();
                    homepage = new GsonBuilder().create().fromJson(object.toString(), listType);

                    Sstatus = homepage.getStatus();

                    if (Sstatus.equalsIgnoreCase("1")) {


                        if(!homepage.getPhoto_video_type().equals("video"))
                        {
                            Glide.with(view.getContext()).load(homepage.getPhoto_video()).placeholder(R.drawable.topstariahtback).into(setbac);
                        }

                        init_slidebanner(view, homepage.getBooking_banner_arr());
                        session.setSecurePin(homepage.getWith_pincode());

                        if (object.has("comp_stage")) {
                            CompleteStage = homepage.getComp_stage();
                            completeStatus = homepage.getComp_status();
                            session.setComplementStatus(homepage.getComp_stage());
                        }

                        if (object.has("user_image")) {
                            String SUser_image = object.getString("user_image");
                            if (!SUser_image.equals("")) {
                                session.setUserImageUpdate(SUser_image);
                            }
                            setuserimageindashboard obj = new setuserimageindashboard();
                            obj.setImage(homepage.getUser_image());
                            EventBus.getDefault().post(obj);
                            Picasso.with(getActivity())
                                    .load(SUser_image)
                                    .placeholder(R.drawable.no_user_img)   // optional
                                    .error(R.drawable.no_user_img)      // optional
                                    .into(newprofilepick);

                        }

                        Intent local = new Intent();
                        local.setAction("com.blackmaria.couponupdate");
                        local.putExtra("coupon", homepage.getCoupen_count());
                        getActivity().sendBroadcast(local);

                        if (homepage.getEmergency_contact().equalsIgnoreCase("1")) {
                            isemergencyexist = true;
                        }

                        onGoingRide = homepage.getOn_goingride();
                        onGoingRideId = homepage.getOn_goingrideid();
                        onGoingTxt = homepage.getOn_going_txt();
                        if(homepage.getAvailable_count()!= null){
                            tv_near_vehicles.setText(getkey("there_are_lable")+" " + homepage.getAvailable_count() +" "+ getkey("vechile_areyou"));
                        }else{
                            tv_near_vehicles.setText(getkey("there_are_lable")+" " +getkey("no") + getkey("vechile_areyou"));
                        }

                        try {
                            if (Integer.parseInt(homepage.getMessage_count()) > 0) {
                                tv_message_noti.setVisibility(View.VISIBLE);
                                tv_message_noti.setText(homepage.getMessage_count());
                            }
                            if (Integer.parseInt(homepage.getTicket_Count()) > 0) {
                                tv_comp_noti.setVisibility(View.VISIBLE);
                                tv_comp_noti.setText(homepage.getTicket_Count());
                            }
                        } catch (NumberFormatException e) {
                        }
                        //updateshowme carlist
                        driver_list.clear();
                        driver_list.addAll(homepage.getDrivers());
                        Gson gson = new Gson();
                        String json = gson.toJson((homepage.getDrivers()));

                        session.setDriverslist(json);

                        openTicketCount = String.valueOf(homepage.getOpen_ticket_Count());

                        user_location = homepage.getUser_location();
                        referalStatus = homepage.getReferral_staus();
                        roaming_mode = homepage.getRoaming_mode();
                        roaming_status = homepage.getRoaming_status();
                        info_badge = String.valueOf(homepage.getInfo_badge());
                        Swallet_amount = homepage.getWallet_amount();
                        currency = CurrencySymbolConverter.getCurrencySymbol(homepage.getCurrencyCode());

                        if (object.has("driver_request_mode")) {
                            driverRequestMode = homepage.getDriver_request_mode();
                        }
                        xendit_key = homepage.getXendit_public_key();
                        if (object.has("exchange_value")) {
                            currencyconversion = String.valueOf(homepage.getExchange_value());
                            session.setcurrencyconversionKey(currencyconversion);
                        }
                        ride_amount = homepage.getGrand_fare();
                        payment_mode = homepage.getPayment_mode();
                        ride_status = homepage.getRide_status();
                        Ssupport_no = homepage.getSupport_no();
                        UserProfileUpdateStatus = homepage.getProfile_complete();
                        basicUserProfileUpdateStatus = homepage.getBasic_complete();
                        session.createWalletAmount(currency + " " + Swallet_amount);
                        tv_walletamount.setText(currency  + Swallet_amount);
//                        tv_dollar.setText(currency);
                        session.setXenditPublicKey(xendit_key);
                        session.setUserBasicProfile(basicUserProfileUpdateStatus);
                        session.setUserCompleteProfile(UserProfileUpdateStatus);
                        totalDistance = homepage.getTot_distance();
                        session.setReferalStatus(referalStatus);
                        session.setbusinessprofile(homepage.getBusiness_profile_status());
                        session.setbusinessname(homepage.getBusiness_name());
                        session.setQrcode_path(homepage.getQrcode_path());

                        if (homepage.getPattern_code_status().equalsIgnoreCase("1")) {
                            session.setpattern(homepage.getPattern_code());
                            session.setsaveplusid(homepage.getSaveplus_id());
                            session.setwithdrawal_status(homepage.getSaveplus_withdrawal_status());
                            session.setsaveplus_balance(homepage.getSaveplus_balance());
                            session.setsaveplus_minbalance(homepage.getSaveplus_min_withdrawal_amt());
                        }

                        if (object.has("dashboard_image")) {
                            dashboard_image = homepage.getDashboard_image();
                            if (homeAnimImage.equalsIgnoreCase("") && homeAnimImage.equalsIgnoreCase(null)) {
                                session.setUserHomeAnimaionImageUpdate(dashboard_image);
                            }
                        }

                        tv_kilometer.setText(totalDistance);
                        tv_tripsount.setText(homepage.getTrip_Count());
                        tv_km.setText(homepage.getDistance_unit());
                        if (session.getfirstimelogin().equalsIgnoreCase("1")) {
                           session.setfirstimelogin("0");
                            welcomepack_popup();
                        }

                    } else {
                        Smessage = object.getString("response");
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    try {
                        session.setInfoBadge(info_badge);

                        isFirstime = session.getIsFirstTime();
                        if (roaming_mode.equalsIgnoreCase("deactivated") && roaming_status.equalsIgnoreCase("1")) {
                            RoamingPopUpLogut();
                        } else if (roaming_mode.equalsIgnoreCase("activated") && roaming_status.equalsIgnoreCase("1") && isFirstime) {
                            RoamingPopUpWelcome(user_location);
                        }
                        if (!openTicketCount.equalsIgnoreCase("0")) {
                            tv_comp_noti.setText(openTicketCount);
                            tv_comp_noti.setVisibility(View.VISIBLE);
                        } else {
                            tv_comp_noti.setVisibility(View.GONE);
                        }
                        if (!onGoingRide.equalsIgnoreCase("0")) {
                            TrackRidePopUp(onGoingRideId);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                // spinKitView.setVisibility(View.GONE);

                dialog.dismiss();
                //dialogDismiss();
            }
        });

    }

    private final MediaPlayer.OnInfoListener onInfoToPlayStateListener = new MediaPlayer.OnInfoListener() {

        @Override
        public boolean onInfo(MediaPlayer mp, int videoReqCode, int extra) {
            if (MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START == videoReqCode) {
//                spin_kit1.setVisibility(View.GONE);
                tv_sample.setVisibility(View.GONE);
            }
            if (MediaPlayer.MEDIA_INFO_BUFFERING_START == videoReqCode) {
//                spin_kit1.setVisibility(View.VISIBLE);
                tv_sample.setVisibility(View.VISIBLE);
            }
            if (MediaPlayer.MEDIA_INFO_BUFFERING_END == videoReqCode) {
//                spin_kit1.setVisibility(View.GONE);
                tv_sample.setVisibility(View.GONE);
            }
            return false;
        }
    };


    private void TrackRidePopUp(final String rideId) {
        if (trackdialog != null) {
            trackdialog.dismiss();
        }
        trackdialog = new Dialog(getActivity(), R.style.DialogSlideAnim3);
        trackdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        trackdialog.setCancelable(false);
        trackdialog.setCanceledOnTouchOutside(false);
        trackdialog.setContentView(R.layout.homepage_track_popup);
        final TextView Ok_Tv = (TextView) trackdialog.findViewById(R.id.custom_dialog_library_cancel_button);
        final RelativeLayout cancel_payment = (RelativeLayout) trackdialog.findViewById(R.id.custom_dialog_library_ok_button);
        final ImageView image_close = (ImageView) trackdialog.findViewById(R.id.image_close);
        final LinearLayout trackLayout = (LinearLayout) trackdialog.findViewById(R.id.track_layout);
        final TextView onGoingText = (TextView) trackdialog.findViewById(R.id.custom_dialog_library_message_textview1);

        final TextView custom_dialog_library_message_textview = (TextView) trackdialog.findViewById(R.id.custom_dialog_library_message_textview);
        custom_dialog_library_message_textview.setText(getkey("tracknow_home"));


        Ok_Tv.setText(getkey("tracknow_home"));
        onGoingText.setText(onGoingTxt);
        if(payment_mode.equalsIgnoreCase("xendit-card") && ride_status.equalsIgnoreCase("Finished"))
        {
            Ok_Tv.setText(getkey("my_rides_payment_header_textview"));
        }

        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trackdialog != null)
                    trackdialog.dismiss();
            }
        });
        trackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CompleteStage.equalsIgnoreCase("onprocess")) {
                    Intent i = new Intent(getActivity(), WaitingTimePage.class);
                    i.putExtra("ride_id", rideId);
                    i.putExtra("isContinue", true);
                    startActivity(i);
                    getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    getActivity().finish();
                    trackdialog.dismiss();

                } else {

                    if(payment_mode.equalsIgnoreCase("xendit-card") && ride_status.equalsIgnoreCase("Finished"))
                    {


                        postCarddetails(Iconstant.wallet_money_url);


                    }
                    else
                    {
                        Intent intent1 = new Intent(getActivity(), TrackRidePage.class);
                        intent1.putExtra("ride_id", rideId);
                        startActivity(intent1);
                        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        getActivity().finish();
                        trackdialog.dismiss();
                    }
                }
            }
        });

        trackdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        trackdialog.show();
    }


    private void RoamingPopUpLogut() {
        final Dialog dialog = new Dialog(getActivity(), R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.homepage_logout_popup);
        final Button Ok_Tv = (Button) dialog.findViewById(R.id.custom_dialog_library_cancel_button);
        Ok_Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                dialog.dismiss();

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void logout() {
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            postRequest_Logout(Iconstant.logout_url);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }
    }

    private void RoamingPopUpWelcome(final String user_location) {
        final Dialog dialog = new Dialog(getActivity(), R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.homepage_roaming_popup);
        final TextView custom_TV = (TextView) dialog.findViewById(R.id.custom_dialog_library_message_textview);
        final TextView custom_TV1 = (TextView) dialog.findViewById(R.id.custom_dialog_library_message_textview1);
        final Button Ok_Tv = (Button) dialog.findViewById(R.id.custom_dialog_library_cancel_button);
        custom_TV.setText(getkey("welcome") + " " + user_location);
        custom_TV1.setText(getkey("roaming_wallet"));
        Ok_Tv.setText(getkey("close_lable"));
        Ok_Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.IsFirstTime(false);
                dialog.dismiss();

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    //--------------Show Dialog Method-----------

    //-----------------------Logout Request-----------------
    private void postRequest_Logout(String Url) {
        spinKitView.setVisibility(View.VISIBLE);
        System.out.println("---------------LogOut Url-----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("device", "ANDROID");

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------------LogOut Response-----------------" + response);
                String Sstatus = "", Sresponse = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                spinKitView.setVisibility(View.GONE);
                if (Sstatus.equalsIgnoreCase("1")) {
                    session.logoutUser();
                    getActivity().stopService(new Intent(getActivity(), XmppService.class));
                    Intent intent = new Intent(getActivity(), SignUpPage.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    getActivity().finish();
                } else {
                    Alert(getkey("action_error"), Sresponse);
                }
            }

            @Override
            public void onErrorListener() {
                spinKitView.setVisibility(View.GONE);
            }
        });
    }



    private void postCarddetails(String Url) {
        final Dialog dialog = new Dialog(Mcontext);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        System.out.println("---------------LogOut Url-----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);


        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------------LogOut Response-----------------" + response);
                String Sstatus = "", Sresponse = "";
                try {

                    JSONObject json = new JSONObject(response);
                    try
                    {
                        session.setpaymentlist(json.toString());
                        Intent intent1 = new Intent(getActivity(), RechargecardpaymentConstrain.class);
                        intent1.putExtra("insertAmount", ride_amount);
                        intent1.putExtra("jsonpojo", json.toString());
                        intent1.putExtra("pendingpayment", onGoingRideId);
                        intent1.putExtra("currency", currency);
                        startActivity(intent1);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void showCarList_Dialog() {
        final Dialog dialog = new Dialog(Mcontext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.car_list_dialog);

        ListView vechiles_listview = (ListView) dialog.findViewById(R.id.nearest_vechile_list_listview);
        RelativeLayout close = (RelativeLayout) dialog.findViewById(R.id.car_list_nearest_vechile_close_layout);
        TextView nearest_vechile = (TextView) dialog.findViewById(R.id.car_list_nearest_vechile_textview);

        nearest_vechile.setText(driver_list.size() + getkey("nearest_vehicles_lable"));

        CarListAdapter personListAdapter = new CarListAdapter(Mcontext, driver_list);

        vechiles_listview.setAdapter(personListAdapter);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        Window w = dialog.getWindow();
        w.setLayout((width * 6) / 8, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.show();
    }


    CallBack callBack = new CallBack() {

        @Override

        public void onComplete(String LocationName) {
            SpickupLocation = LocationName;
        }

        @Override

        public void onError(String errorMsg) {
        }

        @Override
        public void onCompleteAddress(String message) {

        }

    };


    public void showResultDialogue(final String result) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle(getkey("scan_result"))
                .setMessage(getkey("scanned_resultis") + result)
                .setPositiveButton(getkey("copy_result"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("Scan Result", result);
                        clipboard.setPrimaryClip(clip);
                        Toast.makeText(getActivity(), getkey("resulct_clipad"), Toast.LENGTH_SHORT).show();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();
    }


    private void updatepattern(String Url) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> info = session.getUserDetails();
        String UserID = info.get(SessionManager.KEY_USERID);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);


        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        JSONObject jsonObject = object.getJSONObject("response");
                        if (jsonObject.has("pattern_code")) {
                            session.setpattern(jsonObject.getString("pattern_code"));
                            if(!jsonObject.getString("pattern_code").equals(""))
                            {
                                Intent intent = new Intent(getActivity(), Checkpattern.class);
                                intent.putExtra("codepath",homepage.getQrcode_path());
                                intent.putExtra("saveplusid",homepage.getSaveplus_id());
                                startActivity(intent);

                            }
                            else
                            {
                                Intent intent = new Intent(getActivity(), Confirmpatern_activity.class);
                                startActivity(intent);

                            }
                        }
                        else
                        {
                            Intent intent = new Intent(getActivity(), Confirmpatern_activity.class);
                            startActivity(intent);

                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
