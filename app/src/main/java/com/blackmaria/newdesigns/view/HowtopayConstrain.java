package com.blackmaria.newdesigns.view;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.blackmaria.R;

public class HowtopayConstrain extends AppCompatActivity {

    private ImageView backimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_howtopay_constrain);

        backimg = findViewById(R.id.backimg);
        backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


}
