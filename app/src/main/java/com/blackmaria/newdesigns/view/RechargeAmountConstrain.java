package com.blackmaria.newdesigns.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.RechargeamountFactory;
import com.blackmaria.newdesigns.viewmodel.RechargeamountViewModel;
import com.blackmaria.pojo.rechargeamountpojo;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityRechargeAmountConstrainBinding;
import com.blackmaria.iconstant.Iconstant;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class RechargeAmountConstrain extends AppCompatActivity {

    private ActivityRechargeAmountConstrainBinding binding;
    private RechargeamountViewModel rechargeamountViewModel;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private rechargeamountpojo pojo;
    private JSONObject json;
    LanguageDb mhelper;
    private AppUtils appUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(RechargeAmountConstrain.this, R.layout.activity_recharge_amount_constrain);
        rechargeamountViewModel = ViewModelProviders.of(this, new RechargeamountFactory(this)).get(RechargeamountViewModel.class);
        binding.setRechargeamountViewModel(rechargeamountViewModel);
        rechargeamountViewModel.setIds(binding);
        mhelper = new LanguageDb(this);
        initView();
        binding.insertamount.setHint(getkey("insertamountrecharge"));
        binding.confirminsertamount.setText(getkey("confirm_label"));
        binding.preselectamount.setText(getkey("preselectamount"));
        binding.havevoucher.setText(getkey("havevoucher"));
        binding.insertvoucher.setHint(getkey("insertvoucher"));
        binding.okconfirm.setText(getkey("ok_lable"));
        binding.weaccept.setText(getkey("weaccept"));
        setResponse();
        clicklistener();
        

    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", sDriverID);
        rechargeamountViewModel.getmoneypagedriver(Iconstant.wallet_money_url, jsonParams);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    private void setResponse() {
        rechargeamountViewModel.getGetmoneypagedriverresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    System.out.println("resssss--"+response);
                    json = new JSONObject(response);
                    sessionManager.setpaymentlist(json.toString());
                    Type type = new TypeToken<rechargeamountpojo>() {
                    }.getType();
                    pojo = new GsonBuilder().create().fromJson(json.toString(), type);
                    rechargeamountViewModel.setpojo(json);
                    settext(pojo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void settext(rechargeamountpojo pojo) {
        sessionManager.setXenditPublicKey(pojo.getResponse().getXendit_public_key());
        sessionManager.setcurrencyconversionKey(pojo.getResponse().getExchange_value());
        binding.amount.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getRecharge_boundary().getWal_recharge_amount_one());
        binding.amount1.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getRecharge_boundary().getWal_recharge_amount_two());
        binding.amount2.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getRecharge_boundary().getWal_recharge_amount_three());
        binding.amount3.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getRecharge_boundary().getWal_recharge_amount_four());
    }

    private void clicklistener() {
        binding.confirminsertamount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.insertamount.getText().toString().length() == 0) {
                    appUtils.AlertError(RechargeAmountConstrain.this, getkey("action_error"), getkey("rechargeamountempty"));
                } else if (binding.insertamount.getText().toString().equals("0")) {
                    appUtils.AlertError(RechargeAmountConstrain.this,getkey("action_error"), getkey("insertvalidamount"));
                }else{
                    Intent intent = new Intent(RechargeAmountConstrain.this, Rechargechoosepayment.class);
                    intent.putExtra("insertAmount", binding.insertamount.getText().toString().trim());
                    intent.putExtra("jsonpojo", json.toString());
                    startActivity(intent);
                }
            }
        });
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
