package com.blackmaria.newdesigns.view.Withdraw.Bank;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.Withdraw.Bank.WithdrawreeiptFactory;
import com.blackmaria.newdesigns.view.Pin_activity;
import com.blackmaria.newdesigns.viewmodel.withdraw.bank.WithdrawchangereceipeintsendnowViewModel;
import com.blackmaria.pojo.Walletwithdrawpincheck;
import com.blackmaria.pojo.withdrawalspaymentspojo;
import com.blackmaria.pojo.withdrawpaymentsendpojo;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityWithdrawchangereceipeintsendnowConstrainBinding;
import com.blackmaria.iconstant.Iconstant;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class WithdrawchangereceipeintsendnowConstrain extends AppCompatActivity {

    private ActivityWithdrawchangereceipeintsendnowConstrainBinding binding;
    private WithdrawchangereceipeintsendnowViewModel withdrawchangereceipeintsendnowViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private String sDriverID = "", withdrawamount = "";
    private JSONObject json;
    private withdrawalspaymentspojo pojo;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        mhelper = new LanguageDb(this);
        binding = DataBindingUtil.setContentView(WithdrawchangereceipeintsendnowConstrain.this, R.layout.activity_withdrawchangereceipeintsendnow_constrain);
        withdrawchangereceipeintsendnowViewModel = ViewModelProviders.of(this, new WithdrawreeiptFactory(this)).get(WithdrawchangereceipeintsendnowViewModel.class);
        binding.setWithdrawchangereceipeintsendnowViewModel(withdrawchangereceipeintsendnowViewModel);
        withdrawchangereceipeintsendnowViewModel.setIds(binding);

        initView();

        binding.datemonthyear.setText(getkey("fastwallet"));
        binding.withdraw.setText(getkey("withdraw_lable"));
        binding.changereceipiet.setText(getkey("changereceipt"));
        binding.sendnow.setText(getkey("sendnow"));


        clicklitener();
        setresponse();
    }

    private void setresponse() {
        withdrawchangereceipeintsendnowViewModel.getWithapihitresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    Type type = new TypeToken<withdrawpaymentsendpojo>() {
                    }.getType();
                    withdrawpaymentsendpojo pojo = new GsonBuilder().create().fromJson(jsonObj.toString(), type);
                    if (pojo.getStatus().equalsIgnoreCase("1")) {
                        Intent i = new Intent(WithdrawchangereceipeintsendnowConstrain.this, WithdrawbanksuccessConstrain.class);
                        i.putExtra("json", jsonObj.toString());
                        i.putExtra("bank", "bank");
                        startActivity(i);
                        EventBus.getDefault().unregister(this);
                    } else {
                        String Sresponse = jsonObj.getString("response");
                        appUtils.AlertError(WithdrawchangereceipeintsendnowConstrain.this, getkey("action_error"), Sresponse);
                    }
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);

        try {
            if (getIntent().hasExtra("transfer_amount")) {
                withdrawamount = getIntent().getStringExtra("transfer_amount");
                json = new JSONObject(getIntent().getStringExtra("json"));
                Type type = new TypeToken<withdrawalspaymentspojo>() {
                }.getType();
                pojo = new GsonBuilder().create().fromJson(json.toString(), type);
                setText(pojo);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setText(withdrawalspaymentspojo pojo) {
        Double minusamount = 0.0;
        try {
            minusamount = Double.parseDouble(withdrawamount) - Double.parseDouble(pojo.getResponse().getWithdraw_fee());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        binding.payto.setText(getkey("payto") + getkey("account_number")+" : " + pojo.getResponse().getBanking_detailsobj().getAcc_number() + "\n" + pojo.getResponse().getBanking_detailsobj().getAcc_holder_name());
        binding.withdrawnames.setText(getkey("widthdrea") +getkey("freecharges") + getkey("recivedamount"));
        binding.withdrawamount.setText(pojo.getResponse().getCurrency() + " " + withdrawamount + "\n" + pojo.getResponse().getCurrency() + " " + pojo.getResponse().getWithdraw_fee() + "\n" + pojo.getResponse().getCurrency() + " " + minusamount);
        AppUtils.setImageviewwithoutcropplaceholder(WithdrawchangereceipeintsendnowConstrain.this, pojo.getResponse().getBanking_detailsobj().getBank_image(), binding.bankimage);
    }

    private void clicklitener() {
        binding.changereceipiet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WithdrawchangereceipeintsendnowConstrain.this, AddbankdetailsConstrain.class);
                i.putExtra("changereceipient", "");
                i.putExtra("json", json.toString());
                i.putExtra("transfer_amount", withdrawamount);
                startActivity(i);
                EventBus.getDefault().unregister(this);
            }
        });

        binding.sendnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inten = new Intent(WithdrawchangereceipeintsendnowConstrain.this, Pin_activity.class);
                inten.putExtra("mode", "withdrawbankpayment");
                startActivity(inten);
            }
        });
    }

    @Subscribe
    public void pincheck(Walletwithdrawpincheck pojos) {
        if (pojos.isIspincentered()) {
            HashMap<String, String> domain = sessionManager.getbankdetails();
            String bankCode = domain.get(SessionManager.KEY_ACCOUNTCODE);
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", sDriverID);
            jsonParams.put("amount", withdrawamount);
            jsonParams.put("mode", sessionManager.getpaymentcode());
            jsonParams.put("bank_code", bankCode);
            jsonParams.put("bank_name", pojo.getResponse().getBanking_detailsobj().getBank_name());
            jsonParams.put("acc_holder_name", pojo.getResponse().getBanking_detailsobj().getAcc_holder_name());
            jsonParams.put("acc_number", pojo.getResponse().getBanking_detailsobj().getAcc_number());
            withdrawchangereceipeintsendnowViewModel.withdrawapihit(Iconstant.wallet_withdraw_amount_url, jsonParams);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}
