package com.blackmaria.newdesigns.view.Transfer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.Transfer.TransfermobilesearchFactory;
import com.blackmaria.newdesigns.viewmodel.transfer.TransfermobilenumbersearchViewModel;
import com.blackmaria.pojo.moneytransferpojo;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityTransfermobilenumbersearchBinding;
import com.blackmaria.iconstant.Iconstant;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Locale;


public class Transfermobilenumbersearch extends AppCompatActivity {

    private ActivityTransfermobilenumbersearchBinding binding;
    private TransfermobilenumbersearchViewModel transfermobilenumbersearchViewModel;
    private SessionManager sessionManager;
    private String sDriverID = "", CountryCode = "";
    private AppUtils appUtils;
    private CountryPicker picker;
    private moneytransferpojo pojo;
    private JSONObject jsonpojo;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mhelper = new LanguageDb(this);
        binding = DataBindingUtil.setContentView(Transfermobilenumbersearch.this, R.layout.activity_transfermobilenumbersearch);
        transfermobilenumbersearchViewModel = ViewModelProviders.of(this, new TransfermobilesearchFactory(this)).get(TransfermobilenumbersearchViewModel.class);
        binding.setTransfermobilenumbersearchViewModel(transfermobilenumbersearchViewModel);
        transfermobilenumbersearchViewModel.setIds(binding);

        initView();

        binding.transfer.setText(getkey("transfer"));
        binding.sendto.setText(getkey("sendto"));
        binding.mobileno.setHint(getkey("mobilenumber"));
        binding.confirfm.setText(getkey("select_receiving_country_and_insert_mobile_number"));
        binding.confirm.setText(getkey("confirm_lable"));

        clicklistener();
        getresponse();
        binding.codetxt.setText("INDONESIA");
        CountryCode = "+62";


        binding.codeimage.setImageResource(R.drawable.flag_mc);

    }

    private void getresponse() {
        transfermobilenumbersearchViewModel.getFindfreindresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    jsonpojo = new JSONObject(response);
                    if (jsonpojo.getString("status").equalsIgnoreCase("1")) {
                        Type type = new TypeToken<moneytransferpojo>() {
                        }.getType();
                        pojo = new GsonBuilder().create().fromJson(jsonpojo.toString(), type);
                        Intent i = new Intent(Transfermobilenumbersearch.this, TransferamountenterConstrain.class);
                        i.putExtra("json", jsonpojo.toString());
                        startActivity(i);
                    } else {
                        appUtils.AlertError(Transfermobilenumbersearch.this, getkey("action_error"), jsonpojo.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);
        picker = CountryPicker.newInstance(getkey("select_country_lable"));
//        SmartLocation.with(this).location()
//                .start(new OnLocationUpdatedListener() {
//                    @Override
//                    public void onLocationUpdated(Location location) {
//                        Geocoder geocoder = new Geocoder(Transfermobilenumbersearch.this, Locale.getDefault());
//                        try {
//                            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
//                            if (addresses != null && !addresses.isEmpty()) {
//
//                                String Str_getCountryCode = addresses.get(0).getCountryCode();
//                                if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
//                                    String drawableName = "flag_"
//                                            + Str_getCountryCode.toLowerCase(Locale.ENGLISH);
//                                    CountryCode = Str_getCountryCode;
//                                    binding.codeimage.setImageResource(getResId(drawableName));
//                                    binding.codetxt.setText(addresses.get(0).getCountryName());
//                                }
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
    }

    private void clicklistener() {
        binding.code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                binding.codetxt.setText(name);
                CountryCode = dialCode;
                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                binding.codeimage.setImageResource(AppUtils.getResId(drawableName));
                AppUtils.CloseKeyboard(Transfermobilenumbersearch.this);
            }
        });

        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.mobileno.getText().length() == 0) {
                    appUtils.AlertError(Transfermobilenumbersearch.this, getkey("action_error"), getkey("profile_label_alert_mobilempty"));
                } else {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", sDriverID);
                    jsonParams.put("country_code", CountryCode);
                    jsonParams.put("phone_number", binding.mobileno.getText().toString().trim());
                    transfermobilenumbersearchViewModel.findfriendapihitresponse(Iconstant.find_Friend_Account_url, jsonParams);
                }
            }
        });
    }


    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
