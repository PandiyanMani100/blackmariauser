package com.blackmaria.newdesigns.view.Withdraw.Paypal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;


import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.Withdraw.Paypal.WithdrawamountenterFactory;
import com.blackmaria.newdesigns.view.Withdraw.Bank.WithdrawbanksuccessConstrain;
import com.blackmaria.newdesigns.view.Pin_activity;
import com.blackmaria.newdesigns.viewmodel.withdraw.paypal.PaypalamountconfirmViewModel;
import com.blackmaria.pojo.Paypalwithdrawpincheck;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityPaypalamountconfirmBinding;
import com.blackmaria.iconstant.Iconstant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Paypalamountconfirm extends AppCompatActivity {

    private ActivityPaypalamountconfirmBinding binding;
    private PaypalamountconfirmViewModel paypalamountconfirmViewModel;
    private String sDriverID = "", paypal_amount = "", paypal_id = "", paypal_paymentcode = "", currency = "";
    private SessionManager sessionManager;
    LanguageDb mhelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        mhelper = new LanguageDb(this);
        binding = DataBindingUtil.setContentView(Paypalamountconfirm.this, R.layout.activity_paypalamountconfirm);
        paypalamountconfirmViewModel = ViewModelProviders.of(this, new WithdrawamountenterFactory(this)).get(PaypalamountconfirmViewModel.class);
        binding.setPaypalamountconfirmViewModel(paypalamountconfirmViewModel);
        paypalamountconfirmViewModel.setIds(binding);

        binding.paytopaypal.setText(getkey("paytopaypal"));
        binding.datemonthyear.setText(getkey("fastwallet"));
        binding.confirm.setText(getkey("confirm_lable"));
        binding.cancel.setText(getkey("cancel_lable"));

        initView();


        clicklistener();
        setResponse();
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);
        if (getIntent().hasExtra("transfer_amount")) {
            paypal_amount = getIntent().getStringExtra("transfer_amount");
            paypal_paymentcode = getIntent().getStringExtra("paypalcode");
            paypal_id = getIntent().getStringExtra("paypalid");
            currency = getIntent().getStringExtra("currency");
            binding.amount.setText(currency + " " + paypal_amount);
            binding.email.setText(paypal_id);
        }
    }

    private void clicklistener() {
        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inten = new Intent(Paypalamountconfirm.this, Pin_activity.class);
                inten.putExtra("mode", "paypalwithdrawpayment");
                startActivity(inten);
            }
        });
    }

    private void setResponse() {
        paypalamountconfirmViewModel.getPaypalapihitresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Intent i = new Intent(Paypalamountconfirm.this, WithdrawbanksuccessConstrain.class);
                        i.putExtra("json", jsonObject.toString());
                        i.putExtra("paypal", "paypal");
                        startActivity(i);
                    }else{

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Subscribe
    public void pincheck(Paypalwithdrawpincheck pojo) {
        if (pojo.isIspincheck()) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", sDriverID);
            jsonParams.put("amount", paypal_amount);
            jsonParams.put("mode", paypal_paymentcode);
            jsonParams.put("paypal_id", paypal_id);
            paypalamountconfirmViewModel.walletwithdrawtopaypal(Iconstant.wallet_withdraw_amount_url, jsonParams);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
