package com.blackmaria.newdesigns.view.Transfer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.Transfer.TransfersuccessFactory;
import com.blackmaria.newdesigns.viewmodel.transfer.TransfersuccessViewModel;
import com.blackmaria.pojo.moneytransferasuccess;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityTransfersuccessConstrainBinding;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class TransfersuccessConstrain extends AppCompatActivity {

    private ActivityTransfersuccessConstrainBinding binding;
    private TransfersuccessViewModel transfersuccessViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private moneytransferasuccess pojo;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mhelper = new LanguageDb(this);
        binding = DataBindingUtil.setContentView(TransfersuccessConstrain.this, R.layout.activity_transfersuccess_constrain);
        transfersuccessViewModel = ViewModelProviders.of(this, new TransfersuccessFactory(this)).get(TransfersuccessViewModel.class);
        binding.setTransfersuccessViewModel(transfersuccessViewModel);
        transfersuccessViewModel.setIds(binding);



        initView();
        binding.datemonthyear.setText(getkey("fastwallet"));
        binding.transfer.setText(getkey("thankyou"));
        binding.success.setText(getkey("success"));
        binding.notificationtxt.setText(getkey("successcontent"));
        binding.cancel.setText(getkey("close_lable"));

    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);

        if (getIntent().hasExtra("json")) {
            Type type = new TypeToken<moneytransferasuccess>() {
            }.getType();
            pojo = new GsonBuilder().create().fromJson(getIntent().getStringExtra("json").toString(), type);
            AppUtils.setImageView(TransfersuccessConstrain.this, pojo.getUser_image(), binding.userimage);
            binding.amount.setText(pojo.getCurrency() + " " + pojo.getTransfer_amount());
            binding.usercontent.setText(getkey("sentti")+"\n" +  getkey("names")+ pojo.getUser_name() + "\n" + getkey("mylogin_dialog_form_mobile_no_label")+" : " + pojo.getCountry_code() + "" + pojo.getPhone_number() + "\n" + getkey("myprofile_dialog_form_city_label")+" : " + pojo.getCity());
            binding.transactionumber.setText(getkey("transaction_num") + pojo.getTransaction_number());
        }
    }

    @Override
    public void onBackPressed() {
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}
