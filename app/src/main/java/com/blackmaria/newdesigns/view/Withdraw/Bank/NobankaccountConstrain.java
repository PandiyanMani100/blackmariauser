package com.blackmaria.newdesigns.view.Withdraw.Bank;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.Withdraw.Bank.NobankaccountFactory;
import com.blackmaria.newdesigns.viewmodel.withdraw.bank.NobankaccountViewModel;
import com.blackmaria.pojo.withdrawalspaymentspojo;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityNobankaccountConstrainBinding;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class NobankaccountConstrain extends AppCompatActivity {

    private ActivityNobankaccountConstrainBinding binding;
    private NobankaccountViewModel nobankaccountViewModel;
    private SessionManager session;
    private AppUtils appUtils;
    private String withdrawamount = "";
    private JSONObject json;
    private withdrawalspaymentspojo pojo;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mhelper = new LanguageDb(this);
        binding = DataBindingUtil.setContentView(NobankaccountConstrain.this, R.layout.activity_nobankaccount_constrain);
        nobankaccountViewModel = ViewModelProviders.of(this, new NobankaccountFactory(this)).get(NobankaccountViewModel.class);
        binding.setNobankaccountViewModel(nobankaccountViewModel);
        nobankaccountViewModel.setIds(binding);
        binding.datemonthyear.setText(getkey("fastwallet"));
        binding.withdraw.setText(getkey("withdraw_lable"));
        binding.sorry.setText(getkey("sorry"));
        binding.sorrynextcontent.setText(getkey("sorrynoaccount"));

        binding.updateprofilecontent.setText(getkey("updateprofilecontent"));
        binding.continuewithdraw.setText(getkey("updateprofile"));
        binding.cancel.setText(getkey("cancel_lable"));
        initView();





        clicklistener();
    }

    private void initView() {
        session = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        try {
            if (getIntent().hasExtra("transfer_amount")) {
                withdrawamount = getIntent().getStringExtra("transfer_amount");
                json = new JSONObject(getIntent().getStringExtra("json"));
                Type type = new TypeToken<withdrawalspaymentspojo>() {
                }.getType();
                pojo = new GsonBuilder().create().fromJson(json.toString(), type);
            }
        }catch (JSONException e){e.printStackTrace();}
    }

    private void clicklistener() {
        binding.continuewithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NobankaccountConstrain.this, AddbankdetailsConstrain.class);
                i.putExtra("transfer_amount", withdrawamount);
                i.putExtra("json", json.toString());
                startActivity(i);
            }
        });
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
