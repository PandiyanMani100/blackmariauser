package com.blackmaria.newdesigns.view;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.RechargechoosepaymentFactory;
import com.blackmaria.newdesigns.view.Recharge.Bank.RechargeselectbankConstrain;
import com.blackmaria.newdesigns.view.Recharge.Card.RechargecardpaymentConstrain;
import com.blackmaria.newdesigns.view.Recharge.Paypal.PaypalenteridConstrain;
import com.blackmaria.newdesigns.viewmodel.RechargechoosepaymentViewModel;
import com.blackmaria.pojo.bankpaymentlistpojo;
import com.blackmaria.pojo.choosepaymentmodel;
import com.blackmaria.pojo.rechargeamountpojo;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityRechargechoosepaymentBinding;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class Rechargechoosepayment extends AppCompatActivity {

    private ActivityRechargechoosepaymentBinding binding;
    private RechargechoosepaymentViewModel rechargechoosepaymentViewModel;
    private SessionManager sessionManager;
    private String sDriverID = "", insertAmount = "", paymenttype = "",jsonoldcard="";
    private rechargeamountpojo pojomodel;
    private com.blackmaria.pojo.bankpaymentlistpojo bankpojo;
    private JSONObject bankpaymentlistpojo;
    private ServiceRequest mRequest;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(Rechargechoosepayment.this, R.layout.activity_rechargechoosepayment);
        rechargechoosepaymentViewModel = ViewModelProviders.of(this, new RechargechoosepaymentFactory(this)).get(RechargechoosepaymentViewModel.class);
        binding.setRechargechoosepaymentViewModel(rechargechoosepaymentViewModel);
        rechargechoosepaymentViewModel.setIds(binding);
        mhelper = new LanguageDb(this);
        initView();
        setresponse();
        binding.selectpayment.setText(getkey("selectpaymentchannel"));
    }

    private void setresponse() {
        rechargechoosepaymentViewModel.getGetbankpaymentlistresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    bankpaymentlistpojo = new JSONObject(response);
                    Type type = new TypeToken<bankpaymentlistpojo>() {
                    }.getType();
                    bankpojo = new GsonBuilder().create().fromJson(bankpaymentlistpojo.toString(), type);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);

        if (getIntent().hasExtra("jsonpojo")) {
            insertAmount = getIntent().getStringExtra("insertAmount");
            Type type = new TypeToken<rechargeamountpojo>() {
            }.getType();
            jsonoldcard = getIntent().getStringExtra("jsonpojo").toString();
            pojomodel = new GsonBuilder().create().fromJson(getIntent().getStringExtra("jsonpojo").toString(), type);
            setpaymentlist();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", sDriverID);
        rechargechoosepaymentViewModel.getbankrechargepaymentlist(Iconstant.getPaymentOPtion_url_new, jsonParams);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    private void setpaymentlist() {
        try {
            binding.paymentlist.removeAllViews();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= pojomodel.getResponse().getPayment_list().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.choosepaymentcustomlayout, null);
            ImageView image = view.findViewById(R.id.image);
            LinearLayout constrainview = view.findViewById(R.id.constrainview);
            AppUtils.setImageviewwithoutcropplaceholder(Rechargechoosepayment.this, pojomodel.getResponse().getPayment_list().get(i).getIcon(), image);
            final int finalI = i;
            constrainview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", sDriverID);
                    jsonParams.put("amount", insertAmount);
                    jsonParams.put("mode", pojomodel.getResponse().getPayment_list().get(finalI).getCode());
                    jsonParams.put("mode_id", pojomodel.getResponse().getPayment_list().get(finalI).getId());
                    postRequest_walletapicheck(Iconstant.rechargecheck_url, jsonParams,finalI);

                }
            });
            binding.paymentlist.addView(view);
        }
    }

    //-----------------------wallet Check Post Request-----------------
    private void postRequest_walletapicheck(String Url, HashMap<String, String> jsonParams, final int finalI) {
        final Dialog dialog = new Dialog(Rechargechoosepayment.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));


        System.out.println("-------------walletMoney Transaction Url----------------" + Url);


        mRequest = new ServiceRequest(Rechargechoosepayment.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------walletMoney Transaction Response----------------" + response);

                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "",ScurrencySymbol="",Str_total_amount="",Str_total_transaction="";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        paymenttype = pojomodel.getResponse().getPayment_list().get(finalI).getCode();
                        if (paymenttype.equalsIgnoreCase("xendit-banktransfer") || paymenttype.equalsIgnoreCase("xendit-bankpayment")) {
                            Intent i = new Intent(Rechargechoosepayment.this, RechargeselectbankConstrain.class);
                            i.putExtra("paymentlist", bankpaymentlistpojo.toString());
                            i.putExtra("insertAmount", insertAmount);
                            i.putExtra("currency", pojomodel.getResponse().getCurrency());
                            startActivity(i);
                            EventBus.getDefault().unregister(this);
                        }
                        else if (paymenttype.equalsIgnoreCase("xendit-card"))
                        {
                            if (pojomodel.getXendit_charge_status().equalsIgnoreCase("1"))
                            {
                                Double amount = Double.parseDouble(insertAmount) * Double.parseDouble(pojomodel.getResponse().getExchange_value());
                                amount = (Double) Math.ceil(amount);
                                Intent intent1 = new Intent(Rechargechoosepayment.this, RechargecardpaymentConstrain.class);
                                intent1.putExtra("insertAmount", insertAmount);
                                intent1.putExtra("jsonpojo", jsonoldcard);
                                intent1.putExtra("currency", pojomodel.getResponse().getCurrency());
                                startActivity(intent1);
                                EventBus.getDefault().unregister(this);


                            } else {
                                Intent intent1 = new Intent(Rechargechoosepayment.this, RechargecardpaymentConstrain.class);
                                intent1.putExtra("insertAmount", insertAmount);
                                intent1.putExtra("jsonpojo", jsonoldcard);
                                intent1.putExtra("currency", pojomodel.getResponse().getCurrency());
                                startActivity(intent1);
                                EventBus.getDefault().unregister(this);
                            }
                        } else if (paymenttype.toLowerCase().equalsIgnoreCase("paypal")) {
                            AlertPaypal("", getkey("reload_lable")+ " " + insertAmount.trim() +  getkey("viapaypal"));
                        }
                        //finish();

                       /* if (pojomodel.getResponse().getPayment_list().get(finalI).getCode().equalsIgnoreCase("xendit-banktransfer")) {
                            paymenttype = pojomodel.getResponse().getPayment_list().get(finalI).getCode();
                            Intent inten = new Intent(Rechargechoosepayment.this, Pin_activity.class);
                            inten.putExtra("mode", "choosepayment");
                            startActivity(inten);
                        } else if (pojomodel.getResponse().getPayment_list().get(finalI).getCode().equalsIgnoreCase("xendit-card")) {
                            paymenttype = pojomodel.getResponse().getPayment_list().get(finalI).getCode();
                            Intent inten = new Intent(Rechargechoosepayment.this, Pin_activity.class);
                            inten.putExtra("mode", "choosepayment");
                            startActivity(inten);
                        } else if (pojomodel.getResponse().getPayment_list().get(finalI).getCode().equalsIgnoreCase("paypal")) {
                            paymenttype = pojomodel.getResponse().getPayment_list().get(finalI).getCode();
                            Intent inten = new Intent(Rechargechoosepayment.this, Pin_activity.class);
                            inten.putExtra("mode", "choosepayment");
                            startActivity(inten);
                        }
*/
                    } else {
                        String Sresponse = object.getString("response");
                        Toast.makeText(Rechargechoosepayment.this, Sresponse, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

    }

    @Subscribe
    public void pincheck(choosepaymentmodel pojo) {
        if (pojo.isIsenabled()) {

        }
    }

    private void AlertPaypal(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Rechargechoosepayment.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_yes"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("walletMoney_recharge_amount", insertAmount);
                    jsonObject.put("walletMoney_currency_symbol", pojomodel.getResponse().getCurrency());
                    jsonObject.put("walletMoney_currentBalance", pojomodel.getResponse().getCurrent_balance());
                    jsonObject.put("walletMoney_paymentType", paymenttype);
                    Intent intent = new Intent(Rechargechoosepayment.this, PaypalenteridConstrain.class);
                    intent.putExtra("json", jsonObject.toString());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    EventBus.getDefault().unregister(this);
                    mDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        mDialog.setNegativeButton(getkey("action_no"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
