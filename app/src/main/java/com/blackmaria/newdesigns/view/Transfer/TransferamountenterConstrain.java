package com.blackmaria.newdesigns.view.Transfer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.Transfer.TransferamountenterFactory;
import com.blackmaria.newdesigns.view.Pin_activity;
import com.blackmaria.newdesigns.viewmodel.transfer.TransferamountenterViewModel;
import com.blackmaria.pojo.amounttransferfriendpinenter;
import com.blackmaria.pojo.moneytransferpojo;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityTransferamountenterConstrainBinding;
import com.blackmaria.iconstant.Iconstant;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class TransferamountenterConstrain extends AppCompatActivity {

    private ActivityTransferamountenterConstrainBinding binding;
    private TransferamountenterViewModel transferamountenterViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private moneytransferpojo pojo;
    LanguageDb mhelper;
    private String sDriverID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        mhelper= new LanguageDb(this);
        binding = DataBindingUtil.setContentView(TransferamountenterConstrain.this, R.layout.activity_transferamountenter_constrain);
        transferamountenterViewModel = ViewModelProviders.of(this, new TransferamountenterFactory(this)).get(TransferamountenterViewModel.class);
        binding.setTransferamountenterViewModel(transferamountenterViewModel);
        transferamountenterViewModel.setIds(binding);

        initView();

        binding.datemonthyear.setText(getkey("fastwallet"));
        binding.transfer.setText(getkey("transfer"));
        binding.mobileno.setHint(getkey("inseramount"));
        binding.sendnow.setText(getkey("sendnow"));
        binding.cancel.setText(getkey("cancel_lable"));

        clicklistener();
        apiresponse();
    }

    private void apiresponse() {
        transferamountenterViewModel.getTransferapiresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Intent intent = new Intent(TransferamountenterConstrain.this, TransfersuccessConstrain.class);
                        intent.putExtra("json", jsonObject.toString());
                        startActivity(intent);
                        finish();
                        EventBus.getDefault().unregister(this);
                    } else {
                        appUtils.AlertError(TransferamountenterConstrain.this, getkey("action_error"), jsonObject.getString("response"));

//                        binding.notfounduser.setText(jsonObject.getString("response"));
//                        binding.notfounduser.setVisibility(View.VISIBLE);
//                        binding.notfoundusercontent.setVisibility(View.VISIBLE);

                       // binding.transfer.setBackground(getResources().getDrawable(R.drawable.white_curve));
                        binding.notfoundusercontent.setVisibility(View.INVISIBLE);
                        binding.notfoundusercontent.setText(pojo.getResponse().getUser_name() + " " +getkey("usernotfound"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        transferamountenterViewModel.getCrossborderratesresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Intent intent = new Intent(TransferamountenterConstrain.this, TransfercrossborderConstrain.class);
                        intent.putExtra("json", jsonObject.toString());
                        startActivity(intent);
                        EventBus.getDefault().unregister(this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);
        if (getIntent().hasExtra("json")) {
            Type type = new TypeToken<moneytransferpojo>() {
            }.getType();
            pojo = new GsonBuilder().create().fromJson(getIntent().getStringExtra("json").toString(), type);
            setTextresponse(pojo);
        }


    }

    private void clicklistener() {
        binding.sendnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.mobileno.getText().length() == 0) {
                    appUtils.AlertError(TransferamountenterConstrain.this, getkey("action_error"), getkey("amountcantbempty"));
                } else if (Integer.parseInt(binding.mobileno.getText().toString().trim()) < 1) {
                    appUtils.AlertError(TransferamountenterConstrain.this,getkey("action_error"),getkey("amountcantbemptyzero"));
                } else if (pojo.getResponse().getCross_border().equalsIgnoreCase("1")) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", sDriverID);
                    jsonParams.put("transfer_amount", binding.mobileno.getText().toString().trim());
                    jsonParams.put("friend_id", pojo.getResponse().getUser_id());
                    transferamountenterViewModel.crossbordergetrates(Iconstant.cloudmoney_crossbordervalue_Url, jsonParams);
                } else {
                    Intent ii = new Intent(TransferamountenterConstrain.this, Pin_activity.class);
                    ii.putExtra("mode", "transferamount");
                    startActivity(ii);
                }
            }
        });
    }

    @Subscribe
    public void pinentered(amounttransferfriendpinenter pojos) {
        if (pojos.isIspinenter()) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", sDriverID);
            jsonParams.put("transfer_amount", binding.mobileno.getText().toString().trim());
            jsonParams.put("friend_id", pojo.getResponse().getUser_id());
            jsonParams.put("cross_transfer", pojo.getResponse().getCross_border());
            transferamountenterViewModel.transferamounttofriend(Iconstant.find_Friend_Account_transfer_url, jsonParams);
        }
    }

    private void setTextresponse(moneytransferpojo pojo) {
        AppUtils.setImageView(TransferamountenterConstrain.this, pojo.getResponse().getUser_image(), binding.userimage);
        binding.usercontent.setText(getkey("transfer_to")+ getkey("names")+ pojo.getResponse().getUser_name() + "\n" +getkey("mylogin_dialog_form_mobile_no_label")+" : " + pojo.getResponse().getCountry_code() + "" + pojo.getResponse().getPhone_number() + "\n" + getkey("myprofile_dialog_form_city_label")+" : " + pojo.getResponse().getCity());
        if (pojo.getResponse().getCross_border().equalsIgnoreCase("1")) {
            binding.sendnow.setText(getkey("getrates"));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}
