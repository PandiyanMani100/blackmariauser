package com.blackmaria.newdesigns.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.newdesigns.Accountclosed;
import com.blackmaria.newdesigns.Forgetpin;
import com.blackmaria.pojo.Paypalwithdrawpincheck;
import com.blackmaria.pojo.Walletwithdrawpincheck;
import com.blackmaria.pojo.amounttransferfriendpinenter;
import com.blackmaria.pojo.cashwithdrawpincheck;
import com.blackmaria.pojo.choosepaymentmodel;
import com.blackmaria.pojo.crossborderpintenter;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.devspark.appmsg.AppMsg;

import org.greenrobot.eventbus.EventBus;

public class Pin_activity extends AppCompatActivity {

    private TextView img2, img1, img3, img5, img4, img6, img8, img7, img9, img0, img_star, img_hash, confirm;
    private EditText edt_email;
    private LinearLayout ly_forgetpin;
    private ImageView booking_back_imgeview;
    private String number = "", mode = "";
    int[] imageViews = {R.id.img1, R.id.img2, R.id.img3, R.id.img4, R.id.img5, R.id.img6, R.id.img7, R.id.img8, R.id.img9, R.id.img0};
    private float TIME_DELAY = 2;
    private float lastY = 0;
    private SessionManager session;
    private AppUtils appUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_activity);

        init();
        clicklistener();
    }

    private View.OnTouchListener otl = new View.OnTouchListener() {
        public boolean onTouch (View v, MotionEvent event) {
            return true; // the listener has consumed the event
        }
    };

    private void init() {
        session = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        ly_forgetpin = findViewById(R.id.ly_forgetpin);
        edt_email = findViewById(R.id.edt_email);
        edt_email.setOnTouchListener(otl);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);
        confirm = findViewById(R.id.confirm);
        img2 = findViewById(R.id.img2);
        img1 = findViewById(R.id.img1);
        img3 = findViewById(R.id.img3);
        img5 = findViewById(R.id.img5);
        img4 = findViewById(R.id.img4);
        img6 = findViewById(R.id.img6);
        img8 = findViewById(R.id.img8);
        img7 = findViewById(R.id.img7);
        img9 = findViewById(R.id.img9);
        img0 = findViewById(R.id.img0);
        img_star = findViewById(R.id.img_star);
        img_hash = findViewById(R.id.img_hash);
        if (getIntent().hasExtra("mode")) {
            mode = getIntent().getStringExtra("mode");
        }
    }

    private void clicklistener() {

        edt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
            }
        });

        edt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 6) {
                    confirm.setBackground(getResources().getDrawable(R.drawable.green_curve));
                } else {
                    confirm.setBackground(getResources().getDrawable(R.drawable.grey_curve));
                }
            }
        });

        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "1";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());

                setbackground(img1.getId());
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "2";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img2.getId());
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "3";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img3.getId());
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "4";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img4.getId());
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "5";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img5.getId());
            }
        });

        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "6";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img6.getId());
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "7";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img7.getId());
            }
        });
        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "8";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img8.getId());
            }
        });
        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "9";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img9.getId());
            }
        });

        img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "0";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img0.getId());
            }
        });

        img_hash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String SMobileNo = edt_email.getText().toString();
                if (SMobileNo != null && SMobileNo.length() > 0) {
                    edt_email.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                    edt_email.setSelection(edt_email.getText().length());
                    number = edt_email.getText().toString();
                }

                img_hash.setBackground(getResources().getDrawable(R.drawable.round_green));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img_hash.setBackground(getResources().getDrawable(R.drawable.round_white));
                    }
                }, 100);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.getText().toString().length() == 0) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_pincode));
                } else if (edt_email.getText().toString().trim().length() != 6) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_pincodes));
                } else {
                    ConnectionDetector cd = new ConnectionDetector(Pin_activity.this);
                    boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        String sSecurePin = session.getSecurePin();
                        if (sSecurePin.equalsIgnoreCase(edt_email.getText().toString().trim())) {
                            if (mode.equalsIgnoreCase("")) {
                                EventBus.getDefault().post("done");
                                finish();
                            } else if (mode.equalsIgnoreCase("choosepayment")) {
                                choosepaymentmodel pojo = new choosepaymentmodel();
                                pojo.setIsenabled(true);
                                EventBus.getDefault().post(pojo);
                                finish();
                            } else if (mode.equalsIgnoreCase("transferamount")) {
                                amounttransferfriendpinenter pojo = new amounttransferfriendpinenter();
                                pojo.setIspinenter(true);
                                EventBus.getDefault().post(pojo);
                                finish();
                            } else if (mode.equalsIgnoreCase("crossborderamount")) {
                                crossborderpintenter pojo = new crossborderpintenter();
                                pojo.setIspinter(true);
                                EventBus.getDefault().post(pojo);
                                finish();
                            } else if (mode.equalsIgnoreCase("withdrawbankpayment")) {
                                Walletwithdrawpincheck pojo = new Walletwithdrawpincheck();
                                pojo.setIspincentered(true);
                                EventBus.getDefault().post(pojo);
                                finish();
                            } else if (mode.equalsIgnoreCase("paypalwithdrawpayment")) {
                                Paypalwithdrawpincheck pojo = new Paypalwithdrawpincheck();
                                pojo.setIspincheck(true);
                                EventBus.getDefault().post(pojo);
                                finish();
                            } else if (mode.equalsIgnoreCase("cashwithdrawal")) {
                                cashwithdrawpincheck pojo = new cashwithdrawpincheck();
                                pojo.setIspincheck(true);
                                EventBus.getDefault().post(pojo);
                                finish();
                            } else {
                                Intent ii = new Intent(Pin_activity.this, Accountclosed.class);
                                ii.putExtra("mode", mode);
                                startActivity(ii);
                                finish();
                            }
                        } else {
                            number = "";
                            appUtils.AlertError(Pin_activity.this, getResources().getString(R.string.action_error), getResources().getString(R.string.you_enter_wrong_pin));
                        }
                    } else {
                        appUtils.AlertError(Pin_activity.this, getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }
                }
            }
        });

        ly_forgetpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Pin_activity.this, Forgetpin.class);
                startActivity(i);
            }
        });
    }

    private void setbackground(int image) {
        for (int i = 0; i <= imageViews.length - 1; i++) {
            final TextView img = findViewById(imageViews[i]);
            if (image == imageViews[i]) {
                img.setBackground(getResources().getDrawable(R.drawable.round_green));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img.setBackground(getResources().getDrawable(R.drawable.round_white));
                    }
                }, 100);
            } else {
                img.setBackground(getResources().getDrawable(R.drawable.round_white));
            }
        }


    }


    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(Pin_activity.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

}
