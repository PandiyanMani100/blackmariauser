package com.blackmaria.newdesigns.view.Withdraw.Bank;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.Withdraw.Bank.WithdrawbanksuccessFactory;
import com.blackmaria.newdesigns.viewmodel.withdraw.bank.WithdrawbanksuccessViewModel;
import com.blackmaria.pojo.withdrawpaymentsendpojo;
import com.blackmaria.R;
import com.blackmaria.databinding.ActivityWithdrawbanksuccessConstrainBinding;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class WithdrawbanksuccessConstrain extends AppCompatActivity {

    private ActivityWithdrawbanksuccessConstrainBinding binding;
    private WithdrawbanksuccessViewModel withdrawbanksuccessViewModel;
    private withdrawpaymentsendpojo pojo;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mhelper = new LanguageDb(this);
        binding = DataBindingUtil.setContentView(WithdrawbanksuccessConstrain.this, R.layout.activity_withdrawbanksuccess_constrain);
        withdrawbanksuccessViewModel = ViewModelProviders.of(this, new WithdrawbanksuccessFactory(this)).get(WithdrawbanksuccessViewModel.class);
        binding.setWithdrawbanksuccessViewModel(withdrawbanksuccessViewModel);
        withdrawbanksuccessViewModel.setIds(binding);

        initView();

        binding.datemonthyear.setText(getkey("fastwallet"));
        binding.withdraw.setText(getkey("withdraw_lable"));
        binding.success.setText(getkey("success"));
        binding.cancel.setText(getkey("close_lable"));
    }

    private void initView() {
        if (getIntent().hasExtra("json")) {
            Type type = new TypeToken<withdrawpaymentsendpojo>() {
            }.getType();
            pojo = new GsonBuilder().create().fromJson(getIntent().getStringExtra("json").toString(), type);

            if (getIntent().hasExtra("paypal")) {
                setText(pojo, "paypal");
            } else if (getIntent().hasExtra("cash")) {
                setText(pojo, "cash");
            } else if (getIntent().hasExtra("bank")) {
                setText(pojo, "bank");
            }

        }
    }

    private void setText(withdrawpaymentsendpojo pojo, String paymenttype) {
        if (paymenttype.equalsIgnoreCase("bank")) {
            binding.amount.setText(pojo.getCurrency() + " " + pojo.getAmount());
            binding.usercontent.setText(getkey("withdrawnto") + pojo.getBank_name() + "\n" + getkey("new_wallet_lable_accountnumber")+" " + pojo.getAccount() + "\n\n" + getkey("from") + pojo.getFrom() + "\n" +getkey("wallet_credit_statement_date")+" : " + pojo.getDate() + "\n" + getkey("time")+" : " + pojo.getTime());
            binding.transactionumber.setText(getkey("transaction_num") + pojo.getRef_no());
            binding.contentcredit.setText(getkey("youbankcredit") + "\n" + getkey("accpording_to"));
        } else if (paymenttype.equalsIgnoreCase("paypal")) {
            try {
                binding.amount.setText(pojo.getCurrency() + " " + pojo.getAmount());
                binding.usercontent.setText(getkey("from") + pojo.getFrom() + "\n" + getkey("date")+" : " + pojo.getDate() + "\n" + getkey("time")+" : " + pojo.getTime() + "\n" + pojo.getCountry_code() + "" + pojo.getPhone_number() + "\n\n" +getkey("paypalid") + "\n" + pojo.getPaypal_id());
                binding.transactionumber.setText(getkey("transaction_num") + pojo.getRef_no());
                binding.contentcredit.setText(getkey("papalaccountinfi")+ "\n" +getkey("accordingcutmonedy"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {

    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
