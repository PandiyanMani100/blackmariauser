package com.blackmaria.newdesigns.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.RechargepaymentsuccessFactory;
import com.blackmaria.newdesigns.viewmodel.RechargepaymentsuccessViewModel;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityRechargepaymentsuccessConstrainBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RechargepaymentsuccessConstrain extends AppCompatActivity {

    private ActivityRechargepaymentsuccessConstrainBinding binding;
    private RechargepaymentsuccessViewModel rechargepaymentsuccessViewModel;
    private SessionManager sessionManager;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(RechargepaymentsuccessConstrain.this, R.layout.activity_rechargepaymentsuccess_constrain);
        rechargepaymentsuccessViewModel = ViewModelProviders.of(this, new RechargepaymentsuccessFactory(this)).get(RechargepaymentsuccessViewModel.class);
        binding.setRechargepaymentsuccessViewModel(rechargepaymentsuccessViewModel);
        rechargepaymentsuccessViewModel.setIds(binding);
        mhelper = new LanguageDb(this);
        sessionManager = SessionManager.getInstance(this);
        binding.selectpayment.setText(getkey("reload_success"));
        binding.thankyou.setText(getkey("thankyou"));
        binding.from.setText(getkey("trip_label_from"));
        binding.cancel.setText(getkey("close_lable"));

        HashMap<String, String> user = sessionManager.getUserDetails();

        if (getIntent().hasExtra("json")) {
            try {
                JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("json"));
                setText(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    private void setText(JSONObject obj) {
        try {
            binding.name.setText(obj.getString("user_name"));
            binding.date.setText(obj.getString("date"));
            binding.time.setText(obj.getString("time"));
            if (getIntent().getStringExtra("paymenttype").equalsIgnoreCase("bank")) {
                binding.amount.setText(obj.getString("currency") + " " + obj.getString("amount"));
                binding.transactionumber.setText(getkey("trans_no") + obj.getString("transaction_id"));
                binding.mobileno.setText(obj.getString("country_code") + "" + obj.getString("phone_number"));
                binding.paymenttype.setTextColor(getResources().getColor(R.color.blue_color));
                binding.paymentcode.setTextColor(getResources().getColor(R.color.blue_color));
                binding.paymenttype.setText(getkey("paymnet"));
                binding.paymentcode.setAllCaps(true);
                binding.paymentcode.setText(obj.getString("payment_code"));
                AppUtils.setImageviewwithoutcropplaceholder(RechargepaymentsuccessConstrain.this, obj.getString("bank_image"), binding.paymentimage);
            } else if (getIntent().getStringExtra("paymenttype").equalsIgnoreCase("paypal")) {
                binding.mobileno.setText(obj.getString("country_code") + "" + obj.getString("phone_number"));
                binding.paymenttype.setText(getkey("paypal_id"));
                binding.paymentcode.setText(obj.getString("paypal_id"));
                binding.paymentcode.setAllCaps(false);
                AppUtils.setImageviewwithoutcropplaceholder(RechargepaymentsuccessConstrain.this, obj.getString("icon_active"), binding.paymentimage);
            } else if(getIntent().getStringExtra("paymenttype").equalsIgnoreCase("xtendit")){
                binding.transactionumber.setText(getkey("trans_no") + obj.getString("transaction_no"));
                binding.amount.setText(obj.getString("currency_code") + " " + obj.getString("recharge_amount"));
                binding.mobileno.setText(obj.getString("mobile_number"));
                binding.paymenttype.setText(getkey("card_num"));
                binding.paymentcode.setText(obj.getString("card_number"));
                AppUtils.setImageviewwithoutcropplaceholder(RechargepaymentsuccessConstrain.this, obj.getString("card_image"), binding.paymentimage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }



    @Override
    public void onBackPressed() {

    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
