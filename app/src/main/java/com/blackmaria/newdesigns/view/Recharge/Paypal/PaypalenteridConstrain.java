package com.blackmaria.newdesigns.view.Recharge.Paypal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.Recharge.Paypal.PaypalenteridFactory;
import com.blackmaria.newdesigns.viewmodel.recharge.paypal.PaypalenteridViewModel;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.WalletMoneyWebview;
import com.blackmaria.databinding.ActivityPaypalenteridConstrainBinding;

import org.json.JSONException;
import org.json.JSONObject;

public class PaypalenteridConstrain extends AppCompatActivity {

    private ActivityPaypalenteridConstrainBinding binding;
    private PaypalenteridViewModel paypalenteridViewModel;
    private JSONObject json;
    private AppUtils appUtils;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(PaypalenteridConstrain.this, R.layout.activity_paypalenterid_constrain);
        paypalenteridViewModel = ViewModelProviders.of(this, new PaypalenteridFactory(this)).get(PaypalenteridViewModel.class);
        binding.setPaypalenteridViewModel(paypalenteridViewModel);
        paypalenteridViewModel.setIds(binding);
        mhelper = new LanguageDb(this);

        initView();

        binding.datemonthyear.setText(getkey("fastwallet"));
        binding.addcredit.setText(getkey("addcredit"));
        binding.enterpaypalid.setText(getkey("enterpaypalid"));
        binding.paypalemailid.setHint(getkey("enteremailcom"));
        binding.paypalcontent.setText(getkey("paypalcontent"));
        binding.confirm.setText(getkey("confirm_lable"));


        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.paypalemailid.getText().toString().trim().length() == 0) {
                    appUtils.AlertError(PaypalenteridConstrain.this,getkey("timer_label_alert_sorry"), getkey("profile_label_alert_emailempty"));
                } else if (!AppUtils.isValidEmail(binding.paypalemailid.getText().toString().trim().replace(" ", ""))) {
                    appUtils.AlertError(PaypalenteridConstrain.this,getkey("timer_label_alert_sorry"), getkey("profile_label_alert_email"));
                } else {
                    try {
                        Intent intent = new Intent(PaypalenteridConstrain.this, WalletMoneyWebview.class);
                        intent.putExtra("walletMoney_recharge_amount", json.getString("walletMoney_recharge_amount"));
                        intent.putExtra("walletMoney_currency_symbol", json.getString("walletMoney_currency_symbol"));
                        intent.putExtra("walletMoney_currentBalance", json.getString("walletMoney_currentBalance"));
                        intent.putExtra("walletMoney_paymentType", json.getString("walletMoney_paymentType"));
                        intent.putExtra("email", binding.paypalemailid.getText().toString().trim());
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }



    private void initView() {
        appUtils = AppUtils.getInstance(this);
        if (getIntent().hasExtra("json")) {
            try {
                json = new JSONObject(getIntent().getStringExtra("json"));
                binding.creditamount.setText(json.getString("walletMoney_currency_symbol") + " " + json.getString("walletMoney_recharge_amount"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
