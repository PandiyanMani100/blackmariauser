package com.blackmaria.newdesigns.view.Transfer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;

import com.blackmaria.newdesigns.factory.Transfer.TransfercrossborderFactory;
import com.blackmaria.newdesigns.view.Pin_activity;
import com.blackmaria.newdesigns.viewmodel.transfer.TransfercrossborderViewModel;
import com.blackmaria.pojo.crossborderpintenter;
import com.blackmaria.pojo.crossborderratespojo;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityTransfercrossborderConstrainBinding;
import com.blackmaria.iconstant.Iconstant;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class TransfercrossborderConstrain extends AppCompatActivity {

    private ActivityTransfercrossborderConstrainBinding binding;
    private TransfercrossborderViewModel transfercrossborderViewModel;
    private crossborderratespojo pojo;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private AppUtils appUtils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(TransfercrossborderConstrain.this, R.layout.activity_transfercrossborder_constrain);
        transfercrossborderViewModel = ViewModelProviders.of(this, new TransfercrossborderFactory(this)).get(TransfercrossborderViewModel.class);
        binding.setTransfercrossborderViewModel(transfercrossborderViewModel);
        transfercrossborderViewModel.setIds(binding);

        initView();
        clicklistener();
        setResponse();
    }

    private void setResponse() {
        transfercrossborderViewModel.getCrossvordertransferresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Intent intent = new Intent(TransfercrossborderConstrain.this, TransfersuccessConstrain.class);
                        intent.putExtra("json", jsonObject.toString());
                        startActivity(intent);
                        finish();
                        EventBus.getDefault().unregister(this);
                    } else {
                        appUtils.AlertError(TransfercrossborderConstrain.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Subscribe
    public void pinentered(crossborderpintenter pojos) {
        if (pojos.isIspinter()) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", sDriverID);
            jsonParams.put("transfer_amount", pojo.getSend_amount());
            jsonParams.put("friend_id", pojo.getFriend_id());
            jsonParams.put("cross_transfer", "1");
            transfercrossborderViewModel.transferamounttofriend(Iconstant.find_Friend_Account_transfer_url, jsonParams);
        }
    }

    private void clicklistener() {
        binding.sendnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(TransfercrossborderConstrain.this, Pin_activity.class);
                ii.putExtra("mode", "crossborderamount");
                startActivity(ii);
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);

        if (getIntent().hasExtra("json")) {
            Type type = new TypeToken<crossborderratespojo>() {
            }.getType();
            pojo = new GsonBuilder().create().fromJson((getIntent().getStringExtra("json").toString()), type);
            setText(pojo);
        }
    }

    private void setText(crossborderratespojo pojo) {
        binding.senderfrom.setText(getResources().getString(R.string.senderfrom) + pojo.getSender_city());
        binding.senderamount.setText(pojo.getCurrency_transfer() + " " + pojo.getTransfer_amount());
        binding.receivedfrom.setText(getResources().getString(R.string.recivedin)+ pojo.getCity());
        binding.receivedamount.setText(pojo.getCurrency_received() + " " + pojo.getReceived_amount());
        AppUtils.setImageView(TransfercrossborderConstrain.this, pojo.getUser_image(), binding.userimage);
        binding.usercontent.setText(getResources().getString(R.string.transfer_to) + getResources().getString(R.string.names) + pojo.getUser_name() + "\n" + getResources().getString(R.string.mylogin_dialog_form_mobile_no_label)+" : " + pojo.getCountry_code() + "" + pojo.getPhone_number() + "\n" + getResources().getString(R.string.loc) + pojo.getCity());
        binding.amountsender.setText(getResources().getString(R.string.amount_tender) + "\n" + getResources().getString(R.string.feecharge) + "\n" + getResources().getString(R.string.sendamount));
        binding.amountsenderwithcurrency.setText(": " + pojo.getCurrency_transfer() + "  " + pojo.getTransfer_amount() + "\n" + ": " + pojo.getCurrency_transfer() + "  " + pojo.getTransfer_fee() + "\n" + ": " + pojo.getCurrency_transfer() + "  " + pojo.getSend_amount());
        binding.conversationratesvalue.setText("1 " + pojo.getCurrency_transfer() + "  =  " + pojo.getConversion_rate() + " " + pojo.getCurrency_received() + "\n" + pojo.getSend_amount() +" "+ pojo.getCurrency_transfer() + "  =  " + pojo.getReceived_amount() + " " + pojo.getCurrency_received());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
