package com.blackmaria.newdesigns.view.Withdraw.Cash;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.Withdraw.Cash.CashwithdrawalFactory;
import com.blackmaria.newdesigns.view.Pin_activity;
import com.blackmaria.newdesigns.viewmodel.withdraw.cash.CashwithdrawalViewModel;
import com.blackmaria.pojo.cashwithdrawpincheck;
import com.blackmaria.pojo.lookupcashpojo;
import com.blackmaria.pojo.withdrawpaymentsendpojo;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityCashwithdrawalConstrainBinding;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class CashwithdrawalConstrain extends AppCompatActivity {
    private CashwithdrawalViewModel cashwithdrawalViewModel;
    private ActivityCashwithdrawalConstrainBinding binding;
    private String cashwithdrawid = "",sDriverID = "", transfer_amount = "", service_amount = "", name = "", phone = "", image = "", currency = "", paymentcode = "";
    private SessionManager sessionManager;
    private AppUtils appUtils;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        mhelper=new LanguageDb(this);
        binding = DataBindingUtil.setContentView(CashwithdrawalConstrain.this, R.layout.activity_cashwithdrawal_constrain);
        cashwithdrawalViewModel = ViewModelProviders.of(this, new CashwithdrawalFactory(this)).get(CashwithdrawalViewModel.class);
        binding.setCashwithdrawalViewModel(cashwithdrawalViewModel);
        cashwithdrawalViewModel.setIds(binding);




        initView();
        binding.fastpaybalancename.setText(getkey("fastcash"));
        binding.lookupcash.setText(getkey("lookupcash"));
        binding.confirm.setText(getkey("confirm"));
        binding.notes.setText(getkey("notescashwithdrawal"));
        binding.help.setText(getkey("helpcashwithdrawal"));
        binding.cancel.setText(getkey("cancel_lable"));
        clicklistener();
        setResponse();
    }

    private void setResponse() {
        cashwithdrawalViewModel.getWithapihitresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    Type type = new TypeToken<withdrawpaymentsendpojo>() {
                    }.getType();
                    withdrawpaymentsendpojo pojo = new GsonBuilder().create().fromJson(jsonObj.toString(), type);
                    if (pojo.getStatus().equalsIgnoreCase("1")) {
                        Intent i = new Intent(CashwithdrawalConstrain.this, CashwithdrawsuccessConstrain.class);
                        i.putExtra("json", jsonObj.toString());
                        startActivity(i);
                        finish();
                    } else {
                        String Sresponse = jsonObj.getString("response");
                        appUtils.AlertError(CashwithdrawalConstrain.this, getkey("action_error"), Sresponse);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);
        appUtils = AppUtils.getInstance(this);
        if (getIntent().hasExtra("transfer_amount")) {
            transfer_amount = getIntent().getStringExtra("transfer_amount");
            service_amount = getIntent().getStringExtra("service_amount");
            name = getIntent().getStringExtra("name");
            phone = getIntent().getStringExtra("phone");
            image = getIntent().getStringExtra("image");
            currency = getIntent().getStringExtra("currency");
            paymentcode = getIntent().getStringExtra("paymentcode");
            binding.name.setText(name + "\n" + phone);
            binding.withdrawname.setText(getkey("witthdraw_amt")+ "\n" +getkey("service_fee") + "\n" + getkey("recive_amt"));
            AppUtils.setImageviewwithoutcropplaceholder(CashwithdrawalConstrain.this, image, binding.image);
            try {
                long minusamount = Long.parseLong(transfer_amount) - Long.parseLong(service_amount);

                binding.withdrawnamevalue.setText(currency + " " + transfer_amount + "\n" + currency + " " + service_amount + "\n" + currency + " " + String.valueOf(minusamount));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    private void clicklistener() {
        binding.help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CashOutHelpDialog();
            }
        });

        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!cashwithdrawid.equalsIgnoreCase("")) {
                    Intent inten = new Intent(CashwithdrawalConstrain.this, Pin_activity.class);
                    inten.putExtra("mode", "cashwithdrawal");
                    startActivity(inten);
                }else{
                    Alert(getkey("action_error"),getkey("chooselocationforcashwithdraw"));
                }
            }
        });

        binding.lookupcash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inten = new Intent(CashwithdrawalConstrain.this, CashwithdrawlocationchooseConstrian.class);
                startActivity(inten);
            }
        });
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(CashwithdrawalConstrain.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_yes"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent inten = new Intent(CashwithdrawalConstrain.this, CashwithdrawlocationchooseConstrian.class);
                startActivity(inten);
            }
        });
        mDialog.setNegativeButton(getkey("action_no"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", sDriverID);
                jsonParams.put("amount", transfer_amount);
                jsonParams.put("mode", paymentcode);
                jsonParams.put("center_id", "");
                cashwithdrawalViewModel.cashwithdrawapihit(Iconstant.wallet_withdraw_amount_url, jsonParams);
            }
        });
        mDialog.show();
    }

    @Subscribe
    public void getcashcenterid(lookupcashpojo pojo){
        cashwithdrawid = pojo.getCashcenterid();
        appUtils.AlertSuccess(CashwithdrawalConstrain.this, getkey("action_success"), getkey("locationchoosed"));
    }

    private void CashOutHelpDialog() {
        final Dialog driveNowDialog = new Dialog(CashwithdrawalConstrain.this, R.style.SlideUpDialog);
        driveNowDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        driveNowDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        driveNowDialog.setCancelable(true);
        driveNowDialog.setCanceledOnTouchOutside(true);
        driveNowDialog.setContentView(R.layout.cashout_help_dialog);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.90);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.90);//fill only 85% of the screen
        driveNowDialog.getWindow().setLayout(screenWidth, screenHeight);
        final TextView cancel = driveNowDialog.findViewById(R.id.close);
        cancel.setText(getkey("close_lable"));

        final TextView cashoutmywallet = driveNowDialog.findViewById(R.id.cashoutmywallet);
        cashoutmywallet.setText(getkey("cashout"));
        final TextView cashoutmywalletcontent = driveNowDialog.findViewById(R.id.cashoutmywalletcontent);
        cashoutmywalletcontent.setText(getkey("cashoutcontent"));
        final TextView overseastitle = driveNowDialog.findViewById(R.id.overseastitle);
        overseastitle.setText(getkey("overseastitle"));
        final TextView overseascontent = driveNowDialog.findViewById(R.id.overseascontent);
        overseascontent.setText(getkey("overseascontent"));


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                driveNowDialog.dismiss();
            }
        });
        driveNowDialog.show();
    }

    @Subscribe
    public void caswithdrawpincheck(cashwithdrawpincheck pojo) {
        if (pojo.isIspincheck()) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", sDriverID);
            jsonParams.put("amount", transfer_amount);
            jsonParams.put("mode", paymentcode);
            jsonParams.put("center_id", cashwithdrawid);
            cashwithdrawalViewModel.cashwithdrawapihit(Iconstant.wallet_withdraw_amount_url, jsonParams);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
