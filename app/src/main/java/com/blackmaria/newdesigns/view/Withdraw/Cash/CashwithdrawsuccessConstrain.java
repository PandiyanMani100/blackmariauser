package com.blackmaria.newdesigns.view.Withdraw.Cash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import com.blackmaria.newdesigns.factory.Withdraw.Cash.CashwithdrawasuccesslFactory;
import com.blackmaria.newdesigns.viewmodel.withdraw.cash.CashwithdrawsuccessViewModel;
import com.blackmaria.pojo.withdrawpaymentsendpojo;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.DowloadPdfActivity;
import com.blackmaria.databinding.ActivityCashwithdrawsuccessConstrainBinding;
import com.blackmaria.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;

public class CashwithdrawsuccessConstrain extends AppCompatActivity implements DowloadPdfActivity.downloadCompleted {

    private ActivityCashwithdrawsuccessConstrainBinding binding;
    private CashwithdrawsuccessViewModel cashwithdrawsuccessViewModel;
    private withdrawpaymentsendpojo pojo;
    private AppUtils appUtils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(CashwithdrawsuccessConstrain.this, R.layout.activity_cashwithdrawsuccess_constrain);
        cashwithdrawsuccessViewModel = ViewModelProviders.of(this, new CashwithdrawasuccesslFactory(this)).get(CashwithdrawsuccessViewModel.class);
        binding.setCashwithdrawsuccessViewModel(cashwithdrawsuccessViewModel);
        cashwithdrawsuccessViewModel.setIds(binding);

        initView();
    }

    private void initView() {
        appUtils = AppUtils.getInstance(this);
        if (getIntent().hasExtra("json")) {
            try {
                Type type = new TypeToken<withdrawpaymentsendpojo>() {
                }.getType();
                pojo = new GsonBuilder().create().fromJson(getIntent().getStringExtra("json").toString(), type);
                setText(pojo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setText(withdrawpaymentsendpojo pojo) {
        binding.name.setText(getResources().getString(R.string.pay_cash) + "\n" +  getResources().getString(R.string.from) + pojo.getFrom() + "\n" + getResources().getString(R.string.created) + pojo.getDate() + "\n" + getResources().getString(R.string.time)+" : " + pojo.getTime() + "\n" + pojo.getCountry_code() + "" + pojo.getPhone_number());
        binding.transactionumber.setText(getResources().getString(R.string.transaction_num) + pojo.getRef_no());
        binding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DowloadPdfActivity(CashwithdrawsuccessConstrain.this, binding.card, "Cashwithdraw", CashwithdrawsuccessConstrain.this);
            }
        });
        binding.refund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUtils.AlertError(CashwithdrawsuccessConstrain.this,getResources().getString(R.string.action_error),getResources().getString(R.string.work_in_progress));
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void OnDownloadComplete(String fileName) {

        ViewPdfAlert("", getResources().getString(R.string.sucessss), fileName);
    }

    private void ViewPdfAlert(String title, String alert, final String fileName) {

        final PkDialog mDialog = new PkDialog(CashwithdrawsuccessConstrain.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.open), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPDF(fileName);
                mDialog.dismiss();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    public void viewPDF(String pdfFileName) {
        try {
            File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Signature/" + pdfFileName);  // -> filename = maven.pdf
            Uri uri_path = Uri.fromFile(pdfFile);
            Uri uri = uri_path;

            try {
                File file = null;
                String path = uri.toString();// "/mnt/sdcard/FileName.mp3"
                try {
                    file = new File(new URI(path));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                final Intent intent = new Intent(Intent.ACTION_VIEW)//
                        .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                        androidx.core.content.FileProvider.getUriForFile(CashwithdrawsuccessConstrain.this, getPackageName() + ".provider", file) : Uri.fromFile(file),
                                "application/pdf").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(CashwithdrawsuccessConstrain.this,  getResources().getString(R.string.no_pdf), Toast.LENGTH_SHORT).show();
                }

            } catch (ActivityNotFoundException e) {
                Toast.makeText(CashwithdrawsuccessConstrain.this, getResources().getString(R.string.no_read_file), Toast.LENGTH_SHORT).show();
            }

        } catch (NullPointerException e) {

        } catch (Exception ew) {
        }
    }
}
