package com.blackmaria.newdesigns.view.Withdraw.Paypal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.factory.Withdraw.Paypal.WithdrawpaypalidenterFactory;
import com.blackmaria.newdesigns.viewmodel.withdraw.paypal.PaypalwithdrawidenterViewModel;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.databinding.ActivityPaypalwithdrawidenterconstrainBinding;


public class Paypalwithdrawidenterconstrain extends AppCompatActivity {

    private ActivityPaypalwithdrawidenterconstrainBinding binding;
    private PaypalwithdrawidenterViewModel paypalwithdrawidenterViewModel;
    private AppUtils appUtils;
    private String transfer_amount = "", paypalcode = "",currency="";
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(Paypalwithdrawidenterconstrain.this, R.layout.activity_paypalwithdrawidenterconstrain);
        paypalwithdrawidenterViewModel = ViewModelProviders.of(this, new WithdrawpaypalidenterFactory(this)).get(PaypalwithdrawidenterViewModel.class);
        binding.setPaypalwithdrawidenterViewModel(paypalwithdrawidenterViewModel);
        paypalwithdrawidenterViewModel.setIds(binding);
        mhelper  = new LanguageDb(this);
        binding.withdraw.setText(getkey("withdraw_lable"));
        binding.paypalid.setHint(getkey("paypalidenter"));
        binding.transactionumber.setText(getkey("withdrawcontent"));
        binding.confirm.setText(getkey("confirm_lable"));
        initView();



        clicklistener();
    }

    private void initView() {
        appUtils = AppUtils.getInstance(this);
        if (getIntent().hasExtra("transfer_amount")) {
            transfer_amount = getIntent().getStringExtra("transfer_amount");
            paypalcode = getIntent().getStringExtra("paypalcode");
            currency = getIntent().getStringExtra("currency");
        }
    }

    private void clicklistener() {
        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.paypalid.getText().toString().length() == 0) {
                    appUtils.AlertError(Paypalwithdrawidenterconstrain.this, getkey("action_error"), getkey("paypalidnotemepty"));
                } else if (!AppUtils.isValidEmail(binding.paypalid.getText().toString().trim().replace(" ", ""))) {
                    appUtils.AlertError(Paypalwithdrawidenterconstrain.this, getkey("action_error"),getkey("paypalinvalidid"));
                } else {
                    Intent i = new Intent(Paypalwithdrawidenterconstrain.this, Paypalamountconfirm.class);
                    i.putExtra("transfer_amount", transfer_amount);
                    i.putExtra("paypalcode", paypalcode);
                    i.putExtra("paypalid", binding.paypalid.getText().toString());
                    i.putExtra("currency", currency);
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
