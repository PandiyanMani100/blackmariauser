package com.blackmaria.newdesigns.saveplus;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.newdesigns.fastwallet.Fastpaypincheck;
import com.blackmaria.R;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.widgets.PkDialog;
import com.bumptech.glide.Glide;

public class Saveplus_sender_sendmoney extends AppCompatActivity {

    private RoundedImageView iv_profile;
    private ImageView close;
    private TextView tv_name, tv_saveplusid, tv_ok;
    private EditText tv_enteryourprice, tv_entermoney;

    private String phone_number = "", country_code = "", received_amount = "", total_amount = "", finalmoney = "", name = "", saveplusid = "", image = "", ismobilenovisible = "", receiver_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saveplus_sender_sendmoney);


        init();
    }

    private void init() {
        close = findViewById(R.id.close);
        iv_profile = findViewById(R.id.iv_profile);
        tv_name = findViewById(R.id.tv_name);
        tv_saveplusid = findViewById(R.id.tv_saveplusid);
        tv_ok = findViewById(R.id.tv_ok);
        tv_enteryourprice = findViewById(R.id.tv_enteryourprice);
        tv_entermoney = findViewById(R.id.tv_entermoney);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (getIntent().hasExtra("ismobilenovisible")) {
            String ismobilenovisible = getIntent().getStringExtra("ismobilenovisible");

            if (ismobilenovisible.equalsIgnoreCase("1")) {
                phone_number = getIntent().getStringExtra("phone_number");
                country_code = getIntent().getStringExtra("country_code");
                received_amount = getIntent().getStringExtra("received_amount");
                total_amount = getIntent().getStringExtra("total_amount");
                finalmoney = getIntent().getStringExtra("finalmoney");
                name = getIntent().getStringExtra("name");
                image = getIntent().getStringExtra("image");
                saveplusid = getIntent().getStringExtra("saveplusid");
                receiver_id = getIntent().getStringExtra("receiver_id");
            } else {
                receiver_id = getIntent().getStringExtra("receiver_id");
                received_amount = getIntent().getStringExtra("received_amount");
                total_amount = getIntent().getStringExtra("total_amount");
                finalmoney = getIntent().getStringExtra("finalmoney");
                name = getIntent().getStringExtra("name");
                image = getIntent().getStringExtra("image");
            }
        }

        if (getIntent().hasExtra("fromscanqrcode")) {
            receiver_id = getIntent().getStringExtra("receiver_id");
            name = getIntent().getStringExtra("name");
            image = getIntent().getStringExtra("image");
            saveplusid = getIntent().getStringExtra("saveplusid");
        }

        Glide.with(this).load(image).into(iv_profile);
        tv_name.setText(name);
        tv_saveplusid.setText(getResources().getString(R.string.saveplus_id) + saveplusid);
        tv_entermoney.setText(received_amount);
        tv_enteryourprice.setText(total_amount);


        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv_entermoney.getText().toString().length() == 0) {
                    Alert(getResources().getString(R.string.action_error),  getResources().getString(R.string.recivedcntepmty));
                } else if (tv_entermoney.getText().toString().equals("0")) {
                    Alert(getResources().getString(R.string.action_error), "Received amount is not valid");
                } else if (tv_enteryourprice.getText().toString().length() == 0) {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.price_amount_empty));
                } else if (tv_enteryourprice.getText().toString().equals("0")) {
                    Alert(getResources().getString(R.string.action_error), "Your price amount is not valid");
                } else {
                    int totalmoney = Integer.parseInt(tv_entermoney.getText().toString().trim());
                    int yourmoney = Integer.parseInt(tv_enteryourprice.getText().toString().trim());
                    int finalmoney = totalmoney - yourmoney;
                    if (yourmoney < totalmoney) {
                        Intent i = new Intent(Saveplus_sender_sendmoney.this, Fastpaypincheck.class);
                        i.putExtra("receiver_id", receiver_id);
                        i.putExtra("received_amount", tv_enteryourprice.getText().toString().trim());
                        i.putExtra("total_amount", tv_entermoney.getText().toString().trim());
                        i.putExtra("finalmoney", String.valueOf(finalmoney));
                        startActivity(i);
                    } else {
                        Alert(getResources().getString(R.string.action_error), "Received amount shoud be greater than the price amount");
                    }

                }
            }
        });
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Saveplus_sender_sendmoney.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onBackPressed() {

    }
}
