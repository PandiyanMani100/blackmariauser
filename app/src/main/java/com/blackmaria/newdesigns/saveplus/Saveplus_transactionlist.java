package com.blackmaria.newdesigns.saveplus;

import android.app.Dialog;
import android.app.DownloadManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.adapter.Saveplus_transaction_adapter;
import com.blackmaria.pojo.Trans;
import com.blackmaria.pojo.Transactionlistsaveplus;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class Saveplus_transactionlist extends AppCompatActivity {

    private ImageView back;
    private TextView tv_month, tv_download, notransaction;
    private RecyclerView recycler_view;
    private Saveplus_transaction_adapter contactAdapter;
    private Transactionlistsaveplus transactoinpojo;
    private SessionManager sessionManager;
    private HashMap<String, String> info;
    private String UserID = "";
    private int pageno = 1;
    LanguageDb mhelper;
    private ArrayList<Trans> trans = new ArrayList<>();
    public static final String downloadDirectory = "Blackmaria_Saveplustransaction";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saveplus_transactionlist);
        mhelper= new LanguageDb(this);
        init();
    }

    private void init() {
        transactoinpojo = new Transactionlistsaveplus();
        sessionManager = new SessionManager(this);
        recycler_view = findViewById(R.id.recycler_view);
        notransaction = findViewById(R.id.notransaction);
        notransaction.setText(mhelper.getvalueforkey("NostatementFounts"));

        TextView tv_date = findViewById(R.id.tv_date);
        tv_date.setText(mhelper.getvalueforkey("date"));
        TextView tv_from = findViewById(R.id.tv_from);
        tv_from.setText(mhelper.getvalueforkey("from").replace(":",""));
        TextView tv_type = findViewById(R.id.tv_type);
        tv_type.setText(mhelper.getvalueforkey("type"));
        TextView tv_amount = findViewById(R.id.tv_amount);
        tv_amount.setText(mhelper.getvalueforkey("amount"));


        //find view by id and attaching adapter for the RecyclerVie
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setHasFixedSize(true);
        contactAdapter = new Saveplus_transaction_adapter(Saveplus_transactionlist.this, trans);
        recycler_view.setAdapter(contactAdapter);


        info = sessionManager.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        back = findViewById(R.id.back);
        tv_month = findViewById(R.id.tv_month);
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        String month_name = month_date.format(cal.getTime());
        int year = Calendar.getInstance().get(Calendar.YEAR);

        tv_month.setText(month_name + " " + year);
        tv_download = findViewById(R.id.tv_download);
        tv_download.setText(mhelper.getvalueforkey("downloadstatements"));


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        tv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadsaveplustransaction();
            }
        });

        loadfirstdata(Iconstant.saveplustransfer_list);
    }

    private void loadfirstdata(String Url) {
        final Dialog dialog = new Dialog(Saveplus_transactionlist.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("type", "");
        jsonParams.put("month", String.valueOf(month));
        jsonParams.put("year", String.valueOf(year));
        jsonParams.put("page", String.valueOf(pageno));
        jsonParams.put("perPage", "10");

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(Saveplus_transactionlist.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);

                dialog.dismiss();
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        Object check_object = object.getJSONObject("response").get("trans");
                        if (check_object instanceof JSONArray) {
                            Type listType = new TypeToken<Transactionlistsaveplus>() {
                            }.getType();
                            transactoinpojo = new GsonBuilder().create().fromJson(object.toString(), listType);
                            notransaction.setVisibility(View.GONE);
                            recycler_view.setVisibility(View.VISIBLE);
                            tv_download.setVisibility(View.VISIBLE);
                            contactAdapter = new Saveplus_transaction_adapter(Saveplus_transactionlist.this, transactoinpojo.getResponse().getTrans());
                            recycler_view.setAdapter(contactAdapter);
                            contactAdapter.notifyDataSetChanged();
                        }else{
                            notransaction.setVisibility(View.VISIBLE);
                            tv_download.setVisibility(View.GONE);
                        }

//                        contactAdapter.setLoaded();

//                        if (!transactoinpojo.getResponse().getNext_page().equalsIgnoreCase("")) {
//                            pageno = Integer.parseInt(transactoinpojo.getResponse().getNext_page());
//                        }
                    } else {
                        Smessage = object.getString("response");
                        Alert(getResources().getString(R.string.action_error), Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Saveplus_transactionlist.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void downloadsaveplustransaction() {
        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        Uri Download_Uri = Uri.parse(transactoinpojo.getResponse().getDownload_url());
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

        //Restrict the types of networks over which this download may proceed.
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        //Set whether this download may proceed over a roaming connection.
        request.setAllowedOverRoaming(false);
        //Set the title of this download, to be displayed in notifications (if enabled).
        request.setTitle("Blackmaria Saveplus Transaction");
        //Set a description of this download, to be displayed in notifications (if enabled)
        request.setDescription("Downloading the PDF");
        //Set the local destination for the downloaded file to a path within the application's external files directory
//                    request.setDestinationInExternalFilesDir(DriverVerification.this,Environment.DIRECTORY_DOWNLOADS,"CountryList.json");
        File DownloadFolder = new File(Environment.getExternalStorageDirectory(), downloadDirectory);
//
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
        //If File is not present create directory
        if (!DownloadFolder.exists()) {
            if (DownloadFolder.mkdir()) {
                Log.e("DriverVerification", "Directory Created.");
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "BlackmariaSaveplusTransaction_" + new Date().getTime() + ".pdf");
            } else {
                Log.e("DriverVerification", "Directory Not Created.");
            }

        } else {
            Log.e("DriverVerification", "Directory Created.");
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "BlackmariaSaveplusTransaction_" + new Date().getTime() + ".pdf");
        }

        Toast.makeText(Saveplus_transactionlist.this, "PDF will be download in " + DownloadFolder.getAbsolutePath(), Toast.LENGTH_SHORT).show();


        //Enqueue a new download and same the referenceId
        long downloadReference = downloadManager.enqueue(request);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
