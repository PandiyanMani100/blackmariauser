package com.blackmaria.newdesigns.saveplus;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;


import com.android.volley.Request;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.fastwallet.Fastpayhome;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.itsxtt.patternlock.PatternLockView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class SecondConfirmpatern_activity extends AppCompatActivity {

    private PatternLockView patternLockView;
    private ImageView iv_back;
    private TextView confirm;
    private ArrayList<Integer> ids_confirm= new ArrayList<Integer>();

    private SessionManager session;
    LanguageDb mhelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondconfirmactvity);
        mhelper = new LanguageDb(this);
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        ids_confirm  = (ArrayList<Integer>) args.getSerializable("ARRAYLIST");

        session = new SessionManager(this);
        patternLockView = findViewById(R.id.patternLockView);
        iv_back = findViewById(R.id.booking_back_imgeview);
        confirm = findViewById(R.id.confirm);
        confirm.setText(getkey("confirm_lable"));

        TextView pickup_labless= findViewById(R.id.pickup_labless);
        pickup_labless.setText(getkey("re_enter_your_pattern"));


        patternLockView.setOnPatternListener(new PatternLockView.OnPatternListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(ArrayList<Integer> ids) {

                System.out.println("progress........... " + ids);
            }

            @Override
            public boolean onComplete(ArrayList<Integer> ids) {
                /*
                 * A return value required
                 * if the pattern is not correct and you'd like change the pattern to error state, return false
                 * otherwise return true
                 */
                System.out.println("completed........... " + ids);
                return isPatternCorrect(ids);
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatepattern(Iconstant.updatepattern, ids_confirm.toString());
            }
        });


    }



    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(SecondConfirmpatern_activity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), Fastpayhome.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        mDialog.show();
    }


    private boolean isPatternCorrect(ArrayList<Integer> ids) {
        boolean value = false;
        if (equalLists(ids, ids_confirm))
        {

            confirm.setVisibility(View.VISIBLE);
            value = true;
        }

        return value;
    }


    public boolean equalLists(ArrayList<Integer> one, ArrayList<Integer> two) {
        if (one == null && two == null) {
            return true;
        }

        if ((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()) {
            return false;
        }

        //to avoid messing the order of the lists we will use a copy
        //as noted in comments by A. R. S.
        one = new ArrayList<Integer>(one);
        two = new ArrayList<Integer>(two);

        Collections.sort(one);
        Collections.sort(two);
        return one.equals(two);
    }


    private void updatepattern(String Url, String pattern) {

        final Dialog dialog = new Dialog(SecondConfirmpatern_activity.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> info = session.getUserDetails();
        String UserID = info.get(SessionManager.KEY_USERID);

        String  patern= pattern.replace(",","");
        String paterns = patern.replace("[","");
        String paternse = paterns.replace("]","");

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("pattern_code", paternse.trim());


        ServiceRequest mRequest = new ServiceRequest(SecondConfirmpatern_activity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        Alert(getkey("action_success"), Smessage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
