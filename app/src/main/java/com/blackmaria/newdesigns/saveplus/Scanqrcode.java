package com.blackmaria.newdesigns.saveplus;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.fastwallet.Fastpay_afterscan;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Scanqrcode extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private TextView resultTextView;
    private QRCodeReaderView qrCodeReaderView;
    private String UserID = "";
    private SessionManager sessionManager;
    LanguageDb mhelper;
    private HashMap<String, String> info;
    private ImageView close, light;
    private ObjectAnimator objectanimator1;
    private View view;
    private boolean istop = false;
    private Animation slide;
    private int height;

    private TranslateAnimation mAnimation;
    private boolean isfromfatstpay = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanqrcode);
        mhelper = new LanguageDb(this);

        sessionManager = new SessionManager(this);
        info = sessionManager.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        qrCodeReaderView = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
        close = findViewById(R.id.close);
        view = findViewById(R.id.view);
        light = findViewById(R.id.light);

        if (getIntent().hasExtra("fromfastpay")) {
            isfromfatstpay = true;
        }

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        height = size.y;


        qrCodeReaderView.setOnQRCodeReadListener(this);

        // Use this function to enable/disable decoding
        qrCodeReaderView.setQRDecodingEnabled(true);

        // Use this function to change the autofocus interval (default is 5 secs)
        qrCodeReaderView.setAutofocusInterval(2000L);

        // Use this function to enable/disable Torch
        qrCodeReaderView.setTorchEnabled(true);

        // Use this function to set front camera preview
        qrCodeReaderView.setFrontCamera();

        // Use this function to set back camera preview
        qrCodeReaderView.setBackCamera();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    mAnimation = new TranslateAnimation(
                            TranslateAnimation.ABSOLUTE, 0f,
                            TranslateAnimation.ABSOLUTE, 0f,
                            TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                            TranslateAnimation.RELATIVE_TO_PARENT, 1.0f);
                    mAnimation.setDuration(5000);
                    mAnimation.setRepeatCount(-1);
                    mAnimation.setRepeatMode(Animation.REVERSE);
                    mAnimation.setInterpolator(new LinearInterpolator());
                    view.setAnimation(mAnimation);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();



    }


    @Override
    public void onQRCodeRead(String text, PointF[] points) {
//        resultTextView.setText(text);
        System.out.println(".........     " + text);

        if (!text.equalsIgnoreCase("")) {
            qrCodeReaderView.stopCamera();
            if (isfromfatstpay) {
                findaccountfastpay(Iconstant.fastpay_findfriend, text);
            } else {
                findaccount(Iconstant.findaccount, text);
            }

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        qrCodeReaderView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }

    private void findaccount(String Url, final String receiverid) {
        final Dialog dialog = new Dialog(Scanqrcode.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("receiver_id", receiverid);
        jsonParams.put("id", UserID);

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(Scanqrcode.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";

                dialog.dismiss();
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        String name = object.getJSONObject("response").getString("name");
                        String image = object.getJSONObject("response").getString("image");
                        String id = "";
                        if (object.getJSONObject("response").has("saveplus_id")) {
                            id = object.getJSONObject("response").getString("saveplus_id");
                        }

                        Intent i = new Intent(Scanqrcode.this, Saveplus_sender_sendmoney.class);
                        i.putExtra("fromscanqrcode", "");
                        i.putExtra("receiver_id", receiverid);
                        i.putExtra("received_amount", "");
                        i.putExtra("total_amount", "");
                        i.putExtra("finalmoney", "");
                        i.putExtra("name", name);
                        i.putExtra("image", image);
                        i.putExtra("saveplusid", id);
                        startActivity(i);
                        finish();

                    } else {
                        Smessage = object.getString("response");
                        Alert(getkey("action_error"), Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void findaccountfastpay(String Url, final String receiverid) {
        final Dialog dialog = new Dialog(Scanqrcode.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("receiver_id", receiverid);
        jsonParams.put("user_id", UserID);

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(Scanqrcode.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";

                dialog.dismiss();
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Intent sendFastpay_afterscan = new Intent(Scanqrcode.this, Fastpay_afterscan.class);
                        sendFastpay_afterscan.putExtra("json", object.toString());
                        startActivity(sendFastpay_afterscan);
                        finish();
//                        String name = object.getJSONObject("response").getString("name");
//                        String image = object.getJSONObject("response").getString("image");
//                        String id = "";
//                        if (object.getJSONObject("response").has("saveplus_id")) {
//                            id = object.getJSONObject("response").getString("saveplus_id");
//                        }
//
//                        Intent i = new Intent(Scanqrcode.this, Saveplus_sender_sendmoney.class);
//                        i.putExtra("fromscanqrcode", "");
//                        i.putExtra("receiver_id", receiverid);
//                        i.putExtra("received_amount", "");
//                        i.putExtra("total_amount", "");
//                        i.putExtra("finalmoney", "");
//                        i.putExtra("name", name);
//                        i.putExtra("image", image);
//                        i.putExtra("saveplusid", id);
//                        startActivity(i);
//                        finish();
                    } else {
                        Smessage = object.getString("response");
                        Alert(getkey("action_error"), Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Scanqrcode.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                qrCodeReaderView.startCamera();
            }
        });
        mDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
