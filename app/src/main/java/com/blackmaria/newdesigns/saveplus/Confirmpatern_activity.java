package com.blackmaria.newdesigns.saveplus;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.blackmaria.LanguageDb;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.itsxtt.patternlock.PatternLockView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Confirmpatern_activity extends AppCompatActivity {

    private PatternLockView patternLockView;
    private ImageView iv_back;
    private TextView confirm,pickup_lable;
    private ArrayList<Integer> ids_confirm= new ArrayList<Integer>();
    private ArrayList<Integer> retry_confirm = new ArrayList<Integer>();
    private SessionManager session;
    RelativeLayout secondstep,firsststep;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmpatern_activity);
        mhelper = new LanguageDb(this);
        session = new SessionManager(this);
        patternLockView = findViewById(R.id.patternLockView);
        iv_back = findViewById(R.id.booking_back_imgeview);
        confirm = findViewById(R.id.confirm);
        confirm.setText(getkey("confirm_lable"));
        secondstep = findViewById(R.id.secondstep);
        pickup_lable= findViewById(R.id.pickup_lable);
        firsststep = findViewById(R.id.firsststep);
        TextView pickup_labless= findViewById(R.id.pickup_labless);
        pickup_labless.setText(getkey("re_enter_your_pattern"));


        firsststep.setVisibility(View.VISIBLE);
        secondstep.setVisibility(View.GONE);

        pickup_lable.setText(getkey("setup_your_security_patern_n_in_four_steps"));

        patternLockView.setOnPatternListener(new PatternLockView.OnPatternListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(ArrayList<Integer> ids) {

                System.out.println("progress........... " + ids);
            }

            @Override
            public boolean onComplete(ArrayList<Integer> ids) {
                /*
                 * A return value required
                 * if the pattern is not correct and you'd like change the pattern to error state, return false
                 * otherwise return true
                 */
                System.out.println("completed........... " + ids);
                return isPatternCorrect(ids);
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(retry_confirm.size() == 0)
                {
                    Intent i = new Intent(Confirmpatern_activity.this, SecondConfirmpatern_activity.class);
                    Bundle args = new Bundle();
                    args.putSerializable("ARRAYLIST",(Serializable)ids_confirm);
                    i.putExtra("BUNDLE",args);
                    startActivity(i);
                    finish();
                }


            }
        });


    }






    private boolean isPatternCorrect(ArrayList<Integer> ids) {
        boolean value = false;
        if(ids_confirm.size() == 0)
        {
            value = true;
            ids_confirm = ids;
            confirm.setVisibility(View.VISIBLE);
        }
        if(ids_confirm.size() != 0 && firsststep.getVisibility() == View.VISIBLE)
        {
            value = true;
            ids_confirm = ids;
            confirm.setVisibility(View.VISIBLE);
        }
        else if (equalLists(ids, ids_confirm))
        {
            retry_confirm= ids;
            confirm.setVisibility(View.VISIBLE);
            value = true;
        }
        else
        {
            confirm.setVisibility(View.GONE);
        }


        return value;
    }


    public boolean equalLists(ArrayList<Integer> one, ArrayList<Integer> two) {
        if (one == null && two == null) {
            return true;
        }

        if ((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()) {
            return false;
        }

        //to avoid messing the order of the lists we will use a copy
        //as noted in comments by A. R. S.
        one = new ArrayList<Integer>(one);
        two = new ArrayList<Integer>(two);

        Collections.sort(one);
        Collections.sort(two);
        return one.equals(two);
    }


    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
