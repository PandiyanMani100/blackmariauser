package com.blackmaria.newdesigns.saveplus;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.R;
import com.blackmaria.utils.CountryDialCode;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Saveplus_manual extends AppCompatActivity {

    private ImageView close, changeinput;
    private EditText tv_enteryourprice, tv_entermoney, tv_manualsaveplusid, tv_manualsaveplusid_number;
    private TextView tv_ok, ivsaveplsumanual_number;
    private SessionManager sessionManager;
    private HashMap<String, String> info;
    private String UserID = "";
    private ConstraintLayout manualsaveplus_number, manualsaveplus;
    private boolean ismobilenovisible = false;
    private CountryPicker picker;
    private GPSTracker gpsTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saveplus_manual);

        init();
    }

    private void init() {
        sessionManager = new SessionManager(this);
        changeinput = findViewById(R.id.changeinput);
        close = findViewById(R.id.close);
        manualsaveplus_number = findViewById(R.id.manualsaveplus_number);
        manualsaveplus = findViewById(R.id.manualsaveplus);
        tv_manualsaveplusid = findViewById(R.id.tv_manualsaveplusid);
        tv_enteryourprice = findViewById(R.id.tv_enteryourprice);
        tv_entermoney = findViewById(R.id.tv_entermoney);
        ivsaveplsumanual_number = findViewById(R.id.ivsaveplsumanual_number);
        tv_manualsaveplusid_number = findViewById(R.id.tv_manualsaveplusid_number);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_lable));
        tv_ok = findViewById(R.id.tv_ok);
        info = sessionManager.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);


        gpsTracker = new GPSTracker(Saveplus_manual.this);
        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {

            double MyCurrent_lat = gpsTracker.getLatitude();
            double MyCurrent_long = gpsTracker.getLongitude();


            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    String code = "IND";
                    String Str_getCountryCode = addresses.get(0).getCountryCode();
                    if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode);
                        ivsaveplsumanual_number.setText(Str_countyCode);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        changeinput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ismobilenovisible) {
                    ismobilenovisible = false;
                    manualsaveplus.setVisibility(View.VISIBLE);
                    manualsaveplus_number.setVisibility(View.GONE);
                } else {
                    ismobilenovisible = true;
                    manualsaveplus.setVisibility(View.GONE);
                    manualsaveplus_number.setVisibility(View.VISIBLE);
                }
            }
        });
        ivsaveplsumanual_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                ivsaveplsumanual_number.setText(dialCode);
                CloseKeyBoard();
            }
        });

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ismobilenovisible) {
                    if (tv_manualsaveplusid_number.getText().toString().length() == 0) {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.mobileshotnotempty));
                    } else if (tv_entermoney.getText().toString().length() == 0) {
                        Alert(getResources().getString(R.string.action_error),  getResources().getString(R.string.recivedcntepmty));
                    } else if (tv_entermoney.getText().toString().equals("0")) {
                        Alert(getResources().getString(R.string.action_error),  getResources().getString(R.string.recived_inavlid_amount));
                    } else if (tv_enteryourprice.getText().toString().length() == 0) {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.price_amount_empty));
                    } else if (tv_enteryourprice.getText().toString().equals("0")) {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.recived_inavlid_amount));
                    } else {
                        int totalmoney = Integer.parseInt(tv_entermoney.getText().toString().trim());
                        int yourmoney = Integer.parseInt(tv_enteryourprice.getText().toString().trim());

                        if (yourmoney < totalmoney) {
                            int finalmoney = totalmoney - yourmoney;
                            findaccount(Iconstant.findaccount, finalmoney);

                        } else {
                            Alert(getResources().getString(R.string.action_error), "Received amount shoud be greater than the price amount");
                        }
                    }
                } else {
                    if (tv_manualsaveplusid.getText().toString().length() == 0) {
                        Alert(getResources().getString(R.string.action_error), "SavePlus ID should not be empty");
                    } else if (tv_entermoney.getText().toString().length() == 0) {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.recivedcntepmty));
                    } else if (tv_entermoney.getText().toString().equals("0")) {
                        Alert(getResources().getString(R.string.action_error), "Received amount is not valid");
                    } else if (tv_enteryourprice.getText().toString().length() == 0) {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.price_amount_empty));
                    } else if (tv_enteryourprice.getText().toString().equals("0")) {
                        Alert(getResources().getString(R.string.action_error), "Your price amount is not valid");
                    } else {
                        int totalmoney = Integer.parseInt(tv_entermoney.getText().toString().trim());
                        int yourmoney = Integer.parseInt(tv_enteryourprice.getText().toString().trim());

                        if (yourmoney < totalmoney) {
                            int finalmoney = totalmoney - yourmoney;

                            findaccount(Iconstant.findaccount, finalmoney);

                        } else {
                            Alert(getResources().getString(R.string.action_error), "Received amount shoud be greater than the price amount");
                        }
                    }
                }
            }
        });
    }

    private void findaccount(String Url, final int finalmoney) {

        final Dialog dialog = new Dialog(Saveplus_manual.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        if (ismobilenovisible) {
            jsonParams.put("country_code", ivsaveplsumanual_number.getText().toString());
            jsonParams.put("phone_number", tv_manualsaveplusid_number.getText().toString().trim());
            jsonParams.put("id", UserID);
        } else {
            jsonParams.put("receiver_id", tv_manualsaveplusid.getText().toString().trim());
            jsonParams.put("id", UserID);
        }


        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(Saveplus_manual.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";

                dialog.dismiss();
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        String name = object.getJSONObject("response").getString("name");
                        String image = object.getJSONObject("response").getString("image");
                        String id = "", saveplusid = "";
                        if (object.getJSONObject("response").has("saveplus_id")) {
                            saveplusid = object.getJSONObject("response").getString("saveplus_id");
                            id = object.getJSONObject("response").getString("id");
                        }

                        if (ismobilenovisible) {
                            Intent i = new Intent(Saveplus_manual.this, Saveplus_sender_sendmoney.class);
                            i.putExtra("phone_number", tv_manualsaveplusid_number.getText().toString().trim());
                            i.putExtra("country_code", ivsaveplsumanual_number.getText().toString());
                            i.putExtra("received_amount", tv_entermoney.getText().toString().trim());
                            i.putExtra("total_amount", tv_enteryourprice.getText().toString().trim());
                            i.putExtra("finalmoney", String.valueOf(finalmoney));
                            i.putExtra("name", name);
                            i.putExtra("receiver_id", id);
                            i.putExtra("image", image);
                            i.putExtra("saveplusid", saveplusid);
                            i.putExtra("ismobilenovisible", "1");
                            startActivity(i);
                        } else {
                            Intent i = new Intent(Saveplus_manual.this, Saveplus_sender_sendmoney.class);
                            i.putExtra("receiver_id", tv_manualsaveplusid.getText().toString().trim());
                            i.putExtra("received_amount", tv_entermoney.getText().toString().trim());
                            i.putExtra("total_amount", tv_enteryourprice.getText().toString().trim());
                            i.putExtra("finalmoney", String.valueOf(finalmoney));
                            i.putExtra("ismobilenovisible", "0");
                            i.putExtra("name", name);
                            i.putExtra("image", image);
                            startActivity(i);
                        }


                    } else {
                        Smessage = object.getString("response");
                        Alert(getResources().getString(R.string.action_error), Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Saveplus_manual.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onBackPressed() {

    }

    private void CloseKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

       /* View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }*/
    }
}
