package com.blackmaria.newdesigns.saveplus;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.Navigation_new;
import com.blackmaria.newdesigns.Pin_activitynew;
import com.blackmaria.pojo.Saveplustransfer;
import com.blackmaria.R;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.PkDialogtryagain;
import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.itsxtt.patternlock.PatternLockView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Saveplus_billsecurity extends AppCompatActivity {

    private TextView amount;
    private LinearLayout ly_forgetpin;
    private PatternLockView patternLockView;
    private ImageView booking_back_imgeview;
    private TextView confirm;
    //    private ArrayList<Integer> ids_confirm;
    private Saveplustransfer homepage;
    private String UserID = "", receiver_id = "", received_amount = "", total_amount = "", finalmoney = "";
    private SessionManager sessionManager;
    private String ids_confirm = "";
    private HashMap<String, String> info;

    private int countcheck_wrongpattern = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saveplus_billsecurity);


        init();
    }

    private void init() {
        sessionManager = new SessionManager(this);
        info = sessionManager.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        HashMap<String, String> getpattern = sessionManager.getpattern();
        String pattern = getpattern.get(SessionManager.KEY_PATTERN);
        ids_confirm = pattern;
        patternLockView = findViewById(R.id.patternLockView);
        amount = findViewById(R.id.amount);
        ly_forgetpin = findViewById(R.id.ly_forgetpin);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);
        confirm = findViewById(R.id.confirm);


        if (getIntent().hasExtra("receiver_id")) {
            receiver_id = getIntent().getStringExtra("receiver_id");
            received_amount = getIntent().getStringExtra("received_amount");
            total_amount = getIntent().getStringExtra("total_amount");
            finalmoney = getIntent().getStringExtra("finalmoney");
            String ScurrencySymbol = sessionManager.getCurrency();
            amount.setText(ScurrencySymbol + " " + finalmoney);
        }

        patternLockView.setOnPatternListener(new PatternLockView.OnPatternListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(ArrayList<Integer> ids) {

                System.out.println("progress........... " + ids);
            }

            @Override
            public boolean onComplete(ArrayList<Integer> ids) {
                /*
                 * A return value required
                 * if the pattern is not correct and you'd like change the pattern to error state, return false
                 * otherwise return true
                 */
                System.out.println("completed........... " + ids);
                return isPatternCorrect(ids);
            }
        });

        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ly_forgetpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Saveplus_billsecurity.this, Pin_activitynew.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postrequestUpdateProfile(Iconstant.saveplustransfer);
            }
        });
    }


    private boolean isPatternCorrect(ArrayList<Integer> ids) {
        boolean value = false;
        String patern = ids.toString().replace(",", "");
        String paterns = patern.replace("[", "");
        String paternse = paterns.replace("]", "");

        if (isequal(paternse, ids_confirm)) {
            value = true;
            confirm.setVisibility(View.VISIBLE);

        } else {

            if (countcheck_wrongpattern == 1) {
                countcheck_wrongpattern++;
                Alertwrongpattern(getResources().getString(R.string.action_error), getResources().getString(R.string.wrongh_pattern));
            } else if (countcheck_wrongpattern == 2) {
                countcheck_wrongpattern++;
                Alertwrongpattern(getResources().getString(R.string.action_error), getResources().getString(R.string.wrongh_pattern));
            } else if (countcheck_wrongpattern == 3) {
                countcheck_wrongpattern = 1;
                closepopupinseconds(getResources().getString(R.string.wrong_patterns),  getResources().getString(R.string.wrong_poattern_redrae));
            }
            value = false;
            confirm.setVisibility(View.INVISIBLE);
        }
        return value;
    }

    private boolean isequal(String pattern, String newpattern) {
        boolean val = false;

        if (pattern.equalsIgnoreCase(newpattern)) {
            val = true;
        }

        return val;
    }

    public boolean equalLists(ArrayList<Integer> one, ArrayList<Integer> two) {
        if (one == null && two == null) {
            return true;
        }

        if ((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()) {
            return false;
        }

        //to avoid messing the order of the lists we will use a copy
        //as noted in comments by A. R. S.
        one = new ArrayList<Integer>(one);
        two = new ArrayList<Integer>(two);

        Collections.sort(one);
        Collections.sort(two);
        return one.equals(two);
    }

    private void saveplusbill_popup(Saveplustransfer homepage) {
        final Dialog dialog = new Dialog(Saveplus_billsecurity.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.saveplusbill);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);


        final ImageView ivclose = dialog.findViewById(R.id.close);
        final TextView smallext = dialog.findViewById(R.id.smallext);
        final RoundedImageView iv_profile = dialog.findViewById(R.id.iv_profile);
        final TextView tv_name = dialog.findViewById(R.id.tv_name);
        final TextView tv_saveplusid = dialog.findViewById(R.id.tv_saveplusid);
        final TextView tv_billamount = dialog.findViewById(R.id.tv_billamount);
        final TextView tv_date = dialog.findViewById(R.id.tv_date);
        final TextView tv_yourpricevalue = dialog.findViewById(R.id.tv_yourpricevalue);
        final TextView tv_payment_value = dialog.findViewById(R.id.tv_payment_value);
        final TextView tv_moneyreceived_value = dialog.findViewById(R.id.tv_moneyreceived_value);
        final TextView tv_savepluscredit_value = dialog.findViewById(R.id.tv_savepluscredit_value);
        final TextView tv_transationno = dialog.findViewById(R.id.tv_transationno);
        final TextView amount = dialog.findViewById(R.id.amount);

        tv_date.setText(getResources().getString(R.string.dat_col) + homepage.getResponse().getTrans_date());
        tv_name.setText(homepage.getResponse().getReceiver_details().getName());
        tv_saveplusid.setText(getResources().getString(R.string.saveplus_id) + homepage.getResponse().getSaveplus_id());
        tv_billamount.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getCrdit_amount());
        tv_yourpricevalue.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal_amount());
        tv_moneyreceived_value.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getReceived_amount());
        tv_savepluscredit_value.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getCrdit_amount());
        tv_transationno.setText(getResources().getString(R.string.trasn_number) + homepage.getResponse().getTrans_id());
        amount.setText("Merchant ID : " + homepage.getResponse().getSaveplus_id());


        Glide.with(this).load(homepage.getResponse().getReceiver_details().getImage()).into(iv_profile);
        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Saveplus_billsecurity.this, Navigation_new.class);
                startActivity(i);
                dialog.dismiss();
                finish();

            }
        });

        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }


    private void postrequestUpdateProfile(String Url) {

        final Dialog dialog = new Dialog(Saveplus_billsecurity.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("receiver_id", receiver_id);
        jsonParams.put("received_amount", total_amount);
        jsonParams.put("total_amount", received_amount);
        jsonParams.put("user_id", UserID);

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(Saveplus_billsecurity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);
                homepage = new Saveplustransfer();
                dialog.dismiss();
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Type listType = new TypeToken<Saveplustransfer>() {
                        }.getType();
                        homepage = new GsonBuilder().create().fromJson(object.toString(), listType);
                        saveplusbill_popup(homepage);

                    } else {
                        Smessage = object.getString("response");
                        Alert(getResources().getString(R.string.action_error), Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Saveplus_billsecurity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void Alertwrongpattern(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Saveplus_billsecurity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getResources().getString(R.string.forget_pattern), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Saveplus_billsecurity.this, Pin_activitynew.class);
                startActivity(i);
                finish();
            }
        });
        mDialog.show();
    }

    private void closepopupinseconds(String title, final String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Saveplus_billsecurity.this);
        mDialog.setDialogTitle(title);
        mDialog.setCancelble(false);
        mDialog.setCancelOnTouchOutside(false);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mDialog.setDialogMessage(" " + millisUntilFinished / 1000 + "\n \n \n " + alert);
            }

            public void onFinish() {
                mDialog.dismiss();
            }

        }.start();


        mDialog.show();
    }

    @Override
    public void onBackPressed() {

    }
}
