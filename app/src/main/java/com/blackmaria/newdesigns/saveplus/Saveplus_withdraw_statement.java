package com.blackmaria.newdesigns.saveplus;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.WalletMoneyPage1;
import com.blackmaria.widgets.PkDialogtryagain;

import java.util.HashMap;

public class Saveplus_withdraw_statement extends AppCompatActivity {

    private ImageView back;
    private TextView fastwallet, tv_saveplusamount, tv_statement, tv_withdraw, tv_close;
    private SessionManager sessionManager;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saveplus_withdraw_statement);
        mhelper = new LanguageDb(this);
        init();
    }

    private void init() {
        sessionManager = new SessionManager(this);
        back = findViewById(R.id.back);
        fastwallet = findViewById(R.id.fastwallet);
        tv_saveplusamount = findViewById(R.id.tv_saveplusamount);
        tv_statement = findViewById(R.id.tv_statement);
        tv_statement.setText(getkey("statements"));
        tv_withdraw = findViewById(R.id.tv_withdraw);
        tv_withdraw.setText(getkey("withdraw_lable"));
        tv_close = findViewById(R.id.tv_close);
        tv_close.setText(getkey("close_lable"));
        final HashMap<String, String> saveplsu = sessionManager.getsaveplus_balance();
        String amount = saveplsu.get(SessionManager.KEY_SAVEPLUSBALANCE);
        tv_saveplusamount.setText(sessionManager.getCurrency() + " " + amount);

        TextView tv_saveplsu =(TextView)findViewById(R.id.tv_saveplsu);
        tv_saveplsu.setText(getkey("saveplus"));

        TextView tv_text =(TextView)findViewById(R.id.tv_text);
        tv_text.setText(getkey("withdrawaltext_saveplus"));

        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        fastwallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Saveplus_withdraw_statement.this, WalletMoneyPage1.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        tv_withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> withdrawalstatus = sessionManager.getwithdrawal_status();
                String status = withdrawalstatus.get(SessionManager.KEY_SAVEPLUSWITHDRAWAL_STATUS);

                HashMap<String, String> minbalance = sessionManager.getsaveplus_minbalance();
                String minbalance_obj = minbalance.get(SessionManager.KEY_SAVEPLUSMINBALANCE);

                if (status.equalsIgnoreCase("1")) {
                    Intent i = new Intent(Saveplus_withdraw_statement.this, Withdraw_pin.class);
                    startActivity(i);
                } else {
                    Alert(getkey("action_error"),getkey("balv")+sessionManager.getCurrency()+" "+minbalance_obj);
                }
            }
        });

        tv_statement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Saveplus_withdraw_statement.this, Saveplus_transactionlist.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

    private void Alert(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Saveplus_withdraw_statement.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("actionclose"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}
