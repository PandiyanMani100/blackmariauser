package com.blackmaria.newdesigns.saveplus;

import android.content.Intent;
import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.Pin_activitynew;
import com.blackmaria.newdesigns.fastwallet.Fastpayhome;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.widgets.PkDialogtryagain;
import com.itsxtt.patternlock.PatternLockView;

import java.util.ArrayList;
import java.util.HashMap;

public class Checkpattern extends AppCompatActivity {

    private SessionManager sessionManager;
    TextView forgotpattern;
    private LinearLayout ly_forgetpin;
    private PatternLockView patternLockView;
    private TextView confirm;
    private String ids_confirm = "";
    LanguageDb mhelper;
    private int countcheck_wrongpattern = 1;
    private ImageView booking_back_imgeview;
    String saveplusid="",codepath="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkpattern);
        mhelper = new LanguageDb(this);
        Intent intent = getIntent();
        if (intent.hasExtra("saveplusid"))
        {
            saveplusid = intent.getStringExtra("saveplusid");
            codepath = intent.getStringExtra("codepath");
        }

        sessionManager = new SessionManager(this);
        patternLockView = findViewById(R.id.patternLockView);
        forgotpattern= findViewById(R.id.forgotpattern);
        forgotpattern.setText(getkey("forgetpattern"));
        HashMap<String, String> getpattern = sessionManager.getpattern();
        String pattern = getpattern.get(SessionManager.KEY_PATTERN);
        ids_confirm = pattern;
        ly_forgetpin = findViewById(R.id.ly_forgetpin);

        confirm = findViewById(R.id.confirm);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);

        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Checkpattern.this, Fastpayhome.class);
                i.putExtra("codepath",codepath);
                i.putExtra("saveplusid",saveplusid);
                startActivity(i);
                finish();
            }
        });

        forgotpattern.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Checkpattern.this, Pin_activitynew.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });

        ly_forgetpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Checkpattern.this, Pin_activitynew.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });


        patternLockView.setOnPatternListener(new PatternLockView.OnPatternListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(ArrayList<Integer> ids) {

                System.out.println("progress........... " + ids);
            }

            @Override
            public boolean onComplete(ArrayList<Integer> ids) {
                /*
                 * A return value required
                 * if the pattern is not correct and you'd like change the pattern to error state, return false
                 * otherwise return true
                 */
                System.out.println("completed........... " + ids);
                return isPatternCorrect(ids);
            }
        });


    }

    private boolean isPatternCorrect(ArrayList<Integer> ids) {
        boolean value = false;
        String patern = ids.toString().replace(",", "");
        String paterns = patern.replace("[", "");
        String paternse = paterns.replace("]", "");

        if (isequal(paternse, ids_confirm)) {
            value = true;
            confirm.setVisibility(View.VISIBLE);

        } else {

            if (countcheck_wrongpattern == 1) {
                countcheck_wrongpattern++;
                Alertwrongpattern(getkey("action_error"), getkey("wrongh_pattern"));
            } else if (countcheck_wrongpattern == 2) {
                countcheck_wrongpattern++;
                Alertwrongpattern(getkey("action_error"),getkey("wrongh_pattern"));
            } else if (countcheck_wrongpattern == 3) {
                countcheck_wrongpattern = 1;
                closepopupinseconds(getkey("wrong_patterns"), getkey("wrong_poattern_redrae"));
            }
            value = false;
            confirm.setVisibility(View.INVISIBLE);
        }
        return value;
    }

    private boolean isequal(String pattern, String newpattern) {
        boolean val = false;

        if (pattern.equalsIgnoreCase(newpattern)) {
            val = true;
        }

        return val;
    }


    private void Alertwrongpattern(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Checkpattern.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("forget_pattern"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Checkpattern.this, Pin_activitynew.class);
                startActivity(i);
                finish();
            }
        });
        mDialog.show();
    }

    private void closepopupinseconds(String title, final String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Checkpattern.this);
        mDialog.setDialogTitle(title);
        mDialog.setCancelble(false);
        mDialog.setCancelOnTouchOutside(false);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mDialog.setDialogMessage(" " + millisUntilFinished / 1000 + "\n \n \n " + alert);
            }

            public void onFinish() {
                mDialog.dismiss();
            }

        }.start();


        mDialog.show();
    }


    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
