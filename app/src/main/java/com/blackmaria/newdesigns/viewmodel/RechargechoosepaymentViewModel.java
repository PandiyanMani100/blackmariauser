package com.blackmaria.newdesigns.viewmodel;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;


import com.blackmaria.R;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityRechargechoosepaymentBinding;

import java.util.HashMap;

public class RechargechoosepaymentViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private Dialog dialog;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private int count = 0;
    private ActivityRechargechoosepaymentBinding binding;
    private MutableLiveData<String> getbankpaymentlistresponse = new MutableLiveData<>();

    public RechargechoosepaymentViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        appUtils = AppUtils.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);
    }

    public MutableLiveData<String> getGetbankpaymentlistresponse() {
        return getbankpaymentlistresponse;
    }

    public void getbankrechargepaymentlist(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, RechargechoosepaymentViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        getbankpaymentlistresponse.setValue(val);
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityRechargechoosepaymentBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }


}
