package com.blackmaria.newdesigns.viewmodel.withdraw.bank;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import com.blackmaria.Navigation_new;
import com.blackmaria.databinding.ActivityNobankaccountConstrainBinding;

public class NobankaccountViewModel extends ViewModel {
    private Activity context;
    private ActivityNobankaccountConstrainBinding binding;

    public NobankaccountViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityNobankaccountConstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }

    public void cancel() {
        context.startActivity(new Intent(context, Navigation_new.class));
        context.finish();
    }
}
