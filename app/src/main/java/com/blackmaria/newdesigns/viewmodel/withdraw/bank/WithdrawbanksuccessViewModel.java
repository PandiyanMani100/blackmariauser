package com.blackmaria.newdesigns.viewmodel.withdraw.bank;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import com.blackmaria.Navigation_new;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.databinding.ActivityWithdrawbanksuccessConstrainBinding;

public class WithdrawbanksuccessViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private ActivityWithdrawbanksuccessConstrainBinding binding;

    public WithdrawbanksuccessViewModel(Activity context) {
        this.context = context;
    }

    @Override
    public void sucessresponse(String val) {

    }

    @Override
    public void errorreponse() {

    }

    @Override
    public void jsonexception() {

    }

    public void setIds(ActivityWithdrawbanksuccessConstrainBinding binding) {
        this.binding = binding;
    }

    public void cancel() {
        context.startActivity(new Intent(context, Navigation_new.class));
        context.finish();
    }
}
