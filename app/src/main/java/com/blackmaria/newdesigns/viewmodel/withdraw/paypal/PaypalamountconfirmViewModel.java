package com.blackmaria.newdesigns.viewmodel.withdraw.paypal;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;


import com.blackmaria.Navigation_new;
import com.blackmaria.R;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.databinding.ActivityPaypalamountconfirmBinding;

import java.util.HashMap;

public class PaypalamountconfirmViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
    private ActivityPaypalamountconfirmBinding binding;
    private MutableLiveData<String> paypalapihitresponse = new MutableLiveData<>();

    public PaypalamountconfirmViewModel(Activity context) {
        this.context = context;
    }

    public MutableLiveData<String> getPaypalapihitresponse() {
        return paypalapihitresponse;
    }

    public void walletwithdrawtopaypal(String url, HashMap<String, String> jsonParams) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, PaypalamountconfirmViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        paypalapihitresponse.setValue(val);
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityPaypalamountconfirmBinding binding) {
        this.binding = binding;
    }

    public void back(){
        context.onBackPressed();
    }

    public void cancel() {
        context.startActivity(new Intent(context, Navigation_new.class));
        context.finish();
    }
}
