package com.blackmaria.newdesigns.viewmodel.withdraw.bank;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;


import com.blackmaria.R;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.databinding.ActivityWithdrawpaymentschooseConstrainBinding;

import java.util.HashMap;

public class WithdrawpaymentschooseViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
    private ActivityWithdrawpaymentschooseConstrainBinding binding;
    private int count = 0;
    private MutableLiveData<String> walletwithdrawresponse = new MutableLiveData<>();
    private MutableLiveData<String> choosepaymentswithdrawalsresponse = new MutableLiveData<>();

    public WithdrawpaymentschooseViewModel(Activity context) {
        this.context = context;
    }

    public MutableLiveData<String> getWalletwithdrawresponse() {
        return walletwithdrawresponse;
    }

    public MutableLiveData<String> getChoosepaymentswithdrawalsresponse() {
        return choosepaymentswithdrawalsresponse;
    }

    public void walletwithdrawhome(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, WithdrawpaymentschooseViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    public void choosepaymentwithdrawals(String url, HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, WithdrawpaymentschooseViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            walletwithdrawresponse.setValue(val);
        } else if (count == 1) {
            choosepaymentswithdrawalsresponse.setValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityWithdrawpaymentschooseConstrainBinding binding) {
        this.binding = binding;
    }

    public void back(){
        context.onBackPressed();
    }
}
