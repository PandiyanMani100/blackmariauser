package com.blackmaria.newdesigns.viewmodel.withdraw.cash;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import com.blackmaria.Navigation_new;
import com.blackmaria.databinding.ActivityCashwithdrawsuccessConstrainBinding;


public class CashwithdrawsuccessViewModel extends ViewModel {
    private Activity context;
    private ActivityCashwithdrawsuccessConstrainBinding binding;

    public CashwithdrawsuccessViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityCashwithdrawsuccessConstrainBinding binding) {
        this.binding = binding;
    }

    public void close() {
        context.startActivity(new Intent(context, Navigation_new.class));
        context.finish();
    }
}
