package com.blackmaria.newdesigns.viewmodel.transfer;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import com.blackmaria.Navigation_new;
import com.blackmaria.databinding.ActivityTransfersuccessConstrainBinding;

public class TransfersuccessViewModel extends ViewModel {
    private Activity context;
    private ActivityTransfersuccessConstrainBinding binding;

    public TransfersuccessViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityTransfersuccessConstrainBinding binding) {
        this.binding = binding;
    }

    public void close() {
        context.startActivity(new Intent(context, Navigation_new.class));
        context.finish();
    }
}
