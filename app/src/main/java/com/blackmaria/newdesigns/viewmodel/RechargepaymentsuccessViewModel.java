package com.blackmaria.newdesigns.viewmodel;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import com.blackmaria.Navigation_new;
import com.blackmaria.databinding.ActivityRechargepaymentsuccessConstrainBinding;

public class RechargepaymentsuccessViewModel extends ViewModel {
    private Activity context;
    private ActivityRechargepaymentsuccessConstrainBinding binding;

    public RechargepaymentsuccessViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityRechargepaymentsuccessConstrainBinding binding) {
        this.binding = binding;
    }

    public void close() {
        Intent i = new Intent(context, Navigation_new.class);
        context.startActivity(i);
    }

}
