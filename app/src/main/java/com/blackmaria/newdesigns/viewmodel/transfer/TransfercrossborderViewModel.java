package com.blackmaria.newdesigns.viewmodel.transfer;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;


import com.blackmaria.R;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.databinding.ActivityTransfercrossborderConstrainBinding;

import java.util.HashMap;

public class TransfercrossborderViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
    private ActivityTransfercrossborderConstrainBinding binding;
    private MutableLiveData<String> crossvordertransferresponse = new MutableLiveData<>();

    public TransfercrossborderViewModel(Activity context) {
        this.context = context;
    }

    public MutableLiveData<String> getCrossvordertransferresponse() {
        return crossvordertransferresponse;
    }

    public void transferamounttofriend(String url, HashMap<String, String> jsonParams) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, TransfercrossborderViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        crossvordertransferresponse.setValue(val);
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityTransfercrossborderConstrainBinding binding) {
        this.binding = binding;
    }

    public void back(){
        context.onBackPressed();
    }
}
