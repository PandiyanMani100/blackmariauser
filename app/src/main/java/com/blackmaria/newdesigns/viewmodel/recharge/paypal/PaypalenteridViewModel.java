package com.blackmaria.newdesigns.viewmodel.recharge.paypal;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;


import com.blackmaria.R;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityPaypalenteridConstrainBinding;

import java.util.HashMap;

public class PaypalenteridViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private Dialog dialog;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private ActivityPaypalenteridConstrainBinding binding;
    private int count = 0;


    public PaypalenteridViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        appUtils = AppUtils.getInstance(context);
    }

    public void bankpaymentdriver(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, PaypalenteridViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {

    }

    @Override
    public void errorreponse() {

    }

    @Override
    public void jsonexception() {

    }

    public void back() {
        context.onBackPressed();
    }

    public void confirm(){

    }

    public void setIds(ActivityPaypalenteridConstrainBinding binding) {
        this.binding = binding;
    }
}
