package com.blackmaria.newdesigns.viewmodel;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.newdesigns.view.Rechargechoosepayment;
import com.blackmaria.pojo.rechargeamountpojo;
import com.blackmaria.R;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityRechargeAmountConstrainBinding;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class RechargeamountViewModel extends ViewModel implements ApIServices.completelisner {

    private ActivityRechargeAmountConstrainBinding binding;
    private Activity context;
    private Dialog dialog;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private int count = 0;
    private JSONObject json;
    private rechargeamountpojo pojo;
    private MutableLiveData<String> getmoneypagedriverresponse = new MutableLiveData<>();

    public RechargeamountViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        appUtils = AppUtils.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);

    }

    public MutableLiveData<String> getGetmoneypagedriverresponse() {
        return getmoneypagedriverresponse;
    }

    public void getmoneypagedriver(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);

         dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        ApIServices apIServices = new ApIServices(context, RechargeamountViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            getmoneypagedriverresponse.setValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityRechargeAmountConstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }

    public void setpojo(JSONObject json) {
        this.json = json;
        Type type = new TypeToken<rechargeamountpojo>() {
        }.getType();
        pojo = new GsonBuilder().create().fromJson(json.toString(), type);
    }

    public void walletone() {
        Intent intent = new Intent(context, Rechargechoosepayment.class);
        intent.putExtra("insertAmount", pojo.getResponse().getRecharge_boundary().getWal_recharge_amount_one());
        intent.putExtra("jsonpojo", json.toString());
        context.startActivity(intent);
    }

    public void wallettwo() {
        Intent intent = new Intent(context, Rechargechoosepayment.class);
        intent.putExtra("insertAmount", pojo.getResponse().getRecharge_boundary().getWal_recharge_amount_two());
        intent.putExtra("jsonpojo", json.toString());
        context.startActivity(intent);
    }

    public void walletthree() {
        Intent intent = new Intent(context, Rechargechoosepayment.class);
        intent.putExtra("insertAmount", pojo.getResponse().getRecharge_boundary().getWal_recharge_amount_three());
        intent.putExtra("jsonpojo", json.toString());
        context.startActivity(intent);
    }

    public void walletfour() {
        Intent intent = new Intent(context, Rechargechoosepayment.class);
        intent.putExtra("insertAmount", pojo.getResponse().getRecharge_boundary().getWal_recharge_amount_four());
        intent.putExtra("jsonpojo", json.toString());
        context.startActivity(intent);
    }
}
