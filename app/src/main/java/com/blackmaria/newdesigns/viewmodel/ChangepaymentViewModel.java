package com.blackmaria.newdesigns.viewmodel;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;


import com.blackmaria.Navigation_new;
import com.blackmaria.R;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityChangepaymentConstrainBinding;

import java.util.HashMap;

public class ChangepaymentViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityChangepaymentConstrainBinding binding;
    private Dialog dialog;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private MutableLiveData<String> changepaymentretry = new MutableLiveData<>();


    public ChangepaymentViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        appUtils = AppUtils.getInstance(context);
    }

    public MutableLiveData<String> getChangepaymentretry() {
        return changepaymentretry;
    }

    public void changepaymenthitretry(String url, HashMap<String, String> jsonParams) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, ChangepaymentViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        changepaymentretry.setValue(val);
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityChangepaymentConstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }

    public void cancel() {
        Intent i = new Intent(context, Navigation_new.class);
        context.startActivity(i);
        context.finish();
    }
}
