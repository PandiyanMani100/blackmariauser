package com.blackmaria.newdesigns.viewmodel.withdraw.bank;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;


import com.blackmaria.R;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.databinding.ActivityAddbankdetailsConstrainBinding;

import java.util.HashMap;

public class AddbankdetailsViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
    private int count = 0;
    private ActivityAddbankdetailsConstrainBinding binding;
    private MutableLiveData<String> addbankdetailsresponse = new MutableLiveData<>();
    private MutableLiveData<String> updatebankdetailsresponse = new MutableLiveData<>();
    private MutableLiveData<String> gettransactionfeechargesresponse = new MutableLiveData<>();

    public AddbankdetailsViewModel(Activity context) {
        this.context = context;
    }

    public MutableLiveData<String> getAddbankdetailsresponse() {
        return addbankdetailsresponse;
    }

    public MutableLiveData<String> getUpdatebankdetailsresponse() {
        return updatebankdetailsresponse;
    }

    public MutableLiveData<String> getGettransactionfeechargesresponse() {
        return gettransactionfeechargesresponse;
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            addbankdetailsresponse.setValue(val);
        } else if (count == 1) {
            updatebankdetailsresponse.setValue(val);
        } else if (count == 2) {
            gettransactionfeechargesresponse.setValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityAddbankdetailsConstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }

    public void getbanklist(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, AddbankdetailsViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    public void updatebankdetails(String url, HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, AddbankdetailsViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    public void gettransactionfeesandcharges(String url, HashMap<String, String> jsonParams) {
        count = 2;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, AddbankdetailsViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }
}
