package com.blackmaria.newdesigns.viewmodel.withdraw.cash;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;


import com.blackmaria.Navigation_new;
import com.blackmaria.R;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.databinding.ActivityCashwithdrawalConstrainBinding;

import java.util.HashMap;

public class CashwithdrawalViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
    private ActivityCashwithdrawalConstrainBinding binding;
    private MutableLiveData<String> withapihitresponse = new MutableLiveData<>();

    public CashwithdrawalViewModel(Activity context) {
        this.context = context;
    }

    public MutableLiveData<String> getWithapihitresponse() {
        return withapihitresponse;
    }

    public void cashwithdrawapihit(String url, HashMap<String, String> jsonParams) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, CashwithdrawalViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        withapihitresponse.setValue(val);
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    public void back() {
        context.onBackPressed();
    }

    public void setIds(ActivityCashwithdrawalConstrainBinding binding) {
        this.binding = binding;
    }

    public void cancel() {
        context.startActivity(new Intent(context, Navigation_new.class));
        context.finish();
    }
}
