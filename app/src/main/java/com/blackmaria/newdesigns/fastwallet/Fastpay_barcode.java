package com.blackmaria.newdesigns.fastwallet;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.Navigation_new;
import com.blackmaria.newdesigns.saveplus.Scanqrcode;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.widgets.PkDialog;
import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;

public class Fastpay_barcode extends AppCompatActivity {

    LanguageDb mhelper;
    private TextView name, scantopay;
    private ImageView close, barcode;
    private SessionManager sessionManager;
    private LinearLayout invite_whatsapp_layout, invite_earn_line_layout, invite_earn_weechat_layout, invite_earn_messagenger_layout, invite_earn_instogram_layout,
            invite_earn_skype_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fastpay_barcode);
        mhelper= new LanguageDb(this);
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here

        }
        intiView();
    }

    private void intiView() {
        sessionManager = new SessionManager(Fastpay_barcode.this);
        name = findViewById(R.id.name);
        invite_whatsapp_layout = findViewById(R.id.invite_whatsapp_layout);
        invite_earn_line_layout = findViewById(R.id.invite_earn_line_layout);
        invite_earn_weechat_layout = findViewById(R.id.invite_earn_weechat_layout);
        invite_earn_messagenger_layout = findViewById(R.id.invite_earn_messagenger_layout);
        invite_earn_instogram_layout = findViewById(R.id.invite_earn_instogram_layout);
        invite_earn_skype_layout = findViewById(R.id.invite_earn_skype_layout);

        TextView tv_scantopay = findViewById(R.id.tv_scantopay);
        tv_scantopay.setText(getkey("scantopay"));


        TextView tv_sharebarcode = findViewById(R.id.tv_sharebarcode);
        tv_sharebarcode.setText(getkey("sharebarcode"));

        invite_whatsapp_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri myUri = Uri.parse(sessionManager.getQrcode_path());
                //sharewhatsapplink("Blackmaria", myUri,"WhatsApp");
                try {
                    URL url = new URL(sessionManager.getQrcode_path());
                    Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    sharewhatsapplink("Blackmaria", image);
                } catch(IOException e) {
                    System.out.println(e);
                }
            }
        });

        invite_earn_line_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri myUri = Uri.parse(sessionManager.getQrcode_path());
                sharelinelink("Blackmaria", myUri);
            }
        });


        invite_earn_weechat_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri myUri = Uri.parse(sessionManager.getQrcode_path());
                sharewechatlink("Blackmaria", myUri);
            }
        });

        invite_earn_messagenger_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    URL url = new URL(sessionManager.getQrcode_path());
                    Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    sharemessengerlink("Blackmaria", image);
                } catch(IOException e) {
                    System.out.println(e);
                }
            }
        });


        invite_earn_instogram_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri myUri = Uri.parse(sessionManager.getQrcode_path());
                try {
                    URL url = new URL(sessionManager.getQrcode_path());
                    Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    shareInstagramelink("Blackmaria", image);
                } catch(IOException e) {
                    System.out.println(e);
                }

            }
        });


        invite_earn_skype_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri myUri = Uri.parse(sessionManager.getQrcode_path());
                shareskypelink("Blackmaria", myUri);
            }
        });


        name.setText(sessionManager.getbusinessname());
        scantopay = findViewById(R.id.scantopay);
        scantopay.setText(getkey("scan_qrcode"));
        close = findViewById(R.id.close);
        barcode = findViewById(R.id.barcode);
        Glide.with(Fastpay_barcode.this).load(sessionManager.getQrcode_path()).into(barcode);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Fastpay_barcode.this, Navigation_new.class));
            }
        });
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(Fastpay_barcode.this, Fastpay_afterscan.class));
            }
        });

        scantopay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Fastpay_barcode.this, Scanqrcode.class);
                i.putExtra("fromfastpay", "");
                startActivity(i);
            }
        });
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    private void Alert1(String title, String alert, final String url) {

        final PkDialog mDialog = new PkDialog(Fastpay_barcode.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        mDialog.show();

    }

    //----------Share Image and Text on Twitter Method--------
    protected void shareTwitter(String text, Uri image) {
        boolean isAppInstalled = appInstalledOrNot("com.twitter.android");

        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_STREAM, image);
            intent.setType("image/jpeg");
            intent.setPackage("com.twitter.android");

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"), getkey("invite_earn_label_twitter_not_installed"), "https://play.google.com/store/apps/details?id=com.twitter.android");
            }
        } else {
            Alert1(getkey("alert_label_title"),getkey("invite_earn_label_twitter_not_installed"), "https://play.google.com/store/apps/details?id=com.twitter.android");
        }
    }

    protected void shareInstagramelink(String text, Bitmap image) {
        boolean isAppInstalled = appInstalledOrNot("org.telegram.messenger");

        if (isAppInstalled)
        {
            Bitmap b = image;
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), b, "Title", null);
            Uri imageUri =  Uri.parse(path);
            share.setPackage("org.telegram.messenger");
            share.putExtra(Intent.EXTRA_TEXT, text);
            share.putExtra(Intent.EXTRA_STREAM, imageUri);
            try {
                startActivity(Intent.createChooser(share, getkey("sgharetto")));
            }
            catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"), getkey("invite_earn_label_telegram_not_installed"), "https://play.google.com/store/apps/details?id=org.telegram.messenger");
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_telegram_not_installed"), "https://play.google.com/store/apps/details?id=org.telegram.messenger");
        }
    }


    protected void sharewhatsapplink(String text, Bitmap image) {
        boolean isAppInstalled = appInstalledOrNot("com.whatsapp");

        if (isAppInstalled) {

            Bitmap b = image;
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), b, "Title", null);
            Uri imageUri =  Uri.parse(path);
            share.setPackage("com.whatsapp");
            share.putExtra(Intent.EXTRA_TEXT, text);
            share.putExtra(Intent.EXTRA_STREAM, imageUri);
            try {
                startActivity(Intent.createChooser(share, getkey("sgharetto")));
            }
            catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"),getkey("invite_earn_label_whatsApp_not_installed"), "https://play.google.com/store/apps/details?id=com.whatsapp");
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_whatsApp_not_installed"), "https://play.google.com/store/apps/details?id=com.whatsapp");
        }
    }

    protected void sharelinelink(String text, Uri image) {
        boolean isAppInstalled = appInstalledOrNot("jp.naver.line.android");

        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            // intent.putExtra(Intent.EXTRA_STREAM, "");
            intent.setType("image/jpeg");
            intent.setPackage("jp.naver.line.android");

            // Add the URI to the Intent.
            intent.putExtra(Intent.EXTRA_STREAM, image);

            try {
                //startActivity(intent);
                startActivity(Intent.createChooser(intent, "Share to"));
            } catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"),getkey("invite_earn_label_linechat_not_installed"), "https://play.google.com/store/apps/details?id=jp.naver.line.android");
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_linechat_not_installed"), "https://play.google.com/store/apps/details?id=jp.naver.line.android");
        }
    }

    protected void sharewechatlink(String text, Uri image) {
        boolean isAppInstalled = appInstalledOrNot("com.tencent.mm");

        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            // intent.putExtra(Intent.EXTRA_STREAM, "");
            intent.setType("image/jpeg");
            intent.setPackage("com.tencent.mm");

            // Add the URI to the Intent.
            intent.putExtra(Intent.EXTRA_STREAM, image);

            try {
                //startActivity(intent);
                startActivity(Intent.createChooser(intent, "Share to"));
            } catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"), getkey("invite_earn_label_wechat_not_installed"), "https://play.google.com/store/apps/details?id=com.tencent.mm");
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_wechat_not_installed"), "https://play.google.com/store/apps/details?id=com.tencent.mm");
        }
    }

    protected void sharemessengerlink(String text, Bitmap image) {
        boolean isAppInstalled = appInstalledOrNot("com.facebook.orca");

        if (isAppInstalled) {

            Bitmap b = image;
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), b, "Title", null);
            Uri imageUri =  Uri.parse(path);
            share.setPackage("com.facebook.orca");
            share.putExtra(Intent.EXTRA_TEXT, text);
            share.putExtra(Intent.EXTRA_STREAM, imageUri);
            try {
                startActivity(Intent.createChooser(share,getkey("sgharetto")));
            }
            catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"), getkey("invite_earn_label_messenger_not_installed"), "https://play.google.com/store/apps/details?id=com.facebook.orca");
            }

        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_messenger_not_installed"), "https://play.google.com/store/apps/details?id=com.facebook.orca");
        }
    }

    protected void shareskypelink(String text, Uri image) {
        boolean isAppInstalled = appInstalledOrNot("com.skype.raider");

        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            // intent.putExtra(Intent.EXTRA_STREAM, "");
            intent.setType("image/jpeg");
            intent.setPackage("com.skype.raider");

            // Add the URI to the Intent.
            intent.putExtra(Intent.EXTRA_STREAM, image);

            try {
                //startActivity(intent);
                startActivity(Intent.createChooser(intent, "Share to"));
            } catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"), getkey("invite_earn_label_skype_not_installed"), "https://play.google.com/store/apps/details?id=com.skype.raider");
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_skype_not_installed"), "https://play.google.com/store/apps/details?id=com.skype.raider");
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
