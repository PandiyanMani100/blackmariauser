package com.blackmaria.newdesigns.fastwallet.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.blackmaria.pojo.Fastpayhome_pojo;
import com.blackmaria.R;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import id.yuana.chart.pie.PieChartView;

public class FragmentTab1 extends Fragment {

    private PieChartView pieChartView;

    private int[] arrayofcolor = {R.color.white,
            R.color.firstround,
            R.color.secondround,
            R.color.thirdround,
            R.color.fourthround,
            R.color.white};

    private float[] arrayfloat = {400F, 400F, 400F, 400F, 400F, 400F};

    private Fastpayhome_pojo homepage;

    private TextView tv_monthlimitamount, tv_yearhighlights, tv_totalspentamount, tv_payments_value, tv_transfers_value, tv_withdraw_value;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragmenttab1, container, false);

        if (getArguments().containsKey("fastpayhome")) {
            String jsonpojo = getArguments().getString("fastpayhome");
            try {
                homepage = new Fastpayhome_pojo();
                Type listType = new TypeToken<Fastpayhome_pojo>() {
                }.getType();
                homepage = new GsonBuilder().create().fromJson(jsonpojo, listType);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        initView(rootView);

        return rootView;
    }

    private void initView(View rootView) {
        pieChartView = rootView.findViewById(R.id.pieChart);
        pieChartView.setCenterColor(R.color.white);
        pieChartView.setDataPoints(arrayfloat);
        pieChartView.setSliceColor(arrayofcolor);

        tv_monthlimitamount = rootView.findViewById(R.id.tv_monthlimitamount);
        tv_yearhighlights = rootView.findViewById(R.id.tv_yearhighlights);
        tv_totalspentamount = rootView.findViewById(R.id.tv_totalspentamount);
        tv_payments_value = rootView.findViewById(R.id.tv_payments_value);
        tv_transfers_value = rootView.findViewById(R.id.tv_transfers_value);
        tv_withdraw_value = rootView.findViewById(R.id.tv_withdraw_value);

        tv_monthlimitamount.setText(" "+homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal().getWallet_credit());
        tv_yearhighlights.setText(homepage.getResponse().getText());
        tv_totalspentamount.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal().getWallet_spend());
        tv_payments_value.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal().getPayment());
        tv_transfers_value.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal().getTransfer());
        tv_withdraw_value.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal().getWithdrawal());
    }
}