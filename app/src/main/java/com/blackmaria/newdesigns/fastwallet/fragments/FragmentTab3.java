package com.blackmaria.newdesigns.fastwallet.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.view.View;
import android.widget.TextView;

import com.blackmaria.pojo.Fastpayhome_pojo;
import com.blackmaria.R;
import com.blackmaria.WalletMoneyTransaction1;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Locale;

public class FragmentTab3 extends AppCompatActivity {
    private Fastpayhome_pojo homepage;
    private TextView tv_monthlimitamount,tv_debit_amount;
    private CardView cardview_signup;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragmenttab3);

        if (getIntent().hasExtra("fastpayhome")) {
            String jsonpojo = getIntent().getStringExtra("fastpayhome");
            try {
                homepage = new Fastpayhome_pojo();
                Type listType = new TypeToken<Fastpayhome_pojo>() {
                }.getType();
                homepage = new GsonBuilder().create().fromJson(jsonpojo, listType);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



        initView();


    }

    private void initView() {
        tv_monthlimitamount = findViewById(R.id.tv_monthlimitamount);
        tv_debit_amount = findViewById(R.id.tv_debit_amount);
        cardview_signup = findViewById(R.id.cardview_signup);

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);

        Calendar mCalendar = Calendar.getInstance();
        String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        tv_monthlimitamount.setText(month +" "+String.valueOf(year));
        tv_debit_amount.setText(homepage.getResponse().getCurrency()+" "+homepage.getResponse().getTotal().getWallet_credit());

        cardview_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WalletMoneyTransaction1.class);
                intent.putExtra("type", "credit");
                startActivity(intent);
            }
        });
    }

}