package com.blackmaria.newdesigns.fastwallet;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.Navigation_new;
import com.blackmaria.newdesigns.Forgetpin;
import com.blackmaria.newdesigns.Pin_activitynew;
import com.blackmaria.newdesigns.profileInfo.ProfileConstain_activity;
import com.blackmaria.newdesigns.saveplus.Scanqrcode;
import com.blackmaria.pojo.Saveplustransfer;
import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.PkDialogtryagain;
import com.bumptech.glide.Glide;
import com.devspark.appmsg.AppMsg;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class Fastpaypincheck extends AppCompatActivity {

    private TextView img2, img1, img3, img5, img4, img6, img8, img7, img9, img0, img_star, img_hash, confirm;
    private EditText edt_email;
    private LinearLayout ly_forgetpin;
    private ImageView booking_back_imgeview;
    private String number = "";
    int[] imageViews = {R.id.img1, R.id.img2, R.id.img3, R.id.img4, R.id.img5, R.id.img6, R.id.img7, R.id.img8, R.id.img9, R.id.img0};
    private float TIME_DELAY = 2;
    private float lastY = 0;
    private SessionManager session;
    private HashMap<String, String> info;
    private boolean isfromsaveplus_scan= false,isfromsaveplussendmoney = false,isfrommenu=false;

    private String receiver_id ="",received_amount="",total_amount="",finalmoney="",UserID="";
    private Saveplustransfer homepage;

    private int countcheck_wrongpattern = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fastpaypincheck);


        init();
        clicklistener();
    }


    private void init() {
        session = new SessionManager(this);
        info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        ly_forgetpin = findViewById(R.id.ly_forgetpin);
        edt_email = findViewById(R.id.edt_email);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);
        confirm = findViewById(R.id.confirm);
        img2 = findViewById(R.id.img2);
        img1 = findViewById(R.id.img1);
        img3 = findViewById(R.id.img3);
        img5 = findViewById(R.id.img5);
        img4 = findViewById(R.id.img4);
        img6 = findViewById(R.id.img6);
        img8 = findViewById(R.id.img8);
        img7 = findViewById(R.id.img7);
        img9 = findViewById(R.id.img9);
        img0 = findViewById(R.id.img0);
        img_star = findViewById(R.id.img_star);
        img_hash = findViewById(R.id.img_hash);

        if(getIntent().hasExtra("fromsaveplusscan")){
            isfromsaveplus_scan = true;
        }else if(getIntent().hasExtra("receiver_id")){
            isfromsaveplussendmoney = true;
            receiver_id = getIntent().getStringExtra("receiver_id");
            received_amount = getIntent().getStringExtra("received_amount");
            total_amount = getIntent().getStringExtra("total_amount");
            finalmoney = getIntent().getStringExtra("finalmoney");
        }else if(getIntent().hasExtra("frommenu")){
            isfrommenu = true;
        }
    }

    private void clicklistener() {

        edt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
            }
        });
        edt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 6){
                    confirm.setBackground(getResources().getDrawable(R.drawable.green_curve));
                }else{
                    confirm.setBackground(getResources().getDrawable(R.drawable.grey_curve));
                }
            }
        });
        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "1";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());

                setbackground(img1.getId());
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "2";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img2.getId());
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "3";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img3.getId());
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "4";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img4.getId());
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "5";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img5.getId());
            }
        });

        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "6";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img6.getId());
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "7";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img7.getId());
            }
        });
        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "8";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img8.getId());
            }
        });
        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "9";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img9.getId());
            }
        });

        img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "0";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img0.getId());
            }
        });

        img_hash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String SMobileNo = edt_email.getText().toString();
                if (SMobileNo != null && SMobileNo.length() > 0) {
                    edt_email.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                    edt_email.setSelection(edt_email.getText().length());
                    number = edt_email.getText().toString();
                }

                img_hash.setBackground(getResources().getDrawable(R.drawable.round_green));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img_hash.setBackground(getResources().getDrawable(R.drawable.round_white));
                    }
                }, 100);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.getText().toString().length() == 0) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_pincode));
                } else if (edt_email.getText().toString().trim().length() != 6) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_pincodes));
                } else {
                    ConnectionDetector cd = new ConnectionDetector(Fastpaypincheck.this);
                    boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {

                        HashMap<String, String> user = session.getUserDetails();
                        String sSecurePin = session.getSecurePin();
                        if (sSecurePin.equalsIgnoreCase(edt_email.getText().toString().trim())) {
                            if(isfromsaveplus_scan){
                                number = "";
                                edt_email.setText("");
                                edt_email.getText().clear();
                                Intent i = new Intent(Fastpaypincheck.this, Scanqrcode.class);
                                startActivity(i);
                            }else if(isfromsaveplussendmoney){
                                number = "";
                                postrequestUpdateProfile(Iconstant.saveplustransfer);
                            }else if(isfrommenu){
                                number = "";
                                edt_email.setText("");
                                edt_email.getText().clear();
                                Intent intent_profile = new Intent(Fastpaypincheck.this, ProfileConstain_activity.class);
                                intent_profile.putExtra("frommenu", "1");
                                startActivity(intent_profile);
                                finish();
                            }else{
                                number = "";
                                edt_email.setText("");
                                edt_email.getText().clear();
                                Intent i = new Intent(Fastpaypincheck.this, Fastpay_paymentsucess.class);
                                startActivity(i);
                            }
                        } else {
                            number = "";
                            if (countcheck_wrongpattern == 1) {
                                countcheck_wrongpattern++;
                                edt_email.setText("");
                                edt_email.getText().clear();
                                Alertwrongpattern(getResources().getString(R.string.action_error), getResources().getString(R.string.wornspins));
                            } else if (countcheck_wrongpattern == 2) {
                                countcheck_wrongpattern++;
                                edt_email.setText("");
                                edt_email.getText().clear();
                                Alertwrongpattern(getResources().getString(R.string.action_error), getResources().getString(R.string.wrongpin_firstime));
                            } else if (countcheck_wrongpattern == 3) {
                                countcheck_wrongpattern = 1;
                                edt_email.setText("");
                                edt_email.getText().clear();
                                closepopupinseconds(getResources().getString(R.string.wron_pin), getResources().getString(R.string.plse_enter_untlin_last));
                            }


//                            Alert(getResources().getString(R.string.action_error), "You have entered wrong pin");
                        }

                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }
                }
            }
        });

        ly_forgetpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Fastpaypincheck.this, Forgetpin.class);
                startActivity(i);
            }
        });
    }

    private void closepopupinseconds(String title, final String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Fastpaypincheck.this);
        mDialog.setDialogTitle(title);
        mDialog.setCancelble(false);
        mDialog.setCancelOnTouchOutside(false);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mDialog.setDialogMessage(" " + millisUntilFinished / 1000 + "\n \n \n " + alert);
            }

            public void onFinish() {
                mDialog.dismiss();
            }

        }.start();


        mDialog.show();
    }

    private void Alertwrongpattern(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Fastpaypincheck.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getResources().getString(R.string.forget_pattern), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Fastpaypincheck.this, Pin_activitynew.class);
                startActivity(i);
                finish();
            }
        });
        mDialog.show();
    }

    private void setbackground(int image) {
        for (int i = 0; i <= imageViews.length - 1; i++) {
            final TextView img = findViewById(imageViews[i]);
            if (image == imageViews[i]) {
                img.setBackground(getResources().getDrawable(R.drawable.round_green));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img.setBackground(getResources().getDrawable(R.drawable.round_white));
                    }
                }, 100);
            } else {
                img.setBackground(getResources().getDrawable(R.drawable.round_white));
            }
        }
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Fastpaypincheck.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(Fastpaypincheck.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    private void postrequestUpdateProfile(String Url) {

        final Dialog dialog = new Dialog(Fastpaypincheck.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("receiver_id", receiver_id);
        jsonParams.put("received_amount", total_amount);
        jsonParams.put("total_amount", received_amount);
        jsonParams.put("user_id", UserID);

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(Fastpaypincheck.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);
                homepage = new Saveplustransfer();
                dialog.dismiss();
                JSONObject object = null;
                number = "";
                edt_email.setText("");
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Type listType = new TypeToken<Saveplustransfer>() {
                        }.getType();
                        homepage = new GsonBuilder().create().fromJson(object.toString(), listType);
                        saveplusbill_popup(homepage);

                    } else {
                        Smessage = object.getString("response");
                        Alert(getResources().getString(R.string.action_error), Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void saveplusbill_popup(Saveplustransfer homepage) {
        final Dialog dialog = new Dialog(Fastpaypincheck.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.saveplusbill);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);


        final ImageView ivclose = dialog.findViewById(R.id.close);
        final TextView smallext = dialog.findViewById(R.id.smallext);
        final RoundedImageView iv_profile = dialog.findViewById(R.id.iv_profile);
        final TextView tv_name = dialog.findViewById(R.id.tv_name);
        final TextView tv_saveplusid = dialog.findViewById(R.id.tv_saveplusid);
        final TextView tv_billamount = dialog.findViewById(R.id.tv_billamount);
        final TextView tv_date = dialog.findViewById(R.id.tv_date);
        final TextView tv_yourpricevalue = dialog.findViewById(R.id.tv_yourpricevalue);
        final TextView tv_payment_value = dialog.findViewById(R.id.tv_payment_value);
        final TextView tv_moneyreceived_value = dialog.findViewById(R.id.tv_moneyreceived_value);
        final TextView tv_savepluscredit_value = dialog.findViewById(R.id.tv_savepluscredit_value);
        final TextView tv_transationno = dialog.findViewById(R.id.tv_transationno);
        final TextView amount = dialog.findViewById(R.id.amount);

        tv_date.setText("Date : " + homepage.getResponse().getTrans_date()+"\n"+homepage.getResponse().getTrans_time());
        tv_name.setText(homepage.getResponse().getReceiver_details().getName());
        tv_saveplusid.setText(getResources().getString(R.string.saveplus_id)+ homepage.getResponse().getSaveplus_id());
        tv_billamount.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getCrdit_amount());
        tv_yourpricevalue.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal_amount());
        tv_moneyreceived_value.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getReceived_amount());
        tv_savepluscredit_value.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getCrdit_amount());
        tv_transationno.setText(getResources().getString(R.string.trasn_number)+ homepage.getResponse().getTrans_id());
        amount.setText("Merchant ID : " + homepage.getResponse().getSaveplus_id());


        Glide.with(this).load(homepage.getResponse().getReceiver_details().getImage()).into(iv_profile);
        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = "";
                edt_email.setText("");
                Intent i = new Intent(Fastpaypincheck.this, Navigation_new.class);
                startActivity(i);
                dialog.dismiss();
                finish();

            }
        });

        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

}
