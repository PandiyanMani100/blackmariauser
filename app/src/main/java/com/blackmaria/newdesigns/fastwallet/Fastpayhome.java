package com.blackmaria.newdesigns.fastwallet;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.adapter.CustomSpinnerAdapter;
import com.blackmaria.InterFace.Slider;
import com.blackmaria.NewCloudMoneyWithDrawHome;
import com.blackmaria.newdesigns.saveplus.Saveplus_withdraw_statement;
import com.blackmaria.newdesigns.saveplus.Scanqrcode;
import com.blackmaria.newdesigns.slider.CirclePageIndicator;
import com.blackmaria.newdesigns.view.RechargeAmountConstrain;
import com.blackmaria.newdesigns.view.Transfer.Transfermobilenumbersearch;
import com.blackmaria.newdesigns.view.Withdraw.Bank.WithdrawpaymentschooseConstrain;
import com.blackmaria.newdesigns.fastwallet.fragments.FragmentTabnew1;
import com.blackmaria.pojo.Fastpayhome_pojo;
import com.blackmaria.pojo.Homepage_pojo;
import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.PkDialogtryagain;
import com.bumptech.glide.Glide;
import com.devspark.appmsg.AppMsg;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import static com.blackmaria.HomePage.isValidEmail;

public class Fastpayhome extends AppCompatActivity {

    private ImageView saveplusricon, paymenicon, booking_back_imgeview;
    private TextView tv_saveplus, tv_saveplusamount, tv_menu,tv_paymee, tv_balamount, tv_paylateramount;
    private TextView tv_addfunds, tv_sendfunds, tv_withdrawfunds, tv_redemcash;
    LinearLayout topupli,withdrawli,sendli,payli;
    private final Integer[] IMAGES = {R.drawable.cars_home, R.drawable.cars_home, R.drawable.cars_home, R.drawable.cars_home};

    private ViewPager mPager;
    private int currentPage = 0;
    private int NUM_PAGES = 0;

    private ConstraintLayout downlayouts, menuview;
    private boolean isdown = false;
    private String UserID = "", jsonpojo = "";
    private SessionManager session;
    private HashMap<String, String> info;
    private Fastpayhome_pojo homepage;
    private ConnectionDetector cd;
    String saveplusids="",codepath="";
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fastpayhome);
        mhelper = new LanguageDb(this);
        Intent intent = getIntent();
        if (intent.hasExtra("saveplusid"))
        {
            saveplusids = intent.getStringExtra("saveplusid");
            codepath = intent.getStringExtra("codepath");
        }
        initView();
        clicklistener();

    }

    private void initView() {
        cd = new ConnectionDetector(Fastpayhome.this);
        session = new SessionManager(this);
        info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        saveplusricon = findViewById(R.id.saveplusricon);
        paymenicon = findViewById(R.id.paymenicon);
        tv_saveplus = findViewById(R.id.tv_saveplus);
        tv_saveplus.setText(getkey("saveplus_label"));
        tv_saveplusamount = findViewById(R.id.tv_saveplusamount);
        tv_paylateramount = findViewById(R.id.tv_paylateramount);
        mPager = findViewById(R.id.pager);

        tv_paymee= findViewById(R.id.tv_paymee);
        tv_paymee.setText(getkey("sign_up_as_merchant"));
        topupli= findViewById(R.id.topupli);
        withdrawli= findViewById(R.id.withdrawli);
        sendli= findViewById(R.id.sendli);
        payli= findViewById(R.id.payli);


        TextView toppp= findViewById(R.id.toppp);
        toppp.setText(getkey("top_up"));

        TextView widr= findViewById(R.id.widr);
        widr.setText(getkey("withdraw_lable"));

        TextView sendd= findViewById(R.id.sendd);
        sendd.setText(getkey("send"));

        TextView payyy= findViewById(R.id.payyy);
        payyy.setText(getkey("pay"));

        tv_addfunds = findViewById(R.id.tv_addfunds);
        tv_sendfunds = findViewById(R.id.tv_sendfunds);
        tv_withdrawfunds = findViewById(R.id.tv_withdrawfunds);
        tv_redemcash = findViewById(R.id.tv_redemcash);
        tv_menu = findViewById(R.id.tv_menu);
        tv_balamount = findViewById(R.id.tv_balamount);
        downlayouts = findViewById(R.id.downlayouts);
        menuview = findViewById(R.id.menuview);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);

        fastpayhome(Iconstant.fastpayhome);
    }


    private void clicklistener() {

        tv_paymee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (session.getbusinessprofile().equalsIgnoreCase("1")) {
                    Intent i = new Intent(Fastpayhome.this, Fastpay_barcode.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(Fastpayhome.this, Fastpaybusiness_setting.class);
                    startActivity(i);
                }
            }
        });
        paymenicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (session.getbusinessprofile().equalsIgnoreCase("1")) {
                    Intent i = new Intent(Fastpayhome.this, Fastpay_barcode.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(Fastpayhome.this, Fastpaybusiness_setting.class);
                    startActivity(i);
                }
            }
        });

        topupli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Fastpayhome.this, RechargeAmountConstrain.class);
//                Intent intent = new Intent(Fastpayhome.this, NewCloudMoneyReloadHomePage.class);
                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        sendli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(Fastpayhome.this, NewCloudMoneyTransferHome.class);
                Intent intent = new Intent(Fastpayhome.this, Transfermobilenumbersearch.class);
                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        withdrawli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                info = session.getUserCompleteProfile();
//                String UserProfileUpdateStatus = info.get(SessionManager.COMPLETE_PROFILE);
//
//                if (UserProfileUpdateStatus.equalsIgnoreCase("0")) {
//                    System.out.println("==============Muruga UserProfileUpdateStatus==========" + UserProfileUpdateStatus);
//                    UpdateProfilePopUp();
//                } else {
//                    Intent intent = new Intent(Fastpayhome.this, NewCloudMoneyWithDrawHome.class);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.enter, R.anim.exit);
//                }
                Intent intent = new Intent(Fastpayhome.this, WithdrawpaymentschooseConstrain.class);
                startActivity(intent);
            }
        });


        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        payli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   qrcode_popup();
                startActivity(new Intent(getApplicationContext(), Scanqrcode.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
               /* if (session.getbusinessprofile().equalsIgnoreCase("1")) {
                    Intent i = new Intent(Fastpayhome.this, Fastpay_barcode.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(Fastpayhome.this, Fastpaybusiness_setting.class);
                    startActivity(i);
                }*/

            }
        });
        saveplusricon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Fastpayhome.this, Saveplus_withdraw_statement.class);
                startActivity(i);
            }
        });
        tv_saveplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Fastpayhome.this, Saveplus_withdraw_statement.class);
                startActivity(i);
            }
        });
        tv_saveplusamount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Fastpayhome.this, Saveplus_withdraw_statement.class);
                startActivity(i);
            }
        });

        tv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isdown) {
                    isdown = false;
                    slideUp(downlayouts);
                } else {
                    isdown = true;
                    slideDown(downlayouts);
                }

            }
        });

    }


    private void qrcode_popup() {
        final Dialog dialog = new Dialog(Fastpayhome.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.qrcode_saveplus);

        final ImageView ivclose = dialog.findViewById(R.id.close);
        final ImageView qrcode = dialog.findViewById(R.id.qrcode);
        final TextView learnmore = dialog.findViewById(R.id.learnmore);
        final TextView saveplusid = dialog.findViewById(R.id.saveplusid);
        final TextView scanmycode = dialog.findViewById(R.id.scanmycode);
        final TextView scanmycode_qr = dialog.findViewById(R.id.scanmycode_qr);

        Glide.with(getApplicationContext()).load(codepath).into(qrcode);
        saveplusid.setText(getkey("saveplus_id") + saveplusids);

        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        learnmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
          }
        });

        scanmycode_qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(getApplicationContext(), Scanqrcode.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();


    }

    private void init_slidebanner() {
        final float MIN_SCALE = 0.50f;
        final float MIN_ALPHA = 0.3f;
        mPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);
        indicator.setViewPager(mPager);


        mPager.setPageTransformer(false, new ViewPager.PageTransformer() {
                    @Override
                    public void transformPage(View page, float position) {

                        if (position < -1) {
                            page.setScaleY(0.7f);
                            page.setAlpha(1);
                        } else if (position <= 1) {
                            float scaleFactor = Math.max(0.7f, 1 - Math.abs(position - 0.14285715f));
                            page.setScaleX(scaleFactor);

                            page.setScaleY(scaleFactor);
                            page.setAlpha(scaleFactor);
                        } else {
                            page.setScaleY(0.7f);
                            page.setAlpha(1);
                        }
                    }
                }
        );
//        mPager.setPageMargin(pageMargin);
//        mPager.setPadding(viewPagerPadding, 0, viewPagerPadding, 0);
        mPager.setClipToPadding(false);
        // set padding manually, the more you set the padding the more you see of prev & next page
        mPager.setPadding(10, 0, 10, 0);
        // sets a margin b/w individual pages to ensure that there is a gap b/w them
        mPager.setPageMargin(2);

//        mPager.setPageTransformer(false, new ZoomFadeTransformer(mPager.getPaddingLeft(), MIN_SCALE, MIN_ALPHA));
//        mPager.setPageTransformer(true, zoomOutTransformation);

        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);

        NUM_PAGES = IMAGES.length;

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    Slider slider = new Slider() {
        @Override
        public void onComplete(int position, Homepage_pojo.Booking_banner_arr booking_banner_arr) {
            if (position == 0) {

            }
        }
    };

    public void slideUp(View view) {
        view.setVisibility(View.GONE);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        view.setVisibility(View.VISIBLE);
    }


    class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);

        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:

                    Bundle bundle = new Bundle();
                    bundle.putString("fastpayhome", jsonpojo);
                    FragmentTabnew1 fm = new FragmentTabnew1();
                    fm.setArguments(bundle);
                    return fm;
               /* case 1:
                    Bundle bundle_2 = new Bundle();
                    bundle_2.putString("fastpayhome", jsonpojo);
                    FragmentTab2 fragmentTab2 = new FragmentTab2();
                    fragmentTab2.setArguments(bundle_2);
                    return fragmentTab2;
                case 2:
                    Bundle bundle_3 = new Bundle();
                    bundle_3.putString("fastpayhome", jsonpojo);
                    FragmentTab3 fragmentTab3 = new FragmentTab3();
                    fragmentTab3.setArguments(bundle_3);
                    return fragmentTab3;
*/
//                case 3:
//
//                    Bundle bundle_4 = new Bundle();
//                    bundle_4.putString("fastpayhome", jsonpojo);
//                    FragmentTab4 fragmentTab4 = new FragmentTab4();
//                    fragmentTab4.setArguments(bundle_4);
//                    return fragmentTab4;

            }
            return null;
        }

        @Override
        public int getCount() {
            return 1;
        }
    }

    private void fastpayhome(String Url) {

        final Dialog dialog = new Dialog(Fastpayhome.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);

        ServiceRequest mRequest = new ServiceRequest(Fastpayhome.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);

                dialog.dismiss();
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Type listType = new TypeToken<Fastpayhome_pojo>() {
                        }.getType();
                        homepage = new GsonBuilder().create().fromJson(object.toString(), listType);
                        Gson gson = new Gson();
                        Type type = new TypeToken<Fastpayhome_pojo>() {
                        }.getType();
                        jsonpojo = gson.toJson(homepage, type);
                        tv_balamount.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getCurrency_balance());
                        final HashMap<String, String> saveplsu = session.getsaveplus_balance();
                        String amount = saveplsu.get(SessionManager.KEY_SAVEPLUSBALANCE);
                        tv_saveplusamount.setText(homepage.getResponse().getCurrency() + " " + amount);

                        init_slidebanner();
                    } else {
                        Smessage = object.getString("response");
                        Alert(getkey("action_error"), Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialogtryagain mDialog = new PkDialogtryagain(Fastpayhome.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void UpdateProfilePopUp() {
        final Dialog dialog = new Dialog(Fastpayhome.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup1);
        CustomTextView Tv_Later = (CustomTextView) dialog.findViewById(R.id.later_tv);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_profilechange);
        CheckBox profile_checkbox = (CheckBox) dialog.findViewById(R.id.profile_checkbox);
        String checkBoxStatus = "No";
        if (profile_checkbox.isChecked()) {
            checkBoxStatus = "Yes";
        }
        TextView Profilechange = (TextView) dialog.findViewById(R.id.txt_updaste);
        Typeface tf = Typeface.createFromAsset(Fastpayhome.this.getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        final String finalCheckBoxStatus = checkBoxStatus;
        Tv_Later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                info = session.getUserBasicProfile();
                String basicUserProfileUpdateStatus = info.get(SessionManager.BASIC_PROFILE);
                if (basicUserProfileUpdateStatus.equalsIgnoreCase("0")) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    UpdateProfilePopUp2(finalCheckBoxStatus);
                } else {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    System.out.println("==============Muruga basicUserProfileUpdateStatus==========" + basicUserProfileUpdateStatus);
                    Intent intent = new Intent(Fastpayhome.this, NewCloudMoneyWithDrawHome.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_profile = new Intent(Fastpayhome.this, Fastpaypincheck.class);
                intent_profile.putExtra("frommenu", "1");
                startActivity(intent_profile);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void UpdateProfilePopUp2(final String needAssistance) {

        final Dialog dialog = new Dialog(Fastpayhome.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup2);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_tv);

        final CustomEdittext userName = (CustomEdittext) dialog.findViewById(R.id.name_title);
        final CustomEdittext userEmail = (CustomEdittext) dialog.findViewById(R.id.email_title);
        final CustomEdittext user_age_et = (CustomEdittext) dialog.findViewById(R.id.user_age_et);
        final ImageView closeimage = (ImageView) dialog.findViewById(R.id.img_close);
        CustomTextView Profilechange = (CustomTextView) dialog.findViewById(R.id.profile_text1);
        Typeface tf = Typeface.createFromAsset(Fastpayhome.this.getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        Spinner spinnerCustom = (Spinner) dialog.findViewById(R.id.gender_spinner1);
        final TextView Tv_city = (TextView) dialog.findViewById(R.id.ratecard_city_textview);
        // Spinner Drop down elements
        ArrayList<String> spinnerItems = new ArrayList<>();
        spinnerItems.add(getkey("seletc_gender"));
        spinnerItems.add(getkey("male"));
        spinnerItems.add(getkey("female"));
        spinnerItems.add(getkey("others"));

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(Fastpayhome.this, spinnerItems);
        spinnerCustom.setAdapter(customSpinnerAdapter);
        spinnerCustom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // close keyboard
                return false;
            }
        });
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                ConnectionDetector cd = new ConnectionDetector(Fastpayhome.this);
                Tv_city.setText(item);
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Tv_city.setText(getkey("seletc_gender"));
            }
        });
        user_age_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    user_age_et.setCursorVisible(false);
                    user_age_et.clearFocus();
                    InputMethodManager mgr = (InputMethodManager) Fastpayhome.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(user_age_et.getWindowToken(), 0);
                }
                return false;
            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userName.getText().toString().length() == 0) {
                    erroredit(userName, getkey("profile_label_alert_username"));
                } else if (!isValidEmail(userEmail.getText().toString().trim().replace(" ", ""))) {
                    erroredit(userEmail,getkey("profile_label_alert_email"));
                } else if (Tv_city.getText().toString().trim().equalsIgnoreCase("SELECT GENDER")) {
                    AlertError(getkey("alert_label_title"), getkey("profile_label_alert_validgender"));
                } else if (user_age_et.getText().toString().length() == 0) {
                    erroredit(user_age_et, getkey("profile_label_alert_validage"));
                } else if (user_age_et.getText().toString().trim().equalsIgnoreCase("0")) {
                    erroredit(user_age_et, getkey("profile_label_alert_validage"));
                } else {
                    boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        String username = userName.getText().toString().trim().replace(" ", "");
                        String userEmail1 = userEmail.getText().toString().trim().replace(" ", "");
                        String gender = Tv_city.getText().toString().trim().replace(" ", "");
                        String age = user_age_et.getText().toString().trim().replace(" ", "");
                        postrequestUpdateProfile(Iconstant.get_basic_profile_url, username, userEmail1, gender, needAssistance, age);
                    } else {
                        Alert(getkey("action_error"), getkey("alert_nointernet"));
                    }
                    dialog.dismiss();
                }
            }
        });
        closeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(user_age_et);
                dialog.dismiss();
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    //--------------Close KeyBoard Method-----------
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(Fastpayhome.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(Fastpayhome.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    private void postrequestUpdateProfile(String Url, String name, String email, String gender, String needAssistance, String Age) {

        final Dialog dialog = new Dialog(Fastpayhome.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("user_name", name);
        jsonParams.put("email", email);
        jsonParams.put("gender", gender);
        jsonParams.put("age", Age);
        jsonParams.put("assistance", needAssistance);

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(Fastpayhome.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);
                JSONObject object = null;
                dialog.dismiss();
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        session.setUserCompleteProfile("1");
                        Smessage = object.getString("response");
                        Alert1(getkey("action_success"), Smessage, Sstatus);

                    } else {

                        Smessage = object.getString("response");
                        Alert1(getkey("action_error"), Smessage, Sstatus);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert1(String title, String alert, final String status) {

        final PkDialog mDialog = new PkDialog(Fastpayhome.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);

        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")) {
                    mDialog.dismiss();
                } else {
                    mDialog.dismiss();
                }

            }
        });
        mDialog.show();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
