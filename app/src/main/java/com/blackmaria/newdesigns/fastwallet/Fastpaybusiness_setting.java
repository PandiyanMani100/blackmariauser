package com.blackmaria.newdesigns.fastwallet;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialogtryagain;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Fastpaybusiness_setting extends AppCompatActivity {

    private ImageView booking_back_imgeview;
    private TextView confirm, enterscountry,teleponecountry;
    private EditText edt_businessname, enteradress, entersecondadress, entercity, postalcode, contactperson, telepone;
    private CheckBox fastpaycheck;
    private String codes = "",telecodes = "", UserID = "";
LanguageDb mhelper;
    private CountryPicker picker,picker1;
    private SessionManager session;
    private HashMap<String, String> info;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fastpaybusiness_setting);
        mhelper = new LanguageDb(this);

        initView();
    }

    private void initView() {
        session = new SessionManager(this);
        info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        picker = CountryPicker.newInstance(getkey("select_country_lable"));
        picker1 = CountryPicker.newInstance(getkey("select_country_lable"));
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);
        confirm = findViewById(R.id.confirm);
        confirm.setText(getkey("confirm_lable"));
        edt_businessname = findViewById(R.id.edt_businessname);
        edt_businessname.setHint(getkey("enterbussinessortradingname"));
        teleponecountry = findViewById(R.id.teleponecountry);
        teleponecountry.setHint(getkey("code"));
        enteradress = findViewById(R.id.enteradress);
        enteradress.setHint(getkey("enterbusiness"));
        entersecondadress = findViewById(R.id.entersecondadress);
        entercity = findViewById(R.id.entercity);
        entercity.setHint(getkey("entercity"));
        postalcode = findViewById(R.id.postalcode);
        postalcode.setHint(getkey("postcode"));
        enterscountry = findViewById(R.id.enterscountry);
        enterscountry.setHint(getkey("country"));
        contactperson = findViewById(R.id.contactperson);
        contactperson.setHint(getkey("contactperson"));
        telepone = findViewById(R.id.telepone);
        telepone.setHint(getkey("telephonenumber"));
        fastpaycheck = findViewById(R.id.fastpaycheck);

        TextView fastpayname= (TextView)findViewById(R.id.fastpayname);
        fastpayname.setText(getkey("fastpay"));

        TextView agree_lable= (TextView)findViewById(R.id.agree_lable);
        agree_lable.setText(getkey("fastpayagree"));

        TextView tv_address= (TextView)findViewById(R.id.tv_address);
        tv_address.setText(getkey("address"));

        TextView setbusinessname= (TextView)findViewById(R.id.setbusinessname);
        setbusinessname.setText(getkey("setbusinessname"));

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                enterscountry.setText(name);
                codes = dialCode;
            }
        });
        picker1.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker1.dismiss();
                teleponecountry.setText(dialCode);
                telecodes = dialCode;
            }
        });

        enterscountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        teleponecountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker1.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validatemethosd();

            }
        });
    }

    private void validatemethosd() {
        if (edt_businessname.getText().length() == 0) {
            erroredit(edt_businessname, getkey("business_name_cnt_empty"));
        } else if (enteradress.getText().length() == 0) {
            erroredit(enteradress, getkey("addres_cnt_empty"));
        } else if (entercity.getText().length() == 0) {
            erroredit(entercity, getkey("city_cnt_empty"));
        } else if (postalcode.getText().length() == 0) {
            erroredit(postalcode, getkey("postal_cnt_empty"));
        } else if (enterscountry.getText().length() == 0) {
            Alert("Error", getkey("country_cnt_empty"));
        } else if (contactperson.getText().length() == 0) {
            erroredit(contactperson, getkey("contact_person_cnt_epty"));
        } else if (teleponecountry.getText().length() == 0) {
            Alert("Error", getkey("mobile_counrty_cnt_empty"));
        } else if (telepone.getText().length() == 0) {
            erroredit(telepone, getkey("moblr_cnt_empty"));
        } else if (!fastpaycheck.isChecked()) {
            Alert("Error", getkey("terms_condtion"));
        } else {
            submitlimitedregisteration(Iconstant.profilesafe);
//            startActivity(new Intent(Fastpaybusiness_setting.this, Fastpay_barcode.class));
        }
    }

    private void Alert(String title, String alert) {
        final PkDialogtryagain mDialog = new PkDialogtryagain(Fastpaybusiness_setting.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
//        getResources().getString(getResources().getIdentifier("footerLink1", "string", getPackageName()));
    }

    private void Alertsucess(String title, String alert, final String businessname) {
        final PkDialogtryagain mDialog = new PkDialogtryagain(Fastpaybusiness_setting.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                Intent i = new Intent(Fastpaybusiness_setting.this, Fastpay_barcode.class);
                startActivity(i);
                finish();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Fastpaybusiness_setting.this, Fastpay_barcode.class);
                startActivity(i);
                finish();
            }
        });
        mDialog.show();
//        getResources().getString(getResources().getIdentifier("footerLink1", "string", getPackageName()));
    }

    private void submitlimitedregisteration(String Url) {
        final Dialog dialog = new Dialog(Fastpaybusiness_setting.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("mode", "update");
        jsonParams.put("user_id", UserID);
        jsonParams.put("business_name", edt_businessname.getText().toString());
        jsonParams.put("address", enteradress.getText().toString() + "," + entersecondadress.getText().toString());
        jsonParams.put("city", entercity.getText().toString());
        jsonParams.put("state", entercity.getText().toString());
        jsonParams.put("pincode", postalcode.getText().toString());
        jsonParams.put("country", enterscountry.getText().toString());
        jsonParams.put("contact_person", contactperson.getText().toString());
        jsonParams.put("country_code", codes);
        jsonParams.put("phone_number", telepone.getText().toString());


        ServiceRequest mRequest = new ServiceRequest(Fastpaybusiness_setting.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "", SreferalStatus = "", errorStatus = "", userId = "", accountExit = "", errorMessage = "", Sotp_status = "", Sotp = "";

                try {
                    dialog.dismiss();
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        session.setbusinessname(object.getJSONObject("business_profile").getJSONObject("business").getString("business_name"));
                        Alertsucess("Sucess", object.getString("response"),object.getJSONObject("business_profile").getJSONObject("business").getString("business_name"));
                    } else {
                        Alert("Error", object.getString("message"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(Fastpaybusiness_setting.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
