package com.blackmaria.newdesigns.fastwallet.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.blackmaria.LanguageDb;
import com.blackmaria.pojo.Fastpayhome_pojo;
import com.blackmaria.R;
import com.blackmaria.WalletMoneyTransaction1;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import id.yuana.chart.pie.PieChartView;

public class FragmentTabnew1 extends Fragment {

    private PieChartView pieChartView;
    LinearLayout moneyoutlay,moneyinlay;
    LanguageDb mhelper;

    private int[] arrayofcolor = {R.color.white,
            R.color.firstround,
            R.color.secondround,
            R.color.thirdround,
            R.color.fourthround,
            R.color.white};

    private float[] arrayfloat = {400F, 400F, 400F, 400F, 400F, 400F};

    private Fastpayhome_pojo homepage;

    private TextView moneyin,moneyout,tv_monthlimitamount, tv_yearhighlights, tv_totalspentamount, tv_payments_value, tv_transfers_value, tv_withdraw_value;
    String jsonpojo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragmenttabnew1, container, false);

        if (getArguments().containsKey("fastpayhome")) {
             jsonpojo = getArguments().getString("fastpayhome");
            try {
                homepage = new Fastpayhome_pojo();
                Type listType = new TypeToken<Fastpayhome_pojo>() {
                }.getType();
                homepage = new GsonBuilder().create().fromJson(jsonpojo, listType);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mhelper = new LanguageDb(getActivity());
        initView(rootView);

        return rootView;
    }

    private void initView(View rootView) {
        pieChartView = rootView.findViewById(R.id.pieChart);
        pieChartView.setCenterColor(R.color.white);
        pieChartView.setDataPoints(arrayfloat);
        pieChartView.setSliceColor(arrayofcolor);

        moneyinlay= rootView.findViewById(R.id.moneyinlay);
        moneyoutlay= rootView.findViewById(R.id.moneyoutlay);

        tv_monthlimitamount = rootView.findViewById(R.id.tv_monthlimitamount);
        tv_yearhighlights = rootView.findViewById(R.id.tv_yearhighlights);
        tv_totalspentamount = rootView.findViewById(R.id.tv_totalspentamount);
        TextView tv_totalspent = rootView.findViewById(R.id.tv_totalspent);
        tv_totalspent.setText(getkey("balance_new"));

        TextView moin = rootView.findViewById(R.id.moin);
        moin.setText(getkey("money_in"));

        TextView monout = rootView.findViewById(R.id.monout);
        monout.setText(getkey("money_out"));


        tv_payments_value = rootView.findViewById(R.id.tv_payments_value);
        tv_transfers_value = rootView.findViewById(R.id.tv_transfers_value);
        tv_withdraw_value = rootView.findViewById(R.id.tv_withdraw_value);

        moneyin = rootView.findViewById(R.id.moneyin);
        moneyout = rootView.findViewById(R.id.moneyout);

        moneyin.setText(homepage.getResponse().getCurrency()+" "+homepage.getResponse().getTotal().getWallet_credit());

        moneyout.setText(homepage.getResponse().getCurrency()+" "+homepage.getResponse().getTotal().getWallet_spend());

        tv_monthlimitamount.setText( homepage.getResponse().getCurrency_balance());
        tv_yearhighlights.setText(homepage.getResponse().getText());
        tv_totalspentamount.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal().getWallet_spend());
        tv_payments_value.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal().getPayment());
        tv_transfers_value.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal().getTransfer());
        tv_withdraw_value.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getTotal().getWithdrawal());

        moneyinlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), WalletMoneyTransaction1.class);
                intent.putExtra("type", "credit");
                startActivity(intent);


            }
        });

        moneyoutlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), WalletMoneyTransaction1.class);
                intent.putExtra("type", "debit");
                startActivity(intent);
            }
        });


    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}