package com.blackmaria.newdesigns.fastwallet;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.blackmaria.Navigation_new;
import com.blackmaria.R;

import org.json.JSONException;
import org.json.JSONObject;

public class Fastpay_paymentsucess extends AppCompatActivity {

    private TextView close, tv_amount, tv_payto, tv_merchantid, tv_date, tv_time, tv_transationno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fastpay_paymentsucess);

        initView();
        clicklistner();
    }

    private void initView() {
        close = findViewById(R.id.close);
        tv_amount = findViewById(R.id.tv_amount);
        tv_payto = findViewById(R.id.tv_payto);
        tv_merchantid = findViewById(R.id.tv_merchantid);
        tv_date = findViewById(R.id.tv_date);
        tv_time = findViewById(R.id.tv_time);
        tv_transationno = findViewById(R.id.tv_transationno);

        if (getIntent().hasExtra("json")) {
            try {
                JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("json"));
                tv_amount.setText(jsonObject.getJSONObject("response").getString("currency")+" "+jsonObject.getJSONObject("response").getString("transfer_amount"));
                tv_payto.setText(getResources().getString(R.string.pay_to)+jsonObject.getString("pay_to"));
                tv_merchantid.setText(getResources().getString(R.string.merchanid)+jsonObject.getString("receiver_merchant_id"));
                tv_date.setText( getResources().getString(R.string.date)+" : "+jsonObject.getString("date"));
                tv_time.setText(getResources().getString(R.string.time)+" : "+jsonObject.getString("time"));
                tv_transationno.setText(getResources().getString(R.string.transaction_num)+jsonObject.getString("transaction_number"));
                System.out.println("" + jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void clicklistner() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Fastpay_paymentsucess.this, Navigation_new.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
