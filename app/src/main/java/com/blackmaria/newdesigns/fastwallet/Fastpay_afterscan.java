package com.blackmaria.newdesigns.fastwallet;

import android.content.Intent;
import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.blackmaria.Navigation_new;
import com.blackmaria.newdesigns.Fastpaytransfer_pincheck;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Fastpay_afterscan extends AppCompatActivity {

    private TextView tv_paynow, tv_name, tv_category, tv_merchantid, tv_merchantaddressvalue, tv_telephone, tv_manager, cancel;
    private RatingBar simpleRatingBar;
    private EditText amount;
    private ImageView booking_back_imgeview;
    private String receiverid = "", UserID = "";
    private SessionManager sessionManager;
    private HashMap<String, String> info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fastpay_afterscan);

        initView();
        clicklistener();
    }

    private void initView() {
        sessionManager = new SessionManager(this);
        info = sessionManager.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);
        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_paynow = findViewById(R.id.tv_paynow);
        tv_name = findViewById(R.id.tv_name);
        tv_category = findViewById(R.id.tv_category);
        tv_merchantid = findViewById(R.id.tv_merchantid);
        tv_merchantaddressvalue = findViewById(R.id.tv_merchantaddressvalue);
        tv_telephone = findViewById(R.id.tv_telephone);
        tv_manager = findViewById(R.id.tv_manager);
        cancel = findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Fastpay_afterscan.this, Navigation_new.class));
                finish();
            }
        });
        simpleRatingBar = findViewById(R.id.simpleRatingBar);
        simpleRatingBar.setRating(4f);
        amount = findViewById(R.id.amount);

        if (getIntent().hasExtra("json")) {
            try {
                JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("json"));
                JSONObject businessobj = jsonObject.getJSONObject("response").getJSONObject("business_profile");
                JSONObject address = jsonObject.getJSONObject("response").getJSONObject("business_profile").getJSONObject("address");
                tv_name.setText(businessobj.getString("business_name"));
                tv_merchantid.setText(getResources().getString(R.string.merchanid) + businessobj.getString("merchant_id"));
                tv_merchantaddressvalue.setText(address.getString("address") + "\n" + address.getString("state") + "\n" + address.getString("country") + ", " + address.getString("city") +" - "+ address.getString("pincode"));
                tv_telephone.setText(getResources().getString(R.string.tels) + businessobj.getString("country_code") + "" + businessobj.getString("phone_number"));
                tv_manager.setText(getResources().getString(R.string.managers) + businessobj.getString("contact_person"));
                receiverid = businessobj.getString("user_id");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(Fastpay_afterscan.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    private void clicklistener() {
        tv_paynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (amount.getText().length() == 0) {
                    erroredit(amount, getResources().getString(R.string.amount_cnt_empty));
                } else if(amount.getText().toString().equals("0")){
                    erroredit(amount, getResources().getString(R.string.kindly_enter_valid_amount));
                }else {
                    Intent i = new Intent(Fastpay_afterscan.this, Fastpaytransfer_pincheck.class);
                    i.putExtra("receiver_id", receiverid);
                    i.putExtra("received_amount", amount.getText().toString());
                    startActivity(i);
                }
            }
        });
    }


}
