package com.blackmaria.newdesigns;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.newdesigns.fastwallet.Fastpay_paymentsucess;
import com.blackmaria.pojo.Saveplustransfer;
import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialogtryagain;
import com.devspark.appmsg.AppMsg;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Fastpaytransfer_pincheck extends AppCompatActivity {

    private TextView img2, img1, img3, img5, img4, img6, img8, img7, img9, img0, img_star, img_hash, confirm;
    private EditText edt_email;
    private LinearLayout ly_forgetpin;
    private ImageView booking_back_imgeview;
    private String number = "";
    int[] imageViews = {R.id.img1, R.id.img2, R.id.img3, R.id.img4, R.id.img5, R.id.img6, R.id.img7, R.id.img8, R.id.img9, R.id.img0};
    private float TIME_DELAY = 2;
    private float lastY = 0;
    private SessionManager session;
    private HashMap<String, String> info;
    private boolean isfromsaveplus_scan = false, isfromsaveplussendmoney = false, isfrommenu = false;

    private String receiver_id = "", received_amount = "", UserID = "";
    private Saveplustransfer homepage;

    private int countcheck_wrongpattern = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fastpaytransfer_pincheck);

        init();
        clicklistener();
    }


    private void init() {
        session = new SessionManager(this);
        info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        ly_forgetpin = findViewById(R.id.ly_forgetpin);
        edt_email = findViewById(R.id.edt_email);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);
        confirm = findViewById(R.id.confirm);
        img2 = findViewById(R.id.img2);
        img1 = findViewById(R.id.img1);
        img3 = findViewById(R.id.img3);
        img5 = findViewById(R.id.img5);
        img4 = findViewById(R.id.img4);
        img6 = findViewById(R.id.img6);
        img8 = findViewById(R.id.img8);
        img7 = findViewById(R.id.img7);
        img9 = findViewById(R.id.img9);
        img0 = findViewById(R.id.img0);
        img_star = findViewById(R.id.img_star);
        img_hash = findViewById(R.id.img_hash);

        if (getIntent().hasExtra("receiver_id")) {
            receiver_id = getIntent().getStringExtra("receiver_id");
            received_amount = getIntent().getStringExtra("received_amount");
        }
    }

    private void clicklistener() {

        edt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
            }
        });
        edt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
//                if (s.length() == 6) {
//                    confirm.setBackground(getResources().getDrawable(R.drawable.green_curve));
//                } else {
//                    confirm.setBackground(getResources().getDrawable(R.drawable.grey_curve));
//                }
//                String sSecurePin = session.getSecurePin();
//                if (sSecurePin.equalsIgnoreCase(s.toString())) {
//                    confirm.setVisibility(View.VISIBLE);
//                }else{
//                    confirm.setVisibility(View.INVISIBLE);
//                }
                if (s.length() == 6) {
                    confirm.setBackground(getResources().getDrawable(R.drawable.green_curve));
                } else {
                    confirm.setBackground(getResources().getDrawable(R.drawable.grey_curve));
                }
            }
        });
        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "1";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());

                setbackground(img1.getId());
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "2";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img2.getId());
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "3";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img3.getId());
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "4";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img4.getId());
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "5";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img5.getId());
            }
        });

        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "6";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img6.getId());
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "7";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img7.getId());
            }
        });
        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "8";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img8.getId());
            }
        });
        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "9";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img9.getId());
            }
        });

        img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "0";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img0.getId());
            }
        });

        img_hash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String SMobileNo = edt_email.getText().toString();
                if (SMobileNo != null && SMobileNo.length() > 0) {
                    edt_email.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                    edt_email.setSelection(edt_email.getText().length());
                    number = edt_email.getText().toString();
                }

                img_hash.setBackground(getResources().getDrawable(R.drawable.round_green));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img_hash.setBackground(getResources().getDrawable(R.drawable.round_white));
                    }
                }, 100);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.getText().toString().length() == 0) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_pincode));
                } else if (edt_email.getText().toString().trim().length() != 6) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_pincodes));
                } else {
                    ConnectionDetector cd = new ConnectionDetector(Fastpaytransfer_pincheck.this);
                    boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {

                        HashMap<String, String> user = session.getUserDetails();
                        String sSecurePin = session.getSecurePin();
                        if (sSecurePin.equalsIgnoreCase(edt_email.getText().toString().trim())) {
                            fastpaytransfer(Iconstant.fastpay_amounttransfer);
                        } else {
                            number = "";
                            if (countcheck_wrongpattern == 1) {
                                countcheck_wrongpattern++;
                                edt_email.setText("");
                                edt_email.getText().clear();
                                Alertwrongpattern(getResources().getString(R.string.action_error), getResources().getString(R.string.wornspins));
                            } else if (countcheck_wrongpattern == 2) {
                                countcheck_wrongpattern++;
                                edt_email.setText("");
                                edt_email.getText().clear();
                                Alertwrongpattern(getResources().getString(R.string.action_error), getResources().getString(R.string.wrongpin_firstime));
                            } else if (countcheck_wrongpattern == 3) {
                                countcheck_wrongpattern = 1;
                                edt_email.setText("");
                                edt_email.getText().clear();
                                closepopupinseconds(getResources().getString(R.string.wron_pin), getResources().getString(R.string.plse_enter_untlin_last));
                            }


//                            Alert(getResources().getString(R.string.action_error), "You have entered wrong pin");
                        }

                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }
                }
            }
        });

        ly_forgetpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Fastpaytransfer_pincheck.this, Forgetpin.class);
                startActivity(i);
            }
        });
    }

    private void closepopupinseconds(String title, final String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Fastpaytransfer_pincheck.this);
        mDialog.setDialogTitle(title);
        mDialog.setCancelble(false);
        mDialog.setCancelOnTouchOutside(false);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mDialog.setDialogMessage(" " + millisUntilFinished / 1000 + "\n \n \n " + alert);
            }

            public void onFinish() {
                mDialog.dismiss();
            }

        }.start();


        mDialog.show();
    }

    private void Alertwrongpattern(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Fastpaytransfer_pincheck.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getResources().getString(R.string.forget_pattern), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Fastpaytransfer_pincheck.this, Pin_activitynew.class);
                startActivity(i);
                finish();
            }
        });
        mDialog.show();
    }

    private void setbackground(int image) {
        for (int i = 0; i <= imageViews.length - 1; i++) {
            final TextView img = findViewById(imageViews[i]);
            if (image == imageViews[i]) {
                img.setBackground(getResources().getDrawable(R.drawable.round_green));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img.setBackground(getResources().getDrawable(R.drawable.round_white));
                    }
                }, 100);
            } else {
                img.setBackground(getResources().getDrawable(R.drawable.round_white));
            }
        }
    }


    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(Fastpaytransfer_pincheck.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    private void fastpaytransfer(String Url) {
        final Dialog dialog = new Dialog(Fastpaytransfer_pincheck.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("receiver_id", receiver_id);
        jsonParams.put("user_id", UserID);
        jsonParams.put("transfer_amount", received_amount);

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(Fastpaytransfer_pincheck.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";

                dialog.dismiss();
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        Intent i = new Intent(Fastpaytransfer_pincheck.this, Fastpay_paymentsucess.class);
                        i.putExtra("json", object.toString());
                        startActivity(i);
                        finish();

                    } else {
                        Smessage = object.getString("response");
                        Alert(getResources().getString(R.string.action_error), Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void Alert(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Fastpaytransfer_pincheck.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
}
