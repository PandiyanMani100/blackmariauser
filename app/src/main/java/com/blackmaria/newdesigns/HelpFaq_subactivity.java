package com.blackmaria.newdesigns;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.adapter.AnimatedExpandableListView;
import com.blackmaria.newdesigns.adapter.ExpandableListAdapter;
import com.blackmaria.pojo.Helpfaq_pojo;
import com.blackmaria.pojo.faqhelp_pojo;
import com.blackmaria.R;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HelpFaq_subactivity extends AppCompatActivity {

    private LinearLayout helpquestions;
    private ImageView img_back;
    private ArrayList<Helpfaq_pojo> helpfaq_pojos = new ArrayList<>();


    //try

    ExpandableListAdapter listAdapter;
    AnimatedExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private boolean isquestionnothere = false;
    private TextView subtitle, message;
    LanguageDb mhelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_faq_subactivity);
        mhelper = new LanguageDb(this);
        initView();
    }


    private void initView() {
        img_back = findViewById(R.id.img_back);
        subtitle = findViewById(R.id.subtitle);
       TextView sssdf= findViewById(R.id.sssdf);
        sssdf.setText(getkey("helpfaq"));
        message = findViewById(R.id.message);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        helpquestions = findViewById(R.id.helpquestions);
        expListView = (AnimatedExpandableListView) findViewById(R.id.lvExp);
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        listDataHeader.clear();
        listDataChild.clear();

        if (getIntent().hasExtra("json")) {
            try {
                String selectedtitle = getIntent().getStringExtra("name");
                subtitle.setText(selectedtitle);
                JSONObject jsonobj = new JSONObject(getIntent().getStringExtra("json"));
                Type listType = new TypeToken<faqhelp_pojo>() {
                }.getType();
                faqhelp_pojo homepage = new GsonBuilder().create().fromJson(jsonobj.toString(), listType);

                for (int i = 0; i <= homepage.getResponse().size() - 1; i++) {
                    if (selectedtitle.equalsIgnoreCase(homepage.getResponse().get(i).getName())) {
                        if (homepage.getResponse().get(i).getQuestion().size() > 0) {
                            helpquestions.setVisibility(View.VISIBLE);
                            message.setVisibility(View.GONE);

                            isquestionnothere = false;
                            for (int j = 0; j <= homepage.getResponse().get(i).getQuestion().size() - 1; j++) {
                                listDataHeader.add(homepage.getResponse().get(i).getQuestion().get(j).getQuestion());
                                ArrayList<String> listchild = new ArrayList<>();
                                listchild.add(homepage.getResponse().get(i).getQuestion().get(j).getAnswer());
                                listDataChild.put(listDataHeader.get(j), listchild);
                            }

                        } else {
                            message.setText(homepage.getResponse().get(i).getContent());
                            message.setVisibility(View.VISIBLE);
                            helpquestions.setVisibility(View.GONE);
                            isquestionnothere = true;
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        helpfaq_pojos.clear();


        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                if (expListView.isGroupExpanded(groupPosition)) {
                    expListView.collapseGroupWithAnimation(groupPosition);
                } else {
                    expListView.expandGroupWithAnimation(groupPosition);
                }
                return true;
            }
        });

//        // Listview Group expanded listener
//        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Expanded",
//                        Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        // Listview Group collasped listener
//        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//
//            @Override
//            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Collapsed",
//                        Toast.LENGTH_SHORT).show();
//
//            }
//        });


        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });
    }


    private void addviews() {
        helpquestions.removeAllViews();
        for (int i = 0; i <= helpfaq_pojos.size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.helpchildviewconstrain, null);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(helpfaq_pojos.get(i).getTitle());
            ImageView downicon = view.findViewById(R.id.downicon);
            final TextView titlevalue = view.findViewById(R.id.titlevalue);
            titlevalue.setText(helpfaq_pojos.get(i).getValue());
            if (helpfaq_pojos.get(i).isIsopen()) {

//                titlevalue.setVisibility(View.VISIBLE);
//                titlevalue.animate()
//                        .translationY(titlevalue.getHeight())
//                        .setDuration(500);
                titlevalue.animate()
                        .translationY(titlevalue.getHeight())
//                        .alpha(0.0f)
                        .setDuration(300)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                titlevalue.setVisibility(View.VISIBLE);
                            }
                        });
            } else {
//                titlevalue.setVisibility(View.GONE);
//                titlevalue.animate()
//                        .translationY(0)
//                        .setDuration(500);
                titlevalue.animate()
                        .translationY(titlevalue.getHeight())
//                        .alpha(0.0f)
                        .setDuration(300)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                titlevalue.setVisibility(View.GONE);
                            }
                        });
            }


            final int finalI = i;
            downicon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    for (int k = 0; k <= helpfaq_pojos.size() - 1; k++) {
                        Helpfaq_pojo pojo = new Helpfaq_pojo();
                        pojo.setTitle(helpfaq_pojos.get(k).getTitle());
                        pojo.setValue(helpfaq_pojos.get(k).getValue());
                        if (k == finalI) {
                            pojo.setIsopen(true);
                        } else {
                            pojo.setIsopen(false);
                        }

                        helpfaq_pojos.set(k, pojo);
                    }

                    addviews();
                }
            });
            helpquestions.addView(view);
        }
    }

    private void prepareListData() {

        // Adding child data
        listDataHeader.add("Top 250");
        listDataHeader.add("Now Showing");
        listDataHeader.add("Coming Soon..");

        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");
        top250.add("The Godfather: Part II");
        top250.add("Pulp Fiction");
        top250.add("The Good, the Bad and the Ugly");
        top250.add("The Dark Knight");
        top250.add("12 Angry Men");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("The Conjuring");
        nowShowing.add("Despicable Me 2");
        nowShowing.add("Turbo");
        nowShowing.add("Grown Ups 2");
        nowShowing.add("Red 2");
        nowShowing.add("The Wolverine");

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add("2 Guns");
        comingSoon.add("The Smurfs 2");
        comingSoon.add("The Spectacular Now");
        comingSoon.add("The Canyons");
        comingSoon.add("Europa Report");

        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nowShowing);
        listDataChild.put(listDataHeader.get(2), comingSoon);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}
