package com.blackmaria.newdesigns;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.pojo.Singlefriendview;
import com.blackmaria.R;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class Refer_friend_statement extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout friendslistview;
    private ImageView img_back;
    private SessionManager sessionManager;
    private String UserID = "";
    private TextView month, year;
    private ImageView leftarraow, rightarraow;
    private ArrayList<Singlefriendview> refer_friendslists = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_friend_statement);
        initView();
    }

    private void initView() {
        month = findViewById(R.id.month);
        year = findViewById(R.id.year);
        Calendar calendar = Calendar.getInstance();
        int years = calendar.get(Calendar.YEAR);

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        String month_name = month_date.format(cal.getTime());
        month.setText(month_name);
        year.setText(String.valueOf(years));
        leftarraow = findViewById(R.id.leftarraow);
        rightarraow = findViewById(R.id.rightarraow);
        friendslistview = findViewById(R.id.friendslistview);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        sessionManager = new SessionManager(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);


        if (getIntent().hasExtra("friendid")) {
            String id = getIntent().getStringExtra("friendid");
            getfriendslist(Iconstant.single_friendsview, id);
        }

    }

    private void getfriendslist(String Url, String id) {
        final Dialog dialog = new Dialog(Refer_friend_statement.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("friend_id", id);
        jsonParams.put("month", String.valueOf(month + 1));
        jsonParams.put("year", String.valueOf(year));
        ServiceRequest mRequest = new ServiceRequest(Refer_friend_statement.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------displayInvite_Request Response----------------" + response);

                try {

                    JSONObject object = new JSONObject(response);
                    String Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        refer_friendslists.clear();
                        JSONArray friedsarray = object.getJSONObject("response").getJSONArray("friend_list");
                        for (int i = 0; i <= friedsarray.length() - 1; i++) {
                            Singlefriendview refer_friendslist = new Singlefriendview();
                            refer_friendslist.setUser_name(friedsarray.getJSONObject(i).getString("user_name"));
                            refer_friendslist.setAmount(friedsarray.getJSONObject(i).getString("amount"));
                            refer_friendslist.setReference_date(friedsarray.getJSONObject(i).getString("reference_date"));
                            refer_friendslist.setImage(friedsarray.getJSONObject(i).getString("image"));
                            refer_friendslists.add(refer_friendslist);
                        }
                        friendslist(refer_friendslists);
                    } else {
                        String message = object.getString("response");
                        Alert(getResources().getString(R.string.action_ok), message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Refer_friend_statement.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setDialogUpper(false);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }


    private void friendslist(ArrayList<Singlefriendview> refer_friendslists) {
        try {
            friendslistview.removeAllViews();

            for (int i = 0; i <= refer_friendslists.size() - 1; i++) {
                View view = getLayoutInflater().inflate(R.layout.refered_friendslist_singleadapter, null);

                RoundedImageView imageView = view.findViewById(R.id.userimage);
                TextView username = view.findViewById(R.id.username);
                TextView signupdate = view.findViewById(R.id.signupdate);
                username.setText(refer_friendslists.get(i).getReference_date());
                signupdate.setText(sessionManager.getCurrency()+""+String.valueOf(refer_friendslists.get(i).getAmount()));
                Picasso.with(getApplicationContext()).load(String.valueOf(refer_friendslists.get(i).getImage())).placeholder(R.drawable.usericon_getpaid).error(R.drawable.usericon_getpaid).into(imageView);

                friendslistview.addView(view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
        }
    }

}
