package com.blackmaria.newdesigns.sos;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.widgets.PkDialogtryagain;

import java.util.HashMap;

public class Searchpolice extends AppCompatActivity {

    private SessionManager sessionManager;
    private String Userid = "";
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    private ImageView close, callicon;
    private TextView tv_countreduce;

    private ListView listsos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchpolice);

        intiView();
    }

    private void intiView() {
        sessionManager = new SessionManager(this);
        cd = new ConnectionDetector(this);
        isInternetPresent = cd.isConnectingToInternet();
        HashMap<String, String> user = sessionManager.getUserDetails();
        Userid = user.get(SessionManager.KEY_USERID);

        close = findViewById(R.id.close);
        callicon = findViewById(R.id.callicon);
        tv_countreduce = findViewById(R.id.tv_countreduce);
        listsos = findViewById(R.id.listsos);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        callicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callicon.setVisibility(View.INVISIBLE);
                Popupalert();
            }
        });
    }


    private void Popupalert() {
        final Dialog dialog = new Dialog(Searchpolice.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sos_policedialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView image_close = dialog.findViewById(R.id.image_close);
        LinearLayout linearpolice = dialog.findViewById(R.id.linearpolice);
        LinearLayout linearmedic = dialog.findViewById(R.id.linearmedic);
        LinearLayout linearfire = dialog.findViewById(R.id.linearfire);
        LinearLayout linearchat = dialog.findViewById(R.id.linearchat);

        linearpolice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callicon.setVisibility(View.VISIBLE);
                if (isInternetPresent) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getPolice()));
                    startActivity(intent);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }

            }
        });

        linearmedic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callicon.setVisibility(View.VISIBLE);
                if (isInternetPresent) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getAmbulance()));
                    startActivity(intent);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }

            }
        });

        linearfire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callicon.setVisibility(View.VISIBLE);
                if (isInternetPresent) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getFire()));
                    startActivity(intent);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }

            }
        });

        linearchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.no_service));
            }
        });

        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callicon.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void Alert(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Searchpolice.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

}
