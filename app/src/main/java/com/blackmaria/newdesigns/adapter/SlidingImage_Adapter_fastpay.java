package com.blackmaria.newdesigns.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.blackmaria.InterFace.Slider;
import com.blackmaria.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SlidingImage_Adapter_fastpay extends PagerAdapter {


    private ArrayList<Integer> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    private Slider slider;

    public SlidingImage_Adapter_fastpay(Slider slider, Context context, ArrayList<Integer> imagesArray) {
        this.slider = slider;
        this.context = context;
        this.IMAGES=imagesArray;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.fastpay_custombanner_layout, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.iv_car);

        Glide.with(view.getContext()).load(IMAGES.get(position)).into(imageView);

        view.addView(imageLayout, 0);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                slider.onComplete(position,IMAGES.get(position));
            }
        });


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}