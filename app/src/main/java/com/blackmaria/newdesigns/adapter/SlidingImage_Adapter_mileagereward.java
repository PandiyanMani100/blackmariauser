package com.blackmaria.newdesigns.adapter;

import android.content.Context;
import android.os.Parcelable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.blackmaria.LanguageDb;
import com.blackmaria.pojo.MilesRewardsPojo;
import com.blackmaria.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class SlidingImage_Adapter_mileagereward extends PagerAdapter {


    private ArrayList<MilesRewardsPojo> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    redeemRewards redeemrewards;
    LanguageDb mhelper;


    public SlidingImage_Adapter_mileagereward(Context context, ArrayList<MilesRewardsPojo> imagesArray, redeemRewards redeemrewards) {
        this.context = context;
        mhelper = new LanguageDb(context);
        this.IMAGES = imagesArray;
        inflater = LayoutInflater.from(context);
        this.redeemrewards = redeemrewards;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.milagereward_adapterlayout, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.rewardimage);
        final TextView price = imageLayout.findViewById(R.id.price);
        final TextView giftname = imageLayout.findViewById(R.id.giftname);
        final TextView gifttype = imageLayout.findViewById(R.id.gifttype);
        final TextView validity = imageLayout.findViewById(R.id.validity);
        final TextView redeem = imageLayout.findViewById(R.id.redeem);
        redeem.setText(getkey("redeem"));


        if(IMAGES.get(position).getReward_type().equalsIgnoreCase("coupon"))
        {
            giftname.setText(getkey("coupon_amount")+IMAGES.get(position).getCoupon_value());
        }
        else
            {

            giftname.setText(getkey("gift_name")+IMAGES.get(position).getGift_name());

        }
        gifttype.setText(getkey("reward_type") + IMAGES.get(position).getReward_type());
        validity.setText(getkey("validity") + IMAGES.get(position).getValid_to_second());
        price.setText(getkey("price") + IMAGES.get(position).getMiles_distance());

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_group_316)
                .error(R.drawable.ic_group_316)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(IMAGES.get(position).getGift_image()).apply(options).into(imageView);





        view.addView(imageLayout, 0);

        redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                redeemrewards.onClickRedeemList(position);
            }
        });


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public interface redeemRewards {
        void onClickRedeemList(int postion);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}