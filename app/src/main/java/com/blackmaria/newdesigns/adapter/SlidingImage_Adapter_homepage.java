package com.blackmaria.newdesigns.adapter;

import android.content.Context;
import android.os.Parcelable;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.InterFace.Slider;
import com.blackmaria.pojo.Homepage_pojo;
import com.blackmaria.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class SlidingImage_Adapter_homepage  extends PagerAdapter {


    private ArrayList<Homepage_pojo.Booking_banner_arr> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    private Slider slider;


    public SlidingImage_Adapter_homepage(Slider slider,Context context,ArrayList<Homepage_pojo.Booking_banner_arr> IMAGES) {
        this.slider = slider;
        this.context = context;
        this.IMAGES=IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.homepage_custombanner_layout, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.iv_car);

        final FrameLayout backcs = (FrameLayout) imageLayout
                .findViewById(R.id.backcs);


        final TextView username = (TextView) imageLayout
                .findViewById(R.id.username);

        RequestOptions options = new RequestOptions();
        options.centerCrop();
        if(position == (IMAGES.size()-1))
        {
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                backcs.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.blackcarddrwable) );
            } else {
                backcs.setBackground(ContextCompat.getDrawable(context, R.drawable.blackcarddrwable));
            }
        }
        else
        {
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                backcs.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.white_curved_bg) );
            } else {
                backcs.setBackground(ContextCompat.getDrawable(context, R.drawable.white_curved_bg));
            }
        }


        if(position == 0)
        {
            username.setVisibility(View.VISIBLE);
        }
        else
        {
            username.setVisibility(View.GONE);
        }

        if(position == 0)
        {
            Glide.with(view.getContext()).load(IMAGES.get(position).getFile())
                    .into(imageView);
           // imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        else
        {
            Glide.with(view.getContext()).load(IMAGES.get(position).getFile())
                    .apply(options)
                    .into(imageView);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        }


        view.addView(imageLayout, 0);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slider.onComplete(position,IMAGES.get(position));
            }
        });

        ImageView Rl_drawer = (ImageView) imageLayout.findViewById(R.id.loading);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);




        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}