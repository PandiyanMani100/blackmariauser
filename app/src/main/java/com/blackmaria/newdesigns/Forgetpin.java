package com.blackmaria.newdesigns;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.R;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Locale;

import static com.blackmaria.HomePage.isValidEmail;

public class Forgetpin extends AppCompatActivity {

    private EditText edt_email;
    private TextView submit,txt_country_code;
    Button closeall;
    RelativeLayout relativeLayout2;
    private ImageView booking_back_imgeview,flag;
    CountryPicker picker;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpin);
        mhelper = new LanguageDb(this);
        picker = CountryPicker.newInstance(getkey("select_country_lable"));


        init();
    }

    private void init() {
        edt_email = findViewById(R.id.edt_email);
        edt_email.setHint(getkey("email"));
        submit = findViewById(R.id.submit);
        submit.setText(getkey("submit"));
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);
        closeall= findViewById(R.id.closeall);

        TextView forrgotpin = findViewById(R.id.forrgotpin);
        forrgotpin.setText(getkey("forgot_pinc"));

        EditText edit_mobilenumber= findViewById(R.id.edit_mobilenumber);
        edit_mobilenumber.setHint(getkey("edit_mobilenumber"));

        relativeLayout2= findViewById(R.id.relativeLayout2);
        flag= findViewById(R.id.flag);
        txt_country_code= findViewById(R.id.txt_country_code);
        ImageView Rl_drawer = (ImageView) findViewById(R.id.loading);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);

        closeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        relativeLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        flag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                txt_country_code.setText(dialCode);
                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                flag.setImageResource(getResId(drawableName));
                CloseKeyBoard();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edt_email.getText().toString().length() == 0) {
                    erroredit(edt_email, getkey("profile_label_email_age"));
                } else if (!isValidEmail(edt_email.getText().toString().trim().replace(" ", ""))) {
                    erroredit(edt_email, getkey("profile_label_alert_email"));
                } else {
                    checkemail(Iconstant.forgetpin, edt_email.getText().toString());
                }
            }
        });
    }


    private int getResId(String drawableName) {

        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }

    private void CloseKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

       /* View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }*/
    }

    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(Forgetpin.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    //----------------------------------Mobile number change----------------------------------------------
    private void checkemail(String Url, String email) {

        final Dialog dialog = new Dialog(Forgetpin.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", email);


        ServiceRequest mRequest = new ServiceRequest(Forgetpin.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        Alertsucess(getkey("pushnotification_alert_label_ride_arrived_success"), Smessage);
                    } else {
                        Alert(getkey("action_error"), Smessage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialog.dismiss();
                }

                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Forgetpin.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void Alertsucess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Forgetpin.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                onBackPressed();
                finish();
            }
        });
        mDialog.show();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
