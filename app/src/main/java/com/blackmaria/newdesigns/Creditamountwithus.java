package com.blackmaria.newdesigns;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.Navigation_new;
import com.blackmaria.R;

public class Creditamountwithus extends AppCompatActivity {

    private TextView amount, RL_close_account;
    private LinearLayout payments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creditamountwithus);

        initView();
    }

    private void initView() {
        RL_close_account = findViewById(R.id.RL_close_account);
        amount = findViewById(R.id.amount);
        payments = findViewById(R.id.payments);
        RL_close_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Creditamountwithus.this, Navigation_new.class));
                finish();
            }
        });

    }
}
