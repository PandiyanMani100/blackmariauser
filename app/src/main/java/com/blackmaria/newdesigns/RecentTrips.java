package com.blackmaria.newdesigns;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.pojo.Recentrides;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class RecentTrips extends AppCompatActivity {

    private LinearLayout mainlayout_recenttrips;
    private ImageView booking_back_imgeview;
    private SessionManager sessionManager;
    private Recentrides recentrides;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_trips);
        mhelper= new LanguageDb(this);
        TextView tv_recentrips = (TextView) findViewById(R.id.tv_recentrips);
        tv_recentrips.setText(getkey("recent_trips"));

        init();
    }

    private void init() {
        sessionManager = new SessionManager(this);
        mainlayout_recenttrips = findViewById(R.id.mainlayout_recenttrips);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);

        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent returnIntent = new Intent();
//                returnIntent.putExtra("Selected_Location", ScouponSource);
//                setResult(RESULT_OK, returnIntent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        HashMap<String, String> info = sessionManager.getUserDetails();
        String UserID = info.get(SessionManager.KEY_USERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        PostRequest(Iconstant.recent_rides, jsonParams);
    }

    private void PostRequest(String Url, final HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(RecentTrips.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------ApplyCoupon jsonParams--------------" + jsonParams);

        ServiceRequest mRequest = new ServiceRequest(RecentTrips.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                recentrides = new Recentrides();
                String Sstatus = "", Smessage = "", Scode = "";
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equalsIgnoreCase("1")) {
                        Type listType = new TypeToken<Recentrides>() {
                        }.getType();
                        recentrides = new GsonBuilder().create().fromJson(object.toString(), listType);
                        setData(recentrides);
                    } else {
                        Toast.makeText(RecentTrips.this, object.getString("response"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();


            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void setData(final Recentrides recentrides) {

        mainlayout_recenttrips.removeAllViews();
        for (int i = 0; i <= recentrides.getResponse().getRecent_rides().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.child_recenttrips, null);
            TextView tv_recentrips = (TextView) view.findViewById(R.id.tv_recentrips);
            ConstraintLayout recenttrips = view.findViewById(R.id.recenttrips);
            tv_recentrips.setText(recentrides.getResponse().getRecent_rides().get(i).getLocation());
            final int finalI = i;
            recenttrips.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("Selected_Location", recentrides.getResponse().getRecent_rides().get(finalI).getLocation());
                    returnIntent.putExtra("Selected_Lattitude", recentrides.getResponse().getRecent_rides().get(finalI).getLatlong().getLat());
                    returnIntent.putExtra("Selected_Longitude", recentrides.getResponse().getRecent_rides().get(finalI).getLatlong().getLon());
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });
            mainlayout_recenttrips.addView(view);
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
