package com.blackmaria.newdesigns.factory;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.newdesigns.viewmodel.RechargepaymentsuccessViewModel;


public class RechargepaymentsuccessFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public RechargepaymentsuccessFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new RechargepaymentsuccessViewModel(context);
    }
}
