package com.blackmaria.newdesigns.factory.Withdraw.Bank;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.newdesigns.viewmodel.withdraw.bank.AddbankdetailsViewModel;


public class AddbankdetailsFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public AddbankdetailsFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new AddbankdetailsViewModel(context);
    }
}
