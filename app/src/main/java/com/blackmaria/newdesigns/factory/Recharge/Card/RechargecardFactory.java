package com.blackmaria.newdesigns.factory.Recharge.Card;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.newdesigns.viewmodel.recharge.card.RechargecardpaymentViewModel;


public class RechargecardFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;
    String pendingtype;

    public RechargecardFactory(Activity context,String pendingtype) {
        this.context = context;
        this.pendingtype =pendingtype;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new RechargecardpaymentViewModel(context,pendingtype);
    }
}
