package com.blackmaria.newdesigns.factory;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.newdesigns.viewmodel.RechargeamountViewModel;


public class RechargeamountFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public RechargeamountFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new RechargeamountViewModel(context);
    }
}
