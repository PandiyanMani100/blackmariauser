package com.blackmaria.newdesigns.factory.Withdraw.Bank;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.newdesigns.viewmodel.withdraw.bank.WithdrawpaymentschooseViewModel;


public class WithdrawpaymentschooseFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public WithdrawpaymentschooseFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new WithdrawpaymentschooseViewModel(context);
    }
}
