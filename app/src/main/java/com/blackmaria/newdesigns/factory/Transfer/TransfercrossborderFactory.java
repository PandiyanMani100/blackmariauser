package com.blackmaria.newdesigns.factory.Transfer;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.newdesigns.viewmodel.transfer.TransfercrossborderViewModel;


public class TransfercrossborderFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public TransfercrossborderFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new TransfercrossborderViewModel(context);
    }
}
