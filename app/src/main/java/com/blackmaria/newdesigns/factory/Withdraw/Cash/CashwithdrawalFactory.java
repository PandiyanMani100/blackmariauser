package com.blackmaria.newdesigns.factory.Withdraw.Cash;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.newdesigns.viewmodel.withdraw.cash.CashwithdrawalViewModel;


public class CashwithdrawalFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public CashwithdrawalFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new CashwithdrawalViewModel(context);
    }
}
