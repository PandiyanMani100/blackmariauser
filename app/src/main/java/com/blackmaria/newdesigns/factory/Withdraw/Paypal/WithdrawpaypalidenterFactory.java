package com.blackmaria.newdesigns.factory.Withdraw.Paypal;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.newdesigns.viewmodel.withdraw.paypal.PaypalwithdrawidenterViewModel;


public class WithdrawpaypalidenterFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public WithdrawpaypalidenterFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new PaypalwithdrawidenterViewModel(context);
    }
}
