package com.blackmaria.newdesigns.factory.Recharge.Bank;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.newdesigns.viewmodel.recharge.bank.RechargeselectbankViewModel;


public class RechargeselectbankpaymentFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public RechargeselectbankpaymentFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new RechargeselectbankViewModel(context);
    }
}
