package com.blackmaria.newdesigns.factory.Transfer;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.newdesigns.viewmodel.transfer.TransferamountenterViewModel;


public class TransferamountenterFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public TransferamountenterFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new TransferamountenterViewModel(context);
    }
}
