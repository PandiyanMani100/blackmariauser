package com.blackmaria.newdesigns;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.util.IOUtils;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.blackmaria.LanguageDb;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.LoginPage;

import com.blackmaria.OtpPage;
import com.blackmaria.R;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.CountryDialCode;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.AppController;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.volley.VolleyMultipartRequest;
import com.blackmaria.widgets.PkDialogUserinfo;
import com.blackmaria.widgets.PkDialogtryagain;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blackmaria.HomePage.isValidEmail;

public class Registeration extends FragmentActivityHockeyApp implements View.OnClickListener {

    LanguageDb mhelper;
    private EditText et_username, et_email, et_mobile, et_ref;
    private TextView tv_regis, Tv_countryCode, tv_verify,signinn;
    private CountryPicker picker;
    private GPSTracker gpsTracker;
    private String GCM_Id = "", lat = "", lon = "";
    private RoundedImageView iv_user;
    private ImageView backimg;

    String uploaded_file_name = "";
    private String s3_bucket_access_key = "";
    private String s3_bucket_secret_key = "";
    private String s3_bucket_name = "";
    private AmazonS3Client s3Client;
    private BasicAWSCredentials credentials;
    TransferObserver uploadObserver;

    private Uri camera_FileUri;
    Bitmap bitMapThumbnail;
    private byte[] byteArray;
    private int REQUEST_TAKE_PHOTO = 1;
    private int galleryRequestCode = 2;
    private static final String IMAGE_DIRECTORY_NAME = "blackmaria";
    private SessionManager session;
    private String sAuthKey = "";
    private String UserID = "";
    private String gcmID = "";
    private AppInfoSessionManager appInfoSessionManager;
    private int mobno_change_request_code = 200;
    final int PERMISSION_REQUEST_CODE = 111;
    private File destination;
    File imageRoot;
    private Uri mImageCaptureUri, outputUri;
    String appDirectoryName = "";
    String File_names = "";
    String Extension = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newregpage);
        mhelper = new LanguageDb(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        session = new SessionManager(this);
        initView();
        s3BucketInit();
    }

    private void s3BucketInit() {

        s3_bucket_name = session.getbucketname();
        s3_bucket_access_key = session.getaccesskey();
        s3_bucket_secret_key = session.getsecretkey();

        getApplicationContext().startService(new Intent(getApplicationContext(), TransferService.class));
        credentials = new BasicAWSCredentials(s3_bucket_access_key, s3_bucket_secret_key);
        s3Client = new AmazonS3Client(credentials);
        s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
    }



    private void initView() {



       TextView txt_signup = findViewById(R.id.txt_signup);
       TextView singnupp= findViewById(R.id.singnupp);
       TextView referealcodess= findViewById(R.id.referealcodess);
        TextView dontacc= findViewById(R.id.dontacc);

       txt_signup.setText(getkey("uploadphoto"));
       singnupp.setText(getkey("text_signup"));
       referealcodess.setText(getkey("referral_code"));
       dontacc.setText(getkey("have_account"));

        backimg = findViewById(R.id.backimg);
        iv_user = findViewById(R.id.iv_user);
        et_username = findViewById(R.id.edit_username);
        et_username.setHint(getkey("namesmall"));

        et_email = findViewById(R.id.edit_emailaddres);
        et_email.setHint(getkey("email"));

        et_mobile = findViewById(R.id.edit_mobilenumber);
        et_mobile.setHint(getkey("edit_mobilenumber"));

        et_ref = findViewById(R.id.tv_refferalcode);
        et_ref.setHint(getkey("enter_if_any"));




        Tv_countryCode = findViewById(R.id.txt_country_code);

        tv_verify = findViewById(R.id.tv_verify);
        tv_verify.setText(getkey("check"));

        tv_regis = findViewById(R.id.text_register);
        tv_regis.setText(getkey("text_signup"));

        signinn = findViewById(R.id.signinn);
        signinn.setText(getkey("signinnn"));


        tv_regis.setOnClickListener(this);
        picker = CountryPicker.newInstance(getkey("select_country_lable"));
        session = new SessionManager(this);
        appInfoSessionManager = new AppInfoSessionManager(Registeration.this);
        HashMap<String, String> info = session.getUserDetails();
        String tipstime = info.get(SessionManager.KEY_PHONENO);
        UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> app = appInfoSessionManager.getAppInfo();
        sAuthKey = app.get(appInfoSessionManager.KEY_APP_IDENTITY);

        HashMap<String, String> user = session.getUserDetails();
        gcmID = user.get(SessionManager.KEY_GCM_ID);


//        ImageView Rl_drawer = (ImageView) findViewById(R.id.loading);
       /*GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(Rl_drawer);
        Glide.with(this).load(R.drawable.alertgif).into(imageViewTarget);*/
//        final Animation animation = new AlphaAnimation(1, 0);
//        animation.setDuration(500);
//        animation.setInterpolator(new LinearInterpolator());
//        animation.setRepeatCount(Animation.INFINITE);
//        animation.setRepeatMode(Animation.REVERSE);
//        Rl_drawer.startAnimation(animation);

        Tv_countryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                Tv_countryCode.setText(dialCode);
                CloseKeyBoard();
            }
        });


        signinn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Registeration.this, LoginPage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (et_ref.getText().length() != 0) {
                    verifyreferalcode(Iconstant.check_refrrelcode);
                } else {
                    Alert(getkey("action_error"), getkey("otp_page_referral_error"));
                }
            }
        });

        et_ref.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable
                                                 et) {
                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    et_ref.setText(s);
                }
            }
        });

        gpsTracker = new GPSTracker(Registeration.this);
        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {

            double MyCurrent_lat = gpsTracker.getLatitude();
            double MyCurrent_long = gpsTracker.getLongitude();

            lat = String.valueOf(MyCurrent_lat);
            lon = String.valueOf(MyCurrent_long);

            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    String Str_getCountryCode = addresses.get(0).getCountryCode();
                    if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode);


                       // Tv_countryCode.setText(Str_countyCode);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        iv_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()) {
                        requestPermission();
                    } else {
                        chooseimage();
                    }
                } else {
                    chooseimage();
                }
            }
        });
    }

    private void verifyreferalcode(String Url) {
        final Dialog dialog = new Dialog(Registeration.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("referal_code", et_ref.getText().toString());

        ServiceRequest mRequest = new ServiceRequest(Registeration.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "", SreferalStatus = "", errorStatus = "", userId = "", accountExit = "", errorMessage = "", Sotp_status = "", Sotp = "";

                try {
                    dialog.dismiss();
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        String name = object.getJSONObject("user_data").getString("name");
                        String phone = object.getJSONObject("user_data").getString("phone_number");
                        String image = object.getJSONObject("user_data").getString("image");
                        Alert1(getkey("action_success"), name, phone, image);
                    } else {
                        Alert(getkey("action_error"), object.getString("message"));
                    }
                } catch (JSONException e) {

                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }


                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(et_mobile.getWindowToken(), 0);

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private int getResId(String drawableName) {

        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }

    private void CloseKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_register:
                validate();
                break;
        }
    }

    private void validate() {
        if (et_username.getText().toString().length() == 0) {

            Alert(getkey("infotag"), getkey("profile_label_alert_username"));
        } else if (et_mobile.getText().toString().length() < 4) {

            Alert(getkey("infotag"), getkey("profile_lable_error_mobile"));
        } else if (!isValidEmail(et_email.getText().toString().trim().replace(" ", ""))) {

            Alert(getkey("infotag"), getkey("profile_label_alert_email"));
        } else if (session.getUserImageUpdate().equalsIgnoreCase("")) {
            Alert(getkey("infotag"), getkey("profile_mandatory"));
        } else {

            final String SdeviceToken = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            //---------Getting GCM Id----------

            validate_email_mobile(Iconstant.register_url, SdeviceToken, GCM_Id);

        }
    }

    private void validate_email_mobile(String Url, final String SdeviceToken, final String GCM_Id) {
        final Dialog dialog = new Dialog(Registeration.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", et_email.getText().toString());
        jsonParams.put("user_name", et_username.getText().toString());
        jsonParams.put("country_code", Tv_countryCode.getText().toString());
        jsonParams.put("phone_number", et_mobile.getText().toString().trim());
        jsonParams.put("deviceToken", SdeviceToken);
        jsonParams.put("gcm_id", FirebaseInstanceId.getInstance().getToken());
        jsonParams.put("referal_code", et_ref.getText().toString());
        jsonParams.put("lat", lat);
        jsonParams.put("image", session.getUserImageName());
        jsonParams.put("lon", lon);


        ServiceRequest mRequest = new ServiceRequest(Registeration.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "", SreferalStatus = "", errorStatus = "", userId = "", accountExit = "", errorMessage = "", Sotp_status = "", Sotp = "";

                try {
                    dialog.dismiss();
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Sotp_status = object.getString("otp_status");
                        Sotp = object.getString("otp");
                        SreferalStatus = object.getString("referal_code");

                    }
                } catch (JSONException e) {

                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    if (errorStatus.equalsIgnoreCase("1")) {
                        Alert(getkey("action_error"), errorMessage);
                    } else {

                        System.out.println("Sotp--->>"+Sotp);
                        Intent intent = new Intent(Registeration.this, OtpPage.class);
                        intent.putExtra("Otp_Status", Sotp_status);
                        intent.putExtra("Otp", Sotp);
                        intent.putExtra("ReferalStatus", SreferalStatus);
                        intent.putExtra("Phone", et_mobile.getText().toString());
                        intent.putExtra("CountryCode", Tv_countryCode.getText().toString());
                        intent.putExtra("GcmID", GCM_Id);
                        intent.putExtra("rcode", et_ref.getText().toString());

                        intent.putExtra("SdeviceToken", SdeviceToken);
                        intent.putExtra("username", et_username.getText().toString());
                        intent.putExtra("email", et_email.getText().toString());
                        intent.putExtra("registeration", "");
                        startActivity(intent);
                        //  finish();
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    }
                } else {
                    Alert(getkey("action_error"), Smessage);
                }
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(et_mobile.getWindowToken(), 0);

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialogtryagain mDialog = new PkDialogtryagain(Registeration.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void Alert1(String title, String name, String phone, String image) {
        final PkDialogUserinfo mDialog = new PkDialogUserinfo(Registeration.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(getkey("name_col")+ name + "\n\n" + getkey("mobiole_co") + phone);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), image, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(Registeration.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    //image upload
    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseimage();
                } else {
                    finish();
                }
                break;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    private void chooseimage() {
        final Dialog photo_dialog = new Dialog(Registeration.this);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        photo_dialog.setCanceledOnTouchOutside(true);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);

        TextView titles = (TextView) photo_dialog
                .findViewById(R.id.titles);
        TextView cameratext = (TextView) photo_dialog
                .findViewById(R.id.camera);
        TextView galleryy = (TextView) photo_dialog
                .findViewById(R.id.galleryy);

        titles.setText(getkey("takephoto"));
        cameratext.setText(getkey("camra"));
        galleryy.setText(getkey("existingcamera"));

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                photo_dialog.dismiss();
            }
        });
    }

    private void takePicture() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        camera_FileUri = getOutputMediaFileUri(1);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, camera_FileUri);
//        // start the image capture Intent
//        startActivityForResult(intent, REQUEST_TAKE_PHOTO);

        try {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                //Create a file to store the image
                imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), appDirectoryName);
            }
            if (imageRoot != null) {
                String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                destination = new File(imageRoot, name + ".jpg");
                Uri photoURI = FileProvider.getUriForFile(this, "com.provider", destination);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                startActivityForResult(pictureIntent,
                        REQUEST_TAKE_PHOTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, galleryRequestCode);
    }

    /**
     * Creating file uri to store image/video
     */
//    public Uri getOutputMediaFileUri(int type) {
//        return Uri.fromFile(getOutputMediaFile(type));
//    }

//    private static File getOutputMediaFile(int type) {
//        File  imageRoot = null;
//        try {
//            Intent pictureIntent = new Intent(
//                    MediaStore.ACTION_IMAGE_CAPTURE);
//            if (pictureIntent.resolveActivity(.getPackageManager()) != null) {
//                //Create a file to store the image
//                imageRoot = new File(Environment.getExternalStoragePublicDirectory(
//                        Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
//            }
//            if (imageRoot != null) {
//                String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
//                destination = new File(imageRoot, name + ".jpg");
//                Uri photoURI = FileProvider.getUriForFile(this, "com.provider", destination);
//                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
//                        photoURI);
//                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
//                startActivityForResult(pictureIntent,
//                        TAKE_PHOTO_REQUEST);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // External sdcard location
//        File mediaStorageDir = new File(
//                Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
//
//        // Create the storage directory if it does not exist
//        if (!mediaStorageDir.exists()) {
//            if (!mediaStorageDir.mkdirs()) {
//                Log.d("", "Oops! Failed create "
//                        + IMAGE_DIRECTORY_NAME + " directory");
//                return null;
//            }
//        }
//
//        // Create a media file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
//                Locale.getDefault()).format(new Date());
//        File mediaFile;
//        if (type == 1) {
//            mediaFile = new File(mediaStorageDir.getPath() + File.separator
//                    + "IMG_" + timeStamp + ".jpg");
//        } else {
//            return null;
//        }
//
//        return mediaFile;
//    }
    public String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // get the file url
        camera_FileUri = savedInstanceState.getParcelable("file_uri");
    }

    private void onCaptureImageResult(Intent data) {
        try {
            String imagePath = destination.getAbsolutePath();
            camera_FileUri = Uri.fromFile(new File(imagePath));
            outputUri = camera_FileUri;

//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);

//            UCrop.Options options = new UCrop.Options();
//            options.setStatusBarColor(getResources().getColor(R.color.black_color));
//            options.setToolbarColor(getResources().getColor(R.color.app_primary_color));
//            options.setMaxBitmapSize(800);
//
//            UCrop.of(mImageCaptureUri, outputUri)
//                    .withAspectRatio(1, 1)
//                    .withMaxResultSize(8000, 8000)
//                    .withOptions(options)
//                    .start(Registeration.this);

            UCrop.Options Uoptions = new UCrop.Options();
            Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
            Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
            Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));


            try {
                UCrop.of(camera_FileUri, camera_FileUri)
                        .withAspectRatio(4, 4)
                        .withMaxResultSize(8000, 8000)
                        .start(Registeration.this);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onCroppedImageResult(data);
            }
            else if (requestCode == galleryRequestCode) {

                try {
                    Uri selectedImage = data.getData();
                    if (selectedImage.toString().startsWith("content://com.sec.android.gallery3d.provider")) {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        final String picturePath = c.getString(columnIndex);
                        c.close();

                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                        Bitmap thumbnail = bitmap; //getResizedBitmap(bitmap, 600);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        File curFile = new File(picturePath);

                        String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                        curFile = storeImage(thumbnail,name);

                        UCrop.Options Uoptions = new UCrop.Options();
                        Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));


                        try {
                            UCrop.of(Uri.fromFile(curFile),Uri.fromFile(curFile))
                                    .withAspectRatio(4, 4)
                                    .withMaxResultSize(8000, 8000)
                                    .start(Registeration.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                    else
                    {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();

                        int columnIndex = c.getColumnIndex(filePath[0]);
                        final String picturePath = c.getString(columnIndex);

                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                        Bitmap thumbnail = bitmap; //getResizedBitmap(bitmap, 600);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        File curFile = new File(picturePath);


                        String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                        curFile = storeImage(thumbnail,name);

                        UCrop.Options Uoptions = new UCrop.Options();
                        Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));


                        try {
                            UCrop.of(Uri.fromFile(curFile),Uri.fromFile(curFile))
                                    .withAspectRatio(4, 4)
                                    .withMaxResultSize(8000, 8000)
                                    .start(Registeration.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                } catch (OutOfMemoryError | NullPointerException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == mobno_change_request_code && data != null) {
                String Scountrycode = data.getStringExtra("countryCode");
                String SmobNo = data.getStringExtra("phoneNo");
                String Smessage = data.getStringExtra("message");
//                Tv_countryCode.setText(Scountrycode);
//                Et_PhoneNo.setText(SmobNo);
                Alert(getkey("action_success"), Smessage);
            }

        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private void onCroppedImageResult(Intent data) {

        final Uri resultUri = UCrop.getOutput(data);
        Log.d("Crop success", "" + resultUri);
        try
        {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
            Bitmap thumbnail = bitmap;
            final String picturePath = resultUri.getPath();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            File curFile = new File(picturePath);
            try {
                ExifInterface exif = new ExifInterface(curFile.getPath());
                int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationInDegrees = exifToDegrees(rotation);

                Matrix matrix = new Matrix();
                if (rotation != 0f) {
                    matrix.preRotate(rotationInDegrees);
                }
                thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (IOException ex) {
                Log.e("TAG", "Failed to get Exif data", ex);
            }

            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            byteArray = byteArrayOutputStream.toByteArray();

            bitMapThumbnail = thumbnail;
            iv_user.setImageBitmap(thumbnail);

            if(session.getimagestatus().equals("1"))
            {
                uploadToS3Bucket(curFile);
                //UploadDriverImage(Iconstant.Update_profile_image_url);
            }
            else
            {
                UploadDriverImage(Iconstant.Update_profile_image_url);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //-------------------------------------------------Aws S3 Bucket Start

    private void uploadToS3Bucket(File fileUri)
    {
        String[] path_split;
        if (fileUri != null) {

            final Dialog dialog = new Dialog(Registeration.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            uploaded_file_name = "";
            String path = String.valueOf(fileUri);
            String file_name_full = path.substring(path.lastIndexOf("/") + 1);
            path_split = file_name_full.split(".");
            if (path_split.length == 2) {
                File_names = path_split[0];
                Extension = path_split[1];

            } else {
                path_split = file_name_full.split(".jpg");
                if (path_split.length == 1) {
                    File_names = path_split[0];
                }
            }

            String file_name_upload = File_names;
            //current time stamp
            Long tsLong = System.currentTimeMillis() / 1000;
            String str_current_ts = tsLong.toString();
            String concat_image_name = file_name_upload + "" + str_current_ts;
            final String fileName = md5Encryption(concat_image_name) + "." + "jpg";


            TransferUtility transferUtility =
                    TransferUtility.builder()
                            .context(getApplicationContext())
                            .defaultBucket(s3_bucket_name)
                            .s3Client(s3Client)
                            .build();
            uploaded_file_name = fileName;

            uploadObserver = transferUtility.upload("images/users/" + fileName, fileUri, CannedAccessControlList.PublicRead);
            // Attach a listener to the observer to get state update and progress notifications
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    // Handle a completed upload.
                    if (TransferState.COMPLETED == state) {
                        String path = uploadObserver.getAbsoluteFilePath();
                        session.setUserImageUpdate(path);
                        session.setUserImageName(fileName);
                        dialog.dismiss();
                    } else if (TransferState.FAILED == state) {
                        // dialog.dismiss();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                    Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
                }

                @Override
                public void onError(int id, Exception ex) {
                    // Handle errors
                    ex.printStackTrace();
                    uploaded_file_name = "";

                    dialog.dismiss();
                }

            });

            // If you prefer to poll for the data, instead of attaching a
            // listener, check for the state and progress in the observer.
            if (TransferState.COMPLETED == uploadObserver.getState()) {
                // Handle a completed upload.
                Log.d("completed upload", "completed upload");
            }

        }
    }

    private void UploadDriverImage(String url) {

        final Dialog dialog = new Dialog(Registeration.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                System.out.println("------------- image response-----------------" + response.data);


                String resultResponse = new String(response.data);
                System.out.println("-------------  response-----------------" + resultResponse);
                String sStatus = "", sResponse = "", SUser_image = "", Smsg = "";
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    Smsg = jsonObject.getString("response");
                    if (sStatus.equalsIgnoreCase("1")) {
                        SUser_image = jsonObject.getString("picture_path");
                        iv_user.setImageBitmap(bitMapThumbnail);
                        if (!SUser_image.equals("")) {
                            session.setUserImageUpdate(SUser_image);
                        }
                        session.setUserImageName(jsonObject.getString("picture_name"));
                        Alert(getkey("action_success"), Smsg);
                    } else {
                        Alert(getkey("action_error"), Smsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
//                params.put("user_id", UserID);
//                System.out.println("user_id---------------" + UserID);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("image", new DataPart("blackmaria.jpg", byteArray));
                System.out.println("user_image--------edit------" + byteArray);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authkey", sAuthKey);
                headers.put("User-agent", Iconstant.cabily_userAgent);
                headers.put("isapplication", Iconstant.cabily_IsApplication);
                headers.put("applanguage", Iconstant.cabily_AppLanguage);
                headers.put("apptype", Iconstant.cabily_AppType);
                headers.put("userid", UserID);
                headers.put("apptoken", gcmID);
                System.out.println("------------header---------" + headers);
                return headers;
            }

        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }

    public String md5Encryption(String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void createFile(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

    private File storeImage(Bitmap image,String imagename) {
        File myDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), appDirectoryName);
        myDir.mkdirs();

        String fname = imagename+".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

            if (file.exists ())
            {
                System.out.println("sucess");
            }
            else
            {
                System.out.println("failed");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }



}
