package com.blackmaria.newdesigns;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.adapter.Helpfaqtitle_adapter;
import com.blackmaria.pojo.faqhelp_pojo;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialogtryagain;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class Helpfaq extends AppCompatActivity implements Helpfaqtitle_adapter.Faqclicklistener {

    private ImageView img_back;
    private EditText searchhint;
    private String UserID = "";
    private ListView helpquestions;
    private SessionManager session;
    private faqhelp_pojo homepage;
    private Helpfaqtitle_adapter helpfaqtitle_adapter;
    private JSONObject wholeobjec;
    LanguageDb mhelper;

    private ArrayList<String> faqtitles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helpfaq);
        mhelper = new LanguageDb(this);

        initView();


    }

    private void initView() {
        session = new SessionManager(this);
        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        searchhint = (EditText) findViewById(R.id.searchhint);
        searchhint.setHint(getkey("search_help"));
        helpquestions = findViewById(R.id.helpquestions);

        TextView helpfaq = (TextView)findViewById(R.id.helpfaq);
        helpfaq.setText(getkey("helpfaq"));

        TextView howmayassistyou = (TextView)findViewById(R.id.howmayassistyou);
        howmayassistyou.setText(getkey("how_may_we_assist_you"));

        searchhint.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                helpfaqtitle_adapter.getFilter().filter(editable.toString());
                Log.e("searchhint", searchhint.getText().toString());
            }
        });

        Helpfaqlist(Iconstant.faqlist);
    }

    private void Helpfaqlist(String Url) {

        final Dialog dialog = new Dialog(Helpfaq.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);

        ServiceRequest mRequest = new ServiceRequest(Helpfaq.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                if (dialog != null) {
                    dialog.dismiss();
                }
                String Sstatus = "", Salertmsg = "", Smilage = "", SdistanceUnit = "";
                try {
                    JSONObject object = new JSONObject(response);
                    wholeobjec = new JSONObject();
                    wholeobjec = object;
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Type listType = new TypeToken<faqhelp_pojo>() {
                        }.getType();
                        homepage = new GsonBuilder().create().fromJson(object.toString(), listType);
                        faqtitles.clear();

                        for (int i = 0; i <= homepage.getResponse().size() - 1; i++) {
                            faqtitles.add(homepage.getResponse().get(i).getName());
                        }

                        helpfaqtitle_adapter = new Helpfaqtitle_adapter(Helpfaq.this, faqtitles, Helpfaq.this);
                        helpquestions.setAdapter(helpfaqtitle_adapter);

                    } else {
                        Salertmsg = object.getString("response");
                        Alert(getkey("action_error"), Salertmsg);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Helpfaq.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onSelectClick(String question, int position) {
        System.out.println("adsfasdfa" + question + " " + position);

        Intent i = new Intent(Helpfaq.this, HelpFaq_subactivity.class);
        i.putExtra("json", wholeobjec.toString());
        i.putExtra("name", question);
        startActivity(i);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
