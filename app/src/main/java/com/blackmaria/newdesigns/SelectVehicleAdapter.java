package com.blackmaria.newdesigns;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.InterFace.VehicleCatSelect;
import com.blackmaria.pojo.NewCategoryListPojo;
import com.blackmaria.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


/**
 * Created by GANESH on 23/08/2017.
 */
public class SelectVehicleAdapter extends RecyclerView.Adapter<SelectVehicleAdapter.MyViewHolder> {
    private ArrayList<NewCategoryListPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    private VehicleCatSelect vehicleCatSelect;


    public SelectVehicleAdapter(Context c, ArrayList<NewCategoryListPojo> d, VehicleCatSelect vehicleCatSelect) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        this.vehicleCatSelect = vehicleCatSelect;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.select_vehicle_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Glide.with(context).load(data.get(position).getCat_normal_img()).into(holder.iv_car);
        holder.tv_cartype.setText(data.get(position).getCategory_name());
        holder.tv_minutes.setText(data.get(position).getCat_eta());
        holder.tv_max_persons.setText(data.get(position).getMax_person());

        if (data.get(position).getOffer_type().equalsIgnoreCase("hot") && data.get(position).getStrOfferType().equalsIgnoreCase("motorbike")) {
            holder.iv_cathot.setVisibility(View.VISIBLE);
            holder.iv_cathot.setImageResource(R.drawable.hot_motor);
        } else if (data.get(position).getOffer_type().equalsIgnoreCase("hot") && !data.get(position).getStrOfferType().equalsIgnoreCase("motorbike")) {
//            Glide.with(context).load(data.get(position).getCat_normal_img()).into(holder.iv_cathot);
            holder.iv_cathot.setVisibility(View.VISIBLE);
            holder.iv_cathot.setImageResource(R.drawable.hot_standard);

        } else {
            holder.iv_cathot.setVisibility(View.INVISIBLE);
        }


        if (data.get(position).getCat_type().equalsIgnoreCase("delivery") && data.get(position).getStrOfferType().equalsIgnoreCase("motorbike")) {
            holder.tv_max_persons.setVisibility(View.GONE);
            holder.iv_maxpersons.setVisibility(View.VISIBLE);
            holder.iv_maxpersons.setImageResource(R.drawable.motor_courier);
        }/* else if (data.get(position).getCat_type().equalsIgnoreCase("transport") && data.get(position).getStrOfferType().equalsIgnoreCase("car&van")) {
            holder.tv_max_persons.setVisibility(View.GONE);
            holder.iv_maxpersons.setVisibility(View.VISIBLE);
            holder.iv_maxpersons.setImageResource(R.drawable.carandvan);
        }*/ else if (data.get(position).getCat_type().equalsIgnoreCase("delivery")) {
            holder.tv_max_persons.setVisibility(View.GONE);
            holder.iv_maxpersons.setVisibility(View.VISIBLE);
            holder.iv_maxpersons.setImageResource(R.drawable.carandvan);
        } else {
            holder.iv_maxpersons.setVisibility(View.GONE);
            holder.tv_max_persons.setVisibility(View.VISIBLE);
        }


        if (data.get(position).getCat_eta().equalsIgnoreCase("Not Available")) {
            holder.loading.setColorFilter(ContextCompat.getColor(context, R.color.red_color));
        } else {
            holder.loading.setColorFilter(ContextCompat.getColor(context, R.color.btn_green_color));
        }


        holder.lv_catseelct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vehicleCatSelect.Selectvehicle(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_car, iv_cathot, loading, iv_maxpersons;
        private TextView tv_cartype, tv_minutes, tv_max_persons;
        private LinearLayout lv_catseelct;

        public MyViewHolder(View view) {
            super(view);
            tv_cartype = view.findViewById(R.id.tv_cartype);
            tv_minutes = view.findViewById(R.id.tv_minutes);
            tv_max_persons = view.findViewById(R.id.tv_max_persons);

            iv_car = view.findViewById(R.id.iv_car);
            iv_cathot = view.findViewById(R.id.iv_cathot);
            loading = view.findViewById(R.id.loading);
            iv_maxpersons = view.findViewById(R.id.iv_maxpersons);
            lv_catseelct = view.findViewById(R.id.lv_catselect);
        }

    }


}


