package com.blackmaria.newdesigns;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.pojo.Saveplustransfer;
import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.widgets.PkDialogtryagain;
import com.devspark.appmsg.AppMsg;

import java.util.HashMap;

public class Closeaccount_pincheck extends AppCompatActivity {

    private TextView img2, img1, img3, img5, img4, img6, img8, img7, img9, img0, img_star, img_hash, confirm;
    private EditText edt_email;
    private LinearLayout ly_forgetpin;
    private ImageView booking_back_imgeview;
    LanguageDb mhelper;
    private String number = "";
    int[] imageViews = {R.id.img1, R.id.img2, R.id.img3, R.id.img4, R.id.img5, R.id.img6, R.id.img7, R.id.img8, R.id.img9, R.id.img0};
    private float TIME_DELAY = 2;
    private float lastY = 0;
    private SessionManager session;
    private HashMap<String, String> info;
    private boolean isfromsaveplus_scan = false, isfromsaveplussendmoney = false, isfrommenu = false;

    private String receiver_id = "", received_amount = "", UserID = "";
    private Saveplustransfer homepage;

    private int countcheck_wrongpattern = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fastpaytransfer_pincheck);
        mhelper = new LanguageDb(this);
        init();
        clicklistener();
    }


    private void init() {
        session = new SessionManager(this);
        info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        ly_forgetpin = findViewById(R.id.ly_forgetpin);
        edt_email = findViewById(R.id.edt_email);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);
        confirm = findViewById(R.id.confirm);
        confirm.setText(getkey("confirm_lable"));


        TextView pickup_lable = findViewById(R.id.pickup_lable);
        pickup_lable.setText(getkey("enteryourpincode"));

        TextView forgotpin = findViewById(R.id.forgotpin);
        forgotpin.setText(getkey("forgetpin"));

        img2 = findViewById(R.id.img2);
        img1 = findViewById(R.id.img1);
        img3 = findViewById(R.id.img3);
        img5 = findViewById(R.id.img5);
        img4 = findViewById(R.id.img4);
        img6 = findViewById(R.id.img6);
        img8 = findViewById(R.id.img8);
        img7 = findViewById(R.id.img7);
        img9 = findViewById(R.id.img9);
        img0 = findViewById(R.id.img0);
        img_star = findViewById(R.id.img_star);
        img_hash = findViewById(R.id.img_hash);

        if (getIntent().hasExtra("mode")) {
            receiver_id = getIntent().getStringExtra("mode");

        }
    }

    private void clicklistener() {

        edt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
            }
        });

        edt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 6) {
                    confirm.setBackground(getResources().getDrawable(R.drawable.green_curve));
                } else {
                    confirm.setBackground(getResources().getDrawable(R.drawable.grey_curve));
                }
//                String sSecurePin = session.getSecurePin();
//                if (sSecurePin.equalsIgnoreCase(s.toString())) {
//                    confirm.setVisibility(View.VISIBLE);
//                } else {
//                    confirm.setVisibility(View.INVISIBLE);
//                }
            }
        });
        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "1";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());

                setbackground(img1.getId());
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "2";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img2.getId());
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "3";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img3.getId());
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "4";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img4.getId());
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "5";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img5.getId());
            }
        });

        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "6";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img6.getId());
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "7";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img7.getId());
            }
        });
        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "8";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img8.getId());
            }
        });
        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "9";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img9.getId());
            }
        });

        img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "0";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img0.getId());
            }
        });

        img_hash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String SMobileNo = edt_email.getText().toString();
                if (SMobileNo != null && SMobileNo.length() > 0) {
                    edt_email.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                    edt_email.setSelection(edt_email.getText().length());
                    number = edt_email.getText().toString();
                }

                img_hash.setBackground(getResources().getDrawable(R.drawable.round_green));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img_hash.setBackground(getResources().getDrawable(R.drawable.round_white));
                    }
                }, 100);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.getText().toString().length() == 0) {
                    AlertError(getkey("action_error"), getkey("profile_label_alert_pincode"));
                } else if (edt_email.getText().toString().trim().length() != 6) {
                    AlertError(getkey("action_error"), getkey("profile_label_alert_pincodes"));
                } else {
                    ConnectionDetector cd = new ConnectionDetector(Closeaccount_pincheck.this);
                    boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {

                        String sSecurePin = session.getSecurePin();
                        if (sSecurePin.equalsIgnoreCase(edt_email.getText().toString().trim())) {
                            Intent ii = new Intent(Closeaccount_pincheck.this, Accountclosed.class);
                            ii.putExtra("mode", receiver_id);
                            startActivity(ii);
                            finish();
                        } else {
                            number ="";
                            if (countcheck_wrongpattern == 1) {
                                countcheck_wrongpattern++;
                                edt_email.setText("");
                                edt_email.getText().clear();
                                Alertwrongpattern(getkey("action_error"), getkey("wornspins"));
                            } else if (countcheck_wrongpattern == 2) {
                                countcheck_wrongpattern++;
                                edt_email.setText("");
                                edt_email.getText().clear();
                                Alertwrongpattern(getkey("action_error"), getkey("wrongpin_firstime"));
                            } else if (countcheck_wrongpattern == 3) {
                                countcheck_wrongpattern = 1;
                                edt_email.setText("");
                                edt_email.getText().clear();
                                closepopupinseconds(getkey("wron_pin"), getkey("plse_enter_untlin_last"));
                            }
                        }

                    } else {
                        Alert(getkey("alert_nointernet"), getkey("alert_nointernet_message"));
                    }
                }
            }
        });

        ly_forgetpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Closeaccount_pincheck.this, Forgetpin.class);
                startActivity(i);
            }
        });
    }

    private void closepopupinseconds(String title, final String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Closeaccount_pincheck.this);
        mDialog.setDialogTitle(title);
        mDialog.setCancelble(false);
        mDialog.setCancelOnTouchOutside(false);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mDialog.setDialogMessage(" " + millisUntilFinished / 1000 + "\n \n \n " + alert);
            }

            public void onFinish() {
                mDialog.dismiss();
            }

        }.start();
        mDialog.show();
    }

    private void Alertwrongpattern(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Closeaccount_pincheck.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("forget_pattern"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Closeaccount_pincheck.this, Pin_activitynew.class);
                startActivity(i);
                finish();
            }
        });
        mDialog.show();
    }

    private void setbackground(int image) {
        for (int i = 0; i <= imageViews.length - 1; i++) {
            final TextView img = findViewById(imageViews[i]);
            if (image == imageViews[i]) {
                img.setBackground(getResources().getDrawable(R.drawable.round_green));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img.setBackground(getResources().getDrawable(R.drawable.round_white));
                    }
                }, 100);
            } else {
                img.setBackground(getResources().getDrawable(R.drawable.round_white));
            }
        }

    }


    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(Closeaccount_pincheck.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    private void Alert(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Closeaccount_pincheck.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
