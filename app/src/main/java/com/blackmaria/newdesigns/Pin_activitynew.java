package com.blackmaria.newdesigns;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.newdesigns.saveplus.Confirmpatern_activity;
import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;

import java.util.HashMap;

public class Pin_activitynew extends AppCompatActivity {

    private TextView img2, img1, img3, img5, img4, img6, img8, img7, img9, img0, img_star, img_hash, confirm;
    private EditText edt_email;
    private TextView ly_forgetpin;
    private ImageView booking_back_imgeview;
    private String number = "";
    EditText e1,e2,e3,e4,e5,e6;

    int[] imageViews = {R.id.img1, R.id.img2, R.id.img3, R.id.img4, R.id.img5, R.id.img6, R.id.img7, R.id.img8, R.id.img9, R.id.img0};
    private float TIME_DELAY = 2;
    private float lastY = 0;
    private SessionManager session;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newpinscreens);
        mhelper = new LanguageDb(this);
        init();
        clicklistener();
    }

    private void init() {
        session = new SessionManager(this);
        ly_forgetpin = findViewById(R.id.ly_forgetpin);
        ly_forgetpin.setText(getkey("forgot_pin"));
        edt_email = findViewById(R.id.edt_email);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);
        confirm = findViewById(R.id.confirm);
        confirm.setText(getkey("confirm_lable"));

        TextView enterp = findViewById(R.id.enterp);
        enterp.setText(getkey("enter_pin"));

        img2 = findViewById(R.id.img2);
        img1 = findViewById(R.id.img1);

        img3 = findViewById(R.id.img3);
        img5 = findViewById(R.id.img5);
        img4 = findViewById(R.id.img4);
        img6 = findViewById(R.id.img6);
        img8 = findViewById(R.id.img8);
        img7 = findViewById(R.id.img7);
        img9 = findViewById(R.id.img9);
        img0 = findViewById(R.id.img0);
        img_star = findViewById(R.id.img_star);
        img_hash = findViewById(R.id.img_hash);

        e1= findViewById(R.id.et1);
        e2= findViewById(R.id.et2);
        e3= findViewById(R.id.et3);
        e4= findViewById(R.id.et4);
        e5= findViewById(R.id.et5);
        e6= findViewById(R.id.et6);

    }

    private void clicklistener() {

        edt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
            }
        });

        edt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 6) {
                   // confirm.setBackground(getResources().getDrawable(R.drawable.green_curve));
                } else {
                   // confirm.setBackground(getResources().getDrawable(R.drawable.grey_curve));
                }
            }
        });

        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "1";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setedittextstar(edt_email.getText().length());

                setbackground(img1.getId());

            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "2";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img2.getId());
                setedittextstar(edt_email.getText().length());

            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "3";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img3.getId());
                setedittextstar(edt_email.getText().length());

            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "4";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img4.getId());
                setedittextstar(edt_email.getText().length());
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "5";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img5.getId());
                setedittextstar(edt_email.getText().length());

            }
        });

        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "6";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img6.getId());
                setedittextstar(edt_email.getText().length());
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "7";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img7.getId());
                setedittextstar(edt_email.getText().length());
            }
        });
        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "8";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img8.getId());
                setedittextstar(edt_email.getText().length());
            }
        });
        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "9";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img9.getId());
                setedittextstar(edt_email.getText().length());
            }
        });

        img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + "0";
                edt_email.setText(number);
                edt_email.setSelection(edt_email.getText().length());
                setbackground(img0.getId());
                setedittextstar(edt_email.getText().length());
            }
        });

        img_hash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String SMobileNo = edt_email.getText().toString();
                if (SMobileNo != null && SMobileNo.length() > 0) {
                    edt_email.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                    edt_email.setSelection(edt_email.getText().length());
                    number = edt_email.getText().toString();
                    setedittextstar(edt_email.getText().length());
                }

                img_hash.setBackground(getResources().getDrawable(R.drawable.round_green));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img_hash.setBackground(getResources().getDrawable(R.drawable.roundlightgray));
                    }
                }, 100);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.getText().toString().length() == 0) {
                    AlertError(getkey("action_error"), getkey("profile_label_alert_pincode"));
                } else if (edt_email.getText().toString().trim().length() != 6) {
                    AlertError(getkey("action_error"), getkey("profile_label_alert_pincodes"));
                } else {
                    ConnectionDetector cd = new ConnectionDetector(Pin_activitynew.this);
                    boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {

                        HashMap<String, String> user = session.getUserDetails();
                        String sSecurePin = session.getSecurePin();
                        if (sSecurePin.equalsIgnoreCase(edt_email.getText().toString().trim())) {
                            Intent i = new Intent(Pin_activitynew.this, Confirmpatern_activity.class);
                            startActivity(i);
                            finish();
                        } else {
                            number = "";
                            Alert(getkey("action_error"), getkey("you_enter_wrong_pin"));
                        }

                    } else {
                        Alert(getkey("alert_nointernet"), getkey("alert_nointernet_message"));
                    }
                }
            }
        });

        ly_forgetpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Pin_activitynew.this, Forgetpin.class);
                startActivity(i);
            }
        });
    }

    private void setbackground(int image) {
        for (int i = 0; i <= imageViews.length - 1; i++) {
            final TextView img = findViewById(imageViews[i]);
            if (image == imageViews[i]) {
                img.setTextColor(Color.parseColor("#666666"));
                img.setBackground(getResources().getDrawable(R.drawable.round_green));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img.setBackground(getResources().getDrawable(R.drawable.roundlightgray));
                    }
                }, 100);
            } else {
                img.setBackground(getResources().getDrawable(R.drawable.roundlightgray));
                img.setTextColor(Color.parseColor("#666666"));
            }
        }


    }


    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Pin_activitynew.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(Pin_activitynew.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    private void  setedittextstar(int sizeofedit)
    {
        if(sizeofedit == 0)
        {
            e1.setText("");
            e2.setText("");
            e3.setText("");
            e4.setText("");
            e5.setText("");
            e6.setText("");
        }
        else if(sizeofedit == 1)
        {
            e1.setText("*");
            e2.setText("");
            e3.setText("");
            e4.setText("");
            e5.setText("");
            e6.setText("");

        }
        else if(sizeofedit == 2)
        {
            e1.setText("*");
            e2.setText("*");
            e3.setText("");
            e4.setText("");
            e5.setText("");
            e6.setText("");

        }
        else if(sizeofedit == 3)
        {
            e1.setText("*");
            e2.setText("*");
            e3.setText("*");
            e4.setText("");
            e5.setText("");
            e6.setText("");

        }
        else if(sizeofedit == 4)
        {
            e1.setText("*");
            e2.setText("*");
            e3.setText("*");
            e4.setText("*");
            e5.setText("");
            e6.setText("");

        }
        else if(sizeofedit == 5)
        {
            e1.setText("*");
            e2.setText("*");
            e3.setText("*");
            e4.setText("*");
            e5.setText("*");
            e6.setText("");

        }
        else if(sizeofedit == 6)
        {
            e1.setText("*");
            e2.setText("*");
            e3.setText("*");
            e4.setText("*");
            e5.setText("*");
            e6.setText("*");

        }


    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
