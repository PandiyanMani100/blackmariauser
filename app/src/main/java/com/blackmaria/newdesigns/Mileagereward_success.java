package com.blackmaria.newdesigns;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.Navigation_new;
import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.PkDialogtryagain;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class Mileagereward_success extends AppCompatActivity implements View.OnClickListener {

    private TextView close;
    ConstraintLayout hidell;
    private LinearLayout Ll_miles_share_facebook, Ll_miles_share_twitter, Ll_miles_share_googlepuls;

    private String Miles_sharelink = "", Miles_shareMessage = "", Miles_shareSubject = "", Miles_shareImage = "", facebook = "", google = "";
    private boolean isdataPresent = false;

    LanguageDb mhelper;
    final int PERMISSION_REQUEST_CODE = 111;
    final int GOOGLE_PLUS_SHARE_REQUEST_CODE = 222;

    public static final int REQUEST_CODE_FACEBOOK_LOGIN = 111;
    public static final int REQUEST_CODE_FACEBOOK_SHARE = 112;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private ConnectionDetector cd;
    private String UserID = "", rewardid = "", Sdistance = "";
    private boolean isInternetPresent = false;
    private SessionManager session;

    private ImageView imageView;
    private TextView price, giftname, gifttype, validity, redeem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mileagereward_success);
        mhelper = new LanguageDb(this);
        initView();
        facebookSDKInitialize();
    }

    private void initView() {
        session = new SessionManager(this);
        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        cd = new ConnectionDetector(Mileagereward_success.this);
        isInternetPresent = cd.isConnectingToInternet();
        close = findViewById(R.id.close);
        Ll_miles_share_facebook = findViewById(R.id.invite_earn_fb_layout);
        Ll_miles_share_twitter = findViewById(R.id.invite_earn_twitter_layout);
        hidell = findViewById(R.id.hidell);

        TextView milesimage = findViewById(R.id.milesimage);
        milesimage.setText(getkey("success_n_redeem_submitted"));

        TextView howtoredeem = findViewById(R.id.howtoredeem);
        howtoredeem.setText(getkey("your_item_delivery"));

        TextView howtoredeemcontent = findViewById(R.id.howtoredeemcontent);
        howtoredeemcontent.setText(getkey("item_delivery"));

        TextView share_miles_tv = findViewById(R.id.share_miles_tv);
        share_miles_tv.setText(getkey("share_us_get_free_miles"));

        Ll_miles_share_googlepuls = findViewById(R.id.invite_earn_googleplus_layout);
        Ll_miles_share_facebook.setOnClickListener(this);
        Ll_miles_share_twitter.setOnClickListener(this);
        Ll_miles_share_googlepuls.setOnClickListener(this);

        imageView = findViewById(R.id.rewardimage);
        price = findViewById(R.id.price);
        giftname = findViewById(R.id.giftname);
        gifttype = findViewById(R.id.gifttype);
        validity = findViewById(R.id.validity);
        redeem = findViewById(R.id.redeem);

        close.setText(getkey("close"));
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Mileagereward_success.this, Navigation_new.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        if (getIntent().hasExtra("rewardid")) {
            rewardid = getIntent().getStringExtra("rewardid");
            Sdistance = getIntent().getStringExtra("Sdistance");
            PostRequestMilesHome(Iconstant.miles_home_url);
        }

    }

    private void PostRequestMilesHome(String Url) {

        final Dialog dialog = new Dialog(Mileagereward_success.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------MilesHome url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);


        System.out.println("-----------MilesHome jsonParams--------------" + jsonParams);


        ServiceRequest mRequest = new ServiceRequest(Mileagereward_success.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------MilesHome reponse-------------------" + response);

                String Sstatus = "", Stotal_distance = "", Smileage_expiry = "", Sdistance_unit = "", Smileage_timeline = "", Sfree_mileage = "",
                        Ssubject = "", Smessage = "", Surl = "", Salertmsg = "";
                dialog.dismiss();
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {



                        JSONObject jsondata = object.getJSONObject("response");

                        if (jsondata.length() > 0) {

                            Stotal_distance = jsondata.getString("total_distance");
                            Smileage_expiry = jsondata.getString("mileage_expiry");
                            Sdistance_unit = jsondata.getString("distance_unit");
                            Miles_shareImage = jsondata.getString("mileage_timeline");
                            Sfree_mileage = jsondata.getString("free_mileage");
                            Miles_shareSubject = jsondata.getString("subject");
                            Miles_shareMessage = jsondata.getString("message");
                            facebook = jsondata.getString("facebook");
                            google = jsondata.getString("google");
                            Miles_sharelink = jsondata.getString("url");

                            isdataPresent = true;

                            if (isInternetPresent) {

                                PostRequestForApplyRewards(Iconstant.miles_rewards_apply_url);
                            } else {
                                Alert(getkey("action_error"), getkey("alert_nointernet"));
                            }
                        }
                    } else {
                        Salertmsg = object.getString("response");
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });


    }

    private void PostRequestForApplyRewards(String Url) {

        final Dialog dialog = new Dialog(Mileagereward_success.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("reward_id", rewardid);
        jsonParams.put("distance", Sdistance);

        ServiceRequest mRequest = new ServiceRequest(Mileagereward_success.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {


            @Override
            public void onCompleteListener(String response) {
                dialog.dismiss();
                String Sstatus = "", Sremaining_distance = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        hidell.setVisibility(View.VISIBLE);
                        JSONObject data_reponse_object = object.getJSONObject("response");
//                        Sremaining_distance = data_reponse_object.getString("remaining_distance");
                        price.setText(getkey("price")+ data_reponse_object.getString("distance"));

                        gifttype.setText(getkey("gifttype") + data_reponse_object.getJSONObject("details").getString("type"));
                        validity.setText(getkey("validi")+ data_reponse_object.getJSONObject("details").getJSONObject("validity").getString("valid_to"));


                        if (data_reponse_object.getJSONObject("details").getString("type").equalsIgnoreCase("Gift")) {
                            redeem.setVisibility(View.VISIBLE);
                            giftname.setText(getkey("giftname")+data_reponse_object.getJSONObject("details").getString("gift_name"));
                            Picasso.with(Mileagereward_success.this).load(data_reponse_object.getJSONObject("details").getString("gift_image")).placeholder(getResources().getDrawable(R.drawable.nogift_coupen_code)).error(getResources().getDrawable(R.drawable.nogift_coupen_code)).into(imageView);
                            redeem.setText(getkey("sentti")+data_reponse_object.getJSONObject("details").getString("user_name")+"\n"+"Address : "+data_reponse_object.getJSONObject("details").getJSONObject("user_address").getString("locality")+" ,"+data_reponse_object.getJSONObject("details").getJSONObject("user_address").getString("state")+" ,"+data_reponse_object.getJSONObject("details").getJSONObject("user_address").getString("pincode"));
                        } else {
                            giftname.setVisibility(View.GONE);
                            redeem.setVisibility(View.INVISIBLE);
                        }


                    } else {
                        Alert2(getkey("alert_label_title"), object.getString("response"));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    onBackPressed();
                }

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
                onBackPressed();
            }
        });


    }

    //----------Share Image and Text on Twitter Method--------
    protected void shareTwitter(String text, Uri image) {
        boolean isAppInstalled = appInstalledOrNot("com.twitter.android");

        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_STREAM, image);
            intent.setType("image/jpeg");
            intent.setPackage("com.twitter.android");

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                Alert(getkey("alert_label_title"), getkey("invite_earn_label_twitter_not_installed"));
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_twitter_not_installed"), "https://play.google.com/store/apps/details?id=com.twitter.android");
        }
    }

    protected void shareInstagramelink(String text, Uri image) {
        boolean isAppInstalled = appInstalledOrNot("com.instagram.android");

        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            // intent.putExtra(Intent.EXTRA_STREAM, "");
            intent.setType("image/jpeg");
            intent.setPackage("com.instagram.android");

            // Add the URI to the Intent.
            intent.putExtra(Intent.EXTRA_STREAM, image);

            try {
                //startActivity(intent);
                startActivity(Intent.createChooser(intent, "Share to"));
            } catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"), getkey("invite_earn_label_instagram_not_installed"), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_instagram_not_installed"), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
        }
    }

    @Override
    public void onClick(View v) {


        if (v == Ll_miles_share_facebook) {

            if (isInternetPresent) {
                try {
                    if (isFBLoggedIn()) {
                        ShareDialog();
                    } else {
                        LoginManager.getInstance().logInWithPublishPermissions(
                                Mileagereward_success.this,
                                Arrays.asList("publish_actions"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
            }


        } else if (v == Ll_miles_share_twitter) {

            if (facebook.equalsIgnoreCase("1")) {

                Uri imageUri = null;
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.miles_timeline_post), null, null));
                } catch (NullPointerException e) {
                }
                shareTwitter(Miles_shareMessage, imageUri);
            } else {
                Toast.makeText(Mileagereward_success.this, getkey("alreadyusermoiles"), Toast.LENGTH_SHORT).show();
            }


        } else if (v == Ll_miles_share_googlepuls) {

            if (google.equalsIgnoreCase("1")) {
                Uri imageUri = null;
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.miles_timeline_post), null, null));
                } catch (NullPointerException e) {
                }

//                    shareGPlus(Miles_shareMessage, imageUri);
                shareInstagramelink(Miles_shareMessage, imageUri);

            } else {
                Toast.makeText(Mileagereward_success.this,getkey("alreadyusermoiles"), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void Alert1(String title, String alert, final String url) {

        final PkDialog mDialog = new PkDialog(Mileagereward_success.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        mDialog.show();

    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case GOOGLE_PLUS_SHARE_REQUEST_CODE:

                if (resultCode == RESULT_OK) {
                    PostapplyMilesHome(Iconstant.miles_rewards_apply_milageurl, "google");
                    // Google Plus Share Success
                }

                break;

        }

        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();


        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                ShareDialog();

            }

            @Override
            public void onCancel() {
                Toast.makeText(Mileagereward_success.this, getkey("logincancel"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(Mileagereward_success.this, getkey("loginerror"), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void ShareDialog() {

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                System.out.println("Result = " + result.getPostId());
//                Toast.makeText(MilesHome.this, "FB Share success", Toast.LENGTH_SHORT).show();

                if (isInternetPresent) {
                    PostapplyMilesHome(Iconstant.miles_rewards_apply_milageurl, "facebook");
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }

            }

            @Override
            public void onCancel() {
                Toast.makeText(Mileagereward_success.this, getkey("fb_shared"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(Mileagereward_success.this, getkey("fb_error"), Toast.LENGTH_SHORT).show();
            }
        }, REQUEST_CODE_FACEBOOK_SHARE);


        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(Miles_sharelink))
                .build();

        shareDialog.show(content);


    }

    private void sendBroadcast() {
        Intent local = new Intent();
        local.setAction("com.app.MilesPagePage.refreshhomePage");
        local.putExtra("refresh", "yes");
        sendBroadcast(local);
    }

    public boolean isFBLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Mileagereward_success.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }




    //--------------Alert Method-----------
    private void Alert2(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Mileagereward_success.this);
        mDialog.setDialogTitle(title);
        mDialog.setCancelOnTouchOutside(false);
        mDialog.setCancelble(false);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                finish();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                finish();
            }
        });
        mDialog.show();
    }

    private void PostapplyMilesHome(String Url, String media) {

        final Dialog dialog = new Dialog(Mileagereward_success.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------MILESAPPLY URL--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("media", media);

        System.out.println("-----------MILESAPPLY jsonParams--------------" + jsonParams);


        ServiceRequest mRequest = new ServiceRequest(Mileagereward_success.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------MilesHome reponse-------------------" + response);
                if (dialog != null) {
                    dialog.dismiss();
                }
                String Sstatus = "", Salertmsg = "", Smilage = "", SdistanceUnit = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        sendBroadcast();
                        //  JSONObject jsondata = object.getJSONObject("response");
                        Salertmsg = object.getString("response");
                        Smilage = object.getString("mileage");
                        SdistanceUnit = object.getString("distance_unit");
//                        Tv_miles_km.setText(Smilage + " " + SdistanceUnit);
                    } else {
                        Salertmsg = object.getString("response");
                    }
                } catch (JSONException e) {

                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    Alert(getkey("action_success"), Salertmsg);
                } else {
                    Alert(getkey("action_error"), Salertmsg);

                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });


    }


    @Override
    public void onBackPressed() {

    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
