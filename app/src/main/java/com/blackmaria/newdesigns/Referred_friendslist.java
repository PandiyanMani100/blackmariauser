package com.blackmaria.newdesigns;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.pojo.Refer_friendslist;
import com.blackmaria.R;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Referred_friendslist extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout friendslistview;
    private ImageView img_back;
    private SessionManager sessionManager;
    private String UserID = "";
    private ArrayList<Refer_friendslist> refer_friendslists = new ArrayList<>();
    private TextView nolist;
LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referred_friendslist);
        mhelper = new LanguageDb(this);
        initView();
    }

    private void initView() {
        friendslistview = findViewById(R.id.friendslistview);
        nolist = findViewById(R.id.nolist);
        nolist.setText(getkey("no_friends_yet"));
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        sessionManager = new SessionManager(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);


        TextView myreferalcode = findViewById(R.id.myreferalcode);
        myreferalcode.setText(getkey("friends_list"));


        getfriendslist(Iconstant.refered_friendslist);
    }

    private void getfriendslist(String Url) {
        final Dialog dialog = new Dialog(Referred_friendslist.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        ServiceRequest mRequest = new ServiceRequest(Referred_friendslist.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------displayInvite_Request Response----------------" + response);

                try {

                    JSONObject object = new JSONObject(response);
                    String Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        refer_friendslists.clear();
                        JSONArray friedsarray = object.getJSONObject("response").getJSONArray("friend_list");
                        for (int i = 0; i <= friedsarray.length() - 1; i++) {
                            Refer_friendslist refer_friendslist = new Refer_friendslist();
                            refer_friendslist.setId(friedsarray.getJSONObject(i).getString("id"));
                            refer_friendslist.setUser_name(friedsarray.getJSONObject(i).getString("user_name"));
                            refer_friendslist.setImage(friedsarray.getJSONObject(i).getString("image"));
                            refer_friendslist.setNo_of_rides(friedsarray.getJSONObject(i).getInt("no_of_rides"));
                            refer_friendslist.setReference_date(friedsarray.getJSONObject(i).getString("reference_date"));
                            refer_friendslists.add(refer_friendslist);
                        }
                        nolist.setVisibility(View.GONE);
                        friendslist(refer_friendslists);
                    } else {
                        nolist.setVisibility(View.VISIBLE);
                        String message = getkey("no_fnd_availbe");
                        Alert(getkey("action_ok"), message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Referred_friendslist.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setDialogUpper(false);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }


    private void friendslist(final ArrayList<Refer_friendslist> refer_friendslists) {
        try {
            friendslistview.removeAllViews();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= refer_friendslists.size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.refered_friendslist_adapter, null);

            ConstraintLayout constrain = view.findViewById(R.id.constrain);
            RoundedImageView imageView = view.findViewById(R.id.userimage);
            TextView username = view.findViewById(R.id.username);
            TextView signupdate = view.findViewById(R.id.signupdate);
            TextView trips = view.findViewById(R.id.trips);
            username.setText(refer_friendslists.get(i).getUser_name());
            signupdate.setText(getkey("signup")+ refer_friendslists.get(i).getReference_date());
            if (String.valueOf(refer_friendslists.get(i).getNo_of_rides()).equalsIgnoreCase("0")) {
                trips.setText(getkey("no_trips"));
            } else {
                trips.setText(String.valueOf(refer_friendslists.get(i).getNo_of_rides()));
            }

            final String id = refer_friendslists.get(i).getId();

            Picasso.with(getApplicationContext()).load(String.valueOf(refer_friendslists.get(i).getImage())).placeholder(R.drawable.usericon_getpaid).error(R.drawable.usericon_getpaid).into(imageView);

            constrain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Referred_friendslist.this, Refer_friend_statement.class);
                    i.putExtra("friendid", id);
                    startActivity(i);
                }
            });

            friendslistview.addView(view);
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
