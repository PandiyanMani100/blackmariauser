package com.blackmaria.newdesigns;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Notification_subpage extends AppCompatActivity {

    private ImageView img_back, image;
    private TextView txt_page_title, content;
    private String UserId = "";
    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private Boolean isInternetPresent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_subpage);


        initView();
    }

    private void initView() {
        sessionManager = new SessionManager(Notification_subpage.this);
        cd = new ConnectionDetector(Notification_subpage.this);
        isInternetPresent = cd.isConnectingToInternet();
        HashMap<String, String> user = sessionManager.getUserDetails();
        UserId = user.get(SessionManager.KEY_USERID);
        img_back = findViewById(R.id.img_back);
        image = findViewById(R.id.image);
        content = findViewById(R.id.content);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getIntent().hasExtra("image")) {

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.be_partner_banner)
                    .error(R.drawable.be_partner_banner)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(Notification_subpage.this).load(getIntent().getStringExtra("image")).apply(options).into(image);
            content.setText(getIntent().getStringExtra("message"));

            postData2(getIntent().getStringExtra("id"));
        }


    }


    private void postData2(final String notifiId) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", UserId);
        jsonParams.put("notification_id", notifiId);

        if (isInternetPresent) {
            postRequest_MessageUpdate(Iconstant.message_update_list, jsonParams);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
        }
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Notification_subpage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void postRequest_MessageUpdate(String Url, HashMap<String, String> jsonParams) {

        final Dialog dialog = new Dialog(Notification_subpage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));
        System.out.println("-------------Message  Url----------------" + Url);


        ServiceRequest mRequest = new ServiceRequest(Notification_subpage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Message  Response----------------" + response);

                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "", ScurrencySymbol = "", Str_total_amount = "", Str_total_transaction = "";
                if (dialog != null) {
                    dialog.dismiss();
                }
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        String Sresponse = object.getString("response");
                        //  Alert(getResources().getString(R.string.action_success), Sresponse);
                    } else {
                        String Sresponse = object.getString("response");
                        // Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });


    }
}
