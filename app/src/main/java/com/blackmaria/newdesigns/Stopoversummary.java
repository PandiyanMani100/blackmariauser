package com.blackmaria.newdesigns;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.MultiStop_summary_Adapter;
import com.blackmaria.pojo.Geteta;
import com.blackmaria.pojo.MultiDropPojo;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.volley.ServiceRequest;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class Stopoversummary extends AppCompatActivity {

    private TextView cancel_multi, estimated_time;
    private RelativeLayout add_stop_over_layout, multistop_proceed_layout;
    private ExpandableHeightListView multidroplistView;
    private MultiStop_summary_Adapter adapter;
    private ArrayList<MultiDropPojo> multiDropList;
    private Geteta geteta;
    private SessionManager sessionManager;
    private LinearLayout lv_locations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopoversummary);

        init();
    }

    private void init() {
        sessionManager = new SessionManager(this);
        multiDropList = new ArrayList<>();
        cancel_multi = findViewById(R.id.cancel_multi);
        lv_locations = findViewById(R.id.lv_locations);
        estimated_time = findViewById(R.id.estimated_time);
        add_stop_over_layout = findViewById(R.id.add_stop_over_layout);
        multistop_proceed_layout = findViewById(R.id.multistop_proceed_layout);
        multidroplistView = findViewById(R.id.multidroplist);

        Intent i = getIntent();
        if (i.hasExtra("homepage")) {
            try {
                geteta = new Geteta();
                String object = i.getStringExtra("homepage");
                Type listType = new TypeToken<Geteta>() {
                }.getType();
                geteta = new GsonBuilder().create().fromJson(object.toString(), listType);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        lv_locations.removeAllViews();
        for (int j = 0; j <= geteta.getResponse().getEta().getSummary_arr().size() - 1; j++) {
            View view = getLayoutInflater().inflate(R.layout.multidrop_single_summary, null);
            TextView dropoff_lable = view.findViewById(R.id.dropoff_lable);
            TextView booking_drop_address_textview = view.findViewById(R.id.booking_drop_address_textview);
            TextView minutes = view.findViewById(R.id.minutes);
            TextView distance = view.findViewById(R.id.distance);
            TextView eta = view.findViewById(R.id.eta);
            TextView fare = view.findViewById(R.id.fare);
            TextView waitingcharge = view.findViewById(R.id.waitingcharge);
//            TextView stopoverline = view.findViewById(R.id.stopoverline);
            dropoff_lable.setText(geteta.getResponse().getEta().getSummary_arr().get(j).getText());
            booking_drop_address_textview.setText(geteta.getResponse().getEta().getSummary_arr().get(j).getLocaton());

            if (!geteta.getResponse().getEta().getSummary_arr().get(j).getDistance().equals("")) {
                minutes.setText(geteta.getResponse().getEta().getSummary_arr().get(j).getWait_time() +getResources().getString(R.string.minstus)+ getResources().getString(R.string.stop));
                distance.setText(getResources().getString(R.string.distnace) + geteta.getResponse().getEta().getSummary_arr().get(j).getDistance());
                eta.setText(getResources().getString(R.string.eta)+ geteta.getResponse().getEta().getSummary_arr().get(j).getDuration());
                fare.setText(getResources().getString(R.string.fae) + sessionManager.getCurrency() + " " + geteta.getResponse().getEta().getSummary_arr().get(j).getFare());
                waitingcharge.setText(getResources().getString(R.string.waitingcharge) + sessionManager.getCurrency() + " " + geteta.getResponse().getEta().getSummary_arr().get(j).getWait_time_charges());
            }

            lv_locations.addView(view);
        }

//        adapter = new MultiStop_summary_Adapter(Stopoversummary.this, geteta.getResponse().getEta().getSummary_arr());
//        multidroplistView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();

        estimated_time.setText(getResources().getString(R.string.estimated) + sessionManager.getCurrency() + " " + geteta.getResponse().getEta().getRide_amount());

        cancel_multi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(Stopoversummary.this, Navigation_new.class);
//                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        multistop_proceed_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    private void postRequest(String Url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", "");
        jsonParams.put("device", "ANDROID");

        ServiceRequest mRequest = new ServiceRequest(Stopoversummary.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

            }

            @Override
            public void onErrorListener() {
            }
        });
    }
}
