package com.blackmaria.newdesigns;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.Navigation_new;
import com.blackmaria.R;
import com.blackmaria.SignUpPage;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.xmpp.XmppService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Accountclosed extends AppCompatActivity {

    private TextView accountno, name, mobile, withdrawnamountvalue, feeschargesvalue, receivedmountvalue, tracsactionno, RL_close_account;
    private ImageView bankimage;
    private String UserID = "";
    private SessionManager sessionManager;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountclosed);
        sessionManager = new SessionManager(this);
        mhelper = new LanguageDb(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        initView();

        if (getIntent().hasExtra("mode")) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", UserID);
            jsonParams.put("amount", sessionManager.getaccountcloseamount());
            jsonParams.put("payment_mode", getIntent().getStringExtra("mode"));

            closeaccount(Iconstant.closeaccount_request, jsonParams);
        }
    }

    private void initView() {
        accountno = findViewById(R.id.accountno);
        name = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        withdrawnamountvalue = findViewById(R.id.withdrawnamountvalue);
        feeschargesvalue = findViewById(R.id.feeschargesvalue);
        receivedmountvalue = findViewById(R.id.receivedmountvalue);
        tracsactionno = findViewById(R.id.tracsactionno);
        RL_close_account = findViewById(R.id.RL_close_account);
        RL_close_account.setText(getkey("close"));
        bankimage = findViewById(R.id.bankimage);

       TextView accountclosed = findViewById(R.id.accountclosed);
        accountclosed.setText(getkey("account_closed"));

        TextView payto = findViewById(R.id.payto);
        payto.setText(getkey("pay_too"));

        TextView withdrawnamount = findViewById(R.id.withdrawnamount);
        withdrawnamount.setText(getkey("withdrawn_amount"));

        TextView feescharges = findViewById(R.id.feescharges);
        feescharges.setText(getkey("feescharges"));

        TextView receivedmount = findViewById(R.id.receivedmount);
        receivedmount.setText(getkey("received_amount"));

        TextView missu = findViewById(R.id.missu);
        missu.setText(getkey("thank_you_and_we_re_gonna_miss_you"));



        RL_close_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.logoutUser();
                stopService(new Intent(Accountclosed.this, XmppService.class));
                Intent intent = new Intent(Accountclosed.this, SignUpPage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
    }

    private void closeaccount(String Url, final HashMap<String, String> jsonParams) {

        final Dialog dialog = new Dialog(Accountclosed.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ServiceRequest mRequest = new ServiceRequest(Accountclosed.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                String Sstatus = "", Smessage = "";
                dialog.dismiss();
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        withdrawnamountvalue.setText(sessionManager.getCurrency() + " " + object.getString("withdrawal_amount"));
                        feeschargesvalue.setText(sessionManager.getCurrency() + " " + object.getString("transaction_fee"));
                        receivedmountvalue.setText(sessionManager.getCurrency() + " " + object.getString("receive_amount"));
                        name.setText(object.getString("user_name"));
                        mobile.setText(object.getString("phone_number"));

                        if (object.has("id_number")) {
                            if (!object.getString("id_number").equalsIgnoreCase("")) {
                                accountno.setText("Identification :" + object.getString("id_number"));
                                accountno.setVisibility(View.VISIBLE);
                            } else {
                                accountno.setVisibility(View.GONE);
                            }
                        }

                        if (object.has("image")) {
                            if (!object.getString("image").equalsIgnoreCase("")) {
                                bankimage.setVisibility(View.VISIBLE);
                                RequestOptions options = new RequestOptions()
                                        .centerCrop()
                                        .placeholder(R.drawable.may_bank)
                                        .error(R.drawable.may_bank)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL);
                                Glide.with(Accountclosed.this).load(object.getString("image")).apply(options).into(bankimage);
                            } else {
                                bankimage.setVisibility(View.INVISIBLE);
                            }
                        }

                    } else {
                        Alert(getkey("action_error"), object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();

                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Accountclosed.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(Accountclosed.this, Navigation_new.class);
                startActivity(intent);
                finish();
            }
        });
        mDialog.show();

    }

    @Override
    public void onBackPressed() {

    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
