package com.blackmaria.dbconnection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.blackmaria.pojo.myride_pojo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Ridedetails_db extends SQLiteOpenHelper {

    private SQLiteDatabase mDatabaseInstance;
    public static final String DB_NAME = "BlackmariaUser";
    private static final int DB_VERSION = 1;

    public Context mContext;
    public Gson gson;
    public GsonBuilder gsonBuilder;

    private final String TABLE_RIDES = "userrides";
    private final String TABLE_RIDES_DETAIL = "userrides_detail";


    private final String COLUMN_OID = "OID";
    private final String COLUMN_RIDE_ID = "ride_id";
    private final String COLUMN_RIDE_DATA = "ride_data";
    private final String COLUMN_RIDE_STATUS = "ride_status";
    private final String COLUMN_RIDE_DETAIL = "ride_detail";
    private final String COLUMN_RIDE_DATETIME = "ride_date_time";


    String CREATE_TABLE_USER_RIDES = "CREATE TABLE " + TABLE_RIDES + "(" + COLUMN_OID
            + " INTEGER PRIMARY KEY," + COLUMN_RIDE_ID + " TEXT UNIQUE," + COLUMN_RIDE_DATA
            + " TEXT," + COLUMN_RIDE_STATUS + " TEXT," + COLUMN_RIDE_DATETIME + " INTEGER)";

    String CREATE_TABLE_USER_RIDE_DETAILVIEW = "CREATE TABLE " + TABLE_RIDES_DETAIL + "(" + COLUMN_OID
            + " INTEGER PRIMARY KEY," + COLUMN_RIDE_ID + " TEXT UNIQUE," + COLUMN_RIDE_DETAIL
            + " TEXT)";

    public Ridedetails_db(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

        this.mContext = context;
        gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }


    private SQLiteDatabase getDatabaseInstance() {
        if (mDatabaseInstance == null) {
            mDatabaseInstance = getWritableDatabase();
        }

        if (!mDatabaseInstance.isOpen()) {
            mDatabaseInstance = getWritableDatabase();
        }

        return mDatabaseInstance;
    }

    public void close() {
        if (mDatabaseInstance != null && mDatabaseInstance.isOpen()) {
            mDatabaseInstance.close();
        }
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USER_RIDES);
        db.execSQL(CREATE_TABLE_USER_RIDE_DETAILVIEW);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RIDES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RIDES_DETAIL);

        // Create tables again
        onCreate(db);
    }


    public void putride_data(final String userId, JSONObject ride_data) {
        String selectQuery = "SELECT * FROM " + TABLE_RIDES + " WHERE " + COLUMN_RIDE_ID + "='"
                + userId + "'";

        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_ridedata(userId, ride_data);

            } else {
                selectCur.close();
                insert_ridedata(userId, ride_data);
            }
        }
    }

    public void putride_data_detailview(final String userId, JSONObject ride_data) {
        String selectQuery = "SELECT * FROM " + TABLE_RIDES_DETAIL + " WHERE " + COLUMN_RIDE_ID + "='"
                + userId + "'";

        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_ridedata_detailview(userId, ride_data);

            } else {
                selectCur.close();
                insert_ridedata_detailview(userId, ride_data);
            }
        }
    }


    private void insert_ridedata(String rideid, JSONObject ride_data) {
        Log.d("rideid", rideid);
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_RIDE_ID, rideid);
            values.put(COLUMN_RIDE_DATA, ride_data.toString());
            values.put(COLUMN_RIDE_DATETIME, getdatatime_milliseconds(ride_data.getString("datetime")));

            getDatabaseInstance().insert(TABLE_RIDES, null, values);
        } catch (NullPointerException e) {
        } catch (Exception e) {
            System.out.println("---------insert_ridedata-----"+e.toString());
        }
    }


    private void insert_ridedata_detailview(String rideid, JSONObject ride_data) {
        Log.d("rideid", rideid);
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_RIDE_ID, rideid);
            values.put(COLUMN_RIDE_DETAIL, ride_data.toString());

            getDatabaseInstance().insert(TABLE_RIDES_DETAIL, null, values);
        } catch (NullPointerException e) {

        } catch (Exception e) {
            System.out.println("---------insert_ridedata_detailview-----"+e.toString());
        }
    }


    private void update_ridedata(String rideid, JSONObject ride_data) {
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_RIDE_ID, rideid);
            values.put(COLUMN_RIDE_DATA, ride_data.toString());
            values.put(COLUMN_RIDE_DATETIME, getdatatime_milliseconds(ride_data.getString("datetime")));

            getDatabaseInstance().update(TABLE_RIDES, values, COLUMN_RIDE_ID + "='" + rideid + "'", null);
        } catch (NullPointerException e) {

        } catch (Exception e) {
            System.out.println("---------update_ridedata-----"+e.toString());
        }
    }

    private void update_ridedata_detailview(String rideid, JSONObject ride_data) {
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_RIDE_ID, rideid);
            values.put(COLUMN_RIDE_DETAIL, ride_data.toString());

            getDatabaseInstance().update(TABLE_RIDES_DETAIL, values, COLUMN_RIDE_ID + "='" + rideid + "'", null);
        } catch (NullPointerException e) {

        } catch (Exception e) {
            System.out.println("---------update_ridedata_detailview-----"+e.toString());
        }
    }

    public ArrayList<myride_pojo> getridewith_limit(String datetime, int limit) {

        String query;
        long timeStamp;
        try {
            timeStamp = getdatatime_milliseconds(datetime);
        } catch (NumberFormatException e) {
            timeStamp = 0;
        }

        query = "SELECT * FROM " + TABLE_RIDES + " WHERE " + COLUMN_RIDE_DATETIME + ">" + timeStamp + " ORDER BY 1 DESC LIMIT " + limit;

        ArrayList<myride_pojo> rides = new ArrayList<>();
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(COLUMN_RIDE_DATA));
                myride_pojo messageItemChat = gson.fromJson(data, myride_pojo.class);
                rides.add(messageItemChat);
            }
            cursor.close();
        }
        return rides;
    }

    public JSONObject getridedetailView(String rideid) {
        JSONObject jsonObject_ridedata = null;
        String query;

        query = "SELECT * FROM " + TABLE_RIDES + " WHERE " + COLUMN_RIDE_ID + "='"
                + rideid + "'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(COLUMN_RIDE_DETAIL));
                try {
                    jsonObject_ridedata = new JSONObject(data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            cursor.close();
        }
        return jsonObject_ridedata;
    }


    public long getdatatime_milliseconds(String datetime) {
//        String givenDateString = "Tue Apr 23 16:08:28 GMT+05:30 2013";
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy");
        long timeInMilliseconds = 0;
        try {
            Date mDate = sdf.parse(datetime);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

}
