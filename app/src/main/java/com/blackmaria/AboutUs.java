package com.blackmaria;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.newdesigns.Helpfaq;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;

import java.util.HashMap;


public class AboutUs extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back, Iv_facebook, Iv_twitter, Iv_instagram, callicon;
    private TextView helps;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager sessionManager;
    LanguageDb mhelper;
    private ServiceRequest mRequest;
    AppInfoSessionManager SappInfo_Session;
    private String sDriveID = "", basicUserProfileUpdateStatus = "", UserProfileUpdateStatus = "", UserID = "";
    private boolean isDataPresent = false;
//sou
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aboutus_constrain);
        mhelper = new LanguageDb(this);
        initialize();
    }

    private void initialize() {
        sessionManager = new SessionManager(AboutUs.this);
        cd = new ConnectionDetector(AboutUs.this);
        isInternetPresent = cd.isConnectingToInternet();
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_USERID);
        HashMap<String, String> info = sessionManager.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        HashMap<String, String> user1 = sessionManager.getUserBasicProfile();
        basicUserProfileUpdateStatus = user1.get(SessionManager.BASIC_PROFILE);
        HashMap<String, String> user2 = sessionManager.getUserCompleteProfile();
        UserProfileUpdateStatus = user2.get(SessionManager.COMPLETE_PROFILE);

        Iv_back = (ImageView) findViewById(R.id.img_back);
        helps = (TextView) findViewById(R.id.helps);
        helps.setText(getkey("sd"));
        Iv_facebook = (ImageView) findViewById(R.id.img_facebook);
        Iv_twitter = (ImageView) findViewById(R.id.img_twitter);
        Iv_instagram = (ImageView) findViewById(R.id.img_instagram);
        callicon = (ImageView) findViewById(R.id.callicon);

        TextView terms= (TextView) findViewById(R.id.terms);
        terms.setText(getkey("sidemenu_aboutus"));
        TextView demandlabel= (TextView) findViewById(R.id.demandlabel);
        demandlabel.setText(getkey("demand_responsive_transport_technology"));
        TextView content= (TextView) findViewById(R.id.content);
        content.setText(getkey("about_us_label_about_us_content"));


        Iv_instagram.setOnClickListener(this);
        callicon.setOnClickListener(this);
        Iv_back.setOnClickListener(this);
        Iv_facebook.setOnClickListener(this);
        Iv_twitter.setOnClickListener(this);
        helps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AboutUs.this, Helpfaq.class));
            }
        });


    }
    @Override
    protected void onResume() {
        super.onResume();
        CloseKeyBoard();

    }


    private void CloseKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == Iv_back) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == callicon) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getCustomerServiceNo()));
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getCustomerServiceNo()));
                    startActivity(intent);
                }
            } else if (view == Iv_facebook) {
                String url = "http://facebook.com/blackmaria.co";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                //shareFacebookLink("");
            } else if (view == Iv_twitter) {
                String url = "http://twitter.com/blackmaria_inc";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // shareTwitter("");
            } else if (view == Iv_instagram) {
                String url = "http://instagram.com/blackmaria.inc";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        } else {
            Alert(getkey("alert_nointernet"),getkey("alert_nointernet_message"));
        }

    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(AboutUs.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
