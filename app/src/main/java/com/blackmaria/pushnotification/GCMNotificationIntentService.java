package com.blackmaria.pushnotification;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.blackmaria.FetchingDataActivity;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * @author Anitha
 */

public class GCMNotificationIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    NotificationCompat.Builder builder;
    Context context = GCMNotificationIntentService.this;
    private SessionManager session;

    private String key1 = "", key2 = "", key3 = "", message = "", action = "";

    private String driverID = "", driverName = "", driverEmail = "", driverImage = "", driverRating = "",
            driverLat = "", driverLong = "", driverTime = "", rideID = "", driverMobile = "",
            driverCar_no = "", driverCar_model = "";

    public GCMNotificationIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);
        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
               /* for (int i = 0; i < 3; i++) {
                    Log.d("Messsage Coming... ", "" + (i + 1) + "/5 @ " + SystemClock.elapsedRealtime());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                    }
                }
                Log.e("Message Completed work @ ", "" + SystemClock.elapsedRealtime());*/
                Log.e("Received: ", "" + extras.toString());
                Log.e("Received: ", "" + extras.toString());

                if (extras != null) {

                    /*  Intent in=new Intent(this,FetchingDataActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(in);*/
                    try {

                        if (extras.containsKey("message")) {
                            message = extras.get("message").toString();
                        }
                        if (extras.containsKey("action")) {
                            action = extras.get("action").toString();
                        }
                        if (action.equalsIgnoreCase(Iconstant.pushNotification_Ads)) {
                            if (extras.containsKey(Iconstant.Ads_image) && extras.get(Iconstant.Ads_image).toString() != null && extras.get(Iconstant.Ads_image).toString().length() > 0) {
                                sendNotification(message.toString(), extras.get(Iconstant.Ads_image).toString());
                            } else {
                                sendNotification(message.toString());
                            }
                        } else {
                            sendNotification(message.toString(), extras);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    @SuppressWarnings("deprecation")
    private void sendNotification(String msg, String image) {

        Intent intent = new Intent(this, FetchingDataActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder;
        Resources res = GCMNotificationIntentService.this.getResources();
        Bitmap remote_picture = null;
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        try {
            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(image).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        notiStyle.bigPicture(remote_picture);

        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(msg)
                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setStyle(notiStyle);

        NotificationManager nm =
                (NotificationManager) GCMNotificationIntentService.this.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification n = notificationBuilder.getNotification();

        n.defaults |= Notification.DEFAULT_ALL;
        nm.notify(0, n);

    }

    @SuppressWarnings("deprecation")
    private void sendNotification(String msg) {
        Intent notificationIntent = null;
        notificationIntent = new Intent(GCMNotificationIntentService.this, FetchingDataActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(GCMNotificationIntentService.this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm = (NotificationManager) GCMNotificationIntentService.this.getSystemService(Context.NOTIFICATION_SERVICE);

        Resources res = GCMNotificationIntentService.this.getResources();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(GCMNotificationIntentService.this);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.app_icon)
                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                .setTicker(msg)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setLights(0xffff0000, 100, 2000)
                .setPriority(Notification.DEFAULT_SOUND)
                .setContentText(msg);

        Notification n = builder.getNotification();

        n.defaults |= Notification.DEFAULT_ALL;
        nm.notify(0, n);
    }


    @SuppressWarnings("deprecation")
    private void sendNotification(String msg, Bundle data) {
        Intent notificationIntent = null;
      /*  if (action.equalsIgnoreCase(Iconstant.referalcredit_amount)) {
            notificationIntent = new Intent(GCMNotificationIntentService.this, CloudMoneyHomePage.class);
        } else*/
        {
            notificationIntent = new Intent(GCMNotificationIntentService.this, FetchingDataActivity.class);
            // }
            notificationIntent.putExtras(data);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent contentIntent = PendingIntent.getActivity(GCMNotificationIntentService.this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            NotificationManager nm = (NotificationManager) GCMNotificationIntentService.this.getSystemService(Context.NOTIFICATION_SERVICE);

            Resources res = GCMNotificationIntentService.this.getResources();
            NotificationCompat.Builder builder = new NotificationCompat.Builder(GCMNotificationIntentService.this);
            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.drawable.app_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                    .setTicker(msg)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setLights(0xffff0000, 100, 2000)
                    .setPriority(Notification.DEFAULT_SOUND)
                    .setContentText(msg);
            Notification n = builder.getNotification();
            n.defaults |= Notification.DEFAULT_ALL;
            nm.notify(0, n);

        }
    }
}