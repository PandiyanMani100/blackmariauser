package com.blackmaria.pushnotification;
 
public interface AppConfig
{
    String TAG = "GCM TAG";
    String REG_ID = "regId";
    String APP_VERSION = "app_version";
    String PARSE_APPLICATION_ID = "1053606937345";
    //----Blackmaria key----- AIzaSyBMJSxH1rHEZQC6JYnQwayG4kX5hJWdUig
    //public static final String PARSE_CLIENT_KEY = "AIzaSyBMJSxH1rHEZQC6JYnQwayG4kX5hJWdUig";
    int NOTIFICATION_ID = 100;
}