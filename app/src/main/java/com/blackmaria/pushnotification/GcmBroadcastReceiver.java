package com.blackmaria.pushnotification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import androidx.legacy.content.WakefulBroadcastReceiver;

import android.util.Log;

import com.blackmaria.FetchingDataActivity;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * @author Prem Kumar
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    public static final int NOTIFICATION_ID = 1;
    NotificationCompat.Builder builder;
    Context mcontext;
    private SessionManager session;

    private String key1 = "", key2 = "", key3 = "", message = "", action = "";

    private String driverID = "", driverName = "", driverEmail = "", driverImage = "", driverRating = "",
            driverLat = "", driverLong = "", driverTime = "", rideID = "", driverMobile = "",
            driverCar_no = "", driverCar_model = "";
    Bitmap remote_picture_reciver = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        mcontext = context;
        session = new SessionManager(context);
        System.out.println("-------------received push notification--------------" + intent);
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(mcontext);
        String messageType = gcm.getMessageType(intent);
        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
               /* for (int i = 0; i < 3; i++) {
                    Log.d("Messsage Coming... ", "" + (i + 1) + "/5 @ " + SystemClock.elapsedRealtime());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                    }
                }
                Log.e("Message Completed work @ ", "" + SystemClock.elapsedRealtime());*/
                Log.e("Received: ", "" + extras.toString());
                Log.e("Received: ", "" + extras.toString());

                if (extras != null) {

                    /*  Intent in=new Intent(this,FetchingDataActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(in);*/
                    try {

                        if (extras.containsKey("message")) {
                            message = extras.get("message").toString();
                        }
                        if (extras.containsKey("action")) {
                            action = extras.get("action").toString();
                        }
                        if (action.equalsIgnoreCase(Iconstant.drop_user_key)){
                            session.setWaitedTime(0, 0, "", "");
                        }
                        if (action.equalsIgnoreCase(Iconstant.pushNotification_Ads)) {
                            if (extras.containsKey(Iconstant.Ads_image) && extras.get(Iconstant.Ads_image).toString() != null && extras.get(Iconstant.Ads_image).toString().length() > 0) {
                                sendNotification(message.toString(), extras.get(Iconstant.Ads_image).toString());
                            } else {
                                sendNotification(message.toString());
                            }
                        } else {
                            sendNotification(message.toString(), extras);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
//		ComponentName comp = new ComponentName(context.getPackageName(), GCMNotificationIntentService.class.getName());
//		startWakefulService(context, (intent.setComponent(comp)));
        //setResultCode(Activity.RESULT_OK);
    }

    @SuppressWarnings("deprecation")
    private void sendNotification(String msg) {
        Intent notificationIntent = null;
        notificationIntent = new Intent(mcontext, FetchingDataActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(mcontext, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Resources res = mcontext.getResources();
        Notification.Builder builder;


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new Notification.Builder(mcontext, "1");
            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.drawable.app_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                    .setTicker(msg)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle(mcontext.getString(R.string.app_name))
                    .setLights(0xffff0000, 100, 2000)
                    .setPriority(Notification.PRIORITY_DEFAULT)
                    .setContentText(msg);
        } else {
            builder = new Notification.Builder(mcontext);
            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.drawable.app_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                    .setTicker(msg)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle(mcontext.getString(R.string.app_name))
                    .setLights(0xffff0000, 100, 2000)
                    .setPriority(Notification.PRIORITY_DEFAULT)
                    .setContentText(msg);
        }


        NotificationManager notificationmanager = (NotificationManager) mcontext.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("1",
                    mcontext.getResources().getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationmanager.createNotificationChannel(channel);
        }
        notificationmanager.notify(1, builder.build());
    }


    @SuppressWarnings("deprecation")
    private void sendNotification(String msg, Bundle data) {
        Intent notificationIntent = null;
      /*  if (action.equalsIgnoreCase(Iconstant.referalcredit_amount)) {
            notificationIntent = new Intent(GCMNotificationIntentService.this, CloudMoneyHomePage.class);
        } else*/
        {
            notificationIntent = new Intent(mcontext, FetchingDataActivity.class);
            // }
            notificationIntent.putExtras(data);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent contentIntent = PendingIntent.getActivity(mcontext, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            Resources res = mcontext.getResources();
            Notification.Builder builder;


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder = new Notification.Builder(mcontext, "1");
                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.drawable.app_icon)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                        .setTicker(msg)
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setContentTitle(mcontext.getString(R.string.app_name))
                        .setLights(0xffff0000, 100, 2000)
                        .setPriority(Notification.PRIORITY_DEFAULT)
                        .setContentText(msg);
            } else {
                builder = new Notification.Builder(mcontext);
                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.drawable.app_icon)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                        .setTicker(msg)
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setContentTitle(mcontext.getString(R.string.app_name))
                        .setLights(0xffff0000, 100, 2000)
                        .setPriority(Notification.PRIORITY_DEFAULT)
                        .setContentText(msg);
            }


            NotificationManager notificationmanager = (NotificationManager) mcontext.getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel("1",
                        mcontext.getResources().getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                notificationmanager.createNotificationChannel(channel);
            }
            notificationmanager.notify(1, builder.build());
//			NotificationManager nm = (NotificationManager) mcontext.getSystemService(Context.NOTIFICATION_SERVICE);
//
//			Resources res = mcontext.getResources();
//			NotificationCompat.Builder builder = new NotificationCompat.Builder(mcontext);
//			builder.setContentIntent(contentIntent)
//					.setSmallIcon(R.drawable.app_icon)
//					.setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
//					.setTicker(msg)
//					.setWhen(System.currentTimeMillis())
//					.setAutoCancel(true)
//					.setContentTitle(mcontext.getString(R.string.app_name))
//					.setLights(0xffff0000, 100, 2000)
//					.setPriority(Notification.DEFAULT_SOUND)
//					.setContentText(msg);
//			Notification n = builder.getNotification();
//			n.defaults |= Notification.DEFAULT_ALL;
//			nm.notify(0, n);
        }
    }

    @SuppressWarnings("deprecation")
    private void sendNotification(String msg, final String image) {

        Intent intent = new Intent(mcontext, FetchingDataActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(mcontext, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder;
        Resources res = mcontext.getResources();

        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        try {
//            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(image).getContent());

            remote_picture_reciver = getBitmapFromURL(image);

        } catch (Exception e) {
            e.printStackTrace();
        }

        notiStyle.bigPicture(remote_picture_reciver);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder = new NotificationCompat.Builder(mcontext, "1")
                    .setSmallIcon(R.drawable.app_icon)
                    .setContentTitle(mcontext.getResources().getString(R.string.app_name))
                    .setContentText(msg)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setStyle(notiStyle);
        } else {
            notificationBuilder = new NotificationCompat.Builder(mcontext)
                    .setSmallIcon(R.drawable.app_icon)
                    .setContentTitle(mcontext.getResources().getString(R.string.app_name))
                    .setContentText(msg)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setStyle(notiStyle);
        }


        NotificationManager notificationmanager = (NotificationManager) mcontext.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("1",
                    mcontext.getResources().getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationmanager.createNotificationChannel(channel);
        }
        notificationmanager.notify(1, notificationBuilder.build());


    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }



}
