package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.RateCardAdapter;
import com.blackmaria.adapter.RatecardCategoryAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.RateCard_CarPojo;
import com.blackmaria.pojo.RateCard_CardDisplayPojo;
import com.blackmaria.pojo.RateCard_CityPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.CustomTextViewForm;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


/**
 * Created by user144 on 2/7/2017.
 */

public class RateCardPage extends ActivityHockeyApp {
    private SessionManager session;
    private ImageView back;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ListView Lv_radecard_fare_info;
    LanguageDb mhelper;
    ArrayList<RateCard_CardDisplayPojo> rate_itemList;
    ArrayList<RateCard_CardDisplayPojo> dertails_itemList;
    private boolean isDataAvailable = false;
    private String ScurrencySymbol = "";
    private RateCardAdapter fare_adapter;

    private String Sselected_cityID = "", Sselected_CategoryID = "";
    private ServiceRequest mRequest, car_mRequest, rateCard_mRequest;
    Dialog dialog;
    private boolean isCityAvailable = false;
    private boolean isCarAvailable = false;
    ArrayList<String> city_array = new ArrayList<String>();
    ArrayList<String> car_array = new ArrayList<String>();
    ArrayList<RateCard_CityPojo> city_itemList;
    ArrayList<RateCard_CarPojo> car_itemList;
    private CustomTextViewForm FareTextview;
    ArrayList<String> car_category, carCatId, carImage;
    private RatecardCategoryAdapter adapter;
    //   private HorizontalListView car_listview;
    private RelativeLayout Rl_detail, rateCardCatImage_Rl;/*Rl_car,*/
    // private Button Bt_show_rates;
    private CustomTextView Tv_currency_code;
    private String ScurrencyCode = "";
    private ImageView imge_selected;
    //private ImageLoader imageLoader;
    private CustomTextViewForm cartype_text;
    private RefreshReceiver refreshReceiver;
    private LinearLayout Rl_city;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(RateCardPage.this, MyRideRating.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(RateCardPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ratecard_constrain);
mhelper=new LanguageDb(this);
        initialize();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });


    }

    private void initialize() {

        session = new SessionManager(RateCardPage.this);
        cd = new ConnectionDetector(RateCardPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        city_itemList = new ArrayList<RateCard_CityPojo>();
        car_itemList = new ArrayList<RateCard_CarPojo>();
        car_category = new ArrayList<String>();
        carCatId = new ArrayList<String>();
        carImage = new ArrayList<String>();
        rate_itemList = new ArrayList<RateCard_CardDisplayPojo>();
        dertails_itemList = new ArrayList<RateCard_CardDisplayPojo>();

        back = (ImageView) findViewById(R.id.home_ratecard_page_header_back_layout);
        cartype_text = (CustomTextViewForm) findViewById(R.id.cartype_text);
        Rl_city = (LinearLayout) findViewById(R.id.home_ratecard_page_select_city_layout);
        // Rl_car = (RelativeLayout) findViewById(R.id.home_ratecard_page_select_transport_layout);
        Rl_detail = (RelativeLayout) findViewById(R.id.home_ratecard_page_details_layout);
        FareTextview = (CustomTextViewForm) findViewById(R.id.home_ratecard_page_currency_code_textview);
        FareTextview.setText(getkey("fare"));
        rateCardCatImage_Rl = (RelativeLayout) findViewById(R.id.ratecard_image_layout);
        Calendar now = Calendar.getInstance();   // Gets the current date and time
        int year = now.get(Calendar.YEAR);
        FareTextview.setText(getkey("fare_lable")/* + year*/);
        //Bt_show_rates = (Button) findViewById(R.id.home_ratecard_page_show_rates_btn);
//        Tv_currency_code = (CustomTextView) findViewById(R.id.home_ratecard_page_currency_code_textview);

        //car_listview = (HorizontalListView) findViewById(R.id.home_ratecard_page_select_transport_listview);
        Lv_radecard_fare_info = (ListView) findViewById(R.id.ratecard_listview);
        imge_selected = (ImageView) findViewById(R.id.imge_selected);

        // Lv_radecard_fare_info.setExpanded(true);


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);


        if (isInternetPresent) {
            postRequest_CitySelect(Iconstant.ratecard_select_city_url);
        } else {
            Alert(getkey("alert_label_title"),getkey("alert_nointernet"));
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(RateCardPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //-----------------------City Select Post Request-----------------
    private void postRequest_CitySelect(String Url) {
        dialog = new Dialog(RateCardPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-------------CitySelect Url----------------" + Url);

        mRequest = new ServiceRequest(RateCardPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------CitySelect Response----------------" + response);

                String Sstatus = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {

                            Object check_locations_object = response_object.get("locations");
                            if (check_locations_object instanceof JSONArray) {
                                JSONArray location_array = response_object.getJSONArray("locations");
                                if (location_array.length() > 0) {
                                    city_array.clear();
                                    city_itemList.clear();
                                    city_array.add(getkey("city_or_country_lable1"));


                                    for (int i = 0; i < location_array.length(); i++) {
                                        JSONObject location_object = location_array.getJSONObject(i);
                                        RateCard_CityPojo city_pojo = new RateCard_CityPojo();
                                        city_pojo.setCity_id(location_object.getString("id"));
                                        city_pojo.setCity_name(location_object.getString("city"));

                                        city_array.add(location_object.getString("city"));

                                        city_itemList.add(city_pojo);
                                    }

                                    isCityAvailable = true;
                                } else {
                                    isCityAvailable = false;
                                }
                            }
                        } else {
                            isCityAvailable = false;
                        }
                    } else {
                        isCityAvailable = false;
                    }


                    if (Sstatus.equalsIgnoreCase("1") && isCityAvailable) {
                        initCustomSpinner();
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });

    }

    private void initCustomSpinnerVehicle() {
        Spinner spinnerCustom = (Spinner) findViewById(R.id.ratecard_city_spinner1);
        final TextView Tv_city = (TextView) findViewById(R.id.ratecard_city_textview1);
        Tv_city.setText(getkey("choose_vechile_lable"));

        final TextView selecttouser = (TextView) findViewById(R.id.selecttouser);
        selecttouser.setText(getkey("farestruture"));

        final TextView detailss = (TextView) findViewById(R.id.detailss);
        detailss.setText(getkey("details_lable"));

        // Spinner Drop down elements
        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(RateCardPage.this, car_category);
        spinnerCustom.setAdapter(customSpinnerAdapter);
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    System.out.println("spinner-----------------------------");
                    String item = parent.getItemAtPosition(position).toString();
                    Tv_city.setText(item);
                    int pos = position - 1;
                    cd = new ConnectionDetector(RateCardPage.this);
                    isInternetPresent = cd.isConnectingToInternet();

                    if (!item.equalsIgnoreCase(getkey("choose_vechile_lable")) && position > 0) {
                        if (isInternetPresent) {
                            if (car_mRequest != null) {
                                car_mRequest.cancelRequest();
                            }
                            Sselected_CategoryID = carCatId.get(position);
                            System.out.println("-------------Sselected_cityID-----------------" + Sselected_CategoryID);

                            cartype_text.setText(car_itemList.get(pos).getCategory());
                            String Image = String.valueOf(carImage.get(pos));
                            Picasso.with(RateCardPage.this).load(Image).into(imge_selected);
                            //Bt_show_rates.setVisibility(View.VISIBLE);


                            try {
                                if (!"".equalsIgnoreCase(Sselected_CategoryID)) {
                                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                                    jsonParams.put("location_id", Sselected_cityID);
                                    jsonParams.put("category_id", Sselected_CategoryID);
                                    if (isInternetPresent) {
                                        rateCardCatImage_Rl.setVisibility(View.VISIBLE);
                                        rateCard_displayRequest(Iconstant.ratecard_display_url, jsonParams);
                                    } else {
                                        Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                                    }
                                } else {
                                    Alert(getkey("alert_label_title"), getkey("please_select_transport_lable"));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Alert(getkey("alert_label_title"),getkey("alert_nointernet"));
                        }
                    } else {
                        // Sselected_cityID = "";
                        //   Rl_car.setVisibility(View.GONE);
                        Rl_detail.setVisibility(View.GONE);
                        //Bt_show_rates.setVisibility(View.GONE);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Tv_city.setText(getkey("choose_vechile_lable"));
            }
        });

    }

    private void initCustomSpinner() {
        Spinner spinnerCustom = (Spinner) findViewById(R.id.ratecard_city_spinner);
        final TextView Tv_city = (TextView) findViewById(R.id.ratecard_city_textview);
        Tv_city.setText(getkey("city_or_country_lable1"));

        // Spinner Drop down elements
        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(RateCardPage.this, city_array);
        spinnerCustom.setAdapter(customSpinnerAdapter);
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cartype_text.setText("");
                rateCardCatImage_Rl.setVisibility(View.GONE);
                Rl_detail.setVisibility(View.GONE);
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                Tv_city.setText(item);
                int pos = position - 1;
                cd = new ConnectionDetector(RateCardPage.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (!item.equalsIgnoreCase(getkey("city_or_country_lable1")) && position > 0) {
                    if (isInternetPresent) {
                        if (car_mRequest != null) {
                            car_mRequest.cancelRequest();
                        }
                        Sselected_cityID = city_itemList.get(pos).getCity_id();
                        Sselected_CategoryID = "";
                        System.out.println("-------------Sselected_cityID-----------------" + Sselected_cityID);

                        postRequest_CarSelect(Iconstant.ratecard_select_cartype_url, city_itemList.get(pos).getCity_id());
                    } else {
                        Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                    }
                } else {
                    Sselected_cityID = "";
                    Sselected_CategoryID = "";
                    //   Rl_car.setVisibility(View.GONE);
                    Rl_detail.setVisibility(View.GONE);
                    //Bt_show_rates.setVisibility(View.GONE);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Tv_city.setText(getkey("city_or_country_lable1"));
            }
        });
    }

    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<String> asr;

        public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
            this.asr = asr;
            activity = context;
        }


        public int getCount() {
            return asr.size();
        }

        public Object getItem(int i) {
            return asr.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }


        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(RateCardPage.this);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(16);
            txt.setGravity(Gravity.CENTER);
            txt.setText(asr.get(position));
            txt.setBackground(getResources().getDrawable(R.drawable.ratecard_dropdownbgbg));
            txt.setTextColor(getResources().getColor(R.color.black_color));
            return txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(RateCardPage.this);
            txt.setGravity(Gravity.CENTER);
            txt.setPadding(10, 0, 0, 0);
            txt.setTextSize(14);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt.setText(asr.get(i));
            txt.setTextColor(getResources().getColor(R.color.black_color));
            txt.setVisibility(View.INVISIBLE);
            return txt;
        }

    }


    //-----------------------Car Type Select Post Request-----------------
    private void postRequest_CarSelect(String Url, final String location_id) {
        dialog = new Dialog(RateCardPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-------------CarSelect Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("location_id", location_id);

        car_mRequest = new ServiceRequest(RateCardPage.this);
        car_mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------CarSelect Response----------------" + response);

                String Sstatus = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {

                            Object check_category_object = response_object.get("category");
                            if (check_category_object instanceof JSONArray) {
                                JSONArray location_array = response_object.getJSONArray("category");
                                if (location_array.length() > 0) {
                                    car_array.clear();
                                    car_itemList.clear();
                                    car_category.clear();
                                    carCatId.clear();
                                    carImage.clear();
                                    carCatId.add("-1");
                                    car_category.add(getkey("choose_vechile_lable"));
                                    for (int i = 0; i < location_array.length(); i++) {
                                        JSONObject location_object = location_array.getJSONObject(i);
                                        RateCard_CarPojo car_pojo = new RateCard_CarPojo();
                                        car_pojo.setId(location_object.getString("id"));
                                        carCatId.add(location_object.getString("id"));
                                        car_pojo.setCategory(location_object.getString("category"));
                                        car_category.add(location_object.getString("category"));
                                        car_pojo.setIcon_car_image(location_object.getString("icon_car_image"));
                                        carImage.add(location_object.getString("icon_car_image"));
                                        car_pojo.setSelectStatus("0");

                                        car_array.add(location_object.getString("category"));
                                        car_itemList.add(car_pojo);
                                    }

                                    isCarAvailable = true;
                                } else {
                                    isCarAvailable = false;
                                }
                            }
                        } else {
                            isCarAvailable = false;
                        }
                    } else {
                        isCarAvailable = false;
                    }


                    if (Sstatus.equalsIgnoreCase("1") && isCarAvailable) {
                        //   Rl_car.setVisibility(View.VISIBLE);
                        //Bt_show_rates.setVisibility(View.VISIBLE);
                        Rl_detail.setVisibility(View.GONE);

                        initCustomSpinnerVehicle();
                        // adapter = new RatecardCategoryAdapter(RateCardPage.this, car_itemList);
                        //car_listview.setAdapter(adapter);

                    } else {
                        // Rl_car.setVisibility(View.GONE);
                        Rl_detail.setVisibility(View.GONE);
                        //  Bt_show_rates.setVisibility(View.GONE);
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-----------------------Rate Card Display Post Request-----------------
    private void rateCard_displayRequest(String Url, final HashMap<String, String> jsonParams) {
        dialog = new Dialog(RateCardPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-------------Rate Card Url----------------" + Url);
        System.out.println("-------------Rate Card jsonParams----------------" + jsonParams);

        rateCard_mRequest = new ServiceRequest(RateCardPage.this);
        rateCard_mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Rate Card Response----------------" + response);

                String Sstatus = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {

                            JSONObject ratecard_object = response_object.getJSONObject("ratecard");

                            if (ratecard_object.length() > 0) {
                                ScurrencyCode = ratecard_object.getString("currency");
                                if (!ScurrencyCode.equalsIgnoreCase("")) {
                                    ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ratecard_object.getString("currency"));
                                }
                                Object check_triparr_object = ratecard_object.get("values");
                                if (check_triparr_object instanceof JSONArray) {

                                    JSONArray triparr_array = ratecard_object.getJSONArray("values");
                                    if (triparr_array.length() > 0) {
                                        rate_itemList.clear();

                                        for (int j = 0; j < triparr_array.length(); j++) {
                                            JSONArray jsonArray = triparr_array.getJSONArray(j);
                                            if (jsonArray.length() > 0) {

                                                RateCard_CardDisplayPojo stdrate_pojo = new RateCard_CardDisplayPojo();
                                                String title = "", value = "";
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    JSONObject standard_object = jsonArray.getJSONObject(i);
//                                                    if (i == 0) {
//                                                        title = standard_object.getString("title");
//                                                        if ("1".equalsIgnoreCase(standard_object.getString("currency"))) {
//                                                            value = standard_object.getString("fare");
//                                                        } else {
//                                                            value = standard_object.getString("fare");
//                                                        }
//                                                    } else {
//                                                        title = title + "\n" + standard_object.getString("title");
//                                                        if ("1".equalsIgnoreCase(standard_object.getString("currency"))) {
//                                                            value = standard_object.getString("fare");
//                                                        } else {
//                                                            value = standard_object.getString("fare");
//                                                        }
//                                                    }
                                                    if (i == 0) {
                                                        title = standard_object.getString("title");
                                                        if ("1".equalsIgnoreCase(standard_object.getString("currency"))) {
                                                            value = ScurrencySymbol + "\t\t" + standard_object.getString("fare");
                                                        } else {
                                                            value = " "+"\t\t\t\t\t" + standard_object.getString("fare");
                                                        }
                                                    } else {
                                                        title = title + "\n" + standard_object.getString("title");
                                                        if ("1".equalsIgnoreCase(standard_object.getString("currency"))) {
                                                            value = value + "\n" + ScurrencySymbol + "\t\t" + standard_object.getString("fare");
                                                        } else {
                                                            value = value + "\n" +" "+"\t\t\t\t\t" + standard_object.getString("fare");
                                                        }
                                                    }
                                                    if(standard_object.getString("currency").equals("1")){
                                                        stdrate_pojo.setRate_currencySymbol(ScurrencySymbol);
                                                    }else{
                                                        stdrate_pojo.setRate_currencySymbol("");
                                                    }

                                                }

                                                stdrate_pojo.setRate_title(title);
                                                stdrate_pojo.setRate_value(value);

                                                rate_itemList.add(stdrate_pojo);
                                            }

                                        }
                                        isDataAvailable = true;
                                    } else {
                                        isDataAvailable = false;
                                    }

                                }

                            } else {
                                isDataAvailable = false;
                            }

                        } else {
                            isDataAvailable = false;
                        }
                    } else {
                        isDataAvailable = false;
                    }


                    if (Sstatus.equalsIgnoreCase("1") && isDataAvailable) {

                        Rl_detail.setVisibility(View.VISIBLE);

//                        Tv_currency_code.setText(ScurrencyCode);
                        if (rate_itemList.size() > 0) {
                            rateCardCatImage_Rl.setVisibility(View.VISIBLE);
                            fare_adapter = new RateCardAdapter(RateCardPage.this, rate_itemList);
                            Lv_radecard_fare_info.setAdapter(fare_adapter);
                        }
                    } else {
                        Rl_detail.setVisibility(View.GONE);
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(refreshReceiver);
        super.onDestroy();

    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}

