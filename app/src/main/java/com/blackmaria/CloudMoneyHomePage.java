package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;
import com.blackmaria.adapter.ReferanceUserAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.newdesigns.Referred_friendslist;
import com.blackmaria.pojo.CloudMoneyHomePojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.HorizontalListView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.blackmaria.R.id.pinclick_iv;

/**
 * Created by user144 on 5/18/2017.
 */

public class CloudMoneyHomePage extends ActivityHockeyApp implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks {

    private ImageView imageviewBackarrow;
    private RelativeLayout secondLayout;
    //   private RelativeLayout lay2;
    private TextView amount;
    private TextView pinclickIv;
    LanguageDb mhelper;

//    private Button referalCodeButton;

    private TextView numofReferalUser;
    //    private Button earningAmount;
    private TextView withdrawlAmount;
    private RelativeLayout profileLayout;
    private HorizontalListView referaluserListGridview;
    private TextView viewMoreReferalUser, TVnoreferal;
    String UserID = "";
    /*headtxt1,*/
    private String payPalID = "", withdrawedSuccessMoney = "";
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private SessionManager session;
    ArrayList<CloudMoneyHomePojo> userList;
    ReferanceUserAdapter cloudmoneyAdapter;
    String[] mStrings = null, mStringsName;
    private RefreshReceiver refreshReceiver;
    public static Activity cloudmoneyhomepage;
    EditText Et_Otp1, Et_Otp2, Et_Otp3, Et_Otp4, Et_Otp5;
    RelativeLayout Rl_otp;
    private TextView refered_friends_count, refered_friends_rate, today_earnings, nextpayout;
    private String sSecurePin = "", referral_earning = "";
    private String frompage = "";
    private TextView invite_friends;
    private String proceed_status = "", proceed_error = "";

    private float min_amount, max_maount;

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CloudMoneyHomePage.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CloudMoneyHomePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CloudMoneyHomePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newgetfriends);
        mhelper= new LanguageDb(this);
        Intialize();
        cloudmoneyhomepage = CloudMoneyHomePage.this;
        imageviewBackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (frompage.equalsIgnoreCase("fetching")) {
                    Intent intent = new Intent(CloudMoneyHomePage.this, Navigation_new.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Intent intent = new Intent(CloudMoneyHomePage.this, Navigation_new.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });

//        earningAmount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(CloudMoneyHomePage.this, CloudMoneyCreditDebit.class);
//                intent.putExtra("type", "credit");
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);
//            }
//        });
        withdrawlAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                HelpPopUP();
                Intent intent = new Intent(CloudMoneyHomePage.this, Referred_friendslist.class);
                intent.putExtra("stringarray", mStrings);
                intent.putExtra("stringarrayname", mStringsName);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        viewMoreReferalUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CloudMoneyHomePage.this, Referred_friendslist.class);
                intent.putExtra("stringarray", mStrings);
                intent.putExtra("stringarrayname", mStringsName);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        pinclickIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (proceed_status.equalsIgnoreCase("1")) {
                    if (referral_earning.equalsIgnoreCase("0")) {
                        Alert(getkey("action_error"), getkey("you_dnt_have_widthdraw"));
                    } else {
                        confirmPin();
                        //                        float referal_amount_having = Float.parseFloat(referral_earning);
//                        if (referal_amount_having >= min_amount) {
//                            if (proceed_status.equalsIgnoreCase("1")) {
//
//                            } else {
//                                Alert("Sorry", "You dont have minimum amount to withdraw");
//                            }
//                        } else {
//                            Alert("Sorry", "You dont have minimum amount to withdraw");
//                        }
                    }
                } else {
                    Alert(getkey("action_error"), proceed_error);
                }

            }
        });
        invite_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_share = new Intent(CloudMoneyHomePage.this, InviteAndEarn.class);
                intent_share.putExtra("str_page", getkey("profile_label_menu"));
                startActivity(intent_share);
//                finish();
            }
        });
    }


    private void Intialize() {
        cd = new ConnectionDetector(CloudMoneyHomePage.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(CloudMoneyHomePage.this);
        userList = new ArrayList<CloudMoneyHomePojo>();

        imageviewBackarrow = (ImageView) findViewById(R.id.imageview_backarrow);
//        helpTv = (TextView) findViewById(R.id.help_tv);
        secondLayout = (RelativeLayout) findViewById(R.id.second_layout);
        //lay2 = (RelativeLayout) findViewById(R.id.lay2);
        nextpayout = findViewById(R.id.nextpayout);
        amount = findViewById(R.id.amount);
        pinclickIv = (TextView) findViewById(pinclick_iv);
        pinclickIv.setText(getkey("withdraw_lable"));
        numofReferalUser = findViewById(R.id.numof_referal_user);
//        earningAmount = (Button) findViewById(R.id.earning_amount);
        withdrawlAmount = findViewById(R.id.withdrawl_amount);
        withdrawlAmount.setText(getkey("view_statement"));
        profileLayout = (RelativeLayout) findViewById(R.id.profile_layout);
        referaluserListGridview = (HorizontalListView) findViewById(R.id.referaluser_list_gridview);
        viewMoreReferalUser = findViewById(R.id.view_more_referal_user);
        viewMoreReferalUser.setText(getkey("view_all"));
        TVnoreferal = findViewById(R.id.tv_noreferal);
        TVnoreferal.setText(getkey("no_referrals_yet"));
        invite_friends = (TextView) findViewById(R.id.invite_friends);
        invite_friends.setText(getkey("invite_now"));
        refered_friends_count = findViewById(R.id.refered_friends_count);
        refered_friends_rate = findViewById(R.id.refered_friends_rate);
        today_earnings = findViewById(R.id.today_earnings);

        TextView head = findViewById(R.id.head);
        head.setText(getkey("earnings_balance"));

        TextView totalfriends = findViewById(R.id.totalfriends);
        totalfriends.setText(getkey("total_friends"));

        TextView commisionrate = findViewById(R.id.commisionrate);
        commisionrate.setText(getkey("commission_rate"));

        TextView friendin = findViewById(R.id.friendin);
        friendin.setText(getkey("friends_invited"));

        TextView todayycommsion = findViewById(R.id.todayycommsion);
        todayycommsion.setText(getkey("today_commission"));

        TextView myref = findViewById(R.id.myref);
        myref.setText(getkey("my_referral"));


        if (getIntent().hasExtra("frompage")) {
            frompage = getIntent().getStringExtra("frompage");
        }


        // headtxt1.setTypeface(tf);
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        sSecurePin = session.getSecurePin();
        postData();


    }

    private void Min_amount_withdraw() {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);//"584aa27ccae2aa741a00002f");

        System.out.println("-------------Clodmoneywithdrawl jsonParams----------------" + jsonParams);
        if (isInternetPresent) {
            postRequestCloudMoneyWithdrawlPage(Iconstant.cloudmoney_withdrawl_url, jsonParams);
        } else {
            Alert(getkey("action_error"),getkey("alert_nointernet"));
        }


    }


    private void postRequestCloudMoneyWithdrawlPage(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(CloudMoneyHomePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        System.out.println("-------------cloudmoneyhome url----------------" + Url);

        mRequest = new ServiceRequest(CloudMoneyHomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------cloudmoneyhome_success Response----------------" + response);
                dialogDismiss();
                try {
                    JSONObject object = new JSONObject(response);

                    JSONObject obj_response = object.getJSONObject("response");

                    if (obj_response.length() > 0) {
                        if (!obj_response.getString("min_amount").equalsIgnoreCase("null") && !obj_response.getString("max_amount").equalsIgnoreCase("null")) {
                            min_amount = Integer.parseInt(obj_response.getString("min_amount"));
                            max_maount = Integer.parseInt(obj_response.getString("max_amount"));
                        } else {
                            min_amount = 0;
                            max_maount = 0;
                        }

                        proceed_status = obj_response.getString("proceed_status");
                        proceed_error = obj_response.getString("proceed_error");
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogDismiss();
                }

                dialogDismiss();

            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });


    }


    private void HelpPopUP() {
        final Dialog dialog = new Dialog(CloudMoneyHomePage.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloudmoney_popuphelp);
        // Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
//        CustomTextView custom_text1 = (CustomTextView) dialog.findViewById(R.id.custom_text12);
//        CustomTextView custom_text3 = (CustomTextView) dialog.findViewById(R.id.custom_text31);
//        custom_text1.setTypeface(tf);
//        custom_text3.setTypeface(tf);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView closeImage;
        closeImage = (ImageView) dialog.findViewById(R.id.image_close);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void confirmPin() {
        final Dialog confirmPinDialog = new Dialog(CloudMoneyHomePage.this, R.style.DialogSlideAnim);
        confirmPinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmPinDialog.setCancelable(true);
        confirmPinDialog.setContentView(R.layout.alert_enter_pin);

        final PinEntryEditText Ed_pin = (PinEntryEditText) confirmPinDialog.findViewById(R.id.edt_withdrawal_amount);
        final TextView Tv_forgotPin = (TextView) confirmPinDialog.findViewById(R.id.txt_label_forgot_pin);
        final RelativeLayout RL_confirm = (RelativeLayout) confirmPinDialog.findViewById(R.id.Rl_confirm);


        final TextView txt_label_title = (TextView) confirmPinDialog.findViewById(R.id.txt_label_title);
        txt_label_title.setText(getkey("incentives_page_label_enter_pin"));

        final TextView txt_label_forgot_pin = (TextView) confirmPinDialog.findViewById(R.id.txt_label_forgot_pin);
        txt_label_forgot_pin.setText(getkey("incentives_page_label_forgot_pin"));

        final CheckedTextView txt_label_confirm = (CheckedTextView) confirmPinDialog.findViewById(R.id.txt_label_confirm);
        txt_label_confirm.setText(getkey("action_ok"));

        Tv_forgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    ForgotPinRequest(Iconstant.forgot_pin_url);
                } else {
                    Alert(getkey("alert_nointernet"),getkey("alert_nointernet_message"));
                }

            }
        });

        RL_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    CloseKeyboard(Ed_pin);
                } catch (Exception e) {

                }

                if (confirmPinDialog != null && confirmPinDialog.isShowing()) {
                    confirmPinDialog.dismiss();
                }

                if (sSecurePin.equalsIgnoreCase(Ed_pin.getText().toString())) {
                    postData2();
                  /*  Intent intent = new Intent(CloudMoneyHomePage.this, CloudMoneyWithdrawlPage.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);*/
                    if (confirmPinDialog != null && confirmPinDialog.isShowing()) {
                        confirmPinDialog.dismiss();
                    }


                } else {
                    AlertError(getkey("action_error"), getkey("label_incorrect_pin"));
                }

            }
        });

        confirmPinDialog.show();

    }

    private void postData2() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);//"584aa27ccae2aa741a00002f");
                jsonParams.put("amount", referral_earning);
                jsonParams.put("mode", "Wallet");
                jsonParams.put("paypal_id", "");
                System.out.println("-------------ClodmoneywithdrawlAmount jsonParams----------------" + jsonParams);
                if (isInternetPresent) {
                    postRequestCloudMoneyWithdrawlAmount(Iconstant.cloudmoney_withdrawl_anount, jsonParams);
                } else {
                    Alert(getkey("action_error"), getkey("alert_nointernet"));

                }

            }
        });


    }

    private void postRequestCloudMoneyWithdrawlAmount(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(CloudMoneyHomePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        System.out.println("-------------cloudmoneyhome url----------------" + Url);

        mRequest = new ServiceRequest(CloudMoneyHomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------cloudmoneywithdraw  Response----------------" + response);

                String Sstatus = "", Smessage = "", amount = "", response1 = "", mode = "", currency = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        response1 = object.getString("response");
                        mode = object.getString("mode");
                        amount = object.getString("amount");
                        currency = object.getString("currency");
                        withdrawedSuccessMoney = currency + " " + amount;
                    } else {
                        response1 = object.getString("response");
                    }
                    dialogDismiss();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogDismiss();
                }
                final String finalSstatus = Sstatus;
                final String finalResponse = response1;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (finalSstatus.equalsIgnoreCase("1")) {
                            WIthDrawlResponsePopUP(finalResponse);
                            dialogDismiss();
                        }
                    }
                });
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });


    }

    private void WIthDrawlResponsePopUP(String response1) {
        final Dialog dialog = new Dialog(CloudMoneyHomePage.this, R.style.DialogSlideAnim2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloudmoney_withdrawlresponse);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView closeImage = (ImageView) dialog.findViewById(R.id.img_close);
        CustomTextView withdrawResponse = (CustomTextView) dialog.findViewById(R.id.withdrawal_success_tv);
        TextView InRTv = (TextView) dialog.findViewById(R.id.paypal_conform);
        InRTv.setText(getkey("action_ok"));
        // ImageView home_icon = (ImageView) dialog.findViewById(R.id.home_icon);

        withdrawResponse.setText(response1.toUpperCase() + " " + withdrawedSuccessMoney);
        // InRTv.setText(withdrawedSuccessMoney);

          InRTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                postData();
            }

        });
      /*  home_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CloudMoneyWithdrawlPage.this, Navigation_new.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                dialog.dismiss();
            }

        });*/
        dialog.show();
    }


    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(CloudMoneyHomePage.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    @SuppressLint("WrongConstant")
    private void ForgotPinRequest(String Url) {

        dialog = new Dialog(CloudMoneyHomePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);

        System.out.println("--------------ForgotPinRequest Url-------------------" + Url);
        System.out.println("--------------ForgotPinRequest jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(CloudMoneyHomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                System.out.println("--------------ForgotPinRequest Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("message");

                        if (status.equalsIgnoreCase("1")) {
                            Alert(getkey("action_success"), sResponse);
                        } else {
                            Alert(getkey("action_error"), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //--------------Close KeyBoard Method-----------
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void postData() {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);//"584aa27ccae2aa741a00002f");

        System.out.println("-------------ClodmoneyHome jsonParams----------------" + jsonParams);
        if (isInternetPresent) {
            postRequestCloudMoneyHomePage(Iconstant.earningdshboard_url, jsonParams);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));

        }


    }

    private void postRequestCloudMoneyHomePage(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(CloudMoneyHomePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        System.out.println("-------------cloudmoneyhome url----------------" + Url);

        mRequest = new ServiceRequest(CloudMoneyHomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------cloudmoneyhome_success Response----------------" + response);

                String Sstatus = "", nextpayouts = "", ref_rates = "", Smessage = "", earned_today = "", referral_code = "", currency = "", date_txt = "", referral_count = "", user_name = "", user_image = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject obj_response = object.getJSONObject("response");
                    if (obj_response.length() > 0) {
                        referral_earning = obj_response.getString("referral_earning");
                        if (obj_response.has("referral_code"))
                            referral_code = obj_response.getString("referral_code");
                        if (obj_response.has("currency"))
                            currency = obj_response.getString("currency");
                        if (obj_response.has("date_txt"))
                            date_txt = obj_response.getString("date_txt");
                        if (obj_response.has("referral_count"))
                            referral_count = obj_response.getString("referral_count");
                        if (obj_response.has("ref_rates"))
                            ref_rates = obj_response.getString("ref_rates");
                        if (obj_response.has("earned_today"))
                            earned_today = obj_response.getString("earned_today");
                        if (obj_response.has("proceed_date"))
                            nextpayouts = obj_response.getString("proceed_date");

                        Object intervention = obj_response.get("ref_info");
                        if (intervention instanceof JSONArray) {
                            JSONArray refuserInfo = obj_response.getJSONArray("ref_info");

                            if (refuserInfo.length() > 0) {
                                userList.clear();
                                mStrings = new String[refuserInfo.length()];
                                mStringsName = new String[refuserInfo.length()];
                                for (int i = 0; i < refuserInfo.length(); i++) {
                                    CloudMoneyHomePojo pojo = new CloudMoneyHomePojo();
                                    JSONObject ref_user_list_object = refuserInfo.getJSONObject(i);
                                    user_name = ref_user_list_object.getString("user_name");
                                    mStringsName[i] = ref_user_list_object.getString("user_name");
                                    pojo.setUsername(user_name);
                                    user_image = ref_user_list_object.getString("user_image");
                                    mStrings[i] = ref_user_list_object.getString("user_image");
                                    pojo.setUserimage(user_image);
                                    userList.add(pojo);
                                }
                                viewMoreReferalUser.setVisibility(View.VISIBLE);

                            } else {
                                viewMoreReferalUser.setVisibility(View.GONE);
                                TVnoreferal.setVisibility(View.VISIBLE);

                            }

                        }

                    }

                    dialogDismiss();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogDismiss();
                }

                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {

                    refered_friends_count.setText(referral_count);
                    refered_friends_rate.setText(ref_rates + "%");
                    today_earnings.setText(currency + " " + earned_today);
                    if(nextpayouts == null)
                    nextpayout.setText(getkey("next_payout_un"));
                    else
                        nextpayout.setText(getkey("next_payout_un"));
                    amount.setText(currency + " " + referral_earning);
//                    referalCodeButton.setText("CODE :" + referral_code);
                    numofReferalUser.setText(referral_count);
                    date_txt = date_txt.replace("as today", "");

                    dialogDismiss();

                    if(userList.size() == 0)
                    {
                        referaluserListGridview.setVisibility(View.GONE);
                        TVnoreferal.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        referaluserListGridview.setVisibility(View.VISIBLE);
                        TVnoreferal.setVisibility(View.GONE);
                        cloudmoneyAdapter = new ReferanceUserAdapter(CloudMoneyHomePage.this, userList);
                        referaluserListGridview.setAdapter(cloudmoneyAdapter);
                        cloudmoneyAdapter.notifyDataSetChanged();
                    }

                    Min_amount_withdraw();

                } else {

                    dialogDismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });


    }


    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudMoneyHomePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (frompage.equalsIgnoreCase("fetching")) {
            Intent intent = new Intent(CloudMoneyHomePage.this, Navigation_new.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | /*Intent.FLAG_ACTIVITY_NO_HISTORY |*/ Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else {
            //    Intent intent = new Intent(WalletMoneyPage1.this, Navigation_new.class);
            // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
