package com.blackmaria.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user127 on 30-06-2017.
 */

@SuppressLint("AppCompatCustomView")
public class TextRCLight extends TextView {

    public TextRCLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextRCLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextRCLight(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/RobotoCondensed-Light.ttf");
        setTypeface(tf);
    }
}

