package com.blackmaria.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by user144 on 8/23/2017.
 */

public class CustomEdittextCambrialItalic extends EditText {

    public CustomEdittextCambrialItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEdittextCambrialItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEdittextCambrialItalic(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Cambria_Italic.ttf");
            setTypeface(tf);
        }
    }

}