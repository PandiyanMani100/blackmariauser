package com.blackmaria.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user144 on 3/1/2018.
 */

public class ItalicVladmir extends TextView {

    public ItalicVladmir(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ItalicVladmir(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ItalicVladmir(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/italicvladmir.TTF");
        setTypeface(tf);
    }
}
