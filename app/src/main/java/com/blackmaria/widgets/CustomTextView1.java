package com.blackmaria.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user14 on 5/2/2017.
 */

public class CustomTextView1 extends TextView {

    public CustomTextView1(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextView1(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextView1(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto-condensed.regular.ttf");

        //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/robos.ttf");
        setTypeface(tf);
    }


}