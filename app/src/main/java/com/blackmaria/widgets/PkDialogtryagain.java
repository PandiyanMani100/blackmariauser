package com.blackmaria.widgets;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.R;


public class PkDialogtryagain {
    private Context mContext;
    private Dialog dialog;
    private View view;
    private TextView tv_saveplsu,tv_text,tv_yes,tv_tryagain;
    private ImageView close;
    private boolean isPositiveAvailable = false;
    private boolean isNegativeAvailable = false;
    private boolean isCloseAvailable = false;
    LanguageDb mhelper;


    public PkDialogtryagain(Context context) {

        this.mContext = context;
        mhelper = new LanguageDb(mContext);
        //--------Adjusting Dialog width-----
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen

        view = View.inflate(mContext, R.layout.custom_dialog_tryagain, null);

        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);
        tv_saveplsu = view.findViewById(R.id.tv_saveplsu);
        tv_text = view.findViewById(R.id.tv_text);
        tv_yes = view.findViewById(R.id.tv_yes);
        tv_tryagain = view.findViewById(R.id.tv_tryagain);
        close = view.findViewById(R.id.close);

        TextView tv_saveplsu1 = view.findViewById(R.id.tv_saveplsu1);
        tv_saveplsu1.setText(getkey("info"));
    }


    public void show() {

        /*Enable or Disable positive Button*/
        if (isPositiveAvailable) {
            tv_yes.setVisibility(View.VISIBLE);
        } else {
            tv_yes.setVisibility(View.GONE);
        }
        if (isNegativeAvailable) {
            tv_tryagain.setVisibility(View.VISIBLE);
        } else {
            tv_tryagain.setVisibility(View.GONE);
        }

        if (isCloseAvailable) {
            close.setVisibility(View.VISIBLE);
        } else {
            close.setVisibility(View.GONE);
        }

        dialog.show();
    }


    public void dismiss() {
        dialog.dismiss();
    }


    public void setDialogTitle(String title) {
        tv_saveplsu.setText(title);
    }

    public void setDialogUpper(boolean yesNo) {
        tv_text.setAllCaps(yesNo);
    }

    public void setDialogMessage(String message) {
        tv_text.setText(message);
    }

    public void setDialogMessageSize(int size) {
        tv_saveplsu.setTextSize(size);
    }


    public void setOnDismissListener(final DialogInterface.OnDismissListener listener) {
        dialog.setOnDismissListener(listener);
    }

    public void setCancelOnTouchOutside(boolean value) {
        dialog.setCanceledOnTouchOutside(value);
    }

    public void setCancelble(boolean value) {
        dialog.setCancelable(value);
    }

    /*Action Button for Dialog*/
    public void setPositiveButton(String text, final View.OnClickListener listener) {
        isPositiveAvailable = true;
        tv_yes.setText(text);
        tv_yes.setOnClickListener(listener);
    }

    public void setNegativeButton(String text, final View.OnClickListener listener) {
        isNegativeAvailable = true;
        tv_tryagain.setText(text);
        tv_tryagain.setOnClickListener(listener);
    }

    public void setCloseButton(final View.OnClickListener listener) {
        isCloseAvailable = true;
        close.setOnClickListener(listener);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
