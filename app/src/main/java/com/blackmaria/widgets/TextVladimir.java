package com.blackmaria.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Ganesh on 02-06-2017.
 */

public class TextVladimir extends TextView {

    public TextVladimir(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextVladimir(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextVladimir(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/vladimir.ttf");
        setTypeface(tf);
    }
}
