package com.blackmaria.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user144 on 10/24/2017.
 */

public class CustomTextRBLight extends TextView {

    public CustomTextRBLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextRBLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextRBLight(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf");
        setTypeface(tf);
    }

}