package com.blackmaria.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user144 on 2/12/2018.
 */

public class CustomTextView_Palatino_italic extends TextView {

    public CustomTextView_Palatino_italic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextView_Palatino_italic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextView_Palatino_italic(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/palatinolinotype.ttf");
        setTypeface(tf);
    }


}