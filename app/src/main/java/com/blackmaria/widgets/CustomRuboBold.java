package com.blackmaria.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomRuboBold extends TextView {

    public CustomRuboBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomRuboBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRuboBold(Context context) {
        super(context);
        init();
    }


    public void init() {
       Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto-condensed.bold.ttf");
        //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/robos.ttf");
        setTypeface(tf);
    }


}