package com.blackmaria



import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.blackmaria.api_request.MultipartRequest
import com.blackmaria.chatmodule.ChatIntentServiceResult
import com.blackmaria.chatmodule.DbHelper
import com.blackmaria.chatmodule.MessagesAdapter
import com.blackmaria.chatmodule.TextMessage
import com.blackmaria.iconstant.Iconstant
import com.blackmaria.utils.SessionManager
import com.blackmaria.volley.ServiceRequest
import com.blackmaria.widgets.PkDialog
import com.blackmaria.xmpp.MyXMPP
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.yalantis.ucrop.UCrop
import com.yalantis.ucrop.util.BitmapLoadUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MessagesActivity : AppCompatActivity()
{

    private var dialog: Dialog? = null
    private var mImageCaptureUri: Uri? = null
    private  var outputUri:android.net.Uri? = null
    private val SELECT_IMAGE_REQUEST = 9
    var appDirectoryName = "blackmariachat"
    private val RC_LOCATION_CONTACTS_PERM = 124
    private var imageRoot: File? = null
    private val REQUEST_TAKE_PHOTO = 1
    private var dialogs: Dialog? = null
    private  var destination:java.io.File? = null
    private var mHelper: DbHelper? = null
    private var dataBase: SQLiteDatabase? = null

    private lateinit var messagesAdapter: MessagesAdapter
    lateinit var mess:ArrayList<TextMessage>
var close:ImageView ?= null
    private val TAKE_PHOTO_REQUEST = 18
    private lateinit var mContext: Activity
    var driverid:String=""
    var driverimage:String = ""
    var driverphoto:ImageView ?= null
    private var session: SessionManager? = null
    private var array: ByteArray? = null
    var mhelper:LanguageDb?=null
    var rideid:String=""
    var username:String=""
    var messages: RecyclerView?= null
    var callbutton:ImageView ?= null
    var attachment: ImageView?= null
    var sendmessage:ImageView ?= null
var enter_message:EditText ?= null
    var userimagee:String=""
    var useridtosend:String=""
    private var mRequest: ServiceRequest? = null
    var usernames:TextView ?= null
    var justfinish:String = "0"
    var userphonenumber:String =""

    var user_fcm_token:String = ""
    var messagetemplate : BottomSheetDialog? =null
    lateinit private var mSessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.newscreenlay)
        mContext = this@MessagesActivity
        mhelper= LanguageDb(this)
        session = SessionManager(mContext)
        initvalue()
        getExtravalue()
        intlizerpart()
        initlizeadapter()

        initviewmodel()


        setdataonview()



    }
    fun intlizerpart()
    {
        mSessionManager = SessionManager(applicationContext!!)
        mHelper= DbHelper(this);
        val user: HashMap<String, String> = mSessionManager.getUserDetails()
        driverid= user[SessionManager.KEY_USERID]!!
        driverimage= user[SessionManager.KEY_USERIMAGE]!!
        mess = ArrayList<TextMessage>()
    }
    fun getExtravalue()
    {
        rideid = intent?.getStringExtra("rideid").toString()
        username= intent?.getStringExtra("name").toString()
        userimagee= intent?.getStringExtra("userimage").toString()
        useridtosend= intent?.getStringExtra("useridtosend").toString()
        userphonenumber= intent?.getStringExtra("userphonenumber").toString()
        if (intent.hasExtra("jsutcallfinish")) {
           justfinish = "1"
        }

        user_fcm_token= intent?.getStringExtra("driver_fcm_token").toString()
    }
    fun setdataonview()
    {
        setimage(userimagee)
        usernames!!.text = username
    }



    fun locationAndContactsTask()
    {
        attaachmentclick()
    }



    fun initvalue()
    {
        messages= findViewById(R.id.messages) as RecyclerView
        close = findViewById(R.id.close)
        callbutton= findViewById(R.id.callbutton)
        attachment= findViewById(R.id.attachment)
        sendmessage= findViewById(R.id.sendmessage)
        enter_message= findViewById(R.id.enter_message)
        enter_message!!.setHint(getkey("type_a_message"))
        usernames= findViewById(R.id.usernames)
        driverphoto= findViewById(R.id.driverphoto)
    }
    fun initviewmodel()
    {
        sendmessage!!.setOnClickListener {
            sendMessage(enter_message!!.text.toString())
        }
        attachment!!.setOnClickListener {
            locationAndContactsTask()
        }
        close!!.setOnClickListener {
            if(justfinish.equals("1"))
            {
                finish()
            }
            else
            {
                val inputManager = getSystemService(
                        Context.INPUT_METHOD_SERVICE) as InputMethodManager

                inputManager.hideSoftInputFromWindow(enter_message!!.windowToken,
                        InputMethodManager.HIDE_NOT_ALWAYS)
                val intent = Intent(this@MessagesActivity, TrackRidePage::class.java)
                intent.putExtra("ride_id", rideid)
                startActivity(intent)
                finish()
            }
        }
        callbutton!!.setOnClickListener {

            val sPhoneMaskingStatus: String = session!!.getPhoneMaskingStatus()

            if (sPhoneMaskingStatus.equals("Yes", ignoreCase = true)) {
                postRequest_PhoneMasking(Iconstant.phoneMasking_url)
            } else {
                val number = userphonenumber
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$number"))
                    startActivity(intent)
                } else {
                    val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$number"))
                    startActivity(intent)
                }
            }


        }





        retrivelistfromdb()
    }
    private fun sendMessage(message:String)
    {
        if(message.equals(""))
        {
          Toast.makeText(applicationContext,getkey("messagenotempty"),Toast.LENGTH_LONG).show()
        }
        else
        {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
            val currentDateandTime = sdf.format(Date())
            var tsLong:Long = System.currentTimeMillis()/1000;
            var ts:String = tsLong.toString()
            savemessagetodb(message,driverid,ts)
            val domain: HashMap<String, String> = mSessionManager.getXmpp()
            val stringtoid = useridtosend + "@" + domain[SessionManager.KEY_HOST_URL]
            sendxmppmessage(mContext,stringtoid,useridtosend,rideid,ts,message,currentDateandTime,driverid)
            scrollToBottom()
        }
    }
    override fun onBackPressed() {
        // Do Here what ever you want do on back press;
    }
    private fun scrollToBottom()
    {
        messages!!.scrollToPosition(messagesAdapter.getItemCount() - 1)
    }
    fun initlizeadapter()
    {
        val layoutMgr = LinearLayoutManager(this)
        layoutMgr.stackFromEnd = true
        messages!!.layoutManager = layoutMgr
        messagesAdapter = MessagesAdapter(mess,driverid,userimagee,driverimage,applicationContext)
        messages!!.adapter = messagesAdapter
    }
    fun setimage(userimagee:String)
    {
        if(userimagee.startsWith("http"))
        {
            Glide.with(mContext)
                    .asBitmap()
                    .apply(RequestOptions().override(60, 60))
                    .load(userimagee)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            runOnUiThread {
                                driverphoto!!.setImageBitmap(resource)
                            }
                        }
                        override fun onLoadCleared(placeholder: Drawable?)
                        {
                        }
                    })
        }
    }
    override fun onResume()
    {
        super.onResume()
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
    }
    override fun onDestroy()
    {
        super.onDestroy()
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this)
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun doThis(intentServiceResult: ChatIntentServiceResult)
    {
        var desc: String = intentServiceResult.desc
        var sender_ID: String = "others"
        var timestamp: String = intentServiceResult.timestamp
        var ride_id: String = intentServiceResult.ride_id
        var datetime: String = intentServiceResult.datetime
        if(ride_id.equals(rideid))
        {

            val textMessage = TextMessage(rideid,desc,sender_ID,timestamp,"1",datetime)
            enter_message!!.setText("")
            messagesAdapter.appendMessage(textMessage)
            scrollToBottom()
        }
        updateallstatsu(mContext,rideid)
    }
    fun savemessagetodb(message:String,senderid:String,timestamp:String)
    {
        if(checkreocrdexist(timestamp) == 0)
        {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
            val currentDateandTime = sdf.format(Date())
            savemessagetodb(mContext,rideid,message,senderid,timestamp,currentDateandTime)
            val textMessage = TextMessage(rideid,message,senderid,timestamp,"1",currentDateandTime)
            enter_message!!.setText("")
            messagesAdapter.appendMessage(textMessage)
            scrollToBottom()
        }
    }
    fun retrivelistfromdb()
    {
        dataBase = mHelper!!.getWritableDatabase();
        var mCursor: Cursor = dataBase!!.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME+" WHERE rideid='"+rideid+"'", null);
        if (mCursor.moveToFirst()) {
            do {
                var rideid= mCursor.getString(mCursor.getColumnIndex(DbHelper.rideid))
                var message= mCursor.getString(mCursor.getColumnIndex(DbHelper.message))
                var senderid= mCursor.getString(mCursor.getColumnIndex(DbHelper.senderid))
                var timestamp= mCursor.getString(mCursor.getColumnIndex(DbHelper.timestamp))
                var status= mCursor.getString(mCursor.getColumnIndex(DbHelper.status))
                var datetime= mCursor.getString(mCursor.getColumnIndex(DbHelper.datetime))
                val textMessage = TextMessage(rideid,message,senderid,timestamp,status,datetime)
                messagesAdapter.appendMessage(textMessage)
            } while (mCursor.moveToNext());
        }
        mCursor.close()
        scrollToBottom()
        updateallstatsu(mContext,rideid)
    }
    fun checkreocrdexist(timestamp:String):Int
    {
        var valueexist:Int=0
        dataBase = mHelper!!.getWritableDatabase();
        var mCursor: Cursor = dataBase!!.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME+" WHERE rideid='"+rideid+"' AND timestamp='"+timestamp+"'", null);
        if (mCursor.moveToFirst()) {
            do {
                valueexist=1
            } while (mCursor.moveToNext());
        }
        mCursor.close()
        return valueexist
    }
    //attaachmentclick()


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                onSelectFromGalleryResult(data!!)
            } else if (requestCode == REQUEST_TAKE_PHOTO) {
                onCaptureImageResult(data)
            }else if (requestCode == UCrop.REQUEST_CROP) {
                onCroppedImageResult(data)
            } else if (resultCode == UCrop.RESULT_ERROR) {
                val cropError = UCrop.getError(data!!)
            }
        }
    }


    fun onCroppedImageResult(data: Intent?) {
        val resultUri = UCrop.getOutput(data!!)
        Log.d("Crop success", "" + resultUri)
        try {
            val bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri)
            if (bitmap == null) {
                Log.d("Bitmap", "null")
            } else {
                Log.d("Bitmap", "not null")
              //  Toast.makeText(applicationContext,"success",Toast.LENGTH_LONG).show()
            }
            var thumbnail = bitmap
            val picturePath = resultUri!!.path
            val byteArrayOutputStream = ByteArrayOutputStream()
            val curFile = File(picturePath)
            try {
                val exif = ExifInterface(curFile.path)
                val rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                val rotationInDegrees = BitmapLoadUtils.exifToDegrees(rotation)
                val matrix = Matrix()
                if (rotation.toFloat() != 0f) {
                    matrix.preRotate(rotationInDegrees.toFloat())
                }
                thumbnail = Bitmap.createBitmap(thumbnail!!, 0, 0, thumbnail.width, thumbnail.height, matrix, true)
            } catch (ex: IOException) {
                Log.e("TAG", "Failed to get Exif data", ex)
            }
            thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream)
            array = byteArrayOutputStream.toByteArray()
            UploadDriverImage(Iconstant.uploadimageforchat)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    private fun UploadDriverImage(url: String)
    {
        dialog = Dialog(mContext, R.style.CustomDialogTheme)
        dialog!!.getWindow()
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.loading_model)
        dialog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()


        val mRequest = MultipartRequest(mContext, "blackmariachat.jpg")
        mRequest.makeServiceRequest(url, Request.Method.POST, array, object : MultipartRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                val sStatus = ""
                val sResponse = ""
                val SUser_image = ""
                val Smsg = ""
                try {
                    dialog!!.dismiss()
                    val json = JSONObject(response)
                    sendMessage(json.getString("image_path"))
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }
    fun onSelectFromGalleryResult(data: Intent) {
        try {
            mImageCaptureUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mImageCaptureUri)
            if (!imageRoot!!.exists()) {
                imageRoot!!.mkdir()
            }


            val filename = "chatimage.png"
            val sd = Environment.getExternalStorageDirectory()
            val dest = File(sd, filename)


            try {
                val out = FileOutputStream(dest)
                bitmap!!.compress(Bitmap.CompressFormat.PNG, 90, out)
                out.flush()
                out.close()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }



            outputUri = Uri.fromFile(dest)
            val options = UCrop.Options()
            options.setStatusBarColor(getResources().getColor(R.color.black_color))
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color))
            options.setMaxBitmapSize(800)
            UCrop.of(mImageCaptureUri!!, outputUri!!)
                    .withAspectRatio(1f, 1f)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(this@MessagesActivity)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    fun dateToString(date: Date?, format: String?): String {
        val df = SimpleDateFormat(format)
        return df.format(date)
    }

    fun galleryintent()
    {
        try {


            imageRoot = File(Environment.getExternalStorageDirectory(), appDirectoryName)

            if (!imageRoot!!.exists()) {
                imageRoot!!.mkdir()
            }
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            intent.type = "image/*"
            startActivityForResult(intent,SELECT_IMAGE_REQUEST)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun attaachmentclick()
    {
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle(getkey("chooseact"))
        //set message for alert dialog



        //performing positive action
        builder.setPositiveButton(getkey("camera")){ dialogInterface, which ->

            if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED)
            {
                 Toast.makeText(applicationContext,"Camera Permission Denied",Toast.LENGTH_LONG).show()
            }
            else
            {
                takePicture()
            }

        }
        //performing cancel action
        builder.setNeutralButton(getkey("close")){dialogInterface , which ->
         }
        //performing negative action
        builder.setNegativeButton(getkey("gallery")){ dialogInterface, which ->
            galleryintent();
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun takePicture() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        camera_FileUri = getOutputMediaFileUri(1);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, camera_FileUri);
//        // start the image capture Intent
//        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
        try {
            val pictureIntent = Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE)
            if (pictureIntent.resolveActivity(packageManager) != null) {
                //Create a file to store the image
                imageRoot = File(Environment.getExternalStorageDirectory(), appDirectoryName)
                if (!imageRoot!!.exists()) {
                    imageRoot!!.mkdir()
                }
            }
            if (imageRoot != null) {
                val name = dateToString(Date(), "yyyy-MM-dd HH:mm:ss")
                destination = File(imageRoot, "$name.jpg")
                val photoURI = FileProvider.getUriForFile(this, "com.provider", destination!!)
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI)
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1)
                startActivityForResult(pictureIntent,
                        REQUEST_TAKE_PHOTO)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    fun onCaptureImageResult(data: Intent?) {
        try {
            val imagePath = destination!!.absolutePath
            mImageCaptureUri = Uri.fromFile(File(imagePath))
            outputUri = mImageCaptureUri

//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);
            val options = UCrop.Options()
            options.setStatusBarColor(getResources().getColor(R.color.black_color))
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color))
            options.setMaxBitmapSize(800)
            UCrop.of(mImageCaptureUri!!, outputUri!!)
                    .withAspectRatio(1f, 1f)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(mContext)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    fun sendxmppmessage(mcontext: Context, jid:String, useridtosend:String, rideid:String, ts:String, message:String,datetime:String,driverid:String)
    {
        val sessionManager = SessionManager.getInstance(mcontext)
        val domain: HashMap<String, String> = sessionManager.getXmpp()
        val ServiceName = domain[SessionManager.KEY_HOST_NAME]
        val HostAddress = domain[SessionManager.KEY_HOST_URL]
        val user: HashMap<String, String> = sessionManager.getUserDetails()
        val USERNAME = user[SessionManager.KEY_USERID]
        val PASSWORD = user[SessionManager.KEY_XMPP_SEC_KEY]
        val myXMPP = MyXMPP.getInstance(mcontext, ServiceName, HostAddress, USERNAME, PASSWORD)
        val chatID: String = useridtosend + "@" + ServiceName
        val job = JSONObject()
        job.put("message", message)
        job.put("timeStamp", ts)
        job.put("rideID", rideid)
        job.put("action", "blackmariachat")
        job.put("senderID", driverid)
        job.put("receiverID", useridtosend)
        job.put("datetime", datetime)

        if(message.startsWith("http"))
        {
            job.put("msgType", "image")
        }
        else
        {
            job.put("msgType", "text")
        }


        job.put("senderID", driverid)

        val status = myXMPP.sendMessage(chatID, job.toString())
        if (status == 0) {

        }

        val intent1 = Intent(applicationContext, fcmsendingprocess::class.java)
        try {
            intent1.putExtra("fcmtoken", user_fcm_token)
            intent1.putExtra("content", job.toString())
            intent1.putExtra("rideid", rideid)
            startService(intent1)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }




    fun updateallstatsu(mContext:Context,rideid:String)
    {
        var mHelper: DbHelper? = null
        var dataBase: SQLiteDatabase? = null
        mHelper= DbHelper(mContext)
        var values: ContentValues = ContentValues()
        values.put(DbHelper.status,"1")
        dataBase = mHelper!!.getWritableDatabase();
        var mCursor: Cursor = dataBase!!.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME+" WHERE rideid='"+rideid+"'", null);
        if (mCursor.moveToFirst()) {
            do {
                var id:Int= mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_ID)).toInt()
                dataBase!!.update(DbHelper.TABLE_NAME, values, DbHelper.KEY_ID+"="+id, null);
            } while (mCursor.moveToNext());
        }
        mCursor.close()
    }
    fun savemessagetodb(mContext:Context,rideid:String,message:String,senderid:String,timestamp:String,currentDateandTime:String)
    {
        var mHelper: DbHelper? = null
        var dataBase: SQLiteDatabase? = null
        mHelper= DbHelper(mContext)
        dataBase=mHelper!!.getWritableDatabase()
        var values:ContentValues= ContentValues()
        values.put(DbHelper.rideid,rideid)
        values.put(DbHelper.message,message )
        values.put(DbHelper.senderid,senderid )
        values.put(DbHelper.timestamp,timestamp)
        values.put(DbHelper.status,"1")
        values.put(DbHelper.datetime,currentDateandTime)
        dataBase!!.insert(DbHelper.TABLE_NAME, null, values)
    }


    //---------------------------------------------------------------------
    private fun postRequest_PhoneMasking(Url: String) {
        dialogs = Dialog(mContext)
        dialogs!!.window
        dialogs!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogs!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs!!.setContentView(R.layout.custom_loading)
        dialogs!!.setCanceledOnTouchOutside(false)
        dialogs!!.show()
        val dialog_title = dialogs!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = getkey("action_pleasewait")
        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = rideid
        jsonParams["user_type"] = "user"
        println("-------------PhoneMasking Url----------------$Url")
        mRequest = ServiceRequest(mContext)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------PhoneMasking Response----------------$response")
                var Str_status = ""
                var SResponse = ""
                try {
                    val `object` = JSONObject(response)
                    Str_status = `object`.getString("status")
                    SResponse = `object`.getString("response")
                    if (Str_status.equals("1", ignoreCase = true)) {
                        getkey("action_success")?.let { Alert(it, SResponse) }
                    } else {
                        getkey("action_error")?.let { Alert(it, SResponse) }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    dialogs!!.dismiss()
                }
                dialogs!!.dismiss()
            }

            override fun onErrorListener() {
                dialogs!!.dismiss()
            }
        })
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(mContext)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getkey("alert_ok")) { mDialog.dismiss() }
        mDialog.show()
    }

    private fun getkey(key: String): String? {
        return mhelper!!.getvalueforkey(key)
    }

}
