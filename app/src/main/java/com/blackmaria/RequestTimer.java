package com.blackmaria;

import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.blackmaria.subclass.ActivitySubClass;


public class RequestTimer extends ActivitySubClass
{

    int seconds = 100;
    Handler mHandler;
    int count = 0;

    ProgressBar progressBar;

    ImageView Iv_rotate;
    Animation rotate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_timer);
        initialize();
    }
    private void initialize() {

        Iv_rotate=(ImageView)findViewById(R.id.rotate_animation_imageview);

        // load the animation
        rotate = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_image);
        Iv_rotate.startAnimation(rotate);

        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation anim) {
                System.out.println("-------anim.getRepeatCount() onAnimationStart-----------"+anim.getDuration());
            }

            @Override
            public void onAnimationEnd(Animation anim) {
                System.out.println("-------anim.getRepeatCount() onAnimationEnd-----------"+anim.getDuration());
            }


            @Override
            public void onAnimationRepeat(Animation anim) {


                System.out.println("-------anim.getRepeatCount() onAnimationRepeat-----------"+anim.getDuration());



            }
        });
    }
}
