package com.blackmaria;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.widgets.CustomTextView;
import com.squareup.picasso.Picasso;

/**
 * Created by user14 on 4/27/2017.
 */

public class ThankYouActivity extends ActivityHockeyApp {
    String message = "", Action = "", driver_review = "", driver_name = "", driver_image = "", vehicle_number = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thankyou_popup);


        Intent intent = getIntent();
        message = intent.getStringExtra("message");
        Action = intent.getStringExtra("Action");
        driver_review = intent.getStringExtra("driver_review");
        driver_name = intent.getStringExtra("driver_name");
        driver_image = intent.getStringExtra("driver_image");
        vehicle_number = intent.getStringExtra("vehicle_number");

        CustomTextView driver_carnumber = (CustomTextView) findViewById(R.id.driver_carnumber);
        CustomTextView driver_profile_driver_name = (CustomTextView) findViewById(R.id.driver_profile_driver_name);
        RoundedImageView driver_profile_imageview = (RoundedImageView) findViewById(R.id.driver_profile_imageview);
        RatingBar driver_profile_ratingbar = (RatingBar) findViewById(R.id.driver_profile_ratingbar);

        CustomTextView thankyou_text = (CustomTextView) findViewById(R.id.thankyou_text);
        TextView thankyou_text1 = (TextView) findViewById(R.id.thankyou_text1);

        if (driver_review.equalsIgnoreCase(null)) {
            float userRate = Float.parseFloat("0");
            driver_profile_ratingbar.setRating(userRate);
        } else {
            float userRate = Float.parseFloat(driver_review);
            driver_profile_ratingbar.setRating(userRate);
        }

        driver_carnumber.setText(vehicle_number);
        driver_profile_driver_name.setText(driver_name);
        Picasso.with(ThankYouActivity.this).load(driver_image).placeholder(R.drawable.thanku_profile).into(driver_profile_imageview);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Cambria_Italic.ttf");
        thankyou_text1.setTypeface(face,Typeface.BOLD);

        RelativeLayout driver_profile_closebutton_layout = null;
        driver_profile_closebutton_layout = (RelativeLayout) findViewById(R.id.driver_profile_closebutton_layout);
        driver_profile_closebutton_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
