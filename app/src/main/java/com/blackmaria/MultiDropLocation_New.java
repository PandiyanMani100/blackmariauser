package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.adapter.MultiDrop_Adapter;
import com.blackmaria.pojo.MultiDropPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 2/23/2018.
 */

@SuppressLint("Registered")
public class MultiDropLocation_New extends Activity implements MultiDrop_Adapter.deleteMultiDrop {

    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;
    LanguageDb mhelper;
    private ArrayList<MultiDropPojo> multiDropList;
    private TextView Tv_dropAddress,cancel_multi;
    private RelativeLayout Rl_addStopOver,Rl_proceed;

    private String UserID = "", SwaitingTime = "0", StripMode = "", Stracking = "";
    private String ScategoryID = "", SselectedType = "", SselectedDate = "", SselectedTime = "";
    private String SdestinationLatitude = "", SdestinationLongitude = "", SdestinationLocation = "";
    private String SpickupLatitude = "", SpickupLongitude = "", SpickupLocation = "";
    private String str_FreeWaitingTime = "", str_FreeWaitingRate = "";
    private String ScurrencySymbol = "";
    private MultiDrop_Adapter adapter;
    private ExpandableHeightListView multidroplistView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.multidroplocation);
        mhelper = new LanguageDb(this);
        initialize();
        cancel_multi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alert("", getkey("arewantocancel"));
            }
        });
        Rl_addStopOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (multiDropList.size() == 0) {

                    System.out.println("=================Muruga multi drop list size==========" + multiDropList.size());

                    Intent intent = new Intent(MultiDropLocation_New.this, BookingPage3.class);
                    intent.putExtra("page", "MultiDropLocation");
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));

                    intent.putExtra("destinationAddress", SdestinationLocation);
                    intent.putExtra("destinationLatitude", SdestinationLatitude);
                    intent.putExtra("destinationLongitude", SdestinationLongitude);

                    intent.putExtra("pickup_date", SselectedDate);
                    intent.putExtra("pickup_time", SselectedTime);
                    intent.putExtra("CategoryID", ScategoryID);

                    intent.putExtra("type", SselectedType);
                    intent.putExtra("tracking", Stracking);
                    intent.putExtra("mode", StripMode);
                    intent.putExtra("waitingTime", SwaitingTime);
                    intent.putExtra("stopOverCount", (multiDropList.size() + 1) + "");
                    startActivity(intent);
                    finish();
//                    sampleapopaup();

                } else {
                    System.out.println("=================Muruga multi drop list size==========" + multiDropList.size());
                    Intent intent = new Intent(MultiDropLocation_New.this, BookingPage3.class);
                    intent.putExtra("page", "MultiDropLocation");
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));

                    intent.putExtra("destinationAddress", SdestinationLocation);
                    intent.putExtra("destinationLatitude", SdestinationLatitude);
                    intent.putExtra("destinationLongitude", SdestinationLongitude);

                    intent.putExtra("pickup_date", SselectedDate);
                    intent.putExtra("pickup_time", SselectedTime);
                    intent.putExtra("CategoryID", ScategoryID);

                    intent.putExtra("type", SselectedType);
                    intent.putExtra("tracking", Stracking);
                    intent.putExtra("mode", StripMode);
                    intent.putExtra("waitingTime", SwaitingTime);
                    intent.putExtra("stopOverCount", (multiDropList.size() + 1) + "");
                    Bundle bundleObject = new Bundle();
                    bundleObject.putSerializable("MultipleDropLocation_List", multiDropList);
                    intent.putExtras(bundleObject);
                    finish();
                    startActivity(intent);

                }

            }
        });

        Rl_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MultiDropLocation_New.this, BookingPage2.class);
                intent.putExtra("pickupAddress", SpickupLocation);
                intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));

                intent.putExtra("destinationAddress", SdestinationLocation);
                intent.putExtra("destinationLatitude", SdestinationLatitude);
                intent.putExtra("destinationLongitude", SdestinationLongitude);

                intent.putExtra("pickup_date", SselectedDate);
                intent.putExtra("pickup_time", SselectedTime);
                intent.putExtra("CategoryID", ScategoryID);

                intent.putExtra("type", SselectedType);
                intent.putExtra("tracking", Stracking);
                intent.putExtra("mode", StripMode);
                intent.putExtra("waitingTime", SwaitingTime);

                Bundle bundleObject = new Bundle();
                bundleObject.putSerializable("MultipleDropLocation_List", multiDropList);
                intent.putExtras(bundleObject);
                // finish();
                startActivity(intent);


            }
        });
    }


    private void initialize() {

        sessionManager = new SessionManager(MultiDropLocation_New.this);
        cd = new ConnectionDetector(MultiDropLocation_New.this);
        isInternetPresent = cd.isConnectingToInternet();
        multiDropList = new ArrayList<MultiDropPojo>();


        Tv_dropAddress =findViewById(R.id.booking_drop_address_textview);
        Rl_addStopOver =  findViewById(R.id.add_stop_over_layout);
        Rl_proceed =  findViewById(R.id.multistop_proceed_layout);
        multidroplistView =  findViewById(R.id.multidroplist);
        cancel_multi =  findViewById(R.id.cancel_multi);
        cancel_multi.setText(getkey("track_your_ride_label_cancel"));

        TextView stopover =  findViewById(R.id.stopover);
        stopover.setText(getkey("stop_over_trips"));

        TextView maxist =  findViewById(R.id.maxist);
        maxist.setText(getkey("maximum_3_stop_over"));

        TextView gonow =  findViewById(R.id.gonow);
        gonow.setText(getkey("gonow"));

        TextView pickup_plusimagview =  findViewById(R.id.pickup_plusimagview);
        pickup_plusimagview.setText(getkey("add_stop_over"));

        TextView dropoff_lable =  findViewById(R.id.dropoff_lable);
        dropoff_lable.setText(getkey("final_drop_of"));

        getData();

    }

    private void getData() {
        try {

            HashMap<String, String> info = sessionManager.getUserDetails();
            UserID = info.get(SessionManager.KEY_USERID);


            HashMap<String, String> freeWaitingTime = sessionManager.getFreeWaitingTimeRate();
            str_FreeWaitingTime = freeWaitingTime.get(SessionManager.FREE_WAITING_TIME);
            str_FreeWaitingRate = freeWaitingTime.get(SessionManager.FREE_WAITING_RATE);

            Intent intent = getIntent();
            SpickupLocation = intent.getStringExtra("pickupAddress");
            SpickupLatitude = intent.getStringExtra("pickupLatitude");
            SpickupLongitude = intent.getStringExtra("pickupLongitude");
            SdestinationLocation = intent.getStringExtra("destinationAddress");
            SdestinationLatitude = intent.getStringExtra("destinationLatitude");
            SdestinationLongitude = intent.getStringExtra("destinationLongitude");

            SselectedDate = intent.getStringExtra("pickup_date");
            SselectedTime = intent.getStringExtra("pickup_time");
            ScategoryID = intent.getStringExtra("CategoryID");
            SselectedType = intent.getStringExtra("type");
            Stracking = intent.getStringExtra("tracking");
            StripMode = intent.getStringExtra("mode");

            if (intent.hasExtra("waitingTime")) {
                SwaitingTime = intent.getStringExtra("waitingTime");
            }
            if (intent.hasExtra("MultipleDropLocation_List")) {
                try {
                    Bundle bundleObject = getIntent().getExtras();
                    multiDropList = (ArrayList<MultiDropPojo>) bundleObject.getSerializable("MultipleDropLocation_List");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        setData();


    }

    private void setData() {

        Tv_dropAddress.setText(SdestinationLocation.toString());

        if (multiDropList.size() > 0) {
            Rl_proceed.setVisibility(View.VISIBLE);
            setTag();
        } else {
            Rl_proceed.setVisibility(View.GONE);
        }

        if (multiDropList.size() >= 3) {
            Rl_addStopOver.setVisibility(View.GONE);
        } else {
            Rl_addStopOver.setVisibility(View.VISIBLE);
        }

    }

    private void setTag() {

        if (multiDropList.size() > 0) {
            adapter = new MultiDrop_Adapter(MultiDropLocation_New.this, multiDropList, this);
            multidroplistView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            Rl_addStopOver.setVisibility(View.VISIBLE);
        } else {
            Rl_addStopOver.setVisibility(View.GONE);
        }

    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(MultiDropLocation_New.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog.dismiss();
                Intent intent = new Intent(MultiDropLocation_New.this, Navigation_new.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        mDialog.show();
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            return true;
        }
        return false;
    }

    @Override
    public void OnDeleteLocation(int deletePostion) {

        multiDropList.remove(deletePostion);
        if (multiDropList.size() >= 3) {
            Rl_addStopOver.setVisibility(View.GONE);
        } else {
            Rl_addStopOver.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
