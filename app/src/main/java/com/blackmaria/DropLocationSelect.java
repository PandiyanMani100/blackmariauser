package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.InterFace.CallBack;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.GeocoderHelper;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.widgets.PkDialog;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;
import java.util.Locale;

/**
 * Created by Prem Kumar and Anitha on 2/26/2016.
 */
public class DropLocationSelect extends ActivityHockeyApp {
    RelativeLayout Rl_back;
    RelativeLayout Rl_selectDrop;
    private TextView Tv_dropLocation;

    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    private GoogleMap googleMap;
    private GPSTracker gps;
    ProgressBar progressBar;
    LanguageDb mhelper;
    private String sLatitude = "";
    private String sLongitude = "";
    private ImageView bookingBack;
    public static final int ActivityDropRequestCode = 6000;
    private static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
    private String address = "";
    private RefreshReceiver refreshReceiver;
    private RelativeLayout Rl_done;
    MapFragment mapFragment;
    String strPage = "";

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(Fragment_HomePage.this, Fragment_HomePage.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        BookingPage2.BookingPage2_class.finish();
                        Intent intent1 = new Intent(DropLocationSelect.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        BookingPage2.BookingPage2_class.finish();
                        Intent intent1 = new Intent(DropLocationSelect.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.droplocation_select);
        mhelper = new LanguageDb(this);
        initialize();
        initializeMap();

        Rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Selected_Latitude", sLatitude);
                returnIntent.putExtra("Selected_Longitude", sLongitude);
                returnIntent.putExtra("Selected_Location", Tv_dropLocation.getText().toString());
                setResult(RESULT_OK, returnIntent);
                onBackPressed();
                finish();
            }
        });
        bookingBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //onBackPressed();
                if (strPage.equalsIgnoreCase("booking1")) {
                    Intent intent = new Intent(DropLocationSelect.this, Navigation_new.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else if (strPage.equalsIgnoreCase("bookSomeOne")) {
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
        });

        Rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Selected_Latitude", sLatitude);
                returnIntent.putExtra("Selected_Longitude", sLongitude);
                returnIntent.putExtra("Selected_Location", Tv_dropLocation.getText().toString());
                setResult(RESULT_OK, returnIntent);
                onBackPressed();
                finish();
            }
        });

        Rl_selectDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GPSTracker gps = new GPSTracker(DropLocationSelect.this);
                if (gps.canGetLocation()) {
                    Intent i = new Intent(DropLocationSelect.this, LocationSearch.class);
                    startActivityForResult(i, ActivityDropRequestCode);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    Toast.makeText(DropLocationSelect.this, getkey("enanle_gps"), Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }

    private void initialize() {
        cd = new ConnectionDetector(DropLocationSelect.this);
        isInternetPresent = cd.isConnectingToInternet();
        gps = new GPSTracker(DropLocationSelect.this);

        TextView cnfirm= (TextView) findViewById(R.id.cnfirm);
        cnfirm.setText(getkey("confirm_lable"));

        Rl_done = (RelativeLayout) findViewById(R.id.drop_location_select_search_ok_layout);
        Rl_back = (RelativeLayout) findViewById(R.id.drop_location_select_cancel_layout);
        Rl_selectDrop = (RelativeLayout) findViewById(R.id.drop_location_select_dropLocation_layout);
        Tv_dropLocation = (TextView) findViewById(R.id.drop_location_select_drop_address);
        progressBar = (ProgressBar) findViewById(R.id.drop_location_select_progress_bar);
        bookingBack = (ImageView) findViewById(R.id.img_booking_back);
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);
        Intent in = getIntent();
        if (in.hasExtra("page")) {
            strPage = in.getStringExtra("page");

        }

    }

    private void initializeMap() {
      /*  if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.drop_location_select_view_map)).getMap();
            if (googleMap == null) {
                Toast.makeText(DropLocationSelect.this, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }
        }*/
        if (googleMap == null) {
            //googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.book_my_ride3_mapview)).getMap();
            mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.drop_location_select_view_map));
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap arg) {
                    loadMap(arg);
                }
            });
     /*   // Changing map type
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Showing / hiding your current location
        googleMap.setMyLocationEnabled(true);
        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(false);
        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        if (gps.canGetLocation() && gps.isgpsenabled()) {

            double Dlatitude = gps.getLatitude();
            double Dlongitude = gps.getLongitude();

            // Move the camera to last position with a zoom level
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));




        if (CheckPlayService()) {
            googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String tittle = marker.getTitle();
                    return true;
                }
            });
        } else {
            Toast.makeText(DropLocationSelect.this, "Install Google Play service To View Location !!!", Toast.LENGTH_LONG).show();
        }*/
        }
    }

    public void loadMap(GoogleMap arg) {
        googleMap = arg;
        // Changing map type
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Showing / hiding your current location
        googleMap.setMyLocationEnabled(true);
        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(false);
        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        if (gps.canGetLocation() && gps.isgpsenabled()) {

            double Dlatitude = gps.getLatitude();
            double Dlongitude = gps.getLongitude();

            // Move the camera to last position with a zoom level
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        } else {
 }


        if (CheckPlayService()) {
            googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String tittle = marker.getTitle();
                    return true;
                }
            });
        } else {
            Toast.makeText(DropLocationSelect.this, getkey("install_googleplay_view_location"), Toast.LENGTH_LONG).show();
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(DropLocationSelect.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //-----------Check Google Play Service--------
    private boolean CheckPlayService() {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(DropLocationSelect.this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        }
        return true;
    }


    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        DropLocationSelect.this.runOnUiThread(new Runnable() {
            public void run() {
                final Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionStatusCode, DropLocationSelect.this, REQUEST_CODE_RECOVER_PLAY_SERVICES);
                if (dialog == null) {
                    Toast.makeText(DropLocationSelect.this, getkey("incompitable"), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    //-------------------------------code for map marker moving-------------------------------
    GoogleMap.OnCameraChangeListener mOnCameraChangeListener = new GoogleMap.OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition cameraPosition) {
            double latitude = cameraPosition.target.latitude;
            double longitude = cameraPosition.target.longitude;

            cd = new ConnectionDetector(DropLocationSelect.this);
            isInternetPresent = cd.isConnectingToInternet();

            Log.e("camerachange lat-->", "" + latitude);
            Log.e("on_camera_change lon-->", "" + longitude);

            if (googleMap != null) {
                googleMap.clear();

                if (isInternetPresent) {

                    sLatitude = String.valueOf(latitude);
                    sLongitude = String.valueOf(longitude);

                    Map_movingTask asynTask = new Map_movingTask(latitude, longitude);
                    asynTask.execute();
                } else {
                    Alert(getkey("action_error"),getkey("alert_nointernet"));
                }
            }
        }
    };


    private class Map_movingTask extends AsyncTask<String, Void, String> {

        String response = "";
        private double dLatitude = 0.0;
        private double dLongitude = 0.0;

        Map_movingTask(double lat, double lng) {
            dLatitude = lat;
            dLongitude = lng;
        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            Rl_done.setVisibility(View.INVISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {
            address = /*getCompleteAddressString(dLatitude, dLongitude);;//*/new GeocoderHelper().fetchCityName(DropLocationSelect.this, dLatitude, dLongitude, callBack);////getCompleteAddressString(dLatitude, dLongitude);
            return address;
        }

        @Override
        protected void onPostExecute(String result) {
            progressBar.setVisibility(View.GONE);
            System.out.println("----result-----------" + result);
            if (result != null && result.length() > 0) {
                Tv_dropLocation.setText(result);
                Rl_done.setVisibility(View.INVISIBLE);
            } else {
                Tv_dropLocation.setText(result);
                Rl_done.setVisibility(View.INVISIBLE);
            }
        }
    }


    //-------------Method to get Complete Address------------
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(DropLocationSelect.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                Log.e("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Current loction address", "Canont get Address!");
        }
        return strAdd;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == RESULT_OK) {
            if (requestCode == ActivityDropRequestCode) {
                String sAddress = data.getStringExtra("Selected_Location");
                sLatitude = data.getStringExtra("Selected_Latitude");
                sLongitude = data.getStringExtra("Selected_Longitude");
                Tv_dropLocation.setText(sAddress);
                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(sLatitude), Double.parseDouble(sLongitude))).zoom(17).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                Rl_done.setVisibility(View.INVISIBLE);
            }
        }
    }


    @Override
    public void onDestroy() {
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }

    CallBack callBack = new CallBack() {
        @Override
        public void onComplete(String LocationName) {
            address = LocationName;
            Tv_dropLocation.setText(LocationName);
            System.out.println("-------------------addreess----------------0" + LocationName);

        }

        @Override

        public void onError(String errorMsg) {


        }

        @Override
        public void onCompleteAddress(String message) {

        }

    };


    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
