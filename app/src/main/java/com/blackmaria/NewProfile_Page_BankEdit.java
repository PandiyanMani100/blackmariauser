package com.blackmaria;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.WalletMoneyPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by Muruganantham on 12/28/2017.
 */

@SuppressLint("Registered")
public class NewProfile_Page_BankEdit extends ActivityHockeyApp implements View.OnClickListener {

    private ServiceRequest mRequest;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private RefreshReceiver refreshReceiver;
    private SessionManager session;


    private RelativeLayout bankEditLyHeader;
    private ImageView cancelLayout;
    private LinearLayout myloginDialogFormWholeLayout;
    private Spinner profileBanknameSpinner;
    private CustomEdittext bankDetailsAccountNumber;
    private CustomEdittext bankDetailsHolderName;
    private CheckBox agreeBox;
    private RelativeLayout bankInformationDetailsUpdate;
    private SmoothProgressBar profileLoadingProgressbar;

    private Boolean isDatavailable = false;
    private Boolean isPaymentListAvailable = false;
    private String UserID = "";
    ArrayList<WalletMoneyPojo> paymentcardlist;
    ArrayList<String> paymentcardNamelist;

    String bankImageUrl = "";
    String accHolderName = "", holderAccountNumber = "";
    private int selectPositionSpinner = 0;

    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewProfile_Page_BankEdit.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewProfile_Page_BankEdit.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            } else if (intent.getAction().equals("com.package.ACTION_CLASS_complaint_replay_notification")) {
                if (intent.getExtras() != null) {
                    String ticketId = (String) intent.getExtras().get("ticket_id");
                    String userID = (String) intent.getExtras().get("user_id");
                    String message = (String) intent.getExtras().get("message");
                    String dateTime = (String) intent.getExtras().get("date_time");

                    Intent intent1 = new Intent(NewProfile_Page_BankEdit.this, ComplaintPageViewDetailPage.class);
                    intent1.putExtra("ticket_id", ticketId);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_bank_info_edit_layout);
        initialize();
        OtherClickListener();

    }

    private void initialize() {
        session = new SessionManager(NewProfile_Page_BankEdit.this);
        cd = new ConnectionDetector(NewProfile_Page_BankEdit.this);
        isInternetPresent = cd.isConnectingToInternet();
        paymentcardlist = new ArrayList<>();
        paymentcardNamelist = new ArrayList<>();
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);


        HashMap<String, String> info = session.getUserDetails();
        String tipstime = info.get(SessionManager.KEY_PHONENO);
        UserID = info.get(SessionManager.KEY_USERID);


        bankEditLyHeader = (RelativeLayout) findViewById(R.id.bank_edit_ly_header);
        cancelLayout = (ImageView) findViewById(R.id.cancel_layout);
        myloginDialogFormWholeLayout = (LinearLayout) findViewById(R.id.mylogin_dialog_form_whole_layout);
        profileBanknameSpinner = (Spinner) findViewById(R.id.profile_bankname_spinner);
        bankDetailsAccountNumber = (CustomEdittext) findViewById(R.id.bank_details_account_number);
        bankDetailsHolderName = (CustomEdittext) findViewById(R.id.bank_details_holder_name);
        agreeBox = (CheckBox) findViewById(R.id.agree_box);
        bankInformationDetailsUpdate = (RelativeLayout) findViewById(R.id.bank_information_details_update);
        profileLoadingProgressbar = (SmoothProgressBar) findViewById(R.id.profile_loading_progressbar);


        cancelLayout.setOnClickListener(this);
        bankInformationDetailsUpdate.setOnClickListener(this);
        if (isInternetPresent) {
            try {
                PostRquestXenditBankList(Iconstant.getPaymentOPtion_url_new);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
        }
    }

    @Override
    public void onClick(View v) {
        if (v == cancelLayout) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == bankInformationDetailsUpdate) {
            BankUpdate();
        }
    }


    private void OtherClickListener() {
        profileBanknameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                selectPositionSpinner = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void BankUpdate() {
        if (agreeBox.isChecked())
            if (bankDetailsHolderName.getText().toString().trim().length() <= 0) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_holder_name));
            } else if (bankDetailsAccountNumber.getText().toString().trim().length() <= 0) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no_enter));
            } else {
                String bankCode = paymentcardlist.get(selectPositionSpinner).getXenditBankCode();
                String bankName = paymentcardlist.get(selectPositionSpinner).getXenditBankName();
                bankImageUrl = paymentcardlist.get(selectPositionSpinner).getXendiActiveImage();

                accHolderName = bankDetailsHolderName.getText().toString().trim();
                holderAccountNumber = bankDetailsAccountNumber.getText().toString().trim();

                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", UserID);
                    jsonParams.put("acc_holder_name", accHolderName);
                    jsonParams.put("acc_number", holderAccountNumber);
                    jsonParams.put("bank_name", bankName);
                    jsonParams.put("bank_code", bankCode);
                    PostRquestXenditBanDetailSave(Iconstant.getXendit_bank_save_url, jsonParams);
                } else {

                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }

            }
        else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.pls_provide_info));

        }
    }

    private void PostRquestXenditBankList(String Url) {
        profileLoadingProgressbar.setVisibility(View.VISIBLE);
        System.out.println("-----------Xendit bank url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        System.out.println("------------Xendit bank jsonParams--------------" + jsonParams);
        mRequest = new ServiceRequest(NewProfile_Page_BankEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------Xendit bank jsonParams--------------" + response);
                String Ssatus = "";
                try {
                    JSONObject jsondata = new JSONObject(response);
                    Ssatus = jsondata.getString("status");
                    if (Ssatus.equalsIgnoreCase("1")) {
                        // JSONObject jsondata = object.getJSONObject("response");
                        Object check_object = jsondata.get("bank_list");
                        if (check_object instanceof JSONArray) {
                            JSONArray payment_list_jsonArray = jsondata.getJSONArray("bank_list");
                            if (payment_list_jsonArray.length() > 0) {
                                paymentcardlist.clear();
                                paymentcardNamelist.clear();
                                for (int i = 0; i < payment_list_jsonArray.length(); i++) {
                                    JSONObject payment_obj = payment_list_jsonArray.getJSONObject(i);
                                    WalletMoneyPojo wmpojo = new WalletMoneyPojo();
                                    wmpojo.setXenditBankName(payment_obj.getString("name"));
                                    wmpojo.setXenditBankCode(payment_obj.getString("code"));
                                    wmpojo.setXendiActiveImage(payment_obj.getString("image"));
                                    paymentcardlist.add(wmpojo);
                                    paymentcardNamelist.add(payment_obj.getString("name"));
                                }
                                isPaymentListAvailable = true;
                            } else {
                                isPaymentListAvailable = false;
                            }
                        }
                        isDatavailable = true;
                    }
                    profileLoadingProgressbar.setVisibility(View.GONE);
                    if (Ssatus.equalsIgnoreCase("1") && isDatavailable) {
                        String[] BankName = new String[paymentcardNamelist.size()];
                        BankName = paymentcardNamelist.toArray(BankName);
                        ArrayAdapter<CharSequence> monthAdapter = new ArrayAdapter<CharSequence>(NewProfile_Page_BankEdit.this, R.layout.spinner_text1, BankName);
                        monthAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
                        profileBanknameSpinner.setAdapter(monthAdapter);
                    } else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    profileLoadingProgressbar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onErrorListener() {
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
        });
    }

    private void PostRquestXenditBanDetailSave(String Url, HashMap<String, String> jsonParams) {
        profileLoadingProgressbar.setVisibility(View.VISIBLE);

        System.out.println("-----------PostRquestXenditBanDetailSave jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(NewProfile_Page_BankEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------PostRquestXenditBanDetailSave reponse-------------------" + response);

                String Sstatus = "", Smessage = "", acc_holder_name = "", acc_number = "", bank_name = "", bank_code = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");
                        if (jsonObject.has("banking")) {

                            JSONObject banking = jsonObject.getJSONObject("banking");
                            if (banking.length() > 0) {
                                acc_holder_name = banking.getString("acc_holder_name");
                                acc_number = banking.getString("acc_number");
                                bank_name = banking.getString("bank_name");
                                bank_code = banking.getString("bank_code");
                            }

                        }

                        profileLoadingProgressbar.setVisibility(View.GONE);

                    } else {
                        Smessage = object.getString("response");
                        profileLoadingProgressbar.setVisibility(View.GONE);
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    profileLoadingProgressbar.setVisibility(View.GONE);
                }
                profileLoadingProgressbar.setVisibility(View.GONE);

                if (Sstatus.equalsIgnoreCase("1")) {
                    Alert1(getResources().getString(R.string.action_success), getResources().getString(R.string.bank_details_updated));
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
        });


    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewProfile_Page_BankEdit.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_BankEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_BankEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent local = new Intent();
                local.setAction("com.app.PaymentListPage.refreshPage");
                local.putExtra("refresh", "yes");
                sendBroadcast(local);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(NewProfile_Page_BankEdit.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }
    @Override
    public void onDestroy() {
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }

}
