package com.blackmaria.viewmodel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class vehicledetailspojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;

    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("category_id")
        private String category_id;

        public ArrayList<Vehicles> getVehicles() {
            return vehicles;
        }

        public void setVehicles(ArrayList<Vehicles> vehicles) {
            this.vehicles = vehicles;
        }
        @SerializedName("vehicles")
        ArrayList< Vehicles > vehicles = new ArrayList< Vehicles >();
        public String getCategory_id() {
            return category_id;
        }

        // Setter Methods
        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public class Vehicles{
            @SerializedName("id")
            private String id;
            @SerializedName("name")
            private String name;
            // Getter Methods

            public String getId() {
                return id;
            }

            public String getName() {
                return name;
            }

            // Setter Methods

            public void setId(String id) {
                this.id = id;
            }

            public void setName(String name) {
                this.name = name;
            }

            public ArrayList<Cat_id> getCat_id() {
                return cat_id;
            }

            public void setCat_id(ArrayList<Cat_id> cat_id) {
                this.cat_id = cat_id;
            }

            @SerializedName("cat_id")
            ArrayList< Cat_id > cat_id = new ArrayList< Cat_id >();

            public ArrayList<Makers> getMakers() {
                return makers;
            }

            public void setMakers(ArrayList<Makers> makers) {
                this.makers = makers;
            }

            @SerializedName("makers")
            ArrayList< Makers > makers = new ArrayList< Makers >();

            public class Cat_id{
                @SerializedName("cat_id")
                private String cat_id;
                // Getter Methods

                public String getCat_id() {
                    return cat_id;
                }

                // Setter Methods

                public void setCat_id(String cat_id) {
                    this.cat_id = cat_id;
                }
            }

            public class Makers{
                @SerializedName("id")
                private String id;
                @SerializedName("name")
                private String name;

                public ArrayList<Models> getModels() {
                    return models;
                }

                public void setModels(ArrayList<Models> models) {
                    this.models = models;
                }

                @SerializedName("models")
                ArrayList< Models > models = new ArrayList< Models >();
                // Getter Methods

                public String getId() {
                    return id;
                }

                public String getName() {
                    return name;
                }

                // Setter Methods

                public void setId(String id) {
                    this.id = id;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public class Models{
                    @SerializedName("id")
                    private String id;
                    @SerializedName("name")
                    private String name;

                    public ArrayList<Years> getYears() {
                        return years;
                    }

                    public void setYears(ArrayList<Years> years) {
                        this.years = years;
                    }

                    @SerializedName("years")
                    ArrayList< Years > years = new ArrayList< Years >();

                    // Getter Methods

                    public String getId() {
                        return id;
                    }

                    public String getName() {
                        return name;
                    }

                    // Setter Methods

                    public void setId(String id) {
                        this.id = id;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public class Years{
                        @SerializedName("name")
                        private String name;
                        @SerializedName("value")
                        private String value;
                        // Getter Methods

                        public String getName() {
                            return name;
                        }

                        public String getValue() {
                            return value;
                        }

                        // Setter Methods

                        public void setName(String name) {
                            this.name = name;
                        }

                        public void setValue(String value) {
                            this.value = value;
                        }
                    }
                }
            }

        }
    }
}

