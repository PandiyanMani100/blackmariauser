package com.blackmaria.viewmodel;

import android.app.AlertDialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.blackmaria.LanguageDb;
import com.blackmaria.R;
import com.blackmaria.RegistrationSuccess;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.ActivityVerifyPhonenoConstrainBinding;
import com.blackmaria.widgets.PkDialog;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.mukesh.OnOtpCompletionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class VerifyPhoneno_Constrain extends AppCompatActivity {

    private ActivityVerifyPhonenoConstrainBinding binding;
    private VerifyPhonenoViewModel verifyPhonenoViewModel;

    String sOtpPin = "", sOtpPinStatus = "";
    private SessionManager sessionManager;
    private String number = "";
    public CountryPicker picker;
    private AppUtils appUtils;
    private AlertDialog alertDialog;
    private HashMap<String, String> gloablhasmap;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mhelper = new LanguageDb(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verify_phoneno_constrain);
        verifyPhonenoViewModel = ViewModelProviders.of(this, new VerifyPhonenoFactory(this)).get(VerifyPhonenoViewModel.class);
        binding.setVerifyPhonenoViewModel(verifyPhonenoViewModel);
        verifyPhonenoViewModel.setIds(binding);

        binding.personalinfo.setText(getkey("verify"));
        binding.waittext.setText(getkey("pleasewait"));
        binding.resend.setText(getkey("resend"));
        binding.cahngenumber.setText(getkey("change_number"));
        binding.textRegister.setText(getkey("athentication"));
        binding.blackInc.setText(getkey("label_blackmaria_inc"));


        initView();
        setclicklisener();

        binding.otpview.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                // do Stuff
                sOtpPin = otp;
            }
        });

        verifyPhonenoViewModel.getStringMutableLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                String status = "", Sresponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");
                    Sresponse = object.getString("response");

                    if (status.equalsIgnoreCase("1")) {
                        Intent intent = new Intent(VerifyPhoneno_Constrain.this, RegistrationSuccess.class);
                        intent.putExtra("hashMap", gloablhasmap);
                        intent.putExtra("welcome_amount", CurrencySymbolConverter.getCurrencySymbol(object.getString("currency"))+" "+object.getString("welcome_amount"));
                       intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else {
                        Alert(getkey("action_error"), Sresponse);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        verifyPhonenoViewModel.getValidationerrors().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Alert(getkey("action_error"), s);
            }
        });

        verifyPhonenoViewModel.getResendotprespone().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                String status = "", Sresponse = "", otpStatus = "", otpPin = "";
                try {

                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");
                    Sresponse = object.getString("response");
                    if (status.equalsIgnoreCase("1")) {
                        appUtils.AlertSuccess(VerifyPhoneno_Constrain.this,getkey("action_success"),getkey("otp_sent"));
                        otpStatus = object.getString("otp_status");
                        otpPin = object.getString("otp");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    if (otpStatus.equalsIgnoreCase("development")) {
                        binding.otpview.setText(otpPin);
                    }
                    sessionManager.setOtpstatusAndPin(otpStatus, otpPin);
                } else {
                    Alert(getkey("action_error"), Sresponse);
                }
            }
        });
    }

    private void setclicklisener() {

        binding.cahngenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changenumber();
            }
        });

        binding.img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "1";
                binding.otpview.setText(number);
                binding.otpview.setSelection(binding.otpview.getText().length());
                setbackground(binding.img1, v);
            }
        });

        binding.img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "2";
                binding.otpview.setText(number);
                binding.otpview.setSelection(binding.otpview.getText().length());
                setbackground(binding.img2, v);
            }
        });

        binding.img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "3";
                binding.otpview.setText(number);
                binding.otpview.setSelection(binding.otpview.getText().length());
                setbackground(binding.img3, v);
            }
        });

        binding.img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "4";
                binding.otpview.setText(number);
                binding.otpview.setSelection(binding.otpview.getText().length());
                setbackground(binding.img4, v);
            }
        });
        binding.img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "5";
                binding.otpview.setText(number);
                binding.otpview.setSelection(binding.otpview.getText().length());
                setbackground(binding.img5, v);
            }
        });
        binding.img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "6";
                binding.otpview.setText(number);
                binding.otpview.setSelection(binding.otpview.getText().length());
                setbackground(binding.img6, v);
            }
        });
        binding.img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "7";
                binding.otpview.setText(number);
                binding.otpview.setSelection(binding.otpview.getText().length());
                setbackground(binding.img7, v);
            }
        });
        binding.img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "8";
                binding.otpview.setText(number);
                binding.otpview.setSelection(binding.otpview.getText().length());
                setbackground(binding.img8, v);
            }
        });
        binding.img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "9";
                binding.otpview.setText(number);
                binding.otpview.setSelection(binding.otpview.getText().length());
                setbackground(binding.img9, v);
            }
        });
        binding.img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "0";
                binding.otpview.setText(number);
                binding.otpview.setSelection(binding.otpview.getText().length());
                setbackground(binding.img0, v);
            }
        });
        binding.imgHash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String SMobileNo = binding.otpview.getText().toString();
                if (SMobileNo != null && SMobileNo.length() > 0) {
                    binding.otpview.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                    binding.otpview.setSelection(binding.otpview.getText().length());
                    number = binding.otpview.getText().toString();
                }
            }
        });


    }

    public void changenumber() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.changenumberdialog, null);
        dialogBuilder.setView(dialogView);
        final TextView dialcode = dialogView.findViewById(R.id.dialcode);

        final TextView custom_dialog_library_title_textview = dialogView.findViewById(R.id.custom_dialog_library_title_textview);
        custom_dialog_library_title_textview.setText(getkey("changenumber"));

        final ProgressBar progressBar = dialogView.findViewById(R.id.apiload);
        final EditText custom_dialog_library_message_textview = dialogView.findViewById(R.id.custom_dialog_library_message_textview);
        custom_dialog_library_message_textview.setHint(getkey("edit_mobilenumber"));

        final TextView custom_dialog_library_ok_button = dialogView.findViewById(R.id.custom_dialog_library_ok_button);
        TextView custom_dialog_library_cancel_button = dialogView.findViewById(R.id.custom_dialog_library_cancel_button);
        custom_dialog_library_ok_button.setText(getkey("send"));

        custom_dialog_library_cancel_button.setText(getkey("action_cancel"));
        dialcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code, String dialCode) {
                        picker.dismiss();
                        dialcode.setText(dialCode);
                        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        mgr.hideSoftInputFromWindow(dialcode.getWindowToken(), 0);
                    }
                });
            }
        });
        custom_dialog_library_ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (custom_dialog_library_ok_button.getText().toString().length() == 0) {
                    appUtils.AlertError(VerifyPhoneno_Constrain.this, getkey("action_error"), getkey("dialcodecntempty"));
                } else if (custom_dialog_library_message_textview.getText().toString().length() == 0) {
                    appUtils.AlertError(VerifyPhoneno_Constrain.this, getkey("action_error"), getkey("login_page_alert_phoneNoempty"));
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    binding.mobileno.setText(dialcode.getText().toString().trim()+""+custom_dialog_library_message_textview.getText().toString().trim());
                    verifyPhonenoViewModel.changenumber(dialcode.getText().toString().trim(), custom_dialog_library_message_textview.getText().toString().trim());
                    verifyPhonenoViewModel.getChangenumber().observe(VerifyPhoneno_Constrain.this, new Observer<String>() {
                        @Override
                        public void onChanged(@Nullable String response) {
                            progressBar.setVisibility(View.GONE);
                            alertDialog.dismiss();
                            String status = "", Sresponse = "", otpStatus = "", otpPin = "";
                            try {
                                JSONObject object = new JSONObject(response);
                                status = object.getString("status");
                                Sresponse = object.getString("response");
                                if (status.equalsIgnoreCase("1")) {
                                    otpStatus = object.getString("otp_status");
                                    otpPin = object.getString("otp");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (status.equalsIgnoreCase("1")) {
                                if (otpStatus.equalsIgnoreCase("development")) {
                                    binding.otpview.setText(otpPin);
                                }
                                sessionManager.setOtpstatusAndPin(otpStatus, otpPin);
                            } else {
                                Alert(getkey("action_error"), Sresponse);
                            }
                        }
                    });
                }
            }
        });

        custom_dialog_library_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appUtils.CloseKeyboard(VerifyPhoneno_Constrain.this);
                alertDialog.dismiss();
            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.show();

    }

    private void setbackground(final ImageView image, View view) {

        ImageView iv = (ImageView) view;
        final Drawable d = iv.getBackground();
        image.setBackground(getResources().getDrawable(R.drawable.rect_green));
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                image.setBackground(d.getCurrent());
            }
        }, 100);
    }


    private void initView() {
        appUtils = AppUtils.getInstance(this);
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user1 = sessionManager.getOtpstatusAndPin();
        sOtpPin = user1.get(SessionManager.KEY_REG_OTP_PIN);
        sOtpPinStatus = user1.get(SessionManager.KEY_REG_OTP_STATUS);
        picker = CountryPicker.newInstance(getkey("select_country_lable"));

        if (getIntent().hasExtra("hashMap")) {
            HashMap<String, String> hashMap = (HashMap<String, String>) getIntent().getSerializableExtra("hashMap");
            gloablhasmap = hashMap;
            verifyPhonenoViewModel.setHashMap(hashMap);
            binding.mobileno.setText(hashMap.get("dail_code") + "" + hashMap.get("mobile_number"));
        }

        if (sOtpPinStatus.equalsIgnoreCase("development")) {
            binding.otpview.setText(sOtpPin);
        }
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(VerifyPhoneno_Constrain.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onBackPressed() {

    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
