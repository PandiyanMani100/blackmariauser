package com.blackmaria.viewmodel;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;


public class VerifyPhonenoFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public VerifyPhonenoFactory(Activity context)
    {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass)
    {
        return (T) new VerifyPhonenoViewModel(context);
    }
}
