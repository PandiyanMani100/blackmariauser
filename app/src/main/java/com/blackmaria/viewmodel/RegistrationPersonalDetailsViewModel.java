package com.blackmaria.viewmodel;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;


import com.blackmaria.LanguageDb;
import com.blackmaria.api_request.ApIServices;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.databinding.RegisterPersonalDetailsConstrainBinding;
import com.blackmaria.iconstant.Iconstant;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import java.util.HashMap;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class RegistrationPersonalDetailsViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private SessionManager sessionManager;
    LanguageDb mhelper;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private String sDriverID = "";
    private Dialog dialog;
    private MutableLiveData<String> emailchecking = new MutableLiveData<>();

    private RegisterPersonalDetailsConstrainBinding binding;

    public RegistrationPersonalDetailsViewModel(Activity context) {
        this.context = context;
        mhelper = new LanguageDb(context);
        sessionManager = SessionManager.getInstance(context);
        cd = ConnectionDetector.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        isInternetPresent = cd.isConnectingToInternet();
    }


    public void confirm() {
        CloseKeyboardNew();
        if (binding.editName.getText().toString().trim().equalsIgnoreCase("")) {
            AlertError(getkey("timer_label_alert_sorry"), getkey("profile_label_alert_username"));
        } else if (binding.editEmail.getText().toString().trim().length() == 0) {
            AlertError(getkey("timer_label_alert_sorry"), getkey("profile_label_alert_emailempty"));
        }else if (!isValidEmail(binding.editEmail.getText().toString().trim().replace(" ", ""))) {
            AlertError(getkey("timer_label_alert_sorry"),getkey("profile_label_alert_email"));
        } else if (binding.txtspnrgender.getText().toString().trim().equalsIgnoreCase("Select Gender")) {
            AlertError(getkey("timer_label_alert_sorry"), getkey("profile_label_alert_gender"));
        } else if (binding.txtspnrgender.getText().toString().trim().equalsIgnoreCase("Gender")) {
            AlertError(getkey(":timer_label_alert_sorry"), getkey("profile_label_alert_gender"));
        } else if (binding.exptxt.getText().toString().trim().equalsIgnoreCase("EXPERIENCE") || binding.exptxt.getText().toString().trim().equalsIgnoreCase("Driving Experience")) {
            AlertError(getkey("timer_label_alert_sorry"), getkey("profile_lable_experience"));
        } else if (binding.editStreetname.getText().toString().trim().equalsIgnoreCase("")) {
            AlertError(getkey("timer_label_alert_sorry"),getkey("profile_label_alert_streetname"));
        } else if (binding.editCity.getText().toString().trim().equalsIgnoreCase("")) {
            AlertError(getkey("timer_label_alert_sorry"), getkey("profile_label_alert_cityname"));
        } else if (binding.editState.getText().toString().trim().equalsIgnoreCase("")) {
            AlertError(getkey("timer_label_alert_sorry"), getkey("profile_label_alert_state"));
        } else if (binding.editPostalcode.getText().toString().trim().length() == 0) {
            AlertError(getkey("timer_label_alert_sorry"), getkey("profile_label_alert_postcodeempty"));
        } else if (binding.editPostalcode.getText().toString().trim().length() < 5) {
            AlertError(getkey("timer_label_alert_sorry"), getkey("profile_label_alert_postcodeemptyincorrect"));
        } else if (binding.txtspnrcountry.getText().toString().trim().equalsIgnoreCase("Select Country")) {
            AlertError(getkey("timer_label_alert_sorry"), getkey("profile_label_alert_selectcountryname"));
        } else {
            checkEmailexists(Iconstant.Verifyemail, binding.editEmail.getText().toString().trim());
        }
    }


    private void AlertError(String title, String message) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(context);
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE);
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT);
        bar.animation(true);

        SnackbarManager.show(bar, context);


    }


    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == context.getCurrentFocus()) ? null : context.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkEmailexists(String Url, String email) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", email);

        ApIServices apIServices = new ApIServices(context, RegistrationPersonalDetailsViewModel.this);
        apIServices.dooperation(Url, jsonParams);
    }

    public void setvalidation(RegisterPersonalDetailsConstrainBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getEmailchecking() {
        return emailchecking;
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        emailchecking.postValue(val);
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}
