package com.blackmaria.viewmodel;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;


public class RegistrationSelectLocationFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public RegistrationSelectLocationFactory(Activity context)
    {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass)
    {
        return (T) new RegistrationSelectLocationViewModel(context);
    }
}
