package com.blackmaria.viewmodel;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.widget.ImageView;
import android.widget.TextView;


import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;

import java.util.HashMap;

public class RegistrationTermsViewModel extends ViewModel {

    private Activity context;
    private SessionManager sessionManager;
    private ConnectionDetector cd;


    private ImageView RL_exit;
    private TextView Btn_registerNow;

    public RegistrationTermsViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        cd = new ConnectionDetector(context);
        HashMap<String, String> user = sessionManager.getUserDetails();

    }

    public void backclick() {
        context.finish();
        context.overridePendingTransition(R.anim.enter, R.anim.exit);
    }


}
