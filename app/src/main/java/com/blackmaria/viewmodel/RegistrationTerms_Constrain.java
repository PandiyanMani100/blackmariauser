package com.blackmaria.viewmodel;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;


import com.blackmaria.LanguageDb;
import com.blackmaria.api_request.ApiIntentService;
import com.blackmaria.R;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.databinding.NewregisterTermsAndConditionsConstrainBinding;
import com.blackmaria.iconstant.Iconstant;

import java.util.HashMap;

public class RegistrationTerms_Constrain extends AppCompatActivity {

    private RegistrationTermsViewModel registrationTermsViewModel;
    private AppUtils appUtils;
    LanguageDb mhelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appUtils = AppUtils.getInstance(this);
        mhelper = new LanguageDb(this);
        final NewregisterTermsAndConditionsConstrainBinding binding = DataBindingUtil.setContentView(this, R.layout.newregister_terms_and_conditions_constrain);
        registrationTermsViewModel = ViewModelProviders.of(this, new RegisterationTermsFactory(this)).get(RegistrationTermsViewModel.class);
        binding.setRegistrationTermsViewModel(registrationTermsViewModel);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        Intent intents = new Intent(RegistrationTerms_Constrain.this, ApiIntentService.class);
        intents.putExtra("params", jsonParams);
        intents.putExtra("url", Iconstant.getvehicledetails);
        startService(intents);

        binding.txtLabelMagic.setText(getkey("magicofdriver"));
        binding.signupasparter.setText(getkey("signupparter"));
        binding.joiningpartner.setText(getkey("joining"));
        binding.eulatv.setText(getkey("label_eula"));
        binding.privacytv.setText(getkey("label_privacy_policy"));
        binding.ivtermstv.setText(getkey("label_terms_and_conditions"));
        binding.btnRegisterNow.setText(getkey("label_register"));

        binding.btnRegisterNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!binding.eula.isChecked()) {
                    appUtils.AlertError(RegistrationTerms_Constrain.this,getkey("action_error"),getkey("kindly_agree"));
                } else if (!binding.privacy.isChecked()) {
                    appUtils.AlertError(RegistrationTerms_Constrain.this,getkey("action_error"),getkey("kindly_agree_privacy"));
                } else if (!binding.terms.isChecked()) {
                    appUtils.AlertError(RegistrationTerms_Constrain.this,getkey("action_error"),getkey("kindly_agrre_terms_ofservice"));
                } else {
                    Intent intent = new Intent(RegistrationTerms_Constrain.this, RegistrationSelectLocation_constrain.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });

        binding.eulatv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUtils.intentbrowser(RegistrationTerms_Constrain.this,Iconstant.privacyurl);
            }
        });

        binding.privacytv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUtils.intentbrowser(RegistrationTerms_Constrain.this,Iconstant.privacyurl);
            }
        });

        binding.ivtermstv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUtils.intentbrowser(RegistrationTerms_Constrain.this,Iconstant.termsurl);
            }
        });

    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
