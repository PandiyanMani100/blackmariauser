package com.blackmaria.viewmodel;

import android.app.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;


import com.blackmaria.LanguageDb;
import com.blackmaria.R;
import com.blackmaria.databinding.RegisterLoginDetailsConstrainBinding;
import com.blackmaria.iconstant.Iconstant;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileOutputStream;


public class RegistrationLoginDetails extends AppCompatActivity implements RegistrationLoginDetailsViewModel.Getcountrycodepicker {


    private String CarCatId = "", SDriverReferalCode = "", driverLocationPlaceId = "", SimageName = "";
    private static final int PERMISSION_REQUEST_CODE = 111;
    private int permissionCode = -1;
    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int TAKE_PHOTO_REQUEST = 022;
    LanguageDb mhelper;


    private RegistrationLoginDetailsViewModel registrationLoginDetailsViewModel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        mhelper = new LanguageDb(this);
        RegisterLoginDetailsConstrainBinding binding = DataBindingUtil.setContentView(this, R.layout.register_login_details_constrain);
        registrationLoginDetailsViewModel = ViewModelProviders.of(this, new RegistrationLoginDetailsFactory(this)).get(RegistrationLoginDetailsViewModel.class);
        binding.setRegistrationLoginDetailsViewModel(registrationLoginDetailsViewModel);

        Intent in = getIntent();
        if (in.hasExtra("vehicleId")) {
            CarCatId = in.getStringExtra("vehicleId");
            driverLocationPlaceId = in.getStringExtra("driverLocationPlaceId");
        }

        registrationLoginDetailsViewModel.setiDs(binding);
        registrationLoginDetailsViewModel.getcompanynames(Iconstant.getcompanynames,driverLocationPlaceId);

        registrationLoginDetailsViewModel.setpickercontext(RegistrationLoginDetails.this,CarCatId,driverLocationPlaceId);

        binding.logininfo.setText(getkey("logininformation"));
        binding.locationtext.setText(getkey("type_of_registration"));
        binding.editMobilenumber.setHint(getkey("edit_mobilenumber"));
        binding.editPincode.setHint(getkey("create_pincode"));
        binding.tvRefferalcode.setHint(getkey("have_a_referral_code"));
        binding.tvVerify.setText(getkey("verify"));
        binding.btnConfirm.setText(getkey("confirm_label"));
        binding.blackmariainc.setText(getkey("label_blackmaria_inc_lowecase"));

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (permissionCode == 0) {
                        registrationLoginDetailsViewModel.cameraIntent();
                    } else if (permissionCode == 1) {
                        registrationLoginDetailsViewModel.galleryIntent();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                registrationLoginDetailsViewModel.onSelectFromGalleryResult(data);
            } else if (requestCode == TAKE_PHOTO_REQUEST) {
                registrationLoginDetailsViewModel.onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                registrationLoginDetailsViewModel.onCroppedImageResult(data);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
            }
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    public void triggerpicker() {
        registrationLoginDetailsViewModel.picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
