package com.blackmaria.viewmodel;

import android.app.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


import com.blackmaria.LanguageDb;
import com.blackmaria.R;
import com.blackmaria.databinding.RegisterVehicleDetailsConstrainBinding;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.yalantis.ucrop.UCrop;

import java.util.Calendar;
import java.util.HashMap;

public class RegistervehicledetailsConstrain extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, RegistervehicledetailsViewModel.registerclickreturn {

    int uploadtype=0;
    private RegisterVehicleDetailsConstrainBinding binding;
    private RegistervehicledetailsViewModel registervehicledetailsViewModel;
    private static final int PERMISSION_REQUEST_CODE = 111;
    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int TAKE_PHOTO_REQUEST = 022;
    private int year, month, day, pickerNumber = 0;
    private Calendar calendar;
    private DatePickerDialog pickerDialog;
    LanguageDb mhelper;
    private String strRoadTax = "", strLicenseTax = "", strInsuranceExp = "";
    int typeofimage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.register_vehicle_details_constrain);
        registervehicledetailsViewModel = ViewModelProviders.of(this, new RegistervehicledetailsFactory(this)).get(RegistervehicledetailsViewModel.class);
        mhelper = new LanguageDb(this);
        binding.setRegistervehicledetailsViewModel(registervehicledetailsViewModel);

        registervehicledetailsViewModel.setIds(binding);
        registervehicledetailsViewModel.setregisterclickreturn(this);

        binding.roadtax.setOnClickListener(this);
        binding.drivinglicense.setOnClickListener(this);
        binding.insurance.setOnClickListener(this);
        binding.imgBack.setOnClickListener(this);


        binding.attachroadtax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeofimage  =1;
                registervehicledetailsViewModel.uploadpohoto(typeofimage);
            }
        });

        binding.attachinsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeofimage  =2;
                registervehicledetailsViewModel.uploadpohoto(typeofimage);
            }
        });

        binding.drivinglicsenseimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeofimage  =3;
                registervehicledetailsViewModel.uploadpohoto(typeofimage);
            }
        });

        binding.personalinfo.setText(getkey("vehicleinfo"));
        binding.ecpirydatta.setText(getkey("expiry_date"));
        binding.vehiclename.setText(getkey("vehicle_photo_n_upload"));
        binding.roadtax.setHint(getkey("roadtax"));
        binding.insurance.setHint(getkey("insurance"));
        binding.drivinglicense.setHint(getkey("driving_license"));
        binding.platenumber.setHint(getkey("plate_number"));
        binding.vetype.setText(getkey("vehicle_type"));
        binding.vehiclemaker.setText(getkey("vehicle_maker"));
        binding.txtvehicletype.setText(getkey("select"));
        binding.txtvehiclemaker.setText(getkey("select"));
        binding.txtvehiclmodel.setText(getkey("vehicle_model"));
        binding.modelnotlisted.setHint(getkey("type_model_if_not_listed"));
        binding.yeardofma.setText(getkey("manufacturer"));
        binding.txtyears.setText(getkey("select"));
        binding.btnConfirm.setText(getkey("register"));


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // If permissionCode is 0 -> Its For Camera
                    // If permissionCode is 1 -> Its For Pick From Gallery
                    registervehicledetailsViewModel.uploadpohoto(uploadtype);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                registervehicledetailsViewModel.onSelectFromGalleryResult(data);
            } else if (requestCode == TAKE_PHOTO_REQUEST) {
                registervehicledetailsViewModel.onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                registervehicledetailsViewModel.onCroppedImageResult(data);
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }
    }

    private void showDatePicker() {

        pickerDialog = DatePickerDialog.newInstance(this, year, month, day);
        pickerDialog.setThemeDark(false);
        pickerDialog.showYearPickerFirst(false);
        pickerDialog.setAccentColor(getResources().getColor(R.color.signin_and_signup_slider_ball_blue));
        pickerDialog.setMinDate(Calendar.getInstance());
        pickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        datePickerDialog.dismiss();
        int months = month + 1;

        this.year = year;
        this.month = month;
        this.day = day;

        if (pickerNumber == 1) {
            binding.roadtax.setText(day + "-" + months + "-" + year);
            strRoadTax = day + "-" + months + "-" + year;

            uploadtype = 1;
            //registervehicledetailsViewModel.uploadpohoto(uploadtype);

        } else if (pickerNumber == 2) {
            binding.drivinglicense.setText(day + "-" + months + "-" + year);
            strLicenseTax = day + "-" + months + "-" + year;
            uploadtype = 3;
            //registervehicledetailsViewModel.uploadpohoto(uploadtype);
        } else if (pickerNumber == 3) {
            binding.insurance.setText(day + "-" + months + "-" + year);
            strInsuranceExp = day + "-" + months + "-" + year;
            uploadtype = 2;
            //registervehicledetailsViewModel.uploadpohoto(uploadtype);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == binding.roadtax) {
            CloseKeyboardNew();
            pickerNumber = 1;
            showDatePicker();
        } else if (view == binding.drivinglicense) {
            CloseKeyboardNew();
            pickerNumber = 2;
            showDatePicker();
        } else if (view == binding.insurance) {
            CloseKeyboardNew();
            pickerNumber = 3;
            showDatePicker();
        } else if (view == binding.imgBack) {
            onBackPressed();

        }
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void movetonextscreen(HashMap<String, String> jsonParams) {
        Intent i = new Intent(this, VerifyPhoneno_Constrain.class);
        i.putExtra("hashMap", jsonParams);
        startActivity(i);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
