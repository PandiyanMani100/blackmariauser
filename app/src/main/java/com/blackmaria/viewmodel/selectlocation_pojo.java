package com.blackmaria.viewmodel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class selectlocation_pojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        public ArrayList<Locations> getLocations() {
            return locations;
        }

        public void setLocations(ArrayList<Locations> locations) {
            this.locations = locations;
        }

        @SerializedName("locations")
        ArrayList<Locations> locations = new ArrayList<Locations>();


        public class Locations {
            @SerializedName("id")
            private String id;
            @SerializedName("city")
            private String city;
            @SerializedName("lat")
            private String lat;
            @SerializedName("lon")
            private String lon;


            // Getter Methods 

            public String getId() {
                return id;
            }

            public String getCity() {
                return city;
            }

            public String getLat() {
                return lat;
            }

            public String getLong() {
                return lon;
            }

            // Setter Methods 

            public void setId(String id) {
                this.id = id;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public void setLong(String lon) {
                this.lon = lon;
            }
        }
    }
}


