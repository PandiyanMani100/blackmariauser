package com.blackmaria.hockeyapp;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



/**
 * Created by Prem Kumar and Anitha on 11/12/2015.
 */
public class FragmentHockeyApp extends Fragment
{
    private static String APP_ID ="22d9f1b92d89401aa5a83ab6c5169eab";// "9f8e1861d5cc413ba593e3367676bca3";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        checkForUpdates();
        return null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
        System.out.println("************ GC Memory cleared***********");
    }




    @Override
    public void onResume() {
        super.onResume();

//        checkForCrashes();
    }

    @Override
    public void onPause() {
        super.onPause();
//        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        unregisterManagers();
    }

    private void checkForCrashes() {
//        CrashManager.register(getActivity(), APP_ID);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
//        UpdateManager.register(getActivity(), APP_ID);
    }

    private void unregisterManagers() {
//        UpdateManager.unregister();
        // unregister other managers if necessary...
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
    }
}

