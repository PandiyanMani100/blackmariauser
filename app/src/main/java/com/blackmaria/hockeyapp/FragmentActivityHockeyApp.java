package com.blackmaria.hockeyapp;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;


/**
 * Created by Prem Kumar and Anitha on 11/23/2015.
 */
public class FragmentActivityHockeyApp extends FragmentActivity
{
    private static String APP_ID ="22d9f1b92d89401aa5a83ab6c5169eab";// "9f8e1861d5cc413ba593e3367676bca3";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }
    @Override
    protected void onResume() {
        super.onResume();
//        checkForCrashes();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
        System.out.println("************ GC Memory cleared***********");
    }



    @Override
    protected void onPause() {
        super.onPause();
//        unregisterManagers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterManagers();
    }

    private void checkForCrashes() {
//        CrashManager.register(this, APP_ID);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
//        UpdateManager.register(this, APP_ID);
    }

    private void unregisterManagers() {
//        UpdateManager.unregister();
        // unregister other managers if necessary...
    }
}

