package com.blackmaria.hockeyapp;

import android.app.Activity;
import android.os.Bundle;


/**
 * Created by Prem Kumar and Anitha on 11/12/2015.
 */
public class ActivityHockeyApp extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        System.gc();

//        checkForUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();

//        checkForCrashes();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
        System.out.println("************ GC Memory cleared***********");
    }



    @Override
    protected void onPause() {
        super.onPause();
//        unregisterManagers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterManagers();
    }

    private void checkForCrashes() {
//        CrashManager.register(this, APP_ID);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
//        UpdateManager.register(this, APP_ID);
    }

    private void unregisterManagers() {
//        UpdateManager.unregister();
        // unregister other managers if necessary...
    }

}
