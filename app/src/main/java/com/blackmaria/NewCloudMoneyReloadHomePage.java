package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NewCloudMoneyReloadHomePage extends ActivityHockeyApp implements View.OnClickListener {
    private RelativeLayout Rl1;
    private ImageView imgBack;
    private ImageView imgBwallet;
    private CustomTextView txtLabelCurrentBalance;
    private RelativeLayout Rl2;
    private static CustomEdittext edtMobileNo;
    private LinearLayout buyTv;
    private ImageView indicatingImage;
    private RelativeLayout verifylayout;
    private LinearLayout lytReloadAmount1;
    private TextView reloadAmount1;
    private LinearLayout lytReloadAmount2;
    private TextView reloadAmount2;
    private LinearLayout lytReloadAmount3;
    private ImageView vovcherImage;
    private TextView reloadAmount3;
    private View view;
    private LinearLayout lytReloadAmount4;
    private View viewDummy;
    private TextView reloadAmount4;


    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;

    private String buyAmount = "0.0";
    private String UserID = "";
    private Boolean isDatavailable = false;
    private Boolean isPaymentListAvailable = false;

    private RefreshReceiver refreshReceiver;
    private String walletReloadAmount = "";
    private String current_balance = "", auto_charge_status = "", currency = "", wal_recharge_amount_one = "", wal_recharge_amount_two = "", wal_recharge_amount_three = "", wal_recharge_amount_four = "";
    ArrayList<String> paymentcardlistName;
    ArrayList<String> paymentcardlistCode;
    ArrayList<String> paymentcardlistInActive;
    ArrayList<String> paymentcardlistActive;
    HashMap<String, ArrayList<String>> paymentList;
    private String fromBookingpage ="";

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadHomePage.this, Navigation_new.class);
                        startActivity(intent1);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadHomePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadHomePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_money_reload_home_new);
        initialize();
    }

    private void initialize() {

        session = new SessionManager(NewCloudMoneyReloadHomePage.this);
        cd = new ConnectionDetector(NewCloudMoneyReloadHomePage.this);
        isInternetPresent = cd.isConnectingToInternet();


        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        //sSecurePin = session.getSecurePin();

        paymentcardlistName = new ArrayList<String>();
        paymentcardlistCode = new ArrayList<String>();
        paymentcardlistInActive = new ArrayList<String>();
        paymentcardlistActive = new ArrayList<String>();

        Rl1 = (RelativeLayout) findViewById(R.id.Rl_1);
        imgBack = (ImageView) findViewById(R.id.img_back);
        imgBwallet = (ImageView) findViewById(R.id.img_bwallet);
        txtLabelCurrentBalance = (CustomTextView) findViewById(R.id.txt_label_current_balance);
        Rl2 = (RelativeLayout) findViewById(R.id.Rl_2);
        edtMobileNo = (CustomEdittext) findViewById(R.id.edt_mobile_no);
        buyTv = (LinearLayout) findViewById(R.id.buy_tv);
        indicatingImage = (ImageView) findViewById(R.id.indicating_image);
        verifylayout = (RelativeLayout) findViewById(R.id.verifylayout);
        lytReloadAmount1 = (LinearLayout) findViewById(R.id.lyt_reload_amount1);
        reloadAmount1 = (TextView) findViewById(R.id.reload_amount1);
        lytReloadAmount2 = (LinearLayout) findViewById(R.id.lyt_reload_amount2);
        reloadAmount2 = (TextView) findViewById(R.id.reload_amount2);
        lytReloadAmount3 = (LinearLayout) findViewById(R.id.lyt_reload_amount3);
        reloadAmount3 = (TextView) findViewById(R.id.reload_amount3);
        view = (View) findViewById(R.id.view);
        lytReloadAmount4 = (LinearLayout) findViewById(R.id.lyt_reload_amount4);
        viewDummy = (View) findViewById(R.id.view_dummy);
        reloadAmount4 = (TextView) findViewById(R.id.reload_amount4);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        imgBack.setOnClickListener(this);
        verifylayout.setOnClickListener(this);
        lytReloadAmount1.setOnClickListener(this);
        lytReloadAmount2.setOnClickListener(this);
        lytReloadAmount3.setOnClickListener(this);
        lytReloadAmount4.setOnClickListener(this);
        buyTv.setOnClickListener(this);

        if (getIntent().hasExtra("frombookingpage")) {
            fromBookingpage = getIntent().getStringExtra("frombookingpage");
        }

        postData();

    }

    public static void refreshEdittext() {
        edtMobileNo.setText("");
    }

    private void hideSoftKeyboard() {
        if (getCurrentFocus() != null && getCurrentFocus() instanceof EditText) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtMobileNo.getWindowToken(), 0);
        }
    }

    private void postData() {
        if (isInternetPresent) {

            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", UserID);
            PostRquestReloadDashBoardvalue(Iconstant.new_cloudmoney_reload_dashboard_url, jsonParams);
        } else {

            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
        }
    }

    @Override
    public void onClick(View v) {
        if (v == imgBack) {
            hideSoftKeyboard();

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == buyTv) {
            if(paymentList.size()>0 && paymentList!=null) {
                buyAmount = edtMobileNo.getText().toString().trim();
                if (buyAmount.equalsIgnoreCase("")) {
                    buyAmount = "0.0";
                }
/*if (Double.parseDouble(buyAmount) < Double.parseDouble(wal_recharge_amount_one) || Double.parseDouble(buyAmount*/
                if (Double.parseDouble(buyAmount)<=0){
                    erroredit(edtMobileNo,getResources().getString(R.string.inert_amount_lable));
                  //  erroredit(edtMobileNo, getResources().getString(R.string.reloadamount_alert) + "  " + currency + wal_recharge_amount_one + " to " + currency + wal_recharge_amount_four);
                } else {
                    if(paymentList!=null) {
                        Intent intent = new Intent(NewCloudMoneyReloadHomePage.this, NewCloudMoneyReloadPaymentOptionPage.class);
                        intent.putExtra("insertAmount", buyAmount);
                        intent.putExtra("currency", currency);
                        intent.putExtra("currentBalance", current_balance);
                        intent.putExtra("autochargestatus", auto_charge_status);
                        intent.putExtra("paymentListMap", paymentList);
                        if(fromBookingpage.equalsIgnoreCase("1")) {
                            intent.putExtra("fromBookingpage", "1");
                        }
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                }
            }
        } else if (v == lytReloadAmount4) {
            session.setReloadPaymentType("4");
            edtMobileNo.setText(wal_recharge_amount_four);
            walletReloadAmount = edtMobileNo.getText().toString().trim();

            if(paymentList!=null) {
                Intent intent = new Intent(NewCloudMoneyReloadHomePage.this, NewCloudMoneyReloadPaymentOptionPage.class);
                intent.putExtra("insertAmount", wal_recharge_amount_four);
                intent.putExtra("currency", currency);
                intent.putExtra("currentBalance", current_balance);
                intent.putExtra("autochargestatus", auto_charge_status);
                intent.putExtra("paymentListMap", paymentList);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else if (v == lytReloadAmount1) {
            session.setReloadPaymentType("1");
            edtMobileNo.setText(wal_recharge_amount_one);
            walletReloadAmount = edtMobileNo.getText().toString().trim();
            if(paymentList!=null) {
                Intent intent = new Intent(NewCloudMoneyReloadHomePage.this, NewCloudMoneyReloadPaymentOptionPage.class);
                intent.putExtra("insertAmount", wal_recharge_amount_one);
                intent.putExtra("currency", currency);
                intent.putExtra("currentBalance", current_balance);
                intent.putExtra("autochargestatus", auto_charge_status);
                intent.putExtra("paymentListMap", paymentList);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        } else if (v == lytReloadAmount2) {
            session.setReloadPaymentType("2");
            edtMobileNo.setText(wal_recharge_amount_two);
            walletReloadAmount = edtMobileNo.getText().toString().trim();

            if(paymentList!=null) {
                Intent intent = new Intent(NewCloudMoneyReloadHomePage.this, NewCloudMoneyReloadPaymentOptionPage.class);
                intent.putExtra("insertAmount", wal_recharge_amount_two);
                intent.putExtra("currency", currency);
                intent.putExtra("currentBalance", current_balance);
                intent.putExtra("autochargestatus", auto_charge_status);
                intent.putExtra("paymentListMap", paymentList);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        } else if (v == lytReloadAmount3) {
            session.setReloadPaymentType("3");
            edtMobileNo.setText(wal_recharge_amount_three);
            walletReloadAmount = edtMobileNo.getText().toString().trim();
            if(paymentList!=null) {
                Intent intent = new Intent(NewCloudMoneyReloadHomePage.this, NewCloudMoneyReloadPaymentOptionPage.class);
                intent.putExtra("insertAmount", wal_recharge_amount_three);
                intent.putExtra("currency", currency);
                intent.putExtra("currentBalance", current_balance);
                intent.putExtra("autochargestatus", auto_charge_status);
                intent.putExtra("paymentListMap", paymentList);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else if (v == verifylayout) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }


    private void PostRquestReloadDashBoardvalue(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(NewCloudMoneyReloadHomePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------PostRquestReloadDashBoardvalue jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyReloadHomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {


            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------PostRquestReloadDashBoardvalue reponse-------------------" + response);

                String Sstatus = "", Smessage = "", current_month_credit = "", current_month_debit = "", currencyconversion="",xendit_secret_key = "", xendit_public_key = "", month = "", year = "", month_year = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    auto_charge_status = object.getString("auto_charge_status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");

                        currency = jsonObject.getString("currency");
                        current_balance = jsonObject.getString("current_balance");
                        current_month_credit = jsonObject.getString("current_month_credit");
                        current_month_debit = jsonObject.getString("current_month_debit");
                        xendit_secret_key = jsonObject.getString("xendit_secret_key");
                        xendit_public_key = jsonObject.getString("xendit_public_key");
                        if(jsonObject.has("exchange_value")){
                            currencyconversion= jsonObject.getString("exchange_value");
                            session. setcurrencyconversionKey(currencyconversion);
                        }
                        month = jsonObject.getString("month");
                        year = jsonObject.getString("year");
                        month_year = jsonObject.getString("month_year");

                        JSONObject rechBndryObj = jsonObject.getJSONObject("recharge_boundary");

                        if (rechBndryObj.length() > 0) {
                            wal_recharge_amount_one = rechBndryObj.getString("wal_recharge_amount_one");
                            wal_recharge_amount_two = rechBndryObj.getString("wal_recharge_amount_two");
                            wal_recharge_amount_three = rechBndryObj.getString("wal_recharge_amount_three");
                            wal_recharge_amount_four = rechBndryObj.getString("wal_recharge_amount_four");
                        }
                        Object check_object = jsonObject.get("payment_list");
                        if (check_object instanceof JSONArray) {
                            JSONArray payment_list_jsonArray = jsonObject.getJSONArray("payment_list");
                            paymentList = new HashMap<String, ArrayList<String>>();
                            if (payment_list_jsonArray.length() > 0) {
                                paymentcardlistName.clear();
                                paymentcardlistCode.clear();
                                paymentcardlistActive.clear();
                                paymentcardlistInActive.clear();

                                for (int i = 0; i < payment_list_jsonArray.length(); i++) {
                                    JSONObject payment_obj = payment_list_jsonArray.getJSONObject(i);
                                    paymentcardlistName.add(payment_obj.getString("name"));
                                    paymentcardlistCode.add(payment_obj.getString("code"));
                                    paymentcardlistInActive.add(payment_obj.getString("inactive_icon"));
                                    paymentcardlistActive.add(payment_obj.getString("icon"));
                                }
                                paymentList.put("bankName", paymentcardlistName);
                                paymentList.put("bankCode", paymentcardlistCode);
                                paymentList.put("bankInactive", paymentcardlistInActive);
                                paymentList.put("bankActive", paymentcardlistActive);

                                isPaymentListAvailable = true;

                            } else {
                                isPaymentListAvailable = false;
                            }
                        }
                        dialogDismiss();

                    } else {
                        Smessage = object.getString("response");
                        dialogDismiss();
                    }
                    isDatavailable = true;
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();
                if (Sstatus.equalsIgnoreCase("1")) {
                    reloadAmount1.setText(currency +" "+ wal_recharge_amount_one);
                    reloadAmount2.setText(currency +" "+ wal_recharge_amount_two);
                    reloadAmount3.setText(currency +" "+ wal_recharge_amount_three);
                    reloadAmount4.setText(currency +" "+ wal_recharge_amount_four);
                    session.setXenditPublicKey(xendit_public_key);

                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyReloadHomePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewCloudMoneyReloadHomePage.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }
}
