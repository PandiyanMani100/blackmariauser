package com.blackmaria.InterFace;

import android.os.Message;

/**
 * Created by user129 on 7/19/2017.
 */
public interface CallBack {

    void onComplete(String LocationName);

    void onError(String errorMsg);
    void onCompleteAddress(String message);
}
