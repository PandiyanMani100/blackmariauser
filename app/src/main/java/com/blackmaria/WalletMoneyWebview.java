package com.blackmaria;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.blackmaria.newdesigns.view.ChangepaymentConstrain;
import com.blackmaria.newdesigns.view.RechargepaymentsuccessConstrain;
import com.blackmaria.utils.ApIServices;
import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class WalletMoneyWebview extends AppCompatActivity implements ApIServices.completelisner {
    private ImageView back;
    private Context context;
    private WebView webview;
    private ProgressBar progressBar, apiload;

    private String Str_recharge_amount = "";
    private String Str_currency_symbol = "";
    private String Str_currentBalance = "";
    private SessionManager session;
    private String sDriverID = "";
    private String Str_paymentType = "", email = "";
    private String fromBookingpage = "";
    private String radeemNumber = "";
    private Dialog dialog;
    private AppUtils appUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_money_webview);
        context = getApplicationContext();
        initialize();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final PkDialog mDialog = new PkDialog(WalletMoneyWebview.this);
                mDialog.setDialogTitle(getResources().getString(R.string.wallet_webview_lable_cancel_transaction));
                mDialog.setDialogMessage(getResources().getString(R.string.wallet_webview_lable_cancel_transaction_proceed));
                mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        mgr.hideSoftInputFromWindow(back.getWindowToken(), 0);
                        onBackPressed();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                });
                mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });

        // Show the progress bar
        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && progressBar.getVisibility() == ProgressBar.GONE) {
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }
                progressBar.setProgress(progress);

                if (progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                }
            }
        });

    }

    private void initialize() {
        appUtils = AppUtils.getInstance(WalletMoneyWebview.this);
        session = new SessionManager(WalletMoneyWebview.this);
        back = findViewById(R.id.backimg);
        webview = (WebView) findViewById(R.id.wallet_money_webview);
        progressBar = (ProgressBar) findViewById(R.id.wallet_money_webview_progressbar);
        apiload = (ProgressBar) findViewById(R.id.apiload);
        // Enable Javascript to run in WebView
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);
        // Allow Zoom in/out controls
        webview.getSettings().setBuiltInZoomControls(true);
        // Zoom out the best fit your screen
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);
//        radeemNumber = session.REDEEM_CODE;// radeemNumber=session.getRedeemCouponcode();
        Intent i = getIntent();
        Str_recharge_amount = i.getStringExtra("walletMoney_recharge_amount");
        Str_currency_symbol = i.getStringExtra("walletMoney_currency_symbol");
        Str_currentBalance = i.getStringExtra("walletMoney_currentBalance");
        Str_paymentType = i.getStringExtra("walletMoney_paymentType");
        radeemNumber = i.getStringExtra("walletMoney_radeemcode");
        email = i.getStringExtra("email");

        if (getIntent().hasExtra("frombookingpage")) {
            fromBookingpage = getIntent().getStringExtra("frombookingpage");
        }

        if (Str_paymentType.contains("Credit Card")) {
        } else if (Str_paymentType.contains("Paypal") || Str_paymentType.contains("paypal")) {
            startWebView(Iconstant.wallet_money_paypal_webview_url + sDriverID + "&total_amount=" + Str_recharge_amount + "&redeem_code=" + radeemNumber);
        } else if (Str_paymentType.contains("Stripe") || Str_paymentType.contains("stripe")) {
            startWebView(Iconstant.wallet_money_stripe_webview_url + sDriverID + "&total_amount=" + Str_recharge_amount + "&redeem_code=" + radeemNumber);
        }
    }


    private void startWebView(String url) {
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            @Override
            public void onLoadResource(WebView view, String url) {
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                try {

                    if (url.contains("/wallet-recharge/failed")) {
                        Intent intent1 = new Intent(WalletMoneyWebview.this, ChangepaymentConstrain.class);
                        intent1.putExtra("amount", Str_recharge_amount);
                        intent1.putExtra("amounts", Str_currency_symbol + " " + Str_recharge_amount);
                        intent1.putExtra("msg", "SORRY! YOUR PAYPAL PAYMENT FAIL");
                        startActivity(intent1);
                    } else if (url.contains("/wallet-recharge/success")) {
                        webview.clearHistory();
                        String PayPalData[] = url.split("\\&");
                        String transId = PayPalData[1].replace("trans_id=", "");

                        if (Str_paymentType.equalsIgnoreCase("paypal")) {
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("user_id", sDriverID);
                            jsonParams.put("transaction_id", transId);
                            paypalresponse(Iconstant.paypalsuccessapi, jsonParams);
                        }
                    } else if (url.contains("/wallet-recharge/pay-completed")) {
                        webview.clearHistory();
                    } else if (url.contains("/wallet-recharge/pay-cancel")) {
                        Intent intent1 = new Intent(WalletMoneyWebview.this, ChangepaymentConstrain.class);
                        intent1.putExtra("amount", Str_recharge_amount);
                        intent1.putExtra("amounts", Str_currency_symbol + " " + Str_recharge_amount);
                        intent1.putExtra("msg", "payment failed");
                        startActivity(intent1);
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });

        //Load url in webView
        webview.loadUrl(url);
    }


    public void finishMethod() {
        finish();
        onBackPressed();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack();
        } else {
            // Let the system handle the back button
            super.onBackPressed();
        }
    }


    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            final PkDialog mDialog = new PkDialog(WalletMoneyWebview.this);
            mDialog.setDialogTitle(getResources().getString(R.string.wallet_webview_lable_cancel_transaction));
            mDialog.setDialogMessage(getResources().getString(R.string.wallet_webview_lable_cancel_transaction_proceed));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    // close keyboard
                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(back.getWindowToken(), 0);
                    onBackPressed();
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            });
            mDialog.setNegativeButton(getResources().getString(R.string.action_cancel), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();
            return true;
        }
        return false;
    }

    @Override
    public void sucessresponse(String val) {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        JSONObject json = null;
        try {
            json = new JSONObject(val);
            if (json.getString("status").equalsIgnoreCase("1")) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(val);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent1 = new Intent(WalletMoneyWebview.this, RechargepaymentsuccessConstrain.class);
                intent1.putExtra("json", jsonObject.toString());
                intent1.putExtra("paymenttype", "paypal");
                startActivity(intent1);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else {
                appUtils.AlertError(WalletMoneyWebview.this, getResources().getString(R.string.action_error), json.getString("response"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }


    public void paypalresponse(String url, HashMap<String, String> jsonParams) {
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(WalletMoneyWebview.this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(WalletMoneyWebview.this, WalletMoneyWebview.this);
        apIServices.dooperation(url, jsonParams);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


}
