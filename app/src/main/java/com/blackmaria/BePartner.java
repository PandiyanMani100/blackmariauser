package com.blackmaria;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;

/**
 * Created by user129 on 5/5/2017.
 */
public class BePartner extends FragmentActivityHockeyApp {


   private RelativeLayout Rl_back, Bt_Bp_sign_up;
   private ImageView Iv_home;
    SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.be_partner_layout);
        initialize();


        Rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });


        Iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BePartner.this, Navigation_new.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


            }
        });


        Bt_Bp_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Iconstant.driver_signup_web_url));
                startActivity(browserIntent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });



    }


    private void initialize() {

        session=new SessionManager(BePartner.this);
        Rl_back=(RelativeLayout) findViewById(R.id.bepartner_page_header_back_layout);
        Iv_home=(ImageView)findViewById(R.id.bepartner_home);
        Bt_Bp_sign_up=(RelativeLayout)findViewById(R.id.be_partner_page_signup_btn);


    }


    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }
}



