package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.pojo.WalletMoneyPojo;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.AppController;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.volley.VolleyMultipartRequest;
import com.blackmaria.widgets.CircularImageView;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.blackmaria.utils.Myanim.SlideDown;
import static com.blackmaria.utils.Myanim.SlideUP;

/**
 * Created by user144 on 12/28/2017.
 */

@SuppressLint("Registered")
public class NewProfile_Page extends FragmentActivityHockeyApp {
    //------user profile-------------
    private RatingBar profRatingbar;
    private CustomTextView Tv_Email, Tv_name, Tv_Gender, Tv_Age, Tv_Address, Tv_City, Tv_State, Tv_PinCode, Tv_Country, Tv_SOS_Contact1_Add_Remove, Tv_SOS_Contact2_Remove,
            Tv_sos_Name, Tv_sos_mobileno, Tv_sos_email, Tv_sos_relationship, Tv_sos_address,
            Tv_sos_Name1, Tv_sos_mobileno1, Tv_sos_email1, Tv_sos_relationship1, Tv_sos_address1;
    //----------login user---info--------
    private CustomTextView Tv_login_emailId, Tv_login_mobile_no, Tv_login_country_code, Tv_login_pincode;
    //-----------------diglog-user--profile---edit------------
    RelativeLayout User_Sos_Update_btn, Rl_SOS_Contact_2, Rl_SOS_Contact_1;
    ImageView Back_press;
    AutoCompleteTextView Et_Dig_Address, Et_SosDig_Address;
    private String Dialog_shows_sos_city = "", Dialog_shows_sos_state = "", Dialog_shows_sos_country = "", Dialog_shows_sos_pincode = "";
    private String Dialog_shows_sos_city1 = "", Dialog_shows_sos_state1 = "", Dialog_shows_sos_country1 = "", Dialog_shows_sos_pincode1 = "";
    // -------sos--diglog--edit-----
    private CustomEdittext Et_SosDig_Email, Et_SosDig_name, Et_SosDig_Mobile_No, Et_SosDig_Relationship, Et_SosDig_City, Et_SosDig_State, Et_SosDig_PinCode, Et_SosDig_Country;

    private RelativeLayout Rl_sos_contact_1_edit, Rl_sos_contact_2_edit;
    private CircularImageView CIv_UserImage;
    private ImageView Sos_dialog_exit_Im;
    private RelativeLayout Rl_MyProfile_Edit, Rl_LoginInfo_Edit;


    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    AppInfoSessionManager appInfoSessionManager;
    Dialog dialog;
    private ServiceRequest mRequest;
    private String UserID = "";
    private String gcmID = "";
    Context _context;
    final int PERMISSION_REQUEST_CODE = 111;
    private Dialog photo_dialog;
    private Uri camera_FileUri;
    Bitmap bitMapThumbnail;
    private byte[] byteArray;
    private int REQUEST_TAKE_PHOTO = 1;
    private int galleryRequestCode = 2;
    private static final String IMAGE_DIRECTORY_NAME = "blackmaria";
    private static final String TAG = "";
    private String mSelectedFilePath = "";
    private int mobno_change_request_code = 200;
    private String sAuthKey = "";
    private boolean sos_contact1_stats = false;
    private boolean sos_contact2_stats = false;

    private boolean up_sos_contact1_stats = false;
    private boolean up_sos_contact2_stats = false;

    String key_satus = "";
    private boolean isDataAvailable = false;
    private boolean isAddressAvailable = false;
    private Dialog mLoadingDialog;
    private String sLatitude = "", sLongitude = "", sSelected_location = "";

    ArrayList<String> itemList_location = new ArrayList<String>();
    ArrayList<String> itemList_placeId = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    String frommenu = "0";

    String currencySymbol = "";
    String card_id = "", card_number = "", exp_month = "", exp_year = "", card_type = "", customer_id = "", bank_code = "", bank_name = "", acc_number = "", acc_holder_name = "", CountryCodeForBnak = "";
    private boolean isCardAvailable = true;

    private RelativeLayout Rl_card_info;
    private CustomTextView Tv_card_number;
    private CustomTextView Tv_Exp_year;


    private CustomTextView Tv_card_number1, cardinfo_tv;
    private CustomTextView Tv_Exp_date1;
    private CustomTextView Tv_Exp_year1;
    private CustomTextView Tv_Card_type1;
    private CustomTextView Tv_Add_card1;

    private RelativeLayout Rl_Change_Card, Rl_Change_Card1;
    BroadcastReceiver updateReceiver;
    private ServiceRequest stripRequest;
    private RelativeLayout Rl_myprofile_header, Rl_loginInfo_header, Rl_cardInfo_header, Rl_sos_header, AllinOneReletiveLayout;
    private Boolean MyProfile = true, LoginInfo = true, CardInfo = true, Sos = true;
    private RelativeLayout Rl_SOS_View;
    public static Activity newProfile_page;
    ArrayList<WalletMoneyPojo> paymentcardlist;
    private TextView sos_lable;
    RelativeLayout Rl_sos_bottom;

    ExpandableRelativeLayout Rl_myprofile_bottom, Rl_loginInfo_bottom, Rl_cardInfo_bottom, Rl_cardInfo_bottom1;
    TextView customTextView2;
    String Sstatus = "", Smessage = "", SuserName = "", Semail = "", Sgender = "", Sage = "", Scountry = "", Sdistrict = "",
            Sstate = "", Spincode = "", Spasscode = "", Slocality = "", sCountryCode = "", SmobNo = "", suserimage = "", Sreviwe = "";


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewProfile_Page.this, MyRideRating.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewProfile_Page.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_page_new);

        //strict mode

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        intialize();
        newProfile_page = NewProfile_Page.this;
        // Receiving the data from broadcast
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.PaymentListPage.refreshPage");
        updateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent != null) {
                    Refresh_page();
                }
            }
        };
        registerReceiver(updateReceiver, filter);
        Rl_Change_Card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseBankCard(1);
            }
        });

        Rl_Change_Card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseBankCard(2);
            }
        });

        Rl_MyProfile_Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(NewProfile_Page.this, NewProfile_Page_ProfileEdit.class);
                in.putExtra("name", Tv_name.getText().toString().trim());
                in.putExtra("email", Tv_Email.getText().toString().trim());
                in.putExtra("gender", Tv_Gender.getText().toString().trim());
                in.putExtra("age", Tv_Age.getText().toString().trim());
                in.putExtra("address", Tv_Address.getText().toString().trim());
                in.putExtra("city", Tv_City.getText().toString().trim());
                in.putExtra("state", Tv_State.getText().toString().trim());
                in.putExtra("picode", Tv_PinCode.getText().toString().trim());
                in.putExtra("country", Tv_Country.getText().toString().trim());
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        Rl_LoginInfo_Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(NewProfile_Page.this, NewProfile_Page_LoginEdit.class);
                in.putExtra("countrycode", sCountryCode);
                in.putExtra("email", Semail);
                in.putExtra("mobilenumber", SmobNo);
                in.putExtra("pincode", Spasscode);
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        Rl_sos_contact_1_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /* if (!Tv_sos_Name.getText().toString().toLowerCase().equalsIgnoreCase("n/a")) {*/
                Intent in = new Intent(NewProfile_Page.this, NewProfile_Page_SosEdit.class);
                in.putExtra("name", Tv_sos_Name.getText().toString().replace(":", ""));
                in.putExtra("email", Tv_sos_email.getText().toString().replace(":", ""));
                in.putExtra("mobile", Tv_sos_mobileno.getText().toString().replace(":", ""));
                in.putExtra("relatuion", Tv_sos_relationship.getText().toString().trim().replace(":", ""));
                in.putExtra("address", Tv_sos_address.getText().toString().trim().replace(":", ""));
                in.putExtra("city", Dialog_shows_sos_city);
                in.putExtra("state", Dialog_shows_sos_state);
                in.putExtra("picode", Dialog_shows_sos_pincode);
                in.putExtra("country", Dialog_shows_sos_country);

                in.putExtra("countryCode", sCountryCode);
                in.putExtra("soscount", "1");
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                /*}*/
                //ShowSosDialog("1");

            }
        });

        Rl_sos_contact_2_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*  if (!Tv_sos_Name1.getText().toString().toLowerCase().equalsIgnoreCase("n/a")) {*/
                Intent in = new Intent(NewProfile_Page.this, NewProfile_Page_SosEdit.class);
                in.putExtra("name", Tv_sos_Name1.getText().toString().replace(":", ""));
                in.putExtra("email", Tv_sos_email1.getText().toString().replace(":", ""));
                in.putExtra("mobile", Tv_sos_mobileno1.getText().toString().replace(":", ""));
                in.putExtra("relatuion", Tv_sos_relationship1.getText().toString().trim().replace(":", ""));
                in.putExtra("address", Tv_sos_address1.getText().toString().trim().replace(":", ""));
                in.putExtra("city", Dialog_shows_sos_city1);
                in.putExtra("state", Dialog_shows_sos_state1);
                in.putExtra("picode", Dialog_shows_sos_pincode1);
                in.putExtra("country", Dialog_shows_sos_country1);

                in.putExtra("countryCode", sCountryCode);
                in.putExtra("soscount", "2");
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);

                // ShowSosDialog("2");

            }
        });


        Back_press.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (frommenu.equalsIgnoreCase("1")) {
                    Intent intent = new Intent(NewProfile_Page.this, Navigation_new.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.exit, R.anim.enter);
                    finish();
                } else {
                    Intent intent = new Intent(NewProfile_Page.this, Navigation_new.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.exit, R.anim.enter);
                    finish();
                }


            }
        });


        CIv_UserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()) {
                        requestPermission();
                    } else {
                        chooseimage();
                    }
                } else {
                    chooseimage();
                }

            }
        });


        Tv_SOS_Contact1_Add_Remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Tv_SOS_Contact1_Add_Remove.getText().toString().toLowerCase().equalsIgnoreCase("ADD SOS CONTACT")) {

                    Tv_SOS_Contact1_Add_Remove.setText(getResources().getString(R.string.remove));
                    Rl_SOS_Contact_2.setVisibility(View.VISIBLE);
                    Rl_SOS_View.setVisibility(View.VISIBLE);
                    Intent in = new Intent(NewProfile_Page.this, NewProfile_Page_SosEdit.class);
                    in.putExtra("name", "");
                    in.putExtra("email", "");
                    in.putExtra("mobile", "");
                    in.putExtra("relatuion", "");
                    in.putExtra("address", "");
                    in.putExtra("city", "");
                    in.putExtra("state", "");
                    in.putExtra("picode", "");
                    in.putExtra("country", "");

                    in.putExtra("countryCode", sCountryCode);
                    in.putExtra("soscount", "2");
                    startActivity(in);
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                } else if (Tv_SOS_Contact1_Add_Remove.getText().toString().toLowerCase().equalsIgnoreCase("-remove")) {

                    if (!Tv_sos_Name.getText().toString().equalsIgnoreCase("n/a")) {

                        removeContact("key_1");

                    } else {
                        if (!Tv_sos_Name1.getText().toString().equalsIgnoreCase("n/a")) {
                            Tv_sos_Name.setText(":" + Tv_sos_Name1.getText().toString());
                            Tv_sos_email.setText(":" + Tv_sos_email1.getText().toString());
                            Tv_sos_mobileno.setText(":" + Tv_sos_mobileno1.getText().toString());
                            Tv_sos_relationship.setText(":" + Tv_sos_relationship1.getText().toString());
                            Tv_sos_address.setText(":" + Tv_sos_address1.getText().toString());

                            Tv_SOS_Contact1_Add_Remove.setText(getResources().getString(R.string.addsos));
                            Rl_SOS_Contact_2.setVisibility(View.GONE);
                            Rl_SOS_View.setVisibility(View.GONE);

                            Tv_sos_Name1.setText(getResources().getString(R.string.smal_na));
                            Tv_sos_email1.setText(getResources().getString(R.string.smal_na));
                            Tv_sos_mobileno1.setText(getResources().getString(R.string.smal_na));
                            Tv_sos_relationship1.setText(getResources().getString(R.string.smal_na));
                            Tv_sos_address1.setText(getResources().getString(R.string.smal_na));

                        } else {
                            Tv_sos_Name.setText(getResources().getString(R.string.smal_na));
                            Tv_sos_email.setText(getResources().getString(R.string.smal_na));
                            Tv_sos_mobileno.setText(getResources().getString(R.string.smal_na));
                            Tv_sos_relationship.setText(getResources().getString(R.string.smal_na));
                            Tv_sos_address.setText(getResources().getString(R.string.smal_na));

                            Tv_SOS_Contact1_Add_Remove.setText(getResources().getString(R.string.addsos));
                            Rl_SOS_Contact_2.setVisibility(View.GONE);
                            Rl_SOS_View.setVisibility(View.GONE);
                        }

                    }


                }

            }
        });


        Tv_SOS_Contact2_Remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Tv_SOS_Contact2_Remove.getText().toString().toLowerCase().equalsIgnoreCase("-remove")) {
                    if (!Tv_sos_Name1.getText().toString().toLowerCase().equalsIgnoreCase("n/a")) {
                        removeContact("key_2");
                    } else {
                        Tv_SOS_Contact1_Add_Remove.setText(getResources().getString(R.string.addsos));
                        Rl_SOS_Contact_2.setVisibility(View.GONE);
                        Tv_sos_Name1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_email1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_mobileno1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_relationship1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_address1.setText(getResources().getString(R.string.smal_na));
                        if (!Tv_sos_Name.getText().toString().toLowerCase().equalsIgnoreCase("n/a")) {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.VISIBLE);
                        } else {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });

        Rl_myprofile_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaypoutHandle(1);
                //   collapse(AllinOneReletiveLayout);
                if (Rl_myprofile_bottom.isExpanded()) {
                    Rl_myprofile_bottom.toggle();//setVisibility(View.VISIBLE);
                    MyProfile = false;
                } else {
                    Rl_myprofile_bottom.toggle();////Rl_myprofile_bottom.setVisibility(View.GONE);
                    MyProfile = true;
                }

            }
        });
        Rl_loginInfo_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaypoutHandle(2);
                //   collapse(AllinOneReletiveLayout);
                if (Rl_loginInfo_bottom.isExpanded()) {
                    customTextView2.setText(getResources().getString(R.string.login_information));
                    Rl_loginInfo_bottom.toggle();
                    LoginInfo = false;
                } else {
                    customTextView2.setText(getResources().getString(R.string.login_information));
                    Rl_loginInfo_bottom.toggle();
                    LoginInfo = true;
                }
            }
        });
        Rl_cardInfo_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaypoutHandle(3);
                //  collapse(AllinOneReletiveLayout);
                if (CardInfo) {
                    if (CountryCodeForBnak.equalsIgnoreCase("ID")) {
                        Rl_cardInfo_bottom1.toggle();
                        cardinfo_tv.setText(getResources().getString(R.string.bankinfo));
                    } else {
                        Rl_cardInfo_bottom.toggle();
                        cardinfo_tv.setText(getResources().getString(R.string.cardinfo));
                    }
                    CardInfo = false;
                } else {
                    if (CountryCodeForBnak.equalsIgnoreCase("ID")) {
                        Rl_cardInfo_bottom1.toggle();
                        cardinfo_tv.setText(getResources().getString(R.string.addbank));
                    } else {
                        cardinfo_tv.setText(getResources().getString(R.string.addbank));
                        Rl_cardInfo_bottom.toggle();
                    }
                    CardInfo = true;
                }
            }
        });
        Rl_sos_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaypoutHandle(4);
                // collapse(AllinOneReletiveLayout);
                if (Sos) {
                    sos_lable.setText(getResources().getString(R.string.sosinfo));
                    SlideUP(Rl_sos_bottom, NewProfile_Page.this);
                    Sos = false;
                } else {
                    sos_lable.setText(getResources().getString(R.string.sosocontact));
                    SlideDown(Rl_sos_bottom, NewProfile_Page.this);
                    Sos = true;
                }
            }
        });
    }

    private void intialize() {
        _context = NewProfile_Page.this;
        session = new SessionManager(NewProfile_Page.this);
        appInfoSessionManager = new AppInfoSessionManager(NewProfile_Page.this);
        cd = new ConnectionDetector(NewProfile_Page.this);
        isInternetPresent = cd.isConnectingToInternet();

        paymentcardlist = new ArrayList<>();
        HashMap<String, String> info = session.getUserDetails();
        String tipstime = info.get(SessionManager.KEY_PHONENO);
        UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> app = appInfoSessionManager.getAppInfo();
        sAuthKey = app.get(appInfoSessionManager.KEY_APP_IDENTITY);

        HashMap<String, String> user = session.getUserDetails();
        gcmID = user.get(SessionManager.KEY_GCM_ID);


        CIv_UserImage = (CircularImageView) findViewById(R.id.login_info_user_imageview);
        Tv_name = (CustomTextView) findViewById(R.id.myprofile_name_textview);
        Tv_Email = (CustomTextView) findViewById(R.id.myprofile_email_textview);
        Tv_Gender = (CustomTextView) findViewById(R.id.myprofile_gender_textview);
        Tv_Age = (CustomTextView) findViewById(R.id.myprofile_age_textview);
        Tv_Address = (CustomTextView) findViewById(R.id.myprofile_address_textview);
        Tv_City = (CustomTextView) findViewById(R.id.myprofile_city_textview);
        Tv_State = (CustomTextView) findViewById(R.id.myprofile_state_textview);
        Tv_PinCode = (CustomTextView) findViewById(R.id.myprofile_postcode_textview);
        Tv_Country = (CustomTextView) findViewById(R.id.myprofile_country_textview);
        customTextView2 = (TextView) findViewById(R.id.customTextView2);
        Tv_login_emailId = (CustomTextView) findViewById(R.id.login_info_emailid_textview);
        Tv_login_country_code = (CustomTextView) findViewById(R.id.login_info_country_code_textview);
        Tv_login_mobile_no = (CustomTextView) findViewById(R.id.login_info_mobile_no_textview);
        Tv_login_pincode = (CustomTextView) findViewById(R.id.login_info_pincode_textview);
        sos_lable = (TextView) findViewById(R.id.sos_lable);

        Rl_MyProfile_Edit = (RelativeLayout) findViewById(R.id.myprofile_edit_layout);
        Rl_LoginInfo_Edit = (RelativeLayout) findViewById(R.id.login_info_edit_layout);
        Back_press = (ImageView) findViewById(R.id.profile_back_press);


        Tv_sos_Name = (CustomTextView) findViewById(R.id.profile_sos_contact_1_name_textview);
        Tv_sos_email = (CustomTextView) findViewById(R.id.profile_sos_contact_1_email_textview);
        Tv_sos_mobileno = (CustomTextView) findViewById(R.id.profile_sos_contact_1_mobile_no_textview);
        Tv_sos_relationship = (CustomTextView) findViewById(R.id.profile_sos_contact_1_relationship_textview);
        Tv_sos_address = (CustomTextView) findViewById(R.id.profile_sos_contact_1_address_textview);
        Tv_sos_Name1 = (CustomTextView) findViewById(R.id.profile_sos_contact_2_name_textview);
        Tv_sos_email1 = (CustomTextView) findViewById(R.id.profile_sos_contact_2_email_textview);
        Tv_sos_mobileno1 = (CustomTextView) findViewById(R.id.profile_sos_contact_2_mobile_no_textview);
        Tv_sos_relationship1 = (CustomTextView) findViewById(R.id.profile_sos_contact_2_relationship_textview);
        Tv_sos_address1 = (CustomTextView) findViewById(R.id.profile_sos_contact_2_address_textview);

        Rl_sos_contact_1_edit = (RelativeLayout) findViewById(R.id.sos_contact_1_edit_layout);
        Rl_sos_contact_2_edit = (RelativeLayout) findViewById(R.id.sos_contact_2_edit_layout);
        Tv_SOS_Contact1_Add_Remove = (CustomTextView) findViewById(R.id.sos_add_or_remove_textview);
        Tv_SOS_Contact2_Remove = (CustomTextView) findViewById(R.id.sos_contact_2_remove_textview);

        Rl_SOS_View = (RelativeLayout) findViewById(R.id.sos_view);
        profRatingbar = (RatingBar) findViewById(R.id.myride_rating_single_ratingbar);

        Rl_card_info = (RelativeLayout) findViewById(R.id.profile_page_card_info_layouot);
        Tv_card_number = (CustomTextView) findViewById(R.id.card_number_textview);
        Tv_Exp_year = (CustomTextView) findViewById(R.id.card_exp_year_textview);


        Rl_Change_Card = (RelativeLayout) findViewById(R.id.card_info_change_card_layout);


        Rl_myprofile_bottom = (ExpandableRelativeLayout) findViewById(R.id.profile_page_myprofile_bottom_layout);
        Rl_loginInfo_bottom = (ExpandableRelativeLayout) findViewById(R.id.profile_page_login_info_bottom_layout);
        Rl_cardInfo_bottom = (ExpandableRelativeLayout) findViewById(R.id.card_info_bottom_layout);
        Rl_sos_bottom = (RelativeLayout) findViewById(R.id.sos_bottom_layout);
        Rl_cardInfo_bottom1 = (ExpandableRelativeLayout) findViewById(R.id.card_info_bottom_layout1);

        Rl_SOS_Contact_2 = (RelativeLayout) findViewById(R.id.profile_sos_contact_2_layout);
        Rl_SOS_Contact_1 = (RelativeLayout) findViewById(R.id.sos_contact_1_layout);


        Rl_myprofile_header = (RelativeLayout) findViewById(R.id.my_profile_layout);
        Rl_loginInfo_header = (RelativeLayout) findViewById(R.id.profile_login_info_layout);
        Rl_cardInfo_header = (RelativeLayout) findViewById(R.id.card_info_layout);
        Rl_sos_header = (RelativeLayout) findViewById(R.id.profile_page_sos_contact_layout);


        cardinfo_tv = (CustomTextView) findViewById(R.id.cardinfo_tv);
        Tv_card_number1 = (CustomTextView) findViewById(R.id.card_number_textview1);
        Tv_Exp_date1 = (CustomTextView) findViewById(R.id.card_exp_date_textview1);
        Tv_Exp_year1 = (CustomTextView) findViewById(R.id.card_exp_year_textview1);
        Tv_Card_type1 = (CustomTextView) findViewById(R.id.card_type_textview1);

        Tv_Add_card1 = (CustomTextView) findViewById(R.id.ad_bank);

        Rl_Change_Card1 = (RelativeLayout) findViewById(R.id.card_info_change_card_layout1);


        frommenu = "";
        Intent intent = getIntent();
        if (intent.hasExtra("frommenu")) {
            frommenu = intent.getStringExtra("frommenu");
            System.out.println("=============muruga frommenu======" + frommenu);
        }
        try {
            if (isInternetPresent) {
                PostRequest(Iconstant.get_profile_detail_url);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Refresh_page() {

        isInternetPresent = cd.isConnectingToInternet();
        try {
            if (isInternetPresent) {
                PostRequest(Iconstant.get_profile_detail_url);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // --------------------Method for choose image to edit profileimage--------------------
    private void chooseimage() {
        photo_dialog = new Dialog(NewProfile_Page.this);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        photo_dialog.setCanceledOnTouchOutside(true);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                photo_dialog.dismiss();
            }
        });
    }


    public void removeContact(String index) {
        cd = new ConnectionDetector(NewProfile_Page.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            PostRequest_removeSos(Iconstant.profile_sos_remove_url, index);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
        }
    }

    private void PostRequest(String Url) {
        dialog = new Dialog(NewProfile_Page.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        System.out.println("-----------Profilepage getdata jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(NewProfile_Page.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Profilepage getdata reponse-------------------" + response);


                String key1_sos_name = "", key1_sos_email = "", key1_sos_mobileno = "", key1_sos_relationship = "", key1_sos_address = "",
                        key2_sos_name = "", key2_sos_email = "", key2_sos_mobileno = "", key2_sos_relationship = "", key2_sos_address = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject jsonObject = object.getJSONObject("response");
                        JSONObject user_profile_jsonObject = jsonObject.getJSONObject("user_profile");
                        if (user_profile_jsonObject.length() > 0) {

                            SuserName = user_profile_jsonObject.getString("user_name");
                            Semail = user_profile_jsonObject.getString("email");

                            Sgender = user_profile_jsonObject.getString("gender");
                            Sage = user_profile_jsonObject.getString("age");
                            Slocality = user_profile_jsonObject.getString("locality");
                            Sdistrict = user_profile_jsonObject.getString("district");
                            Sstate = user_profile_jsonObject.getString("state");
                            Scountry = user_profile_jsonObject.getString("country");
                            Spincode = user_profile_jsonObject.getString("pincode");
                            SmobNo = user_profile_jsonObject.getString("phone_number");
                            sCountryCode = user_profile_jsonObject.getString("country_code");
                            suserimage = user_profile_jsonObject.getString("user_image");
                            Sreviwe = user_profile_jsonObject.getString("avg_review");
                            Spasscode = user_profile_jsonObject.getString("passcode");

                        }

                        Object check_object1 = jsonObject.get("card_details");
                        if (check_object1 instanceof JSONArray) {

                            JSONArray cardDetailArray = jsonObject.getJSONArray("card_details");
                            if (cardDetailArray.length() > 0) {
                                for (int j = 0; j < cardDetailArray.length(); j++) {
                                    JSONObject driver_object = cardDetailArray.getJSONObject(j);
                                    card_id = driver_object.getString("card_id");
                                    card_number = driver_object.getString("card_number");
                                    exp_month = driver_object.getString("exp_month");
                                    exp_year = driver_object.getString("exp_year");
                                    card_type = driver_object.getString("card_type").toUpperCase();
                                    customer_id = driver_object.getString("customer_id");
                                }

                            } else {
                                card_id = "N/A";
                                card_number = "N/A";
                                exp_month = "N/A";
                                exp_year = "N/A";
                                card_type = "N/A";
                                customer_id = "N/A";
                                Tv_card_number.setText("");
                                Tv_Exp_year.setText("");
                            }
                        }


                        CountryCodeForBnak = jsonObject.getString("country_code_cca");
                        Object check_object2 = jsonObject.get("bankingArr");
                        if (check_object2 instanceof JSONObject) {
                            JSONObject bankDetailObj = jsonObject.getJSONObject("bankingArr");
                            if (bankDetailObj.length() > 0) {
                                acc_holder_name = bankDetailObj.getString("acc_holder_name");
                                acc_number = bankDetailObj.getString("acc_number");
                                bank_name = bankDetailObj.getString("bank_name");
                                bank_code = bankDetailObj.getString("bank_code");
                            }
                        }


                        Object check_object = jsonObject.get("emergency_contact");
                        if (check_object instanceof JSONObject) {

                            JSONObject emergency_contact_jsonObject = jsonObject.getJSONObject("emergency_contact");
                            if (emergency_contact_jsonObject.length() > 0) {

                                if (emergency_contact_jsonObject.has("key_1")) {
                                    JSONObject emergency_contactkey_1_jsonObject = emergency_contact_jsonObject.getJSONObject("key_1");
                                    if (emergency_contactkey_1_jsonObject.length() > 0) {


                                        key1_sos_name = emergency_contactkey_1_jsonObject.getString("name");
                                        key1_sos_email = emergency_contactkey_1_jsonObject.getString("email");
                                        key1_sos_mobileno = emergency_contactkey_1_jsonObject.getString("mobile");
                                        key1_sos_relationship = emergency_contactkey_1_jsonObject.getString("relationship");
                                        key1_sos_address = emergency_contactkey_1_jsonObject.getString("address");
                                        Dialog_shows_sos_city = emergency_contactkey_1_jsonObject.getString("city");
                                        Dialog_shows_sos_state = emergency_contactkey_1_jsonObject.getString("state");
                                        Dialog_shows_sos_pincode = emergency_contactkey_1_jsonObject.getString("postcode");
                                        Dialog_shows_sos_country = emergency_contactkey_1_jsonObject.getString("country");

                                        sos_contact1_stats = true;


                                        if (emergency_contact_jsonObject.has("key_2")) {
                                            JSONObject emergency_contactkey_2_jsonObject = emergency_contact_jsonObject.getJSONObject("key_2");
                                            if (emergency_contactkey_2_jsonObject.length() > 0) {

                                                key2_sos_name = emergency_contactkey_2_jsonObject.getString("name");
                                                key2_sos_email = emergency_contactkey_2_jsonObject.getString("email");
                                                key2_sos_mobileno = emergency_contactkey_2_jsonObject.getString("mobile");
                                                key2_sos_relationship = emergency_contactkey_2_jsonObject.getString("relationship");
                                                key2_sos_address = emergency_contactkey_2_jsonObject.getString("address");
                                                Dialog_shows_sos_city1 = emergency_contactkey_2_jsonObject.getString("city");
                                                Dialog_shows_sos_state1 = emergency_contactkey_2_jsonObject.getString("state");
                                                Dialog_shows_sos_pincode1 = emergency_contactkey_2_jsonObject.getString("postcode");
                                                Dialog_shows_sos_country1 = emergency_contactkey_2_jsonObject.getString("country");

                                                sos_contact2_stats = true;
                                            }
                                        }

                                    }
                                }
                            }

                        }
                        dialogDismiss();

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {

                  /*  if (CountryCodeForBnak.equalsIgnoreCase("ID")) {
                        cardinfo_tv.setText(getResources().getString(R.string.bankinfo));
                        cardinfo_tv.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        cardinfo_tv.setText(getResources().getString(R.string.cardinfo));
                        Rl_cardInfo_header.setBackground(getResources().getDrawable(R.drawable.gray_gradient_new_track));
                        cardinfo_tv.setTextColor(getResources().getColor(R.color.black));
                    }*/
                    Rl_loginInfo_bottom.setExpanded(true);

                    MyProfile = false;
                    if (acc_holder_name.equalsIgnoreCase("") && (acc_number.equalsIgnoreCase(""))) {
                        Tv_Add_card1.setText(getResources().getString(R.string.caps_addbank));
                        Tv_card_number1.setText(getResources().getString(R.string.na));
                        Tv_Exp_date1.setText(getResources().getString(R.string.na));
                        Tv_Exp_year1.setText(getResources().getString(R.string.na));
                        Tv_Card_type1.setText(getResources().getString(R.string.na));
                    } else {
                        Tv_Add_card1.setText(getResources().getString(R.string.edit_bank));
                        Tv_card_number1.setText(":" + acc_holder_name);
                        Tv_Exp_date1.setText(":" + acc_number);
                        Tv_Exp_year1.setText(":" + bank_name);
                        Tv_Card_type1.setText(":" + bank_code);
                    }


                    if (SuserName.equalsIgnoreCase("")) {
                        Tv_name.setText(getResources().getString(R.string.na));
                    } else {
                        Tv_name.setText(SuserName);
                    }
                    if (Semail.equalsIgnoreCase("")) {
                        Tv_Email.setText(getResources().getString(R.string.na));
                        Tv_login_emailId.setText(getResources().getString(R.string.na));
                    } else {
                        Tv_Email.setText(Semail);
                        Tv_login_emailId.setText(":" + Semail);
                    }


                    Picasso.with(NewProfile_Page.this).load(suserimage).placeholder(R.drawable.new_no_user_img).into(CIv_UserImage);
                    Tv_login_country_code.setText(":" + sCountryCode);
                    Tv_login_mobile_no.setText(SmobNo);
                    Tv_login_pincode.setText(Spasscode);


                    if (!Sage.equalsIgnoreCase("") || !Spincode.equalsIgnoreCase("") || !sCountryCode.equalsIgnoreCase("")) {

                        Tv_Gender.setText(Sgender);
                        Tv_Age.setText(Sage);
                        Tv_Address.setText(Slocality);
                        Tv_City.setText(Sdistrict);
                        Tv_State.setText(Sstate);
                        Tv_PinCode.setText(Spincode);
                        Tv_Country.setText(Scountry);
                        profRatingbar.setRating(Float.parseFloat(Sreviwe));

                    }
                    if (sos_contact1_stats) {
                        Tv_SOS_Contact1_Add_Remove.setVisibility(View.VISIBLE);
                        Tv_sos_Name.setText(":" + key1_sos_name);
                        Tv_sos_email.setText(":" + key1_sos_email);
                        Tv_sos_mobileno.setText(":" + key1_sos_mobileno);
                        Tv_sos_relationship.setText(":" + key1_sos_relationship);
                        Tv_sos_address.setText(":" + key1_sos_address);

                    } else {
                        Tv_SOS_Contact1_Add_Remove.setVisibility(View.GONE);
                    }
                    if (sos_contact2_stats) {

                        Rl_SOS_Contact_2.setVisibility(View.VISIBLE);
                        Tv_SOS_Contact1_Add_Remove.setText(getResources().getString(R.string.remove));
                        Rl_SOS_View.setVisibility(View.VISIBLE);

                        Tv_sos_Name1.setText(":" + key2_sos_name);
                        Tv_sos_email1.setText(":" + key2_sos_email);
                        Tv_sos_mobileno1.setText(":" + key2_sos_mobileno);
                        Tv_sos_relationship1.setText(":" + key2_sos_relationship);
                        Tv_sos_address1.setText(":" + key2_sos_address);

                    } else {
                        Rl_SOS_Contact_2.setVisibility(View.GONE);
                        Rl_SOS_View.setVisibility(View.GONE);
                    }
                    if (!card_number.equalsIgnoreCase("")) {
                        String cardNumber = card_number.substring(card_number.length() - 9, card_number.length());
                        cardNumber = cardNumber.substring(0, 4) + "-" + cardNumber.substring(4, cardNumber.length());
                        Tv_card_number.setText(cardNumber);
                        isCardAvailable = true;
                    } else {
                        isCardAvailable = false;
                        Tv_card_number.setText(getResources().getString(R.string.na));
                    }
                    if (!exp_year.equalsIgnoreCase("")) {
                        Tv_Exp_year.setText(getResources().getString(R.string.exp) + exp_month + "/" + exp_year);
                    } else {
                        Tv_Exp_year.setText(getResources().getString(R.string.na));
                    }
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void LaypoutHandle(int flag) {
        if (flag == 1) {
            if (Rl_loginInfo_bottom.isExpanded()) {
                customTextView2.setText(getResources().getString(R.string.login_information));
                Rl_loginInfo_bottom.toggle();
                LoginInfo = true;
            }

            if (Rl_cardInfo_bottom1.isExpanded()) {
                Rl_cardInfo_bottom1.toggle();
                cardinfo_tv.setText(getResources().getString(R.string.addbank));
                CardInfo = true;
            }

            if (Rl_cardInfo_bottom.isExpanded()) {
                Rl_cardInfo_bottom.toggle();
                cardinfo_tv.setText(getResources().getString(R.string.addbank));
                CardInfo = true;
            }
            if (Rl_sos_bottom.getVisibility() == View.VISIBLE) {
                sos_lable.setText(getResources().getString(R.string.sosocontact));
                Rl_sos_bottom.setVisibility(View.GONE);
                Sos = true;
            }
        } else if (flag == 2) {
            if (Rl_myprofile_bottom.isExpanded())
                Rl_myprofile_bottom.toggle();

            if (Rl_cardInfo_bottom1.isExpanded()) {
                Rl_cardInfo_bottom1.toggle();
                cardinfo_tv.setText(getResources().getString(R.string.addbank));
                CardInfo = true;
            }
            if (Rl_cardInfo_bottom.isExpanded()) {
                Rl_cardInfo_bottom.toggle();
                cardinfo_tv.setText(getResources().getString(R.string.addbank));
                CardInfo = true;
            }
            if (Rl_sos_bottom.getVisibility() == View.VISIBLE) {
                Rl_sos_bottom.setVisibility(View.GONE);
                sos_lable.setText(getResources().getString(R.string.sosocontact));
            }
            Sos = true;
        } else if (flag == 3) {
            if (Rl_myprofile_bottom.isExpanded())
                Rl_myprofile_bottom.toggle();


            if (Rl_loginInfo_bottom.isExpanded()) {
                customTextView2.setText(getResources().getString(R.string.login_information));
                Rl_loginInfo_bottom.toggle();
                LoginInfo = true;
            }

            if (Rl_sos_bottom.getVisibility() == View.VISIBLE)
                sos_lable.setText(getResources().getString(R.string.sosocontact));
            Sos = true;
            SlideDown(Rl_sos_bottom, NewProfile_Page.this);
        } else if (flag == 4) {
            if (Rl_loginInfo_bottom.isExpanded()) {
                customTextView2.setText(getResources().getString(R.string.login_information));
                Rl_loginInfo_bottom.toggle();
                LoginInfo = true;
            }
            if (Rl_myprofile_bottom.isExpanded())
                Rl_myprofile_bottom.toggle();

            if (Rl_cardInfo_bottom1.isExpanded()) {
                Rl_cardInfo_bottom1.toggle();
                cardinfo_tv.setText(getResources().getString(R.string.addbank));
                CardInfo = true;
            }
            if (Rl_cardInfo_bottom.isExpanded()) {
                Rl_cardInfo_bottom.toggle();
                cardinfo_tv.setText(getResources().getString(R.string.addbank));
                CardInfo = true;
            }

        }
    }

    private void PostRequest_removeSos(String Url, String index) {

        key_satus = index;
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("edit_index", index);
        System.out.println("-----------PostRequest_removeSos jsonParams--------------" + jsonParams);

        System.out.println("-----------PostRequest_removeSos index------------" + index);

        mRequest = new ServiceRequest(NewProfile_Page.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                Log.e("registr", response);

                System.out.println("--------------PostRequest_removeSos reponse-------------------" + response);

                String Sstatus = "", Smessage = "", Sotp_status = "", Sotp = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }

                if (Sstatus.equalsIgnoreCase("1")) {

                    if (key_satus.equalsIgnoreCase("key_1")) {


                        Tv_sos_Name.setText(Tv_sos_Name1.getText().toString());
                        Tv_sos_email.setText(Tv_sos_email1.getText().toString());
                        Tv_sos_mobileno.setText(Tv_sos_mobileno1.getText().toString());
                        Tv_sos_relationship.setText(Tv_sos_relationship1.getText().toString());
                        Tv_sos_address.setText(Tv_sos_address1.getText().toString());

                        Tv_SOS_Contact1_Add_Remove.setText(getResources().getString(R.string.addsos));
                        Rl_SOS_Contact_2.setVisibility(View.GONE);
                        Rl_SOS_View.setVisibility(View.GONE);

                        Tv_sos_Name1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_email1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_mobileno1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_relationship1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_address1.setText(getResources().getString(R.string.smal_na));

                        if (!Tv_sos_Name1.getText().toString().toLowerCase().equalsIgnoreCase("n/a")) {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.VISIBLE);
                        } else {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.GONE);
                        }

                    } else {

                        Tv_SOS_Contact1_Add_Remove.setText(getResources().getString(R.string.addsos));
                        Rl_SOS_Contact_2.setVisibility(View.GONE);

                        Tv_sos_Name1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_email1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_mobileno1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_relationship1.setText(getResources().getString(R.string.smal_na));
                        Tv_sos_address1.setText(getResources().getString(R.string.smal_na));

                        if (!Tv_sos_Name.getText().toString().toLowerCase().equalsIgnoreCase("n/a")) {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.VISIBLE);
                        } else {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.GONE);
                        }
                    }
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewProfile_Page.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        camera_FileUri = getOutputMediaFileUri(1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, camera_FileUri);
        // start the image capture Intent
        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, galleryRequestCode);
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", camera_FileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // get the file url
        camera_FileUri = savedInstanceState.getParcelable("file_uri");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;

                    final Bitmap bitmap = BitmapFactory.decodeFile(camera_FileUri.getPath(), options);
                    Bitmap thumbnail = bitmap;
                    final String picturePath = camera_FileUri.getPath();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                    File curFile = new File(picturePath);
                    try {
                        ExifInterface exif = new ExifInterface(curFile.getPath());
                        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                        int rotationInDegrees = exifToDegrees(rotation);

                        Matrix matrix = new Matrix();
                        if (rotation != 0f) {
                            matrix.preRotate(rotationInDegrees);
                        }
                        thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                    } catch (IOException ex) {
                        Log.e("TAG", "Failed to get Exif data", ex);
                    }

                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();

                    //------------Code to update----------
                    bitMapThumbnail = thumbnail;
                    CIv_UserImage.setImageBitmap(thumbnail);

                    UploadDriverImage(Iconstant.Edit_profile_image_url);

                } catch (OutOfMemoryError | NullPointerException e) {
                    e.printStackTrace();
                }

            } else if (requestCode == galleryRequestCode) {

                try {
                    Uri selectedImage = data.getData();
                    if (selectedImage.toString().startsWith("content://com.sec.android.gallery3d.provider")) {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        final String picturePath = c.getString(columnIndex);
                        c.close();

                        Picasso.with(NewProfile_Page.this).load(picturePath).resize(100, 100).into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                Bitmap thumbnail = bitmap;
                                mSelectedFilePath = picturePath;
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                                byteArray = byteArrayOutputStream.toByteArray();

                                //------------Code to update----------
                                bitMapThumbnail = thumbnail;
                                CIv_UserImage.setImageBitmap(thumbnail);
                                UploadDriverImage(Iconstant.Edit_profile_image_url);
                                // UploadDriverImage(" https://www.yourprintertechnician.com/v3/app/save-image");
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                            }
                        });
                    } else {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();

                        int columnIndex = c.getColumnIndex(filePath[0]);
                        final String picturePath = c.getString(columnIndex);
                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                        Bitmap thumbnail = bitmap; //getResizedBitmap(bitmap, 600);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        File curFile = new File(picturePath);

                        try {
                            ExifInterface exif = new ExifInterface(curFile.getPath());
                            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                            int rotationInDegrees = exifToDegrees(rotation);

                            Matrix matrix = new Matrix();
                            if (rotation != 0f) {
                                matrix.preRotate(rotationInDegrees);
                            }
                            thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                        } catch (IOException ex) {
                            Log.e("TAG", "Failed to get Exif data", ex);
                        }
                        thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                        byteArray = byteArrayOutputStream.toByteArray();
                        c.close();

                        //------------Code to update----------
                        bitMapThumbnail = thumbnail;
                        CIv_UserImage.setImageBitmap(thumbnail);
                        UploadDriverImage(Iconstant.Edit_profile_image_url);

                        // UploadDriverImage(" https://www.yourprintertechnician.com/v3/app/save-image");
                    }
                } catch (OutOfMemoryError | NullPointerException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == mobno_change_request_code && data != null) {
                String Scountrycode = data.getStringExtra("countryCode");
                String SmobNo = data.getStringExtra("phoneNo");
                String Smessage = data.getStringExtra("message");
//                Tv_countryCode.setText(Scountrycode);
//                Et_PhoneNo.setText(SmobNo);
                Alert(getResources().getString(R.string.action_success), Smessage);
            }

        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private void UploadDriverImage(String url) {

        dialog = new Dialog(NewProfile_Page.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                System.out.println("------------- image response-----------------" + response.data);


                String resultResponse = new String(response.data);
                System.out.println("-------------  response-----------------" + resultResponse);
                String sStatus = "", sResponse = "", SUser_image = "", Smsg = "", name = "";
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    Smsg = jsonObject.getString("response");
                    if (sStatus.equalsIgnoreCase("1")) {
                        SUser_image = jsonObject.getString("image_url");
//                        name = jsonObject.getString("picture_name");
                        CIv_UserImage.setImageBitmap(bitMapThumbnail);
                        if(!SUser_image.equals("")){
                            session.setUserImageUpdate(SUser_image);
                        }

//                        session.setUserImageName(name);
                        Alert(getResources().getString(R.string.action_success), Smsg);
                    } else {
                        Alert(getResources().getString(R.string.action_error), Smsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialogDismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialogDismiss();

                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserID);
                System.out.println("user_id---------------" + UserID);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("user_image", new DataPart("blackmaria.jpg", byteArray));
                System.out.println("user_image--------edit------" + byteArray);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authkey", sAuthKey);
                headers.put("User-agent", Iconstant.cabily_userAgent);
                headers.put("isapplication", Iconstant.cabily_IsApplication);
                headers.put("applanguage", Iconstant.cabily_AppLanguage);
                headers.put("apptype", Iconstant.cabily_AppType);
                headers.put("userid", UserID);
                headers.put("apptoken", gcmID);
                System.out.println("------------header---------" + headers);
                return headers;
            }

        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }

    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseimage();
                } else {
                    finish();
                }
                break;
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        closeKeyboard();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (updateReceiver != null) {
            unregisterReceiver(updateReceiver);
            updateReceiver = null;
        }
    }

    private void closeKeyboard() {
        /*InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);*/
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    private void ChooseBankCard(final int flag) {
//        final Dialog dialog = new Dialog(NewProfile_Page.this, R.style.DialogSlideAnim);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.otp_check_popup);
//        final Button confirm = (Button) dialog.findViewById(R.id.confirm_button);
//        final Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
//        final EditText insetPicode_Edt = (EditText) dialog.findViewById(R.id.insert_code);
//        final SmoothProgressBar smoothBar = (SmoothProgressBar) dialog.findViewById(R.id.smooth_progressbar);
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//        confirm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (insetPicode_Edt.getText().toString().trim().equalsIgnoreCase(session.getSecurePin())) {
//                    if (isCardAvailable) {
//                        if (flag == 1) {
//                            Intent intent = new Intent(NewProfile_Page.this, CardListPage.class);
//                            startActivity(intent);
//                            overridePendingTransition(R.anim.exit, R.anim.enter);
//                        } else {
//                            Intent in = new Intent(NewProfile_Page.this, NewProfile_Page_BankEdit.class);
//                            startActivity(in);
//                            overridePendingTransition(R.anim.enter, R.anim.exit);
//                        }
//                    } else {
//                        if (flag == 1) {
//                            Intent intent = new Intent(NewProfile_Page.this, NewProfile_Page_CardEdit.class);
//                            startActivity(intent);
//                            overridePendingTransition(R.anim.exit, R.anim.enter);
//                        } else {
//                            Intent in = new Intent(NewProfile_Page.this, NewProfile_Page_BankEdit.class);
//                            startActivity(in);
//                            overridePendingTransition(R.anim.enter, R.anim.exit);
//                        }
//                    }
//                } else {
//                    Alert(getResources().getString(R.string.timer_sorry), "Enter Valid Pincode");
//                }
//                dialog.dismiss();
//            }
//        });
//
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();

        if (isCardAvailable) {
            if (flag == 1) {
                Intent intent = new Intent(NewProfile_Page.this, CardListPage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit, R.anim.enter);
            } else {
                Intent in = new Intent(NewProfile_Page.this, NewProfile_Page_BankEdit.class);
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        } else {
            if (flag == 1) {
                Intent intent = new Intent(NewProfile_Page.this, NewProfile_Page_CardEdit.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit, R.anim.enter);
            } else {
                Intent in = new Intent(NewProfile_Page.this, NewProfile_Page_BankEdit.class);
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        }

    }


//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        if (frommenu.equalsIgnoreCase("1")) {
//            Intent intent = new Intent(NewProfile_Page.this, Navigation_new.class);
//            startActivity(intent);
//            overridePendingTransition(R.anim.exit, R.anim.enter);
//            finish();
//        } else {
//            Intent intent = new Intent(NewProfile_Page.this, Navigation_new.class);
//            startActivity(intent);
//            overridePendingTransition(R.anim.exit, R.anim.enter);
//            finish();
//        }
//    }


}
