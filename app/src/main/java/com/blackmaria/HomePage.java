package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.VideoView;

import com.android.volley.Request;
import com.blackmaria.adapter.CustomSpinnerAdapter;
import com.blackmaria.hockeyapp.FragmentHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.xmpp.XmppService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
//import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.danikula.videocache.HttpProxyCache;
import com.danikula.videocache.ProxyCacheException;
import com.devspark.appmsg.AppMsg;
import com.github.ybq.android.spinkit.SpinKitView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import static com.blackmaria.R.id.mylage;


public class HomePage extends FragmentHockeyApp {

    final String TAG = "TAG";
    private RelativeLayout Rl_BidFare, Rl_MyRide, Rl_MyWallet, Rl_Earnings, Rl_Mileage;
    ImageView Rl_BookNow, homeAnimateImage;
    private LinearLayout Ll_Sos, Ll_Support;
    private CustomTextView Tv_BidFare_Amount, /*Tv_Wallet_Amount,*/
            Tv_Ride_arrive_Time, Tv_Mylege, Tv_good_mrng;
    private static View parentView;
    private RelativeLayout Ll_Complaint;
    private TextView account_block_layout;
    private VideoView videoView;
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    HashMap<String, String> info;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String UserID = "", UserProfileUpdateStatus = "", basicUserProfileUpdateStatus = "", homeAnimImage = "";
    private String Ssupport_no = "";
    final int PERMISSION_REQUEST_CODE = 111;
    BroadcastReceiver updateReceiver, updateReceiver1;
    private RefreshReceiver refreshReceiver;
    SpinKitView spinKitView, spin_kit1;
    int stopPosition = 0;
    HttpProxyCache proxyCache;
    SeekBar seekBar1;
    private TextView complaint_ticket_count;
    private boolean isFirstime = true;
    IntentFilter filter, fileter2;
    private String completeStatus = "", CompleteStage = "", waitingtime;
    private String onGoingTxt = "";
    private String RideID = "", driverRequestMode = "", onGoingRideId = "", currencyconversion = "", xendit_key = "", payment_mode = "", ride_status = "", ride_amount = "", currency = "", insertAmount = "";
    final Handler handler = new Handler();
    private Context Mcontext;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(getActivity(), Navigation_new.class);
                        getActivity().startActivity(intent1);
                        getActivity().finish();
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(getActivity(), FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        getActivity().startActivity(intent1);
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                        getActivity().finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(getActivity(), FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        getActivity().startActivity(intent1);
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                        getActivity().finish();
                    }
                }
            }
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView != null) {
            ViewGroup parent = (ViewGroup) parentView.getParent();
            if (parent != null)
                parent.removeView(parentView);
        }
        try {
            parentView = inflater.inflate(R.layout.home_new, container, false);
        } catch (InflateException e) {
        }


        initialize(parentView);

        filter = new IntentFilter();
        filter.addAction("com.app.PaymentListPage.refreshhomedashBoardImage");
        updateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    info = session.getUserDetails();
                    homeAnimImage = info.get(SessionManager.KEY_HOMEUSERIMAGE);
                    if (!homeAnimImage.equalsIgnoreCase("") && !homeAnimImage.equalsIgnoreCase(null)) {
                        String Imsge = homeAnimImage.substring(homeAnimImage.lastIndexOf("."));
                        if (Imsge.equalsIgnoreCase(".gif")) {
                            System.out.println("=====================homeAnimImage gif==============" + Imsge + " " + homeAnimImage);
                           /* GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(homeAnimateImage);
                            Glide.with(HomePage.this).load(homeAnimImage).into(imageViewTarget);*/
                            Glide.with(HomePage.this)
                                    .load(homeAnimImage)
                                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                                    .into(new SimpleTarget<Drawable>() {
                                        @Override
                                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                            homeAnimateImage.setImageDrawable(resource);
                                        }
                                    });
                        } else {
                            System.out.println("=====================homeAnimImage==============" + homeAnimImage);
                            Picasso.with(getActivity()).load(String.valueOf(homeAnimImage)).into(homeAnimateImage);

                        }
                    }
                }
            }
        };
        getActivity().registerReceiver(updateReceiver, filter);
        fileter2 = new IntentFilter();
        fileter2.addAction("com.app.MilesPagePage.refreshhomePage");
        updateReceiver1 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    if (isInternetPresent) {
                        PostRequest(Iconstant.get_homepage_url);
                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }
                }
            }
        };
        getActivity().registerReceiver(updateReceiver1, fileter2);

//        MainPage.navigationNotifyChange();

        homeAnimateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HomePageImage_Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        Rl_BookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.setContinueTrip("no");
                if (UserProfileUpdateStatus.equalsIgnoreCase("0")) {
                    System.out.println("==============Muruga UserProfileUpdateStatus==========" + UserProfileUpdateStatus);
                    UpdateProfilePopUp();
                } else {
                    System.out.println("==============Muruga UserProfileUpdateStatus==========" + UserProfileUpdateStatus);
                    Intent intent = new Intent(getActivity(), Navigation_new.class);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
//                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });
        Rl_MyRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RideListHomePage.class);
                //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Class", "Home");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        Rl_BidFare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BidFarePage.class);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        Rl_MyWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Intent intent = new Intent(getActivity(), WalletMoneyPage1.class);
                    // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

                //    getActivity().finish();

            }
        });
        Rl_Earnings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CloudMoneyHomePage.class);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                //  getActivity().finish();
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        Rl_Mileage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MilesHome.class);
                //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                // getActivity().finish();
            }
        });

        Ll_Sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SosPage.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                //  getActivity().finish();
            }
        });
        Ll_Complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), Complaint_Page_new.class);
                intent.putExtra("frompage", "Homepage");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);


            }
        });
        Ll_Support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallSupport();
            }
        });
        return parentView;
    }

    private void CallSupport() {
        final Dialog dialog = new Dialog(getActivity(), R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.tech_support_popup);
        Button report = (Button) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        ImageView call_image = (ImageView) dialog.findViewById(R.id.image_call);
        ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        call_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getCustomerServiceNo()));
                    startActivity(intent);


                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getCustomerServiceNo()));
                    startActivity(intent);

                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Complaint_Page_new.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void UpdateProfilePopUp() {
        final Dialog dialog = new Dialog(getActivity(), R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup1);
        CustomTextView Tv_Later = (CustomTextView) dialog.findViewById(R.id.later_tv);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_profilechange);
        CheckBox profile_checkbox = (CheckBox) dialog.findViewById(R.id.profile_checkbox);
        String checkBoxStatus = "No";
        if (profile_checkbox.isChecked()) {
            checkBoxStatus = "Yes";
        }
        TextView Profilechange = (TextView) dialog.findViewById(R.id.txt_updaste);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        final String finalCheckBoxStatus = checkBoxStatus;
        Tv_Later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (basicUserProfileUpdateStatus.equalsIgnoreCase("0")) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    UpdateProfilePopUp2(finalCheckBoxStatus);
                } else {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    System.out.println("==============Muruga basicUserProfileUpdateStatus==========" + basicUserProfileUpdateStatus);
                    Intent intent = new Intent(getActivity(), Navigation_new.class);
                    // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewProfile_Page.class);
                intent.putExtra("Register", "1");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void UpdateProfilePopUp2(final String needAssistance) {

        final Dialog dialog = new Dialog(getActivity(),  R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup2);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_tv);

        final CustomEdittext userName = (CustomEdittext) dialog.findViewById(R.id.name_title);
        final CustomEdittext userEmail = (CustomEdittext) dialog.findViewById(R.id.email_title);
        final CustomEdittext user_age_et = (CustomEdittext) dialog.findViewById(R.id.user_age_et);
        final ImageView closeimage = (ImageView) dialog.findViewById(R.id.img_close);
        CustomTextView Profilechange = (CustomTextView) dialog.findViewById(R.id.profile_text1);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        Spinner spinnerCustom = (Spinner) dialog.findViewById(R.id.gender_spinner1);
        final TextView Tv_city = (TextView) dialog.findViewById(R.id.ratecard_city_textview);
        // Spinner Drop down elements
        ArrayList<String> spinnerItems = new ArrayList<>();
        spinnerItems.add(getResources().getString(R.string.seletc_gender));
        spinnerItems.add(getResources().getString(R.string.male));
        spinnerItems.add(getResources().getString(R.string.female));
        spinnerItems.add(getResources().getString(R.string.others));

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(), spinnerItems);
        spinnerCustom.setAdapter(customSpinnerAdapter);
        spinnerCustom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // close keyboard
                return false;
            }
        });
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                cd = new ConnectionDetector(getActivity());
                isInternetPresent = cd.isConnectingToInternet();
                Tv_city.setText(item);
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Tv_city.setText(getResources().getString(R.string.seletc_gender));
            }
        });
        user_age_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    user_age_et.setCursorVisible(false);
                    user_age_et.clearFocus();
                    InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(user_age_et.getWindowToken(), 0);
                }
                return false;
            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userName.getText().toString().length() == 0) {
                    erroredit(userName, getResources().getString(R.string.profile_label_alert_username));
                } else if (!isValidEmail(userEmail.getText().toString().trim().replace(" ", ""))) {
                    erroredit(userEmail, getResources().getString(R.string.profile_label_alert_email));
                } else if (Tv_city.getText().toString().trim().equalsIgnoreCase("SELECT GENDER")) {
                    AlertError(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.profile_label_alert_validgender));
                } else if (user_age_et.getText().toString().length() == 0) {
                    erroredit(user_age_et, getResources().getString(R.string.profile_label_alert_validage));
                } else if (user_age_et.getText().toString().trim().equalsIgnoreCase("0")) {
                    erroredit(user_age_et, getResources().getString(R.string.profile_label_alert_validage));
                } else {
                    if (isInternetPresent) {
                        String username = userName.getText().toString().trim().replace(" ", "");
                        String userEmail1 = userEmail.getText().toString().trim().replace(" ", "");
                        String gender = Tv_city.getText().toString().trim().replace(" ", "");
                        String age = user_age_et.getText().toString().trim().replace(" ", "");
                        postrequestUpdateProfile(Iconstant.get_basic_profile_url, username, userEmail1, gender, needAssistance, age);
                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }
                    dialog.dismiss();
                }
            }
        });
        closeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(user_age_et);
                dialog.dismiss();
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(getActivity(), msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    private void initialize(View rootview) {
        Mcontext = rootview.getContext();
  /*      String countryName = getActivity().getResources().getConfiguration().locale.getDisplayCountry();
        System.out.println("=============Default Country Name "+" "+countryName);*/
        session = new SessionManager(getActivity());
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        homeAnimImage = info.get(SessionManager.KEY_HOMEUSERIMAGE);

        Rl_BookNow = (ImageView) rootview.findViewById(R.id.home_booknow_relative_layout);
        Rl_BidFare = (RelativeLayout) rootview.findViewById(R.id.home_bidefare_relative_layout);
        Rl_MyRide = (RelativeLayout) rootview.findViewById(R.id.home_myride_relative_layout);
        Rl_MyWallet = (RelativeLayout) rootview.findViewById(R.id.home_mywallet_relative_layout);
        Rl_Earnings = (RelativeLayout) rootview.findViewById(R.id.home_earning_relative_layout);
        Rl_Mileage = (RelativeLayout) rootview.findViewById(R.id.home_mileage_relative_layout);
        homeAnimateImage = (ImageView) rootview.findViewById(R.id.home_animate_image);
        spinKitView = (SpinKitView) rootview.findViewById(R.id.spin_kit);
        spin_kit1 = (SpinKitView) rootview.findViewById(R.id.spin_kit1);
        complaint_ticket_count = (TextView) rootview.findViewById(R.id.complaint_ticket_count);

        Ll_Sos = (LinearLayout) rootview.findViewById(R.id.home_sos_layout);
        Ll_Complaint = (RelativeLayout) rootview.findViewById(R.id.home_complaint_layout);
        Ll_Support = (LinearLayout) rootview.findViewById(R.id.home_support_layout);
        account_block_layout = (TextView) rootview.findViewById(R.id.account_block_layout);
        //Rl_BidFare_Amount = (RelativeLayout) rootview.findViewById(R.id.home_bid_fare_amount_layout);
        //Rl_Ride_arrive_Time = (RelativeLayout) rootview.findViewById(R.id.home_my_ride_arrive_time_layout);
        //Rl_Wallet_Amount = (RelativeLayout) rootview.findViewById(R.id.home_wallet_amount_layout);
        //    Tv_BidFare_Amount = (CustomTextView) rootview.findViewById(R.id.home_bid_fare_amount_textview);
        // Tv_Wallet_Amount = (CustomTextView) rootview.findViewById(R.id.home_wallet_amount_textview);
        // Tv_Ride_arrive_Time = (CustomTextView) rootview.findViewById(R.id.home_my_ride_arrive_time_textview);
        Tv_good_mrng = (CustomTextView) rootview.findViewById(R.id.Tv_good_mrng);
        Tv_Mylege = (CustomTextView) rootview.findViewById(mylage);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/digital-7.ttf");
        Tv_Mylege.setTypeface(face,Typeface.BOLD);


        videoView = (VideoView) rootview.findViewById(R.id.videoView);

        try {
            PlayVideos();
        } catch (ProxyCacheException e) {
            e.printStackTrace();
        }

        ImageView Rl_drawer = (ImageView) rootview.findViewById(R.id.drawer_icon);

        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 0 && timeOfDay < 12) {
            Tv_good_mrng.setText(getResources().getString(R.string.good_morning_lable));
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            Tv_good_mrng.setText(getResources().getString(R.string.good_afternoon_lable));
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            Tv_good_mrng.setText(getResources().getString(R.string.good_evening_lable));
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            Tv_good_mrng.setText(getResources().getString(R.string.good_night_lable));
        }
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        getActivity().registerReceiver(refreshReceiver, intentFilter);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isInternetPresent) {
                    PostRequest(Iconstant.get_homepage_url);
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }
            }
        }, 100);

//        MainPage.navigationNotifyChange();

    }

    private void TrackRidePopUp(final String rideId) {
        final Dialog dialog = new Dialog(getActivity(), R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.homepage_track_popup);
        final TextView Ok_Tv = (TextView) dialog.findViewById(R.id.custom_dialog_library_cancel_button);
        final RelativeLayout cancel_payment = (RelativeLayout) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        final ImageView image_close = (ImageView) dialog.findViewById(R.id.image_close);
        final LinearLayout trackLayout = (LinearLayout) dialog.findViewById(R.id.track_layout);
        final CustomTextView onGoingText = (CustomTextView) dialog.findViewById(R.id.custom_dialog_library_message_textview1);


        onGoingText.setText(onGoingTxt);
        if (payment_mode.equalsIgnoreCase("xendit-card") && driverRequestMode.equalsIgnoreCase("completed") && (ride_status.equalsIgnoreCase("completed") || ride_status.equalsIgnoreCase("Finished"))) {
            Ok_Tv.setText(getResources().getString(R.string.my_rides_payment_header_textview));
        }

        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null)
                    dialog.dismiss();
            }
        });
        trackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            /*    if (payment_mode.equalsIgnoreCase("xendit-card") && ride_status.equalsIgnoreCase("Finished")) {
                    Intent i1 = new Intent(getActivity(), XenditRidePaymentActivity.class);
                    i1.putExtra("UserID", UserID);
                    i1.putExtra("RideID", onGoingRideId);
                    i1.putExtra("xendit_key", xendit_key);
                    i1.putExtra("conversionkey", currencyconversion);
                    i1.putExtra("ride_amount", ride_amount);
                    i1.putExtra("currency", currency);
                    startActivity(i1);
                } else*/
//               if (payment_mode.equalsIgnoreCase("xendit-card") && driverRequestMode.equalsIgnoreCase("processing")||ride_status.equalsIgnoreCase("Finished") && (ride_status.equalsIgnoreCase("completed"))) {
////                    Intent intent1 = new Intent(getActivity(), FareBreakUp.class);
////                    intent1.putExtra("RideID", rideId);
////                    intent1.putExtra("ratingflag", "2");
////                    getActivity().startActivity(intent1);
////                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
////                    getActivity().finish();
//                   dialog.dismiss();
//                } else {
                    if (CompleteStage.equalsIgnoreCase("onprocess")) {
                        Intent i = new Intent(getActivity(), WaitingTimePage.class);
                        i.putExtra("ride_id", rideId);
                        i.putExtra("isContinue", true);
                        startActivity(i);
                        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        getActivity().finish();
                        dialog.dismiss();
                        Log.d(TAG, "ChatHandler: "+"waitingtime_home");
                    } else {
                        Intent intent1 = new Intent(getActivity(), TrackRidePage.class);
                        intent1.putExtra("ride_id", rideId);
                        startActivity(intent1);
                        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        getActivity().finish();
                        dialog.dismiss();
                    }
//                }

              /*  if (CompleteStage.equalsIgnoreCase("onprocess")) {
                    Intent i = new Intent(getActivity(), WaitingTimePage.class);
                    i.putExtra("ride_id", rideId);
                    i.putExtra("isContinue", true);
                    startActivity(i);
                    getActivity(). overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    getActivity().  finish();
                    dialog.dismiss();
                } else {
                    Intent intent1 = new Intent(getActivity(), TrackRidePage.class);
                    intent1.putExtra("ride_id", rideId);
                    startActivity(intent1);
                    getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    getActivity().finish();
                    dialog.dismiss();
                }*/

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void RoamingPopUpLogut() {
        final Dialog dialog = new Dialog(getActivity(),  R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.homepage_logout_popup);
        final Button Ok_Tv = (Button) dialog.findViewById(R.id.custom_dialog_library_cancel_button);
        Ok_Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                dialog.dismiss();

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void logout() {
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            postRequest_Logout(Iconstant.logout_url);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }
    }

    private void RoamingPopUpWelcome(final String user_location) {
        final Dialog dialog = new Dialog(getActivity(),  R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.homepage_roaming_popup);
        final TextView custom_TV = (TextView) dialog.findViewById(R.id.custom_dialog_library_message_textview);
        final Button Ok_Tv = (Button) dialog.findViewById(R.id.custom_dialog_library_cancel_button);
        custom_TV.setText(getActivity().getResources().getString(R.string.welcome) + " " + user_location);
        Ok_Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.IsFirstTime(false);
                dialog.dismiss();

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    //--------------Show Dialog Method-----------

    //-----------------------Logout Request-----------------
    private void postRequest_Logout(String Url) {
        spinKitView.setVisibility(View.VISIBLE);
        System.out.println("---------------LogOut Url-----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("device", "ANDROID");

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------------LogOut Response-----------------" + response);
                String Sstatus = "", Sresponse = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                spinKitView.setVisibility(View.GONE);
                if (Sstatus.equalsIgnoreCase("1")) {
                    session.logoutUser();
                    getActivity().stopService(new Intent(getActivity(), XmppService.class));
                    Intent intent = new Intent(getActivity(), SplashPage1.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    getActivity().finish();
                } else {
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }
            }

            @Override
            public void onErrorListener() {
                spinKitView.setVisibility(View.GONE);
            }
        });
    }

    private void PlayVideos() throws ProxyCacheException {
        spin_kit1.setVisibility(View.VISIBLE);

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) videoView.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        videoView.setLayoutParams(params);

        videoView.setOnInfoListener(onInfoToPlayStateListener);

        /*Cache cache = new FileCache(new File(getActivity().getExternalCacheDir(), "ANIMATE_VIDEO"));
        HttpUrlSource source = new HttpUrlSource("http://ak4.picdn.net/shutterstock/videos/2671694/preview/stock-footage-driving-behind-formula-one-race-car-on-desert-circuit-high-quality-d-animation-visit-our-portfo.mp4");
        proxyCache = new HttpProxyCache(source, cache);
           videoView.setVideoPath(proxyCache.getUrl());*/


        String uriPath = "android.resource://com.blackmaria/" + R.raw.homevideo;
        Uri uri = Uri.parse(uriPath);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        MediaController mediaController = null;
        mediaController = new MediaController(getActivity());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
        if (mediaController != null) {
            mediaController.setAnchorView(videoView);
        }
        mediaController.setVisibility(View.GONE);
        videoView.setMediaController(mediaController);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                spin_kit1.setVisibility(View.GONE);
                mp.setLooping(true);
            }
        });
        videoView.start();

    }

    private final MediaPlayer.OnInfoListener onInfoToPlayStateListener = new MediaPlayer.OnInfoListener() {

        @Override
        public boolean onInfo(MediaPlayer mp, int videoReqCode, int extra) {
            if (MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START == videoReqCode) {
                spin_kit1.setVisibility(View.GONE);
            }
            if (MediaPlayer.MEDIA_INFO_BUFFERING_START == videoReqCode) {
                spin_kit1.setVisibility(View.VISIBLE);
            }
            if (MediaPlayer.MEDIA_INFO_BUFFERING_END == videoReqCode) {
                spin_kit1.setVisibility(View.GONE);
            }
            return false;
        }
    };

    private void postrequestUpdateProfile(String Url, String name, String email, String gender, String needAssistance, String Age) {

        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("user_name", name);
        jsonParams.put("email", email);
        jsonParams.put("gender", gender);
        jsonParams.put("age", Age);
        jsonParams.put("assistance", needAssistance);

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");
                        Alert1(getActivity().getResources().getString(R.string.action_success), Smessage, Sstatus);
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Smessage = object.getString("response");
                        Alert1(getActivity().getResources().getString(R.string.action_error), Smessage, Sstatus);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void PostRequest(String Url) {

        dialog = new Dialog(Mcontext);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
       // spinKitView.setVisibility(View.VISIBLE);
        System.out.println("-----------HomePage url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        System.out.println("-----------HomePage jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------HomePage reponse-------------------" + response);

                String user_location = "", info_badge = "", referalStatus = "", onGoingRide = "", roaming_status = "", roaming_mode = "", Sstatus = "", totalTicketCount = "", dashboard_image = "", Smessage = "", Swallet_amount = "", Sbid_fare = "", Sride_min = "", ScurrencySymbol = "", Sprofile_complete = "", totalDistance, closedTicketCount = "", openTicketCount = "", userActive = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        if (object.has("comp_stage")) {
                            CompleteStage = object.getString("comp_stage");
                            completeStatus = object.getString("comp_status");
                            session.setComplementStatus(object.getString("comp_stage"));
                        }

                        onGoingRide = object.getString("on_goingride");
                        onGoingRideId = object.getString("on_goingrideid");
                        onGoingTxt = object.getString("on_going_txt");


                        totalTicketCount = object.getString("total_ticket_Count");
                        closedTicketCount = object.getString("closed_ticket_Count");
                        openTicketCount = object.getString("open_ticket_Count");
                        userActive = object.getString("user_active");
                        user_location = object.getString("user_location");
                        referalStatus = object.getString("referral_staus");
                        roaming_mode = object.getString("roaming_mode");
                        roaming_status = object.getString("roaming_status");
                        info_badge = object.getString("info_badge");
                        Swallet_amount = object.getString("wallet_amount");
                        Sbid_fare = object.getString("bid_fare");
                        Sride_min = object.getString("ride_min");
                        currency = CurrencySymbolConverter.getCurrencySymbol(object.getString("currencyCode"));
                        Sprofile_complete = object.getString("profile_complete");
                        if (object.has("driver_request_mode")) {
                            driverRequestMode = object.getString("driver_request_mode");
                        }
                        xendit_key = object.getString("xendit_public_key");
                        if (object.has("exchange_value")) {
                            currencyconversion = object.getString("exchange_value");
                            session.setcurrencyconversionKey(currencyconversion);
                        }
                        ride_amount = object.getString("grand_fare");
                        payment_mode = object.getString("payment_mode");
                        ride_status = object.getString("ride_status");
                        Ssupport_no = object.getString("support_no");
                        UserProfileUpdateStatus = object.getString("profile_complete");
                        basicUserProfileUpdateStatus = object.getString("basic_complete");
                        session.createWalletAmount(currency + " " + Swallet_amount);
                        session.setXenditPublicKey(xendit_key);
                        session.setUserBasicProfile(basicUserProfileUpdateStatus);
                        session.setUserCompleteProfile(UserProfileUpdateStatus);
                        totalDistance = object.getString("tot_distance") + " " + object.getString("distance_unit");
                        session.setReferalStatus(referalStatus);
                        if (object.has("dashboard_image")) {
                            dashboard_image = object.getString("dashboard_image");
                            if (homeAnimImage.equalsIgnoreCase("") && homeAnimImage.equalsIgnoreCase(null)) {
                                session.setUserHomeAnimaionImageUpdate(dashboard_image);
                            }
                        }
                        Tv_Mylege.setText(totalDistance);
                       // spinKitView.setVisibility(View.GONE);
                        if( dialog!=null){
                            dialog.dismiss();
                        }
                        if (!homeAnimImage.equalsIgnoreCase("") && !homeAnimImage.equalsIgnoreCase(null)) {
                            String Imsge = homeAnimImage.substring(homeAnimImage.lastIndexOf("."));
                            if (Imsge.equalsIgnoreCase(".gif")) {
                                System.out.println("=====================homeAnimImage gif==============" + Imsge + " " + homeAnimImage);
                               /* GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(homeAnimateImage);
                                Glide.with(HomePage.this).load(homeAnimImage).into(imageViewTarget);*/
                                Glide.with(HomePage.this)
                                        .load(homeAnimImage)
                                        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                                        .into(new SimpleTarget<Drawable>() {
                                            @Override
                                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                                homeAnimateImage.setImageDrawable(resource);
                                            }
                                        });
                            } else {
                                System.out.println("=====================homeAnimImage==============" + homeAnimImage);
                                Picasso.with(getActivity()).load(String.valueOf(homeAnimImage)).into(homeAnimateImage);
                            }
                        } else {
                            if (!dashboard_image.equalsIgnoreCase("")) {
                                String Imsge = dashboard_image.substring(dashboard_image.lastIndexOf("."));
                                if (Imsge.equalsIgnoreCase(".gif")) {
                                    System.out.println("=====================dashboard_image gif==============" + Imsge + " " + dashboard_image);
                                   /* GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(homeAnimateImage);
                                    Glide.with(HomePage.this).load(dashboard_image).into(imageViewTarget);*/
                                    Glide.with(HomePage.this)
                                            .load(dashboard_image)
                                            .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                                            .into(new SimpleTarget<Drawable>() {
                                                @Override
                                                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                                    homeAnimateImage.setImageDrawable(resource);
                                                }
                                            });
                                } else {
                                    System.out.println("=====================dashboard_image==============" + dashboard_image);
                                    Picasso.with(getActivity()).load(String.valueOf(dashboard_image)).into(homeAnimateImage);
                                }
                            }
                        }
                    } else {
                        Smessage = object.getString("response");
                        //spinKitView.setVisibility(View.GONE);
                        if( dialog!=null){
                            dialog.dismiss();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                  //  spinKitView.setVisibility(View.GONE);
                    if( dialog!=null){
                        dialog.dismiss();
                    }
                    //dialogDismiss();
                }
              //  spinKitView.setVisibility(View.GONE);
                if( dialog!=null){
                    dialog.dismiss();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    try {
                        session.setInfoBadge(info_badge);

                        isFirstime = session.getIsFirstTime();
                        if (roaming_mode.equalsIgnoreCase("deactivated") && roaming_status.equalsIgnoreCase("1")) {
                            RoamingPopUpLogut();
                        } else if (roaming_mode.equalsIgnoreCase("activated") && roaming_status.equalsIgnoreCase("1") && isFirstime) {
                            RoamingPopUpWelcome(user_location);
                        }
                        if (!openTicketCount.equalsIgnoreCase("0")) {
                            complaint_ticket_count.setText(openTicketCount);
                            complaint_ticket_count.setVisibility(View.VISIBLE);
                        } else {
                            complaint_ticket_count.setVisibility(View.GONE);
                        }
                        if (!onGoingRide.equalsIgnoreCase("0")) {
                            TrackRidePopUp(onGoingRideId);
                        }
                        if (userActive.equalsIgnoreCase("0")) {
                            account_block_layout.setVisibility(View.VISIBLE);
                        } else {
                            account_block_layout.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
               // spinKitView.setVisibility(View.GONE);
                if( dialog!=null){
                    dialog.dismiss();
                }
                //dialogDismiss();
            }
        });

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void AlertExit(String title, String alert) {

        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                getActivity().finish();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void Alert1(String title, String alert, final String status) {

        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);

        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")) {
                    Intent intent = new Intent(getActivity(), Navigation_new.class);
                    // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
//                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                    mDialog.dismiss();
                } else {
                    mDialog.dismiss();
                }

            }
        });
        mDialog.show();
    }

   /* private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }*/

    //----------------------------Run time permission for call--------------------------------------------

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getCustomerServiceNo()));
                    startActivity(intent);

                }
                break;


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume called");
        videoView.seekTo(stopPosition);
        videoView.start(); //Or use resume() if it doesn't work. I'm not sure

    }

    @Override
    public void onPause() {
        super.onPause();
        /*if(dialog!=null){
            dialog.dismiss();
        }*/
        if (spinKitView.getVisibility() == View.VISIBLE) {
            spinKitView.setVisibility(View.GONE);
        }
        stopPosition = videoView.getCurrentPosition(); //stopPosition is an int
        videoView.pause();
        //dialogDismiss();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:

                AlertExit(getResources().getString(R.string.alert), getResources().getString(R.string.exit_alert));

                return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (spinKitView.getVisibility() == View.VISIBLE) {
            spinKitView.setVisibility(View.GONE);
        }

        if (proxyCache != null) {
            proxyCache.shutdown();
        }
        //dialogDismiss();
        if (updateReceiver != null) {
            getActivity().unregisterReceiver(updateReceiver);
            updateReceiver = null;
        }
        if (updateReceiver1 != null) {
            getActivity().unregisterReceiver(updateReceiver1);
            updateReceiver1 = null;
        }
        getActivity().unregisterReceiver(refreshReceiver);
    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getActivity().getCurrentFocus()) ? null : getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
