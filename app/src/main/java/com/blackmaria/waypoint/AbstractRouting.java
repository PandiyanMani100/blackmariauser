package com.blackmaria.waypoint;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by user144 on 9/5/2017.
 */

public abstract class AbstractRouting extends AsyncTask<Void, Void, ArrayList<Route>> {
    protected ArrayList<RoutingListener> _aListeners = new ArrayList();
    protected static final String DIRECTIONS_API_URL = "https://maps.googleapis.com/maps/api/directions/json?";
    private RouteException mException = null;

    protected AbstractRouting(RoutingListener listener) {
        this.registerListener(listener);
    }

    public void registerListener(RoutingListener mListener) {
        if (mListener != null) {
            this._aListeners.add(mListener);
        }

    }

    protected void dispatchOnStart() {
        Iterator i$ = this._aListeners.iterator();

        while (i$.hasNext()) {
            RoutingListener mListener = (RoutingListener) i$.next();
            mListener.onRoutingStart();
        }

    }

    protected void dispatchOnFailure(com.blackmaria.waypoint.RouteException exception) {
        Iterator i$ = this._aListeners.iterator();

        while (i$.hasNext()) {
            RoutingListener mListener = (RoutingListener) i$.next();
            mListener.onRoutingFailure(exception);
        }

    }

    protected void dispatchOnSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        Iterator i$ = this._aListeners.iterator();

        while (i$.hasNext()) {
            RoutingListener mListener = (RoutingListener) i$.next();
            mListener.onRoutingSuccess(route, shortestRouteIndex);
        }

    }


    protected void dispatchOnSuccess1(ArrayList<LatLng> route) {
        Iterator i$ = this._aListeners.iterator();
        while (i$.hasNext()) {
            RoutingListener mListener = (RoutingListener) i$.next();
            mListener.onPostExecute(route);
        }

    }


    private void dispatchOnCancelled() {
        Iterator i$ = this._aListeners.iterator();

        while (i$.hasNext()) {
            RoutingListener mListener = (RoutingListener) i$.next();
            mListener.onRoutingCancelled();
        }

    }


    protected ArrayList<Route> doInBackground(Void... voids) {
        ArrayList result = new ArrayList();

        try {
            System.out.println("====URL==========="+voids.toString());
            result = (new GoogleParser(this.constructURL())).parse();
        } catch (RouteException var4) {
            this.mException = var4;
        }

        return result;
    }

    protected abstract String constructURL();

    protected void onPreExecute() {
        this.dispatchOnStart();
    }

    protected void onPostExecute(ArrayList<Route> result) {
        ArrayList<LatLng> latLan = new ArrayList<>();
        if (!result.isEmpty()) {
            int shortestRouteIndex = 0;
            int minDistance = 2147483647;

            for (int i = 0; i < result.size(); ++i) {
                PolylineOptions mOptions = new PolylineOptions();
                Route route = (Route) result.get(i);
                if (route.getLength() < minDistance) {
                    shortestRouteIndex = i;
                    minDistance = route.getLength();
                }
                Iterator i$ = route.getPoints().iterator();
                while (i$.hasNext()) {
                    LatLng point = (LatLng) i$.next();
                    latLan.add(point);
                    mOptions.add(point);
                }
                ((Route) result.get(i)).setPolyOptions(mOptions);
            }
            this.dispatchOnSuccess1(latLan);
            System.out.println("======================Muruga animation latlang=============" + latLan);
            this.dispatchOnSuccess(result, shortestRouteIndex);
        } else {
            this.dispatchOnFailure(this.mException);
        }

    }

    protected void onCancelled() {
        this.dispatchOnCancelled();
    }

    public static enum AvoidKind {
        TOLLS(1, "tolls"),
        HIGHWAYS(2, "highways"),
        FERRIES(4, "ferries");

        private final String _sRequestParam;
        private final int _sBitValue;

        private AvoidKind(int bit, String param) {
            this._sBitValue = bit;
            this._sRequestParam = param;
        }

        protected int getBitValue() {
            return this._sBitValue;
        }

        protected static String getRequestParam(int bit) {
            String ret = "";
            AbstractRouting.AvoidKind[] arr$ = values();
            int len$ = arr$.length;

            for (int i$ = 0; i$ < len$; ++i$) {
                AbstractRouting.AvoidKind kind = arr$[i$];
                if ((bit & kind._sBitValue) == kind._sBitValue) {
                    ret = ret + kind._sRequestParam;
                    ret = ret + "|";
                }
            }

            return ret;
        }
    }

    public static enum TravelMode {
        BIKING("bicycling"),
        DRIVING("driving"),
        WALKING("walking"),
        TRANSIT("transit");

        protected String _sValue;

        private TravelMode(String sValue) {
            this._sValue = sValue;
        }

        protected String getValue() {
            return this._sValue;
        }
    }
}

