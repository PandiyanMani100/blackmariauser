package com.blackmaria.waypoint;

import com.directions.route.RouteException;

import java.util.ArrayList;

/**
 * Created by user144 on 9/5/2017.
 */

public interface Parser {
    ArrayList<com.blackmaria.waypoint.Route> parse() throws RouteException, com.blackmaria.waypoint.RouteException;
}

