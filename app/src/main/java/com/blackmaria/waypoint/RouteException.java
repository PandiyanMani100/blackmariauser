package com.blackmaria.waypoint;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user144 on 9/5/2017.
 */

public class RouteException extends Exception {
    private static final String TAG = "RouteException";
    private final String KEY_STATUS = "status";
    private final String KEY_MESSAGE = "error_message";
    private String statusCode;
    private String message;

    public RouteException(JSONObject json) {
        if(json == null) {
            this.statusCode = "";
            this.message = "Parsing error";
        } else {
            try {
                this.statusCode = json.getString("status");
                this.message = json.getString("error_message");
            } catch (JSONException var3) {
                Log.e("RouteException", "JSONException while parsing RouteException argument. Msg: " + var3.getMessage());
            }

        }
    }

    public RouteException(String msg) {
        this.message = msg;
    }

    public String getMessage() {
        return this.message;
    }

    public String getStatusCode() {
        return this.statusCode;
    }
}
