package com.blackmaria.waypoint;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by user144 on 9/5/2017.
 */

public class XMLParser {
    protected static final String MARKERS = "markers";
    protected static final String MARKER = "marker";
    protected URL feedUrl;

    protected XMLParser(String feedUrl) {
        try {
            this.feedUrl = new URL(feedUrl);
        } catch (MalformedURLException var3) {
            Log.e("Routing Error", var3.getMessage());
        }

    }

    protected InputStream getInputStream() {
        try {
            return this.feedUrl.openConnection().getInputStream();
        } catch (IOException var2) {
            Log.e("Routing Error", var2.getMessage());
            return null;
        }
    }
}
