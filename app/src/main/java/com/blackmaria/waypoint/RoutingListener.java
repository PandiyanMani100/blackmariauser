package com.blackmaria.waypoint;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by user144 on 9/5/2017.
 */

public interface RoutingListener {
    void onRoutingFailure(RouteException var1) ;

    void onRoutingStart() ;

    void onRoutingSuccess(ArrayList<Route> var1, int var2) ;

    void onRoutingCancelled() ;
    void onPostExecute(ArrayList<LatLng> result);


}
