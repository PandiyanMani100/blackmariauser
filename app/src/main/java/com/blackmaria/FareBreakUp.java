package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.FareBreakupListAdapter;
import com.blackmaria.pojo.FareBreakupPojo;
import com.blackmaria.subclass.ActivitySubClass;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.Saveridedata_to_localdb;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by user14 on 12/22/2016.
 */

public class FareBreakUp extends ActivitySubClass {
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "", Str_ride_id = "", ratingflag = "2", cashbackStatus = "", Username = "", UserNo = "";
    AppInfoSessionManager SappInfo_Session;
    private Dialog dialog;
    private ServiceRequest mRequest;
//    private ImageView paynment_image;
    LanguageDb mhelper;

    private TextView Tv_date_time, crn_value, total_fare_tv,/* Tv_makePayment,*/
            farebreakup_header_Tv;
    private TextView Tv_tc_email_invoice;
    private TextView Tv_farebreakup_header_textview1, username, userphoneno, Rl_cashback, gotcashback;
    private ExpandableHeightListView Lv_faredetail;
    TextView farebreakup_header_textview;
    private boolean isDataAvailable = false;
    private ArrayList<FareBreakupPojo> fareList;
    private String sFareStatus = "";
    private String ScurrencySymbol = "";
    private FareBreakupListAdapter fareBreakupListAdapter;
    public static Activity farebreakup_class;
    private String sTotalPayment = "", sTripDate = "", striptime = "";

    private MaterialDialog invoice_dialog;
    private EditText Et_dialog_InvoiceEmail;

    RelativeLayout ridecompleted_layout, payment_paid_layout;
    private LinearLayout Rl_rate_user, Rl_report;
    private RefreshReceiver refreshReceiver;
    Typeface face, tf;
    private ImageView ivclose;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {


            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                try {
                   /* cashbackStatus = session.getCashbackStatus();
                    if (!cashbackStatus.equalsIgnoreCase("OFF")) {
                        Rl_rider_profile.setVisibility(View.GONE);
                    } else {
                        Rl_rider_profile.setVisibility(View.VISIBLE);
                    }*/
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("=============Muruga  cashbackStatus error==============" + e.toString());
                }

                System.out.println("--------------------------------------niramjan------------------imitialize");

                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
/*
                    ratingflag= (String) intent.getExtras().get("ratingflag");
*/

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(FareBreakUp.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {

                        System.out.println("--------------------------------------niramjan------------------payment paid");

                        Tv_tc_email_invoice.setVisibility(View.VISIBLE);
                        Tv_farebreakup_header_textview1.setVisibility(View.GONE);
                        ridecompleted_layout.setVisibility(View.GONE);
                        payment_paid_layout.setVisibility(View.VISIBLE);
                        if (cashbackStatus.equalsIgnoreCase("OFF")) {
                            Rl_cashback.setVisibility(View.GONE);
                            gotcashback.setVisibility(View.GONE);
                        } else {
                            Rl_cashback.setVisibility(View.VISIBLE);
                            gotcashback.setVisibility(View.VISIBLE);
                        }
                       /* Intent intent1 = new Intent(FareBreakUp.this, FareBreakUp.class);
                        intent1.putExtra("RideID", Str_ride_id);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {

                        ridecompleted_layout.setVisibility(View.VISIBLE);
                        payment_paid_layout.setVisibility(View.GONE);


                        Tv_tc_email_invoice.setVisibility(View.GONE);
                        Tv_farebreakup_header_textview1.setVisibility(View.VISIBLE);


                        System.out.println("--------------------------------------niramjan------------------asfasfsda");
                        Intent intent1 = new Intent(FareBreakUp.this, FareBreakUp.class);
                        intent1.putExtra("RideID", Str_ride_id);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.farebreakup_layout);
        mhelper= new LanguageDb(this);
        farebreakup_class = FareBreakUp.this;
        initialize();

        Tv_tc_email_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mailInvoice();
            }
        });
//        Tv_makePayment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cd = new ConnectionDetector(FareBreakUp.this);
//                isInternetPresent = cd.isConnectingToInternet();
//                if (isInternetPresent) {
//                    Intent passIntent = new Intent(FareBreakUp.this, FareBreakUpPaymentList.class);
//                    passIntent.putExtra("RideID", Str_ride_id);
//                    passIntent.putExtra("totalAmount", sTotalPayment);
//                    startActivity(passIntent);
//                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                } else
//
//            }
//        });
        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(FareBreakUp.this, MyRideRating.class);
                intent1.putExtra("RideID", Str_ride_id);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |/* Intent.FLAG_ACTIVITY_NO_HISTORY | */Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                overridePendingTransition(R.anim.enter, R.anim.exit);

//                Intent intent = new Intent(FareBreakUp.this, Navigation_new.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.exit, R.anim.enter);

            }
        });
//        Rl_rate_user.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent1 = new Intent(FareBreakUp.this, MyRideRating.class);
//                intent1.putExtra("RideID", Str_ride_id);
//                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |/* Intent.FLAG_ACTIVITY_NO_HISTORY | */Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent1);
//                overridePendingTransition(R.anim.enter, R.anim.exit);
//                // finish();
//            }
//        });

        Rl_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });
        Rl_cashback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cahbackIntent = new Intent(FareBreakUp.this, CashBackFare.class);
                cahbackIntent.putExtra("RideID", Str_ride_id);
                cahbackIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                cahbackIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(cahbackIntent);
                //  finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

    }

    private void initialize() {

//        paynment_image = (ImageView) findViewById(R.id.paynment_image);
        session = new SessionManager(FareBreakUp.this);
        cd = new ConnectionDetector(FareBreakUp.this);
        isInternetPresent = cd.isConnectingToInternet();
        fareList = new ArrayList<FareBreakupPojo>();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        Username = user.get(SessionManager.KEY_USERNAME);
        UserNo = user.get(SessionManager.KEY_PHONENO);
        cashbackStatus = session.getCashbackStatus();
        Lv_faredetail = (ExpandableHeightListView) findViewById(R.id.farebeakup_listview);
        Tv_date_time = findViewById(R.id.farebeakup_date_time_textview);
//        Tv_makePayment =  findViewById(R.id.farebeakup_finish_textview);
        Tv_tc_email_invoice = findViewById(R.id.tc_email_invoice);
        Tv_tc_email_invoice.setText(getkey("trip_email_invoice_receipt_thankyou"));
        farebreakup_header_Tv = findViewById(R.id.farebreakup_header_textview_ridecompletd);
        farebreakup_header_Tv.setText(getkey("trip_completed"));
        Tv_farebreakup_header_textview1 = findViewById(R.id.farebreakup_header_textview1);
        username = findViewById(R.id.username);
        userphoneno = findViewById(R.id.userphoneno);
        Rl_cashback = findViewById(R.id.Rl_cashback);
        Rl_cashback.setText(getkey("acccept_now"));
        gotcashback = findViewById(R.id.gotcashback);
        gotcashback.setText(getkey("you_ve_got_cashback"));
        username.setText(Username);
        userphoneno.setText(UserNo);

        farebreakup_header_textview = findViewById(R.id.farebreakup_header_textview);
        farebreakup_header_textview.setText(getkey("thankyou_lable_new"));

        TextView rec = findViewById(R.id.rec);
        rec.setText(getkey("receipt_lable"));

        TextView totalfare = findViewById(R.id.totalfare);
        totalfare.setText(getkey("totoal_fare"));

        TextView waitinf = findViewById(R.id.waitinf);
        waitinf.setText(getkey("paymentreq_receipt"));


        TextView commpla = findViewById(R.id.commpla);
        commpla.setText(getkey("compliant"));


        total_fare_tv = findViewById(R.id.total_fare_tv);
        crn_value = findViewById(R.id.crn_value);
        ivclose = findViewById(R.id.ivclose);
//        Rl_rate_user = findViewById(R.id.Rl_rate_user);
        Rl_report = findViewById(R.id.tc_complaint_invoice);
//        Rl_rider_profile = findViewById(R.id.Rl_rider_profile);

        ridecompleted_layout = findViewById(R.id.ridecompleted_layout);
        payment_paid_layout = findViewById(R.id.payment_paid_layout);
        Lv_faredetail.setExpanded(true);

        face = Typeface.createFromAsset(getAssets(), "fonts/vladimir.ttf");
        farebreakup_header_textview.setTypeface(face, Typeface.BOLD);
        farebreakup_header_Tv.setTypeface(face);

        tf = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
        total_fare_tv.setTypeface(tf, Typeface.BOLD);
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);


        System.out.println("--------------------------------------niramjan------------------intent");
        Intent intent = getIntent();
        if (cashbackStatus.equalsIgnoreCase("OFF")) {
            Rl_cashback.setVisibility(View.GONE);
            gotcashback.setVisibility(View.GONE);
        } else {
            Rl_cashback.setVisibility(View.VISIBLE);
            gotcashback.setVisibility(View.VISIBLE);
        }
        if (intent.hasExtra("RideID")) {
            Str_ride_id = intent.getStringExtra("RideID");
            session.setFarebreakup_rideId(Str_ride_id);
            crn_value.setText(getkey("bookno") + Str_ride_id);
        } else {
            Str_ride_id = session.getFarebreakup_rideId();
        }
        if (intent.hasExtra("ratingflag")) {

            System.out.println("--------------------------------------niramjan--------------" + ratingflag);

            ratingflag = intent.getStringExtra("ratingflag");
            if (ratingflag.equalsIgnoreCase("1")) {
                System.out.println("--------------------------------------niramjan---ssdfsd-----------" + ratingflag);
                ridecompleted_layout.setVisibility(View.GONE);
                payment_paid_layout.setVisibility(View.VISIBLE);
                Tv_tc_email_invoice.setVisibility(View.VISIBLE);
                Tv_farebreakup_header_textview1.setVisibility(View.GONE);
            } else {
                System.out.println("--------------------------------------niramjan--fsdfsdf------------" + ratingflag);
                ridecompleted_layout.setVisibility(View.VISIBLE);
                payment_paid_layout.setVisibility(View.GONE);
                Tv_tc_email_invoice.setVisibility(View.GONE);
                Tv_farebreakup_header_textview1.setVisibility(View.VISIBLE);
            }
        }
        if (isInternetPresent) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("ride_id", Str_ride_id);
            jsonParams.put("user_id", UserID);
            postRequest_FareBreakUp(Iconstant.getfareBreakUpURL, jsonParams);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }

    }



    //----------Method to Send Email--------
    protected void sendEmail() {


        Intent intent = new Intent(FareBreakUp.this, Complaint_Page_new.class);
        intent.putExtra("mo","1");
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }
    //---------------------------postRequest Fare breakup---------------------------

    private void postRequest_FareBreakUp(String Url, final HashMap<String, String> jsonParams) {
        dialog = new Dialog(FareBreakUp.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-------------Fare breakup Url----------------" + Url);
        System.out.println("-------------Fare breakup jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(FareBreakUp.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                // here we have to store the ride data in local db

                System.out.println("-------------Fare breakup Response----------------" + response);


                String Sstatus = "", currency = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {



                        JSONObject response_object = object.getJSONObject("response");
                        currency = response_object.getString("currency");
                        if (response_object.length() > 0) {
                            ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(response_object.getString("currency"));

                            Object check_fare_object = response_object.get("fare");
                            if (check_fare_object instanceof JSONObject) {
                                JSONObject fare_object = response_object.getJSONObject("fare");
                                String cash_orxendit = fare_object.getString("payment_type");
                                String need_payment = fare_object.getString("need_payment");
                                if(cash_orxendit.equals("xendit-card") && need_payment.equalsIgnoreCase("YES"))
                                {
                                    Intent intent = new Intent(getApplicationContext(), Navigation_new.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                }
                                else
                                {
                                    total_fare_tv.setText(currency + " " + fare_object.getString("total"));
                                    if (fare_object.length() > 0) {

                                        sTotalPayment = fare_object.getString("payment_type");
                                        sTripDate = fare_object.getString("trip_date");
                                        if(fare_object.has("trip_time")){
                                            striptime = fare_object.getString("trip_time");
                                        }
                                        if (fare_object.has("cashback_mode")) {
                                            System.out.println("+++++++++++++cashback_mode++++++++++++" + fare_object.getString("cashback_mode"));
                                            session.setCashbackStatus(fare_object.getString("cashback_mode"));
                                        }
                                        Object check_fare_arr_object = fare_object.get("fare_arr");
                                        if (check_fare_arr_object instanceof JSONArray) {

                                            JSONArray fare_arr_array = fare_object.getJSONArray("fare_arr");
                                            if (fare_arr_array.length() > 0) {
                                                fareList.clear();
                                                for (int j = 0; j < fare_arr_array.length(); j++) {

                                                    JSONObject fare_arr_object = fare_arr_array.getJSONObject(j);
                                                    FareBreakupPojo pojo = new FareBreakupPojo();

                                                    if (fare_arr_array.length() - 1 == j) {
                                                        if (j != 0) {
                                                            pojo.setTitle("\n" + fare_arr_object.getString("title"));
                                                            if ("1".equalsIgnoreCase(fare_arr_object.getString("currency_status"))) {
                                                                pojo.setValue("\n" + ScurrencySymbol + " " + fare_arr_object.getString("value"));
                                                            } else {
                                                                pojo.setValue("\n" + fare_arr_object.getString("value"));
                                                            }
                                                        } else {
                                                            pojo.setTitle(fare_arr_object.getString("title"));
                                                            if ("1".equalsIgnoreCase(fare_arr_object.getString("currency_status"))) {
                                                                pojo.setValue(ScurrencySymbol + " " + fare_arr_object.getString("value"));
                                                            } else {
                                                                pojo.setValue(fare_arr_object.getString("value"));
                                                            }
                                                        }
                                                    } else {
                                                        pojo.setTitle(fare_arr_object.getString("title"));
                                                        if ("1".equalsIgnoreCase(fare_arr_object.getString("currency_status"))) {
                                                            pojo.setValue(ScurrencySymbol + " " + fare_arr_object.getString("value"));
                                                        } else {
                                                            pojo.setValue(fare_arr_object.getString("value"));
                                                        }
                                                    }

                                                    fareList.add(pojo);
                                                }
                                                sFareStatus = "1";
                                            } else {
                                                sFareStatus = "0";
                                            }
                                        }
                                    }
                                }


                            }
                            isDataAvailable = true;

                        } else {
                            isDataAvailable = false;
                        }
                    } else {
                        isDataAvailable = false;
                    }


                    if (Sstatus.equalsIgnoreCase("1")) {

                        Tv_date_time.setText(getkey("date_lable") + " " + sTripDate + "\n" + striptime);
                        fareBreakupListAdapter = new FareBreakupListAdapter(FareBreakUp.this, fareList);
                        Lv_faredetail.setAdapter(fareBreakupListAdapter);
                        Lv_faredetail.setExpanded(true);

                        try {
                            new Saveridedata_to_localdb(FareBreakUp.this, Str_ride_id);
                        }catch (Exception e){}
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(FareBreakUp.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }
    private void hideSoftKeyBoardOnTabClicked(View v) {
        if (v != null && getApplicationContext() != null) {
            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
    //----------Method for Invoice Email--------
    private void mailInvoice() {
        final Dialog dialog = new Dialog(FareBreakUp.this, R.style.DialogSlideAnim2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.mail_invoice_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button sendBtn;
        ImageView cancelBtn;
        Et_dialog_InvoiceEmail = (EditText) dialog.findViewById(R.id.mail_invoice_email_edittext);
        Et_dialog_InvoiceEmail.addTextChangedListener(mailInvoice_EditorWatcher);
        cancelBtn = (ImageView) dialog.findViewById(R.id.cancel_btn);

        TextView mail_invoice_title = (TextView) dialog.findViewById(R.id.mail_invoice_title);
        mail_invoice_title.setText(getkey("insert_valid_email_address_for_receipt"));

        TextView text_mail = (TextView) dialog.findViewById(R.id.text_mail);
        text_mail.setText(getkey("fare_lable1"));

        sendBtn = (Button) dialog.findViewById(R.id.send_btn);
        sendBtn.setText(getkey("confirm_lable"));
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Check if no view has focus:
                hideSoftKeyBoardOnTabClicked(v);
                cd = new ConnectionDetector(FareBreakUp.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (!isValidEmail(Et_dialog_InvoiceEmail.getText().toString())) {
                    erroredit(Et_dialog_InvoiceEmail, getkey("register_label_alert_email"));
                } else {
                    if (isInternetPresent) {
                        postRequest_EmailInvoice(Iconstant.myride_details_inVoiceEmail_url, Et_dialog_InvoiceEmail.getText().toString(), Str_ride_id);
                    } else {
                        Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                    }
                    dialog.dismiss();
                }
            }
        });


        Et_dialog_InvoiceEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(Et_dialog_InvoiceEmail.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return false;
            }
        });

        dialog.show();

    }


    //-----------------------MyRide Email Invoice Post Request-----------------
    private void postRequest_EmailInvoice(String Url, final String Semail, final String SrideId) {
        dialog = new Dialog(FareBreakUp.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_sending_invoice"));

        System.out.println("-------------MyRide Email Invoice Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", Semail);
        jsonParams.put("ride_id", SrideId);
        System.out.println("-------------MyRide Email Invoice jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(FareBreakUp.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------MyRide Email Invoice response----------------" + response);

                String Sstatus = "", Sresponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        Alert(getkey("action_success"), Sresponse);
                    } else {
                        Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //----------------------Code for TextWatcher-------------------------
    private final TextWatcher mailInvoice_EditorWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            //clear error symbol after entering text
            if (Et_dialog_InvoiceEmail.getText().length() > 0) {
                Et_dialog_InvoiceEmail.setError(null);
            }
        }
    };

    //code to Check Email Validation
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(FareBreakUp.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("==============farebreakup finishd========");
        unregisterReceiver(refreshReceiver);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
