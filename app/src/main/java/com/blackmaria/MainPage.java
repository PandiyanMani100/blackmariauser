package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.FragmentHockeyApp;
import com.blackmaria.newdesigns.Homepage_fragment;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Prem  Kumar on 7/27/2016.
 */
public class MainPage extends FragmentHockeyApp implements View.OnClickListener {


    private static View parentView;
    private Dialog dialog;
    private Dialog photo_dialog;
    private LinearLayout about_layout, terms_layout, faq_layout;
    private RelativeLayout info_layout;
    private Boolean isInternetPresent = false;
    final int PERMISSION_REQUEST_CODE = 111;
    private static String infoBadgeCount = "";
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    private ImageView Iv_home, Iv_earnings, Iv_rating, Iv_account, Rl_drawer;
    private TextView Tv_home, Tv_earnings, Tv_rating, Tv_account;
    private static SessionManager session;
    private TextView Tv_HeaderText;
    private static TextView infoBadge;
    private RelativeLayout Rl_Home;
    private boolean isHomePage = false;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.mainpage_layout, container, false);


        initialize(parentView);

        postData();

        if (savedInstanceState == null) {
//            Iv_home.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.home_select));
//            Tv_home.setTextColor(getActivity().getResources().getColor(R.color.bottom_navigation_item_color));
            FragmentTransaction tx = getFragmentManager().beginTransaction();
            tx.replace(R.id.content_frame1, new Homepage_fragment());
            tx.commit();
        }

        about_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Put the value
                isHomePage = true;
                Intent intent1 = new Intent(getActivity(), AboutUs.class);
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.slideup, R.anim.exit);
              /*  DynamicPage dynamicPage = new DynamicPage();
                Bundle args = new Bundle();
                args.putString("pageType", "about-us");
                dynamicPage.setArguments(args);
                getFragmentManager().beginTransaction().add(R.id.content_frame1, dynamicPage).commit();*/
            }
        });

        terms_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHomePage = true;
                //Put the value
                Intent intent1 = new Intent(getActivity(), LegalPageNew.class);
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.slideup, R.anim.exit);


            }
        });


        faq_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHomePage = true;
                //Put the value
                Intent intent1 = new Intent(getActivity(), HelpAndAssistance.class);
                intent1.putExtra("pagecm","2");
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.slideup, R.anim.exit);

            }
        });


        info_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHomePage = true;
                Intent intent1 = new Intent(getActivity(), InfoPage.class);
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.slideup, R.anim.exit);

            }
        });

        Rl_Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isHomePage == true) {
                    FragmentTransaction tx = getFragmentManager().beginTransaction();
                    tx.replace(R.id.content_frame1, new HomePage());
                    tx.commit();
                    isHomePage = false;
                } else {
                    Navigation_new.openDrawer();
                }
            }
        });


        setLocationRequest();

        infoBadgeCount = session.getInfoBadge();
        infoBadge.setText(infoBadgeCount);
//        buildGoogleApiClient();
        //addView();
        return parentView;
    }

    private void postData() {
        HashMap<String, String> user = session.getUserDetails();
        String UserId = user.get(SessionManager.KEY_USERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", UserId);
        jsonParams.put("type", "notification");
        jsonParams.put("page", String.valueOf("1"));
        jsonParams.put("perPage", "5");
        if (isInternetPresent) {
            postRequest_walletMoney(Iconstant.mesggaeg_url, jsonParams);
        } else {

        }
    }


    private void postRequest_walletMoney(String Url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("-------------Message  Url----------------" + Url);


        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Message  Response----------------" + response);

                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "", ScurrencySymbol = "", Str_total_amount = "", Str_total_transaction = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");

                        if (response_object.length() > 0) {
                            String totlanotification = response_object.getString("total_record");
                            session.setInfoBadge(totlanotification);
                        }
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

    }


    public static void navigationNotifyChange() {
        infoBadgeCount = session.getInfoBadge();
        infoBadge.setText(infoBadgeCount);
        if(!infoBadgeCount.equalsIgnoreCase("0") && !infoBadgeCount.equalsIgnoreCase("")){
            infoBadge.setVisibility(View.VISIBLE);
        }else{
            infoBadge.setVisibility(View.GONE);
        }
    }
    public void showDialog(String message) {
        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /* protected void startLocationUpdates() {
         if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
             LocationServices.FusedLocationApi.requestLocationUpdates(
                     mGoogleApiClient, mLocationRequest, this);
         }
     }

     protected synchronized void buildGoogleApiClient() {
         mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                 .addConnectionCallbacks(this)
                 .addOnConnectionFailedListener(this)
                 .addApi(LocationServices.API)
                 .build();
     }
 */
    private void initialize(View rootview) {
        session = new SessionManager(getActivity());
        about_layout = (LinearLayout) rootview.findViewById(R.id.bottom_layout_about_layout);
        terms_layout = (LinearLayout) rootview.findViewById(R.id.bottom_layout_terms_layout);
        faq_layout = (LinearLayout) rootview.findViewById(R.id.bottom_layout_faq_layout);
        info_layout = (RelativeLayout) rootview.findViewById(R.id.bottom_layout_info_layout);
        Rl_Home = (RelativeLayout) rootview.findViewById(R.id.home_page_main_layout);

        infoBadgeCount = session.getInfoBadge();
        Tv_home = (TextView) rootview.findViewById(R.id.bottom_layout_about_textview);
        Tv_earnings = (TextView) rootview.findViewById(R.id.bottom_layout_terms_textview);
        Tv_rating = (TextView) rootview.findViewById(R.id.bottom_layout_faq_textview);
        Tv_account = (TextView) rootview.findViewById(R.id.bottom_layout_info_textview);
        infoBadge = (TextView) rootview.findViewById(R.id.main_page_info_notification_textView);

        infoBadge.setText(infoBadgeCount);
        if(!infoBadgeCount.equalsIgnoreCase("0") && !infoBadgeCount.equalsIgnoreCase("")){
            infoBadge.setVisibility(View.VISIBLE);
           // Navigation_new.navigationNotifyChange();
        }else{
            infoBadge.setVisibility(View.GONE);
        }
       /* Tv_HeaderText = (TextView) rootview.findViewById(R.id.main_page_header_textview);

        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/AdventPro-SemiBold.ttf");
        Tv_HeaderText.setTypeface(face);*/

    }

    public static boolean isOnline = false;


    public void dismissDialog() {
        if (dialog != null)
            dialog.dismiss();
    }


    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

    }


}
