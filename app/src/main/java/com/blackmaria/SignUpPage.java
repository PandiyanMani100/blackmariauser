package com.blackmaria;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.newdesigns.Registeration;
import com.blackmaria.widgets.CustomTextView;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by user14 on 11/17/2016.
 */

public class SignUpPage extends ActivityHockeyApp implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    ObjectAnimator objectanimator;
    private SliderLayout mSlider;
    private PagerIndicator pagerIndicator;
    private TextView Rl_DriveMe;
    LinearLayout signup;
    ImageView movim;
    LanguageDb mhelper;
    private CustomTextView Tv_WhisesLable;
    private CustomTextView Tv_Identy;
    int p=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_page);
        mhelper = new LanguageDb(this);

        initialize();
        objectanimator = ObjectAnimator.ofFloat(movim,"x",200);
        objectanimator.setDuration(2000);
        objectanimator.start();
        Rl_DriveMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SignUpPage.this, LoginPage.class);
                startActivity(intent);


            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpPage.this, Registeration.class);
                startActivity(intent);

            }
        });


    }

    private void initialize() {

        mSlider = (SliderLayout) findViewById(R.id.signup_page_image_slider);
        pagerIndicator = (PagerIndicator) findViewById(R.id.signup_page_image_slider_indicator);

        Rl_DriveMe = (TextView) findViewById(R.id.signup_page_drive_me_layout);
        TextView signuptext = (TextView) findViewById(R.id.signuptext);
        TextView blackmarianicn= (TextView) findViewById(R.id.blackmarianicn);

        Rl_DriveMe.setText(getkey("signinnn"));
        signuptext.setText(getkey("signup"));
        blackmarianicn.setText(getkey("blackmaria_inc"));

        signup= (LinearLayout) findViewById(R.id.signup);
        Tv_WhisesLable = (CustomTextView) findViewById(R.id.signup_page_whishes_textview);
        Tv_Identy = (CustomTextView) findViewById(R.id.signup_page_identy_lable);

        movim = (ImageView) findViewById(R.id.movim);




        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/AdventPro-SemiBold.ttf");
        Tv_WhisesLable.setTypeface(face);
        Tv_Identy.setTypeface(face);

        sliderMethod();
    }

    private void sliderMethod() {

        ArrayList<Integer> productImage = new ArrayList<Integer>();
        productImage.add(R.drawable.slide_new1);
        productImage.add(R.drawable.slide_new2);
        productImage.add(R.drawable.slide_new3);
        productImage.add(R.drawable.slide_new4);
        productImage.add(R.drawable.slide_new5);

        for (int i = 0; i < productImage.size(); i++) {

            TextSliderView textSliderView = new TextSliderView(SignUpPage.this);
            // initialize a SliderLayout
            textSliderView.description(String.valueOf(i)).image(productImage.get(i)).setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(SignUpPage.this);
            //add your extra information
//            textSliderView.bundle(new Bundle());
//            textSliderView.getBundle().putString("extra", "sdfsdfsdf");

            mSlider.addSlider(textSliderView);

        }


        mSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mSlider.setCustomAnimation(new DescriptionAnimation());
        mSlider.setCustomIndicator(pagerIndicator);
        mSlider.setDuration(2000);
        mSlider.addOnPageChangeListener(this);
        mSlider.startAutoCycle();


    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
//        Toast.makeText(this,slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();
        mSlider.startAutoCycle();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}

