package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.RequiresApi;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.DowloadPdfActivity;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.TextRCBold;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Muruganantham on 3/1/2018.
 */
@SuppressLint("Registered")
public class PaymentSuccessInfoActivity extends ActivityHockeyApp implements DowloadPdfActivity.downloadCompleted {

    private TextView Transection_id, paypal_tv;
    private CustomTextView transection_mode, paypal_id, current_username, date, time;
    private TextRCBold Amount;
    RelativeLayout Rl_close, Rl_save;
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String UserNmae = "", UserID = "", Scurrent_user = "", Smode = "", Spaypalid = "", amount = "", transection_id = "", account = "", currency = "", flag = "";

    private RefreshReceiver refreshReceiver;
    TextView payid_dot;


    RelativeLayout savelyt;
    LinearLayout pdflayout;
    private LinearLayout bottom_laout;
    private TextView username, header_text, sava_tv;


    private TextView username1, usernamedot, usernameTv;
    private String fromBookingpage ="";

    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(PaymentSuccessInfoActivity.this, Navigation_new.class);
                        startActivity(intent1);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(PaymentSuccessInfoActivity.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(PaymentSuccessInfoActivity.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xenditbank_payment_success);
        initiaztion();

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initiaztion() {

        session = new SessionManager(PaymentSuccessInfoActivity.this);
        cd = new ConnectionDetector(PaymentSuccessInfoActivity.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        UserNmae = info.get(SessionManager.KEY_USERNAME);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);


        Transection_id = (TextView) findViewById(R.id.txt_trasection_id);
        transection_mode = (CustomTextView) findViewById(R.id.paypal_to);
        paypal_id = (CustomTextView) findViewById(R.id.paypal_id_txt);
        current_username = (CustomTextView) findViewById(R.id.current_username);
        date = (CustomTextView) findViewById(R.id.trans_date);
        time = (CustomTextView) findViewById(R.id.trans_time);
        Amount = (TextRCBold) findViewById(R.id.amount_text);
        Rl_close = (RelativeLayout) findViewById(R.id.close_btn);
        Rl_save = (RelativeLayout) findViewById(R.id.tarnsection_save);
        paypal_tv = (TextView) findViewById(R.id.paypal_tv);
        payid_dot = (TextView) findViewById(R.id.payid_dot);
        pdflayout = (LinearLayout) findViewById(R.id.pdflayout);
        username1 = (TextView) findViewById(R.id.name);
        usernamedot = (TextView) findViewById(R.id.name_dot_value);
        usernameTv = (TextView) findViewById(R.id.name_tv);


        savelyt = (RelativeLayout) findViewById(R.id.savelyt);
        header_text = (TextView) findViewById(R.id.header_text);
        bottom_laout = (LinearLayout) findViewById(R.id.bottom_laout);
        sava_tv = (TextView) findViewById(R.id.sava_tv);


        Intent in = getIntent();
        flag = in.getStringExtra("flag");

        if (getIntent().hasExtra("frombookingpage")) {
            fromBookingpage = getIntent().getStringExtra("frombookingpage");
        }

        if (flag.equalsIgnoreCase("XenditbankTransfer")) {
            Smode = in.getStringExtra("mode");
            amount = in.getStringExtra("amount");
            transection_id = in.getStringExtra("ref_no");
            currency = in.getStringExtra("currency");
            account = in.getStringExtra("account");
            paypal_id.setVisibility(View.VISIBLE);
            paypal_tv.setVisibility(View.VISIBLE);
            paypal_tv.setText(getResources().getString(R.string.account));
            paypal_id.setText(account);

            usernamedot.setVisibility(View.GONE);
            usernameTv.setVisibility(View.GONE);
            username1.setVisibility(View.GONE);

        } else if (flag.equalsIgnoreCase("paypal_withdrawal")) {
            paypal_tv.setVisibility(View.VISIBLE);
            paypal_id.setVisibility(View.VISIBLE);
            Smode = in.getStringExtra("mode");
            amount = in.getStringExtra("amount");
            transection_id = in.getStringExtra("ref_no");
            currency = in.getStringExtra("currency");
            Spaypalid = in.getStringExtra("paypal_id");
            paypal_id.setText(Spaypalid);
            paypal_id.setAllCaps(false);
            header_text.setText(getResources().getString(R.string.payments_suucess));

            usernamedot.setVisibility(View.GONE);
            usernameTv.setVisibility(View.GONE);
            username1.setVisibility(View.GONE);
        } else {
            Smode = in.getStringExtra("mode");
            amount = in.getStringExtra("amount");
            transection_id = in.getStringExtra("ref_no");
            currency = in.getStringExtra("currency");
            paypal_tv.setVisibility(View.VISIBLE);
            paypal_id.setText(transection_id);
            paypal_tv.setText(getResources().getString(R.string.trn_number));
            payid_dot.setVisibility(View.GONE);
            paypal_id.setVisibility(View.VISIBLE);
            header_text.setText(getResources().getString(R.string.payments_suucess));

            username1 .setVisibility(View.VISIBLE);
            usernamedot.setVisibility(View.VISIBLE);
            usernameTv.setVisibility(View.VISIBLE);

            usernameTv.setText(UserNmae);
        }


        setvalues();

        Rl_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(PaymentSuccessInfoActivity.this, WalletMoneyPage1.class);
                if(fromBookingpage.equalsIgnoreCase("1")){
                    intent1.putExtra("fromBookingpage", "1");
                }
                startActivity(intent1);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();

            }
        });
        Rl_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savelyt.setVisibility(View.GONE);
                new DowloadPdfActivity(PaymentSuccessInfoActivity.this, pdflayout, Smode, PaymentSuccessInfoActivity.this);

            }
        });

    }


    public static Bitmap getBitmapFromView(View view, int totalHeight, int totalWidth) {
        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            bgDrawable.draw(canvas);
        } else {
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }

    private void setvalues() {
        Transection_id.setText(getResources().getString(R.string.trans_no) + " " + ":" + " " + transection_id);
        transection_mode.setText(Smode);
        current_username.setText(UserNmae);
        Amount.setText(currency + " " + amount);
        date.setText(GetCurrentDate());
        time.setText(GetCurrentTime());
    }

    private String GetCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    private String GetCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss aaa");
        String formattedtime = df.format(c.getTime());
        return formattedtime;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }


    @Override
    public void OnDownloadComplete(String fileName) {
        savelyt.setVisibility(View.VISIBLE);
        sava_tv.setText(getResources().getString(R.string.saved));

        ViewPdfAlert("", getResources().getString(R.string.sucessss), fileName);
    }

    //--------------Alert Method-----------
    private void ViewPdfAlert(String title, String alert, final String fileName) {

        final PkDialog mDialog = new PkDialog(PaymentSuccessInfoActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.open), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPDF(fileName);
                mDialog.dismiss();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    public void viewPDF(String pdfFileName) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Signature/" + pdfFileName);  // -> filename = maven.pdf
        Uri uri_path = Uri.fromFile(pdfFile);
        Uri uri = uri_path;

        try {
            File file = null;
            String path = uri.toString();// "/mnt/sdcard/FileName.mp3"
            try {
                file = new File(new URI(path));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            final Intent intent = new Intent(Intent.ACTION_VIEW)//
                    .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                    androidx.core.content.FileProvider.getUriForFile(PaymentSuccessInfoActivity.this, getPackageName() + ".provider", file) : Uri.fromFile(file),
                            "application/pdf").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(PaymentSuccessInfoActivity.this,  getResources().getString(R.string.no_pdf), Toast.LENGTH_SHORT).show();
            }

        } catch (ActivityNotFoundException e) {
            Toast.makeText(PaymentSuccessInfoActivity.this, getResources().getString(R.string.no_read_file), Toast.LENGTH_SHORT).show();
        }

    }

}
