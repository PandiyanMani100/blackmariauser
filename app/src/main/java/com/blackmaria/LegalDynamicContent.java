package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by GANESH on 28-08-2017.
 */

public class LegalDynamicContent extends ActivityHockeyApp implements View.OnClickListener {

    private String sDriveID = "", sPageType = "", sPageTitle = "", sPageNo = "";

    private TextView Tv_dynamicContent, Tv_dynamicTitle;
    private RelativeLayout RL_back;

    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;
    Dialog dialog;
    ImageView imgPageLogo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.legal_content_single);
        initialize();
    }

    private void initialize() {
        sessionManager = new SessionManager(LegalDynamicContent.this);
        cd = new ConnectionDetector(LegalDynamicContent.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_USERID);

        RL_back = (RelativeLayout) findViewById(R.id.RL_back);
        Tv_dynamicContent = (TextView) findViewById(R.id.txt_dynamic_content);
        Tv_dynamicTitle = (TextView) findViewById(R.id.txt_page_title);
        imgPageLogo = (ImageView) findViewById(R.id.img_page_logo1);
        RL_back.setOnClickListener(this);

        getIntentData();

        Tv_dynamicTitle.setText(sPageTitle);


        if (cd.isConnectingToInternet()) {
            dynamicContentRequest(Iconstant.Dynamic_content_Url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            sPageType = intent.getStringExtra("pageType");
            sPageTitle = intent.getStringExtra("pageTitle");
            sPageNo = intent.getStringExtra("pageNo");
        }

        if (sPageTitle.equalsIgnoreCase(getResources().getString(R.string.legal_page_label_facebook_policy))) {
            imgPageLogo.setBackgroundResource(R.drawable.legal_round_fb);
            //imgPageLogo.setBackgroundDrawable(getResources().getDrawable(R.drawable.legal_round_fb));
        } else {
            imgPageLogo.setBackgroundResource(R.drawable.legal_round_hammer);
            //imgPageLogo.setBackgroundDrawable(getResources().getDrawable(R.drawable.legal_round_hammer));
        }
    }


    //-------------------dynamic Content Request----------------
    private void dynamicContentRequest(String Url) {

        dialog = new Dialog(LegalDynamicContent.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", sDriveID);
        jsonParams.put("page_type", sPageType);
        jsonParams.put("app_type", "user");

        System.out.println("--------------dynamic Content Url-------------------" + Url);
        System.out.println("--------------dynamic Content Params-------------------" + jsonParams);

        mRequest = new ServiceRequest(LegalDynamicContent.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------dynamic Content Response-------------------" + response);
                String status = "", sTitle = "", sContent = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {
                                JSONObject pageObject = jsonObject.getJSONObject("pages_details");
                                if (pageObject.length() > 0) {
                                    sTitle = pageObject.getString("page_title");
                                    sContent = pageObject.getString("page_description");
                                }
                            }
                        } else {
                            sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    Tv_dynamicContent.setText(Html.fromHtml(sContent));
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(LegalDynamicContent.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onClick(View view) {
        if (view == RL_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }
}
