package com.blackmaria.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.text.format.DateFormat;

import androidx.core.app.NotificationManagerCompat;

import com.blackmaria.FetchingDataActivity;
import com.blackmaria.InfoPage;
import com.blackmaria.PushNotificationAlert;
import com.blackmaria.R;
import com.blackmaria.TrackRideAcceptPage;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.chatmodule.chathandlingintentservice;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.redirectingtchatpage;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final int NOTIFICATION_ID = 1;
    NotificationCompat.Builder builder;
    Context mcontext;
    private SessionManager session;
    public JSONObject extras;
    private String key1 = "", key2 = "", key3 = "", message = "", action = "";
    private Bitmap bitmap;
    private Bitmap frombitmap;
    private String driverID = "", driverName = "", driverEmail = "", driverImage = "", driverRating = "",
            driverLat = "", driverLong = "", driverTime = "", rideID = "", driverMobile = "",
            driverCar_no = "", driverCar_model = "";
    Bitmap remote_picture_reciver = null;
    private NotificationManagerCompat notificationManagerCompat;
    private static final String CHANNEL_ID = "channel_id";
    String key22="";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);
        mcontext = this;
        notificationManagerCompat = NotificationManagerCompat.from(mcontext);
        session = new SessionManager(this);
        bitmap = BitmapFactory.decodeResource(mcontext.getResources(), R.drawable.app_icon);
        if (remoteMessage.getData() != null) {
            try {
                Map<String, String> params = remoteMessage.getData();


                extras = new JSONObject(params);
                System.out.println("firebasemsg  " + extras.toString());
                if (extras.has("message")) {
                    message = extras.getString("message").toString();
                }
                if (extras.has("action")) {
                    action = extras.getString("action").toString();
                }
                if (action.equalsIgnoreCase(Iconstant.drop_user_key)) {
                    session.setWaitedTime(0, 0, "", "");
                }

                if (action.equalsIgnoreCase(Iconstant.pushNotification_Ads)) {
                    key22 = extras.getString("key2").toString();
                    if (extras.has(Iconstant.Ads_image) && extras.getString(Iconstant.Ads_image).toString() != null && extras.getString(Iconstant.Ads_image).toString().length() > 0) {
                        frombitmap=getBitmapFromURL(extras.get(Iconstant.Ads_image).toString());
                        //sendNotification(message.toString(), extras.get(Iconstant.Ads_image).toString());
                        createExpandableImageNotification(message.toString(),action,key22);
                    } else {
                       // sendNotification(message.toString());
                        dummytest(message.toString(),action,key22);
                    }
                } else {
                    if (action.equalsIgnoreCase(Iconstant.PushNotification_AcceptRide_Key)) {
                        try {
                            sendBroadCastToRideConfirm(extras);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else if (action.equalsIgnoreCase(Iconstant.complementry_time_closed)) {
                        try {
                            complementryTimeClosed(extras);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else{
                        //sendNotification(message.toString(), extras);
                        if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_CHATMESSAGE))
                        {
                            if (extras.has("rideID"))
                            {
                                rideID = extras.getString("rideID").toString();
                                chatmessagetest(message.toString(),rideID,extras);
                            }
                        }
                        else
                        {
                            dummytest(message.toString(),action,key22);
                        }


                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void complementryTimeClosed(JSONObject messageObject) throws Exception {
//        refreshMethod();
        dummytest(messageObject.getString(Iconstant.Push_Complementrytime_Completed),action,key22);
        Intent i1 = new Intent(mcontext, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Complementrytime_Completed));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_ComplementrytimeAction_Completed));
        i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Cancelled));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mcontext.startActivity(i1);
    }

    @SuppressWarnings("deprecation")
    private void dummytest(String msg,String action,String subtitle)
    {
        Intent intent = null;
        String titilee = mcontext.getString(R.string.app_name);
        String subtitilee =msg;
        if(action.equals("ads"))
        {
            titilee = msg;
            subtitilee= subtitle;
            intent = new Intent(mcontext, InfoPage.class);
        }
        else if(action.equals("coupon_info"))
        {
            titilee = "Coupon";
            intent = new Intent(mcontext, FetchingDataActivity.class);
        }
        else if(action.equals("ride_later_confirmed"))
        {
            titilee = "Ride Request";
            intent = new Intent(mcontext, FetchingDataActivity.class);
        }
        else if(action.equals("cab_arrived") || action.equals("trip_begin") || action.equals("make_payment"))
        {
            titilee = "Trip Info";
            intent = new Intent(mcontext, FetchingDataActivity.class);
        }
        else if(action.equals("payment_paid"))
        {
            titilee = "Payment Paid";
            intent = new Intent(mcontext, FetchingDataActivity.class);
        }
        else if(action.equals("blackmariachat"))
        {
            titilee = "New Message From Driver";
            intent = new Intent(mcontext, FetchingDataActivity.class);
        }
        else
        {
            intent = new Intent(mcontext, FetchingDataActivity.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        showNotification(getApplicationContext(),titilee,subtitilee,intent);
        /*
        PendingIntent pendingIntent = PendingIntent.getActivity(mcontext, 0, intent, 0);
        androidx.core.app.NotificationCompat.Builder builder = new androidx.core.app.NotificationCompat.Builder(mcontext, CHANNEL_ID)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(titilee)
                .setLargeIcon(bitmap)
                .setContentIntent(pendingIntent)
                .setStyle(new androidx.core.app.NotificationCompat.BigTextStyle()
                        .bigText(subtitilee));

        notificationManagerCompat.notify(5, builder.build());*/
    }

    @SuppressWarnings("deprecation")
    private void chatmessagetest(String msg, String rideid, JSONObject response)
    {
       System.out.println("first came---xmpp");
        Intent intent = new Intent(mcontext, redirectingtchatpage.class);
        intent.putExtra("RideID", rideid);

        showNotification(getApplicationContext(),"New Message From Partner",msg,intent);

        /*PendingIntent pendingIntent = PendingIntent.getActivity(mcontext, 0, intent, 0);
        androidx.core.app.NotificationCompat.Builder builder = new androidx.core.app.NotificationCompat.Builder(mcontext, CHANNEL_ID)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle("New Message From Partner")
                .setLargeIcon(bitmap)
                .setContentIntent(pendingIntent)
                .setStyle(new androidx.core.app.NotificationCompat.BigTextStyle()
                        .bigText(msg));

        notificationManagerCompat.notify(5, builder.build());*/



        Intent intent1 = new Intent(mcontext, chathandlingintentservice.class);
        try {
            intent1.putExtra("desc", response.getString("message"));
            intent1.putExtra("sender_ID", response.getString("senderID"));
            intent1.putExtra("timestamp", response.getString("timeStamp"));
            intent1.putExtra("ride_id", response.getString("rideID"));
            if(response.has("datetime"))
            {
                intent1.putExtra("datetime", response.getString("datetime"));
            }
            else
            {
                intent1.putExtra("datetime", getDate(Long.parseLong(response.getString("timeStamp"))));
            }
            mcontext.startService(intent1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("dd MMM yy hh:mm a", cal).toString();
        return date;
    }

    public void showNotification(Context context, String title, String body, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(title)
                .setLargeIcon(frombitmap)
                .setContentText(body);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(notificationId, mBuilder.build());
    }

    private void createExpandableImageNotification(String msg,String action,String subtitle){

        Intent intent= null;
        String titilee = mcontext.getString(R.string.app_name);
        String subtitilee =msg;
        if(action.equals("ads"))
        {
            titilee = msg;
            subtitilee= subtitle;
            intent = new Intent(mcontext, InfoPage.class);
        }
        else
        {
            intent = new Intent(mcontext, FetchingDataActivity.class);
        }

        showNotification(getApplicationContext(),titilee,subtitilee,intent);
/*
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(mcontext, 0, intent, 0);
        final int min = 20;
        final int max = 80;
        final int random = new Random().nextInt((max - min) + 1) + min;


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            *//* Create or update. *//*
            NotificationChannel channel = new NotificationChannel("my_channel_01",
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManagerCompat.createNotificationChannel(channel);
        }

        androidx.core.app.NotificationCompat.Builder builder = new androidx.core.app.NotificationCompat.Builder(mcontext, CHANNEL_ID)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(titilee)
                .setContentText(subtitilee)
                .setLargeIcon(frombitmap)
                .setContentIntent(pendingIntent)
                .setStyle(new androidx.core.app.NotificationCompat.BigPictureStyle()
                        .bigPicture(frombitmap)
                        .bigLargeIcon(null));
        notificationManagerCompat.notify(random, builder.build());*/
    }

    private void sendBroadCastToRideConfirm(JSONObject messageObject) throws Exception {

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.app.timerPage.finish");
        sendBroadcast(broadcastIntent);
        Intent broadcastIntent1 = new Intent();
        broadcastIntent1.setAction("com.package.finish.BookingPage");
        sendBroadcast(broadcastIntent1);

        session.setCouponCode("", "");
        session.setReferralCode("", "");
        Intent i = new Intent(this, TrackRideAcceptPage.class);
        i.putExtra("driverID", messageObject.getString(Iconstant.DriverID));
        i.putExtra("driverName", messageObject.getString(Iconstant.DriverName));
        i.putExtra("driverImage", messageObject.getString(Iconstant.DriverImage));
        i.putExtra("driverRating", messageObject.getString(Iconstant.DriverRating));
        i.putExtra("driverTime", messageObject.getString(Iconstant.DriverTime));
        i.putExtra("rideID", messageObject.getString(Iconstant.RideID));
        i.putExtra("driverMobile", messageObject.getString(Iconstant.DriverMobile));
        i.putExtra("driverCar_no", messageObject.getString(Iconstant.DriverCar_No));
        i.putExtra("driverCar_model", messageObject.getString(Iconstant.DriverCar_Model));
        i.putExtra("flag", "1");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }







    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
}
