package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.MilesListAdapter;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.pojo.MilesListPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 5/19/2017.
 */
public class MilesRewardsList extends FragmentActivityHockeyApp {


    private ImageView back, Iv_rewards_list_home, Iv_rewards_list_next, Iv_rewards_list_previous;

    //    private Spinner Sp_datedisply;
    private ListView details_listview;
    private TextView  no_miles_text,all,today,thisweek,thismonth,thisyear;


    private ArrayList<MilesListPojo> MilesList_itemList;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    LanguageDb mhelper;
    private ServiceRequest mRequest;
    Dialog dialog;
    private SessionManager session;
    private String UserID = "", Slist_type = "all", perPage = "5", Stotal_rides = "", Stotal_distance = "";
    private int currentPage = 1;
    private int nextPage;
    private boolean isMilesAvailable = false;
    private MilesListAdapter milesadapter;
    private RefreshReceiver refreshReceiver;


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(MileageDetails.this, Fragment_HomePage.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(MilesRewardsList.this, MyRideRating.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(MilesRewardsList.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.miles_list_view_constrainlayout);
        mhelper = new LanguageDb(this);
        initization();
        postData();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MilesRewardsList.this, MilesHome.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        Iv_rewards_list_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MilesRewardsList.this, Navigation_new.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);


            }
        });


        Iv_rewards_list_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentPage = currentPage + 1;
                postData();


            }
        });


        Iv_rewards_list_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentPage = currentPage - 1;
                postData();

            }
        });


    }

    @Override
    public void onBackPressed() {

    }

    private void initization() {

        cd = new ConnectionDetector(MilesRewardsList.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(MilesRewardsList.this);

        MilesList_itemList = new ArrayList<MilesListPojo>();


        all =  findViewById(R.id.all);
        all.setText(getkey("all"));
        today = findViewById(R.id.today);
        today.setText(getkey("today"));
        thisweek = findViewById(R.id.thisweek);
        thisweek.setText(getkey("this_week"));
        thismonth =  findViewById(R.id.thismonth);
        thismonth.setText(getkey("this_month"));
        thisyear = findViewById(R.id.thisyear);
        thisyear.setText(getkey("this_year"));
        back = (ImageView) findViewById(R.id.miles_back_img);
        details_listview = (ListView) findViewById(R.id.wallet_money_transaction_listview);
        Iv_rewards_list_home = (ImageView) findViewById(R.id.rewards_list_home);
        Iv_rewards_list_next = (ImageView) findViewById(R.id.rewards_list_next_page);
        Iv_rewards_list_previous = (ImageView) findViewById(R.id.rewards_list_prev_page);
        no_miles_text = findViewById(R.id.mileage_no_rides_textview);
        no_miles_text.setText(getkey("no_mileage_rides_label"));
        TextView txt_page_title =  findViewById(R.id.txt_page_title);
        txt_page_title.setText(getkey("statements"));



        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

//        initCustomSpinner();

        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Slist_type = "all";
                postData();
            }
        });

        today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Slist_type = "day";
                postData();
            }
        });

        thisweek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Slist_type = "week";
                postData();
            }
        });

        thismonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Slist_type = "month";
                postData();
            }
        });

        thisyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Slist_type = "year";
                postData();
            }
        });

    }


    //--------------------------custom spinner---------------------------------

    private void initCustomSpinner() {
        Spinner spinnerCustom = (Spinner) findViewById(R.id.date_spinner);

        // Spinner Drop down elements
        ArrayList<String> searchList = new ArrayList<String>();
        searchList.add(getkey("all_label"));
        searchList.add(getkey("today_label"));
        searchList.add(getkey("last_one_week_label"));
        searchList.add(getkey("last_one_month_label"));
        searchList.add(getkey("last_one_year_label"));

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(MilesRewardsList.this, searchList);
        spinnerCustom.setAdapter(customSpinnerAdapter);
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
                currentPage = 1;
                if (getkey("today_label").equalsIgnoreCase(item)) {
                    Slist_type = "day";
                    postData();
                } else if (getkey("last_one_week_label").equalsIgnoreCase(item)) {
                    Slist_type = "week";
                    postData();
                } else if (getkey("last_one_month_label").equalsIgnoreCase(item)) {
                    Slist_type = "month";
                    postData();
                } else if (getkey("last_one_year_label").equalsIgnoreCase(item)) {
                    Slist_type = "year";
                    postData();
                } else {
                    Slist_type = "all";
                    postData();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {


        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.regular.ttf");

        private final Context activity;
        private ArrayList<String> asr;

        public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
            this.asr = asr;
            activity = context;
        }


        public int getCount() {
            return asr.size();
        }

        public Object getItem(int i) {
            return asr.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }


        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(MilesRewardsList.this);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(13);
            txt.setGravity(Gravity.CENTER_VERTICAL);
            txt.setText(asr.get(position));
            txt.setBackground(getResources().getDrawable(R.drawable.spinner_drop_down_background));
            txt.setTypeface(face);
            txt.setTextColor(Color.parseColor("#ffffff"));
            return txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(MilesRewardsList.this);
            txt.setGravity(Gravity.CENTER);
            txt.setPadding(10, 0, 0, 0);
            txt.setTextSize(12);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.spinner_arrow_bg_new, 0);
            txt.setText(asr.get(i));
            txt.setAllCaps(true);
            txt.setTextColor(Color.parseColor("#ffffff"));

            txt.setTypeface(face);

            return txt;
        }

    }


    private void postData() {


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("list_type", Slist_type);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        System.out.println("-------------MileageDetails jsonParams----------------" + jsonParams);
        if (isInternetPresent) {
            postRequestMileageDetails(Iconstant.mileage_url, jsonParams);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));

        }
    }


    private void postRequestMileageDetails(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(MilesRewardsList.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));


        mRequest = new ServiceRequest(MilesRewardsList.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Mileagelist Response----------------" + response);

                String Sstatus = "", Scurrency_code = "", Scurrentbalance = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    JSONObject jobject = object.getJSONObject("response");
                    if (jobject.length() > 0) {

                        currentPage = Integer.parseInt(jobject.getString("current_page"));

                        String nextPages = jobject.getString("next_page");
                        if (!nextPages.equalsIgnoreCase("")) {
                            nextPage = Integer.parseInt(jobject.getString("next_page"));
                        } else {
                            nextPage = 0;
                        }
                        Stotal_rides = jobject.getString("total_rides");
                        Stotal_distance = jobject.getString("total_distance");
//                        Smileage_expiry = jobject.getString("mileage_expiry");

                        Object check_object = jobject.get("rides");
                        if (check_object instanceof JSONArray) {
                            JSONArray RideList_list_jsonArray = jobject.getJSONArray("rides");
                            if (RideList_list_jsonArray.length() > 0) {

                                MilesList_itemList.clear();

                                for (int i = 0; i < RideList_list_jsonArray.length(); i++) {
                                    JSONObject rides_obj = RideList_list_jsonArray.getJSONObject(i);
                                    MilesListPojo mileslistpojo = new MilesListPojo();
//                                    mileslistpojo.setRide_id(rides_obj.getString("ride_id"));
                                    mileslistpojo.setRide_time(rides_obj.getString("time"));
                                    mileslistpojo.setRide_date(rides_obj.getString("date"));
//                                    mileslistpojo.setRide_satus(rides_obj.getString("ride_status"));
                                    mileslistpojo.setRide_details(rides_obj.getString("detail"));
                                    mileslistpojo.setRide_distance(rides_obj.getString("distance"));
                                    mileslistpojo.setRide_refname(rides_obj.getString("ref_name"));
                                    mileslistpojo.setRide_datetime(rides_obj.getString("datetime"));
                                    MilesList_itemList.add(mileslistpojo);
                                }

                                isMilesAvailable = true;

                            } else {
                                isMilesAvailable = false;
                                MilesList_itemList.clear();
                            }
                        } else {
                            isMilesAvailable = false;
                            MilesList_itemList.clear();
                        }
                    } else {
                        Smessage = jobject.getString("response");
                    }
                    dialogDismiss();
                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (currentPage == 1) {
//                            Iv_rewards_list_next.setVisibility(View.GONE);
                            Iv_rewards_list_next.setVisibility(View.VISIBLE);
                            Iv_rewards_list_previous.setVisibility(View.GONE);
                        } else {
                            Iv_rewards_list_previous.setVisibility(View.VISIBLE);
                            Iv_rewards_list_next.setVisibility(View.VISIBLE);
                        }

                        if (nextPage == 0) {
                            Iv_rewards_list_previous.setVisibility(View.GONE);
                            Iv_rewards_list_next.setVisibility(View.GONE);
                            dialogDismiss();
                        }

                        if (isMilesAvailable) {
                            no_miles_text.setVisibility(View.GONE);

                            milesadapter = new MilesListAdapter(MilesRewardsList.this, MilesList_itemList);
                            details_listview.setAdapter(milesadapter);
                            milesadapter.notifyDataSetChanged();
                        } else {

                            no_miles_text.setVisibility(View.VISIBLE);
                        }
                    } else {

                        Alert(getkey("action_error"), Smessage);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    dialogDismiss();
                }


            }

            @Override
            public void onErrorListener() {

                dialogDismiss();
            }
        });


    }


    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(MilesRewardsList.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }


    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
