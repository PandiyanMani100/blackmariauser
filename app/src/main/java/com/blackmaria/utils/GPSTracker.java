package com.blackmaria.utils;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;


import static android.content.Context.LOCATION_SERVICE;

public class GPSTracker {
    public Context mContext;
    // Flag for GPS status
    public boolean isGPSEnabled = false;

    // Flag for network status
    boolean isNetworkEnabled = false;
    public static GPSTracker instance = null;
    // Flag for GPS status
    public boolean canGetLocation = false;

    // Declaring a Location Manager
    protected LocationManager locationManager;
    public static GPSTracker getInstance(Context context) {
        if (instance == null) {
            instance = new GPSTracker(context);
        }
        return instance;
    }

    public GPSTracker(Context context){
        try {
            mContext = context;
            mContext.startService(new Intent(mContext, Locationservices.class));
            getLocation();
        }catch (IllegalStateException e){

        }catch (Exception e){}

    }

    private void getLocation() {
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        // Getting GPS status
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // Getting network status
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled && !isNetworkEnabled) {
            // No network provider is enabled
        } else {
            this.canGetLocation = true;
            this.isGPSEnabled = true;
        }
    }

    public double getLatitude() {
        SessionManager sessionManager = new SessionManager(mContext);
        // return latitude
        return sessionManager.getLatitude();
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        // return longitude
        SessionManager sessionManager = new SessionManager(mContext);
        return sessionManager.getLongitude();
    }


    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public boolean isgpsenabled() {
        return this.isGPSEnabled;
    }
}
