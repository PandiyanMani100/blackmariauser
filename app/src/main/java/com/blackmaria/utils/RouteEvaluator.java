package com.blackmaria.utils;

import android.animation.TypeEvaluator;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by amal.chandran on 22/12/16.
 */

public class RouteEvaluator implements TypeEvaluator<LatLng> {
    @Override
    public LatLng evaluate(float t, LatLng startPoint, LatLng endPoint) {
        double lat = 0, lng = 0;
        try {
            lat = startPoint.latitude + t * (endPoint.latitude - startPoint.latitude);
            lng = startPoint.longitude + t * (endPoint.longitude - startPoint.longitude);
        } catch (Exception e) {

        }
        return new LatLng(lat, lng);
    }
}
