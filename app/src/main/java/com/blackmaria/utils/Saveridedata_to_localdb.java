package com.blackmaria.utils;

import android.content.Context;

import com.android.volley.Request;
import com.blackmaria.dbconnection.Ridedetails_db;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.AppController;
import com.blackmaria.volley.ServiceRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.HashMap;

public class Saveridedata_to_localdb {

    public Context mcontext;
    public SessionManager sessionManager;
    public String mrideid = "";
    private Gson gson;
    private GsonBuilder gsonBuilder;
    String UserID = "";

    public Saveridedata_to_localdb(Context context, String rideid) {
        mcontext = context;
        mrideid = rideid;
        sessionManager = new SessionManager(mcontext);
        gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();


        HashMap<String, String> user = sessionManager.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", mrideid);
        jsonParams.put("user_id", UserID);

        getRidedata(Iconstant.GetRideDetail, jsonParams);
    }

    private void getRidedata(String Url, final HashMap<String, String> jsonParams) {

        ServiceRequest mRequest = new ServiceRequest(mcontext);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                // here we have to store the ride data in local db
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        JSONObject ride_data = jsonObject.getJSONArray("response").getJSONObject(0);
                        Ridedetails_db db = AppController.getDBInstance(mcontext);
                        db.putride_data(mrideid, ride_data);

                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("ride_id", mrideid);
                        jsonParams.put("user_id", UserID);

                        getRidedata_detailview(Iconstant.GetRideDetail_view,jsonParams);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }

    private void getRidedata_detailview(String Url, final HashMap<String, String> jsonParams) {

        ServiceRequest mRequest = new ServiceRequest(mcontext);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                // here we have to store the ride data in local db
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        JSONObject ride_data = jsonObject.getJSONObject("response");
                        Ridedetails_db db = AppController.getDBInstance(mcontext);
                        db.putride_data_detailview(mrideid, ride_data);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }

}
