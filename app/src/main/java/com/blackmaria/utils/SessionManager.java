package com.blackmaria.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SessionManager {

    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "PremKumar";

    public static String LOCATION_LATITUDE = "location_latitiude";
    public static String LOCATION_LONGITUDE = "location_longitude";


    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_CONTACT_NUMBER = "contact_number";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_CONTACT_EMAIL = "contact_email";
    public static final String KEY_VEHICLE_TYPE = "vehicle_type";
    public static final String KEY_BUSSINESPROFILE = "businessprofile";
    public static final String KEY_BUSSINESNAME = "businessname";
    public static final String KEY_QRCODEPATH = "qrcodepath";
    public static final String KEY_FIRSTTIMELOGIN = "firstimelogin";
    public static final String KEY_USERID = "userid";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_USERIMAGE = "userimage";


    public static final String KEY_IMAGESTATUS = "imagesstatus";
    public static final String KEY_ACCESSKEY = "access_key";
    public static final String KEY_SECRETKEY = "secret_key";
    public static final String KEY_BUCKETNAME = "bucket_name";
    public static final String KEY_BUCKETURL = "bucket_url";

    public static final String KEY_currency = "KEY_currency";
    public static final String KEY_saveplusbalance = "KEY_saveplusbalance";
    public static final String KEY_saveplusid = "KEY_saveplusid";
    public static final String KEY_codepath = "KEY_codepath";




    public static final String KEYMODEUPDATE = "modeupdate";
    public static final String KEYCURRENTLANGUAGE = "currentlanguage";


    public static final String KEY_CLOSEAMOUNT = "closeamount";
    public static final String KEY_USERIMAGENAME = "userimagename";
    public static final String KEY_HOMEUSERIMAGE = "userimage_home";
    public static final String KEY_GCM_ID = "gcmId";
    public static final String KEY_DRIVERID = "driverid";
    public static final String KEY_EMERGENCY_NUM = "gcmId";
    public static final String KEY_FIRSTTIME_BOOK = "firsttimebooking";

    public static final String KEY_COUNTRYCODE = "countrycode";
    public static final String KEY_PHONENO = "phoneno";
    public static final String KEY_REFERAL_CODE = "referalcode";
    public static final String KEY_REFERAL_CODE_MSG = "referalcodemsg";
    public static final String KEY_CATEGORY = "category";
    public static final String KEY_CATEGORY_ID = "categoryId";
    public static final String KEY_CASHLOCTIONSLIST = "cashlocationlist";


    public static final String KEY_COUPON_CODE = "coupon";
    public static final String KEY_COUPON_CODE_SOURCE = "couponsource";
    public static final String KEY_VEHICLE_BitMap_IMAGE = "bitmap";
    public static final String KEY_WALLET_AMOUNT = "walletAmount";

    public static final String KEY_XMPP_USERID = "xmppUserId";
    public static final String KEY_XMPP_SEC_KEY = "xmppSecKey";
    public static final String KEY_AUTOCHARE = "status";
    public static final String KEY_TIPTIME = "time";
    public static final String KEY_APP_STATUS = "appStatus";
    public static final String KEY_FACBOOKID = "facbookid";
    public static final String KEY_SITECONTACTEMAIL = "sitecontactemail";

    public static final String KEY_LANGUAGE_CODE = "language_code";
    public static final String KEY_PATTERN = "savepluspattern_code";
    public static final String KEY_SAVEPLUSID = "saveplusid";
    public static final String KEY_SAVEPLUSWITHDRAWAL_STATUS = "savepluswithdrawalstatus";
    public static final String KEY_SAVEPLUSBALANCE = "saveplusbalance";
    public static final String KEY_SAVEPLUSMINBALANCE = "saveplusminbalance";
    public static final String KEY_LANGUAGE = "language";
    public static final String KEY_HOST_URL = "xmpphostUrl";
    public static final String KEY_HOST_NAME = "xmpphostName";
    public static final String KEY_ID_NAME = "Id_Name";
    public static final String KEY_TIMER_AVAILABLE = "available";
    public static final String KEY_PHONE_MASKING_STATUS = "no";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_REFERAL_STATUS = "referalstatus";
    public static final String COMPLAINT_U_NAME = "username1";
    public static final String COMPLAINT_U_IMAGE = "umiamge";
    public static final String COMPLAINT_U_LOCATION = "ulocation";

    public static final String BASIC_PROFILE = "basicprofile";
    public static final String COMPLETE_PROFILE = "completeprofile";

    public static final String FREE_WAITING_TIME = "free_wating_time";
    public static final String FREE_WAITING_RATE = "free_waoiting_rate";

    public static final String SELECTED_CAR_IMAGE = "selected_car_image";
    public static final String CURRENCY = "currency_code";
    public static final String XenditKey1 = "XenditKey";
    public static final String currencyconversion = "currencyconversion";
    public static final String reloadtype = "reloadtype";
    public static final String redeemCode = "redeemcode";

    public static final String KEY_WAITED_TIME = "waited_time";
    public static final String KEY_WAITED_MINS = "waited_mins";
    public static final String KEY_WAITED_SECS = "waited_secs";
    public static final String KEY_CALENDAR_TIME = "calendar_time";

    public static final String KEY_CAR_CAT_IAMGE = "carcat_image";
    public static final String KEY_IS_FIRST_TIME = "firsttime";
    public static final String KEY_IS_INFO_BADGE = "infobadge";
    public static final String KEY_MAXPERSON = "maxperson";
    public static final String KEY_IS_USERGUIDE = "userguide";

    public static final String KEY_IS_REFERALSTATUS = "refalstatus";

    public static final String KEY_IS_REFERAL_STATUS = "key_is_referal_status";
    public static final String KEY_IS_REFERAL_PHONE = "key_is_referal_phone";
    public static final String KEY_IS_REFERAL_COUNTRY_CODE = "key_is_referal_country_code";
    public static final String KEY_SECURE_PIN = "secure_pin";
    public static final String KEY_CASH_BACK = "cashback";
    public static final String KEY_IS_CARIMAGE = "carimage";
    public static final String KEY_IS_ONLINECAR = "caravailable";
    public static final String KEY_IS_FREEWAITINGTIME = "freewaitingtime";
    public static final String KEY_IS_DRIVERPROFILE = "driverporfileview";
    public static String REDEEM_CODE = "";

    public static final String KEY_REG_OTP_STATUS = "otp_status";
    public static final String KEY_REG_OTP_PIN = "otp_pin";
    public static final String KEY_ISNIGHTMODE = "isnightmode";
    public static final String KEY_PINCODE = "keypincode";



    public static final String KEY_DRIVER_PHONENUMBER = "driverphonenumber";
    public static final String KEY_XENDITCHARGESTATUS = "xenditchargestatus";
    public static final String KEY_CONTINUE_TRIP = "continuetrip";
    public static final String ComplementStatus = "complementstatus";
    public static final String KEY_EMER_POLICE = "Police";
    public static final String KEY_EMER_FIRE = "Fire";
    public static final String KEY_EMER_AMBULANCE = "Ambulance";
    public static final String KEY_CUSTOMERSERVICE = "CustomerServiceNo";
    public static final String KEY_PHANTOMCAR = "Phantomcar";
    public static final String KEY_BOOKINGPROGRESS = "Bookingreload";
    public static final String KEY_CURRENTBOOKING = "CurrentBooking";
    public static final String KEY_FAREBREAKUP_RIDEID = "rideidfarebreakup";
    public static final String userlogin = "userlogin";
    public static final String driversjson = "driverjson";
    public static final String usercurrency = "usercurrency";
    public static final String userwatingcharge = "userwaitingcharge";
    public static final String KEY_PAYMENTS = "paymentslist";
    public static final String JOB_ID = "demojob";
    public static final String KEY_USER_GUIDE = "user_guide";
    public static final String KEY_PAYMENTBANKCODE = "paymentbankcode";
    public static final String KEY_IS_VEHICLEDEAILS = "vehicledetailsinfo";
    public static final String KEY_ACCOUNTNAME = "accountname";
    public static final String KEY_ACCOUNTNUMBER = "accountnumber";
    public static final String KEY_ACCOUNTCODE = "accountcode";
    public static final String KEY_ACCOUNTBANK = "accountbank";
    public static final String KEY_DRIVER_REVIEW = "review";
    public static Map<String, List<String>> carList;
    public static boolean driverStatus;
    public static SessionManager instance = null;

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static SessionManager getInstance(Context context) {
        if (instance == null) {
            instance = new SessionManager(context);
        }
        return instance;
    }



    /**
     * Create login session
     */
    public void createLoginSession(String email, String userid, String username, String userimage, String countrycode, String phoneno, String referalcode, String category, String gcmID) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_USERID, userid);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_USERIMAGE, userimage);
        editor.putString(KEY_COUNTRYCODE, countrycode);
        editor.putString(KEY_PHONENO, phoneno);
        editor.putString(KEY_REFERAL_CODE, referalcode);
        editor.putString(KEY_CATEGORY, category);
        editor.putString(KEY_GCM_ID, gcmID);
        // commit changes
        editor.commit();
    }

    public void setCategoryID(String categoryID) {
        editor.putString(KEY_CATEGORY, categoryID);
        editor.commit();
    }


    public void setModeupdate(String modeupdate) {
        editor.putString(KEYMODEUPDATE, modeupdate);
        editor.commit();
    }

    public void setCurrentlanguage(String lan) {
        editor.putString(KEYCURRENTLANGUAGE, lan);
        editor.commit();
    }

    public String getCurrentlanguage() {
        return pref.getString(KEYCURRENTLANGUAGE, "en");
    }

    public String getModeupdate() {
        return pref.getString(KEYMODEUPDATE, "");
    }

    public void setUserGuide(String userGuide) {
        editor.putString(KEY_USER_GUIDE, userGuide);
        editor.commit();
    }

    public String getUserGuide() {
        return pref.getString(KEY_USER_GUIDE, "");
    }

    public void setcashlocationlist(String currency) {
        editor.putString(KEY_CASHLOCTIONSLIST, currency);
        editor.commit();
    }

    public String getcashlocationlist() {
        String currency = "";
        currency = pref.getString(KEY_CASHLOCTIONSLIST, "");
        return currency;
    }


    public void setbankdetails(String accountname, String accountnumber, String accountcode,String bankname) {
        editor.putString(KEY_ACCOUNTNAME, accountname);
        editor.putString(KEY_ACCOUNTNUMBER, accountnumber);
        editor.putString(KEY_ACCOUNTCODE, accountcode);
        editor.putString(KEY_ACCOUNTBANK, bankname);
        editor.commit();
    }

    public HashMap<String, String> getbankdetails() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_ACCOUNTNAME, pref.getString(KEY_ACCOUNTNAME, ""));
        code.put(KEY_ACCOUNTNUMBER, pref.getString(KEY_ACCOUNTNUMBER, ""));
        code.put(KEY_ACCOUNTCODE, pref.getString(KEY_ACCOUNTCODE, ""));
        code.put(KEY_ACCOUNTBANK, pref.getString(KEY_ACCOUNTBANK, ""));
        return code;
    }

    public void setAutoCharge(String status) {
        editor.putString(KEY_AUTOCHARE, status);
        editor.commit();
    }


    public void createWalletAmount(String amount) {
        editor.putString(KEY_WALLET_AMOUNT, amount);
        // commit changes
        editor.commit();
    }



    public void setvehicleinfo(String vehicle) {
        editor.putString(KEY_IS_VEHICLEDEAILS, vehicle);
        editor.commit();
    }

    public String getvehicleinfo() {
        String badge = "";
        badge = pref.getString(KEY_IS_VEHICLEDEAILS, "");
        return badge;
    }

    //------username update code-----
    public void setUserNameUpdate(String name) {
        editor.putString(KEY_USERNAME, name);
        editor.commit();
    }

    //------MobileNumber update code-----
    public void setMobileNumberUpdate(String code, String mobile) {
        editor.putString(KEY_COUNTRYCODE, code);
        editor.putString(KEY_PHONENO, mobile);
        editor.commit();
    }

    //------string user coupon code-----
    public void setCouponCode(String code, String source) {
        editor.putString(KEY_COUPON_CODE, code);
        editor.putString(KEY_COUPON_CODE_SOURCE, source);
        editor.commit();
    }

    public void setVehicle_BitmapImage(String count) {
        editor.putString(KEY_VEHICLE_BitMap_IMAGE, count);
        editor.commit();
    }

    public HashMap<String, String> getVehicleBitmap() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_VEHICLE_BitMap_IMAGE, pref.getString(KEY_VEHICLE_BitMap_IMAGE, ""));
        return catID;
    }

    //------string user complaint detail code-----
    public void setcomplaintUserDetail(String unamr, String uimage, String userlocation) {
        editor.putString(COMPLAINT_U_NAME, unamr);
        editor.putString(COMPLAINT_U_IMAGE, uimage);
        editor.putString(COMPLAINT_U_LOCATION, userlocation);
        editor.commit();
    }

    public void setfirstimelogin(String vehicleType) {
        editor.putString(KEY_FIRSTTIMELOGIN, vehicleType);
        editor.commit();
    }

    public String getfirstimelogin() {
        return pref.getString(KEY_FIRSTTIMELOGIN, "");
    }


    public void setPolice(String vehicleType) {
        editor.putString(KEY_EMER_POLICE, vehicleType);
        editor.commit();
    }

    public String getPolice() {
        return pref.getString(KEY_EMER_POLICE, "");
    }


    //farebreakup
    public void setFarebreakup_rideId(String rideid) {
        editor.putString(KEY_FAREBREAKUP_RIDEID, rideid);
        editor.commit();
    }

    public String getFarebreakup_rideId() {
        return pref.getString(KEY_FAREBREAKUP_RIDEID, "");
    }

    public void setpaymentcode(String pin) {
        editor.putString(KEY_PAYMENTBANKCODE, pin);
        editor.commit();
    }

    public String getpaymentcode() {
        return pref.getString(KEY_PAYMENTBANKCODE, "");
    }

    //farebreakup
    public void setbusinessprofile(String rideid) {
        editor.putString(KEY_BUSSINESPROFILE, rideid);
        editor.commit();
    }

    public String getbusinessprofile() {
        return pref.getString(KEY_BUSSINESPROFILE, "");
    }


    //farebreakup
    public void setbusinessname(String rideid) {
        editor.putString(KEY_BUSSINESNAME, rideid);
        editor.commit();
    }

    public String getbusinessname() {
        return pref.getString(KEY_BUSSINESNAME, "");
    }


    //farebreakup
    public void setQrcode_path(String rideid) {
        editor.putString(KEY_QRCODEPATH, rideid);
        editor.commit();
    }

    public String getQrcode_path() {
        return pref.getString(KEY_QRCODEPATH, "");
    }


    public void setFire(String vehicleType) {
        editor.putString(KEY_EMER_FIRE, vehicleType);
        editor.commit();
    }

    public String getFire() {
        return pref.getString(KEY_EMER_FIRE, "");
    }

    public void setAmbulance(String vehicleType) {
        editor.putString(KEY_EMER_AMBULANCE, vehicleType);
        editor.commit();
    }

    public String getAmbulance() {
        return pref.getString(KEY_EMER_AMBULANCE, "");
    }



    public void setbookingonProgress(String booking) {
        editor.putString(KEY_BOOKINGPROGRESS, booking);
        editor.commit();
    }

    public String getbookingonProgress() {
        return pref.getString(KEY_BOOKINGPROGRESS, "");
    }



    public void setCurrentBooking(String booking) {
        editor.putString(KEY_CURRENTBOOKING, booking);
        editor.commit();
    }

    public String getCurrentBooking() {
        return pref.getString(KEY_CURRENTBOOKING, "");
    }

    public void setpaymentlist(String currency) {
        editor.putString(KEY_PAYMENTS, currency);
        editor.commit();
    }

    public String getpaymentlist() {
        String currency = "";
        currency = pref.getString(KEY_PAYMENTS, "");
        return currency;
    }



    //-----------Get user complaint detail code-----
    public HashMap<String, String> getcomplaintUserDetail() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(COMPLAINT_U_NAME, pref.getString(COMPLAINT_U_NAME, ""));
        code.put(COMPLAINT_U_IMAGE, pref.getString(COMPLAINT_U_IMAGE, ""));
        code.put(COMPLAINT_U_LOCATION, pref.getString(COMPLAINT_U_LOCATION, ""));
        return code;
    }

    //-----------Get user coupon code-----
    public HashMap<String, String> getCouponCode() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_COUPON_CODE, pref.getString(KEY_COUPON_CODE, ""));
        code.put(KEY_COUPON_CODE_SOURCE, pref.getString(KEY_COUPON_CODE_SOURCE, ""));
        return code;
    }

    //------ Xmpp Connect Secrect Code-----
    public void setXmppKey(String userId, String secretKey) {
        editor.putString(KEY_XMPP_USERID, userId);
        editor.putString(KEY_XMPP_SEC_KEY, secretKey);
        editor.commit();
    }

    //------ set tip time -----
    public void setTipTime(String time) {
        editor.putString(KEY_TIPTIME, time);
        editor.commit();
    }

    //------ Xmpp Connect Secrect Code-----
    public void setCurrency(String watingmin) {
        editor.putString(CURRENCY, watingmin);
        editor.commit();
    }

    public String getCurrency() {
        String currency = "";
        currency = pref.getString(CURRENCY, "");
        return currency;
    }

    public void setRedeemCouponcode(String XenditKey) {
        editor.putString(redeemCode, XenditKey);
        editor.commit();
    }

    public String getRedeemCouponcode() {
        String currency = "";
        currency = pref.getString(redeemCode, "");
        return currency;
    }

    public void setKeyDriverPhonenumber(String XenditKey) {
        editor.putString(KEY_DRIVER_PHONENUMBER, XenditKey);
        editor.commit();
    }

    public String getKeyDriverPhonenumber() {
        String currency = "";
        currency = pref.getString(KEY_DRIVER_PHONENUMBER, "");
        return currency;
    }


    public void setXenditChargeStatus(String XenditKey) {
        editor.putString(KEY_XENDITCHARGESTATUS, XenditKey);
        editor.commit();
    }

    public String getXenditChargeStatus() {
        String currency = "";
        currency = pref.getString(KEY_XENDITCHARGESTATUS, "");
        return currency;
    }



    public void setXenditPublicKey(String XenditKey) {
        editor.putString(XenditKey1, XenditKey);
        editor.commit();
    }

    public String getXenditPublicKey() {
        String currency = "";
        currency = pref.getString(XenditKey1, "");
        return currency;
    }

    public void setcurrencyconversionKey(String XenditKey) {
        editor.putString(currencyconversion, XenditKey);
        editor.commit();
    }

    public String getcurrencyconversionKey() {
        String currency = "";
        currency = pref.getString(currencyconversion, "");
        return currency;
    }

    public void setReloadPaymentType(String type) {
        editor.putString(reloadtype, type);
        editor.commit();
    }

    public String getReloadPaymentType() {
        String type = "";
        type = pref.getString(reloadtype, "");
        return type;
    }

    public void setContinueTrip(String type) {
        editor.putString(KEY_CONTINUE_TRIP, type);
        editor.commit();
    }
    public String getContinueTrip() {
        String type = "";
        type = pref.getString(KEY_CONTINUE_TRIP, "No");
        return type;
    }


    public void setComplementStatus(String type) {
        editor.putString(ComplementStatus, type);
        editor.commit();
    }
    public String getComplementStatus() {
        String type = "";
        type = pref.getString(ComplementStatus, "No");
        return type;
    }


    //------ Xmpp Connect Secrect Code-----
    public void setFreeWaitingTimeRate(String watingmin, String rate) {
        editor.putString(FREE_WAITING_TIME, watingmin);
        editor.putString(FREE_WAITING_RATE, rate);
        editor.commit();
    }

    public void setSelectCatCarImage(String carIMage) {
        editor.putString(SELECTED_CAR_IMAGE, carIMage);
        editor.commit();
    }

    public HashMap<String, String> getSelectedCarCatImage() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(SELECTED_CAR_IMAGE, pref.getString(SELECTED_CAR_IMAGE, ""));
        return catID;
    }

    public void IsFirstTime(Boolean first) {
        editor.putBoolean(KEY_IS_FIRST_TIME, first);
        editor.commit();
    }

    public Boolean getIsFirstTime() {
        Boolean isFirst = false;
        isFirst = pref.getBoolean(KEY_IS_FIRST_TIME, true);
        ;
        return isFirst;
    }

    public void setInfoBadge(String badge) {
        editor.putString(KEY_IS_INFO_BADGE, badge);
        editor.commit();
    }

    public String getInfoBadge() {
        String badge = "";
        badge = pref.getString(KEY_IS_INFO_BADGE, "");
        ;
        return badge;
    }


    //-----------Get CategoryId code-----
    public HashMap<String, String> getFreeWaitingTimeRate() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(FREE_WAITING_TIME, pref.getString(FREE_WAITING_TIME, ""));
        catID.put(FREE_WAITING_RATE, pref.getString(FREE_WAITING_RATE, ""));
        return catID;
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, ""));
        user.put(KEY_USERID, pref.getString(KEY_USERID, ""));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, ""));
        user.put(KEY_USERIMAGE, pref.getString(KEY_USERIMAGE, ""));
        user.put(KEY_COUNTRYCODE, pref.getString(KEY_COUNTRYCODE, ""));
        user.put(KEY_PHONENO, pref.getString(KEY_PHONENO, ""));
        user.put(KEY_REFERAL_CODE, pref.getString(KEY_REFERAL_CODE, ""));
        user.put(KEY_CATEGORY, pref.getString(KEY_CATEGORY, ""));
        user.put(KEY_GCM_ID, pref.getString(KEY_GCM_ID, ""));
        user.put(KEY_XMPP_SEC_KEY, pref.getString(KEY_XMPP_SEC_KEY, ""));
        user.put(KEY_HOMEUSERIMAGE, pref.getString(KEY_HOMEUSERIMAGE, ""));


        return user;
    }

    public void setLatitude(String latitude) {
        editor.putString(LOCATION_LATITUDE, latitude);
        editor.commit();
    }

    public Double getLatitude() {
        double value = 0.0;
        String getlati= pref.getString(LOCATION_LATITUDE, "");
        if(getlati.equalsIgnoreCase("")){
            value = 0.0;
        }else{
            value = Double.parseDouble(getlati);
        }
        return value;
    }


    public void setLongitude(String longitude) {
        editor.putString(LOCATION_LONGITUDE, longitude);
        editor.commit();
    }

    public Double getLongitude() {
        double value = 0.0;
        String getlati= pref.getString(LOCATION_LONGITUDE, "");
        if(getlati.equalsIgnoreCase("")){
            value = 0.0;
        }else{
            value = Double.parseDouble(getlati);
        }
        return value;

    }




    //-----------Get CategoryId code-----
    public HashMap<String, String> getCategoryID() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_CATEGORY_ID, pref.getString(KEY_CATEGORY_ID, ""));
        return catID;
    }


    //-----------Get Autocharge code-----
    public HashMap<String, String> getAutoCharge() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_AUTOCHARE, pref.getString(KEY_AUTOCHARE, ""));
        return catID;
    }


    //-----------Get XMPP Secret Key-----
    public HashMap<String, String> getXmppKey() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_XMPP_USERID, pref.getString(KEY_XMPP_USERID, ""));
        code.put(KEY_XMPP_SEC_KEY, pref.getString(KEY_XMPP_SEC_KEY, ""));
        return code;
    }

    public void setSecurePin(String pin) {
        editor.putString(KEY_SECURE_PIN, pin);
        editor.commit();
    }

    public String getSecurePin() {
        return pref.getString(KEY_SECURE_PIN, "");
    }


    public void setCashbackStatus(String cashbackStatus) {
        editor.putString(KEY_CASH_BACK, cashbackStatus);
        editor.commit();
    }
    public String getCashbackStatus() {
        return pref.getString(KEY_CASH_BACK, "");
    }
    public void setFreeWaitingTime(String pin) {
        editor.putString(KEY_IS_FREEWAITINGTIME, pin);
        editor.commit();
    }

    public void setDriverprofile(String driverprofile) {
        editor.putString(KEY_IS_DRIVERPROFILE, driverprofile);
        editor.commit();
    }

    public String getDriverprofile() {
        return pref.getString(KEY_IS_DRIVERPROFILE, "");
    }


    public String getFreeWaitingTimen() {
        return pref.getString(KEY_IS_FREEWAITINGTIME, "");
    }



    //-----------Get Wallet Amount-----
    public String getWalletAmount() {
        String balance = "";
        balance = pref.getString(KEY_WALLET_AMOUNT, "");
        return balance;
    }
    //-----------Get TipTime-----
    public HashMap<String, String> getTipTime() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_TIPTIME, pref.getString(KEY_TIPTIME, ""));
        return code;
    }

    //------string App Status----
    public void setAppStatus(String state) {
        editor.putString(KEY_APP_STATUS, state);
        editor.commit();
    }

    //-----------Get App Status-----
    public HashMap<String, String> getAppStatus() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_APP_STATUS, pref.getString(KEY_APP_STATUS, ""));
        return code;
    }

    public void setGender(String state) {
        editor.putString(KEY_GENDER, state);
        editor.commit();
    }

    //-----------Get App Status-----
    public HashMap<String, String> getGender() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_GENDER, pref.getString(KEY_GENDER, ""));
        return code;
    }

    //------string facebookId----
    public void setFacebookId(String facebookId) {
        editor.putString(KEY_FACBOOKID, facebookId);
        editor.commit();
    }

    //-----------Get facebookId-----
    public HashMap<String, String> getFacebookId() {
        HashMap<String, String> facebookId = new HashMap<String, String>();
        facebookId.put(KEY_FACBOOKID, pref.getString(KEY_FACBOOKID, ""));
        return facebookId;
    }


    //------string facebookId----
    public void setSiteContactEmail(String sitecontactemail) {
        editor.putString(KEY_SITECONTACTEMAIL, sitecontactemail);
        editor.commit();
    }

    //-----------Get facebookId-----
    public HashMap<String, String> getSiteContactEmail() {
        HashMap<String, String> sitecontactemail = new HashMap<String, String>();
        sitecontactemail.put(KEY_SITECONTACTEMAIL, pref.getString(KEY_SITECONTACTEMAIL, ""));
        return sitecontactemail;
    }

    //------string user coupon code-----
    public void setReferralCode(String code, String message) {
        editor.putString(KEY_REFERAL_CODE, code);
        editor.putString(KEY_REFERAL_CODE_MSG, message);
        editor.commit();
    }

    //-----------Get user coupon code-----
    public HashMap<String, String> getReferralCode() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_REFERAL_CODE, pref.getString(KEY_REFERAL_CODE, ""));
        code.put(KEY_REFERAL_CODE_MSG, pref.getString(KEY_REFERAL_CODE_MSG, ""));

        return code;
    }


    public void setAgent(String userId) {
        editor.putString(KEY_ID_NAME, userId);

        editor.commit();
    }

    public HashMap<String, String> getAgent() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_ID_NAME, pref.getString(KEY_ID_NAME, ""));

        return code;
    }

    public void setPhoneMaskingDetail(String phoneMasking) {
        editor.putString(KEY_PHONE_MASKING_STATUS, phoneMasking);
        editor.commit();
    }

    public void setContactNumber(String phoneMasking) {
        editor.putString(KEY_CONTACT_NUMBER, phoneMasking);
        editor.commit();
    }

    public String getContactNumber() {
        return pref.getString(KEY_CONTACT_NUMBER, "");
    }



    public void setContactEmail(String email) {
        editor.putString(KEY_CONTACT_EMAIL, email);
        editor.commit();
    }

    public String getContactEmail() {
        return pref.getString(KEY_CONTACT_EMAIL, "");
    }

    public void setVehicleType(String vehicleType) {
        editor.putString(KEY_VEHICLE_TYPE, vehicleType);
        editor.commit();
    }

    public String getVehicleType() {
        return pref.getString(KEY_VEHICLE_TYPE, "");
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        setUserloggedIn(false);
        /*// After logout redirect user to Loing Activity
        Intent i = new Intent(_context, FetchingDataActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // Add new Flag to start new Activity
      *//*  i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);*//*
        // Staring Login Activity
        _context.startActivity(i);*/



    }

    public void setpattern(String userId) {
        editor.putString(KEY_PATTERN, userId);
        editor.commit();
    }

    public HashMap<String, String> getpattern() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_PATTERN, pref.getString(KEY_PATTERN, ""));
        return catID;
    }


    public void setsaveplusid(String id) {
        editor.putString(KEY_SAVEPLUSID, id);
        editor.commit();
    }

    public HashMap<String, String> getsaveplusid() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_SAVEPLUSID, pref.getString(KEY_SAVEPLUSID, ""));
        return catID;
    }

    public void setwithdrawal_status(String status) {
        editor.putString(KEY_SAVEPLUSWITHDRAWAL_STATUS, status);
        editor.commit();
    }

    public HashMap<String, String> getwithdrawal_status() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_SAVEPLUSWITHDRAWAL_STATUS, pref.getString(KEY_SAVEPLUSWITHDRAWAL_STATUS, ""));
        return catID;
    }


    public void setsaveplus_balance(String balance) {
        editor.putString(KEY_SAVEPLUSBALANCE, balance);
        editor.commit();
    }

    public HashMap<String, String> getsaveplus_balance() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_SAVEPLUSBALANCE, pref.getString(KEY_SAVEPLUSBALANCE, ""));
        return catID;
    }

    public void setsaveplus_minbalance(String balance) {
        editor.putString(KEY_SAVEPLUSMINBALANCE, balance);
        editor.commit();
    }

    public HashMap<String, String> getsaveplus_minbalance() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_SAVEPLUSMINBALANCE, pref.getString(KEY_SAVEPLUSMINBALANCE, ""));
        return catID;
    }


    public void setlamguage(String userId, String secretKey) {
        editor.putString(KEY_LANGUAGE_CODE, userId);
        editor.putString(KEY_LANGUAGE, secretKey);
        editor.commit();
    }

    public HashMap<String, String> getLanaguage() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_LANGUAGE, pref.getString(KEY_LANGUAGE, ""));
        return catID;
    }

    public void setXmpp(String userId, String secretKey) {
        editor.putString(KEY_HOST_URL, userId);
        editor.putString(KEY_HOST_NAME, secretKey);
        editor.commit();
    }

    public HashMap<String, String> getXmpp() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_HOST_URL, pref.getString(KEY_HOST_URL, ""));
        code.put(KEY_HOST_NAME, pref.getString(KEY_HOST_NAME, ""));
        return code;
    }

    public void setTimerPage(String available) {
        editor.putString(KEY_TIMER_AVAILABLE, available);
        editor.commit();
    }

    public HashMap<String, String> getTimerPage() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_TIMER_AVAILABLE, pref.getString(KEY_TIMER_AVAILABLE, ""));
        return code;
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }


    //------UserImage update code-----
    public void setUserImageUpdate(String image) {
        editor.putString(KEY_USERIMAGE, image);
        editor.commit();
    }
    public void setImagestatus(String st) {
        editor.putString(KEY_IMAGESTATUS, st);
        editor.commit();
    }

    //------UserImage update code-----
    public void setsavevalue(String currency,String saveplusbalance,String saveplusid,String codepath) {
        editor.putString(KEY_currency, currency);
        editor.putString(KEY_saveplusbalance, saveplusbalance);
        editor.putString(KEY_saveplusid, saveplusid);
        editor.putString(KEY_codepath, codepath);
        editor.commit();
    }


    public static final String KEY_Usernamewel = "KEY_Usernamewel";
    public static final String KEY_walletamountwel = "KEY_walletamountwel";
    public static final String KEY_coupocountwel = "KEY_coupocountwel";
    public static final String KEY_totaldiswel = "KEY_totaldiswel";
    public static final String KEY_disunitwel = "KEY_disunitwel";

    //------UserImage update code-----
    public void setwelcomemeessage(String Username,String walletamount,String coupocount,String totaldis,String disunit) {
        editor.putString(KEY_Usernamewel, Username);
        editor.putString(KEY_walletamountwel, walletamount);
        editor.putString(KEY_coupocountwel, coupocount);
        editor.putString(KEY_totaldiswel, totaldis);
        editor.putString(KEY_disunitwel, disunit);
        editor.commit();
    }

    public String getuserwelname() {
        return pref.getString(KEY_Usernamewel, "");
    }
    public String getwwelcomewalletamount() {
        return pref.getString(KEY_walletamountwel, "");
    }
    public String getcouponcountwel() {
        return pref.getString(KEY_coupocountwel, "");
    }
    public String gettotaldiwel() {
        return pref.getString(KEY_totaldiswel, "");
    }
    public String getdistunitwel() {
        return pref.getString(KEY_disunitwel, "");
    }


    public String getssavedcurnecy() {
        return pref.getString(KEY_currency, "");
    }
    public String getsaveplusbalance() {
        return pref.getString(KEY_saveplusbalance, "");
    }
    public String getsaveplusidd() {
        return pref.getString(KEY_saveplusid, "");
    }
    public String getcodepathh() {
        return pref.getString(KEY_codepath, "");
    }

    public void setAmazondetails(String imagestatus,String access_key,String secret_key,String bucket_name,String bucket_url) {
        editor.putString(KEY_IMAGESTATUS, imagestatus);
        editor.putString(KEY_ACCESSKEY, access_key);
        editor.putString(KEY_SECRETKEY, secret_key);
        editor.putString(KEY_BUCKETNAME, bucket_name);
        editor.putString(KEY_BUCKETURL, bucket_url);
        editor.commit();
    }

    public String getimagestatus() {
        return pref.getString(KEY_IMAGESTATUS, "");
    }

    public String getaccesskey() {
        return pref.getString(KEY_ACCESSKEY, "");
    }

    public String getsecretkey() {
        return pref.getString(KEY_SECRETKEY, "");
    }

    public String getbucketname() {
        return pref.getString(KEY_BUCKETNAME, "");
    }

    public String getbucketurl() {
        return pref.getString(KEY_BUCKETURL, "");
    }

    //------UserImage update code-----
    public String getUserImageUpdate() {
        return pref.getString(KEY_USERIMAGE, "");
    }


    //------UserImage update code-----
    public void setaccountcloseamount(String image) {
        editor.putString(KEY_CLOSEAMOUNT, image);
        editor.commit();
    }

    //------UserImage update code-----
    public String getaccountcloseamount() {
        return pref.getString(KEY_CLOSEAMOUNT, "");
    }

    //------UserImage update code-----
    public void setUserImageName(String image) {
        editor.putString(KEY_USERIMAGENAME, image);
        editor.commit();
    }

    //------UserImage update code-----
    public String getUserImageName() {
        return pref.getString(KEY_USERIMAGENAME, "");
    }



    //------UserImage update code-----
    public void setUserHomeAnimaionImageUpdate(String image) {
        editor.putString(KEY_HOMEUSERIMAGE, image);
        editor.commit();
    }

    //------UserImage update code-----
    public void setUserBasicProfile(String image) {
        editor.putString(BASIC_PROFILE, image);
        editor.commit();
    }

    //------UserImage update code-----
    public void setUserCompleteProfile(String image) {
        editor.putString(COMPLETE_PROFILE, image);
        editor.commit();
    }

    public HashMap<String, String> getUserBasicProfile() {
        HashMap<String, String> ststus = new HashMap<String, String>();
        ststus.put(BASIC_PROFILE, pref.getString(BASIC_PROFILE, ""));
        return ststus;
    }

    public HashMap<String, String> getUserCompleteProfile() {
        HashMap<String, String> ststus = new HashMap<String, String>();
        ststus.put(COMPLETE_PROFILE, pref.getString(COMPLETE_PROFILE, ""));
        return ststus;
    }


    public HashMap<String, String> getUserHomeAnimaionImageUpdate() {
        HashMap<String, String> ststus = new HashMap<String, String>();
        ststus.put(KEY_HOMEUSERIMAGE, pref.getString(KEY_HOMEUSERIMAGE, ""));
        return ststus;
    }

//    public HashMap<String, String> getPhoneMaskingStatus() {
//        HashMap<String, String> ststus = new HashMap<String, String>();
//        ststus.put(KEY_PHONE_MASKING_STATUS, pref.getString(KEY_PHONE_MASKING_STATUS, ""));
//        return ststus;
//    }
//
    public String getPhoneMaskingStatus(){
        String masking = "";
        masking = pref.getString(KEY_PHONE_MASKING_STATUS, "");
        return masking;
    }

    public void setPhoneMaskingStatus(String phone_masking_status) {
        editor.putString(KEY_PHONE_MASKING_STATUS, phone_masking_status);
        editor.commit();
    }


    public String getUserGuidePdf() {
        String userGuide = "";
        userGuide = pref.getString(KEY_IS_USERGUIDE, "");
        return userGuide;
    }


    public void setUserGuidePdf(String phone_masking_status) {
        editor.putString(KEY_IS_USERGUIDE, phone_masking_status);
        editor.commit();
    }




    public String getReferelStatus() {
        String userGuide = "";
        userGuide = pref.getString(KEY_IS_REFERALSTATUS, "0");
        return userGuide;
    }


    public void setReferelStatus(String phone_masking_status) {
        editor.putString(KEY_IS_REFERALSTATUS, phone_masking_status);
        editor.commit();
    }

    public void setDriverReview(String name) {
        editor.putString(KEY_DRIVER_REVIEW, name);
        editor.commit();
    }

    public String getDriverReview() {
        return pref.getString(KEY_DRIVER_REVIEW, "0");
    }




    public String getCarImage() {
        String userGuide = "";
        userGuide = pref.getString(KEY_IS_CARIMAGE, "");
        return userGuide;
    }

    public void setCarImage(String phone_masking_status) {
        editor.putString(KEY_IS_CARIMAGE, phone_masking_status);
        editor.commit();
    }


    public HashMap<String, String> getReferalStatus() {
        HashMap<String, String> ststus = new HashMap<String, String>();
        ststus.put(KEY_IS_REFERAL_STATUS, pref.getString(KEY_IS_REFERAL_STATUS, ""));
        ststus.put(KEY_IS_REFERAL_COUNTRY_CODE, pref.getString(KEY_IS_REFERAL_COUNTRY_CODE, ""));
        ststus.put(KEY_IS_REFERAL_PHONE, pref.getString(KEY_IS_REFERAL_PHONE, ""));

        return ststus;
    }

    public void setReferalStatus(String status) {
        editor.putString(KEY_IS_REFERAL_STATUS, status);
        editor.commit();
    }

    //------string user coupon code-----
    public void setEmegencyNumber(String source) {
        editor.putString(KEY_EMERGENCY_NUM, source);
        editor.commit();
    }

    //-----------Get user coupon code-----
    public HashMap<String, String> getEmegencyNumber() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_EMERGENCY_NUM, pref.getString(KEY_EMERGENCY_NUM, ""));
        return code;
    }

    public void isFirstimeBook(String value) {
        editor.putString(KEY_FIRSTTIME_BOOK, value);
        editor.commit();
    }

    public String isFirsttimeBook() {
        String isFirstTime = pref.getString(KEY_FIRSTTIME_BOOK, "");
        return isFirstTime;
    }
    public void setnightMode(boolean value) {
        editor.putBoolean(KEY_ISNIGHTMODE, value);
        editor.commit();
    }

    public boolean getNightMode() {
        boolean isFirstTime = pref.getBoolean(KEY_ISNIGHTMODE, false);
        return isFirstTime;
    }
    public void setWaitedTime(long mins, long secs, String waitedTime, String calendarTime) {
        editor.putString(KEY_WAITED_MINS, String.valueOf(mins));
        editor.putString(KEY_WAITED_SECS, String.valueOf(secs));
        editor.putString(KEY_WAITED_TIME, waitedTime);
        editor.putString(KEY_CALENDAR_TIME, calendarTime);
        editor.commit();
    }

    public HashMap<String, String> getWaitedTime() {
        HashMap<String, String> mapWaitingTime = new HashMap<String, String>();
        mapWaitingTime.put(KEY_WAITED_MINS, pref.getString(KEY_WAITED_MINS, ""));
        mapWaitingTime.put(KEY_WAITED_SECS, pref.getString(KEY_WAITED_SECS, ""));
        mapWaitingTime.put(KEY_WAITED_TIME, pref.getString(KEY_WAITED_TIME, ""));
        mapWaitingTime.put(KEY_CALENDAR_TIME, pref.getString(KEY_CALENDAR_TIME, ""));
        return mapWaitingTime;
    }

    public void setOtpstatusAndPin(String status, String otpPin) {
        editor.putString(KEY_REG_OTP_STATUS, status);
        editor.putString(KEY_REG_OTP_PIN, otpPin);
        editor.commit();
    }
    public String getOtpPin() {
       return pref.getString(KEY_REG_OTP_PIN, "");
    }
    public void SetPincode(String code) {
        editor.putString(KEY_PINCODE, code);
        editor.commit();
    }
    public String getPincode() {
        return pref.getString(KEY_PINCODE, "");
    }
    public HashMap<String, String> getOtpstatusAndPin() {
        HashMap<String, String> otpstatus = new HashMap<String, String>();
        otpstatus.put(KEY_REG_OTP_STATUS, pref.getString(KEY_REG_OTP_STATUS, ""));
        otpstatus.put(KEY_REG_OTP_PIN, pref.getString(KEY_REG_OTP_PIN, ""));
        return otpstatus;
    }

    //------string App Status----
    public void setCarCatImage(String state) {
        editor.putString(KEY_CAR_CAT_IAMGE, state);
        editor.commit();
    }

    //------string App Status----
    public void setMaxperson(String maxperson) {
        editor.putString(KEY_MAXPERSON, maxperson);
        editor.commit();
        editor.apply();
    }

    public String getMaxperson() {
        return pref.getString(KEY_MAXPERSON, "5");
    }

    //-----------Get App Status-----
    public HashMap<String, String> getCarCatImage() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_CAR_CAT_IAMGE, pref.getString(KEY_CAR_CAT_IAMGE, ""));
        return code;
    }

    public void setCustomerServiceNo(String sCustomerServiceNumber) {
        editor.putString(KEY_CUSTOMERSERVICE, sCustomerServiceNumber);
        editor.commit();
    }

    public void setPhantomcar(String sCustomerServiceNumber) {
        editor.putString(KEY_PHANTOMCAR, sCustomerServiceNumber);
        editor.commit();
    }



    public String getPhantomcar() {
        return pref.getString(KEY_PHANTOMCAR, "");
    }


    public String getCustomerServiceNo() {
        return pref.getString(KEY_CUSTOMERSERVICE, "");
    }

    public void setjobid(int job) {
        editor.putInt(JOB_ID, job);
        editor.commit();
    }

    public  int getjobid() {
        return pref.getInt(JOB_ID, 0);

    }

    public void setUserloggedIn(boolean val) {
        editor.putBoolean(userlogin, val);
        editor.commit();
    }

    public boolean getUserloggedIn() {
        return pref.getBoolean(userlogin, false);
    }

    public void setDriverslist(String json) {
        editor.putString(driversjson, json);
        editor.commit();
    }

    public String getDriverslist() {
        return pref.getString(driversjson, "");
    }

    public void setUsercurrency_and_waitingcharge_perminute(String currency,String waitingchanrge) {
        editor.putString(usercurrency, currency);
        editor.putString(userwatingcharge, waitingchanrge);
        editor.commit();
    }


    public HashMap<String, String> getUsercurrency_and_waitingcharge_perminute() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(usercurrency, pref.getString(usercurrency, ""));
        code.put(userwatingcharge, pref.getString(userwatingcharge, ""));
        return code;
    }
}
