package com.blackmaria.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.blackmaria.InterFace.CallBack;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Muruganantham on 1/23/2018.
 */

public class LocationAddress {
    private static   CallBack callBack;
    private static final String TAG = "LocationAddress";

    public static String getAddressFromLocation(final double latitude, final double longitude,
                                                final Context context, final CallBack callBacks) {
         callBack= callBacks;
        final String[] result = {null};
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());

                try {
                    List<Address> addressList = geocoder.getFromLocation(
                            latitude, longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                            sb.append(address.getAddressLine(i)).append("\n");
                        }
                        sb.append(address.getLocality()).append("\n");
                        sb.append(address.getPostalCode()).append("\n");
                        sb.append(address.getCountryName());
                        result[0] = sb.toString();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Unable connect to Geocoder", e);
                } finally {
                    if (result[0] != null) {
                        result[0] = result[0];
                    } else {
                        result[0] = "Unable to get address for this ";
                    }
                    callBack.onCompleteAddress(result[0]);
                }
            }
        };
        thread.start();
        return result[0];
    }
}
