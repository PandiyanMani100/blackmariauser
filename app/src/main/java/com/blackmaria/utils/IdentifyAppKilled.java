package com.blackmaria.utils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.blackmaria.BookingPage1;
import com.blackmaria.CloudMoneyHomePage;
import com.blackmaria.FareBreakUp;
import com.blackmaria.FavoriteList;
import com.blackmaria.Navigation_new;
import com.blackmaria.NewProfile_Page;
import com.blackmaria.RideDetailPage;
import com.blackmaria.RideList;
import com.blackmaria.RideList1;
import com.blackmaria.TrackRideAcceptPage;
import com.blackmaria.TrackRidePage;
import com.blackmaria.WaitingTimePage;
import com.blackmaria.WalletMoneyPage1;
import com.blackmaria.xmpp.Getlocation;
import com.blackmaria.xmpp.XmppService;


/**
 * Created by Prem Kumar and Anitha on 1/25/2016.
 */
public class IdentifyAppKilled extends Service {


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        try{
//            if(Fragment_HomePage.fragment_homePage_class != null){
//                Fragment_HomePage.fragment_homePage_class.finish();
//            }}catch (Exception e){}
        finishallActivies();
        stopService(new Intent(getApplicationContext(), Getlocation.class));
        stopService(new Intent(getApplicationContext(), XmppService.class));
        stopService(new Intent(getApplicationContext(), Locationservices.class));
//        SessionManager sessionManager = new SessionManager(this);
//        JobManager.instance().cancel(sessionManager.getjobid());
//        DemoSyncJob.scheduleJob1();
//        DemoSyncJob.scheduleJob();

//        Intent intent = new Intent(IdentifyAppKilled.this, SplashPage1.class);
//        intent.putExtra("frommylifecyle", "finished");
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
    }

    private void finishallActivies() {

        try {
            if (Navigation_new.activity != null) {
                Navigation_new.activity.finish();
            }
        }catch (Exception e){}
        try {

            if(TrackRidePage.trackyour_ride_class != null){
                TrackRidePage.trackyour_ride_class.finish();
            }
        }catch (Exception e){}

        try{
            if(TrackRideAcceptPage.trackRideAcceptPage_class != null){
                TrackRideAcceptPage.trackRideAcceptPage_class.finish();
            }
        }catch (Exception e){}

        try{
            if(NewProfile_Page.newProfile_page != null){
                NewProfile_Page.newProfile_page.finish();
            }
        }catch (Exception e){}

        try{
            if(FareBreakUp.farebreakup_class != null){
                FareBreakUp.farebreakup_class.finish();
            }}catch (Exception e){}

        try{
            if(WaitingTimePage.activity != null){
                WaitingTimePage.activity.finish();
            }}catch (Exception e){}

        try{
            if(WalletMoneyPage1.WalletMoneyPage1_class != null){
                WalletMoneyPage1.WalletMoneyPage1_class.finish();
            }}catch (Exception e){}

//        try{
//            if(BookingPage2.BookingPage2_class != null){
//                BookingPage2.BookingPage2_class.finish();
//            }}catch (Exception e){}

        try{
            if(RideDetailPage.rideDetailPage_class != null){
                RideDetailPage.rideDetailPage_class.finish();
            }}catch (Exception e){}

        try{
            if(RideList1.rideList_class != null){
                RideList1.rideList_class.finish();
            }}catch (Exception e){}

        try{
            if(RideList.rideList_class != null){
                RideList.rideList_class.finish();
            }}catch (Exception e){}



//        try{
//            if(BookingPage3.BookingPage3_class != null){
//                BookingPage3.BookingPage3_class.finish();
//            }}catch (Exception e){}

        try{
            if(FavoriteList.favoriteList_class != null){
                FavoriteList.favoriteList_class.finish();
            }}catch (Exception e){}

        try{
            if(BookingPage1.activity != null){
                BookingPage1.activity.finish();
            }}catch (Exception e){}

        try{
            if(CloudMoneyHomePage.cloudmoneyhomepage != null){
                CloudMoneyHomePage.cloudmoneyhomepage.finish();
            }}catch (Exception e){}


    }

    public void onTaskRemoved(Intent rootIntent) {
        stopSelf();
    }
}