package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.ComplaintsListAdaptere;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.ComplaintsListPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.TextRCBold;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Muruganantham on 3/8/2018.
 */

@SuppressLint("Registered")
public class ComplaintListPage extends ActivityHockeyApp{
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    LanguageDb mhelper;
    private SessionManager session;

    private Button mailesRewardsBack;
    private RelativeLayout complaintvalue;
    private TextView valueReporttext;
    private TextView complaintText;
    private ListView rewardlistview;

    ArrayList<ComplaintsListPojo> ComplaintsList = null;
    private boolean isComplaintsAvailable;

    private String UserID = "";
    private String complaintsType = "", cmpCounts = "";
    private RefreshReceiver refreshReceiver;
    private String complainttype = "";
    ComplaintsListAdaptere adapetr;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(ComplaintListPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(ComplaintListPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            } else if (intent.getAction().equals("com.package.ACTION_CLASS_complaint_replay_notification")) {
                if (intent.getExtras() != null) {
                    String ticketId = (String) intent.getExtras().get("ticket_id");
                    String userID = (String) intent.getExtras().get("user_id");
                    String message = (String) intent.getExtras().get("message");
                    String dateTime = (String) intent.getExtras().get("date_time");

                    Intent intent1 = new Intent(ComplaintListPage.this, ComplaintPageViewDetailPage.class);
                    intent1.putExtra("ticket_id", ticketId);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
            else if(intent.getAction().equals("com.package.ACTION_CLASS_complaint_refreshhomePageComplaintlist")){
                PostRequest();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complaint_listpage);
        mhelper = new LanguageDb(this);
        initialize();
        GotoDetailPage();

    }


    private void initialize() {
        cd = new ConnectionDetector(ComplaintListPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(ComplaintListPage.this);

        ComplaintsList = new ArrayList<>();

        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);


        mailesRewardsBack = (Button) findViewById(R.id.mailes_rewards_back);
        complaintvalue = (RelativeLayout) findViewById(R.id.complaintvalue);
        valueReporttext = (TextView) findViewById(R.id.value_reporttext);
        complaintText = (TextView) findViewById(R.id.complaint_text);
        rewardlistview = (ListView) findViewById(R.id.rewardlistview);

        mailesRewardsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.package.ACTION_CLASS_complaint_refreshhomePageComplaintlist");
        registerReceiver(refreshReceiver, intentFilter);

        Intent in = getIntent();
        complaintsType = in.getStringExtra("typeOfCase");
        cmpCounts = in.getStringExtra("cmpCounts");
        complaintText.setText(complaintsType);
        valueReporttext.setText(cmpCounts);


        if (complaintsType.equalsIgnoreCase("COMPLAINTS")) {
            complainttype = "all";
        } else {
            complainttype = complaintsType.toLowerCase();
        }
        PostRequest();

    }

    private void PostRequest() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);//"584aa27ccae2aa741a00002f");
        jsonParams.put("type", complainttype);

        System.out.println("-------------ComplaintsList jsonParams----------------" + jsonParams);
        if (isInternetPresent) {
            postRequest_ComplaintsList(Iconstant.complaint_list, jsonParams);
        } else {
            Alert(getkey("action_error"),getkey("alert_nointernet"));
        }

    }

    //-----------------------viewTicket Post Request-----------------
    private void postRequest_ComplaintsList(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(ComplaintListPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));
        System.out.println("-------------ComplaintsList Url----------------" + Url);


        System.out.println("-------------ComplaintsList jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(ComplaintListPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------ComplaintsList Response----------------" + response);
                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject resObj = object.getJSONObject("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Object check_object1 = resObj.get("ticket_list");
                        if (check_object1 instanceof JSONArray) {
                            JSONArray ticketArray = resObj.getJSONArray("ticket_list");
                            if (ticketArray.length() > 0) {
                                ComplaintsList.clear();
                                for (int i = 0; i < ticketArray.length(); i++) {
                                    JSONObject complaintList = ticketArray.getJSONObject(i);
                                    ComplaintsListPojo complaintsPojo = new ComplaintsListPojo();
                                    complaintsPojo.setSubName(complaintList.getString("subject"));
                                    complaintsPojo.setTickNumer(complaintList.getString("ticket_number"));
                                    complaintsPojo.setTicketStatus(complaintList.getString("ticket_status"));
                                    if (complaintList.has("ticket_date")) {
                                        complaintsPojo.setDateTime("Date " + complaintList.getString("ticket_date"));
                                    } else {
                                        complaintsPojo.setDateTime("");
                                    }
                                    ComplaintsList.add(complaintsPojo);
                                }
                                isComplaintsAvailable = true;
                            } else {
                                isComplaintsAvailable = false;
                            }

                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {
                        adapetr = new ComplaintsListAdaptere(ComplaintListPage.this, ComplaintsList, complaintsType);
                        rewardlistview.setAdapter(adapetr);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void GotoDetailPage() {
        rewardlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (ComplaintsList.get(position).getTicketStatus().equalsIgnoreCase("open")) {
                    Intent intent1 = new Intent(ComplaintListPage.this, ComplaintPageViewDetailPage.class);
                    intent1.putExtra("ticket_id", ComplaintsList.get(position).getTickNumer());
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(ComplaintListPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
