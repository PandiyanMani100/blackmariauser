package com.blackmaria;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.MyRideCancelTripAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.CancelTripPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user14 on 12/22/2016.
 */
public class MyRideCancelTrip extends ActivityHockeyApp {
    private RelativeLayout back;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private static Context context;
    private SessionManager session;
    private String UserID = "";

    private ServiceRequest mRequest;
    Dialog dialog;
    ArrayList<CancelTripPojo> itemlist;
    MyRideCancelTripAdapter adapter;
    private ExpandableHeightListView listview;
    private String SrideId_intent = "", driverImage = "", sShare_ride_status = "", sShare_pool_ride_id = "";
    private RelativeLayout home_button_layout,cancelimage;
    private String cancelReason = "";
    private RelativeLayout cancel_trackme_layout;
    private RoundedImageView cancel_tripDriverImage;
    private  ImageView blinking_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myride_cancel_trip);
        context = getApplicationContext();
        initialize();

        cancel_trackme_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(MyRideCancelTrip.this, TrackRidePage.class);
                intent1.putExtra("ride_id", SrideId_intent);
                startActivity(intent1);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
            }
        });

        cancelimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(MyRideCancelTrip.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    if (!cancelReason.equalsIgnoreCase("")) {
                        cancel_MyRide(Iconstant.cancel_myride_url, cancelReason);
                    } else {
                        Alert(getResources().getString(R.string.alert), getResources().getString(R.string.alert_label));
                    }
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }
            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cancelReason = itemlist.get(position).getReasonId();
                for (int i = 0; i < itemlist.size(); i++) {
                    if (i == position) {
                        itemlist.get(i).setSetPayment_selected_cancel_id("true");
                    } else {
                        itemlist.get(i).setSetPayment_selected_cancel_id("false");
                    }
                }
                cd = new ConnectionDetector(MyRideCancelTrip.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    if (!cancelReason.equalsIgnoreCase("")) {
                        cancel_MyRide(Iconstant.cancel_myride_url, cancelReason);
                    } else {
                        Alert(getResources().getString(R.string.alert), getResources().getString(R.string.alert_label));
                    }
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }
                adapter.notifyDataSetChanged();
            }
        });
        home_button_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(MyRideCancelTrip.this,Navigation_new.class);
                startActivity(in);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
    }

    private void initialize() {
        session = new SessionManager(MyRideCancelTrip.this);
        cd = new ConnectionDetector(MyRideCancelTrip.this);
        isInternetPresent = cd.isConnectingToInternet();

        cancel_trackme_layout = (RelativeLayout) findViewById(R.id.cancel_trip_trackme_layout);
        listview = (ExpandableHeightListView) findViewById(R.id.my_rides_cancel_trip_listView);
        cancelimage = (RelativeLayout) findViewById(R.id.cancelimage_layout);
        blinking_image= (ImageView) findViewById(R.id.blinking_image);
        home_button_layout= (RelativeLayout) findViewById(R.id.home_button_layout);

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        blinking_image= (ImageView) findViewById(R.id.blinking_image);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        blinking_image.startAnimation(animation);

        Intent intent = getIntent();
        SrideId_intent = intent.getStringExtra("RideID");
        if (intent.hasExtra("driverImage")) {
            driverImage = intent.getStringExtra("driverImage");
        }
        if (!driverImage.equalsIgnoreCase("")) {
            Picasso.with(getApplicationContext()).load(String.valueOf(driverImage)).into(cancel_tripDriverImage);
        }
        try {
            Bundle bundleObject = getIntent().getExtras();
            itemlist = (ArrayList<CancelTripPojo>) bundleObject.getSerializable("Reason");
            adapter = new MyRideCancelTripAdapter(MyRideCancelTrip.this, itemlist);
            listview.setAdapter(adapter);
            listview.setExpanded(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(MyRideCancelTrip.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-----------------------Cancel Myride Post Request-----------------
    private void cancel_MyRide(String Url, final String reasonId) {
        dialog = new Dialog(MyRideCancelTrip.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.my_rides_cancel_trip_action_cancel));


        System.out.println("-------------Cancel Myride Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", SrideId_intent);
        jsonParams.put("reason", reasonId);
        System.out.println("-------------Cancel Myride jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(MyRideCancelTrip.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Cancel Myride Response----------------" + response);

                String Sstatus = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            String message = response_object.getString("message");


                            final PkDialog mDialog = new PkDialog(MyRideCancelTrip.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.action_success));
                            mDialog.setDialogMessage(message);
                            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                    RideDetailPage.rideDetailPage_class.finish();
                                    RideList.rideList_class.finish();
                                    Intent intent = new Intent(MyRideCancelTrip.this, Navigation_new.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            mDialog.show();

                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

