package com.blackmaria;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;
import com.xendit.Models.Card;
import com.xendit.Models.Token;
import com.xendit.Models.XenditError;
import com.xendit.TokenCallback;
import com.xendit.Xendit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by user144 on 9/18/2017.
 */
public class NewCloudMoneyReloadCardDetailPage extends ActivityHockeyApp {
    EditText Edt1, Edt2, Edt3, Edt4, CardHolderName, EdtCvv;
    Spinner spinner_month, spinner_year;
    private RelativeLayout rlPayNow, cardCancelLayout, reset;
    private String expYear = "", expMonth = "";
    private int expYearNum = 0, expMnthNUm = 0;
    private RefreshReceiver refreshReceiver;
    private String cardNumber = "", insertAmount = "", insertAmountPay = "";
    private CustomTextView dueAmount;
    private Boolean isInternetPresent = false;
    private String currency = "";
    private SessionManager session;
    private String authenticationId = "";
    //Arockiasamy anna xendit account
    public static String PUBLISHABLE_KEY = "";//xnd_public_development_P46DfOV207L6lJM4fLAbTjOTZNalp9d5kiG1+Rxj+GTU+7WkCQVwjg== "xnd_public_development_O4uGfOR3gbOunJU4frcaHmLCYNLy8oQuknDm+R1r9G3S/b2lBQR+gQ==";
    public static String CURRENCYCONVERSIONKEY_KEY = "";
    SmoothProgressBar loadingProgressbar;
    Xendit xendit = null;
    ArrayAdapter<String> yearAdapter = null;
    ArrayAdapter<CharSequence> monthAdapter = null;
    Double amount = 0.0;
    private ImageView img_back;
    private String fromBookingpage ="";

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadCardDetailPage.this, Navigation_new.class);
                        startActivity(intent1);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadCardDetailPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadCardDetailPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//       setContentView(R.layout.new_wallet_money_page);
        setContentView(R.layout.new_cloud_money_reload_card_enterpage_new);
        initialzation();
        spinner_month_and_year_process();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        cardCancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        Edt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (Edt1.getText().toString().length() == 4) {

                    Edt2.requestFocus();
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        Edt2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Edt2.getText().toString().length() == 4) {
                    Edt3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        Edt3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Edt3.getText().toString().length() == 4) {
                    Edt4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        Edt4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (Edt3.getText().toString().length() == 4) {

                    Edt4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        rlPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int thisYear = Calendar.getInstance().get(Calendar.YEAR);
                int thismnth = Calendar.getInstance().get(Calendar.MONTH);
                cardNumber = Edt1.getText().toString() + Edt2.getText().toString() + Edt3.getText().toString() + Edt4.getText().toString();
                if (cardNumber.length() == 0) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no));

                    //erroredit(Edt1, getResources().getString(R.string.valid_card_no));
                } else if (cardNumber.length() < 16) {
                    if (Edt1.getText().toString().length() < 4) {
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no_enter));

//                        erroredit(Edt1, getResources().getString(R.string.valid_card_no_enter));
                    } else if (Edt2.getText().toString().length() < 4) {
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no_enter));

//                        erroredit(Edt2, getResources().getString(R.string.valid_card_no_enter));
                    } else if (Edt3.getText().toString().length() < 4) {
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no_enter));

                        erroredit(Edt3, getResources().getString(R.string.valid_card_no_enter));
                    } else if (Edt4.getText().toString().length() < 4) {
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no_enter));

                        // erroredit(Edt4, getResources().getString(R.string.valid_card_no_enter));
                    }
                } else if (CardHolderName.getText().toString().length() <= 0) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_holder_name));

                    // erroredit(EdtCvv, getResources().getString(R.string.valid_card_holder_name));
                } else if (EdtCvv.getText().toString().length() < 3) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_cvv_code));
                    //erroredit(EdtCvv, getResources().getString(R.string.valid_cvv_code));
                } else if (expMnthNUm < thismnth && expYearNum == thisYear) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_validexp_mth));
                } else if (expYearNum < thisYear) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_exp_yrr));

                } else if (expMonth.length() < 0 || expYear.length() < 0) {

                } else {
                    loadingProgressbar.setVisibility(View.VISIBLE);
                    CreateXenditToken();
                }
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edt1.setText("");
                Edt2.setText("");
                Edt3.setText("");
                Edt4.setText("");
                EdtCvv.setText("");

                CardHolderName.setText("");
                yearAdapter.notifyDataSetChanged();
                monthAdapter.notifyDataSetChanged();
            }
        });
    }

    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewCloudMoneyReloadCardDetailPage.this, R.anim.shake);
        editname.startAnimation(shake);
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(NewCloudMoneyReloadCardDetailPage.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    private void initialzation() {

        session = new SessionManager(NewCloudMoneyReloadCardDetailPage.this);
        PUBLISHABLE_KEY = session.getXenditPublicKey();
        CURRENCYCONVERSIONKEY_KEY = session.getcurrencyconversionKey();

        img_back = (ImageView) findViewById(R.id.img_back);
        System.out.println("=========CURRENCYCONVERSIONKEY_KEY==========" + CURRENCYCONVERSIONKEY_KEY);
        spinner_month = (Spinner) findViewById(R.id.spinner2);
        spinner_year = (Spinner) findViewById(R.id.spinner3);
        Edt1 = (EditText) findViewById(R.id.card_number_edt1);
        Edt2 = (EditText) findViewById(R.id.card_number_edt2);
        Edt3 = (EditText) findViewById(R.id.card_number_edt3);
        Edt4 = (EditText) findViewById(R.id.card_number_edt4);
        reset = (RelativeLayout) findViewById(R.id.reset);
        rlPayNow = (RelativeLayout) findViewById(R.id.paynow_rl);
        CardHolderName = (EditText) findViewById(R.id.card_holder_name);
        EdtCvv = (EditText) findViewById(R.id.cvv_edt);
        dueAmount = (CustomTextView) findViewById(R.id.tv_due_amount);
        cardCancelLayout = (RelativeLayout) findViewById(R.id.card_cancel_layout);

        loadingProgressbar = (SmoothProgressBar) findViewById(R.id.timerpage_loading_progressbar);

// -----code to refresh drawer using broadcast receiver-----
        currency = session.getCurrency();
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        insertAmount = getIntent().getStringExtra("insertAmount");
        insertAmountPay = getIntent().getStringExtra("insertAmountPay");
        dueAmount.setText(getResources().getString(R.string.reload_amount_due) + currency + insertAmountPay);

        if (getIntent().hasExtra("frombookingpage")) {
            fromBookingpage = getIntent().getStringExtra("frombookingpage");
        }

    }


    private void spinner_month_and_year_process() {


        String[] month = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        monthAdapter = new ArrayAdapter<CharSequence>(NewCloudMoneyReloadCardDetailPage.this, R.layout.spinner_text, month);
        monthAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        spinner_month.setAdapter(monthAdapter);

        String thisMonth = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);


        if (thisMonth.length() == 1) {
            thisMonth = "0" + thisMonth;
        }


        for (int j = 0; j <= month.length; j++) {

            if (month[j].equalsIgnoreCase(thisMonth)) {
                spinner_month.setSelection(j);
                break;

            }

        }
        spinner_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                expMonth = parent.getSelectedItem().toString();
                expMnthNUm = Integer.parseInt(expMonth);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);


        for (int i = thisYear; i <= thisYear + 40; i++) {
            years.add(Integer.toString(i));
        }
        yearAdapter = new ArrayAdapter<String>(NewCloudMoneyReloadCardDetailPage.this, R.layout.spinner_text, years);
        yearAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        spinner_year.setAdapter(yearAdapter);
        spinner_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                expYear = parent.getSelectedItem().toString();
                expYearNum = Integer.parseInt(expYear);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyReloadCardDetailPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }

    private void CreateXenditToken() {
        xendit = new Xendit(getApplicationContext(), PUBLISHABLE_KEY);

        final boolean isMultipleUse = true;
        boolean shouldAuthenticate = true;

        Card card = new Card(cardNumber,
                expMonth,
                expYear,
                EdtCvv.getText().toString());

        TokenCallback callback = new TokenCallback() {
            @Override
            public void onSuccess(Token token) {
                Double amountf = Double.parseDouble(insertAmountPay) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
                Double amountfloat = (Double) Math.ceil(amountf);

                NumberFormat formatter = new DecimalFormat("#0.00");
                String amount = formatter.format(amountfloat);

                if (token.getStatus().equalsIgnoreCase("VERIFIED")  /*&& isMultipleUse == false*/) {
                    System.out.println("=======murugan  payment TokenId=========" + token.getId());
                    loadingProgressbar.setVisibility(View.GONE);
                    Intent intent = new Intent(NewCloudMoneyReloadCardDetailPage.this, NewCloudMoneyTransferUrlProgress.class);
                    intent.putExtra("total_amount", insertAmount);
                    intent.putExtra("pay_amount", amount + "");
                    intent.putExtra("authentication_id", ""/* authentication.getId()*/);
                    intent.putExtra("token_id", token.getId());
                    intent.putExtra("flag", "2");
                    if(fromBookingpage.equalsIgnoreCase("1")){
                        intent.putExtra("fromBookingpage", "1");
                    }
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                    //createAuthenticationId(token.getId());
                }
            }

            @Override
            public void onError(XenditError xenditError) {
                Alert(getResources().getString(R.string.alert_label_title), xenditError.getErrorCode() + xenditError.getErrorMessage());
                loadingProgressbar.setVisibility(View.GONE);
            }
        };


        if (isMultipleUse) {
            xendit.createMultipleUseToken(card, callback);
        } else {
            float amount = 0;
            Math.round(Integer.parseInt(insertAmountPay) * Float.parseFloat(CURRENCYCONVERSIONKEY_KEY));
//            amount = Double.parseDouble(insertAmountPay) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
//            amount = (Double) Math.ceil(amount);
            xendit.createSingleUseToken(card, Double.valueOf(amount), shouldAuthenticate, callback);
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

//
//    private void createAuthenticationId(final String tokenId) {
//        try {
//
//            amount = Double.parseDouble(insertAmountPay) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
//            amount = (Double) Math.ceil(amount);
//            //amount=amount*1000;
//            // Double amount = Double.parseDouble(insertAmount) * 1000;
//            xendit.createAuthentication(tokenId, amount, new AuthenticationCallback() {
//                @Override
//                public void onSuccess(Authentication authentication) {
//                    loadingProgressbar.setVisibility(View.GONE);
//                    System.out.println("=======murugan  payment AUTHENTICATOIN_KEY1=========" + authentication.getId());
//                    Intent intent = new Intent(NewCloudMoneyReloadCardDetailPage.this, NewCloudMoneyTransferUrlProgress.class);
//                    intent.putExtra("total_amount", insertAmount);
//                    intent.putExtra("pay_amount", amount + "");
//                    intent.putExtra("authentication_id", authentication.getId());
//                    intent.putExtra("token_id", tokenId);
//                    intent.putExtra("flag", "2");
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.enter, R.anim.exit);
//                    finish();
//                }
//
//                @Override
//                public void onError(XenditError xenditError) {
//                    loadingProgressbar.setVisibility(View.GONE);
//                    Alert(getResources().getString(R.string.alert_label_title), xenditError.getErrorCode() + xenditError.getErrorMessage());
//                    System.out.println("================xenditError============" + xenditError.getErrorCode() + xenditError.getErrorMessage());
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }
}