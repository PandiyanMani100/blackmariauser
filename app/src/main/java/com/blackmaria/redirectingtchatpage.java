package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.pojo.Trackrideafteraccept_pojo;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

/**
 * Created by Prem Kumar and Anitha on 11/6/2015.
 */
public class redirectingtchatpage extends Activity {

    private String response_time = "";
    private Trackrideafteraccept_pojo trackpojo;
    private Boolean moveNext = true;
    private String riderId = "", Str_driver_image = "", carCatImage = "";
    private Dialog dialog;
    private SessionManager session;
    private ServiceRequest mRequest;

    private SessionManager sessionManager;



    String SrideId_intent = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.redirecttochat);
        session = new SessionManager(redirectingtchatpage.this);
        initialize();
        ConfirmRideRequest(Iconstant.track_your_ride_url, "");
    }

    private void initialize() {
        sessionManager = new SessionManager(redirectingtchatpage.this);


        Intent intent = getIntent();

        if (getIntent().getExtras().containsKey("RideID")) {
            SrideId_intent = intent.getStringExtra("RideID");
        }
    }



    private void ConfirmRideRequest(String Url, final String try_value) {
        dialog = new Dialog(redirectingtchatpage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView loading = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        loading.setText(getResources().getString(R.string.action_pleasewait));

        System.out.println("--------------Confirm Ride url----bpp---------------" + Url);

        HashMap<String, String> info = session.getUserDetails();
        String UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", SrideId_intent);

        mRequest = new ServiceRequest(redirectingtchatpage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------Confirm Ride reponse-------------------" + response);

                trackpojo = new Trackrideafteraccept_pojo();

                try {
                    JSONObject object = new JSONObject(response);
                    Type listType = new TypeToken<Trackrideafteraccept_pojo>() {
                    }.getType();

                    try {
                        trackpojo = new GsonBuilder().create().fromJson(object.toString(), listType);
                    } catch (JsonSyntaxException e) {
                        System.out.println("sout" + e);
                    }

                    if (object.length() > 0) {
                        String status = object.getString("status");
                        if (status.equalsIgnoreCase("1")) {
                            JSONObject response_object = object.getJSONObject("response");
                            if (response_object.length() > 0) {
                                Object check_driver_profile_object = response_object.get("driver_profile");
                                if (check_driver_profile_object instanceof JSONObject) {
                                    JSONObject driver_profile_object = response_object.getJSONObject("driver_profile");
                                    if (driver_profile_object.length() > 0) {
                                        session.setDriverprofile(driver_profile_object.toString());
                                        String driver_fcm_token="";
                                        Str_driver_image = trackpojo.getResponse().getDriver_profile().getDriver_image();
                                        String Str_driver_name = trackpojo.getResponse().getDriver_profile().getDriver_name();
                                        String Str_driver_id = trackpojo.getResponse().getDriver_profile().getDriver_id();
                                        String Str_phone_number = trackpojo.getResponse().getDriver_profile().getPhone_number();
                                        session.setKeyDriverPhonenumber(Str_phone_number);

                                        Object notification_details = driver_profile_object.get("notification_details");
                                        if (notification_details instanceof JSONObject) {
                                            JSONObject notification_detailsjson = driver_profile_object.getJSONObject("notification_details");
                                            if (notification_detailsjson.length() > 0) {
                                                driver_fcm_token = notification_detailsjson.getString("driver_fcm_token");
                                            }
                                        }

                                        Intent intent = new Intent(redirectingtchatpage.this,MessagesActivity.class);
                                        intent.putExtra("name",Str_driver_name);
                                        intent.putExtra("userimage",Str_driver_image);
                                        intent.putExtra("rideid",SrideId_intent);
                                        intent.putExtra("useridtosend",Str_driver_id);
                                        intent.putExtra("userphonenumber",Str_phone_number);
                                        intent.putExtra("driver_fcm_token",driver_fcm_token);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            }
                        } else {
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialogDismiss();

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }






    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
