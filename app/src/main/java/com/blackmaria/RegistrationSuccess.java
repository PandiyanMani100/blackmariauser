package com.blackmaria;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.blackmaria.utils.AppUtils;
import com.blackmaria.utils.SessionManager;

import java.util.HashMap;

public class RegistrationSuccess extends AppCompatActivity implements View.OnClickListener {

    private SessionManager sessionManager;



    private TextView Loginnow, RL_readMe, username,contentaware;
    private ImageView userimage;
    String walletammount = "";
    TextView credites;
    public String sContactNumber = "";
    final int PERMISSION_REQUEST_CODE = 111;
    LanguageDb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newreegsuu);
        db = new LanguageDb(this);
        walletammount = (String) getIntent().getExtras().get("welcome_amount");
        initialize();
        credites.setText(getkey("gotfreec")+" "+walletammount);
    }

    private void initialize() {
        sessionManager = SessionManager.getInstance(RegistrationSuccess.this);
        sContactNumber = sessionManager.getContactNumber();
        HashMap<String, String> user = sessionManager.getUserDetails();

        credites= findViewById(R.id.credites);
        RL_readMe = findViewById(R.id.readme);
        Loginnow = findViewById(R.id.loginnow);
        userimage = findViewById(R.id.userimage);
        username = findViewById(R.id.username);
        contentaware = findViewById(R.id.contentaware);
        contentaware.setMovementMethod(new ScrollingMovementMethod());
        contentaware.setText(getkey("content_registration_success"));
        Loginnow.setOnClickListener(this);
        RL_readMe.setOnClickListener(this);
        RL_readMe.setText(getkey("readme"));

        TextView welcome = findViewById(R.id.welcome);
        welcome.setText(getkey("welcomeblackaria"));

        TextView blackmariainc = findViewById(R.id.blackmariainc);
        blackmariainc.setText(getkey("label_blackmaria_inc_lowecase"));

        TextView submitted = findViewById(R.id.submitted);
        submitted.setText(getkey("registrationsubmitted"));


        TextView loginnow = findViewById(R.id.loginnow);
        loginnow.setText(getkey("label_login_now"));


        TextView thankyou = findViewById(R.id.thankyou);
        thankyou.setText(getkey("thank_you_for_joining"));



        if (getIntent().hasExtra("hashMap")) {
            HashMap<String, String> hashMap = (HashMap<String, String>) getIntent().getSerializableExtra("hashMap");
            AppUtils.setImageView(RegistrationSuccess.this, "https://blackmaria.casperon.co/images/users/"+hashMap.get("profile_image"), userimage);
            username.setText(hashMap.get("driver_name"));
        }

    }


    @Override
    public void onClick(View view) {
        if (view == RL_readMe) {
            showBasicProcedures();
        } else if (view == Loginnow) {


            try {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://play.google.com/store/apps/details?id=com.blackmaria.partner"));
                startActivity(viewIntent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Unable to Connect Try Again...",
                        Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            Intent intent = new Intent(RegistrationSuccess.this, Navigation_new.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();



        }
    }

    private void showBasicProcedures() {

        final Dialog basicProceduresDialog = new Dialog(RegistrationSuccess.this, R.style.SlideUpDialog);
        basicProceduresDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        basicProceduresDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        basicProceduresDialog.setCancelable(true);
        basicProceduresDialog.setContentView(R.layout.alert_registration_read_me);


        //--------Adjusting Dialog width and height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 85% of the screen
        basicProceduresDialog.getWindow().setLayout(screenWidth, screenHeight);


        final ImageView Iv_close = (ImageView) basicProceduresDialog.findViewById(R.id.img_close);

        TextView txt_basic_procedures = (TextView) basicProceduresDialog.findViewById(R.id.txt_basic_procedures);
        txt_basic_procedures.setText(getkey("basic_procedures"));
        TextView txt_title_unemployed_person = (TextView) basicProceduresDialog.findViewById(R.id.txt_title_unemployed_person);
        txt_title_unemployed_person.setText(getkey("unemployed_person"));
        TextView txt_unemployed_person = (TextView) basicProceduresDialog.findViewById(R.id.txt_unemployed_person);
        txt_unemployed_person.setText(getkey("you_may_drive_with_us_at_anytime_without_restriction_24_hours_a_day"));

        TextView txt_title_working_person = (TextView) basicProceduresDialog.findViewById(R.id.txt_title_working_person);
        txt_title_working_person.setText(getkey("working_person"));
        TextView txt_working_person = (TextView) basicProceduresDialog.findViewById(R.id.txt_working_person);
        txt_working_person.setText(getkey("for_working_person_please_drive_outside_your_working_hours"));





        TextView txt_title_woman_driver = (TextView) basicProceduresDialog.findViewById(R.id.txt_title_woman_driver);
        txt_title_woman_driver.setText(getkey("woman_driver"));
        TextView txt_woman_driver = (TextView) basicProceduresDialog.findViewById(R.id.txt_woman_driver);
        txt_woman_driver.setText(getkey("your_are_not_allowed_to_drive_from_7pm_to_7am_consent_letter_from_husband_required_except_single_mother"));
        TextView txt_title_student_driver = (TextView) basicProceduresDialog.findViewById(R.id.txt_title_student_driver);
        txt_title_student_driver.setText(getkey("student_driver"));
        TextView txt_student_driver = (TextView) basicProceduresDialog.findViewById(R.id.txt_student_driver);
        txt_student_driver.setText(getkey("driving_are_allowed_not_more_than_6_hours_per_day_consent_letter_from_parents_guardian_is_required"));






        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                basicProceduresDialog.dismiss();
            }
        });

        basicProceduresDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                RL_readMe.setClickable(true);
            }
        });

        basicProceduresDialog.show();
        RL_readMe.setClickable(false);

    }

    @Override
    public void onBackPressed() {
             Toast.makeText(getApplicationContext(),"adfda",Toast.LENGTH_LONG).show();
    }

    private String getkey(String key)
    {
        return db.getvalueforkey(key);
    }
}