package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.DowloadPdfActivity;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.TextRCBold;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Murugananatham on 3/2/2018.
 */

@SuppressLint("Registered")
public class ReloadXenditPaymentSuccessActivity extends ActivityHockeyApp implements View.OnClickListener, DowloadPdfActivity.downloadCompleted {

    private TextView Transection_id, paymentcode;
    private CustomTextView current_username, date, time;
    private TextRCBold Amount;
    RelativeLayout Rl_close, Rl_save;
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String UserNmae = "", UserID = "", xenditPaymentId = "", Scurrent_user = "", Smode = "", amount = "", transection_id = "", currency = "", flag = "";
    private TextView username;
    String from1 = "", date1 = "", time1 = "";
    private LinearLayout xenditcard, xenditbank;
    private RelativeLayout paypalpayment;
    private RefreshReceiver refreshReceiver;

    LinearLayout pdflayout;
    RelativeLayout savelyt;
    private TextView sava_tv;
    private String fromBookingpage = "";
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(ReloadXenditPaymentSuccessActivity.this, Navigation_new.class);
                        startActivity(intent1);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(ReloadXenditPaymentSuccessActivity.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(ReloadXenditPaymentSuccessActivity.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reloadxenditpayment_success);
        initiaztion();

    }


    private void initiaztion() {

        session = new SessionManager(ReloadXenditPaymentSuccessActivity.this);
        cd = new ConnectionDetector(ReloadXenditPaymentSuccessActivity.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        UserNmae = info.get(SessionManager.KEY_USERNAME);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        Transection_id = (TextView) findViewById(R.id.txt_trasection_id);
        current_username = (CustomTextView) findViewById(R.id.current_username);
        date = (CustomTextView) findViewById(R.id.trans_date);
        time = (CustomTextView) findViewById(R.id.trans_time);
        Amount = (TextRCBold) findViewById(R.id.amount_text);
        Rl_close = (RelativeLayout) findViewById(R.id.close_btn);
        Rl_save = (RelativeLayout) findViewById(R.id.tarnsection_save);
        paymentcode = (TextView) findViewById(R.id.paymentcode);
        xenditcard = (LinearLayout) findViewById(R.id.xenditcard);
        xenditbank = (LinearLayout) findViewById(R.id.xenditbank);
        paypalpayment = (RelativeLayout) findViewById(R.id.paypalpayment);


        pdflayout=(LinearLayout) findViewById(R.id.pdflayout);
        savelyt= (RelativeLayout) findViewById(R.id.savelyt);
        sava_tv= (TextView) findViewById(R.id.sava_tv);

        Intent in = getIntent();
        flag = in.getStringExtra("flag");
        amount = in.getStringExtra("amount");
        transection_id = in.getStringExtra("ref_no");
        currency = in.getStringExtra("currency");

        if (flag.equalsIgnoreCase("xenditReaload")) {
            date1 = in.getStringExtra("date");
            time1 = in.getStringExtra("time");
            xenditcard.setVisibility(View.VISIBLE);
            xenditbank.setVisibility(View.GONE);
        } else if (flag.equalsIgnoreCase("xenditBankReaload")) {
            date1 = in.getStringExtra("date");
            time1 = in.getStringExtra("time");
            xenditPaymentId = in.getStringExtra("xenditPaymentId");
            xenditcard.setVisibility(View.GONE);
            paymentcode.setVisibility(View.VISIBLE);
            xenditbank.setVisibility(View.VISIBLE);
            paymentcode.setText(getResources().getString(R.string.payment_code) + xenditPaymentId);
        } else if (flag.equalsIgnoreCase("paypalpayment")) {

            from1 = in.getStringExtra("from");
            date1 = in.getStringExtra("date");
            time1 = in.getStringExtra("time");

            paypalpayment.setVisibility(View.VISIBLE);
            xenditcard.setVisibility(View.GONE);
            xenditbank.setVisibility(View.GONE);
            paymentcode.setVisibility(View.GONE);
        }

        if (getIntent().hasExtra("frombookingpage")) {
            fromBookingpage = getIntent().getStringExtra("frombookingpage");
        }

        setvalues();
        Rl_close.setOnClickListener(this);
        Rl_save.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if (v == Rl_close) {
            Intent intent = new Intent(ReloadXenditPaymentSuccessActivity.this,WalletMoneyPage1.class);
            if(fromBookingpage.equalsIgnoreCase("1")){
                intent.putExtra("fromBookingpage", "1");
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == Rl_save) {
            savelyt.setVisibility(View.GONE);
            new DowloadPdfActivity(ReloadXenditPaymentSuccessActivity.this,pdflayout,"moneytransfer",ReloadXenditPaymentSuccessActivity.this);
        }
    }

    private void setvalues() {
        Transection_id.setText(getResources().getString(R.string.trans_no) + " " + ":" + " " + transection_id);

        Amount.setText(currency + " " + amount);
        if (date1.equalsIgnoreCase("")) {
            date.setText(GetCurrentDate());
        } else {
            date.setText(date1);
        }
        if (date1.equalsIgnoreCase("")) {
            time.setText(GetCurrentTime());
        } else {
            time.setText(time1);
        }
        if (from1.equalsIgnoreCase("")) {
            current_username.setText(UserNmae);
        } else {
            current_username.setText(from1);
        }

    }

    private String GetCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    private String GetCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss aaa");
        String formattedtime = df.format(c.getTime());
        return formattedtime;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    @Override
    public void OnDownloadComplete(String fileName) {
        savelyt.setVisibility(View.VISIBLE);
        sava_tv.setText(getResources().getString(R.string.saved));

        ViewPdfAlert("",getResources().getString(R.string.sucessss),fileName);
    }
    //--------------Alert Method-----------
    private void ViewPdfAlert( String title, String alert,final String fileName) {
        final PkDialog mDialog = new PkDialog(ReloadXenditPaymentSuccessActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.open), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPDF(fileName);
                mDialog.dismiss();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    public void viewPDF(String pdfFileName) {

        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Signature/" + pdfFileName);  // -> filename = maven.pdf
        Uri uri_path = Uri.fromFile(pdfFile);
        Uri uri = uri_path;

        try {
            File file = null;
            String path = uri.toString();// "/mnt/sdcard/FileName.mp3"
            try {
                file = new File(new URI(path));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            final Intent intent = new Intent(Intent.ACTION_VIEW)//
                    .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                    androidx.core.content.FileProvider.getUriForFile(ReloadXenditPaymentSuccessActivity.this, getPackageName() + ".provider", file) : Uri.fromFile(file),
                            "application/pdf").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(ReloadXenditPaymentSuccessActivity.this,  getResources().getString(R.string.no_pdf), Toast.LENGTH_SHORT).show();
            }

        } catch (ActivityNotFoundException e) {
            Toast.makeText(ReloadXenditPaymentSuccessActivity.this, getResources().getString(R.string.no_read_file), Toast.LENGTH_SHORT).show();
        }
    }
}
