package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.newdesigns.Closeaccount_pincheck;
import com.blackmaria.pojo.Payment_closeaccount_pojo;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class Close_creditwithus extends AppCompatActivity {

    private ImageView img_back;
    private TextView amount, RL_close_account;
    private LinearLayout payments;
    private String UserID = "";
    private SessionManager sessionManager;
    LanguageDb mhelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close_creditwithus);
        mhelper = new LanguageDb(this);
        sessionManager = new SessionManager(Close_creditwithus.this);

        HashMap<String, String> user = sessionManager.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        img_back = findViewById(R.id.img_back);
        amount = findViewById(R.id.amount);
        if (getIntent().hasExtra("amount")) {
            amount.setText(getIntent().getStringExtra("amount"));
        }
        RL_close_account = findViewById(R.id.RL_close_account);
        RL_close_account.setText(getkey("track_your_ride_label_cancel"));
        payments = findViewById(R.id.payments);

        TextView getpaid= findViewById(R.id.getpaid);
        getpaid.setText(getkey("how_do_you_want_to_be_paid"));


        TextView closesummary= findViewById(R.id.closesummary);
        closesummary.setText(getkey("your_credit_with_us"));

        TextView severaldays= findViewById(R.id.severaldays);
        severaldays.setText(getkey("please_allow_several_days_to_process"));

        RL_close_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        paymentlist(Iconstant.closeaccountpaymentslist, jsonParams);
    }


    private void paymentlist(String Url, final HashMap<String, String> jsonParams) {

        final Dialog dialog = new Dialog(Close_creditwithus.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ServiceRequest mRequest = new ServiceRequest(Close_creditwithus.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                String Sstatus = "", Smessage = "";
                dialog.dismiss();
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        Type listType = new TypeToken<Payment_closeaccount_pojo>() {
                        }.getType();
                        Payment_closeaccount_pojo obj = new GsonBuilder().create().fromJson(object.toString(), listType);
                        paymentlistconstrian(obj);
                    } else {
                        Alert(getkey("action_error"), Smessage);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();

                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Close_creditwithus.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }


    private void paymentlistconstrian(final Payment_closeaccount_pojo refer_friendslists) {
        try {
            payments.removeAllViews();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= refer_friendslists.getResponse().getPayment().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.closeaccountadapter_paymentslist, null);

            ImageView imageView = view.findViewById(R.id.payment);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.new_no_user_img)
                    .error(R.drawable.new_no_user_img)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
            if(!refer_friendslists.getResponse().getPayment().get(i).getIcon().equalsIgnoreCase("")){
                Glide.with(this).load(refer_friendslists.getResponse().getPayment().get(i).getIcon()).apply(options).into(imageView);
            }

            final int finalI = i;
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent ii = new Intent(Close_creditwithus.this, Closeaccount_pincheck.class);
                    ii.putExtra("mode", refer_friendslists.getResponse().getPayment().get(finalI).getCode());
                    startActivity(ii);
                }
            });

            payments.addView(view);
        }
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}


