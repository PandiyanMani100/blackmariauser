package com.blackmaria;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.widgets.PkDialog;

import java.util.HashMap;

/**
 * Created by GANESH on 26-09-2017.
 */

public class RegistrationTerms extends ActivityHockeyApp implements View.OnClickListener {

    private SessionManager sessionManager;
    private ConnectionDetector cd;

    private String sDriverID = "";

    private RelativeLayout RL_exit;
    private TextView Tv_terms, Tv_privacyPolicy, Tv_eula;
    private CheckBox Cb_terms, Cb_privacyPolicy, Cb_eula;
    private Button Btn_readTerms, Btn_registerNow;
    private TextView vladirText;
    private Typeface face;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_terms_and_conditions);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(RegistrationTerms.this);
        cd = new ConnectionDetector(RegistrationTerms.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);

        RL_exit = (RelativeLayout) findViewById(R.id.RL_exit);
        Tv_terms = (TextView) findViewById(R.id.txt_label_terms);
        Tv_privacyPolicy = (TextView) findViewById(R.id.txt_label_privacy_policy);
        Tv_eula = (TextView) findViewById(R.id.txt_label_eula);
        Cb_terms = (CheckBox) findViewById(R.id.cb_terms_and_conditions);
        Cb_privacyPolicy = (CheckBox) findViewById(R.id.cb_privacy_policy);
        Cb_eula = (CheckBox) findViewById(R.id.cb_eula);
        Btn_readTerms = (Button) findViewById(R.id.btn_read_terms);
        Btn_registerNow = (Button) findViewById(R.id.btn_register_now);

        vladirText = (TextView) findViewById(R.id.txt_label_magic);
        face = Typeface.createFromAsset(getAssets(), "fonts/vladimir.ttf");
        vladirText.setTypeface(face);

        RL_exit.setOnClickListener(this);
        Btn_readTerms.setOnClickListener(this);
        Btn_registerNow.setOnClickListener(this);
        Tv_terms.setOnClickListener(this);
        Tv_privacyPolicy.setOnClickListener(this);
        Tv_eula.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (view == RL_exit) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == Btn_readTerms) {

            String url = "https://blackmaria.co/";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);

        } else if (view == Btn_registerNow) {

            if (Cb_terms.isChecked() && Cb_privacyPolicy.isChecked() && Cb_eula.isChecked()) {
                // All 3 check boxes are checked

                Intent intent = new Intent(RegistrationTerms.this, RegistrationSelectLocation.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            } else {
                // All 3 check boxes are not checked
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.please_accept_terms_and_conditions));
            }

        } else if (view == Tv_terms) {
            if (Cb_terms.isChecked()) {
                Cb_terms.setChecked(false);
            } else {
                Cb_terms.setChecked(true);
            }
        } else if (view == Tv_privacyPolicy) {
            if (Cb_privacyPolicy.isChecked()) {
                Cb_privacyPolicy.setChecked(false);
            } else {
                Cb_privacyPolicy.setChecked(true);
            }
        } else if (view == Tv_eula) {
            if (Cb_eula.isChecked()) {
                Cb_eula.setChecked(false);
            } else {
                Cb_eula.setChecked(true);
            }
        }

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(RegistrationTerms.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


}
