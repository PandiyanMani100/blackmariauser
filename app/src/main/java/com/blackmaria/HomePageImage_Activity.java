package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.AppController;
import com.blackmaria.volley.VolleyMultipartRequest;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by user144 on 5/16/2017.
 */

public class HomePageImage_Activity extends ActivityHockeyApp {
    private ImageView prof_popup_cancel;
    private RelativeLayout camera,gallery,layout_addimg;
    final int PERMISSION_REQUEST_CODE = 111;
    private Dialog photo_dialog,dialog;
    private Uri camera_FileUri;
    Bitmap bitMapThumbnail;
    private byte[] byteArray;
    private int REQUEST_TAKE_PHOTO = 1;
    private int galleryRequestCode = 2;
    private static final String IMAGE_DIRECTORY_NAME = "blackmaria";
    private static final String TAG = "";
    private String mSelectedFilePath = "";
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    AppInfoSessionManager appInfoSessionManager;
    private String UserID = "";
    private String gcmID = "";
    private String sAuthKey = "";
    Bitmap selectedBitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepageimagechange);
        initialize();
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                callchooseUploadType();
            }
        } else {
            callchooseUploadType();

        }
        prof_popup_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePageImage_Activity.this, Navigation_new.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void initialize() {
        session = new SessionManager(HomePageImage_Activity.this);
        appInfoSessionManager = new AppInfoSessionManager(HomePageImage_Activity.this);
        cd = new ConnectionDetector(HomePageImage_Activity.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> info = session.getUserDetails();
        String tipstime = info.get(SessionManager.KEY_PHONENO);
        UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> app = appInfoSessionManager.getAppInfo();
        sAuthKey = app.get(appInfoSessionManager.KEY_APP_IDENTITY);

        HashMap<String, String> user = session.getUserDetails();
        gcmID = user.get(SessionManager.KEY_GCM_ID);


         prof_popup_cancel=(ImageView)findViewById(R.id.prof_popup_cancel);
         camera = (RelativeLayout) findViewById(R.id.profilelayout_takephotofromcamera);
         gallery = (RelativeLayout)findViewById(R.id.profilelayout_takephotofromgallery);
        layout_addimg= (RelativeLayout)findViewById(R.id.layout_addimg);
    }
   private void callchooseUploadType(){
       camera.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               takePicture();
               layout_addimg.setVisibility(View.GONE);

           }
       });

       gallery.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               openGallery();
               layout_addimg.setVisibility(View.GONE);

           }
       });
    }
    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        camera_FileUri = getOutputMediaFileUri(1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, camera_FileUri);
        // start the image capture Intent
        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, galleryRequestCode);
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }


    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", camera_FileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        camera_FileUri = savedInstanceState.getParcelable("file_uri");
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;

                    final Bitmap bitmap = BitmapFactory.decodeFile(camera_FileUri.getPath(), options);
                    Bitmap thumbnail = bitmap;
                    final String picturePath = camera_FileUri.getPath();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                    File curFile = new File(picturePath);
                    try {
                        ExifInterface exif = new ExifInterface(curFile.getPath());
                        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                        int rotationInDegrees = exifToDegrees(rotation);

                        Matrix matrix = new Matrix();
                        if (rotation != 0f) {
                            matrix.preRotate(rotationInDegrees);
                        }
                        thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                    } catch (IOException ex) {
                        Log.e("TAG", "Failed to get Exif data", ex);
                    }

                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();

                    //------------Code to update----------
                    bitMapThumbnail = thumbnail;
                    UploadDriverImage(Iconstant.dashboardimage_url);
                    //UploadDriverImage("https://www.yourprintertechnician.com/v3/app/save-image");
                    System.out.println("edit-----" + Iconstant.Edit_profile_image_url);

                    Uri picUri = Uri.fromFile(curFile);

                    UCrop.of(picUri, picUri)
                            .withAspectRatio(4, 3)
                            .withMaxResultSize(8000, 8000)
                            .start(HomePageImage_Activity.this);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == galleryRequestCode) {

                Uri selectedImage = data.getData();
                if (selectedImage.toString().startsWith("content://com.sec.android.gallery3d.provider")) {
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    final String picturePath = c.getString(columnIndex);
                    c.close();

                    Picasso.with(HomePageImage_Activity.this).load(picturePath).resize(100, 100).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            Bitmap thumbnail = bitmap;
                            mSelectedFilePath = picturePath;
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                            byteArray = byteArrayOutputStream.toByteArray();

                            //------------Code to update----------
                            bitMapThumbnail = thumbnail;
                            File curFile = new File(picturePath);
                            Uri picUri = Uri.fromFile(curFile);

                            UCrop.of(picUri, picUri)
                                    .withAspectRatio(4, 3)
                                    .withMaxResultSize(8000, 8000)
                                    .start(HomePageImage_Activity.this);


                           // UploadDriverImage(Iconstant.dashboardimage_url);
                            // UploadDriverImage(" https://www.yourprintertechnician.com/v3/app/save-image");
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                        }
                    });
                } else {
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();

                    int columnIndex = c.getColumnIndex(filePath[0]);
                    final String picturePath = c.getString(columnIndex);
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                    Bitmap thumbnail = bitmap; //getResizedBitmap(bitmap, 600);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    File curFile = new File(picturePath);

                    try {
                        ExifInterface exif = new ExifInterface(curFile.getPath());
                        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                        int rotationInDegrees = exifToDegrees(rotation);

                        Matrix matrix = new Matrix();
                        if (rotation != 0f) {
                            matrix.preRotate(rotationInDegrees);
                        }
                        thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                    } catch (IOException ex) {
                        Log.e("TAG", "Failed to get Exif data", ex);
                    }
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();
                    c.close();

                    //------------Code to update----------
                    bitMapThumbnail = thumbnail;
                    Uri picUri = Uri.fromFile(curFile);

                    UCrop.of(picUri, picUri)
                            .withAspectRatio(4, 3)
                            .withMaxResultSize(8000, 8000)
                            .start(HomePageImage_Activity.this);


                    //UploadDriverImage(Iconstant.dashboardimage_url);

                    // UploadDriverImage(" https://www.yourprintertechnician.com/v3/app/save-image");
                }
            }else  if(requestCode == UCrop.REQUEST_CROP){
                    if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
                        final Uri resultUri = UCrop.getOutput(data);
                        try {
                            selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                        byteArray = byteArrayOutputStream.toByteArray();
                        System.out.println("===================images============"+resultUri);
                        UploadDriverImage(Iconstant.dashboardimage_url);
                       /* goodsImage.setImageBitmap(selectedBitmap);
                        goodsImage.setImageURI(resultUri);*/
                    } else if (resultCode == UCrop.RESULT_ERROR) {
                        final Throwable cropError = UCrop.getError(data);
                        System.out.println("========muruga cropError==========="+cropError);
                    }
            }

        }
    }
    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }



    private void UploadDriverImage(String url) {

         dialog = new Dialog(HomePageImage_Activity.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                System.out.println("------------- image response-----------------" + response.data);


                String resultResponse = new String(response.data);
                System.out.println("-------------  response-----------------" + resultResponse);
                String sStatus = "", sResponse = "", SUser_image = "", Smsg = "";
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    Smsg = jsonObject.getString("response");
                    if (sStatus.equalsIgnoreCase("1")) {
                        SUser_image = jsonObject.getString("image_url");
                        session.setUserHomeAnimaionImageUpdate(SUser_image);

                        Alert(getResources().getString(R.string.action_success), Smsg,1);
                    } else {
                        Alert(getResources().getString(R.string.action_error), Smsg,2);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserID);
                System.out.println("user_id---------------" + UserID);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("dashboard_image", new DataPart("blackmaria.jpg", byteArray));
                System.out.println("user_image--------edit------" + byteArray);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authkey", sAuthKey);
                headers.put("User-agent", Iconstant.cabily_userAgent);
                headers.put("isapplication", Iconstant.cabily_IsApplication);
                headers.put("applanguage", Iconstant.cabily_AppLanguage);
                headers.put("apptype", Iconstant.cabily_AppType);
                headers.put("userid", UserID);
                headers.put("apptoken", gcmID);
                System.out.println("------------header---------" + headers);
                return headers;
            }

        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert,final int flag) {
        final PkDialog mDialog = new PkDialog(HomePageImage_Activity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag==1) {
                    sendBroadcast();
                }
             /*   Intent intent = new Intent(HomePageImage_Activity.this, Navigation_new.class);
                startActivity(intent);*/
                overridePendingTransition(R.anim.enter,R.anim.exit);
                finish();
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }


    private void sendBroadcast() {
        Intent local = new Intent();
        local.setAction("com.app.PaymentListPage.refreshhomedashBoardImage");
        local.putExtra("refresh", "yes");
        sendBroadcast(local);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callchooseUploadType();
                } else {
                    finish();
                }
                break;
        }
    }
    @Override
    public void onPause() {
        super.onPause();

        if ((dialog != null) && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(HomePageImage_Activity.this, Navigation_new.class);
        startActivity(intent);
        overridePendingTransition(R.anim.exit, R.anim.enter);
        finish();
    }
}
