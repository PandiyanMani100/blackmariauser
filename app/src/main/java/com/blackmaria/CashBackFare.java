package com.blackmaria;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import static com.blackmaria.iconstant.Iconstant.Url;

/**
 * Created by user144 on 5/4/2017.
 */

public class CashBackFare extends ActivityHockeyApp {

    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "", SrideId_intent = "";
    private Dialog dialog;
    private ServiceRequest mRequest;
    private TextView accept_cashback,cashback_text,exitLyt;
    LanguageDb mhelper;
    private String cashbackInvitationLink = "", Smessage = "";
    TextView cashbbac;
    public static final int REQUEST_CODE_FACEBOOK_SHARE = 112;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private boolean check_status = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookSDKInitialize();
        setContentView(R.layout.cashback_offer_layout);
        mhelper = new LanguageDb(this);
        initialize();

        exitLyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CashBackFare.this, MyRideRating.class);
                intent1.putExtra("RideID", SrideId_intent);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |/* Intent.FLAG_ACTIVITY_NO_HISTORY | */Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                overridePendingTransition(R.anim.enter, R.anim.exit);
//                onBackPressed();
//                finish();
            }
        });

        accept_cashback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetPresent) {
                    //SharecashBackOnFaceBook();

                    try {

                        if (isFBLoggedIn()) {
                            ShareDialog();
                        } else {
                            LoginManager.getInstance().logInWithPublishPermissions(
                                    CashBackFare.this,
                                    Arrays.asList("publish_actions"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Alert(getkey("alert_label_title"),getkey("alert_nointernet"));
                }

            }
        });

    }


    private void initialize() {
        session = new SessionManager(CashBackFare.this);
        cd = new ConnectionDetector(CashBackFare.this);
        isInternetPresent = cd.isConnectingToInternet();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        Intent intent = getIntent();
        SrideId_intent = intent.getStringExtra("RideID");

        //tvLearnmore = (TextView) findViewById(R.id.tv_learnmore);
        exitLyt = findViewById(R.id.Rl_close);
        exitLyt.setText(getkey("closelable"));
        cashback_text = findViewById(R.id.cashback_text);
        cashbbac = findViewById(R.id.cashbbac);
        cashbbac.setText(getkey("cash_back"));

        TextView cashbackid = findViewById(R.id.cashbackid);
        cashbackid.setText(getkey("cashback_head"));


        accept_cashback = findViewById(R.id.accept_cashback);
        accept_cashback.setText(getkey("accept_now"));
        if (isInternetPresent) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("ride_id", SrideId_intent);
            jsonParams.put("user_id", UserID);
            postRequest_Cashback(Iconstant.cashback_home, jsonParams);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }

//        ImageView Rl_drawer = (ImageView) findViewById(R.id.drawer_icon);
//
//        final Animation animation = new AlphaAnimation(1, 0);
//        animation.setDuration(500);
//        animation.setInterpolator(new LinearInterpolator());
//        animation.setRepeatCount(Animation.INFINITE);
//        animation.setRepeatMode(Animation.REVERSE);
//        Rl_drawer.startAnimation(animation);

    }

    private void postRequest_Cashback(String cashbackurl, HashMap<String, String> jsonParams) {
        dialog = new Dialog(CashBackFare.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-------------CashBackFare Url----------------" + Url);
        System.out.println("-------------CashBackFarejsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(CashBackFare.this);
        mRequest.makeServiceRequest(cashbackurl, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------CashBackFare Response----------------" + response);

                String Sstatus = "", cashback_amount = "", currency = "", subject = "", message = "", url = "", cashbackType = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        JSONObject obj_detail = response_object.getJSONObject("details");
                        if (obj_detail.length() > 0) {
                            cashback_amount = obj_detail.getString("cashback_amount");
                            cashbackType = obj_detail.getString("cashback_type");

                            currency = obj_detail.getString("currency");
                            subject = obj_detail.getString("subject");
                            message = obj_detail.getString("message");
                            url = obj_detail.getString("url");
                            cashbackInvitationLink = url;
                            Smessage = message;
                        }
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (cashbackType.equalsIgnoreCase("Percentage")) {
                            cashback_text.setText(cashback_amount + getkey("percentage"));
                        } else {
                            cashback_text.setText(currency + " " + cashback_amount);
                        }

                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(CashBackFare.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCancelble(false);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    private void SharecashBackOnFaceBook() {
        Uri imageUri = null;
        try {
            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                    BitmapFactory.decodeResource(getResources(), R.drawable.invite_and_earn_car), null, null));
        } catch (NullPointerException e) {
        }

        if (cashbackInvitationLink.length() > 0) {
            shareFacebookLink(cashbackInvitationLink);
        } else {
            shareFacebook(Smessage, imageUri);
        }
    }

    private void shareFacebook(String smessage, Uri imageUri) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        //intent.setType("text/plain");
        intent.setType("image/*");
        //intent.putExtra(Intent.EXTRA_TEXT, "http://project.dectar.com/fortaxi/rider/signup?ref=QVJPQ0tJQQ==");
        intent.putExtra(Intent.EXTRA_STREAM, imageUri);
        Intent pacakage2 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana");
        } else {
            intent.setPackage("");
        }

        try {
            startActivityForResult(intent, 1);
        } catch (ActivityNotFoundException ex) {
            Alert(getkey("alert_label_title"),getkey("invite_earn_label_facebook_not_installed"));
        }
    }

    private void shareFacebookLink(String cashbackInvitationLink) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, cashbackInvitationLink);
        Intent pacakage2 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana");
        } else {
            intent.setPackage("");
        }

        try {
            startActivityForResult(intent, 1);
        } catch (ActivityNotFoundException ex) {
            Alert(getkey("alert_label_title"), getkey("invite_earn_label_facebook_not_installed"));
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if(requestCode==1)
        {
            if (isInternetPresent) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("ride_id", SrideId_intent);
                jsonParams.put("user_id", UserID);
                postRequest_CashbackApply(Iconstant.cashback_Apply_url, jsonParams);
            } else {
            }
        }
        onActivityResult(requestCode, resultCode, data);*/

        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void postRequest_CashbackApply(String cashbackurl, HashMap<String, String> jsonParams) {
        dialog = new Dialog(CashBackFare.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-------------CashBackApply Url----------------" + Url);
        System.out.println("-------------CashBackApply jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(CashBackFare.this);
        mRequest.makeServiceRequest(cashbackurl, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------CashBackApply Response----------------" + response);

                String Sstatus = "", cashback_amount = "", currency = "", wallet_amount = "", message = "", url = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        JSONObject obj_detail = response_object.getJSONObject("details");
                        if (obj_detail.length() > 0) {
                            cashback_amount = obj_detail.getString("cashback_amount");
                            currency = obj_detail.getString("currency");
                            wallet_amount = obj_detail.getString("wallet_amount");
                        }
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Intent cahbackIntent = new Intent(CashBackFare.this, CashBackFareShare.class);
                        cahbackIntent.putExtra("cashback_amount", cashback_amount);
                        cahbackIntent.putExtra("currency", currency);
                        cahbackIntent.putExtra("wallet_amount", wallet_amount);
                        startActivity(cahbackIntent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        /*finish();*/
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();


        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                ShareDialog();

            }

            @Override
            public void onCancel() {
                Toast.makeText(CashBackFare.this, getkey("logincancel"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(CashBackFare.this, getkey("loginerror"), Toast.LENGTH_SHORT).show();
            }
        });


    }


    public boolean isFBLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }


    public void ShareDialog() {

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                System.out.println("Result = " + result.getPostId());
//                Toast.makeText(CashBackFare.this, "FB Share success", Toast.LENGTH_SHORT).show();

                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("ride_id", SrideId_intent);
                    jsonParams.put("user_id", UserID);
                    postRequest_CashbackApply(Iconstant.cashback_Apply_url, jsonParams);
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }

            }

            @Override
            public void onCancel() {
                Toast.makeText(CashBackFare.this,getkey("fb_shared"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(CashBackFare.this,  getkey("fb_error"), Toast.LENGTH_SHORT).show();
            }
        }, REQUEST_CODE_FACEBOOK_SHARE);


        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(cashbackInvitationLink))
                .build();

        shareDialog.show(content);


    }
//    -------------------------Agreement Dialog-----------------------------

    private void LearnMoreDialog() {


        check_status = false;

        final Dialog dialog = new Dialog(CashBackFare.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_user_and_earn);


        final ImageView cancel = (ImageView) dialog.findViewById(R.id.image_close);
        final ImageView cancel_header = (ImageView) dialog.findViewById(R.id.img_header);
        final Button ok_button = (Button) dialog.findViewById(R.id.btn_ok);
        final CheckBox chck_agree = (CheckBox) dialog.findViewById(R.id.agree_checkBox);


        chck_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((CheckBox) v).isChecked()) {

                    check_status = true;

                } else {

                    check_status = false;
                }


            }
        });

        cancel_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_status) {
                    dialog.dismiss();
                }
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // remove background dim
        dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}

