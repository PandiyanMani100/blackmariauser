package com.blackmaria;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.blackmaria.adapter.RatingAdapterNew;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.newdesigns.Thanksrating;
import com.blackmaria.pojo.RatingPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.chatmodule.DbHelper;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Prem Kumar and Anitha on 11/4/2015.
 */
public class MyRideRating extends ActivityHockeyApp {
    private ImageView skip;
    private TextView submit;
    private EditText Et_comment;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "";

    private ServiceRequest mRequest;
    Dialog dialog;
    ArrayList<RatingPojo> itemlist;
    LanguageDb mhelper;
    RatingAdapterNew adapter;
    private RecyclerView listview;
    private String SrideId_intent = "";
    private boolean isDataAvailable = false;
    private RoundedImageView Iv_deriverImg;
    private TextView Tv_driverName, Tv_driverCarType;//Tv_arriveTime;
    private RatingBar Rb_driverRating;

    private String driverName = "", driverImage = "", driverRating = "", driverTime = "", driverCar_no = "",
            driverCar_model = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myride_rating_new);
        mhelper =new LanguageDb(this);
        initialize();

        deleteDatabase(DbHelper.DATABASE_NAME);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("com.pushnotification.updateBottom_view");
                sendBroadcast(broadcastIntent);
                onBackPressed();

                Intent intent = new Intent(MyRideRating.this, Navigation_new.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isRatingEmpty = false;

                if (itemlist != null) {
                    for (int i = 0; i < itemlist.size(); i++) {
                        if (itemlist.get(i).getRatingcount().length() == 0 || itemlist.get(i).getRatingcount().equalsIgnoreCase("0.0")) {
                            isRatingEmpty = true;
                        }
                    }


                    if (!isRatingEmpty) {
                        if (isInternetPresent) {

                            System.out.println("------------ride_id-------------" + SrideId_intent);
                            System.out.println("------------comments-------------" + Et_comment.getText().toString());
                            System.out.println("------------ratingsFor-------------" + "driver");
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("comments", Et_comment.getText().toString());
                            jsonParams.put("ratingsFor", "driver");
                            jsonParams.put("ride_id", SrideId_intent);
                            for (int i = 0; i < itemlist.size(); i++) {
                                jsonParams.put("ratings[" + i + "][option_id]", itemlist.get(i).getRatingId());
                                jsonParams.put("ratings[" + i + "][option_title]", itemlist.get(i).getRatingName());
                                jsonParams.put("ratings[" + i + "][rating]", itemlist.get(i).getRatingcount());
                            }
                            System.out.println("------------jsonParams-------------" + jsonParams);
                            postRequest_SubmitRating(Iconstant.myride_rating_submit_url, jsonParams);
                        } else {
                            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                        }
                    } else {
                        Alert(getkey("my_rides_rating_header_sorry_textview"), getkey("my_rides_rating_header_enter_all"));
                    }

                }
            }
        });
    }

    private void initialize() {
        session = new SessionManager(MyRideRating.this);
        cd = new ConnectionDetector(MyRideRating.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemlist = new ArrayList<RatingPojo>();
        skip = findViewById(R.id.iv_close);

        Iv_deriverImg = findViewById(R.id.iv_usericon);
        Tv_driverName = findViewById(R.id.rating_page_driver_profilename_textview);

        TextView rateme = findViewById(R.id.rateme);
        rateme.setText(getkey("rate_me"));

        TextView yourcomment = findViewById(R.id.yourcomment);
        yourcomment.setText(getkey("your_comment_is_appreciated"));

        listview = (RecyclerView) findViewById(R.id.Lv_rating);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        listview.setLayoutManager(linearLayoutManager);
        listview.setItemAnimator(new DefaultItemAnimator());

        submit = findViewById(R.id.my_rides_rating_submit_layout);
        submit.setText(getkey("submit"));
        Et_comment = findViewById(R.id.my_rides_rating_comment_edittext);
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        Intent intent = getIntent();
        SrideId_intent = intent.getStringExtra("RideID");
        if (isInternetPresent) {
            postRequest_RatingList(Iconstant.myride_rating_url);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(MyRideRating.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-----------------------Rating List Post Request-----------------
    private void postRequest_RatingList(String Url) {
        dialog = new Dialog(MyRideRating.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));


        System.out.println("-------------Rating List Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("optionsFor", "driver");
        jsonParams.put("ride_id", SrideId_intent);

        System.out.println("rideid-----------" + SrideId_intent);

        mRequest = new ServiceRequest(MyRideRating.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                Log.e("rateing", response);

                System.out.println("-------------Rating List Response----------------" + response);

                String Sstatus = "";
                String SRating_status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    SRating_status = object.getString("ride_ratting_status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject driver_profile_object = object.getJSONObject("driver_profile");

                        if (driver_profile_object.length() > 0) {

                            driverName = driver_profile_object.getString("driver_name");
                            driverImage = driver_profile_object.getString("driver_image");
                            driverRating = driver_profile_object.getString("driver_review");
                            driverCar_no = driver_profile_object.getString("vehicle_number");
                            driverCar_model = driver_profile_object.getString("vehicle_model");
                            driverTime = driver_profile_object.getString("last_ride");

                        }


                        JSONArray payment_array = object.getJSONArray("review_options");
                        if (payment_array.length() > 0) {
                            itemlist.clear();
                            for (int i = 0; i < payment_array.length(); i++) {
                                JSONObject reason_object = payment_array.getJSONObject(i);
                                RatingPojo pojo = new RatingPojo();
                                pojo.setRatingId(reason_object.getString("option_id"));
                                pojo.setRatingName(reason_object.getString("option_title"));
                                pojo.setRatingcount("");
                                itemlist.add(pojo);
                            }
                            isDataAvailable = true;
                        } else {
                            isDataAvailable = false;
                        }
                    }

                    if (Sstatus.equalsIgnoreCase("1") && isDataAvailable) {

                        if (SRating_status.equalsIgnoreCase("1")) {

                            Toast.makeText(getApplicationContext(),  getkey("allready_submited"), Toast.LENGTH_SHORT).show();

                        } else {

                            Picasso.with(MyRideRating.this)
                                    .load(driverImage)
                                    .into(Iv_deriverImg);
                            Tv_driverName.setText(driverName);
//                            Tv_VechileNo.setText(driverCar_no);
//                            Tv_driverCarType.setText(driverCar_model);
                            //         Tv_arriveTime.setText(driverTime);
//                            Rb_driverRating.setRating(Float.parseFloat(driverRating));

                            adapter = new RatingAdapterNew(MyRideRating.this, itemlist);
                            listview.setAdapter(adapter);
                        }

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //-----------------------Submit Rating Post Request-----------------
    private void postRequest_SubmitRating(String Url, final HashMap<String, String> jsonParams) {
        dialog = new Dialog(MyRideRating.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));


        System.out.println("-------------Submit Rating Url----------------" + Url);

        mRequest = new ServiceRequest(MyRideRating.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("------------Submit Rating Response----------------" + response);

                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        /*Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction("com.pushnotification.updateBottom_view");
                        sendBroadcast(broadcastIntent);*/

                        session.setCouponCode("", "");
                        session.setReferralCode("", "");

                        final PkDialog mDialog = new PkDialog(MyRideRating.this);
                        mDialog.setDialogTitle(getkey("action_success"));
                        mDialog.setDialogMessage(getkey("my_rides_rating_submit_successfully"));
                        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                /*finish();
                                onBackPressed();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
                                Intent intent = new Intent(MyRideRating.this, Thanksrating.class);
                                intent.putExtra("drivername", driverName);
                                intent.putExtra("driverimg", driverImage);
                                startActivity(intent);
//                                onBackPressed();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            }
                        });
                        mDialog.show();
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }



}
