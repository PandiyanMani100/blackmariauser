package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CountryDialCode;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CircularImageView;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomEdittextCambrialItalic;
import com.blackmaria.widgets.CustomTextCambriaItalic;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.TextRCLight;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.devspark.appmsg.AppMsg;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by user144 on 9/18/2017.
 */
public class NewCloudMoneyTransferHome extends FragmentActivityHockeyApp implements View.OnClickListener {
    private RelativeLayout Rl1;
    private ImageView imgBack;
    private ImageView imgBwallet;
    private CustomTextView txtLabelCurrentBalance;
    private RelativeLayout Rl2,conformBtn,cancel,Rl_help,transferlayout;
    private CustomTextCambriaItalic txtCountryCode;
    private CustomEdittextCambrialItalic edtMobileNo;
    private ImageView indicatingImage;
    private RelativeLayout verifylayout;
    private CircularImageView ratingPageProfilephoto1;
    private CustomTextView userNameTv;
    private CustomTextView userCityTv;
    private CustomTextView userStatusTv;
    private SmoothProgressBar progressbar;
//    private Button cancel;
//    private Button conformBtn;
    CountryPicker picker;
    private LinearLayout bankacc_details;
    private String CountryCode = "";
    private GPSTracker gpsTracker;
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String sSecurePin = "";
    private String UserID = "";
    String user_name = "", user_image = "", city = "", status1 = "", friendId = "";
    private RefreshReceiver refreshReceiver;
    private String strCrossBorderStatus = "";
    private LinearLayout cross_value;
    String mobileno_checkfinal ="";



   private TextRCLight userNameTv1, userCityTv1 , userStatusTv1, transfer_amount_tv,transferfee_tv ,debit_amount_tv,received_amount_tv,conversion_tv,currency_conversion;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyTransferHome.this, Navigation_new.class);
                        startActivity(intent1);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyTransferHome.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyTransferHome.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_transfer_home_page);
        initialize();


        txtCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                txtCountryCode.setText(dialCode);
                CountryCode = dialCode;
                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);

                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(txtCountryCode.getWindowToken(), 0);
            }
        });


    }

    private void initialize() {


        session = new SessionManager(NewCloudMoneyTransferHome.this);
        cd = new ConnectionDetector(NewCloudMoneyTransferHome.this);
        isInternetPresent = cd.isConnectingToInternet();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        sSecurePin = session.getSecurePin();
        Rl1 = (RelativeLayout) findViewById(R.id.Rl_1);
        imgBack = (ImageView) findViewById(R.id.img_back);
        imgBwallet = (ImageView) findViewById(R.id.img_bwallet);
        txtLabelCurrentBalance = (CustomTextView) findViewById(R.id.txt_label_current_balance);
        Rl2 = (RelativeLayout) findViewById(R.id.Rl_2);
        txtCountryCode = (CustomTextCambriaItalic) findViewById(R.id.txt_country_code);
        edtMobileNo = (CustomEdittextCambrialItalic) findViewById(R.id.edt_mobile_no);
        indicatingImage = (ImageView) findViewById(R.id.indicating_image);
        verifylayout = (RelativeLayout) findViewById(R.id.verifylayout);
        ratingPageProfilephoto1 = (CircularImageView) findViewById(R.id.rating_page_profilephoto1);
        userNameTv = (CustomTextView) findViewById(R.id.user_name_tv);
        userCityTv = (CustomTextView) findViewById(R.id.user_city_tv);
        userStatusTv = (CustomTextView) findViewById(R.id.user_status_tv);
        cancel = (RelativeLayout) findViewById(R.id.cancel);
        conformBtn = (RelativeLayout) findViewById(R.id.conform_btn);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_lable));
        Rl_help=(RelativeLayout)findViewById(R.id.clude_help_dialog);
        bankacc_details = (LinearLayout) findViewById(R.id.bankacc_details);
        transferlayout = (RelativeLayout) findViewById(R.id.transferlayout);
        progressbar=(SmoothProgressBar)findViewById(R.id.could_money_transfar_progressbar);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        ImageView Rl_drawer = (ImageView) findViewById(R.id.indicating_image);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);


        gpsTracker = new GPSTracker(NewCloudMoneyTransferHome.this);
        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {

            double MyCurrent_lat = gpsTracker.getLatitude();
            double MyCurrent_long = gpsTracker.getLongitude();

            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {

                    String Str_getCountryCode = addresses.get(0).getCountryCode();
                    if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode);
                        txtCountryCode.setText(Str_countyCode);
                        String drawableName = "flag_"
                                + Str_getCountryCode.toLowerCase(Locale.ENGLISH);

                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        imgBack.setOnClickListener(this);
        cancel.setOnClickListener(this);
        conformBtn.setOnClickListener(this);
        verifylayout.setOnClickListener(this);
        Rl_help.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == cancel) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == conformBtn) {
//            if (strCrossBorderStatus.equalsIgnoreCase("1")) {

            if(mobileno_checkfinal.equalsIgnoreCase(txtCountryCode.getText().toString() + edtMobileNo.getText().toString())) {
                Intent in = new Intent(NewCloudMoneyTransferHome.this, CloudTransferAmountPinEnter.class);
                in.putExtra("user_name", user_name);
                in.putExtra("user_image", user_image);
                in.putExtra("city", city);
                in.putExtra("status1", status1);
                in.putExtra("friendId", friendId);
                in.putExtra("mobileno", txtCountryCode.getText().toString() + edtMobileNo.getText().toString());
                in.putExtra("strCrossBorderStatus", strCrossBorderStatus);
                startActivity(in);
            }else{
                Alert(getResources().getString(R.string.action_error),getResources().getString(R.string.mobile_numinavalid));
            }

//                ConfirmTransferPopUpCrossBorder1();
//            } else {
//                ConfirmTransferPopUp();



//            }

        } else if (v == verifylayout) {
            CloseKeyboardNew();
            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(edtMobileNo.getApplicationWindowToken(), 0);
            transferlayout.setBackgroundColor(Color.parseColor("#70000000"));

            if (txtCountryCode.getText().toString().trim().length() < 0) {

            } else if (!isValidPhoneNumber(edtMobileNo.getText().toString().trim())) {
                erroredit(edtMobileNo, "invalid mobile number");
            } else {
                if (isInternetPresent) {
                    mobileno_checkfinal = txtCountryCode.getText().toString().trim()+edtMobileNo.getText().toString().trim();
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", UserID);
                    jsonParams.put("country_code", txtCountryCode.getText().toString().trim());
                    jsonParams.put("phone_number", edtMobileNo.getText().toString());
                    //jsonParams.put("authentication_id", authentication_id);
                    findBankAccount(Iconstant.find_Friend_Account_url, jsonParams);
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }
            }

        } else if (v == imgBack) {
            CloseKeyboardNew();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }else if(v == Rl_help){


//            Intent in=new Intent(NewCloudMoneyTransferHome.this,DummyActivity.class );
//            startActivity(in);


            CludeHelpDilog();

        }
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void crossBorderCheck(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(NewCloudMoneyTransferHome.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("--------------crossBorderCheck Url-------------------" + Url);
        System.out.println("--------------crossBorderCheck jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyTransferHome.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener()

        {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------crossBorderCheck Response-------------------" + response);
                String sResponse = "", wallet_amount = "", response1 = "";
                String status = "", respose = "", user_name = "", friend_id = "", user_image = "", city = "", user_status = "", transfer_amount = "", transfer_fee = "", debit_amount = "", currency_transfer = "", received_amount = "", currency_received = "", conversion_rate = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        try {
                            status = object.getString("status");
                            if (status.equalsIgnoreCase("1")) {
                                respose = object.getString("response");
                                user_name = object.getString("user_name");
                                friend_id = object.getString("friend_id");
                                user_image = object.getString("user_image");
                                city = object.getString("city");
                                user_status = object.getString("user_status");

                                transfer_amount = object.getString("transfer_amount");
                                transfer_fee = object.getString("transfer_fee");
                                debit_amount = object.getString("debit_amount");

                                currency_transfer = object.getString("currency_transfer");
                                received_amount = object.getString("received_amount");
                                currency_received = object.getString("currency_received");
                                conversion_rate = object.getString("conversion_rate");
                            } else {
                                sResponse = object.getString("response");
                            }

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                    if (status.equalsIgnoreCase("1")) {
                        cross_value.setVisibility(View.VISIBLE);
                        userNameTv1.setText(user_name);
                        userStatusTv1.setText(user_status);
                        userCityTv1.setText(city);

                        transfer_amount_tv.setText(currency_transfer+" "+transfer_amount);
                        transferfee_tv.setText(currency_transfer+" "+transfer_fee);
                        debit_amount_tv.setText(currency_transfer+" "+debit_amount);

                        received_amount_tv.setText(currency_received+" "+received_amount);
                        currency_conversion .setText(getResources().getString(R.string.cinversion)+currency_transfer);
                        conversion_tv.setText(conversion_rate+" "+currency_received);

                    } else {
                        Alert(getResources().getString(R.string.action_error), sResponse);
                    }


                } catch (JSONException e)

                {
                    e.printStackTrace();
                }
                if (dialog != null)

                {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void findBankAccount(String Url, HashMap<String, String> jsonParams) {


//        dialog = new Dialog(NewCloudMoneyTransferHome.this);
//        dialog.getWindow();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.custom_loading);
//        dialog.getWindow().  setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.show();

        progressbar.setVisibility(View.VISIBLE);


//        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
//        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("--------------findBankAccount Url-------------------" + Url);
        System.out.println("--------------findBankAccount jsonParams-------------------" + jsonParams);

        mRequest = new

                ServiceRequest(NewCloudMoneyTransferHome.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener()

        {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------findBankAccount Response-------------------" + response);
                String status = "", sResponse = "", wallet_amount = "", response1 = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        if (status.equalsIgnoreCase("1")) {
                            if (object.has("response")) {
                                JSONObject objectResponse = object.getJSONObject("response");
                                if (objectResponse.length() > 0) {
                                    user_name = objectResponse.getString("user_name");
                                    user_image = objectResponse.getString("user_image");
                                    city = objectResponse.getString("city");
                                    status1 = objectResponse.getString("status");
                                    friendId = objectResponse.getString("user_id");
                                    strCrossBorderStatus = objectResponse.getString("cross_border");
                                }
                            }
                        } else {
                            sResponse = object.getString("response");

                        }
                        if (status.equalsIgnoreCase("1")) {
                            bankacc_details.setVisibility(View.VISIBLE);
                            transferlayout.setBackgroundColor(Color.parseColor("#70000000"));
                            if (!user_image.equalsIgnoreCase("")) {
                                Picasso.with(NewCloudMoneyTransferHome.this).load(String.valueOf(user_image)).into(ratingPageProfilephoto1);
                            }
                            userNameTv.setText(user_name);
                            userStatusTv.setText(status1);
                            userCityTv.setText(city);



//                            if (dialog != null) {
//                                dialog.dismiss();
//                            }


                            progressbar.setVisibility(View.GONE);

                        } else {
                            Alert(getResources().getString(R.string.action_error), sResponse);
                            bankacc_details.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                if (dialog != null) {
//                    dialog.dismiss();
//                }

                progressbar.setVisibility(View.GONE);

            }

            @Override
            public void onErrorListener() {
//                if (dialog != null) {
//                    dialog.dismiss();
//                }

                progressbar.setVisibility(View.GONE);

            }
        });
    }

    private void ConfirmTransferPopUpCrossBorder1() {
        final Dialog dialog = new Dialog(NewCloudMoneyTransferHome.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cross_border_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button sendNow = (Button) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);

        final CustomEdittext inserAmountTv = (CustomEdittext) dialog.findViewById(R.id.custom_dialog_library_message_textview);
        cross_value= (LinearLayout) dialog.findViewById(R.id.cross_value);

         userNameTv1 = (TextRCLight) dialog.findViewById(R.id.user_name_tv);
         userCityTv1 = (TextRCLight) dialog.findViewById(R.id.user_city_tv);
         userStatusTv1 = (TextRCLight) dialog.findViewById(R.id.user_status_tv);

         transfer_amount_tv = (TextRCLight) dialog.findViewById(R.id.transfer_amount_tv);
         transferfee_tv = (TextRCLight) dialog.findViewById(R.id.transferfee_tv);
         debit_amount_tv = (TextRCLight) dialog.findViewById(R.id.debit_amount_tv);

         received_amount_tv = (TextRCLight) dialog.findViewById(R.id.received_amount_tv);
         conversion_tv = (TextRCLight) dialog.findViewById(R.id.conversion_tv);
         currency_conversion = (TextRCLight) dialog.findViewById(R.id.currency_conversion);

        userNameTv1.setText(user_name);
        userStatusTv1.setText(status1);
        userCityTv1.setText(city);

        final PinEntryEditText Ed_pin = (PinEntryEditText) dialog.findViewById(R.id.edt_withdrawal_amount);
        Ed_pin.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                }
                return false;
            }
        });
        Ed_pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                System.out.println("========== Muruga pincode count==============" + Ed_pin.getText().toString().trim().length());
                if (Ed_pin.getText().toString().trim().length()==6) {
                    if (inserAmountTv.getText().toString().trim().length() <= 0) {
                        erroredit(inserAmountTv, getResources().getString(R.string.insert_transfer_amount));
                    }  else if (!sSecurePin.equalsIgnoreCase(Ed_pin.getText().toString())) {
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.label_incorrect_pin));
                    } else {
                        if (isInternetPresent) {
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("user_id", UserID);
                            jsonParams.put("transfer_amount", inserAmountTv.getText().toString().trim());
                            jsonParams.put("friend_id", friendId);
                            //jsonParams.put("authentication_id", authentication_id);
                            crossBorderCheck(Iconstant.cloudmoney_crossbordervalue_Url, jsonParams);
                        } else {
                            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                        }
                    }
                }else{
                    cross_value.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ImageView close = (ImageView) dialog.findViewById(R.id.close_icon);


        ImageView Rl_drawer = (ImageView) dialog.findViewById(R.id.indicating_image);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        sendNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin = Ed_pin.getText().toString().trim();
                if (inserAmountTv.getText().toString().trim().length() <= 0) {
                    erroredit(inserAmountTv, getResources().getString(R.string.insert_transfer_amount));
                } else if (inserAmountTv.getText().toString().trim().equalsIgnoreCase(".")) {
                    erroredit(inserAmountTv, getResources().getString(R.string.insert_transfer_amount));
                } else if (!sSecurePin.equalsIgnoreCase(pin)) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.label_incorrect_pin));
                } else {
                    dialog.dismiss();
                    Intent intent = new Intent(NewCloudMoneyTransferHome.this, NewCloudMoneyTransferUrlProgress.class);
                    intent.putExtra("flag", "0");
                    intent.putExtra("reloadamount", inserAmountTv.getText().toString().trim());
                    intent.putExtra("friendId", friendId);
                    intent.putExtra("crosstransfer", strCrossBorderStatus);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void ConfirmTransferPopUp() {
        final Dialog dialog = new Dialog(NewCloudMoneyTransferHome.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_cloud_transfer_withdraw_otp_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button sendNow = (Button) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);

        final CustomEdittext inserAmountTv = (CustomEdittext) dialog.findViewById(R.id.custom_dialog_library_message_textview);
        TextRCLight userNameTv = (TextRCLight) dialog.findViewById(R.id.user_name_tv);
        TextRCLight userCityTv = (TextRCLight) dialog.findViewById(R.id.user_city_tv);
        TextRCLight userStatusTv = (TextRCLight) dialog.findViewById(R.id.user_status_tv);
        final PinEntryEditText Ed_pin = (PinEntryEditText) dialog.findViewById(R.id.edt_withdrawal_amount);
        ImageView close = (ImageView) dialog.findViewById(R.id.close_icon);

        userNameTv.setText(user_name);
        userStatusTv.setText(status1);
        userCityTv.setText(city);

        ImageView Rl_drawer = (ImageView) dialog.findViewById(R.id.indicating_image);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        sendNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin = Ed_pin.getText().toString().trim();
                if (inserAmountTv.getText().toString().trim().length() <= 0) {
                    erroredit(inserAmountTv, getResources().getString(R.string.insert_transfer_amount));
                } else if (inserAmountTv.getText().toString().trim().equalsIgnoreCase(".")) {
                    erroredit(inserAmountTv, getResources().getString(R.string.insert_transfer_amount));
                } else if (!sSecurePin.equalsIgnoreCase(pin)) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.label_incorrect_pin));
                } else {
                    dialog.dismiss();
                    Intent intent = new Intent(NewCloudMoneyTransferHome.this, NewCloudMoneyTransferUrlProgress.class);
                    intent.putExtra("flag", "0");
                    intent.putExtra("reloadamount", inserAmountTv.getText().toString().trim());
                    intent.putExtra("friendId", friendId);
                    intent.putExtra("crosstransfer", strCrossBorderStatus);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();

                }

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(NewCloudMoneyTransferHome.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewCloudMoneyTransferHome.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5 || target.length() > 15) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }
    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyTransferHome.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }

    @Override
    protected void onDestroy() {
        CloseKeyboardNew();
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }


    private void CludeHelpDilog(){

        final Dialog dialog = new Dialog(NewCloudMoneyTransferHome.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloud_help_dialog5);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final RelativeLayout cancel=(RelativeLayout) dialog.findViewById(R.id.dig_cancel);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });





        dialog.show();


    }



}