package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;
import com.blackmaria.adapter.EwalletWithdrawalAdapter;
import com.blackmaria.adapter.XenditBankDetailsAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.WalletMoneyPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittextCambrialItalic;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 9/18/2017.
 */

public class NewCloudMoneyWithDrawHome extends ActivityHockeyApp implements View.OnClickListener {
    private RelativeLayout Rl1;
    private ImageView imgBack;
    private ImageView imgBwallet;
    private TextView txtLabelCurrentBalance, lastWithdrawlDate;
    private RelativeLayout Rl2;
    private CustomEdittextCambrialItalic insertAmount;
    private ImageView indicatingImage;
    private RelativeLayout verifylayout;
    private RelativeLayout paymentLyt;
    private GridView walletPagePaymentListGridview;
    private CustomEdittextCambrialItalic Ed_paypal_id;
    private SessionManager session;
    private TextView help_tv;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String UserID = "", Savil_amount = "", bankingInfo = "", Scurrency = "", Slast_withdrawwl = "", Swithdrawel_status = "", Swithdrawel_status_txt = "";
    ArrayList<WalletMoneyPojo> paymentcardlist;
    ArrayList<WalletMoneyPojo> paymentcardlist1;
    private Boolean isDatavailable = false;
    private Boolean isDatavailable1 = false;
    private Boolean isPaymentListAvailable = false;
    private Boolean isPaymentListAvailable1 = false;
    EwalletWithdrawalAdapter Eadaptter;
    XenditBankDetailsAdapter Eadaptter1;
    private int Listposition = 0;

    private RefreshReceiver refreshReceiver;
    String bankImageUrl = "";
    String accHolderName = "", holderAccountNumber = "";
    private String sSecurePin = "", paymentMode = "", strPaypal = "";

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyWithDrawHome.this, Navigation_new.class);
                        startActivity(intent1);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyWithDrawHome.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyWithDrawHome.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_transfer_withdraw_home_page_new);

        initialize();

        walletPagePaymentListGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (Swithdrawel_status.equalsIgnoreCase("1")) {

                    Listposition = position;
                    if (insertAmount.getText().toString().equalsIgnoreCase("")) {
                        erroredit(insertAmount, getResources().getString(R.string.plsenetrtwidthrwd));
                    } else if (insertAmount.getText().toString().equalsIgnoreCase("0")) {
                        erroredit(insertAmount, getResources().getString(R.string.invalidamount));
                    } else {
                        for (int i = 0; i < paymentcardlist.size(); i++) {
                            if (i == position) {
                                paymentcardlist.get(i).setPayment_selected_payment_id("true");
                            } else {
                                paymentcardlist.get(i).setPayment_selected_payment_id("false");
                            }
                        }
                        Eadaptter.notifyDataSetChanged();
                        if (paymentcardlist.get(position).getPaymnet_name().equalsIgnoreCase("QR-code")) {
                            paymentMode = paymentcardlist.get(position).getPayment_code();
                            Alert(getResources().getString(R.string.sorry_qr_paymmentnotavailabel), "Please select other option");

                        } else if (paymentcardlist.get(position).getPaymnet_name().equalsIgnoreCase("Paypal")) {
                            strPaypal = paymentcardlist.get(position).getPaymnet_name().trim();
                            // GetPayPalIdDialog();
                            Intent in = new Intent(NewCloudMoneyWithDrawHome.this, PayPalTransferPage.class);
                            in.putExtra("withdrawel_status", Swithdrawel_status);
                            in.putExtra("payment_code", paymentcardlist.get(Listposition).getPayment_code());
                            in.putExtra("amount", insertAmount.getText().toString());
                            in.putExtra("currency", Scurrency);
                            in.putExtra("Swithdrawel_status_txt", Swithdrawel_status_txt);
                            startActivity(in);
                            finish();
                        } else if (paymentcardlist.get(position).getPaymnet_name().equalsIgnoreCase("xendit-banktransfer")) {
                            paymentMode = paymentcardlist.get(position).getPayment_code();
                            if (bankingInfo.equalsIgnoreCase("0")) {
                                ChooseBankUpdate();
                            } else {
                                Intent bankIntent = new Intent(NewCloudMoneyWithDrawHome.this, NewCloudMoneyWithDrawBankPage.class);
                                bankIntent.putExtra("transfer_amount", insertAmount.getText().toString().trim());
                                bankIntent.putExtra("paymentMode", paymentcardlist.get(position).getPayment_code());
                                bankIntent.putExtra("flag", "1");
                                startActivity(bankIntent);
                                overridePendingTransition(R.anim.enter, R.anim.exit);
                                finish();
                            }


                            //  finish();

                        } else if (paymentcardlist.get(position).getPaymnet_name().equalsIgnoreCase("banktransfer")) {
                            Alert("", getResources().getString(R.string.new_wallet_lable_cash_tv1));
                        } else if (paymentcardlist.get(position).getPaymnet_name().equalsIgnoreCase("cash")) {
                            if (isInternetPresent) {
                                if (Swithdrawel_status.equalsIgnoreCase("1")) {
                                    strPaypal = "";
                                    //confirmPin();
                                    Intent in = new Intent(NewCloudMoneyWithDrawHome.this, CashOutActivity.class);
                                    in.putExtra("currency", Scurrency);
                                    in.putExtra("payment_code", paymentcardlist.get(Listposition).getPayment_code());
                                    in.putExtra("amount", insertAmount.getText().toString().trim());
                                    startActivity(in);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                    finish();
                                } else {
                                    Alert(getResources().getString(R.string.action_error), Swithdrawel_status_txt);
                                }
                            } else {
                                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                            }
                        }
                    }
                } else {
                    Alert(getResources().getString(R.string.action_error), Swithdrawel_status_txt);
                }
            }
        });


    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    private void initialize() {


        session = new SessionManager(NewCloudMoneyWithDrawHome.this);
        cd = new ConnectionDetector(NewCloudMoneyWithDrawHome.this);
        isInternetPresent = cd.isConnectingToInternet();
        paymentcardlist = new ArrayList<WalletMoneyPojo>();
        paymentcardlist1 = new ArrayList<WalletMoneyPojo>();
        sSecurePin = session.getSecurePin();
        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);


        Rl1 = (RelativeLayout) findViewById(R.id.Rl_1);
        imgBack = (ImageView) findViewById(R.id.img_back);
        imgBwallet = (ImageView) findViewById(R.id.img_bwallet);
        txtLabelCurrentBalance = (TextView) findViewById(R.id.txt_label_current_balance);
        Rl2 = (RelativeLayout) findViewById(R.id.Rl_2);
        insertAmount = (CustomEdittextCambrialItalic) findViewById(R.id.insert_amount);
        indicatingImage = (ImageView) findViewById(R.id.indicating_image);
        verifylayout = (RelativeLayout) findViewById(R.id.verifylayout);
        paymentLyt = (RelativeLayout) findViewById(R.id.payment_lyt);
        lastWithdrawlDate = (TextView) findViewById(R.id.last_withdrawl_date);
        walletPagePaymentListGridview = (GridView) findViewById(R.id.wallet_page_payment_list_gridview);
        help_tv = (TextView) findViewById(R.id.help_tv);
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        imgBack.setOnClickListener(this);
        verifylayout.setOnClickListener(this);
        help_tv.setOnClickListener(this);


        if (isInternetPresent) {

            PostRquestForEwalletDetails(Iconstant.Ewallet_withdrawal_url);
        } else {

            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
        }


    }

    @Override
    public void onClick(View v) {
        if (v == imgBack) {
            CloseKeyboardNew();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == help_tv) {
            infoPage();
        } else if (v == verifylayout) {
            CloseKeyboardNew();
            Intent intent = new Intent(NewCloudMoneyWithDrawHome.this, WalletMoneyPage1.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeBankDetails(final String flag) {


        final Dialog dialog = new Dialog(NewCloudMoneyWithDrawHome.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_cloud_transfer_withdraw_changebank_popup);
        ImageView close_imag = (ImageView) dialog.findViewById(R.id.close_imag);
        final CustomEdittextCambrialItalic name = (CustomEdittextCambrialItalic) dialog.findViewById(R.id.fullname);
        final CustomEdittextCambrialItalic accNum = (CustomEdittextCambrialItalic) dialog.findViewById(R.id.accountnum);
        final TextView help_tv = (TextView) dialog.findViewById(R.id.help_tv);

        help_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                infoPage3();
            }
        });
        GridView grid_view = (GridView) dialog.findViewById(R.id.grid_view);
        close_imag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (name.getText().toString().trim().length() <= 0) {
                    erroredit(name, getResources().getString(R.string.valid_card_holder_name));
                } else if (accNum.getText().toString().trim().length() <= 0) {
                    erroredit(accNum, getResources().getString(R.string.valid_card_no_enter));
                } else {

                    String bankCode = paymentcardlist1.get(position).getXenditBankCode();
                    String bankName = paymentcardlist1.get(position).getXenditBankName();
                    bankImageUrl = paymentcardlist1.get(position).getXendiActiveImage();
                    accHolderName = name.getText().toString().trim();
                    holderAccountNumber = accNum.getText().toString().trim();

                    if (flag.equalsIgnoreCase("2")) {
                        if (!insertAmount.getText().toString().trim().equalsIgnoreCase("0")) {
                            Intent bankIntent = new Intent(NewCloudMoneyWithDrawHome.this, NewCloudMoneyWithDrawBankPage.class);
                            bankIntent.putExtra("transfer_amount", insertAmount.getText().toString().trim());
                            bankIntent.putExtra("paymentMode", paymentMode);
                            bankIntent.putExtra("bankCode", bankCode);
                            bankIntent.putExtra("accHolderName", accHolderName);
                            bankIntent.putExtra("accHolderNumber", holderAccountNumber);
                            bankIntent.putExtra("flag", "2");
                            startActivity(bankIntent);
                            dialog.dismiss();
                        } else {
                            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.conotproceedamount));
                        }
                    } else {
                        if (isInternetPresent) {
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("user_id", UserID);
                            jsonParams.put("acc_holder_name", accHolderName);
                            jsonParams.put("acc_number", holderAccountNumber);
                            jsonParams.put("bank_name", bankName);
                            jsonParams.put("bank_code", bankCode);
                            PostRquestXenditBanDetailSave(Iconstant.getXendit_bank_save_url, jsonParams);
                            dialog.dismiss();
                        } else {

                            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                        }
                    }


                }


            }
        });
        if (isPaymentListAvailable1) {

            Eadaptter1 = new XenditBankDetailsAdapter(NewCloudMoneyWithDrawHome.this, paymentcardlist1);
            grid_view.setAdapter(Eadaptter1);

        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void PostRquestXenditBanDetailSave(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(NewCloudMoneyWithDrawHome.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------PostRquestXenditBanDetailSave jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyWithDrawHome.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------PostRquestXenditBanDetailSave reponse-------------------" + response);

                String Sstatus = "", Smessage = "", acc_holder_name = "", acc_number = "", bank_name = "", bank_code = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");
                        if (jsonObject.has("banking")) {

                            JSONObject banking = jsonObject.getJSONObject("banking");
                            if (banking.length() > 0) {
                                acc_holder_name = banking.getString("acc_holder_name");
                                acc_number = banking.getString("acc_number");
                                bank_name = banking.getString("bank_name");
                                bank_code = banking.getString("bank_code");
                            }

                        }

                        dialogDismiss();

                    } else {
                        Smessage = object.getString("response");
                        dialogDismiss();
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {
                    if (!insertAmount.getText().toString().trim().equalsIgnoreCase("0")) {
                        Intent bankIntent = new Intent(NewCloudMoneyWithDrawHome.this, NewCloudMoneyWithDrawBankPage.class);
                        bankIntent.putExtra("transfer_amount", insertAmount.getText().toString().trim());
                        bankIntent.putExtra("paymentMode", paymentMode);

                        bankIntent.putExtra("flag", "1");
                        startActivity(bankIntent);
                    } else {
                        Alert(getResources().getString(R.string.action_error), "Cannot proceed this Amount");
                    }
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });


    }


    private void ChooseBankUpdate() {
        final Dialog dialog = new Dialog(NewCloudMoneyWithDrawHome.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.update_profile_or_temp_bankvalues);
        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        Button update = (Button) dialog.findViewById(R.id.update_profile);
        Button continueUpadate = (Button) dialog.findViewById(R.id.continue_update);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeBankDetails("1");
                dialog.dismiss();
            }
        });
        continueUpadate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeBankDetails("2");
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void infoPage3() {
        final Dialog dialog = new Dialog(NewCloudMoneyWithDrawHome.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloud_help_dialog_bank);
        // Button sendNow = (Button) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        RelativeLayout close_imag = (RelativeLayout) dialog.findViewById(R.id.dig_cancel);

        close_imag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void infoPage() {
        final Dialog dialog = new Dialog(NewCloudMoneyWithDrawHome.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloud_help_dialog);
        // Button sendNow = (Button) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        ImageView close_imag = (ImageView) dialog.findViewById(R.id.close_imag);

        close_imag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

       /* sendNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewCloudMoneyWithDrawHome.this, NewCloudMoneyTransferUrlProgress.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                dialog.dismiss();

            }
        });*/
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void GetPayPalIdDialog() {


        final Dialog dialog = new Dialog(NewCloudMoneyWithDrawHome.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_cloud_transfer_withdraw_reload_paypal_popup);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        ImageView closeImage = (ImageView) dialog.findViewById(R.id.img_close);
        Ed_paypal_id = (CustomEdittextCambrialItalic) dialog.findViewById(R.id.email_et1);
        CustomTextView ok_btn = (CustomTextView) dialog.findViewById(R.id.paypal_conform);

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(Ed_paypal_id);
                dialog.dismiss();
            }
        });

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CloseKeyboard(Ed_paypal_id);
                if (insertAmount.getText().toString().length() == 0) {

                    dialog.dismiss();
                    erroredit(insertAmount, getResources().getString(R.string.enteramount));


                } else if (insertAmount.getText().toString().equals("0")) {
                    dialog.dismiss();
                    erroredit(insertAmount, getResources().getString(R.string.entervalid));

                } else if (Ed_paypal_id.getText().toString().length() == 0) {
                    dialog.dismiss();
                    erroredit(Ed_paypal_id, getResources().getString(R.string.enterpaidid));

                } else {


                    dialog.dismiss();

                    cd = new ConnectionDetector(NewCloudMoneyWithDrawHome.this);
                    isInternetPresent = cd.isConnectingToInternet();

                    if (isInternetPresent) {

                        if (Swithdrawel_status.equalsIgnoreCase("1")) {

                            confirmPin();

                        } else {

                            Alert(getResources().getString(R.string.action_error), Swithdrawel_status_txt);
                        }


                    } else {

                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

                    }
                }
            }
        });
        dialog.show();
    }

    private void PostRquestForEwalletDetails(String Url) {
        dialog = new Dialog(NewCloudMoneyWithDrawHome.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        System.out.println("-----------E Wallet WithDrawal url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);


        System.out.println("------------E Wallet WithDrawal jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(NewCloudMoneyWithDrawHome.this);

        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------E Wallet WithDrawal jsonParams--------------" + response);

                String Ssatus = "";


                try {

                    JSONObject object = new JSONObject(response);
                    Ssatus = object.getString("status");

                    if (Ssatus.equalsIgnoreCase("1")) {


                        JSONObject jsondata = object.getJSONObject("response");

                        if (jsondata.length() > 0) {

                            Savil_amount = jsondata.getString("available_amount");
                            Scurrency = jsondata.getString("currency");
                            Slast_withdrawwl = jsondata.getString("last_withdrawal_txt");
                            Swithdrawel_status = jsondata.getString("proceed_status");
                            Swithdrawel_status_txt = jsondata.getString("error_message");
                            bankingInfo = jsondata.getString("banking_info");

                            Object check_object = jsondata.get("payment");

                            if (check_object instanceof JSONArray) {
                                JSONArray payment_list_jsonArray = jsondata.getJSONArray("payment");


                                if (payment_list_jsonArray.length() > 0) {

                                    paymentcardlist.clear();


                                    for (int i = 0; i < payment_list_jsonArray.length(); i++) {

                                        JSONObject payment_obj = payment_list_jsonArray.getJSONObject(i);

                                        WalletMoneyPojo wmpojo = new WalletMoneyPojo();

                                        wmpojo.setPaymnet_name(payment_obj.getString("name"));
                                        wmpojo.setPayment_code(payment_obj.getString("code"));
                                        wmpojo.setPayment_normal_img(payment_obj.getString("inactive_icon"));
                                        wmpojo.setPayment_active_img(payment_obj.getString("icon"));
                                        wmpojo.setPayment_selected_payment_id("false");

                                        paymentcardlist.add(wmpojo);

                                    }


                                    isPaymentListAvailable = true;

                                } else {

                                    isPaymentListAvailable = false;

                                }


                            }


                            isDatavailable = true;

                        } else {

                            isDatavailable = false;

                        }


                    }


                    dialogDismiss();
                    try {
                        PostRquestXenditBankList(Iconstant.getPaymentOPtion_url_new);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (Ssatus.equalsIgnoreCase("1") && isDatavailable) {

                        // Tv_currency_code.setText(Scurrency);
                        //  Tv_last_trip.setText(Slast_withdrawwl);
                        if (!Slast_withdrawwl.equalsIgnoreCase("")) {
                            lastWithdrawlDate.setText("LAST WITHDRAWAL " + Slast_withdrawwl);
                        } else {
                            lastWithdrawlDate.setText(getResources().getString(R.string.wallet_money_lable_transaction_lastwithdrawl));
                        }
                        if (isPaymentListAvailable) {

                            Eadaptter = new EwalletWithdrawalAdapter(NewCloudMoneyWithDrawHome.this, paymentcardlist);
                            walletPagePaymentListGridview.setAdapter(Eadaptter);

                        }

                    } else {
                        lastWithdrawlDate.setText(getResources().getString(R.string.wallet_money_lable_transaction_lastwithdrawl));
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                    dialogDismiss();
                }


            }

            @Override
            public void onErrorListener() {
                dialogDismiss();

            }
        });


    }

    private void PostRquestForConfirm(String Url) {
        if (!insertAmount.getText().toString().trim().equalsIgnoreCase("0")) {
            dialog = new Dialog(NewCloudMoneyWithDrawHome.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_loading);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            System.out.println("-----------CloudMoney withdraw url--------------" + Url);
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", UserID);
            jsonParams.put("amount", insertAmount.getText().toString());
            jsonParams.put("mode", paymentcardlist.get(Listposition).getPayment_code());

            jsonParams.put("bank_code", "");
            jsonParams.put("acc_holder_name", "");
            jsonParams.put("acc_number", "");

            if (strPaypal.equalsIgnoreCase("Paypal")) {
                jsonParams.put("paypal_id", Ed_paypal_id.getText().toString());
            }
            System.out.println("------------CloudMoney withdraw jsonParams--------------" + jsonParams);
            mRequest = new ServiceRequest(NewCloudMoneyWithDrawHome.this);

            mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

                @Override
                public void onCompleteListener(String response) {

                    System.out.println("------------CloudMoney withdraw response--------------" + response);

                    String Ssatus = "";

                    try {

                        JSONObject object = new JSONObject(response);
                        Ssatus = object.getString("status");


                        dialogDismiss();

                        if (Ssatus.equalsIgnoreCase("1")) {

                            String Sresponse = object.getString("response");
                            insertAmount.setText("");
                            Alert(getResources().getString(R.string.pushnotification_alert_label_ride_arrived_success), Sresponse);

                        } else {
                            String Sresponse = object.getString("response");
                            Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        dialogDismiss();
                    }


                }

                @Override
                public void onErrorListener() {
                    dialogDismiss();

                }
            });
        } else {
            Alert(getResources().getString(R.string.action_error), "Cannot proceed this Amount");
        }

    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewCloudMoneyWithDrawHome.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setDialogMessageSize(13);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewCloudMoneyWithDrawHome.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    private void confirmPin() {
        final Dialog confirmPinDialog = new Dialog(NewCloudMoneyWithDrawHome.this, R.style.DialogSlideAnim);
        confirmPinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmPinDialog.setCancelable(true);
        confirmPinDialog.setContentView(R.layout.alert_enter_pin);

        final PinEntryEditText Ed_pin = (PinEntryEditText) confirmPinDialog.findViewById(R.id.edt_withdrawal_amount);
        final TextView Tv_forgotPin = (TextView) confirmPinDialog.findViewById(R.id.txt_label_forgot_pin);
        final RelativeLayout RL_confirm = (RelativeLayout) confirmPinDialog.findViewById(R.id.Rl_confirm);

        Tv_forgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    ForgotPinRequest(Iconstant.forgot_pin_url);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        });

        RL_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    CloseKeyboard(Ed_pin);
                } catch (Exception e) {

                }

                if (confirmPinDialog != null && confirmPinDialog.isShowing()) {
                    confirmPinDialog.dismiss();
                }

                if (sSecurePin.equalsIgnoreCase(Ed_pin.getText().toString())) {

                    if (isInternetPresent) {
                        PostRquestForConfirm(Iconstant.Ewallet_withdrawal_confirm_url);

                    } else {

                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

                    }
                    if (confirmPinDialog != null && confirmPinDialog.isShowing()) {
                        confirmPinDialog.dismiss();
                    }


                } else {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.label_incorrect_pin));
                }

            }
        });

        confirmPinDialog.show();

    }

    //--------------Close KeyBoard Method-----------
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(NewCloudMoneyWithDrawHome.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    @SuppressLint("WrongConstant")
    private void ForgotPinRequest(String Url) {

        dialog = new Dialog(NewCloudMoneyWithDrawHome.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);

        System.out.println("--------------ForgotPinRequest Url-------------------" + Url);
        System.out.println("--------------ForgotPinRequest jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyWithDrawHome.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                System.out.println("--------------ForgotPinRequest Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("message");

                        if (status.equalsIgnoreCase("1")) {
                            Alert(getResources().getString(R.string.action_success), sResponse);
                        } else {
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void PostRquestXenditBankList(String Url) {

        final Dialog dialog = new Dialog(NewCloudMoneyWithDrawHome.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------Xendit bank url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);


        System.out.println("------------Xendit bank jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(NewCloudMoneyWithDrawHome.this);

        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {


            @Override
            public void onCompleteListener(String response) {


                System.out.println("------------Xendit bank jsonParams--------------" + response);

                String Ssatus = "";


                try {

                    JSONObject jsondata = new JSONObject(response);
                    Ssatus = jsondata.getString("status");

                    if (Ssatus.equalsIgnoreCase("1")) {


                        // JSONObject jsondata = object.getJSONObject("response");

                        Object check_object = jsondata.get("bank_list");

                        if (check_object instanceof JSONArray) {


                            JSONArray payment_list_jsonArray = jsondata.getJSONArray("bank_list");


                            if (payment_list_jsonArray.length() > 0) {

                                paymentcardlist1.clear();


                                for (int i = 0; i < payment_list_jsonArray.length(); i++) {

                                    JSONObject payment_obj = payment_list_jsonArray.getJSONObject(i);

                                    WalletMoneyPojo wmpojo = new WalletMoneyPojo();

                                    wmpojo.setXenditBankName(payment_obj.getString("name"));
                                    wmpojo.setXenditBankCode(payment_obj.getString("code"));
                                    //   wmpojo.setPayment_normal_img(payment_obj.getString("inactive_icon"));
                                    wmpojo.setXendiActiveImage(payment_obj.getString("image"));
                                    //  wmpojo.setPayment_selected_payment_id("false");

                                    paymentcardlist1.add(wmpojo);

                                }


                                isPaymentListAvailable1 = true;

                            } else {

                                isPaymentListAvailable1 = false;

                            }


                        }


                        isDatavailable1 = true;


                    }


                    if (dialog != null) {
                        dialog.dismiss();
                    }


                    if (Ssatus.equalsIgnoreCase("1") && isDatavailable) {

                        // Tv_currency_code.setText(Scurrency);
                        //  Tv_last_trip.setText(Slast_withdrawwl);


                    } else {

                       /* String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
*/
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }


            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    protected void onDestroy() {
        CloseKeyboardNew();
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }
}
