package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.FareSummeryAdapter;
import com.blackmaria.adapter.Multistoplistadapter;
import com.blackmaria.adapter.RideDetailsAdapter;
import com.blackmaria.adapter.RideDetailsAdapter_new;
import com.blackmaria.pojo.CancelTripPojo;
import com.blackmaria.pojo.ComplaintsPojo;
import com.blackmaria.pojo.RideDetailPojo;
import com.blackmaria.pojo.RideDetails_DisplayPojo;
import com.blackmaria.pojo.multistop_pojo;
import com.blackmaria.subclass.ActivitySubClass;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.countrycodepicker.CountryPicker;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by user14 on 12/14/2016.
 */

public class RideDetailPage extends ActivitySubClass {
    private SessionManager session;
    private ImageView back, carimage;
    private TextView tripdate, pickupvalue, dropoffvalue, bookingnovalue, pickuptimevalue, dropofftimevalue, distancevalue, durationvalue, vehiclevalue, drivernamevalue, vehiclenumbervalue, farevalue, fareinfo, sendreciept, sendcomplaint;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    //    private ExpandableHeightListView Lv_ride_detail, Lv_ride_fare_info;
    private ServiceRequest rateCard_mRequest;
    private Dialog dialog;
    ArrayList<RideDetailPojo> itemlist;
    ArrayList<RideDetails_DisplayPojo> rate_itemList;
    ArrayList<RideDetails_DisplayPojo> freRate_itemList;
    ArrayList<RideDetails_DisplayPojo> details_itemList;
    private boolean isDataAvailable = false;
    private String ScurrencySymbol = "";
    private RideDetailsAdapter details_adapter;
    RideDetailsAdapter_new fare_adapter;
    private boolean isPickUpAvailable = false;
    private boolean isSummaryAvailable = false, isFareAvailable = false;
    private boolean isDropAvailable = false;
    private String UserID = "", SrideId = "";
    private String completeStatus = "", CompleteStage = "";
    //    private LinearLayout detailtrip_lyut, Ll_cancelTrip, Ll_payment, Ll_mailInvoice, Ll_trackRide, Ll_share_Ride, Ll_reportIssue,bottom_layout;
    private RelativeLayout Rl_button;
    private ArrayList<CancelTripPojo> itemlist_reason;
    private boolean isReasonAvailable = false;
    private ServiceRequest mRequest;

    public static Activity rideDetailPage_class;
    private MaterialDialog invoice_dialog;
    private EditText Et_dialog_InvoiceEmail;
    private AppInfoSessionManager SappInfo_Session;
    // private MaterialDialog completejob_dialog;
    //private RelativeLayout Rl_countryCode, tripdetail_header_back_layout;
    private TextView Tv_countryCode;
    CountryPicker picker;
    private EditText Et_share_trip_mobileno;
    private CustomTextView fareInformation_Tv, openSupportTicket_Tv, TV_TripStatus;
    private boolean hasData = false;
    FareSummeryAdapter adapter;
    private CustomTextView openSupportticket;
    private Dialog dialog1;
    private String referalUserId = "";
    private String driverRequestMode = "", paymentMode = "", RideStatus = "", onGoingRideId = "", ride_amount = "", currencyconverion = "", xendit_key = "", currency = "";
    //Complaint detail
    String userName = "", userImage = "", userLocation = "", todayDate;
    private ArrayList<ComplaintsPojo> complaint_itemList;
    private ArrayList<String> complaint_itemList1;
    private ArrayList<String> complaint_itemList2;
    private boolean isComplaintsAvailable = false;
    private String rideId = "", rideStatus = "";
    private ExpandableHeightListView multistoplist;
    private String page = "";
    private String Shide = "";
    private LinearLayout fareinfolinear,tripinfolinear;
    LanguageDb mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_detail_page_constrain);
        rideDetailPage_class = RideDetailPage.this;
        mhelper=new LanguageDb(this);
        initialize();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page.equalsIgnoreCase("MenuUserratingHOme")) {
                    Intent intent = new Intent(RideDetailPage.this, MenuUserratingHOme.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else if (page.equalsIgnoreCase("MenuUserratingList")) {
                    Intent intent = new Intent(RideDetailPage.this, MenuRatingListActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                } else if (page.equalsIgnoreCase("RideListHomepage")) {
                    Intent intent = new Intent(RideDetailPage.this, RideListHomePage.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else if (page.equalsIgnoreCase("RideList")) {
                    Intent intent = new Intent(RideDetailPage.this, RideList.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else if (page.equalsIgnoreCase("CloudMoneyCreditDebit")) {
                    Intent intent = new Intent(RideDetailPage.this, CloudMoneyCreditDebit.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Intent intent = new Intent(RideDetailPage.this, RideList.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }

            }
        });
        sendreciept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mailInvoice();
            }
        });

//        Ll_reportIssue.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mailInvoice();
//            }
//        });

//        Ll_trackRide.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Move to ride Detail page
//
//                if (CompleteStage.equalsIgnoreCase("onprocess")) {
//                    Intent i = new Intent(RideDetailPage.this, WaitingTimePage.class);
//                    i.putExtra("ride_id", SrideId);
//                    i.putExtra("isContinue", true);
//                    startActivity(i);
//                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                    finish();
//                } else {
//
//                    if (RideList.active) {
//                        System.out.println("=============active==========" + RideList.active);
//                        RideList.rideList_class.finish();
//                        Intent i = new Intent(RideDetailPage.this, TrackRidePage.class);
//                        i.putExtra("ride_id", SrideId);
//                        startActivity(i);
//                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                        finish();
//                    } else {
//                        System.out.println("=============active1==========" + RideList.active);
//                        Intent i = new Intent(RideDetailPage.this, TrackRidePage.class);
//                        i.putExtra("ride_id", SrideId);
//                        startActivity(i);
//                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                        finish();
//                    }
//                }
//
//            }
//        });

//
//        Ll_share_Ride.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                shareTrip();
//
//            }
//        });

        fareinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fareSummeryShow();

            }
        });

        sendcomplaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RideDetailPage.this, CreateNew_Complaint.class);
                intent.putExtra("user_image", userImage);
                intent.putExtra("location_name", userLocation);
                intent.putExtra("user_name", userName);
                intent.putExtra("today_datetime", todayDate);
                intent.putExtra("ride_id", rideId);
                intent.putExtra("valuesfrom", "RideDetailPage");
                intent.putStringArrayListExtra("complaintsubjects", complaint_itemList1);
                intent.putStringArrayListExtra("complaintoptionid", complaint_itemList2);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

    }

    private void fareSummeryShow() {
        final Dialog dialog = new Dialog(RideDetailPage.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.faresummery_popup);

        final CustomTextView Tv_close = (CustomTextView) dialog.findViewById(R.id.fare_close_textview);
        Tv_close.setText(getkey("profile_label_close"));
        ExpandableHeightListView overview_listView = (ExpandableHeightListView) dialog.findViewById(R.id.fareinformation_expandable_listview);
        CustomTextView Tv_listempty = (CustomTextView) dialog.findViewById(R.id.driver_profile_list_empty_textview);
        CustomTextView fareTextview = (CustomTextView) dialog.findViewById(R.id.fare_textview);
        fareTextview.setText(getkey("fare_information_lable"));
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/roboto-condensed.bold.ttf");
        fareTextview.setTypeface(tf);
        if (hasData) {
            Tv_listempty.setVisibility(View.GONE);
            adapter = new FareSummeryAdapter(RideDetailPage.this, freRate_itemList);
            overview_listView.setAdapter(adapter);
        } else {
            Tv_listempty.setVisibility(View.VISIBLE);
        }
        Tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void initialize() {

        session = new SessionManager(RideDetailPage.this);
        cd = new ConnectionDetector(RideDetailPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemlist_reason = new ArrayList<CancelTripPojo>();
        itemlist = new ArrayList<RideDetailPojo>();
        rate_itemList = new ArrayList<RideDetails_DisplayPojo>();
        freRate_itemList = new ArrayList<RideDetails_DisplayPojo>();
        details_itemList = new ArrayList<RideDetails_DisplayPojo>();

        complaint_itemList = new ArrayList<ComplaintsPojo>();
        complaint_itemList1 = new ArrayList<String>();
        complaint_itemList2 = new ArrayList<String>();

        TextView pasttripsinforation= findViewById(R.id.pasttripsinforation);
        pasttripsinforation.setText(getkey("past_trips_summary"));

        TextView haveproblem= findViewById(R.id.haveproblem);
        haveproblem.setText(getkey("have_problem_to_this_trip"));

        multistoplist= findViewById(R.id.multistoplist);
        fareinfolinear = findViewById(R.id.fareinfolinear);
        tripinfolinear = findViewById(R.id.tripinfolinear);
        back = findViewById(R.id.back_imageview);
        carimage = findViewById(R.id.carimage);
        tripdate = findViewById(R.id.tripdate);
        pickupvalue = findViewById(R.id.pickupvalue);
        dropoffvalue = findViewById(R.id.dropoffvalue);
        bookingnovalue = findViewById(R.id.bookingnovalue);
        pickuptimevalue = findViewById(R.id.pickuptimevalue);
        dropofftimevalue = findViewById(R.id.dropofftimevalue);
        distancevalue = findViewById(R.id.distancevalue);
        durationvalue = findViewById(R.id.durationvalue);
        vehiclevalue = findViewById(R.id.vehiclevalue);
        drivernamevalue = findViewById(R.id.drivernamevalue);
        vehiclenumbervalue = findViewById(R.id.vehiclenumbervalue);
        farevalue = findViewById(R.id.farevalue);
        fareinfo = findViewById(R.id.fareinfo);
        fareinfo.setText(getkey("fare_info"));
        sendreciept = findViewById(R.id.sendreciept);
        sendreciept.setText(getkey("send_receipt"));
        sendcomplaint = findViewById(R.id.sendcomplaint);
        sendcomplaint.setText(getkey("send_complaint"));
//        Lv_ride_detail = (ExpandableHeightListView) findViewById(R.id.ride_detail_details_listView);
//        Lv_ride_fare_info = (ExpandableHeightListView) findViewById(R.id.ride_detail_fare_info_listView);

//        bottom_layout = (LinearLayout) findViewById(R.id.bottomlayout);
//        detailtrip_lyut = (LinearLayout) findViewById(R.id.detailtrip_lyut);
//        Ll_cancelTrip = (LinearLayout) findViewById(R.id.rides_detail_cancel_trip_layout);
//        Ll_payment = (LinearLayout) findViewById(R.id.rides_detail_payment_layout);
//        Ll_mailInvoice = (LinearLayout) findViewById(R.id.rides_detail_mail_invoice_layout);
//        Ll_reportIssue = (LinearLayout) findViewById(R.id.rides_detail_report_issue_layout);
//        Ll_trackRide = (LinearLayout) findViewById(R.id.rides_detail_track_ride_layout);
//        Ll_share_Ride = (LinearLayout) findViewById(R.id.rides_detail_share_layout);
//        Rl_button = (RelativeLayout) findViewById(R.id.rides_detail_button_layout);
//        fareInformation_Tv = (CustomTextView) findViewById(R.id.fareinformation_tv);
//        TV_TripStatus= (CustomTextView) findViewById(R.id.trip_status_tv);
        // tripdetail_header_back_layout = (RelativeLayout) findViewById(R.id.tripdetail_header_back_layout);
//        openSupportticket = (CustomTextView) findViewById(R.id.opensupportticket);
        HashMap<String, String> info = session.getUserDetails();

        Intent intent = getIntent();
        if (intent.hasExtra("ride_id")) {
            SrideId = intent.getStringExtra("ride_id");
        }
        if (intent.hasExtra("hidedetails")) {
            Shide = intent.getStringExtra("hidedetails");
            if (Shide.equalsIgnoreCase("yes")) {
//                bottom_layout.setVisibility(View.GONE);
            }
        }
        if (intent.hasExtra("user_id")) {
            referalUserId = intent.getStringExtra("user_id");

        }
        if (intent.hasExtra("class")) {
            page = intent.getStringExtra("class");
        }

        if (!referalUserId.equalsIgnoreCase("")) {
            UserID = referalUserId;
        } else {
            UserID = info.get(SessionManager.KEY_USERID);
        }
//        Lv_ride_detail.setExpanded(true);
//        Lv_ride_fare_info.setExpanded(true);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", SrideId);

        if (isInternetPresent) {
            PostRequest_RideDetail(Iconstant.myride_details_url, jsonParams);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }


        HashMap<String, String> jsonParams1 = new HashMap<String, String>();
        jsonParams1.put("user_id", UserID);//"584aa27ccae2aa741a00002f");

        System.out.println("-------------Complaint Page jsonParams----------------" + jsonParams1);
        if (isInternetPresent) {
            postRequestComplaintPage(Iconstant.complaint_list_url, jsonParams1);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));

        }

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(RideDetailPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-----------------------Rate Card Display Post Request-----------------
    private void PostRequest_RideDetail(String Url, final HashMap<String, String> jsonParams) {
        dialog = new Dialog(RideDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-------------Ride Detail Url----------------" + Url);
        System.out.println("-------------Ride Detail jsonParams----------------" + jsonParams);

        rateCard_mRequest = new ServiceRequest(RideDetailPage.this);
        rateCard_mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Ride Detail Response----------------" + response);

                String Sstatus = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {

                            JSONObject details_object = response_object.getJSONObject("details");
                            if (details_object.length() > 0) {

                                itemlist.clear();
                                RideDetailPojo pojo = new RideDetailPojo();
                                pojo.setCarType(details_object.getString("cab_type"));
                                pojo.setRideId(details_object.getString("ride_id"));
                                rideId = details_object.getString("ride_id");
                                pojo.setRideStatus(details_object.getString("ride_status"));
                                rideStatus = details_object.getString("ride_status");
                                pojo.setDisplayStatus(details_object.getString("disp_status"));
                                pojo.setDoCancelAction(details_object.getString("do_cancel_action"));
                                pojo.setDoTrackAction(details_object.getString("do_track_action"));
                                pojo.setIsFavLocation(details_object.getString("is_fav_location"));
                                pojo.setPay_status(details_object.getString("pay_status"));
                                pojo.setPickup(details_object.getString("pickup_date"));
                                pojo.setDistanceUnit(details_object.getString("distance_unit"));
                                pojo.setDrop(details_object.getString("drop_date"));
                                pojo.setCurrrencySymbol(details_object.getString("currency"));
                                pojo.setPaymentMOde(details_object.getString("payment_mode"));
                                if (object.has("driver_request_mode")) {
                                    driverRequestMode = object.getString("driver_request_mode");
                                }

                                if (rideStatus.equalsIgnoreCase("Completed")) {
//                                    TV_TripStatus.setText("PAST TRIP SUMMARY");
                                } else {
//                                    TV_TripStatus.setText("TRIP SUMMARY");
                                }

                                if (details_object.has("cat_image")) {
                                    Picasso.with(RideDetailPage.this).load(details_object.getString("cat_image")).placeholder(R.drawable.car_new1).into(carimage);
                                }



                                tripdate.setText(getkey("trip_date")+ " " + details_object.getString("trip_date"));
//                                pickupvalue.setText(details_object.getJSONObject("pickup").getString("location"));
//                                dropoffvalue.setText(details_object.getJSONObject("drop").getString("location"));
//                                bookingnovalue.setText(details_object.getString("ride_id"));
//                                pickuptimevalue.setText(details_object.getString("pickup_time"));
//                                dropofftimevalue.setText(details_object.getString("drop_time"));
//                                dropofftimevalue.setText(details_object.getString("drop_time"));
////                                distancevalue.setText(details_object.getJSONObject("summary").getString("ride_distance"));
////                                durationvalue.setText(details_object.getJSONObject("summary").getString("ride_duration"));
//                                vehiclevalue.setText(details_object.getString("cab_type"));
//                                drivernamevalue.setText(details_object.getString("driver_name"));
//                                vehiclenumbervalue.setText(details_object.getString("vehicle_number"));
//                                ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(details_object.getString("currency"));
//                                farevalue.setText(ScurrencySymbol + details_object.getString("trip_fare"));

                                String farebreakam="";
                                if(details_object.getString("ride_status").equals("Booked"))
                                {
                                    farebreakam=details_object.getString("est_cost");
                                }

                                ArrayList<multistop_pojo> muli = new ArrayList<multistop_pojo>();
                                muli.clear();
                                Object check_multi_stops_object = details_object.get("multi_stops");
                                if (check_multi_stops_object instanceof JSONArray) {
                                    JSONArray multi_stops = details_object.getJSONArray("multi_stops");
                                    if (multi_stops.length() > 0) {

                                        for (int j = 0; j < multi_stops.length(); j++) {
                                            JSONObject location = multi_stops.getJSONObject(j);
                                            multistop_pojo jk = new multistop_pojo();
                                            jk.setTitle(location.getString("location"));
                                            muli.add(jk);
                                        }


                                    }
                                }
                                Object check_pickup_object = details_object.get("pickup");
                                if (check_pickup_object instanceof JSONObject) {

                                    JSONObject pickup_object = details_object.getJSONObject("pickup");
                                    if (pickup_object.length() > 0) {
                                        pojo.setPickupaddress(pickup_object.getString("location"));
                                        JSONObject latlong_object = pickup_object.getJSONObject("latlong");
                                        if (latlong_object.length() > 0) {
                                            pojo.setPickupLat(latlong_object.getString("lat"));
                                            pojo.setPickupLong(latlong_object.getString("lon"));
                                        }

                                        isPickUpAvailable = true;
                                    } else {
                                        isPickUpAvailable = false;
                                    }
                                }

                                Object check_drop_object = details_object.get("drop");
                                if (check_drop_object instanceof JSONObject) {

                                    JSONObject drop_object = details_object.getJSONObject("drop");
                                    if (drop_object.length() > 0) {
                                        pojo.setPickupaddress(drop_object.getString("location"));
                                        JSONObject latlong_object = drop_object.getJSONObject("latlong");
                                        if (latlong_object.length() > 0) {
                                            pojo.setPickupLat(latlong_object.getString("lat"));
                                            pojo.setPickupLong(latlong_object.getString("lon"));
                                        }

                                        isDropAvailable = true;
                                    } else {
                                        isDropAvailable = false;
                                    }
                                }


                                Object check_summary_object = details_object.get("summary");
                                if (check_summary_object instanceof JSONObject) {

                                    JSONObject summary_object = details_object.getJSONObject("summary");
                                    if (summary_object.length() > 0) {
                                        pojo.setRideDistance(summary_object.getString("ride_distance"));
                                        pojo.setTimeTaken(summary_object.getString("ride_duration"));
                                        pojo.setWaitTime(summary_object.getString("waiting_duration"));

                                        isSummaryAvailable = true;
                                    } else {
                                        isSummaryAvailable = false;
                                    }
                                }


                                Object check_fare_object = details_object.get("fare");
                                if (check_fare_object instanceof JSONObject) {

                                    JSONObject fare_object = details_object.getJSONObject("fare");
                                    if (fare_object.length() > 0) {
                                        pojo.setTotalBill(fare_object.getString("grand_bill"));
                                        pojo.setTotalPaid(fare_object.getString("total_paid"));
                                        pojo.setCouponDiscount(fare_object.getString("coupon_discount"));
                                        pojo.setWalletUsuage(fare_object.getString("wallet_usage"));
                                        pojo.setTip_amount(fare_object.getString("tips_amount"));

                                        isFareAvailable = true;
                                    } else {
                                        isFareAvailable = false;
                                    }
                                }


                                ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(details_object.getString("currency"));
                                Object check_triparr_object = details_object.get("tripArr");
                                if (check_triparr_object instanceof JSONArray) {
                                    JSONArray triparr_array = details_object.getJSONArray("tripArr");
                                    if (triparr_array.length() > 0) {
                                        triplocationfarearry(triparr_array,muli);
                                        details_itemList.clear();
                                        for (int i = 0; i < triparr_array.length(); i++) {
                                            JSONObject standard_object = triparr_array.getJSONObject(i);
                                            RideDetails_DisplayPojo stdrate_pojo = new RideDetails_DisplayPojo();
                                            stdrate_pojo.setRate_title(standard_object.getString("title"));

                                            if ("Fare".equalsIgnoreCase(standard_object.getString("title"))) {
                                                stdrate_pojo.setRate_value(ScurrencySymbol + " " + standard_object.getString("value"));
                                            } else {
                                                stdrate_pojo.setRate_value(standard_object.getString("value"));
                                            }
                                            stdrate_pojo.setRate_currencySymbol(ScurrencySymbol);
                                            details_itemList.add(stdrate_pojo);
                                        }
                                    }
                                }
                                Object check_fareall_object = details_object.get("fareArrL");
                                if (check_fareall_object instanceof JSONArray) {

                                    JSONArray fare_array = details_object.getJSONArray("fareArrL");
                                    if (fare_array.length() > 0) {
                                        freRate_itemList.clear();
                                        for (int j = 0; j < fare_array.length(); j++) {
                                            JSONObject extra_object = fare_array.getJSONObject(j);
                                            RideDetails_DisplayPojo rate_pojo = new RideDetails_DisplayPojo();
                                            rate_pojo.setFarerate_title((extra_object.getString("title")));
                                            rate_pojo.setFarerate_value(ScurrencySymbol + " " + extra_object.getString("value"));
                                            rate_pojo.setFarerate_currencySymbol(ScurrencySymbol);
                                            freRate_itemList.add(rate_pojo);
                                        }
                                        hasData = true;
                                    } else {
                                        hasData = false;
                                    }
                                }



                                Object check_ratecard_object = details_object.get("booking_summary");
                                if (check_ratecard_object instanceof JSONArray) {

                                    JSONArray fare_array = details_object.getJSONArray("booking_summary");
                                    if (fare_array.length() > 0) {
                                        updatefarearry(fare_array,muli,farebreakam);
                                        rate_itemList.clear();
                                        for (int j = 0; j < fare_array.length(); j++) {
                                            JSONObject extra_object = fare_array.getJSONObject(j);
                                            RideDetails_DisplayPojo rate_pojo = new RideDetails_DisplayPojo();
                                            rate_pojo.setRate_title(extra_object.getString("title"));
                                            rate_pojo.setRate_value(/*ScurrencySymbol +*/ extra_object.getString("value"));
                                            rate_pojo.setRate_currencySymbol(ScurrencySymbol);
                                            rate_itemList.add(rate_pojo);
                                        }
                                    }
                                }

                                if(muli.size() > 0)
                                {
                                    multistoplist.setVisibility(View.VISIBLE);
                                    Multistoplistadapter paymentListAdapter = new Multistoplistadapter(RideDetailPage.this, muli);
                                    multistoplist.setAdapter(paymentListAdapter);
                                    paymentListAdapter.notifyDataSetChanged();
                                    multistoplist.setExpanded(true);
                                }
                                else
                                {
                                    multistoplist.setVisibility(View.GONE);
                                }

                                CompleteStage = details_object.getString("comp_stage");
                                completeStatus = details_object.getString("comp_status");

                                itemlist.add(pojo);
                                isDataAvailable = true;




                            } else {
                                isDataAvailable = false;
                            }

                        } else {
                            isDataAvailable = false;
                        }
                    } else {
                        isDataAvailable = false;
                    }


                    if (Sstatus.equalsIgnoreCase("1") && isDataAvailable) {
                        dialogDismiss();

//                        if (details_itemList.size() > 0) {
//                            details_adapter = new RideDetailsAdapter(RideDetailPage.this, details_itemList);
//                            Lv_ride_detail.setAdapter(details_adapter);
//                        }
//
//                        if (rate_itemList.size() > 0) {
//                            fare_adapter = new RideDetailsAdapter_new(RideDetailPage.this, rate_itemList);
//                            Lv_ride_fare_info.setAdapter(fare_adapter);
//                        }


                        //------------------------------------------------------------------------------

                        //------------Button Change Function-------
                        if (itemlist.get(0).getPay_status().equalsIgnoreCase("Pending") || itemlist.get(0).getPay_status().equalsIgnoreCase("Processing")) {
//                            Ll_cancelTrip.setVisibility(View.GONE);
//                            detailtrip_lyut.setVisibility(View.VISIBLE);
//                            Ll_payment.setVisibility(View.VISIBLE);
//                            Ll_mailInvoice.setVisibility(View.GONE);
//                            Ll_reportIssue.setVisibility(View.GONE);

                        }

                        if (itemlist.get(0).getDoCancelAction().equalsIgnoreCase("1")) {
//                            Ll_cancelTrip.setVisibility(View.VISIBLE);
//                            detailtrip_lyut.setVisibility(View.VISIBLE);
//                            Ll_payment.setVisibility(View.GONE);
//                            Ll_mailInvoice.setVisibility(View.GONE);
//                            Ll_reportIssue.setVisibility(View.GONE);
                        }

                        if (itemlist.get(0).getRideStatus().equalsIgnoreCase("Completed")) {
//                            Ll_cancelTrip.setVisibility(View.GONE);
//                            detailtrip_lyut.setVisibility(View.VISIBLE);
//                            Ll_payment.setVisibility(View.GONE);
//                            Ll_mailInvoice.setVisibility(View.GONE);
//                            Ll_reportIssue.setVisibility(View.VISIBLE);

                        }

                        if (itemlist.get(0).getRideStatus().equalsIgnoreCase("Finished") && itemlist.get(0).getPay_status().equalsIgnoreCase("Pending")) {

//                            Ll_cancelTrip.setVisibility(View.GONE);
//                            detailtrip_lyut.setVisibility(View.VISIBLE);
//                            Ll_payment.setVisibility(View.VISIBLE);
//                            Ll_mailInvoice.setVisibility(View.GONE);
//                            Ll_reportIssue.setVisibility(View.GONE);
//                            Ll_payment.setVisibility(View.GONE);
                        }


                        //------Show and Hide Track Ride Button Layout------
                        if (itemlist.get(0).getDoTrackAction().equalsIgnoreCase("1")) {
//                            Ll_trackRide.setVisibility(View.VISIBLE);
//                            Ll_share_Ride.setVisibility(View.VISIBLE);
//                            detailtrip_lyut.setVisibility(View.VISIBLE);
                        } else {
//                            Ll_share_Ride.setVisibility(View.GONE);
//                            Ll_trackRide.setVisibility(View.GONE);
                        }

                        //------Show and Hide the Button Layout------
                        if (itemlist.get(0).getPay_status().equalsIgnoreCase("Pending") || itemlist.get(0).getPay_status().equalsIgnoreCase("Processing") || itemlist.get(0).getDoCancelAction().equalsIgnoreCase("1") || itemlist.get(0).getRideStatus().equalsIgnoreCase("Completed") || itemlist.get(0).getRideStatus().equalsIgnoreCase("Onride")) {
//                            Rl_button.setVisibility(View.VISIBLE);
                        } else {
//                            Rl_button.setVisibility(View.GONE);
                        }


                        if (itemlist.get(0).getRideStatus().equalsIgnoreCase("Expired")) {
//                            Ll_reportIssue.setVisibility(View.GONE);
//                            openSupportticket.setVisibility(View.GONE);
                        }
                        //---------------------------------------
                    } else {
                        String Sresponse = object.getString("response");
                        dialogDismiss();
                        Alert(getkey("alert_label_title"), Sresponse);
                    }
                    dialogDismiss();

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void updatefarearry(JSONArray rate_itemList,ArrayList<multistop_pojo> muli,String farebreakam) {
        try {
            fareinfolinear.removeAllViews();
            for (int i = 0; i <= rate_itemList.length() - 1; i++) {
                View view = LayoutInflater.from(RideDetailPage.this).inflate(R.layout.farinfolayoutlist, null);
                TextView pickup = view.findViewById(R.id.pickup);
                TextView pickupvalue = view.findViewById(R.id.pickupvalue);

                pickup.setText(rate_itemList.getJSONObject(i).getString("title"));



                if (rate_itemList.getJSONObject(i).getString("currency_status").equals("1")) {
                    if(farebreakam.equals(""))
                    {

                        pickupvalue.setText(ScurrencySymbol +" "+ rate_itemList.getJSONObject(i).getString("value"));
                    }
                    else
                    {
                        pickupvalue.setText(ScurrencySymbol +" "+ farebreakam);
                    }
                      } else {
                    pickupvalue.setText(rate_itemList.getJSONObject(i).getString("value"));
                }
                fareinfolinear.addView(view);
            }
        } catch (JSONException e) {
        }
    }

    private void triplocationfarearry(JSONArray rate_itemList,ArrayList<multistop_pojo> muli) {
        try {
            tripinfolinear.removeAllViews();
            for (int i = 0; i <= rate_itemList.length() - 1; i++) {
                View view = LayoutInflater.from(RideDetailPage.this).inflate(R.layout.farinfolayoutlist, null);
                TextView pickup = view.findViewById(R.id.pickup);
                TextView pickupvalue = view.findViewById(R.id.pickupvalue);
                ExpandableHeightListView multistoplist = view.findViewById(R.id.multistoplist);
                pickup.setText(rate_itemList.getJSONObject(i).getString("title"));

                if (rate_itemList.getJSONObject(i).getString("currency_status").equals("1")) {
                    pickupvalue.setText(ScurrencySymbol +" "+ rate_itemList.getJSONObject(i).getString("value"));
                } else {
                    pickupvalue.setText(rate_itemList.getJSONObject(i).getString("value"));
                }

                tripinfolinear.addView(view);
            }
        } catch (JSONException e) {
        }
    }


    //-----------------------MyRide Cancel Reason Post Request-----------------
    private void postRequest_CancelRides_Reason(String Url) {
        dialog = new Dialog(RideDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));


        System.out.println("-------------MyRide Cancel Reason Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", SrideId);

        mRequest = new ServiceRequest(RideDetailPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------MyRide Cancel Reason Response----------------" + response);

                String Sstatus = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {

                            Object check_reason_object = response_object.get("reason");
                            if (check_reason_object instanceof JSONArray) {

                                JSONArray reason_array = response_object.getJSONArray("reason");
                                if (reason_array.length() > 0) {
                                    itemlist_reason.clear();
                                    for (int i = 0; i < reason_array.length(); i++) {
                                        JSONObject reason_object = reason_array.getJSONObject(i);
                                        CancelTripPojo pojo = new CancelTripPojo();
                                        pojo.setReason(reason_object.getString("reason"));
                                        pojo.setReasonId(reason_object.getString("id"));

                                        itemlist_reason.add(pojo);
                                    }

                                    isReasonAvailable = true;
                                } else {
                                    isReasonAvailable = false;
                                }
                            }
                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }


                    if (Sstatus.equalsIgnoreCase("1") && isReasonAvailable) {
                        Intent passIntent = new Intent(RideDetailPage.this, MyRideCancelTrip.class);
                        Bundle bundleObject = new Bundle();
                        bundleObject.putSerializable("Reason", itemlist_reason);
                        passIntent.putExtras(bundleObject);
                        passIntent.putExtra("RideID", SrideId);
                        startActivity(passIntent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //----------Method for Invoice Email--------
    private void mailInvoice() {
        final Dialog dialog = new Dialog(RideDetailPage.this, R.style.DialogSlideAnim2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.mail_invoice_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView cancelBtn;
        Button sendBtn;
        Et_dialog_InvoiceEmail = (EditText) dialog.findViewById(R.id.mail_invoice_email_edittext);
        Et_dialog_InvoiceEmail.addTextChangedListener(mailInvoice_EditorWatcher);
        cancelBtn = (ImageView) dialog.findViewById(R.id.cancel_btn);

        TextView mail_invoice_title = (TextView) dialog.findViewById(R.id.mail_invoice_title);
        mail_invoice_title.setText(getkey("insert_valid_email_address_for_receipt"));

        TextView text_mail = (TextView) dialog.findViewById(R.id.text_mail);
        text_mail.setText(getkey("fare_lable1"));

        TextView balcc = (TextView) dialog.findViewById(R.id.balcc);
        balcc.setText(getkey("blackmaria_inc"));


        sendBtn = (Button) dialog.findViewById(R.id.send_btn);
        sendBtn.setText(getkey("confirm_lable"));
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(RideDetailPage.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (!isValidEmail(Et_dialog_InvoiceEmail.getText().toString())) {
                    erroredit(Et_dialog_InvoiceEmail, getkey("register_label_alert_email"));
                } else {
                    if (isInternetPresent) {
                        dialog.dismiss();
                        postRequest_EmailInvoice(Iconstant.myride_details_inVoiceEmail_url, Et_dialog_InvoiceEmail.getText().toString(), SrideId);

                    } else {
                        Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                    }
                }


            }
        });


        Et_dialog_InvoiceEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(Et_dialog_InvoiceEmail.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return false;
            }
        });

        dialog.show();

    }


    //code to Check Email Validation
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(RideDetailPage.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    //----------------------Code for TextWatcher-------------------------
    private final TextWatcher mailInvoice_EditorWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            //clear error symbol after entering text
            if (Et_dialog_InvoiceEmail.getText().length() > 0) {
                Et_dialog_InvoiceEmail.setError(null);
            }
        }
    };


    //-----------------------MyRide Email Invoice Post Request-----------------
    private void postRequest_EmailInvoice(String Url, final String Semail, final String SrideId) {
        dialog = new Dialog(RideDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_sending_invoice"));

        System.out.println("-------------MyRide Email Invoice Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", Semail);
        jsonParams.put("ride_id", SrideId);
        System.out.println("-------------MyRide Email Invoice jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(RideDetailPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------MyRide Email Invoice response----------------" + response);

                String Sstatus = "", Sresponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        //  invoice_dialog.dismiss();
                        Alert(getkey("action_success"), Sresponse);
                    } else {
                        Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
                Alert(getkey("alert_label_title"),getkey("sentfailed"));
            }
        });
    }


    //----------Method to Send Email--------
    protected void sendEmail() {

        SappInfo_Session = new AppInfoSessionManager(RideDetailPage.this);
        HashMap<String, String> appInfo = SappInfo_Session.getAppInfo();
        String toAddress = appInfo.get(AppInfoSessionManager.KEY_CONTACT_EMAIL);

        String[] TO = {toAddress};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Message");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
//            Toast.makeText(RideDetailPage.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }


    //---------------------------Share Location----------------------------------------------

    private void shareTrip() {
        dialog1 = new Dialog(RideDetailPage.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.share_trip_popup);

        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Et_share_trip_mobileno = (EditText) dialog1.findViewById(R.id.sharetrip_mobilenoEt);
        Button Bt_Submit = (Button) dialog1.findViewById(R.id.jsharetrip_popup_submit);
        ImageView Bt_Cancel = (ImageView) dialog1.findViewById(R.id.sharetrip_popup_cancel);


        Bt_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Et_share_trip_mobileno.setText(Html.fromHtml(Et_share_trip_mobileno.getText().toString()).toString());

                if (!isValidPhoneNumber(Et_share_trip_mobileno.getText().toString())) {
                    erroredit(Et_share_trip_mobileno, getkey("share_label_alert_phoneno"));
                } else {
                    if (isInternetPresent) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("ride_id", SrideId);
                        jsonParams.put("mobile_no", Et_share_trip_mobileno.getText().toString());

                        share_trip_postRequest_MyRides(Iconstant.share_trip_url, jsonParams);
                        if (dialog1 != null) {
                            dialog1.dismiss();
                        }
                    } else {
                        Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                    }
                }
            }
        });

        Bt_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
    }


    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    private void postRequestComplaintPage(String Url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(RideDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        mRequest = new ServiceRequest(RideDetailPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------ComplaintPage Response----------------" + response);

                String Sstatus = "", Scurrency_code = "", user_image = "", Scurrentbalance = "", Smessage = "", Scurrency = "", user_name = "", user_location = "", user_id = "", member_since = "", Date = "", StotalTicketCount = "", SopenTicketCount = "", SclosedTicketCount = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject jobject = object.getJSONObject("response");
                    StotalTicketCount = jobject.getString("total_ticket_Count");
                    SopenTicketCount = jobject.getString("open_ticket_Count");
                    SclosedTicketCount = jobject.getString("closed_ticket_Count");


                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (jobject.length() > 0) {
                            Date = jobject.getString("today_datetime");
                            todayDate = Date;
                            Object check_object1 = jobject.get("user_profile");
                            if (check_object1 instanceof JSONObject) {
                                JSONObject user_detail_object = jobject.getJSONObject("user_profile");
                                if (user_detail_object.length() > 0) {
                                    user_name = user_detail_object.getString("user_name");
                                    user_id = user_detail_object.getString("user_id");

                                    member_since = user_detail_object.getString("member_since");
                                    user_location = user_detail_object.getString("location_name");
                                    user_image = user_detail_object.getString("user_image");

                                    userImage = user_image;
                                    userName = user_name;
                                    userLocation = user_location;
                                    session.setcomplaintUserDetail(userName, userImage, userLocation);

                                }
                            }

                            Object check_object = jobject.get("subject");
                            if (check_object instanceof JSONArray) {
                                JSONArray complaint_list_jsonArray = jobject.getJSONArray("subject");
                                if (complaint_list_jsonArray.length() > 0) {
                                    isComplaintsAvailable = true;
                                    complaint_itemList.clear();
                                    complaint_itemList1.clear();
                                    complaint_itemList1.add("SELECT SUBJECT");
                                    complaint_itemList2.add("0");

                                    for (int i = 0; i < complaint_list_jsonArray.length(); i++) {
                                        JSONObject earning_list_obj = complaint_list_jsonArray.getJSONObject(i);
                                        ComplaintsPojo complaintsPojo = new ComplaintsPojo();
                                        complaintsPojo.setoptionsId(earning_list_obj.getString("option_id"));
                                        complaintsPojo.setsubjectId(earning_list_obj.getString("subject_id"));
                                        complaintsPojo.setsubjectName(earning_list_obj.getString("subject_name"));
                                        complaintsPojo.setReasonStatus("0");
                                        complaint_itemList.add(complaintsPojo);
                                       /*complaint_itemList1.add(earning_list_obj.getString("option_id"));
                                        complaint_itemList1.add(earning_list_obj.getString("subject_id"));*/
                                        complaint_itemList1.add(earning_list_obj.getString("subject_name"));
                                        complaint_itemList2.add(earning_list_obj.getString("option_id"));
                                    }
                                    isComplaintsAvailable = true;
                                } else {
                                    isComplaintsAvailable = false;
                                }
                            } else {
                                isComplaintsAvailable = false;
                            }
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                        }
                    } else {
                        Smessage = jobject.getString("response");
                    }
                    if (dialog != null) {
                        dialog.dismiss();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (Sstatus.equalsIgnoreCase("1")) {


                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    //----------------------------------Share Trip post reques------------------------
    private void share_trip_postRequest_MyRides(String url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(RideDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        System.out.println("------------- Share Trip url----------------" + url);
        System.out.println("------------- Share Trip jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(RideDetailPage.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("share trip", response);

                String Str_status = "", Str_response = "";
                System.out.println("------------------sharetrip response-------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    Str_response = object.getString("response");

                    if (Str_status.equalsIgnoreCase("1")) {

                        Alert(getkey("action_success"), Str_response);

                    } else {
                        Alert(getkey("action_error"), Str_response);

                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {

                dialog.dismiss();

            }


        });

    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(RideDetailPage.this, RideListHomePage.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
