package com.blackmaria;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextViewForm;
import com.blackmaria.widgets.PkDialog;
import com.countrycodepicker.CountryPicker;
import com.devspark.appmsg.AppMsg;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

import static com.blackmaria.NewProfile_Page_LoginEdit.myloginDialogFormPincodeEdittext;
import static com.blackmaria.NewProfile_Page_LoginEdit.newpincode;

/**
 * Created by Muruganantham on 12/28/2017.
 */

@SuppressLint("Registered")
public class NewProfile_Page_Login_Pincode_Edit extends FragmentActivityHockeyApp implements View.OnClickListener {
    private ServiceRequest mRequest;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private RefreshReceiver refreshReceiver;
    private SessionManager session;
    private GPSTracker gpsTracker;
    private RelativeLayout loginEditLyHeader;
    private ImageView cancelLayout;
    private LinearLayout myloginDialogFormWholeLayout, myloginDialogOtpLayout;
    //    private CustomTextViewForm myloginDialogFormCountryCodeTextview;
    private CustomEdittext myloginDialogFormOldPincodeEdittext;
    private CustomEdittext myloginDialogFormCofirmPincodeEdittext;
    private CustomEdittext myloginDialogFormNewPincodeEdittext;

    private CustomTextViewForm myloginDialogFormPasswordTextview;
    private CustomEdittext myloginDialogFormPasswordEdittext;
    private RelativeLayout profileLoginInfoBtn;
    private LinearLayout loginEditOtp;
    private CustomEdittext myloginDialogOtp;
    private RelativeLayout loginDetailsOtpConfirm;
    private SmoothProgressBar profileLoadingProgressbar;

    private CountryPicker picker;
    AppInfoSessionManager appInfoSessionManager;
    private String UserID = "";
    private String gcmID = "";
    private String sAuthKey = "";
    String  mobilenumber = "", email = "", pincode = "", contrycode = "", phonenumber = "";

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewProfile_Page_Login_Pincode_Edit.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewProfile_Page_Login_Pincode_Edit.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            } else if (intent.getAction().equals("com.package.ACTION_CLASS_complaint_replay_notification")) {
                if (intent.getExtras() != null) {
                    String ticketId = (String) intent.getExtras().get("ticket_id");
                    String userID = (String) intent.getExtras().get("user_id");
                    String message = (String) intent.getExtras().get("message");
                    String dateTime = (String) intent.getExtras().get("date_time");

                    Intent intent1 = new Intent(NewProfile_Page_Login_Pincode_Edit.this, ComplaintPageViewDetailPage.class);
                    intent1.putExtra("ticket_id", ticketId);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_login_details_pincode_edit_layout);
        initialize();
        OtherClickEvents();
    }

    private void initialize() {

        appInfoSessionManager = new AppInfoSessionManager(NewProfile_Page_Login_Pincode_Edit.this);
        session = new SessionManager(NewProfile_Page_Login_Pincode_Edit.this);
        cd = new ConnectionDetector(NewProfile_Page_Login_Pincode_Edit.this);
        isInternetPresent = cd.isConnectingToInternet();

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction("com.pushnotification.updateBottom_view");
//        registerReceiver(refreshReceiver, intentFilter);


        HashMap<String, String> info = session.getUserDetails();
        String tipstime = info.get(SessionManager.KEY_PHONENO);
        UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> app = appInfoSessionManager.getAppInfo();
        sAuthKey = app.get(appInfoSessionManager.KEY_APP_IDENTITY);

        HashMap<String, String> user = session.getUserDetails();
        gcmID = user.get(SessionManager.KEY_GCM_ID);


        loginEditLyHeader = (RelativeLayout) findViewById(R.id.login_edit_ly_header);
        cancelLayout = (ImageView) findViewById(R.id.cancel_layout);
        myloginDialogFormWholeLayout = (LinearLayout) findViewById(R.id.mylogin_dialog_form_whole_layout);
        myloginDialogOtpLayout = (LinearLayout) findViewById(R.id.mobile_otp_layout);
        myloginDialogFormOldPincodeEdittext = (CustomEdittext) findViewById(R.id.mylogin_dialog_form_oldpincode_edittext);
        myloginDialogFormNewPincodeEdittext = (CustomEdittext) findViewById(R.id.mylogin_dialog_form_newpincode_edittext);
        myloginDialogFormCofirmPincodeEdittext = (CustomEdittext) findViewById(R.id.mylogin_dialog_form_confirmpincode_edittext);

        myloginDialogFormPasswordTextview = (CustomTextViewForm) findViewById(R.id.mylogin_dialog_form_password_textview);
        myloginDialogFormPasswordEdittext = (CustomEdittext) findViewById(R.id.mylogin_dialog_form_password_edittext);
        profileLoginInfoBtn = (RelativeLayout) findViewById(R.id.profile_login_info_btn);
        loginEditOtp = (LinearLayout) findViewById(R.id.login_edit_otp);
        myloginDialogOtp = (CustomEdittext) findViewById(R.id.mylogin_dialog_otp);
        loginDetailsOtpConfirm = (RelativeLayout) findViewById(R.id.login_details_otp_confirm);
        profileLoadingProgressbar = (SmoothProgressBar) findViewById(R.id.profile_loading_progressbar);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_lable));

        cancelLayout.setOnClickListener(this);
        loginDetailsOtpConfirm.setOnClickListener(this);
        profileLoginInfoBtn.setOnClickListener(this);


        Intent in = getIntent();
//        oldpincode = in.getStringExtra("oldpincode");
        contrycode = in.getStringExtra("countrycode");
        email = in.getStringExtra("email");
        phonenumber = in.getStringExtra("phonemnumber");

//        myloginDialogFormCountryCodeTextview.setText(countrycode);
//        myloginDialogFormMobileNoEdittext.setText(mobilenumber);
//        myloginDialogFormEmailidEdittext.setText(email);
//        myloginDialogFormPincodeEdittext.setText(pincode);


    }

    @Override
    public void onClick(View v) {
        if (v == cancelLayout) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == loginDetailsOtpConfirm) {
//            LoginUpdate();
        } else if (v == profileLoginInfoBtn) {
            CloseKeyboardNew();
            LoginUpdate();
//            OtpUpdate();

        }
    }

    private void OtherClickEvents() {
//        myloginDialogFormCountryCodeTextview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
//            }
//        });
//
//        picker.setListener(new CountryPickerListener() {
//            @Override
//            public void onSelectCountry(String name, String code, String dialCode) {
//                picker.dismiss();
//                myloginDialogFormCountryCodeTextview.setText(dialCode);
//                // close keyboard
//                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                mgr.hideSoftInputFromWindow(myloginDialogFormCountryCodeTextview.getWindowToken(), 0);
//            }
//        });
    }

//    private void OtpUpdate() {
//        try {
////            myloginDialogFormEmailidEdittext.setText(Html.fromHtml(myloginDialogFormEmailidEdittext.getText().toString()).toString());
////            myloginDialogFormMobileNoEdittext.setText(Html.fromHtml(myloginDialogFormMobileNoEdittext.getText().toString()).toString());
////            myloginDialogFormPincodeEdittext.setText(Html.fromHtml(myloginDialogFormPincodeEdittext.getText().toString()).toString());
//
//            if (myloginDialogFormOldPincodeEdittext.getText().toString().length() == 0) {
//                AlertError(getResources().getString(R.string.action_error), "Old Pincode can't be empty");
//            } else if (!myloginDialogFormOldPincodeEdittext.getText().toString().equalsIgnoreCase(oldpincode)) {
//                AlertError(getResources().getString(R.string.action_error), "Enter Pincode is not Old Pincode");
//            } else if (myloginDialogFormNewPincodeEdittext.getText().toString().length() == 0) {
//                AlertError(getResources().getString(R.string.action_error), "New Pincode can't be empty");
//            } else if (myloginDialogFormCofirmPincodeEdittext.getText().toString().length() == 0) {
//                AlertError(getResources().getString(R.string.action_error), "Confirm Pincode can't be empty");
//            } else if (myloginDialogFormCofirmPincodeEdittext.getText().toString().equalsIgnoreCase(myloginDialogFormNewPincodeEdittext.getText().toString())) {
//                AlertError(getResources().getString(R.string.action_error), "New and Old Picode must be same");
//            } else {
//                cd = new ConnectionDetector(NewProfile_Page_Login_Pincode_Edit.this);
//                isInternetPresent = cd.isConnectingToInternet();
//                myloginDialogOtpLayout.setVisibility(View.VISIBLE);
//                    if (isInternetPresent) {
//                        HashMap<String, String> jsonParams = new HashMap<String, String>();
//                        jsonParams.put("user_id", UserID);
//                        jsonParams.put("country_code", myloginDialogFormCountryCodeTextview.getText().toString());
//                        jsonParams.put("phone_number", myloginDialogFormMobileNoEdittext.getText().toString());
//                        PostRequest_updateOtp_info(Iconstant.User_Otp_LoginUpdate, jsonParams);
//                    } else {
//                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
//                    }
//                }
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void LoginUpdate() {
        try {
            cd = new ConnectionDetector(NewProfile_Page_Login_Pincode_Edit.this);
            isInternetPresent = cd.isConnectingToInternet();
            if (myloginDialogFormOldPincodeEdittext.getText().toString().length() == 0) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.enteroldpin));
            } else if (!myloginDialogFormOldPincodeEdittext.getText().toString().equalsIgnoreCase(session.getSecurePin())) {
                AlertError(getResources().getString(R.string.action_error),  getResources().getString(R.string.oldpinincoorect));
            } else if (myloginDialogFormOldPincodeEdittext.getText().toString().trim().length() < 6) {
                AlertError(getResources().getString(R.string.action_error),  getResources().getString(R.string.newpin_must));
            } else if (myloginDialogFormNewPincodeEdittext.getText().toString().length() == 0) {
                AlertError(getResources().getString(R.string.action_error),  getResources().getString(R.string.kindlyenternewpin));
            }else if (myloginDialogFormNewPincodeEdittext.getText().toString().length() < 6) {
                AlertError(getResources().getString(R.string.action_error),  getResources().getString(R.string.newpinmustdigits));
            } else if (myloginDialogFormCofirmPincodeEdittext.getText().toString().length() == 0) {
                AlertError(getResources().getString(R.string.action_error),  getResources().getString(R.string.kindlyenterconfirmpin));
            } else if (myloginDialogFormCofirmPincodeEdittext.getText().toString().length() < 6) {
                AlertError(getResources().getString(R.string.action_error),  getResources().getString(R.string.newpinmustdigits));
            }else if (!myloginDialogFormCofirmPincodeEdittext.getText().toString().equalsIgnoreCase(myloginDialogFormNewPincodeEdittext.getText().toString())) {
                AlertError(getResources().getString(R.string.action_error),  getResources().getString(R.string.confirmcodemismatching));
            } else {
                session.SetPincode(myloginDialogFormCofirmPincodeEdittext.getText().toString().trim());
//                session.setSecurePin(myloginDialogFormCofirmPincodeEdittext.getText().toString().trim());
                newpincode = myloginDialogFormCofirmPincodeEdittext.getText().toString().trim();
                myloginDialogFormPincodeEdittext.setText(myloginDialogFormCofirmPincodeEdittext.getText().toString().trim());
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
//                if (isInternetPresent) {
//                    HashMap<String, String> jsonParams = new HashMap<String, String>();
//                    jsonParams.put("mode", "update");
//                    jsonParams.put("user_id", UserID);
//                    jsonParams.put("email", email);
//                    jsonParams.put("country_code", contrycode);
//                    jsonParams.put("phone_number", phonenumber);
//                    jsonParams.put("passcode", myloginDialogFormCofirmPincodeEdittext.getText().toString().trim());
//                    PostRequest_updatelogin_info(Iconstant.userprofile_update_login_info_url, jsonParams);
//                } else {
//                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//
//    private void PostRequest_updateOtp_info(String Url, final HashMap<String, String> jsonParams) {
//        profileLoadingProgressbar.setVisibility(View.VISIBLE);
//        System.out.println("--------------PostRequest_updateOtp_info-------------------" + Url);
//        System.out.println("--------------PostRequest_updateOtp_info jsonParams-------------------" + jsonParams);
//        mRequest = new ServiceRequest(NewProfile_Page_Login_Pincode_Edit.this);
//        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
//            @Override
//            public void onCompleteListener(String response) {
//                System.out.println("--------------PostRequest_updateOtp_info reponse-------------------" + response);
//                String Sstatus = "", Smessage = "", otp_status = "", Up_home_passcode = "", otp = "", Up_login_phone_no = "", Up_login_country_code = "", Up_home_pho_no = "";
//                try {
//                    JSONObject object = new JSONObject(response);
//                    Sstatus = object.getString("status");
//                    if (Sstatus.equalsIgnoreCase("1")) {
//                        Smessage = object.getString("response");
//                        Up_login_phone_no = object.getString("phone_number");
//                        Up_login_country_code = object.getString("country_code");
//                        otp_status = object.getString("otp_status");
//                        otp = object.getString("otp");
//                        session.setOtpstatusAndPin(otp_status, otp);
//                    } else {
//                        Smessage = object.getString("response");
//                    }
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    System.out.println("eee------------------" + e);
//                    e.printStackTrace();
//                    profileLoadingProgressbar.setVisibility(View.GONE);
//                }
//                if (Sstatus.equalsIgnoreCase("1")) {
//
//                    if (otp_status.equalsIgnoreCase("development")) {
//                        myloginDialogOtp.setText(otp);
//                    } else {
//                        myloginDialogOtp.setText("");
//                    }
//
//                } else {
//                    Alert(getResources().getString(R.string.action_error), Smessage);
//                    profileLoadingProgressbar.setVisibility(View.GONE);
//                }
//                profileLoadingProgressbar.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onErrorListener() {
//                profileLoadingProgressbar.setVisibility(View.GONE);
//            }
//        });
//    }

    //    --------------------login info update---------------------------
    private void PostRequest_updatelogin_info(String Url, final HashMap<String, String> jsonParams) {

        profileLoadingProgressbar.setVisibility(View.VISIBLE);
        System.out.println("--------------updateProfile-------------------" + Url);
        System.out.println("--------------updateProfile jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewProfile_Page_Login_Pincode_Edit.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------updateProfile reponse-------------------" + response);
                String Sstatus = "", Smessage = "", Up_login_user_name = "", Up_home_passcode = "", Up_home_email_id = "", Up_login_phone_no = "", Up_login_country_code = "", Up_home_pho_no = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");
                        JSONObject jsonObject_userprofile = object.getJSONObject("user_profile");
                        if (jsonObject_userprofile.length() > 0) {
//                            Up_login_phone_no = jsonObject_userprofile.getString("phone_number");
//                            Up_login_country_code = jsonObject_userprofile.getString("country_code");
//                            Up_home_email_id = jsonObject_userprofile.getString("email");
                            Up_home_passcode = jsonObject_userprofile.getString("passcode");
                            session.setSecurePin(Up_home_passcode);
                        }
                    } else {
                        Smessage = object.getString("response");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    profileLoadingProgressbar.setVisibility(View.GONE);
                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    Alert1(getResources().getString(R.string.action_success), Smessage);

                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
                profileLoadingProgressbar.setVisibility(View.GONE);
            }

            @Override
            public void onErrorListener() {
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
        });
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewProfile_Page_Login_Pincode_Edit.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_Login_Pincode_Edit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_Login_Pincode_Edit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent local = new Intent();
                local.setAction("com.app.PaymentListPage.refreshPage");
                local.putExtra("refresh", "yes");
                sendBroadcast(local);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(NewProfile_Page_Login_Pincode_Edit.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
//        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }

}
