package com.blackmaria.volley;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import androidx.multidex.MultiDex;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.blackmaria.dbconnection.Ridedetails_db;
import com.blackmaria.utils.MyActivityLifecycleCallbacks;
import com.blackmaria.utils.SessionManager;
import com.devs.acr.AutoErrorReporter;
import com.google.firebase.FirebaseApp;

//import com.crashlytics.android.Crashlytics;
//import io.fabric.sdk.android.Fabric;


public class AppController extends Application {
 
    public static final String TAG = AppController.class.getSimpleName();
 
    private RequestQueue mRequestQueue;

    SessionManager sessionManager;
 
    private static AppController mInstance;

    public static Ridedetails_db ridedetails_db;
 
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        mInstance = this;

        ridedetails_db = new Ridedetails_db(this);
        MultiDex.install(this);

       /* AutoErrorReporter.get(this)
                .setEmailAddresses("pandiyan@teamtweaks.com")
                .setEmailSubject("Crash Report")
                .start();*/
    }
    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);

        registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks());
    }
 
    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public static synchronized Ridedetails_db getDBInstance(Context context) {
        if (ridedetails_db == null) {
            ridedetails_db = new Ridedetails_db(context);
        }
        return ridedetails_db;
    }

    public static void setDBInstance(Context context) {
        ridedetails_db = new Ridedetails_db(context);
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
 
        return mRequestQueue;
    }
 
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }
 
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
 
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


}