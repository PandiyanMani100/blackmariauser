package com.blackmaria;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.adapter.NewCarListAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.InterFace.CallBack;
import com.blackmaria.newdesigns.adapter.FindplaceAdapter_constrain;
import com.blackmaria.newdesigns.slider.CirclePageIndicator;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.GeocoderHelper;
import com.blackmaria.utils.GoogleNavigationService;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/**
 * Created by user144 on 6/28/2017.
 */

public class SearchNearBytoBooking extends ActivityHockeyApp implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private RefreshReceiver refreshReceiver;
    LanguageDb mhelper;
    //-----Declaration For Enabling Gps-------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 299;
    private RelativeLayout sosPageLayout;
    private ServiceRequest mRequest;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    GPSTracker gps;
    private double SpickupLatitude, SpickupLongitude;
    private String SpickupLocation = "";
    private TextView placename, placedistance;
    private TextView /*Tv_PickupAddress*/ address, getDistance, getTime, book_cars_textview, phone_num;
    private String searchType = "", placeName = "", placeImage = "", placeDistamnce = "", placerating = "", currentLat = "", currentLon = "", placeLat = "", placeLon = "";
    private double currentLat1 = 0.0, currentLon1 = 0.0, placeLat1 = 0.0, placeLon1 = 0.0;
    private ImageView shopimage;
    //    private RatingBar placerating1;
    private TextView getdirectiolayout;
    private LinearLayout scheduleBooking;
    private TextView carbooking_lyt;
    String[] placeNameAddress;
    final int PERMISSION_REQUEST_NAVIGATION_CODE = 222;
    public static int OVERLAY_PERMISSION_REQ_CODE = 1234;
    final int PERMISSION_REQUEST_CODE = 111;
    private String destAddress = "", Savailable_count, placeNumber = "", Pickup_location = "", Pickup_lat = "", Pickup_lon = "";

    private ImageView btn_home, back;
    private TextView Tv_wish, Tv_userName;
    private SessionManager session;
    private String UserID = "", UserName = "", CategoryID = "", Gender = "";
    RelativeLayout showMeLayout;
    Map<String, List<String>> carList;
    private ArrayList<String> mylist = new ArrayList<>();

    private ViewPager mPager;


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(BookingPage1.this, BookingPage1.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(SearchNearBytoBooking.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(SearchNearBytoBooking.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.findplace_placesearch_constrain);
        mhelper= new LanguageDb(this);
        Intialize();
        phone_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phone_num.getText().toString().contains("MOBILE") || phone_num.getText().toString().contains("mobile"))
                {

                }
                else
                {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+phone_num.getText().toString()));
                    startActivity(intent);
                }

            }
        });
        getdirectiolayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Build.VERSION.SDK_INT >= 23) {
                    if (!checkWriteExternalStoragePermission()) {
                        requestNavigationPermission();
                    } else {
                        if (!Settings.canDrawOverlays(SearchNearBytoBooking.this)) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
                        } else {
                            moveNavigation();
                        }
                    }
                } else {
                    moveNavigation();
                }

            }
        });
        carbooking_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("-----------SdestinationLatitude----------------" + placeLat);
                System.out.println("-----------SdestinationLongitude----------------" + placeLon);
                System.out.println("-----------SdestinationLocation----------------" + placeName);


                Intent intent = new Intent(SearchNearBytoBooking.this, SelectVehicleCategoryPage.class);
                intent.putExtra("pickupAddress", Pickup_location);
                intent.putExtra("pickupLatitude", Pickup_lat);
                intent.putExtra("pickupLongitude", Pickup_lon);
                intent.putExtra("destinationAddress", placeName);
                intent.putExtra("destinationLatitude", placeLat);
                intent.putExtra("destinationLongitude", placeLon);
                intent.putExtra("page", "BookingPage1");
                intent.putExtra("intentCall", "findplaceBooking");
                startActivity(intent);
//                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });


//        scheduleBooking.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                System.out.println("-----------SdestinationLatitude----------------" + placeLat);
//                System.out.println("-----------SdestinationLongitude----------------" + placeLon);
//                System.out.println("-----------SdestinationLocation----------------" + placeName);
//
//
//                Intent intent = new Intent(SearchNearBytoBooking.this, SelectVehicleCategoryPage.class);
//                intent.putExtra("pickupAddress", SpickupLocation);
//                intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
//                intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
//                intent.putExtra("destinationAddress", placeName);
//                intent.putExtra("destinationLatitude", placeLat);
//                intent.putExtra("destinationLongitude", placeLon);
//                intent.putExtra("page", "BookingPage1");
//                intent.putExtra("intentCall", "findplaceBooking");
//                startActivity(intent);
////                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);
//
//            }
//        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(back.getWindowToken(), 0);
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }

    private void Intialize() {
        carList = new HashMap<>();
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        gps = new GPSTracker(SearchNearBytoBooking.this);
        session = new SessionManager(SearchNearBytoBooking.this);
//        Tv_PickupAddress = (CustomTextView) findViewById(R.id.book_my_ride_pickup_address_textView);
        shopimage = (ImageView) findViewById(R.id.shopimage);
        placename = (TextView) findViewById(R.id.placename);
        placedistance = (TextView) findViewById(R.id.placedistance);
//        placerating1 = (RatingBar) findViewById(R.id.placerating);
        address = findViewById(R.id.address);
        getdirectiolayout = findViewById(R.id.getdirectiolayout);
        getdirectiolayout.setText(getkey("get_direction"));
        carbooking_lyt = findViewById(R.id.carbooking_lyt);
        carbooking_lyt.setText(getkey("book_now_lable"));
//        scheduleBooking = (LinearLayout) findViewById(R.id.carbooking_schedule_ll);
//        getDistance = (CustomTextView) findViewById(R.id.getdistance);
//        getTime = (CustomTextView) findViewById(R.id.gettime);
        back = findViewById(R.id.back_button);
//        btn_home = (ImageView) findViewById(R.id.home_image);
        phone_num = (TextView) findViewById(R.id.phone_num);
        book_cars_textview = findViewById(R.id.book_my_ride_no_of_cars_textview);
        Tv_wish = findViewById(R.id.book_my_ride_wish_textview);
        mPager = findViewById(R.id.pager);
//        Tv_userName = (CustomTextView) findViewById(R.id.book_my_ride_user_name_textview);
//        showMeLayout = (RelativeLayout) findViewById(R.id.carbooking_lyt1);


        carList = session.carList;

        TextView find_place_txt = (TextView) findViewById(R.id.find_place_txt);
        find_place_txt.setText(getkey("booking_page_findplaces"));


        Intent intent = getIntent();
        if (intent.hasExtra("placeType")) {
            searchType = intent.getStringExtra("placeType");
        }
        if (getIntent().hasExtra("list")) {
            mylist.clear();
            mylist = getIntent().getStringArrayListExtra("list");
        }

        placeName = intent.getStringExtra("placeName");
        placeImage = intent.getStringExtra("placeImage");
        placeDistamnce = intent.getStringExtra("placeDistamnce");
        placerating = intent.getStringExtra("placerating");
        currentLat = intent.getStringExtra("currentLat");
        currentLon = intent.getStringExtra("currentLon");
        placeLat = intent.getStringExtra("placeLat");
        placeLon = intent.getStringExtra("placeLon");
        Savailable_count = intent.getStringExtra("Savailable_count");
        Pickup_location = intent.getStringExtra("Pickup_location");
        Pickup_lat = getIntent().getStringExtra("pickuplat");
        Pickup_lon = getIntent().getStringExtra("pickuplon");
        placeNumber = intent.getStringExtra("placeNumber");

        if (!placeNumber.equalsIgnoreCase("")) {
            phone_num.setText(placeNumber);
        } else {
            phone_num.setText(getkey("phonenumber_not_available"));
        }
        currentLat1 = Double.parseDouble(currentLat);
        currentLon1 = Double.parseDouble(currentLon);
        placeLat1 = Double.parseDouble(placeLat);
        placeLon1 = Double.parseDouble(placeLon);

        placeNameAddress = placeName.split(",");
        placename.setText(placeNameAddress[0]);

        if (searchType.equalsIgnoreCase("atm") || searchType.startsWith("atm")) {
            shopimage.setBackgroundDrawable(getResources().getDrawable(R.drawable.icn_atm));
        } else if (searchType.equalsIgnoreCase("cafe") || searchType.startsWith("cafe")) {
            shopimage.setBackgroundDrawable(getResources().getDrawable(R.drawable.icn_cafe));
        } else if (searchType.equalsIgnoreCase("food") || searchType.startsWith("food")) {
            shopimage.setBackgroundDrawable(getResources().getDrawable(R.drawable.icn_food));
        } else if (searchType.equalsIgnoreCase("shopping") || searchType.startsWith("shopping")) {
            shopimage.setBackgroundDrawable(getResources().getDrawable(R.drawable.icn_shopping));
        } else if (searchType.equalsIgnoreCase("hotel") || searchType.startsWith("hotel")) {
            shopimage.setBackgroundDrawable(getResources().getDrawable(R.drawable.icn_hotel));
        } else if (searchType.equalsIgnoreCase("spa") || searchType.startsWith("spa")) {
            shopimage.setBackgroundDrawable(getResources().getDrawable(R.drawable.icn_spa));
        } else {
            Picasso.with(getApplicationContext()).load(placeImage).into(shopimage);
        }

        placedistance.setText(placeDistamnce + " KM AWAY");
//        placerating1.setRating(Float.parseFloat(placerating));
        address.setText(placeName);
//        getDistance.setText(placeDistamnce + "KM");


        book_cars_textview.setText(getkey("there_is_lable") + " " + Savailable_count + " " + "BLACKMARIA'S NEAR YOU");

        if (gps.canGetLocation() && gps.isgpsenabled()) {
            SpickupLatitude = gps.getLatitude();
            SpickupLongitude = gps.getLongitude();
            SpickupLocation = new GeocoderHelper().fetchCityName(SearchNearBytoBooking.this, SpickupLatitude, SpickupLongitude, callBack);//
//            Tv_PickupAddress.setText(SpickupLocation);

        } else {
            enableGpsService();
        }
        HashMap<String, String> info = session.getUserDetails();
        CategoryID = info.get(SessionManager.KEY_CATEGORY);
        UserID = info.get(SessionManager.KEY_USERID);
        UserName = info.get(SessionManager.KEY_USERNAME);
        session.setTimerPage("");
        HashMap<String, String> info1 = session.getGender();
        Gender = info1.get(SessionManager.KEY_GENDER);

       /*GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(Rl_drawer);
        Glide.with(this).load(R.drawable.alertgif).into(imageViewTarget);*/

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (Gender.equalsIgnoreCase("male")) {
//            Tv_userName.setText("Mr " + UserName.toUpperCase());
            UserName = "Mr " + UserName.toUpperCase();
            System.out.println("==============Murugan UserName=========" + Gender + " " + UserName);
        } else if (Gender.equalsIgnoreCase("female")) {
//            Tv_userName.setText("Mrs " + UserName.toUpperCase());
            UserName = "Mrs " + UserName.toUpperCase();
            System.out.println("==============Murugan UserName=========" + Gender + " " + UserName);
        } else {
//            Tv_userName.setText(UserName.toUpperCase());
            UserName = UserName.toUpperCase();
            System.out.println("==============Murugan UserName=========" + Gender + " " + UserName);
        }

        if (timeOfDay >= 0 && timeOfDay < 12) {
            Tv_wish.setText(getkey("good_morning_lable").toUpperCase() + " " + UserName);
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            Tv_wish.setText(getkey("good_afternoon_lable").toUpperCase() + " " + UserName);
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            Tv_wish.setText(getkey("good_evening_lable").toUpperCase() + " " + UserName);
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            Tv_wish.setText(getkey("good_night_lable").toUpperCase() + " " + UserName);
        }


        //RandomView();
        //RandomView2();
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        registerReceiver(refreshReceiver, intentFilter);
        LatLng origin = new LatLng(currentLat1, currentLon1);
        LatLng dest = new LatLng(placeLat1, placeLon1);

        String sTravelTimeUrl = getDirectionsUrl(origin, dest);

        init_slidebanner(mylist);
        //   String sFindPhoneNumberUrl = getPhoneNumberUrl(placeId);
        Postdata(sTravelTimeUrl);
    }


    private String getPhoneNumberUrl(String placeId) {
        String url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeId + "&key=" + "AIzaSyCTUEtptTc_iVc8EVhLfmL1qDHYaEo6iHQ";
        return url;
    }


    private void Postdata(String Url) {
        cd = new ConnectionDetector(SearchNearBytoBooking.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            if (mRequest != null) {
                mRequest.cancelRequest();
            }

            getDistancetime(Url);
        }

    }


    private void showCarList_Dialog() {
        final Dialog dialog = new Dialog(SearchNearBytoBooking.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.car_list_dialog);

        ListView vechiles_listview = (ListView) dialog.findViewById(R.id.nearest_vechile_list_listview);
        RelativeLayout close = (RelativeLayout) dialog.findViewById(R.id.car_list_nearest_vechile_close_layout);
        TextView nearest_vechile = (TextView) dialog.findViewById(R.id.car_list_nearest_vechile_textview);

        nearest_vechile.setText(carList.get("distance").size() + getkey("nearest_vehicles_lable"));

        NewCarListAdapter personListAdapter = new NewCarListAdapter(SearchNearBytoBooking.this, carList);
        vechiles_listview.setAdapter(personListAdapter);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        Window w = dialog.getWindow();
        w.setLayout((width * 6) / 8, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.show();
    }


    private void getDistancetime(String url) {

        System.out.println("--------------nearBySearchDuration  url-------------------" + url);

        mRequest = new ServiceRequest(SearchNearBytoBooking.this);
        mRequest.makeServiceRequest(url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------nearBySearchDuration   reponse-------------------" + response);
                String status = "";
                String distance = "";
                String duration = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");
                        if (status.equalsIgnoreCase("OK")) {
                            JSONArray jPlaces_des = object.getJSONArray("destination_addresses");
                            destAddress = (String) jPlaces_des.get(0);
                            JSONArray jPlaces_pickup = object.getJSONArray("origin_addresses");
                            String pickupAddress = (String) jPlaces_pickup.get(0);

                            JSONArray jPlaces = object.getJSONArray("rows");
                            for (int i = 0; i < jPlaces.length(); i++) {
                                JSONObject object1 = jPlaces.getJSONObject(i);
                                JSONArray ja = object1.getJSONArray("elements");
                                for (int j = 0; j < ja.length(); j++) {
                                    JSONObject object2 = ja.getJSONObject(j);
                                    JSONObject distanceobj = object2.getJSONObject("distance");
                                    distance = distanceobj.getString("text");
                                    JSONObject timeobj = object2.getJSONObject("duration");
                                    duration = timeobj.getString("text");
                                }

                            }
                        }
                    }
                    if (status.equalsIgnoreCase("OK")) {
//                        getDistance.setText(distance);
//                        getTime.setText(duration);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                // close keyboard

            }
        });
    }


    private boolean isGoogleMapsInstalled() {
        try {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {

            System.out.println("=========serviceClass.getName()=========" + serviceClass.getName());
            System.out.println("=========serviceClass.geafasaggaaa=========" + service.service.getClassName());

            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

    private void moveNavigation() {

        if (isGoogleMapsInstalled()) {
            if (!isMyServiceRunning(GoogleNavigationService.class)) {
                startService(new Intent(getApplicationContext(), GoogleNavigationService.class));
            }
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(placeLat), Double.parseDouble(placeLon), "&mode=c")));
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), getkey("alert_label_no_google_map_installed"), Toast.LENGTH_LONG).show();
        }
//        addBackLayout();
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
// Origin of route
        String str_origin = "origins=" + origin.latitude + "," + origin.longitude;
// Destination of route
        String str_dest = "destinations=" + dest.latitude + "," + dest.longitude;
        //Mode of drivng=""
        String mode = "mode=driving";
// Sensor enabled
        String sensor = "sensor=false";
//language
        String language = "language=en-EN";
// Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode + "&" + language + "&" + sensor;
// Output format
        String output = "json";
// Building the url to the web service
        String url = "http://maps.googleapis.com/maps/api/distancematrix/" + output + "?" + parameters;
        return url;
    }

    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(SearchNearBytoBooking.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SearchNearBytoBooking.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestNavigationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_NAVIGATION_CODE);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    //-------------Method to get Complete Address------------
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(SearchNearBytoBooking.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                Log.e("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Current loction address", "Cannot get Address!");
        }
        return strAdd;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {


            case PERMISSION_REQUEST_NAVIGATION_CODE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.canDrawOverlays(SearchNearBytoBooking.this)) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, 1234);
                    } else {
                        moveNavigation();
                    }
                }
        }
    }

    private void RandomView() {


        // Randomise a background
        int[] asoderimage = {R.drawable.random_img1, R.drawable.random_img2, R.drawable.random_img3, R.drawable.random_img4, R.drawable.random_img5, R.drawable.random_img6};

        Random random = new Random(System.currentTimeMillis());
        int posOfImage = random.nextInt(asoderimage.length - 1);
        ImageView Iv_show_car_nw = (ImageView) findViewById(R.id.random_show_car1);
        System.out.println("-----------posOfImage number----------------" + posOfImage);
        Iv_show_car_nw.setBackgroundResource(asoderimage[posOfImage]);


    }


    private void RandomView2() {

        // Randomise a background
        int[] asoderimage = {R.drawable.random_img6, R.drawable.random_img5, R.drawable.random_img4, R.drawable.random_img3, R.drawable.random_img2, R.drawable.random_img1};

        Random random = new Random(System.currentTimeMillis());
        int posOfImage = random.nextInt(asoderimage.length - 1);
        ImageView Iv_show_car_nw2 = (ImageView) findViewById(R.id.random_show_car2);
        System.out.println("-----------posOfImage number-2---------------" + posOfImage);
        Iv_show_car_nw2.setBackgroundResource(asoderimage[posOfImage]);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(SearchNearBytoBooking.this)) {
                    Toast.makeText(SearchNearBytoBooking.this, "SYSTEM_ALERT_WINDOW permission not granted...", Toast.LENGTH_SHORT).show();
                } else {
                    moveNavigation();
                }
            }
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(SearchNearBytoBooking.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    CallBack callBack = new CallBack() {

        @Override

        public void onComplete(String LocationName) {
            System.out.println("-------------------addreess----------------0" + LocationName);
//            Tv_PickupAddress.setText(LocationName);
            SpickupLocation = LocationName;
        }

        @Override

        public void onError(String errorMsg) {


        }

        @Override
        public void onCompleteAddress(String message) {

        }

    };

    private void init_slidebanner(ArrayList<String> booking_banner_arr) {
//        for (int i = 0; i < booking_banner_arr.size(); i++)
            mPager.setAdapter(new FindplaceAdapter_constrain(SearchNearBytoBooking.this, booking_banner_arr));
        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);
        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);

        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}

