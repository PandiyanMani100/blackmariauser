package com.blackmaria;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.DowloadPdfActivity;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.widgets.CircularImageView;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.TextRCBold;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by user144 on 9/18/2017.
 */
public class NewCloudMoneyTransferUrlResponse extends ActivityHockeyApp implements View.OnClickListener, DowloadPdfActivity.downloadCompleted{
    private TextRCBold fareTv;
    private CustomTextView userrideresponse;
    private CustomTextView userTripnum;
    private TextView userRidenum;
    private CircularImageView ratingPageProfilephoto1;
    private CustomTextView userNameTv, Tv_current_username, Tv_date, Tv_time;
    private CustomTextView userCityTv;
    private CustomTextView userStatusTv;
    private RelativeLayout closeBtn,save_btn ;
    SessionManager session;
    private String current_userName = "";

    private String transNum = "", userNmae = "", userImage = "", userStatus = "", userCity = "", transAmount = "", crosstransfer="",currency = "",time="";
    private RefreshReceiver refreshReceiver;
   TextView type_cloude_trnfr;

    LinearLayout pdflayout;
    RelativeLayout savelyt;
    private TextView sava_tv;
    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyTransferUrlResponse.this, Navigation_new.class);
                        startActivity(intent1);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyTransferUrlResponse.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyTransferUrlResponse.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        new_cloud_transfer_complete_responsepage

        setContentView(R.layout.cloud_transfer_compalete_page);

        initialize();
    }

    private void initialize() {

        session = new SessionManager(NewCloudMoneyTransferUrlResponse.this);

        fareTv = (TextRCBold) findViewById(R.id.fare_tv);
//        userrideresponse = (CustomTextView) findViewById(R.id.userrideresponse);
//        userTripnum = (CustomTextView) findViewById(R.id.user_tripnum);
        userRidenum = (TextView) findViewById(R.id.user_ridenum);
        ratingPageProfilephoto1 = (CircularImageView) findViewById(R.id.rating_page_profilephoto1);
        userNameTv = (CustomTextView) findViewById(R.id.user_name_tv);
        userCityTv = (CustomTextView) findViewById(R.id.user_city_tv);
//        userStatusTv = (CustomTextView) findViewById(R.id.user_status_tv);
        closeBtn = (RelativeLayout) findViewById(R.id.close_btn);
        Tv_current_username = (CustomTextView) findViewById(R.id.current_username);
        Tv_date = (CustomTextView) findViewById(R.id.trans_date);
        Tv_time = (CustomTextView) findViewById(R.id.trans_time);
        save_btn= (RelativeLayout) findViewById(R.id.save_btn);
        type_cloude_trnfr = (CustomTextView) findViewById(R.id.type_cloude_trnfr);

        pdflayout=(LinearLayout) findViewById(R.id.pdflayout);
        savelyt= (RelativeLayout) findViewById(R.id.savelyt);
        sava_tv= (TextView) findViewById(R.id.sava_tv);

        Intent in = getIntent();
        transNum = in.getStringExtra("transaction_number");
        userNmae = in.getStringExtra("user_name");
        userImage = in.getStringExtra("user_image");
        userStatus = in.getStringExtra("status");
        userCity = in.getStringExtra("city");
        transAmount = in.getStringExtra("reloadamount");
        currency = in.getStringExtra("currency");
        time = in.getStringExtra("time");
        crosstransfer= in.getStringExtra("crosstransfer");
        HashMap<String, String> user_name = session.getUserDetails();
        current_userName = user_name.get(SessionManager.KEY_USERNAME);


        if(crosstransfer.equalsIgnoreCase("1")){
            type_cloude_trnfr.setVisibility(View.VISIBLE);
        }


        userCityTv.setText(userCity);
//        userStatusTv.setText(userStatus);
        userNameTv.setText(userNmae);
        userRidenum.setText(getResources().getString(R.string.trans_no) + " " + ":" + " "+ transNum);
        fareTv.setText(currency + " " + transAmount);
        Tv_current_username.setText(userNmae);
        Tv_date.setText(GetCurrentDate());
        Tv_time.setText(time);
        Picasso.with(NewCloudMoneyTransferUrlResponse.this).load(String.valueOf(userImage)).into(ratingPageProfilephoto1);


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        save_btn.setOnClickListener(this);
        closeBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == closeBtn) {
            Intent intent = new Intent(NewCloudMoneyTransferUrlResponse.this, WalletMoneyPage1.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
            // Handle clicks for closeBtn
        }else if(v==save_btn){
            savelyt.setVisibility(View.GONE);
            new DowloadPdfActivity(NewCloudMoneyTransferUrlResponse.this,pdflayout,"moneytransfer",NewCloudMoneyTransferUrlResponse.this);

        }
    }


    private String GetCurrentDate() {

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }


    private String GetCurrentTime() {

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss aaa");
        String formattedtime = df.format(c.getTime());

        return formattedtime;
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(NewCloudMoneyTransferUrlResponse.this, WalletMoneyPage1.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }



    @Override
    public void OnDownloadComplete(String fileName) {
        savelyt.setVisibility(View.VISIBLE);
        sava_tv.setText(getResources().getString(R.string.saved));

        ViewPdfAlert("",getResources().getString(R.string.sucessss),fileName);
    }
    //--------------Alert Method-----------
    private void ViewPdfAlert( String title, String alert,final String fileName) {

        final PkDialog mDialog = new PkDialog(NewCloudMoneyTransferUrlResponse.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.open), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPDF(fileName);
                mDialog.dismiss();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    public void viewPDF(String pdfFileName) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Signature/" + pdfFileName);  // -> filename = maven.pdf
        Uri uri_path = Uri.fromFile(pdfFile);
        Uri uri = uri_path;

        try {
            File file = null;
            String path = uri.toString();// "/mnt/sdcard/FileName.mp3"
            try {
                file = new File(new URI(path));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            final Intent intent = new Intent(Intent.ACTION_VIEW)//
                    .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                    androidx.core.content.FileProvider.getUriForFile(NewCloudMoneyTransferUrlResponse.this, getPackageName() + ".provider", file) : Uri.fromFile(file),
                            "application/pdf").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(NewCloudMoneyTransferUrlResponse.this, getResources().getString(R.string.no_pdf), Toast.LENGTH_SHORT).show();
            }

        } catch (ActivityNotFoundException e) {
            Toast.makeText(NewCloudMoneyTransferUrlResponse.this, getResources().getString(R.string.no_read_file), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("=========Muruga view PDF==========="+data+resultCode+requestCode);


    }
}
