package com.blackmaria;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Telephony;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ShareCompat;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.CustomSpinnerAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.newdesigns.fastwallet.Fastpaypincheck;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

import static com.blackmaria.HomePage.isValidEmail;


/**
 * Created by user144 on 5/17/2017.
 */
public class InviteAndEarn extends ActivityHockeyApp implements View.OnClickListener {
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    LanguageDb mhelper;
    private SessionManager session;
    private TextView Tv_referral_code;
    private CustomTextView Tv_barcode_referrral, Tv_how_works;
    private LinearLayout IV_whatsApp, IV_instagram, IV_sms, IV_sykpe, IV_twitter, IV_facebook, IV_gplus;
    ImageView back, cancel;
    private ServiceRequest mRequest;
    private String UserID = "";
    private boolean isdataPresent = false;
    private String Sstatus = "", image = "", friend_earn_amount = "", you_earn_amount = "", friends_rides = "", ScurrencyCode = "", referral_code = "", sShareLink = "";
    String sCurrencySymbol = "";
    AppInfoSessionManager SappInfo_Session;
    final int PERMISSION_REQUEST_CODE = 111;
    final int GOOGLE_PLUS_SHARE_REQUEST_CODE = 222;
    private String Ssubject = "", Smessage = "";
    private Dialog dialog;
    private Dialog alert_dialog;
    private String Spage_title = "", Spage_description = "";
    private boolean check_status = false;
    TextView dashboardvclick;
    private String isFirstTime = "";
    private RefreshReceiver refreshReceiver;
    Uri imageUri = null;
    private String sPage = "";
    HashMap<String, String> info;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(InviteAndEarn.this, MyRideRating.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(InviteAndEarn.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            } else if (intent.getAction().equals("profileupdated")) {
                if (isInternetPresent) {
                    displayInvite_Request(Iconstant.invite_earn_friends_url);
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invitefriendsnewpage);
        mhelper = new LanguageDb(this);
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here

        }
        initialize();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getkey("profile_label_menu").equalsIgnoreCase(sPage)) {
                    overridePendingTransition(R.anim.exit, R.anim.enter);
                    finish();
                } else {
                    Intent intent1 = new Intent(InviteAndEarn.this, Navigation_new.class);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        });

    }

    private void initialize() {
        session = new SessionManager(InviteAndEarn.this);
        cd = new ConnectionDetector(InviteAndEarn.this);
        isInternetPresent = cd.isConnectingToInternet();
        SappInfo_Session = new AppInfoSessionManager(InviteAndEarn.this);
        IV_facebook = (LinearLayout) findViewById(R.id.invite_earn_fb_layout);
        IV_twitter = (LinearLayout) findViewById(R.id.invite_earn_twitter_layout);
        IV_instagram = (LinearLayout) findViewById(R.id.invite_earn_instogram_layout);
        IV_whatsApp = (LinearLayout) findViewById(R.id.invite_earn_watsup_layout);
        IV_gplus = (LinearLayout) findViewById(R.id.invite_earn_googleplus_layout);
        IV_sykpe = (LinearLayout) findViewById(R.id.invite_earn_skype_layout);
        back = (ImageView) findViewById(R.id.img_back);
        dashboardvclick = findViewById(R.id.dashboardvclick);
        dashboardvclick.setText(getkey("dashboard"));
        Tv_referral_code = findViewById(R.id.invite_earn_referral_code_tv);

        TextView myres = findViewById(R.id.myres);
        myres.setText(getkey("my_referral_code"));

        TextView nextpayout = findViewById(R.id.nextpayout);
        nextpayout.setText(getkey("invite_friends_to_my_dashboard"));


        Intent intent = getIntent();
        if (intent.hasExtra("str_page")) {
            sPage = intent.getStringExtra("str_page");
        }
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("profileupdated");
        registerReceiver(refreshReceiver, intentFilter);


        IV_whatsApp.setOnClickListener(this);
        IV_instagram.setOnClickListener(this);
//        IV_sms.setOnClickListener(this);
        IV_sykpe.setOnClickListener(this);
        IV_twitter.setOnClickListener(this);
        IV_facebook.setOnClickListener(this);
        IV_gplus.setOnClickListener(this);

        dashboardvclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        if (!session.getReferelStatus().equalsIgnoreCase("0")) {
            LearnMoreDialog();
        }
        if (isInternetPresent) {
            displayInvite_Request(Iconstant.invite_earn_friends_url);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }
    }


    @Override
    public void onClick(View v) {
        if (isdataPresent) {

            info = session.getUserCompleteProfile();
            String UserProfileUpdateStatus = info.get(SessionManager.COMPLETE_PROFILE);
            if (UserProfileUpdateStatus.equalsIgnoreCase("0")) {
                UpdateProfilePopUp(v);
            } else {
                if (v == IV_whatsApp) {
                    imageUri = Uri.parse(image);

                   Uri imageUri = null;
                    try {
                        imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                                BitmapFactory.decodeResource(getResources(), R.drawable.blackmaria_share), null, null));
                    } catch (NullPointerException e) {
                    }

                    try {
                        URL url = new URL(image);
                        Bitmap image = BitmapFactory.decodeResource(getResources(),
                                R.drawable.app_icon);
                        shareInstagramelink(Smessage, image);
                    } catch(IOException e) {
                        System.out.println(e);
                    }
                    /*if (sShareLink.length() > 0) {
                        System.out.println("link");
                        //shareFacebookLink(sShareLink);

                    } else {
                        System.out.println("text");
                        shareInstagrame(Smessage);
                    }*/
//                shareInstagrame(Smessage, imageUri);
                } else if (v == IV_twitter) {
                    try {
                        imageUri = Uri.parse(image);//Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),                            BitmapFactory.decodeResource(getResources(), R.drawable.invite_and_earn_car), null, null));
                    } catch (NullPointerException e) {
                    }
                    shareTwitter(Smessage, imageUri);

                } else if (v == IV_facebook) {
                    try {
                        imageUri = Uri.parse(image);
                    } catch (NullPointerException e) {
                    }
                    try {
                        URL url = new URL(image);
                        Bitmap image = BitmapFactory.decodeResource(getResources(),
                                R.drawable.app_icon);
                        shareFacebook(Smessage, image);
                    } catch(IOException e) {
                        System.out.println(e);
                    }
                    //shareFacebook(Smessage, imageUri);
                   /* if (sShareLink.length() > 0) {
                        shareFacebookLink(sShareLink);
                    } else {
                        shareFacebook(Smessage, imageUri);
                    }*/
                } else if (v == IV_gplus) {
                    whatsApp_sendMsg(Smessage);
                    try {
                        imageUri = Uri.parse(image);// Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                    } catch (NullPointerException e) {
                    }


                } else if (v == IV_sykpe) {
                    Uri imageUri = null;
                    try {
                        imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                                BitmapFactory.decodeResource(getResources(), R.drawable.blackmaria_share), null, null));
                    } catch (NullPointerException e) {
                    }
                    if (sShareLink.length() > 0) {
                        telegram(Smessage, imageUri);

                    } else {
                        telegram(Smessage, imageUri);
                    }



//telegram
                } else if (v == IV_instagram) {
//                    shareGPlus(Smessage, imageUri);
                    wechat(Smessage,imageUri);
                    //we chat

                }
            }

        } else {
            Alert(getkey("alert_label_title"), getkey("invite_earn_label_problem_server"));
        }
    }

    private void telegram(String smessage, Uri imageUri) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, smessage);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_STREAM, imageUri);
        intent.setType("image/jpeg");
        intent.setPackage("org.telegram.messenger");

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_telegram_not_installed"), "https://play.google.com/store/apps/details?id=org.telegram.messenger&hl=en");
        }
    }


    private void UpdateProfilePopUp(final View vierw) {
        final Dialog dialog = new Dialog(InviteAndEarn.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup1);
        CustomTextView Tv_Later = (CustomTextView) dialog.findViewById(R.id.later_tv);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_profilechange);
        CheckBox profile_checkbox = (CheckBox) dialog.findViewById(R.id.profile_checkbox);
        String checkBoxStatus = "No";
        if (profile_checkbox.isChecked()) {
            checkBoxStatus = "Yes";
        }
        TextView Profilechange = (TextView) dialog.findViewById(R.id.txt_updaste);
        Typeface tf = Typeface.createFromAsset(InviteAndEarn.this.getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        final String finalCheckBoxStatus = checkBoxStatus;
        Tv_Later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                info = session.getUserBasicProfile();
                String basicUserProfileUpdateStatus = info.get(SessionManager.BASIC_PROFILE);
                if (basicUserProfileUpdateStatus.equalsIgnoreCase("0")) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    UpdateProfilePopUp2(finalCheckBoxStatus);
                } else {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    System.out.println("==============Muruga basicUserProfileUpdateStatus==========" + basicUserProfileUpdateStatus);
                    socialview(vierw);
                }
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_profile = new Intent(InviteAndEarn.this, Fastpaypincheck.class);
                intent_profile.putExtra("frommenu", "1");
                startActivity(intent_profile);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void socialview(View v){
        if (v == IV_whatsApp) {
            imageUri = Uri.parse(image);

            Uri imageUri = null;
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                        BitmapFactory.decodeResource(getResources(), R.drawable.blackmaria_share), null, null));
            } catch (NullPointerException e) {
            }
            try {
                URL url = new URL(image);
                Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                shareInstagramelink(Smessage, image);
            } catch(IOException e) {
                System.out.println(e);
            }
            /*if (sShareLink.length() > 0) {
                System.out.println("link");
                //shareFacebookLink(sShareLink);


            } else {
                System.out.println("text");
                shareInstagrame(Smessage);
            }*/
//                shareInstagrame(Smessage, imageUri);
        } else if (v == IV_twitter) {
            try {
                imageUri = Uri.parse(image);//Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),                            BitmapFactory.decodeResource(getResources(), R.drawable.invite_and_earn_car), null, null));
            } catch (NullPointerException e) {
            }
            shareTwitter(Smessage, imageUri);

        } else if (v == IV_facebook) {
            try {
                imageUri = Uri.parse(image);
            } catch (NullPointerException e) {
            }
            try {
                URL url = new URL(image);
                Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                shareFacebook(Smessage, image);
            } catch(IOException e) {
                System.out.println(e);
            }
            /*if (sShareLink.length() > 0) {
                shareFacebookLink(sShareLink);
            } else {
                shareFacebook(Smessage, imageUri);
            }*/
        } else if (v == IV_gplus) {
            whatsApp_sendMsg(Smessage);
            try {
                imageUri = Uri.parse(image);// Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
            } catch (NullPointerException e) {
            }


        } else if (v == IV_sykpe) {
            Uri imageUri = null;
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                        BitmapFactory.decodeResource(getResources(), R.drawable.blackmaria_share), null, null));
            } catch (NullPointerException e) {
            }
            if (sShareLink.length() > 0) {
                telegram(Smessage, imageUri);

            } else {
                telegram(Smessage, imageUri);
            }


            //telegram

        } else if (v == IV_instagram) {
//            shareGPlus(Smessage, imageUri);
            wechat(Smessage, imageUri);
            //wechat

        }
    }

    private void wechat(String smessage, Uri imageUri) {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, smessage);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_STREAM, imageUri);
        intent.setType("image/jpeg");
        intent.setPackage("com.tencent.mm");

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_wechat_not_installed"), "https://play.google.com/store/apps/details?id=com.tencent.mm&hl=en");
        }
    }

    private void UpdateProfilePopUp2(final String needAssistance) {

        final Dialog dialog = new Dialog(InviteAndEarn.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup2);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_tv);

        final CustomEdittext userName = (CustomEdittext) dialog.findViewById(R.id.name_title);
        final CustomEdittext userEmail = (CustomEdittext) dialog.findViewById(R.id.email_title);
        final CustomEdittext user_age_et = (CustomEdittext) dialog.findViewById(R.id.user_age_et);
        final ImageView closeimage = (ImageView) dialog.findViewById(R.id.img_close);
        CustomTextView Profilechange = (CustomTextView) dialog.findViewById(R.id.profile_text1);
        Typeface tf = Typeface.createFromAsset(InviteAndEarn.this.getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        Spinner spinnerCustom = (Spinner) dialog.findViewById(R.id.gender_spinner1);
        final TextView Tv_city = (TextView) dialog.findViewById(R.id.ratecard_city_textview);
        // Spinner Drop down elements
        ArrayList<String> spinnerItems = new ArrayList<>();
        spinnerItems.add(getkey("seletc_gender"));
        spinnerItems.add(getkey("male"));
        spinnerItems.add(getkey("female"));
        spinnerItems.add(getkey("others"));

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(InviteAndEarn.this, spinnerItems);
        spinnerCustom.setAdapter(customSpinnerAdapter);
        spinnerCustom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // close keyboard
                return false;
            }
        });
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                cd = new ConnectionDetector(InviteAndEarn.this);
                isInternetPresent = cd.isConnectingToInternet();
                Tv_city.setText(item);
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Tv_city.setText(getkey("seletc_gender"));
            }
        });
        user_age_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    user_age_et.setCursorVisible(false);
                    user_age_et.clearFocus();
                    InputMethodManager mgr = (InputMethodManager) InviteAndEarn.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(user_age_et.getWindowToken(), 0);
                }
                return false;
            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userName.getText().toString().length() == 0) {
                    erroredit(userName, getkey("profile_label_alert_username"));
                } else if (!isValidEmail(userEmail.getText().toString().trim().replace(" ", ""))) {
                    erroredit(userEmail,getkey("profile_label_alert_email"));
                } else if (Tv_city.getText().toString().trim().equalsIgnoreCase("SELECT GENDER")) {
                    AlertError(getkey("alert_label_title"), getkey("profile_label_alert_validgender"));
                } else if (user_age_et.getText().toString().length() == 0) {
                    erroredit(user_age_et, getkey("profile_label_alert_validage"));
                } else if (user_age_et.getText().toString().trim().equalsIgnoreCase("0")) {
                    erroredit(user_age_et, getkey("profile_label_alert_validage"));
                } else {
                    if (isInternetPresent) {
                        String username = userName.getText().toString().trim().replace(" ", "");
                        String userEmail1 = userEmail.getText().toString().trim().replace(" ", "");
                        String gender = Tv_city.getText().toString().trim().replace(" ", "");
                        String age = user_age_et.getText().toString().trim().replace(" ", "");
                        postrequestUpdateProfile(Iconstant.get_basic_profile_url, username, userEmail1, gender, needAssistance, age);
                    } else {
                        Alert(getkey("action_error"), getkey("alert_nointernet"));
                    }
                    dialog.dismiss();
                }
            }
        });
        closeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(user_age_et);
                dialog.dismiss();
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }


    private void postrequestUpdateProfile(String Url, String name, String email, String gender, String needAssistance, String Age) {

        dialog = new Dialog(InviteAndEarn.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("user_name", name);
        jsonParams.put("email", email);
        jsonParams.put("gender", gender);
        jsonParams.put("age", Age);
        jsonParams.put("assistance", needAssistance);

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        mRequest = new ServiceRequest(InviteAndEarn.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        session.setUserCompleteProfile("1");
                        Smessage = object.getString("response");
                        dialog.dismiss();
                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction("profileupdated");
                        sendBroadcast(broadcastIntent);
                        Alert2(getkey("action_success"), Smessage, Sstatus);
                    } else {
                        dialog.dismiss();
                        Smessage = object.getString("response");
                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction("profileupdated");
                        sendBroadcast(broadcastIntent);
                        Alert2(getkey("action_error"), Smessage, Sstatus);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //--------------Close KeyBoard Method-----------
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(InviteAndEarn.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(InviteAndEarn.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    //--------------Alert Method-----------
    private void Alert1(String title, String alert, final String url) {

        final PkDialog mDialog = new PkDialog(InviteAndEarn.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        mDialog.show();

    }

    private void Alert2(String title, String alert, final String url) {

        final PkDialog mDialog = new PkDialog(InviteAndEarn.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        mDialog.show();

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(InviteAndEarn.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setDialogUpper(false);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    //--------Sending message on WhatsApp Method------
    private void whatsApp_sendMsg(String text) {
        PackageManager pm = InviteAndEarn.this.getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);  //Check if package exists or not. If not then codein catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_whatsApp_not_installed"), "https://play.google.com/store/apps/details?id=com.whatsapp&hl=en");
        }
    }

    //--------Sending message on Facebook Messenger Method------
    private void messenger_sendMsg(String text) {
        PackageManager pm = InviteAndEarn.this.getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.facebook.orca", PackageManager.GET_META_DATA);  //Check if package exists or not. If not then codein catch block will be called
            waIntent.setPackage("com.facebook.orca");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Alert(getkey("alert_label_title"), getkey("invite_earn_label_messenger_not_installed"));
        }
    }

    //--------Sending message on SMS Method------
    private void sms_sendMsg(String text) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkSmsPermission()) {
                requestPermission();
            } else {
                String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(InviteAndEarn.this); //Need to change the build to API 19
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                if (defaultSmsPackageName != null) {
                    sendIntent.setPackage(defaultSmsPackageName);
                }
                startActivity(sendIntent);
            }
        } else {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.putExtra("sms_body", text);
            sendIntent.setType("vnd.android-dir/mms-sms");
            startActivity(sendIntent);
        }
    }


    //----------Sending message on Email Method--------
    protected void sendEmail(String text) {
        String[] TO = {""};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, Ssubject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, text);
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (ActivityNotFoundException ex) {
            Alert(getkey("alert_label_title"), getkey("invite_earn_label_email_not_installed"));
        }
    }


    //----------Share Image and Text on Twitter Method--------
    protected void shareTwitter(String text, Uri image) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_STREAM, image);
        intent.setType("image/jpeg");
        intent.setPackage("com.twitter.android");

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Alert1(getkey("alert_label_title"),getkey("invite_earn_label_twitter_not_installed"), "https://play.google.com/store/apps/details?id=com.twitter.android&hl=en");
        }
    }

    //----------Share Link on Method--------
    private void shareFacebookLink(String link) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, link);
        Intent pacakage2 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana");
        } else {
            intent.setPackage("");
        }

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_facebook_not_installed"), "https://play.google.com/store/apps/details?id=com.facebook.katana&hl=en");
        }
    }


    //----------Share Image and Text on Facebook Method--------
    protected void shareFacebook(String text, Bitmap image) {
        Bitmap b = image;
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContentResolver(), b, "Title", null);
        Uri imageUri =  Uri.parse(path);
        share.setPackage("com.facebook.orca");
        share.putExtra(Intent.EXTRA_TEXT, text);
        try {
            startActivity(Intent.createChooser(share, getkey("sgharetto")));
        }
        catch (android.content.ActivityNotFoundException ex) {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_messenger_not_installed"), "https://play.google.com/store/apps/details?id=com.facebook.orca");
        }

    }

    protected void shareInstagrame(String text) {
        boolean isAppInstalled = appInstalledOrNot("com.instagram.android");
        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            // intent.putExtra(Intent.EXTRA_STREAM, "");
//        intent.setType("image/jpeg");
            intent.setPackage("com.instagram.android");

            // Add the URI to the Intent.
//        intent.putExtra(Intent.EXTRA_STREAM, image);

            try {
                //startActivity(intent);
                startActivity(Intent.createChooser(intent, "Share to"));
            } catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"),getkey("invite_earn_label_instagram_not_installed"), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_instagram_not_installed"), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
        }


    }

    protected void shareInstagramelink(String text, Bitmap image) {


        boolean isAppInstalled = appInstalledOrNot("com.instagram.android");

        if (isAppInstalled) {



            Bitmap b = image;
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), b, "Title", null);
            Uri imageUri =  Uri.parse(path);
            share.setPackage("com.instagram.android");
            share.putExtra(Intent.EXTRA_TEXT, text);
            try {
                startActivity(Intent.createChooser(share,getkey("sgharetto")));
            }
            catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"),getkey("invite_earn_label_messenger_not_installed"), "https://play.google.com/store/apps/details?id=com.instagram.android");
            }



        } else {
            Alert1(getkey("alert_label_title"),getkey("invite_earn_label_instagram_not_installed"), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
        }
    }


    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }


    //----------Share Image and Text on Twitter Method--------
//    protected void shareInstagrame(String text, Uri image) {
//
//
//        Intent share = new Intent(Intent.ACTION_SEND);
//        share.setType("text/plain");
//        share.putExtra(Intent.EXTRA_TEXT, text);
//        startActivity(Intent.createChooser(share, "Title of the dialog the system will open"));
//
////        String type = "image/*";
////        Intent intent = getPackageManager().getLaunchIntentForPackage(
////                "com.instagram.android");
////        if (intent != null) {
////
////            try {
////                Intent shareIntent = new Intent(Intent.ACTION_SEND);
////                shareIntent.setPackage("com.instagram.android");
////                shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//////                shareIntent.putExtra(Intent.EXTRA_STREAM, image);
////                shareIntent.setType(type);
////                shareIntent.putExtra(Intent.EXTRA_TEXT, text);
////                // Broadcast the Intent.
////
////                startActivity(shareIntent);
////
////                // startActivity(Intent.createChooser(shareIntent,
////                // "Share to"));
////            } catch (Exception e) {
////                // TODO Auto-generated catch block
////                e.printStackTrace();
////            }
////
////        } else {
////            // bring user to the market to download the app.
////            // or let them choose an app?
////            intent = new Intent(Intent.ACTION_VIEW);
////            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////            intent.setData(Uri.parse("market://details?id="
////                    + "com.instagram.android"));
////            startActivity(intent);
////        }
////
////        Intent instagramIntent = new Intent(Intent.ACTION_SEND);
////        instagramIntent.setType("image/*");
////        instagramIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////        instagramIntent.putExtra(Intent.EXTRA_TEXT, text);
////        instagramIntent.putExtra(Intent.EXTRA_STREAM, image);
////        instagramIntent.setPackage("com.instagram.android");
////        PackageManager packManager = getPackageManager();
////
////        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(instagramIntent, PackageManager.MATCH_DEFAULT_ONLY);
////
////        boolean resolved = false;
////        try {
////            ApplicationInfo info = getPackageManager().getApplicationInfo("com.instagram.android", 0);
////            resolved = true;
////        } catch (PackageManager.NameNotFoundException e) {
////            resolved = false;
////        }
////
////        if (resolved) {
////            startActivity(Intent.createChooser(instagramIntent, "Share to"));
////        } else {
////        }
//    }

    //----------Share Image and Text on googleplus Method--------
    protected void shareGPlus(String text, Uri image) {
        Intent shareIntent = ShareCompat.IntentBuilder.from(this)
                .setText(text)
                .setType("image/jpeg")
                .setStream(image)
                .getIntent()
                .setPackage("com.google.android.apps.plus");

        try {
            startActivityForResult(shareIntent, GOOGLE_PLUS_SHARE_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_gplus_not_installed"), "https://play.google.com/store/apps/details?id=com.google.android.apps.plus&hl=en");
//            Toast.makeText(YourActivity.this, "You haven't installed google+ on your device", Toast.LENGTH_SHORT).show();
        }


    }

    //----------Share Image and Text on skype Method--------
    protected void shareskype(String text, Uri image) {
        // Create the Intent from our Skype URI.
        Intent skypeIntent = new Intent(Intent.ACTION_SEND);
        skypeIntent.putExtra(Intent.EXTRA_TEXT, text);
        skypeIntent.setType("text/plain");
        skypeIntent.setPackage("com.skype.raider");
        PackageManager packManager = getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(skypeIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        try {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.skype.raider", 0);
            resolved = true;
        } catch (PackageManager.NameNotFoundException e) {
            resolved = false;
        }

        if (resolved) {
            startActivity(Intent.createChooser(skypeIntent, "Share to"));
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_skype_not_installed"), "https://play.google.com/store/apps/details?id=com.skype.raider&hl=en");
        }

    }


    //-----------------------Display Invite Amount Post Request-----------------
    private void displayInvite_Request(String Url) {
        dialog = new Dialog(InviteAndEarn.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));

        System.out.println("-------------displayInvite_Request Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);

        mRequest = new ServiceRequest(InviteAndEarn.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------displayInvite_Request Response----------------" + response);

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (object.length() > 0) {
                        JSONObject response_Object = object.getJSONObject("response");
                        if (response_Object.length() > 0) {
                            JSONObject detail_object = response_Object.getJSONObject("details");
                            if (detail_object.length() > 0) {
                                image = detail_object.getString("image");
                                friend_earn_amount = detail_object.getString("friends_earn_amount");
                                you_earn_amount = detail_object.getString("your_earn_amount");
                                friends_rides = detail_object.getString("your_earn");
                                referral_code = detail_object.getString("referral_code");
                                ScurrencyCode = detail_object.getString("currency");
                                Ssubject = detail_object.getString("subject");
                                Smessage = detail_object.getString("message");
                                sShareLink = detail_object.getString("url");

                                isdataPresent = true;
                            } else {
                                isdataPresent = false;
                            }
                        } else {
                            isdataPresent = false;
                        }
                    } else {
                        isdataPresent = false;
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isdataPresent) {
                    Tv_referral_code.setText(referral_code);
//                    Tv_barcode_referrral.setText(referral_code);
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //------------------------------------------------------


    private void alertDialog() {

        final MaterialDialog dialog = new MaterialDialog(InviteAndEarn.this);
        View view = LayoutInflater.from(InviteAndEarn.this).inflate(R.layout.invite_earn_alert_page, null);

        CustomTextView Tv_close = (CustomTextView) view.findViewById(R.id.invite_earn_alert_close_textview);
        CustomTextView Tv_title = (CustomTextView) view.findViewById(R.id.invite_earn_alert_title);
        CustomTextView Tv_content = (CustomTextView) view.findViewById(R.id.invite_earn_alert_content);

        Tv_title.setText(Spage_title);
        Tv_content.setText(Html.fromHtml(Spage_description));

        Tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.setBackground(getResources().getDrawable(R.drawable.transparant_bg));
        dialog.setView(view).show();


    }


    private void PostRequest(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(InviteAndEarn.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------DynamicPage jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(InviteAndEarn.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DynamicPage reponse-------------------" + response);

                String Sstatus = "", Smessage = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");
                        if (jsonObject.length() > 0) {
                            JSONObject pages_details_jsonObject = jsonObject.getJSONObject("pages_details");
                            if (pages_details_jsonObject.length() > 0) {
                                Spage_title = pages_details_jsonObject.getString("page_title");
                                Spage_description = pages_details_jsonObject.getString("page_description");

                            }

                        }

                        dialogDismiss();

                    } else {
                        Smessage = object.getString("response");
                        dialogDismiss();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {
                    alertDialog();

                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    private boolean checkSmsPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    String defaultSmsPackageName = null; //Need to change the build to API 19
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this);
                    }

                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, Smessage);
                    if (defaultSmsPackageName != null) {
                        sendIntent.setPackage(defaultSmsPackageName);
                    }
                    startActivity(sendIntent);
                } else {
                }
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(refreshReceiver);
        super.onDestroy();

    }


//    -------------------------Agreement Dialog-----------------------------

    private void LearnMoreDialog() {
        check_status = false;
        final Dialog dialog = new Dialog(InviteAndEarn.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.invite_and_earn_popip_new);
        final Button ok_button = (Button) dialog.findViewById(R.id.btn_ok);
        ok_button.setText(getkey("activate"));
        final CheckBox chck_agree = (CheckBox) dialog.findViewById(R.id.agree_checkBox);
        final Button cancelbtn = (Button) dialog.findViewById(R.id.cancel);
        cancelbtn.setText(getkey("cancel_lable"));
        final TextView custom_text1 = (TextView) dialog.findViewById(R.id.custom_text1);
        custom_text1.setText(getkey("ride_earn"));

        final TextView custom_text3 = (TextView) dialog.findViewById(R.id.custom_text3);
        custom_text3.setText(getkey("cloud_custom_text5"));

        final TextView ag = (TextView) dialog.findViewById(R.id.ag);
        ag.setText(getkey("cloud_custom_text6"));

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        chck_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((CheckBox) v).isChecked()) {

                    check_status = true;

                } else {

                    check_status = false;
                }


            }
        });


        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_status) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", UserID);
                    jsonParams.put("confirmed", "yes");
                    ActivateReferandEarn(Iconstant.User_Activiate_Referal, jsonParams);
                    dialog.dismiss();
                } else {
                    Alert(getkey("action_error"), getkey("kindly_terms_service"));
                }
            }
        });

        dialog.show();


    }

    private void ActivateReferandEarn(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(InviteAndEarn.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------ActivateReferandEarn jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(InviteAndEarn.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------ActivateReferandEarn reponse-------------------" + response);

                String Sstatus = "", Smessage = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");
                    dialogDismiss();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();
                if (Sstatus.equalsIgnoreCase("1")) {
                    session.setReferelStatus("0");
                    Alert("", Smessage);
                } else {
                    Alert("", Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}