package com.blackmaria;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.widgets.PkDialog;

import java.util.ArrayList;

/**
 * Created by user144 on 6/27/2017.
 */

public class SearchByNearHome extends ActivityHockeyApp implements SeekBar.OnSeekBarChangeListener {
    private TextView seekRange, location_search_editText;
     LanguageDb mhelper;
    private Button nearyby_btn;
    private LinearLayout rlAtm, rlCafe, rlFood, rlShopping, rlHotel, rlSpaCenter;
    private String searchType = "", searchRange = "";
    private ImageView back_button;
    RelativeLayout relativeLayout2,nearmebulay;
    private RefreshReceiver refreshReceiver;
    private CardView bookHotelLayout, Rl_flightBook;
    private String Savailable_count = "", Pickup_location = "", Pickup_lat = "", Pickup_lon = "";
    private ArrayList<String> mylist = new ArrayList<>();


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(BookingPage1.this, BookingPage1.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(SearchByNearHome.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(SearchByNearHome.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.findplace_home_constrain);
        mhelper  = new LanguageDb(this);

        Initialize();
        location_search_editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent findIntent = new Intent(getApplicationContext(), SearchNearBy.class);
                findIntent.putExtra("Savailable_count", Savailable_count);
                findIntent.putExtra("Pickup_location", Pickup_location);
                findIntent.putExtra("pickuplat", Pickup_lat);
                findIntent.putExtra("pickuplon", Pickup_lon);
                findIntent.putExtra("placeTypeflag", 10);
                findIntent.putExtra("searchRange", searchRange);
                findIntent.putStringArrayListExtra("list", mylist);
                startActivity(findIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        nearmebulay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              Intent findIntent = new Intent(getApplicationContext(), SearchNearBy.class);
               findIntent.putExtra("Savailable_count", Savailable_count);
                findIntent.putExtra("Pickup_location", Pickup_location);
               findIntent.putExtra("pickuplat", Pickup_lat);
                findIntent.putExtra("pickuplon", Pickup_lon);
                findIntent.putExtra("placeTypeflag", 0);
                findIntent.putExtra("searchRange", searchRange);
               findIntent.putStringArrayListExtra("list", mylist);
               startActivity(findIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        nearyby_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

//                Intent findIntent = new Intent(getApplicationContext(), SearchNearBy.class);
//                findIntent.putExtra("Savailable_count", Savailable_count);
//                findIntent.putExtra("Pickup_location", Pickup_location);
//                findIntent.putExtra("pickuplat", Pickup_lat);
//                findIntent.putExtra("pickuplon", Pickup_lon);
//                findIntent.putExtra("placeTypeflag", 0);
//                findIntent.putExtra("searchRange", searchRange);
//                findIntent.putStringArrayListExtra("list", mylist);
//                startActivity(findIntent);
//                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        relativeLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        rlAtm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent findIntent = new Intent(getApplicationContext(), SearchNearBy.class);
                findIntent.putExtra("Savailable_count", Savailable_count);
                findIntent.putExtra("Pickup_location", Pickup_location);
                findIntent.putExtra("pickuplat", Pickup_lat);
                findIntent.putExtra("pickuplon", Pickup_lon);
                findIntent.putExtra("placeType", "atm");
                findIntent.putExtra("placeTypeflag", 1);
                findIntent.putExtra("searchRange", searchRange);
                findIntent.putStringArrayListExtra("list", mylist);
                startActivity(findIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        rlCafe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent findIntent = new Intent(getApplicationContext(), SearchNearBy.class);
                findIntent.putExtra("Savailable_count", Savailable_count);
                findIntent.putExtra("Pickup_location", Pickup_location);
                findIntent.putExtra("pickuplat", Pickup_lat);
                findIntent.putExtra("pickuplon", Pickup_lon);
                findIntent.putExtra("placeType", "cafe");
                findIntent.putExtra("placeTypeflag", 1);
                findIntent.putExtra("searchRange", searchRange);
                findIntent.putStringArrayListExtra("list", mylist);
                startActivity(findIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        rlFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent findIntent = new Intent(getApplicationContext(), SearchNearBy.class);
                findIntent.putExtra("Savailable_count", Savailable_count);
                findIntent.putExtra("Pickup_location", Pickup_location);
                findIntent.putExtra("pickuplat", Pickup_lat);
                findIntent.putExtra("pickuplon", Pickup_lon);
                findIntent.putExtra("placeType", "food");
                findIntent.putStringArrayListExtra("list", mylist);
                findIntent.putExtra("placeTypeflag", 1);
                findIntent.putExtra("searchRange", searchRange);
                startActivity(findIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        rlShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent findIntent = new Intent(getApplicationContext(), SearchNearBy.class);
                findIntent.putExtra("Savailable_count", Savailable_count);
                findIntent.putExtra("Pickup_location", Pickup_location);
                findIntent.putExtra("pickuplat", Pickup_lat);
                findIntent.putExtra("pickuplon", Pickup_lon);
                findIntent.putExtra("placeType", "shopping");
                findIntent.putExtra("placeTypeflag", 1);
                findIntent.putStringArrayListExtra("list", mylist);
                findIntent.putExtra("searchRange", searchRange);
                startActivity(findIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        rlHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent findIntent = new Intent(getApplicationContext(), SearchNearBy.class);
                findIntent.putExtra("Savailable_count", Savailable_count);
                findIntent.putExtra("Pickup_location", Pickup_location);
                findIntent.putExtra("pickuplat", Pickup_lat);
                findIntent.putExtra("pickuplon", Pickup_lon);
                findIntent.putExtra("placeType", "govt");
                findIntent.putExtra("placeTypeflag", 1);
                findIntent.putExtra("searchRange", searchRange);
                findIntent.putStringArrayListExtra("list", mylist);
                startActivity(findIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        rlSpaCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent findIntent = new Intent(getApplicationContext(), SearchNearBy.class);
                findIntent.putExtra("Savailable_count", Savailable_count);
                findIntent.putExtra("Pickup_location", Pickup_location);
                findIntent.putExtra("pickuplat", Pickup_lat);
                findIntent.putExtra("pickuplon", Pickup_lon);
                findIntent.putExtra("placeType", "spa");
                findIntent.putExtra("placeTypeflag", 1);
                findIntent.putStringArrayListExtra("list", mylist);
                findIntent.putExtra("searchRange", searchRange);
                startActivity(findIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
//        location_search_editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
//                    CloseKeyboard(location_search_editText);
//                }
//                return false;
//            }
//        });
//        location_search_editText.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                InputMethodManager keyboard = (InputMethodManager)
//                        getSystemService(Context.INPUT_METHOD_SERVICE);
//                keyboard.showSoftInput(location_search_editText, 0);
//            }
//        }, 200);


        bookHotelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent findIntent = new Intent(getApplicationContext(), BookFlightAndHotelWebActivity.class);
                findIntent.putExtra("flag", 1);
                startActivity(findIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        Rl_flightBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent findIntent = new Intent(getApplicationContext(), BookFlightAndHotelWebActivity.class);
                findIntent.putExtra("flag", 2);
                startActivity(findIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }


    private void Initialize() {
        seekRange = findViewById(R.id.radius_range);
        SeekBar seekBar1 = findViewById(R.id.seekBarDistance);
        location_search_editText = findViewById(R.id.location_search_editText);
        location_search_editText.setHint(getkey("estimate_price_label_search1"));

        TextView foodtext = findViewById(R.id.foodtext);
        foodtext.setText(getkey("food_txt"));

        TextView sandbosxs = findViewById(R.id.sandbosxs);
        sandbosxs.setText(getkey("shopping"));

        TextView goc = findViewById(R.id.goc);
        goc.setText(getkey("govt"));

        nearyby_btn = findViewById(R.id.nearyby_btn);
        nearyby_btn.setText(getkey("back_lable"));
        nearmebulay= findViewById(R.id.nearmebulay);
        rlAtm = findViewById(R.id.r_atm);
        rlCafe = findViewById(R.id.r_cafe);
        rlFood = findViewById(R.id.r_food);
        rlShopping = findViewById(R.id.r_shopping);
        rlHotel = findViewById(R.id.r_hotel);
        rlSpaCenter = findViewById(R.id.r_spacanter);
        back_button = findViewById(R.id.back_button);
        relativeLayout2= findViewById(R.id.relativeLayout2);
        bookHotelLayout = findViewById(R.id.bookhotel_layout);
        Rl_flightBook = findViewById(R.id.getflight_rl);
        Savailable_count = getIntent().getStringExtra("Savailable_count");
        Pickup_location = getIntent().getStringExtra("pickuplocation");
        Pickup_lat = getIntent().getStringExtra("pickuplat");
        Pickup_lon = getIntent().getStringExtra("pickuplon");

        if(getIntent().hasExtra("list")){
            mylist.clear();
            mylist = getIntent().getStringArrayListExtra("list");
        }

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        registerReceiver(refreshReceiver, intentFilter);

        seekBar1.setMax(10000);
        seekBar1.setProgress(5000);
        seekBar1.setOnSeekBarChangeListener(this);
        seekRange.setText((seekBar1.getProgress() / 1000) + getkey("km"));
        searchRange = seekBar1.getProgress() + "";
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        seekRange.setText((progress / 1000) + getkey("km"));
        searchRange = String.valueOf((progress / 1000) * 1000.0);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(SearchByNearHome.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
