package com.blackmaria;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.blackmaria.adapter.PersonListAdapter;
import com.blackmaria.pojo.PersonPojo;

import java.util.ArrayList;

/**
 * Created by user14 on 3/9/2017.
 */

public class PersonList extends Activity {


    private ListView Lv_ride;
    private PersonListAdapter personListAdapter;
    private ArrayList<PersonPojo> person_itemList;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_list);
        initialize();
    }

    private void initialize() {
        person_itemList = new ArrayList<PersonPojo>();
        Lv_ride = (ListView) findViewById(R.id.person_list_listview);

        person_itemList.clear();
        for (int i = 0; i < 6; i++) {
            PersonPojo RideListPojo = new PersonPojo();
            RideListPojo.setCount(String.valueOf(i));

            person_itemList.add(RideListPojo);
        }

        personListAdapter = new PersonListAdapter(PersonList.this, person_itemList);
        Lv_ride.setAdapter(personListAdapter);
        personListAdapter.notifyDataSetChanged();
    }

}
