package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.newdesigns.Pin_activitynew;
import com.blackmaria.newdesigns.saveplus.Checkpattern;
import com.blackmaria.newdesigns.saveplus.Confirmpatern_activity;
import com.blackmaria.newdesigns.saveplus.Scanqrcode;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.volley.ServiceRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class welcomeplusdialog extends BottomSheetDialogFragment {

    private SessionManager session;


    String saveplusida,codepath;

    public static welcomeplusdialog newInstance() {
        return new welcomeplusdialog();
    }

LanguageDb mhelper;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setCancelable(true);
        getDialog().setTitle("Title");

        View view = inflater.inflate(R.layout.welcomedialog, null, false);
        mhelper = new LanguageDb(getActivity());
        session=new SessionManager(getActivity());




        final TextView username = view.findViewById(R.id.username);
        username.setText(getkey("hello")+" "+ session.getuserwelname() );

        final TextView welcome_amounts = view.findViewById(R.id.welcome_amounts);
        final TextView discountcoupons = view.findViewById(R.id.discountcoupons);
        final TextView kmprint = view.findViewById(R.id.kmprint);


        final TextView thanksfo = view.findViewById(R.id.thanksfo);
        final TextView ewall = view.findViewById(R.id.ewall);
        final TextView copp = view.findViewById(R.id.copp);
        final TextView miless = view.findViewById(R.id.miless);

        thanksfo.setText(getkey("thank_you_for_joinings"));
        ewall.setText(getkey("ewallet"));
        copp.setText(getkey("coupons"));
        miless.setText(getkey("milesx"));




        welcome_amounts.setText(session.getwwelcomewalletamount() );
        discountcoupons.setText( session.getcouponcountwel());
        kmprint.setText(session.gettotaldiwel()  + session.getdistunitwel().toUpperCase());


        ImageView closedialog = (ImageView)view.findViewById(R.id.closedialog);
        closedialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });



        return view;
    }


    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}