package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;
import com.xendit.AuthenticationCallback;
import com.xendit.Models.Authentication;
import com.xendit.Models.Card;
import com.xendit.Models.Token;
import com.xendit.Models.XenditError;
import com.xendit.TokenCallback;
import com.xendit.Xendit;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by user144 on 9/29/2017.
 */

public class XenditRidePaymentActivity extends ActivityHockeyApp {
    EditText Edt1, Edt2, Edt3, Edt4, CardHolderName, EdtCvv;
    Spinner spinner_month, spinner_year;
    private RelativeLayout rlPayNow, cardCancelLayout;
    private String expYear = "", expMonth = "";
    private int expYearNum = 0, expMnthNUm = 0;
    private String cardNumber = "", insertAmount = "";
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private String currency = "";
    private SessionManager session;
    private String authenticationId = "";
    private String UserID = "";
    //Arockiasamy anna xendit account
    public static String PUBLISHABLE_KEY = "";
    public static String CURRENCYCONVERSIONKEY_KEY = "";
    ServiceRequest mRequest;

    private String RideID = "", xendit_key = "", currencyConvesrion = "", ride_amount = "";
    SmoothProgressBar loadingProgressbar;
    Xendit xendit = null;
    PkDialog mDialog = null;
    Double amount = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xenditridepaymentlayout);
        Initialization();
        spinner_month_and_year_process();


        Edt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (Edt1.getText().toString().length() == 4) {

                    Edt2.requestFocus();
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        Edt2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Edt2.getText().toString().length() == 4) {
                    Edt3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        Edt3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Edt3.getText().toString().length() == 4) {
                    Edt4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        Edt4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (Edt3.getText().toString().length() == 4) {

                    Edt4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        rlPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int thisYear = Calendar.getInstance().get(Calendar.YEAR);
                int thismnth = Calendar.getInstance().get(Calendar.MONTH);
                cardNumber = Edt1.getText().toString() + Edt2.getText().toString() + Edt3.getText().toString() + Edt4.getText().toString();
                if (cardNumber.length() == 0) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no));

                    //erroredit(Edt1, getResources().getString(R.string.valid_card_no));
                } else if (cardNumber.length() < 16) {
                    if (Edt1.getText().toString().length() < 4) {
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no_enter));

//                        erroredit(Edt1, getResources().getString(R.string.valid_card_no_enter));
                    } else if (Edt2.getText().toString().length() < 4) {
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no_enter));

//                        erroredit(Edt2, getResources().getString(R.string.valid_card_no_enter));
                    } else if (Edt3.getText().toString().length() < 4) {
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no_enter));

                        erroredit(Edt3, getResources().getString(R.string.valid_card_no_enter));
                    } else if (Edt4.getText().toString().length() < 4) {
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no_enter));

                        // erroredit(Edt4, getResources().getString(R.string.valid_card_no_enter));
                    }
                } else if (CardHolderName.getText().toString().length() <= 0) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_holder_name));

                    // erroredit(EdtCvv, getResources().getString(R.string.valid_card_holder_name));
                } else if (EdtCvv.getText().toString().length() < 3) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_cvv_code));
                    //erroredit(EdtCvv, getResources().getString(R.string.valid_cvv_code));
                } else if (expMnthNUm < thismnth && expYearNum == thisYear) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_validexp_mth));
                } else if (expYearNum < thisYear) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_exp_yrr));

                } else if (expMonth.length() < 0 || expYear.length() < 0) {

                } else {
                    loadingProgressbar.setVisibility(View.VISIBLE);
                    CreateXenditToken();
                }
            }
        });
    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(XenditRidePaymentActivity.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    private void Initialization() {

        cd = new ConnectionDetector(XenditRidePaymentActivity.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(XenditRidePaymentActivity.this);
        //PUBLISHABLE_KEY = session.getXenditPublicKey();
        // PUBLISHABLE_KEY = session.getXenditPublicKey();
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        spinner_month = (Spinner) findViewById(R.id.spinner2);
        spinner_year = (Spinner) findViewById(R.id.spinner3);
        Edt1 = (EditText) findViewById(R.id.card_number_edt1);
        Edt2 = (EditText) findViewById(R.id.card_number_edt2);
        Edt3 = (EditText) findViewById(R.id.card_number_edt3);
        Edt4 = (EditText) findViewById(R.id.card_number_edt4);
        rlPayNow = (RelativeLayout) findViewById(R.id.paynow_rl);
        CardHolderName = (EditText) findViewById(R.id.card_holder_name);
        EdtCvv = (EditText) findViewById(R.id.cvv_edt);
        loadingProgressbar = (SmoothProgressBar) findViewById(R.id.timerpage_loading_progressbar);
// -----code to refresh drawer using broadcast receiver-----
//        currency = session.getCurrency();
        Intent in = getIntent();
        if (in.hasExtra("RideID")) {
            RideID = in.getStringExtra("RideID");
            xendit_key = in.getStringExtra("xendit_key");
            currencyConvesrion = in.getStringExtra("conversionkey");
            ride_amount = in.getStringExtra("ride_amount");
            currency = in.getStringExtra("currency");
            System.out.println("=========currencyConvesrion==========" + currencyConvesrion);
            PUBLISHABLE_KEY = xendit_key;
            CURRENCYCONVERSIONKEY_KEY = currencyConvesrion;//session.getXenditPublicKey();
            insertAmount = ride_amount;
        }


    }

    private void spinner_month_and_year_process() {


        String[] month = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        ArrayAdapter<CharSequence> monthAdapter = new ArrayAdapter<CharSequence>(XenditRidePaymentActivity.this, R.layout.spinner_text, month);
        monthAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        spinner_month.setAdapter(monthAdapter);

        String thisMonth = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);


        if (thisMonth.length() == 1) {
            thisMonth = "0" + thisMonth;
        }


        for (int j = 0; j <= month.length; j++) {

            if (month[j].equalsIgnoreCase(thisMonth)) {
                spinner_month.setSelection(j);
                break;

            }

        }
        spinner_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                expMonth = parent.getSelectedItem().toString();
                expMnthNUm = Integer.parseInt(expMonth);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);


        for (int i = thisYear; i <= thisYear + 40; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(XenditRidePaymentActivity.this, R.layout.spinner_text, years);
        yearAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        spinner_year.setAdapter(yearAdapter);
        spinner_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                expYear = parent.getSelectedItem().toString();
                expYearNum = Integer.parseInt(expYear);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void CreateXenditToken() {
        xendit = new Xendit(getApplicationContext(), PUBLISHABLE_KEY);
        final boolean isMultipleUse = true;
        boolean shouldAuthenticate = true;

        Card card = new Card(cardNumber,
                expMonth,
                expYear,
                EdtCvv.getText().toString());

        TokenCallback callback = new TokenCallback() {
            @Override
            public void onSuccess(Token token) {

                System.out.println("=======murugan  payment TokenId=========" + token.getId());
                if (token.getStatus().equalsIgnoreCase("VERIFIED") /*&& isMultipleUse == false*/) {
                    createAuthenticationId(token.getId());
                    //   authenticationId = Xendit.AUTHENTICATOIN_KEY;
                    //   System.out.println("=======murugan  payment AUTHENTICATOIN_KEY=========" + authenticationId);
                }
            }
            @Override
            public void onError(XenditError xenditError) {
                loadingProgressbar.setVisibility(View.GONE);
                // Toast.makeText(XenditRidePaymentActivity.this, xenditError.getErrorCode(), Toast.LENGTH_SHORT).show();
            }
        };

        if (isMultipleUse) {
            xendit.createMultipleUseToken(card, callback);
        } else {
            Double amount = 0.0;
            amount = Double.parseDouble(insertAmount) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
            amount = (Double) Math.ceil(amount);
            /*int amount ;
            amount = Integer.parseInt(insertAmount)*Integer.parseInt(CURRENCYCONVERSIONKEY_KEY);*/
            //  amount=amount*1000;
            // Double amount = Double.parseDouble(insertAmount) * 1000;
            System.out.println("===================amount=================" + amount);
            xendit.createSingleUseToken(card, amount, shouldAuthenticate, callback);
        }

    }

    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(XenditRidePaymentActivity.this, R.anim.shake);
        editname.startAnimation(shake);
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    @SuppressLint("WrongConstant")
    private void xenditBankMethod(String Url, HashMap<String, String> jsonParams) {

        final Dialog dialog = new Dialog(XenditRidePaymentActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("--------------xenditBankMethod Url-------------------" + Url);
        System.out.println("--------------xenditBankMethod jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(XenditRidePaymentActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------xenditBankMethod Response-------------------" + response);
                String status = "", sResponse = "", wallet_amount = "", payment_code = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("response");

                        if (status.equalsIgnoreCase("1")) {
                            AlertSuccess(getResources().getString(R.string.action_success), sResponse);
                        } else {
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void AlertSuccess(String title, String alert) {
        mDialog = new PkDialog(XenditRidePaymentActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setCancelOnTouchOutside(false);
        mDialog.setCancelble(false);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent1 = new Intent(XenditRidePaymentActivity.this, FareBreakUp.class);
                intent1.putExtra("RideID", RideID);
                intent1.putExtra("ratingflag", "1");
                startActivity(intent1);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        mDialog.show();
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(XenditRidePaymentActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }

    private void createAuthenticationId(final String tokenId) {
        try {

            amount = Double.parseDouble(insertAmount) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
            amount = (Double) Math.ceil(amount);
        /*  final  int amount ;
            amount = Integer.parseInt(insertAmount)*Integer.parseInt(CURRENCYCONVERSIONKEY_KEY);*/
            //  amount=amount*1000;
            // Double amount = Double.parseDouble(insertAmount) * 1000;
            System.out.println("===================amount=================" + amount);
            xendit.createAuthentication(tokenId, amount, new AuthenticationCallback() {
                @Override
                public void onSuccess(Authentication authentication) {
                    loadingProgressbar.setVisibility(View.GONE);
                    if (isInternetPresent) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("amount", amount + "");
                        jsonParams.put("user_id", UserID);
                        jsonParams.put("ride_id", RideID);
                        jsonParams.put("token_id", tokenId);
                        jsonParams.put("authentication_id", authentication.getId());
                        System.out.println("===============xendit jphareas===================" + jsonParams);
                        xenditBankMethod(Iconstant.XenditCard_Ride_Payment, jsonParams);
                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }
                }

                @Override
                public void onError(XenditError xenditError) {
                    loadingProgressbar.setVisibility(View.GONE);
                    Alert(getResources().getString(R.string.alert_label_title), xenditError.getErrorCode() + xenditError.getErrorMessage());

                    System.out.println("================xenditError============" + xenditError.getErrorCode() + xenditError.getErrorMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

}
