package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ParseException;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.RideListAdapter;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.pojo.RideListPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class RideList extends FragmentActivityHockeyApp {

    private CaldroidFragment dialogCaldroidFragment;
    LanguageDb mhelper;
    private boolean isInternetPresent = false;
    private SessionManager session;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private boolean searchstatus = false;
    public static ListView Lv_ride;
    private static RideListAdapter rideListAdapter;
    private ArrayList<RideListPojo> RideList_itemList;
    //   private RelativeLayout R1_ongoing_ride,R1_search_ride;
    private TextView Tv_no_rides, currentdate_day, tripcount;
    private ImageView Tv_next, Tv_previous, homeback;
    private ImageView Iv_back, rightarraow, leftarraow;
    private TextView username_tv;
    private TextView currentDateDay;
    String ScurrentYear = "", ScurrentMonth;
    int currentYear, currentMonth, currentMonthStatic, ScurrentYearStatic, getDate;
    private String UserID = "", FilterDate = "", perPage = "5", filterType = "year", userName = "";
    private Dialog dialog;
    private String SongoningRideDate = "", Scategory = "", Svechile_no = "", Sridestatus = "", Seta_text = "", Scompleted_rides = "",
            Stotal_rides = "", Scancelled_rides = "", Stotal_distance = "", SonGoingRideId = "";
    private int currentPage = 1;
    //private EditText Et_search_date;
    private String nextPage;
    private boolean ongoing_arrstatus = false;
    private RelativeLayout R1_bottom;
    public static Activity rideList_class;
    private String type = "", drivername = "";
    private Spinner spinnerCustom;
    private TextView Tv_city;
    static boolean active = false;
    int monthvalue = 0;

    private RefreshReceiver refreshReceiver;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(RideList.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(RideList.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(RideList.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }


        }


    }


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_listpage_new_constrain);
        mhelper = new LanguageDb(this);
        rideList_class = RideList.this;
        monthvalue = Calendar.getInstance().get(Calendar.MONTH) + 1;
        initialize();
        RideListFilter();

        Tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage + 1;
                postData();
                scrollMyListViewToBottom();
            }
        });

        Tv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage - 1;
                postData();
                scrollMyListViewToBottom();
            }
        });

       /* Et_search_date.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                datePicker(savedInstanceState);
            }
        });*/
//        homeback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(RideList.this, Navigation_new.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);
//            }
//
//        });
        Iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("Complaint")) {
                    onBackPressed();
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else if (type.equals("Home")) {
                    Intent intent = new Intent(RideList.this, Navigation_new.class);
                    // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Intent intent = new Intent(RideList.this, RideListHomePage.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }

        });

        Lv_ride.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (type.equals("Complaint")) {
                    if (!RideList_itemList.get(position).getRide_status().equalsIgnoreCase("Booked")) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("ride_id", RideList_itemList.get(position).getRide_id());
                        setResult(RESULT_OK, returnIntent);
                        //onBackPressed();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else {
                        Alert(getkey("action_error"), getkey("choobk"));
                    }


                } else if (type.equals("Home")) {

                    Intent intent = new Intent(RideList.this, RideDetailPage.class);
                    intent.putExtra("ride_id", RideList_itemList.get(position).getRide_id());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {

                    Intent intent = new Intent(RideList.this, RideDetailPage.class);
                    intent.putExtra("ride_id", RideList_itemList.get(position).getRide_id());
                    intent.putExtra("class", "RideList");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }

            }

        });


    }

    private void initialize() {
        session = new SessionManager(RideList.this);
        cd = new ConnectionDetector(RideList.this);
        isInternetPresent = cd.isConnectingToInternet();
        RideList_itemList = new ArrayList<RideListPojo>();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        userName = info.get(SessionManager.KEY_USERNAME);
//        homeback = (ImageView) findViewById(R.id.wallet_money_transaction_header_back_layout);
        //R1_ongoing_ride = (RelativeLayout) findViewById(R.id.ride_list_current_ride_main_textview);
       /* Tv_ongoing_ride_lable = (TextView) findViewById(R.id.ride_list_current_ride_textview);
        Tv_ongoing_ride_date = (TextView) findViewById(R.id.ride_list_current_ride_date_textview);
        Tv_ongoing_ride_cartype = (TextView) findViewById(R.id.ride_list_current_ride_car_type_textview);
        Tv_ongoing_ride_carno = (TextView) findViewById(R.id.ride_list_current_ride_carno_textview);
        Tv_ongoing_ride_status = (TextView) findViewById(R.id.ride_list_current_ride_ride_status_textview);*/
        //  R1_search_ride = (RelativeLayout) findViewById(R.id.ride_list_search_ride_layout);
        // Et_search_date = (EditText) findViewById(R.id.ride_list_date_edittext);
        // Tv_go = (TextView) findViewById(R.id.ride_list_go_textview);
        // R1_search_ride.setVisibility(View.GONE);
//        R1_bottom = (RelativeLayout) findViewById(R.id.ride_list_bottom_main_layout1);
        currentdate_day = (TextView) findViewById(R.id.currentdate_day);
        tripcount = (TextView) findViewById(R.id.tripcount);
        Tv_no_rides = (TextView) findViewById(R.id.ride_list_no_rides_textview1);
        Tv_no_rides.setText(getkey("no_rides_lable"));
        Lv_ride = (ListView) findViewById(R.id.ride_list_listview);
        Tv_previous = (ImageView) findViewById(R.id.ride_list_previous_textview);
        Tv_next = (ImageView) findViewById(R.id.ride_list_next_textview);
        rightarraow = (ImageView) findViewById(R.id.rightarraow);
        leftarraow = (ImageView) findViewById(R.id.leftarraow);


        TextView pasttripsinforation = (TextView) findViewById(R.id.pasttripsinforation);
        pasttripsinforation.setText(getkey("past_trips_information"));
        TextView tabfordetails = (TextView) findViewById(R.id.tabfordetails);
        tabfordetails.setText(getkey("tap_for_details"));


//        spinnerCustom = (Spinner) findViewById(R.id.gender_spinner1);
//        Tv_city = (TextView) findViewById(R.id.ratecard_city_textview1);
        Iv_back = (ImageView) findViewById(R.id.ride_list_back_imageview);
//        username_tv = (CustomTextView1) findViewById(R.id.username_tv);
//        username_tv.setText(userName);


        currentDateDay = findViewById(R.id.currentdate_day);

        rightarraow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monthvalue = monthvalue + 1;

                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);
                jsonParams.put("month", String.valueOf(monthvalue));
                jsonParams.put("year", String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
                jsonParams.put("filter_type", "custom");
                jsonParams.put("page", String.valueOf(currentPage));
                jsonParams.put("perPage", perPage);
                if (isInternetPresent) {
                    PostRequest_myrideList(Iconstant.myrides_url, jsonParams);
                } else {
                    Alert(getkey("action_error"),getkey("alert_nointernet"));

                }
            }
        });
        leftarraow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monthvalue = monthvalue - 1;

                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);
                jsonParams.put("month", String.valueOf(monthvalue));
                jsonParams.put("year", String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
                jsonParams.put("filter_type", "custom");
                jsonParams.put("page", String.valueOf(currentPage));
                jsonParams.put("perPage", perPage);
                if (isInternetPresent) {
                    PostRequest_myrideList(Iconstant.myrides_url, jsonParams);
                } else {
                    Alert(getkey("action_error"), getkey("alert_nointernet"));

                }
            }
        });


        Intent intent = getIntent();
        if (intent.hasExtra("Class")) {
            type = intent.getStringExtra("Class");
        }
        if (intent.hasExtra("drivername")) {
            drivername = intent.getStringExtra("drivername");
        }

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        Calendar c = Calendar.getInstance();
        currentYear = c.get(Calendar.YEAR);
        currentMonth = c.get(Calendar.MONTH) + 1;
        getDate = c.get(Calendar.DATE);


        String day = getResources().getStringArray(R.array.day_list_array)[c.get(Calendar.DAY_OF_WEEK) - 1].toUpperCase();
        ScurrentYear = currentYear + "";
        ScurrentMonth = (currentMonth) + "";
        try {
            formatMonth(ScurrentMonth, ScurrentYear, day);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        postData();

    }

    public String formatMonth(String month, String Year, String day) throws java.text.ParseException {
        SimpleDateFormat monthParse = new SimpleDateFormat("MM");
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMMM");
        String month1 = monthDisplay.format(monthParse.parse(month)).toUpperCase();
        currentDateDay.setText(day + " " + getDate + " " + month1 + " " + Year);
        return monthDisplay.format(monthParse.parse(month));
    }

    private void postData() {

//        HashMap<String, String> jsonParams = new HashMap<String, String>();
//        jsonParams.put("user_id", UserID);
//        jsonParams.put("filter_type", filterType);
//        jsonParams.put("page", String.valueOf(currentPage));
//        jsonParams.put("perPage", perPage);
//        if (isInternetPresent) {
//            PostRequest_myrideList(Iconstant.myrides_url, jsonParams);
//        } else {

//        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("month", String.valueOf(monthvalue));
        jsonParams.put("year", String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
        jsonParams.put("filter_type", "custom");
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        if (isInternetPresent) {
            PostRequest_myrideList(Iconstant.myrides_url, jsonParams);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));

        }
    }


    private void datePicker(Bundle savedInstanceState) {

        dialogCaldroidFragment = new CaldroidFragment();
        dialogCaldroidFragment.setCaldroidListener(caldroidListener);

        // If activity is recovered from rotation
        final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
        if (savedInstanceState != null) {
            dialogCaldroidFragment.restoreDialogStatesFromKey(getSupportFragmentManager(), savedInstanceState,
                    "DIALOG_CALDROID_SAVED_STATE", dialogTag);
            Bundle args = dialogCaldroidFragment.getArguments();
            if (args == null) {
                args = new Bundle();
                dialogCaldroidFragment.setArguments(args);
            }
        } else {
            // Setup arguments
            Bundle bundle = new Bundle();
            // Setup dialogTitle
            dialogCaldroidFragment.setArguments(bundle);
        }


        Calendar cal = Calendar.getInstance();
        Date currentDate = null;
        Date maximumDate = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            String formattedDate = df.format(cal.getTime());
            currentDate = df.parse(formattedDate);

            // Max date is next 7 days
            cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 7);
            maximumDate = cal.getTime();

        } catch (ParseException e1) {
            e1.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }


        dialogCaldroidFragment.setMaxDate(currentDate);
        dialogCaldroidFragment.show(getSupportFragmentManager(), dialogTag);
        dialogCaldroidFragment.refreshView();
    }

    private void RideListFilter() {

    }

    // Setup CaldroidListener
    final CaldroidListener caldroidListener = new CaldroidListener() {
        @Override
        public void onSelectDate(Date date, View view) {


            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = df.format(date);
            FilterDate = formattedDate;
            //   Et_search_date.setText(formattedDate);
            dialogCaldroidFragment.dismiss();
        }


        @Override
        public void onChangeMonth(int month, int year) {
            String text = "month: " + month + " year: " + year;
        }

        @Override
        public void onLongClickDate(Date date, View view) {

        }

        @Override
        public void onCaldroidViewCreated() {
        }
    };

    private void PostRequest_myrideList(String url, HashMap<String, String> jsonParams) {

        final Dialog dialog = new Dialog(RideList.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("----------rideList--json-------------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(RideList.this);

        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-----------rideList---reponse-------------------" + response);
                dialog.dismiss();
                String Sstatus = "", Smessage = "";

                try {
                    JSONObject obj = new JSONObject(response);
                    Sstatus = obj.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject obj2 = obj.getJSONObject("response");
                        currentPage = Integer.parseInt(obj2.getString("current_page"));
                        nextPage = obj2.getString("next_page");

                        if (nextPage.equalsIgnoreCase("")) {
                            Tv_next.setVisibility(View.GONE);
                        } else {
                            Tv_next.setVisibility(View.VISIBLE);
                        }
                        if (obj2.has("total_rides")) {
                            Stotal_rides = obj2.getString("total_rides");
                        }
                        currentdate_day.setText(obj2.getString("month_year"));
                        Scompleted_rides = obj2.getString("completed_rides");
                        Scancelled_rides = obj2.getString("cancelled_rides");
                        Stotal_distance = obj2.getString("total_distance");
                        Object check_object = obj2.get("rides");
                        if (check_object instanceof JSONArray) {
                            JSONArray RideList_list_jsonArray = obj2.getJSONArray("rides");
                            if (RideList_list_jsonArray.length() > 0) {
                                tripcount.setText(String.valueOf(RideList_list_jsonArray.length()));

                                RideList_itemList.clear();
                                for (int i = 0; i < RideList_list_jsonArray.length(); i++) {
                                    JSONObject search_obj = RideList_list_jsonArray.getJSONObject(i);
                                    RideListPojo RideListPojo = new RideListPojo();
                                    RideListPojo.setDatetime(search_obj.getString("datetime"));
                                    RideListPojo.setDisplay_status(search_obj.getString("display_status"));
                                    RideListPojo.setDistance(search_obj.getString("distance"));
                                    RideListPojo.setGroup(search_obj.getString("group"));
                                    RideListPojo.setPickup(search_obj.getString("pickup"));
                                    RideListPojo.setRide_category(search_obj.getString("ride_category"));
                                    RideListPojo.setRide_date(search_obj.getString("ride_date"));
                                    RideListPojo.setRide_id(search_obj.getString("ride_id"));
                                    RideListPojo.setDriver_name(search_obj.getString("driver_name"));
                                    RideListPojo.setRide_status(search_obj.getString("ride_status"));
                                    RideListPojo.setRide_time(search_obj.getString("ride_time"));
                                    RideListPojo.setRide_type(search_obj.getString("ride_type"));
                                    RideListPojo.setDriverImage(search_obj.getString("driver_image"));
                                    RideListPojo.setRideAmount(search_obj.getString("currency") + " " + search_obj.getString("tot_fare"));
                                    RideList_itemList.add(RideListPojo);
                                }
                                searchstatus = true;
                            } else {
                                tripcount.setText("0");
                                searchstatus = false;
                                RideList_itemList.clear();
                            }
                        } else {
                            tripcount.setText("0");
                            searchstatus = false;
                            RideList_itemList.clear();
                        }

                        Object check_object1 = obj2.get("ongoing_arr");
                        if (check_object1 instanceof JSONObject) {

                            JSONObject jsonObject = obj2.getJSONObject("ongoing_arr");
                            if (jsonObject.length() > 0) {
                                ongoing_arrstatus = true;
                                SongoningRideDate = jsonObject.getString("date");
                                Scategory = jsonObject.getString("category");
                                Svechile_no = jsonObject.getString("vechile_no");
                                Sridestatus = jsonObject.getString("ride_status");
                                Seta_text = jsonObject.getString("eta_text");
                                SonGoingRideId = jsonObject.getString("ride_id");

                            } else {
                                ongoing_arrstatus = false;
                            }


                        }

                    } else {
                        Smessage = obj.getString("response");
                    }

                    dialogDismiss();

                } catch (JSONException e) {

                    e.printStackTrace();
                    dialogDismiss();

                }
                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {

                    if (currentPage == 1) {
                        Tv_previous.setVisibility(View.GONE);
                    } else {
                        Tv_previous.setVisibility(View.VISIBLE);
                    }


                    if (searchstatus) {
                        //  Tv_next.setVisibility(View.VISIBLE);
//                        R1_bottom.setVisibility(View.VISIBLE);.
                        Lv_ride.setVisibility(View.VISIBLE);
                        Tv_no_rides.setVisibility(View.GONE);

                    } else {
                        //  Tv_next.setVisibility(View.GONE);
                        if (currentPage == 1) {
//                            R1_bottom.setVisibility(View.GONE);
                            Lv_ride.setVisibility(View.GONE);
                        } else {
//                            R1_bottom.setVisibility(View.VISIBLE);
                            Lv_ride.setVisibility(View.VISIBLE);
                        }
                        Tv_no_rides.setVisibility(View.VISIBLE);
                    }
                    rideListAdapter = new RideListAdapter(RideList.this, RideList_itemList, drivername);
                    Lv_ride.setAdapter(rideListAdapter);
                    rideListAdapter.notifyDataSetChanged();

                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();

            }

        });
    }

    //---Scroll ListView to bottom---
    private static void scrollMyListViewToBottom() {
        Lv_ride.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                Lv_ride.setSelection(rideListAdapter.getCount() - 1);
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(RideList.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (type.equals("Complaint")) {
            /*   onBackPressed();*/
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        } else if (type.equals("Home")) {
            Intent intent = new Intent(RideList.this, Navigation_new.class);
            // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(RideList.this, RideListHomePage.class);
            // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<String> asr;

        public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
            this.asr = asr;
            activity = context;
        }


        public int getCount() {
            return asr.size();
        }

        public Object getItem(int i) {
            return asr.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }


        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(RideList.this);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(16);
            txt.setGravity(Gravity.CENTER);
            txt.setText(asr.get(position));
            txt.setBackground(getResources().getDrawable(R.drawable.spinner_drop_down_background));
            txt.setTextColor(Color.parseColor("#ffffff"));
            return txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(RideList.this);
            txt.setGravity(Gravity.CENTER);
            txt.setPadding(10, 0, 0, 0);
            txt.setTextSize(14);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt.setText(asr.get(i));
            txt.setTextColor(Color.parseColor("#ffffff"));
            txt.setVisibility(View.INVISIBLE);
            return txt;
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        active = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        active = false;
        unregisterReceiver(refreshReceiver);
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}




