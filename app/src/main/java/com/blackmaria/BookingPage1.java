package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.CarListAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.InterFace.CallBack;
import com.blackmaria.pojo.BookingPojo1;
import com.blackmaria.pojo.NewCategoryListPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.GeocoderHelper;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/*
 Created by user144 on 3/7/2017.
 */

public class BookingPage1 extends ActivityHockeyApp implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    GPSTracker gps;

    private CustomTextView Tv_PickupAddress;
    private CustomTextView Tv_CarAvilable, Tv_wish, Tv_userName, Tv_destination;
    private ImageView Iv_back, Rl_drawer, Iv_show_car1, Iv_show_car2, roadImage;
    private TextView findPlaceTv, BookNowTv;
    private String UserID = "", UserName = "", CategoryID = "", Gender = "";

    //-----Declaration For Enabling Gps-------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 299;

    private int placeSearch_request_code = 200;
    private String SdestinationLatitude = "";
    private String SdestinationLongitude = "", intentcall = "";
    private String SdestinationLocation = "";
    private double SpickupLatitude, SpickupLongitude;
    private String SpickupLocation = "";

    private RefreshReceiver refreshReceiver;
    private Dialog dialog, dialog1;
    private ServiceRequest mRequest;
    ArrayList<BookingPojo1> driver_list;
    ArrayList<String> driver_list1;
    ArrayList<String> driver_list2;
    private boolean driver_status = false;
    private RelativeLayout bookMyRidePickupAddressLayout, Rl_CarAvilable, refresh_layout, check_car_available, Rl_ready_book, Rl_schedual_ride, Rl_book_now;
    private CustomTextView Tv_booking1, Tv_booking2, Tv_notes, TV_vechicles_count;
    private ImageView Iv_car;
    RelativeLayout hand_tounch_image;
    private RelativeLayout tvBookingFindplaces;
    private ArrayList<NewCategoryListPojo> catetorylist;

    private boolean isCategoryDataAvailable = false;
    private String Category_Selected_id = "";
    private String Str_free_waittime = "", Str_wait_time_person = "", Str_car_img, Str_car_eta;
    private String Savailable_count = "";
    private String Spage = "";


    //Set the values
    Map<String, List<String>> set = new HashMap<>();

    public static Activity activity;


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(BookingPage1.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(BookingPage1.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_page1);
        activity = BookingPage1.this;
        initialize();

        Iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Spage.equalsIgnoreCase("aboutus")) {
                    Intent intent = new Intent(BookingPage1.this, AboutUs.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else if (Spage.equalsIgnoreCase("bidfare")) {
                    Intent intent = new Intent(BookingPage1.this, BidFarePage.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                } else {
                    Intent intent = new Intent(BookingPage1.this, Navigation_new.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                }

            }
        });
      /*  Tv_destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SpickupLocation.length() > 0) {
                    Intent intent = new Intent(BookingPage1.this, DropLocationSelect.class);
                    startActivityForResult(intent, placeSearch_request_code);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Alert(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.home_label_invalid_pickUp));
                }

            }
        });*/
        refresh_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gps.canGetLocation() && gps.isgpsenabled()) {
                    SpickupLatitude = gps.getLatitude();
                    SpickupLongitude = gps.getLongitude();
                    SpickupLocation = new GeocoderHelper().fetchCityName(BookingPage1.this, SpickupLatitude, SpickupLongitude, callBack);//getCompleteAddressString(SpickupLatitude, SpickupLongitude);
                    Tv_PickupAddress.setText(SpickupLocation);
                    if (isInternetPresent) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("user_id", UserID);
                        jsonParams.put("lat", String.valueOf(SpickupLatitude));
                        jsonParams.put("lon", String.valueOf(SpickupLongitude));
                        jsonParams.put("category", CategoryID);
                        PostRequest(Iconstant.get_cabs_url, jsonParams);
                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }
                } else {
                    enableGpsService();
                }
            }
        });
        hand_tounch_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCarList_Dialog();
            }
        });
        tvBookingFindplaces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchNearByIntent = new Intent(BookingPage1.this, SearchByNearHome.class);
                searchNearByIntent.putExtra("Savailable_count", Savailable_count);
                searchNearByIntent.putExtra("pickuplocation", Tv_PickupAddress.getText().toString());
                searchNearByIntent.putExtra("pickuplat", String.valueOf(SpickupLatitude));
                searchNearByIntent.putExtra("pickuplon", String.valueOf(SpickupLongitude));
                startActivity(searchNearByIntent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        Rl_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (intentcall.equalsIgnoreCase("findplaceBooking")) {

                    Intent intent = new Intent(BookingPage1.this, SelectVehicleCategoryPage.class);
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                    intent.putExtra("destinationAddress", SdestinationLocation);
                    intent.putExtra("destinationLatitude", SdestinationLatitude);
                    intent.putExtra("destinationLongitude", SdestinationLongitude);
                    intent.putExtra("page", "BookingPage1");
                    intent.putExtra("intentCall", "findplaceBooking");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Intent intent = new Intent(BookingPage1.this, SelectVehicleCategoryPage.class);
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                    intent.putExtra("destinationAddress", "");
                    intent.putExtra("destinationLatitude", "");
                    intent.putExtra("destinationLongitude", "");
                    intent.putExtra("page", "BookingPage1");
                    intent.putExtra("intentCall", "normalBooking");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }

            }
        });


        Rl_schedual_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentcall.equalsIgnoreCase("findplaceBooking")) {
                    Intent intent = new Intent(BookingPage1.this, SelectVehicleCategoryPage.class);
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                    intent.putExtra("destinationAddress", SdestinationLocation);
                    intent.putExtra("destinationLatitude", SdestinationLatitude);
                    intent.putExtra("destinationLongitude", SdestinationLongitude);
                    intent.putExtra("page", "BookingPage1");
                    intent.putExtra("intentCall", "findplaceBooking");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Intent intent = new Intent(BookingPage1.this, SelectVehicleCategoryPage.class);
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                    intent.putExtra("destinationAddress", "");
                    intent.putExtra("destinationLatitude", "");
                    intent.putExtra("destinationLongitude", "");
                    intent.putExtra("page", "BookingPage1");
                    intent.putExtra("intentCall", "normalBooking");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });


        bookMyRidePickupAddressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookingPage1.this, DropLocationSelect.class);
                intent.putExtra("page", "booking1");
                startActivityForResult(intent, placeSearch_request_code);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

    }


    private void initialize() {
        cd = new ConnectionDetector(BookingPage1.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(BookingPage1.this);

        gps = new GPSTracker(BookingPage1.this);
        driver_list = new ArrayList<BookingPojo1>();
        catetorylist = new ArrayList<NewCategoryListPojo>();
        driver_list1 = new ArrayList<>();
        driver_list2 = new ArrayList<>();
        HashMap<String, String> info = session.getUserDetails();
        CategoryID = info.get(SessionManager.KEY_CATEGORY);
        UserID = info.get(SessionManager.KEY_USERID);
        UserName = info.get(SessionManager.KEY_USERNAME);
        session.setTimerPage("");

        HashMap<String, String> info1 = session.getGender();
        Gender = info1.get(SessionManager.KEY_GENDER);


        Tv_PickupAddress = (CustomTextView) findViewById(R.id.book_my_ride_pickup_address_textView);
        Tv_CarAvilable = (CustomTextView) findViewById(R.id.book_my_ride_no_of_cars_textview);
        Rl_CarAvilable = (RelativeLayout) findViewById(R.id.book_my_ride_no_of_cars_layout);
        bookMyRidePickupAddressLayout = (RelativeLayout) findViewById(R.id.book_my_ride_pickup_address_layout1);
        hand_tounch_image = (RelativeLayout) findViewById(R.id.hand_tounch_image);
        Tv_booking1 = (CustomTextView) findViewById(R.id.book_my_ride_near_by_cars_textview1);
        Tv_booking2 = (CustomTextView) findViewById(R.id.book_my_ride_near_by_cars_textview2);
        Tv_notes = (CustomTextView) findViewById(R.id.book_my_ride_notes_textview);
        Iv_car = (ImageView) findViewById(R.id.book_my_ride_no_of_cars_imageview);
        refresh_layout = (RelativeLayout) findViewById(R.id.refresh_layout);
        check_car_available = (RelativeLayout) findViewById(R.id.check_car_available);
        Tv_wish = (CustomTextView) findViewById(R.id.book_my_ride_wish_textview);
        Tv_userName = (CustomTextView) findViewById(R.id.book_my_ride_user_name_textview);
        Tv_destination = (CustomTextView) findViewById(R.id.book_my_ride_set_destination_textview);
        Iv_back = (ImageView) findViewById(R.id.book_my_ride_back_imageview);
        tvBookingFindplaces = (RelativeLayout) findViewById(R.id.tv_booking_findplaces);
        Rl_ready_book = (RelativeLayout) findViewById(R.id.ready_to_book_layout);
        Rl_schedual_ride = (RelativeLayout) findViewById(R.id.booking_page_schedule_ride);
        Rl_book_now = (RelativeLayout) findViewById(R.id.booking_page_book_ride);
        TV_vechicles_count = (CustomTextView) findViewById(R.id.book_my_ride_no_vehicles);
        roadImage = (ImageView) findViewById(R.id.road_image);
        Iv_show_car2 = (ImageView) findViewById(R.id.random_show_car2);

        findPlaceTv = (TextView) findViewById(R.id.find_place_tv);
        BookNowTv = (TextView) findViewById(R.id.booknow_tv);


        RandomView();
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        registerReceiver(refreshReceiver, intentFilter);


        Intent intent = getIntent();
        if (intent.hasExtra("intentCall")) {
            intentcall = intent.getStringExtra("intentCall");
            if (intentcall.equalsIgnoreCase("findplaceBooking")) {
                SdestinationLocation = intent.getStringExtra("destinationAddress");
                SdestinationLatitude = intent.getStringExtra("destinationLatitude");
                SdestinationLongitude = intent.getStringExtra("destinationLongitude");

            }
        }
        if (intent.hasExtra("str_page")) {
            Spage = intent.getStringExtra("str_page");
        }

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 0 && timeOfDay < 12) {
            Tv_wish.setText(getResources().getString(R.string.good_morning_lable).toUpperCase());
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            Tv_wish.setText(getResources().getString(R.string.good_afternoon_lable).toUpperCase());
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            Tv_wish.setText(getResources().getString(R.string.good_evening_lable).toUpperCase());
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            Tv_wish.setText(getResources().getString(R.string.good_night_lable).toUpperCase());
        }
        if (Gender.equalsIgnoreCase("male")) {
            Tv_userName.setText("Mr " + UserName.toUpperCase());
            System.out.println("==============Murugan UserName=========" + Gender + " " + UserName);
        } else if (Gender.equalsIgnoreCase("female")) {
            Tv_userName.setText("Mrs " + UserName.toUpperCase());
            System.out.println("==============Murugan UserName=========" + Gender + " " + UserName);
        } else {
            Tv_userName.setText(UserName.toUpperCase());
            System.out.println("==============Murugan UserName=========" + Gender + " " + UserName);
        }

        if (gps.canGetLocation() && gps.isgpsenabled()) {
            SpickupLatitude = gps.getLatitude();
            SpickupLongitude = gps.getLongitude();
            SpickupLocation = new GeocoderHelper().fetchCityName(BookingPage1.this, SpickupLatitude, SpickupLongitude, callBack);//
            Tv_PickupAddress.setText(SpickupLocation);
            if (isInternetPresent) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);
                jsonParams.put("lat", String.valueOf(SpickupLatitude));
                jsonParams.put("lon", String.valueOf(SpickupLongitude));
                jsonParams.put("category", CategoryID);
                PostRequest(Iconstant.get_cabs_url, jsonParams);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
            }
        } else {
            enableGpsService();
        }

        Rl_drawer = (ImageView) findViewById(R.id.drawer_icon);

        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);
    }

    //-------------Method to get Complete Address------------
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(BookingPage1.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                Log.e("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Current loction address", "Cannot get Address!");
        }
        return strAdd;
    }


    //---------------------------------------------------------------
    private void PostRequest(String Url, HashMap<String, String> jsonParams) {
        dialog1 = new Dialog(BookingPage1.this);
        dialog1.getWindow();
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.custom_loading);

        dialog1.setCanceledOnTouchOutside(false);
        dialog1.show();

        System.out.println("-----------BookingPage1 Url--------------" + Url);


        System.out.println("-----------BookingPage1 jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(BookingPage1.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------BookingPage1 reponse-------------------" + response);

                String Sstatus = "", Smessage = "", Sride_min = "", ScurrencySymbol = "", Sprofile_complete = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject jobject = object.getJSONObject("response");
                        if (jobject.length() > 0) {
                            Savailable_count = jobject.getString("available_count");
                            Sprofile_complete = jobject.getString("profile_complete");

                            Object check_driver_object = jobject.get("drivers");
                            if (check_driver_object instanceof JSONArray) {

                                JSONArray driver_array = jobject.getJSONArray("drivers");
                                if (driver_array.length() > 0) {
                                    driver_list.clear();

                                    for (int j = 0; j < driver_array.length(); j++) {
                                        JSONObject driver_object = driver_array.getJSONObject(j);

                                        BookingPojo1 pojo = new BookingPojo1();
                                        if (driver_object.has("distance")) {
                                            pojo.setDistance(driver_object.getString("distance"));
                                        }
                                        if (driver_object.has("brand_image")) {
                                            pojo.setBrand_image(driver_object.getString("brand_image"));
                                        }
                                        driver_list1.add(driver_object.getString("distance"));
                                        driver_list2.add(driver_object.getString("brand_image"));

                                        driver_list.add(pojo);

                                    }
                                    set.put("distance", driver_list1);
                                    set.put("brand_image", driver_list2);
                                    session.carList = set;

                                    driver_status = true;
                                    session.driverStatus = driver_status;
                                } else {
                                    session.carList = set;
                                    driver_list.clear();
                                    driver_status = false;
                                    session.driverStatus = driver_status;
                                }
                            } else {

                                driver_status = false;
                                session.driverStatus = driver_status;
                            }


                            Object check_catgory_object = jobject.get("category");

                            if (check_catgory_object instanceof JSONArray) {


                                JSONArray category_array = jobject.getJSONArray("category");

                                if (category_array.length() > 0) {

                                    catetorylist.clear();


                                    for (int m = 0; m < category_array.length(); m++) {

                                        JSONObject category_object = category_array.getJSONObject(m);
                                        NewCategoryListPojo cate_pojo = new NewCategoryListPojo();
                                        cate_pojo.setCategory_Id(category_object.getString("id"));
                                        cate_pojo.setCategory_name(category_object.getString("name"));
                                        cate_pojo.setCat_eta(category_object.getString("eta"));
                                        cate_pojo.setMax_person(category_object.getString("no_of_seats"));
                                        cate_pojo.setCat_type(category_object.getString("type"));
                                        cate_pojo.setCat_normal_img(category_object.getString("icon_normal"));
                                        cate_pojo.setCat_active_img(category_object.getString("icon_active"));
                                        cate_pojo.setCar_image(category_object.getString("icon_car_image"));
                                        cate_pojo.setOffer_type(category_object.getString("offer_type"));
                                        cate_pojo.setDelivery_des(category_object.getString("description"));
                                        cate_pojo.setStr_free_waittime(category_object.getString("free_waiting_time"));
                                        cate_pojo.setStr_wait_time_person(category_object.getString("waiting_time_per"));
                                        cate_pojo.setStrOfferType(category_object.getString("vehicle_type"));


                                        catetorylist.add(cate_pojo);

                                    }

                                    isCategoryDataAvailable = true;

                                } else {
                                    isCategoryDataAvailable = false;
                                }


                            } else {

                                isCategoryDataAvailable = false;

                            }


                            dialogDismiss1();
                        }

                    } else {
                        Smessage = object.getString("response");
                        dialogDismiss1();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss1();
                }
                dialogDismiss1();

                if (Sstatus.equalsIgnoreCase("1")) {
                    if (!Savailable_count.equalsIgnoreCase("NO ONE NEAR YOU")) {
                        refresh_layout.setVisibility(View.GONE);
                    } else {

                        refresh_layout.setVisibility(View.VISIBLE);
                    }

                    if (driver_status) {
                        Rl_CarAvilable.setBackground(getResources().getDrawable(R.drawable.booking_curve_background));
                        check_car_available.setBackgroundColor(getResources().getColor(R.color.transparant_color));
                        Tv_CarAvilable.setText(Savailable_count);
                        Tv_booking1.setText(getResources().getString(R.string.where_do_you_want_to_do_lable));
                        Tv_booking2.setText(getResources().getString(R.string.there_are_lable));
                        Tv_booking1.setTextColor(getResources().getColor(R.color.black));
                        Tv_booking2.setTextColor(getResources().getColor(R.color.black));
                        Tv_notes.setVisibility(View.GONE);
                        Tv_notes.setText(getResources().getString(R.string.we_take_you_wherever_you_need_lable));
//                        Iv_car.setVisibility(View.VISIBLE);
                        Rl_ready_book.setVisibility(View.VISIBLE);
                        Rl_book_now.setVisibility(View.VISIBLE);
                        Rl_schedual_ride.setVisibility(View.GONE);
                        TV_vechicles_count.setText("there are" + " " + Savailable_count + " " + "VEHICLES NEAR YOU");
                        Rl_CarAvilable.setClickable(true);
                        Rl_drawer.setVisibility(View.VISIBLE);
                        roadImage.setImageResource(R.drawable.square_box_white);

                        findPlaceTv.setTypeface(findPlaceTv.getTypeface(), Typeface.BOLD);
                        BookNowTv.setTypeface(BookNowTv.getTypeface(), Typeface.BOLD);

                    } else {
                        Rl_CarAvilable.setBackground(getResources().getDrawable(R.drawable.no_cars_bg));
                        Tv_CarAvilable.setText(Savailable_count);
                        Tv_booking1.setText(getResources().getString(R.string.sorry_all_our_parteners_on_trip_lable));
                        Tv_booking2.setText(getResources().getString(R.string.or_there_is_lable));
                        check_car_available.setBackgroundResource(R.drawable.white_curved_bg);
                        Tv_notes.setVisibility(View.VISIBLE);
                        Tv_notes.setText(getResources().getString(R.string.would_you_like_to_schedule_your_trip_lable));
//                        Iv_car.setVisibility(View.GONE);
                        Rl_ready_book.setVisibility(View.GONE);
                        Rl_book_now.setVisibility(View.GONE);
                        Rl_schedual_ride.setVisibility(View.VISIBLE);
                        TV_vechicles_count.setText(getResources().getString(R.string.novehicle_label));
                        roadImage.setImageResource(R.drawable.roadimage);
                        Rl_CarAvilable.setClickable(false);
                        Rl_drawer.setVisibility(View.GONE);
                    }

                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss1();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(BookingPage1.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //--------------Alert Method-----------
    private void AlertDelivary(int title, String alert) {

        final PkDialog mDialog = new PkDialog(BookingPage1.this);
        mDialog.setTitleImage(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentcall.equalsIgnoreCase("findplaceBooking")) {

                    Intent intent = new Intent(BookingPage1.this, BookingPage3.class);
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                    intent.putExtra("destinationAddress", SdestinationLocation);
                    intent.putExtra("destinationLatitude", SdestinationLatitude);
                    intent.putExtra("destinationLongitude ", SdestinationLongitude);
                    intent.putExtra("CategoryID", Category_Selected_id);
                    intent.putExtra("driver_status", driver_status);
                    intent.putExtra("page", "BookingPage1");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Intent intent = new Intent(BookingPage1.this, BookingPage3.class);
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                    intent.putExtra("destinationAddress", "");
                    intent.putExtra("destinationLatitude", "");
                    intent.putExtra("destinationLongitude", "");
                    intent.putExtra("CategoryID", Category_Selected_id);
                    intent.putExtra("page", "BookingPage1");
                    intent.putExtra("driver_status", driver_status);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void dialogDismiss1() {
        if (dialog1 != null) {
            dialog1.dismiss();
        }
    }


    private void showCarList_Dialog() {
        final Dialog dialog = new Dialog(BookingPage1.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.car_list_dialog);

        ListView vechiles_listview = (ListView) dialog.findViewById(R.id.nearest_vechile_list_listview);
        RelativeLayout close = (RelativeLayout) dialog.findViewById(R.id.car_list_nearest_vechile_close_layout);
        TextView nearest_vechile = (TextView) dialog.findViewById(R.id.car_list_nearest_vechile_textview);

        nearest_vechile.setText(driver_list.size() + getResources().getString(R.string.nearest_vehicles_lable));

        CarListAdapter personListAdapter = new CarListAdapter(BookingPage1.this, driver_list);

        vechiles_listview.setAdapter(personListAdapter);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        Window w = dialog.getWindow();
        w.setLayout((width * 6) / 8, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.show();
    }


    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(BookingPage1.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(BookingPage1.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOCATION) {
            if (resultCode == Activity.RESULT_OK) {


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        gps = new GPSTracker(BookingPage1.this);
                        SpickupLatitude = gps.getLatitude();
                        SpickupLongitude = gps.getLongitude();
                        System.out.println("-----------SpickupLatitude--------------" + SpickupLatitude);
                        System.out.println("-----------SpickupLongitude--------------" + SpickupLongitude);
                        SpickupLocation = new GeocoderHelper().fetchCityName(BookingPage1.this, SpickupLatitude, SpickupLongitude, callBack);//
                        Tv_PickupAddress.setText(SpickupLocation);

                    }
                }, 4000);


            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                enableGpsService();
            }
        } else if ((requestCode == placeSearch_request_code && resultCode == Activity.RESULT_OK && data != null)) {


            SpickupLatitude = Double.parseDouble(data.getStringExtra("Selected_Latitude"));
            SpickupLongitude = Double.parseDouble(data.getStringExtra("Selected_Longitude"));
            SpickupLocation = data.getStringExtra("Selected_Location");
            Tv_PickupAddress.setText(SpickupLocation);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (Spage.equalsIgnoreCase("aboutus")) {
            Intent intent = new Intent(BookingPage1.this, AboutUs.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (Spage.equalsIgnoreCase("bidfare")) {
            Intent intent = new Intent(BookingPage1.this, BidFarePage.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else {
            Intent intent = new Intent(BookingPage1.this, Navigation_new.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    private void RandomView() {
        // Randomise a background
        int[] asoderimage = {R.drawable.cars1, R.drawable.cars2, R.drawable.cars3, R.drawable.cars4, R.drawable.cars5, R.drawable.cars6};

        Random random = new Random(System.currentTimeMillis());
        int posOfImage = random.nextInt(asoderimage.length - 1);
        ImageView Iv_show_car_nw = (ImageView) findViewById(R.id.random_show_car1);
        System.out.println("-----------posOfImage number----------------" + posOfImage);
        Iv_show_car_nw.setBackgroundResource(asoderimage[posOfImage]);
    }


    CallBack callBack = new CallBack() {

        @Override

        public void onComplete(String LocationName) {

            System.out.println("-------------------addreess----------------0" + LocationName);

            Tv_PickupAddress.setText(LocationName);
            SpickupLocation = LocationName;
        }

        @Override

        public void onError(String errorMsg) {
        }

        @Override
        public void onCompleteAddress(String message) {

        }

    };
}
