package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.blackmaria.adapter.FavoriteListAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.FavoriteListPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialogtryagain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FavoriteList extends ActivityHockeyApp {

    private TextView Tv_selectedlocation, Rl_favorite;
    private RelativeLayout Rl_empty;
    private ImageView Rl_back;
    ImageView IV_next_page, IV_prev_page;
    private SwipeMenuListView listview;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "";
    LanguageDb mhelper;
    private String SselectedAddress = "";
    private String Sselected_latitude = "", Sselected_longitude = "";
    private StringRequest postrequest, deleteRequest;
    private Dialog dialog;
    private boolean isFavouriteListAvailable = false;
    private ArrayList<FavoriteListPojo> itemList;
    private FavoriteListAdapter adapter;
    private BroadcastReceiver updateReceiver;
    private ServiceRequest mRequest;
    public static Activity favoriteList_class;

    private RefreshReceiver refreshReceiver;
    private int currentPage = 1;
    private String sPage = "", perPage = "5";
    private boolean searchstatus = false;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(Fragment_HomePage.this, Fragment_HomePage.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        if (getkey("profile_label_menu").equalsIgnoreCase(sPage)) {
                        } else {
                            BookingPage2.BookingPage2_class.finish();
                        }

                        Intent intent1 = new Intent(FavoriteList.this, MyRideRating.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        if (getkey("profile_label_menu").equalsIgnoreCase(sPage)) {
                        } else {
                            BookingPage2.BookingPage2_class.finish();
                        }
                        Intent intent1 = new Intent(FavoriteList.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }

                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favourite_list_new_constrain);
        mhelper = new LanguageDb(this);
        initialize();
        favoriteList_class = FavoriteList.this;
        /* swipeMenu_Initialize();*/
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.favoriteList.refresh");
        updateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("com.favoriteList.refresh")) {
                    if (isInternetPresent) {
                        postData();
                        System.out.println("*****************refresh working***********************");

                    }
                }
            }
        };
        registerReceiver(updateReceiver, filter);

        Rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getkey("profile_label_menu").equalsIgnoreCase(sPage)) {
                    /*Intent intent = new Intent(FavoriteList.this, Navigation_new.class);
                    startActivity(intent);*/
                    overridePendingTransition(R.anim.exit, R.anim.enter);
                    finish();
                } else {
                    onBackPressed();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        });
        IV_next_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage + 1;
                postData();
            }
        });

        IV_prev_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage - 1;
                postData();

            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if ("booking3".equalsIgnoreCase(sPage)) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("Selected_Latitude", itemList.get(position).getLatitude());
                    returnIntent.putExtra("Selected_Longitude", itemList.get(position).getLongitude());
                    returnIntent.putExtra("Selected_Location", itemList.get(position).getAddress());
                    setResult(RESULT_OK, returnIntent);
                    onBackPressed();
                    finish();
                } else if (!getkey("profile_label_menu").equalsIgnoreCase(sPage) && !"booking3".equalsIgnoreCase(sPage)) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("Selected_Latitude", itemList.get(position).getLatitude());
                    returnIntent.putExtra("Selected_Longitude", itemList.get(position).getLongitude());
                    returnIntent.putExtra("Selected_Location", itemList.get(position).getAddress());
                    setResult(RESULT_OK, returnIntent);
                    onBackPressed();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        });
        Rl_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FavoriteList.this, FavoriteAdd_New.class);
                intent.putExtra("Intent_Latitude", Sselected_latitude);
                intent.putExtra("Intent_Longitude", Sselected_longitude);
                intent.putExtra("Intent_Address", SselectedAddress);
                intent.putExtra("Intent_IdentityKey", "New");
                if (getkey("profile_label_menu").equalsIgnoreCase(sPage)) {
                    intent.putExtra("str_page", sPage);
                } else if ("booking3".equalsIgnoreCase(sPage)) {
                    intent.putExtra("str_page", sPage);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        listview.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {


                cd = new ConnectionDetector(FavoriteList.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    switch (index) {
                        case 0:
                            System.out.println("----------sPage------------" + sPage);
                            Intent intent = new Intent(FavoriteList.this, FavoriteAdd_New.class);
                            intent.putExtra("Intent_Title", itemList.get(position).getTitle());
                            intent.putExtra("Intent_Latitude", itemList.get(position).getLatitude());
                            intent.putExtra("Intent_Longitude", itemList.get(position).getLongitude());
                            intent.putExtra("Intent_Address", itemList.get(position).getAddress());
                            intent.putExtra("Intent_LocationKey", itemList.get(position).getLocation_key());
                            intent.putExtra("Intent_IdentityKey", "Edit");

                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            break;
                        case 1:
                            Alertremove("CONFIRMATION", itemList.get(position).getTitle(), itemList.get(position).getLocation_key(), position);

                            break;
                    }
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }

                return false;// false : close the menu; true : not close the menu
            }
        });

    }

    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("user_id", UserID);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        if (isInternetPresent) {
            postRequest_FavoriteList(Iconstant.favoritelist_display_url, jsonParams);
        } else {
            Alert(getkey("action_error"),getkey("alert_nointernet"));

        }
    }

    private void initialize() {
        session = new SessionManager(FavoriteList.this);
        cd = new ConnectionDetector(FavoriteList.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemList = new ArrayList<FavoriteListPojo>();
        listview = (SwipeMenuListView) findViewById(R.id.favorite_list_listView);
//        Tv_selectedlocation = (TextView) findViewById(R.id.favorite_list_favorite_location_textview);
        Rl_back = (ImageView) findViewById(R.id.back_imag);
        Rl_favorite = findViewById(R.id.add_new_fav_btn);
        Rl_favorite.setText(getkey("add_new_favourites_places"));
        Rl_empty = (RelativeLayout) findViewById(R.id.favorite_list_listview_empty_layout);

        IV_next_page = (ImageView) findViewById(R.id.fav_list_nxt_page);
        IV_prev_page = (ImageView) findViewById(R.id.fav_list_prev_page);

        TextView favorite_list_listview_empty_heart_label1= (TextView) findViewById(R.id.favorite_list_listview_empty_heart_label1);
        favorite_list_listview_empty_heart_label1.setText(getkey("favorite_list_label_no_favorite_yet"));

        TextView dfff= (TextView) findViewById(R.id.dfff);
        dfff.setText(getkey("favorite_list_label_frequent_access"));

        TextView myreferalcode= (TextView) findViewById(R.id.myreferalcode);
        myreferalcode.setText(getkey("favourites"));

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        Intent intent = getIntent();

        if (intent.hasExtra("SelectedAddress")) {
            SselectedAddress = intent.getStringExtra("SelectedAddress");
            Sselected_latitude = intent.getStringExtra("SelectedLatitude");
            Sselected_longitude = intent.getStringExtra("SelectedLongitude");
//            Tv_selectedlocation.setText(SselectedAddress);
        } else if (intent.hasExtra("str_page")) {
            sPage = intent.getStringExtra("str_page");
        }
        postData();

    }

    private void swipeMenu_Initialize() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem editItem = new SwipeMenuItem(getApplicationContext());
                // set item background
                editItem.setBackground(new ColorDrawable(0xFFAFAFAF));
                // set item width
                editItem.setWidth(150);
                // set a icon
                // editItem.setIcon(R.drawable.menu_edit_icon);
                // set item title
                editItem.setTitle("Edit");
                // set item title fontsize
                editItem.setTitleSize(18);
                // set item title font color
                editItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(editItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(0xFFFD4542));
                // set item width
                deleteItem.setWidth(150);
                // set a icon
                deleteItem.setIcon(R.drawable.menu_delete_icon);
                // set item title
                //deleteItem.setTitle("Delete");
                // set item title fontsize
                //deleteItem.setTitleSize(17);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        listview.setMenuCreator(creator);
        // Swipe directions Right
        listview.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(FavoriteList.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void Alertremove(final String confirmation, String title, final String alert, final int position) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(FavoriteList.this);
        mDialog.setDialogTitle(confirmation);
        mDialog.setDialogMessage(title);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                postRequest_FavoriteDelete(Iconstant.favoritelist_delete_url, alert, position);
            }
        });
        mDialog.show();
    }

    //-----------------------Favourite List Display Post Request-----------------

    private void postRequest_FavoriteList(String Url, HashMap<String, String> jsonParams) {

        dialog = new Dialog(FavoriteList.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));
        System.out.println("-------------Favourite List Url----------------" + Url);


        mRequest = new ServiceRequest(FavoriteList.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("-------------Favourite List Response----------------" + response);
                String Sstatus = "", str_NextPage;
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        str_NextPage = response_object.getString("next_page");
                        if (str_NextPage.equalsIgnoreCase("")) {
                            IV_next_page.setVisibility(View.GONE);
                        } else {
                            IV_next_page.setVisibility(View.VISIBLE);
                        }
                        if (response_object.length() > 0) {
                            Object intervention = response_object.get("locations");
                            if (intervention instanceof JSONArray) {

                                JSONArray location_array = response_object.getJSONArray("locations");
                                if (location_array.length() > 0) {
                                    itemList.clear();
                                    for (int i = 0; i < location_array.length(); i++) {
                                        JSONObject location_object = location_array.getJSONObject(i);
                                        FavoriteListPojo pojo = new FavoriteListPojo();
                                        pojo.setTitle(location_object.getString("title"));
                                        pojo.setAddress(location_object.getString("address"));
                                        pojo.setLatitude(location_object.getString("latitude"));
                                        pojo.setLongitude(location_object.getString("longitude"));
                                        pojo.setLocation_key(location_object.getString("location_key"));
                                        itemList.add(pojo);
                                    }
                                    isFavouriteListAvailable = true;
                                    searchstatus = true;
                                } else {
                                    searchstatus = false;
                                }
                            } else {
                                isFavouriteListAvailable = false;
                                searchstatus = false;
                            }
                        }
                    } else {
                        IV_next_page.setVisibility(View.GONE);
                        isFavouriteListAvailable = false;
                    }
                    if (currentPage == 1) {
                        IV_prev_page.setVisibility(View.GONE);
                    } else {
                        IV_prev_page.setVisibility(View.VISIBLE);
                    }
                    if (searchstatus == true) {
                        // IV_next_page.setVisibility(View.VISIBLE);
                    } else {
                        // IV_next_page.setVisibility(View.GONE);
                        Rl_empty.setVisibility(View.VISIBLE);
                        listview.setVisibility(View.GONE);
                    }
                    if (Sstatus.equalsIgnoreCase("1") && isFavouriteListAvailable) {
                        listview.setVisibility(View.VISIBLE);
                        //  IV_next_page.setVisibility(View.VISIBLE);
                        Rl_empty.setVisibility(View.GONE);
                        adapter = new FavoriteListAdapter(FavoriteList.this,sPage, itemList);
                        listview.setAdapter(adapter);
                    } else {
                        listview.setVisibility(View.GONE);
                        //   IV_next_page.setVisibility(View.GONE);
                        Rl_empty.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void postRequest_FavoriteDelete(String Url, final String locationKey, final int position) {

        dialog = new Dialog(FavoriteList.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));
        System.out.println("-------------Favourite List Url----------------" + Url);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("location_key", locationKey);

        mRequest = new ServiceRequest(FavoriteList.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("-------------Favourite Delete Response----------------" + response);

                String Sstatus = "", Smessage = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        //removing the deleted position from listView
                        itemList.remove(position);
                        adapter.notifyDataSetChanged();

                        //code to show empty layout
                        if (itemList.size() == 0) {
                            listview.setVisibility(View.GONE);
                            IV_next_page.setVisibility(View.GONE);
                            Rl_empty.setVisibility(View.VISIBLE);
                        }

                        Alert(getkey("action_success"), Smessage);

                    } else {
                        Alert(getkey("alert_label_title"), Smessage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    //-----------------Move Back on pressed phone back button------------------
  /*  @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //onBackPressed();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    protected void onResume() {
        System.out.println("----------sPage------------" + sPage);
        super.onResume();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(updateReceiver);
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
