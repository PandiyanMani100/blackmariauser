package com.blackmaria;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by user144 on 9/18/2017.
 */

public class NewCloudMoneyTransferUrlProgress extends ActivityHockeyApp {
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private String payAmount="",reloadAmount = "", tokenId = "", flag = "", UserID = "", bankcode = "", friendId = "", authenticationId = "";
    //private Dialog dialog;
    private String radeemNumber = "",PayPalData="",accHolderName1 = "", bankCode="", holderAccountNumber1 = "",paymentMod="",withdrawlAmount="",crosstransfer="" ,paypal_amount = "",paypal_paymentcode="",paypal_id = "";
private View cashoutview;
    String transId="",from="",date="",currencyCode="",time="",Amount="",currency="";
    private String fromBookingpage ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_transfer_url_progress);
        initialize();
    }

    private void initialize() {

        session = new SessionManager(NewCloudMoneyTransferUrlProgress.this);
        cd = new ConnectionDetector(NewCloudMoneyTransferUrlProgress.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        radeemNumber = session.REDEEM_CODE;//session.getRedeemCouponcode();
        cashoutview=(View)findViewById(R.id.cashoutview);
        Intent reloadIn = getIntent();

        flag = reloadIn.getStringExtra("flag");
        if (flag.equalsIgnoreCase("2")) {

            if (reloadIn.hasExtra("authentication_id")) {
                authenticationId = reloadIn.getStringExtra("authentication_id");
            }


            reloadAmount = reloadIn.getStringExtra("total_amount");
            payAmount = reloadIn.getStringExtra("pay_amount");
            tokenId = reloadIn.getStringExtra("token_id");


        } else if (flag.equalsIgnoreCase("3")) {
            reloadAmount = reloadIn.getStringExtra("reloadamount");
            bankcode = reloadIn.getStringExtra("bankcode");
        } else if (flag.equalsIgnoreCase("0")) {
            reloadAmount = reloadIn.getStringExtra("reloadamount");
            friendId = reloadIn.getStringExtra("friendId");
            crosstransfer = reloadIn.getStringExtra("crosstransfer");
        }else if (flag.equalsIgnoreCase("XenditbankTransfer")) {
            withdrawlAmount = reloadIn.getStringExtra("amount");
            paymentMod = reloadIn.getStringExtra("mode");

            if(reloadIn.hasExtra("accHolderName1")) {
                accHolderName1 = reloadIn.getStringExtra("accHolderName1");
                holderAccountNumber1 = reloadIn.getStringExtra("holderAccountNumber1");
                bankCode = reloadIn.getStringExtra("bankCode");

            }


        }else if (flag.equalsIgnoreCase("paypal_withdrawal")) {
            paypal_amount = reloadIn.getStringExtra("amount");
            paypal_paymentcode = reloadIn.getStringExtra("payment_code");
            paypal_id = reloadIn.getStringExtra("paypal_id");

        } else if (flag.equalsIgnoreCase("cash_withdrawal")) {
            paypal_amount = reloadIn.getStringExtra("amount");
            paypal_paymentcode = reloadIn.getStringExtra("payment_code");
            cashoutview.setVisibility(View.VISIBLE);
        }
        if (getIntent().hasExtra("frombookingpage")) {
            fromBookingpage = getIntent().getStringExtra("frombookingpage");
        }



        if (flag.equalsIgnoreCase("1")) {

            transId= reloadIn.getStringExtra("transId");
            from= reloadIn.getStringExtra("from");
            date= reloadIn.getStringExtra("date").replace("%20"," ");
            time= reloadIn.getStringExtra("time").replace("%20"," ");
            Amount= reloadIn.getStringExtra("amount");
            currency= reloadIn.getStringExtra("currency");
            Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, ReloadXenditPaymentSuccessActivity.class);

            intent1.putExtra("amount",Amount );
            intent1.putExtra("ref_no",transId);
            intent1.putExtra("currency",currency );

            intent1.putExtra("from",from );
            intent1.putExtra("date",date );
            intent1.putExtra("time",time);
            if(fromBookingpage.equalsIgnoreCase("1")){
                intent1.putExtra("fromBookingpage", "1");
            }

            intent1.putExtra("flag","paypalpayment");
            startActivity(intent1);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
            session.createWalletAmount(currency +" "+ Amount);


           /* if (isInternetPresent) {
                postRequest_WalletMoneyPage1(Iconstant.wallet_money_url);
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
            }*/
        } else if (flag.equalsIgnoreCase("2")) {
            if (isInternetPresent) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();

                jsonParams.put("user_id", UserID);
                jsonParams.put("total_amount", reloadAmount);
                jsonParams.put("pay_amount", payAmount);
                jsonParams.put("token_id", tokenId);
                jsonParams.put("redeem_code", radeemNumber);
                jsonParams.put("authentication_id", authenticationId);
                xenditcardMethod(Iconstant.Xendit_Card_Url, jsonParams);
            } else {

                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
            }
        } else if (flag.equalsIgnoreCase("3")) {
            if (isInternetPresent) {

                HashMap<String, String> jsonParams = new HashMap<String, String>();

                jsonParams.put("user_id", UserID);
                jsonParams.put("total_amount", reloadAmount);
                jsonParams.put("bank_code", bankcode);
                jsonParams.put("redeem_code", radeemNumber);
                //jsonParams.put("authentication_id", authentication_id);
                xenditBankMethod(Iconstant.Xendit_Bank_Url, jsonParams);
            } else {

                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
            }
        } else if (flag.equalsIgnoreCase("0")) {
            if (isInternetPresent) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);
                jsonParams.put("transfer_amount", reloadAmount);
                jsonParams.put("friend_id", friendId);
                jsonParams.put("cross_transfer", crosstransfer);
                //jsonParams.put("authentication_id", authentication_id);
                transferToFriendAccount(Iconstant.find_Friend_Account_transfer_url, jsonParams);
            } else {

                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
            }
        } else if (flag.equalsIgnoreCase("XenditbankTransfer")) {
            if (isInternetPresent) {
                PostRquestForConfirm(Iconstant.Ewallet_withdrawal_confirm_url);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
            }
        }else if (flag.equalsIgnoreCase("paypal_withdrawal")) {


            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", UserID);
            jsonParams.put("amount", paypal_amount);
            jsonParams.put("mode", paypal_paymentcode);
            jsonParams.put("paypal_id", paypal_id);
            System.out.println("------------CloudMoney withdraw jsonParams--------------" + jsonParams);

            PostRquestForConfirm(Iconstant.Ewallet_withdrawal_confirm_url, jsonParams);


        } else if (flag.equalsIgnoreCase("cash_withdrawal")) {


            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", UserID);
            jsonParams.put("amount", paypal_amount);
            jsonParams.put("mode", paypal_paymentcode);
            System.out.println("------------CloudMoney withdraw jsonParams--------------" + jsonParams);

            PostRquestForConfirm(Iconstant.Ewallet_withdrawal_confirm_url, jsonParams);


        }

    }


   // ---------------------PAYPAL WITHDRAEA--------------------------

    private void PostRquestForConfirm(String Url, final HashMap<String, String> jsonParams) {


        System.out.println("-----------CloudMoneypaypal withdraw url--------------" + Url);

        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);

        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("------------CloudMoney withdraw response--------------" + response);

                String Ssatus = "", mode = "", ref_no = "", amount = "", currency = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Ssatus = object.getString("status");


                    if (Ssatus.equalsIgnoreCase("1")) {
//                        Alert(getResources().getString(R.string.pushnotification_alert_label_ride_arrived_success), Sresponse);
                        String Sresponse = object.getString("response");
                        mode = object.getString("mode");
                        ref_no = object.getString("ref_no");
                        amount = object.getString("amount");
                        currency = object.getString("currency");


                        if (flag.equalsIgnoreCase("paypal_withdrawal")) {

//                            Intent in = new Intent(NewCloudMoneyTransferUrlProgress.this, PaymentSuccessInfoActivity.class);
//                            in.putExtra("flag", flag);
//                            in.putExtra("mode", mode);
//                            in.putExtra("ref_no", ref_no);
//                            in.putExtra("amount", amount);
//                            in.putExtra("currency", currency);
//                            in.putExtra("paypal_id", paypal_id);
                            Intent in = new Intent(NewCloudMoneyTransferUrlProgress.this, WalletMoneyPage1.class);
                            if(fromBookingpage.equalsIgnoreCase("1")){
                                in.putExtra("fromBookingpage", "1");
                            }
                            startActivity(in);
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.exit);

                        }else if(flag.equalsIgnoreCase("cash_withdrawal")){

//                            Intent in = new Intent(NewCloudMoneyTransferUrlProgress.this, PaymentSuccessInfoActivity.class);
//                            in.putExtra("flag", flag);
//                            in.putExtra("mode", mode);
//                            in.putExtra("ref_no", ref_no);
//                            in.putExtra("amount", amount);
//                            in.putExtra("currency", currency);
                            Intent in = new Intent(NewCloudMoneyTransferUrlProgress.this, WalletMoneyPage1.class);
                            if(fromBookingpage.equalsIgnoreCase("1")){
                                in.putExtra("fromBookingpage", "1");
                            }
                            startActivity(in);
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.exit);

                        }


                    } else {
                        String Sresponse = object.getString("response");
                        String setresponse = getResources().getString(R.string.sorry_excesslimit);
//                        Alert1(getResources().getString(R.string.alert_label_title), Sresponse);
                        Alert1(getResources().getString(R.string.alert_label_title), setresponse);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {

            }
        });


    }

    private void PostRquestForConfirm(String Url) {

        System.out.println("-----------E Wallet confirm url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("amount", withdrawlAmount);
        jsonParams.put("mode", paymentMod);

        jsonParams.put("bank_code", bankCode);
        jsonParams.put("acc_holder_name",accHolderName1);
        jsonParams.put("acc_number", holderAccountNumber1);

        System.out.println("------------E Wallet confirm jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);

        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("------------E Wallet confirm response--------------" + response);

                String Ssatus = "",ref_no="",currency="",amount="",mode="",pay_to="",account="";

                try {

                    JSONObject object = new JSONObject(response);
                    Ssatus = object.getString("status");

                    if (Ssatus.equalsIgnoreCase("1")) {

                        ref_no=object.getString("ref_no");
                        currency=object.getString("currency");
                        amount=object.getString("amount");
                        mode = object.getString("mode");
                        pay_to = object.getString("pay_to");
                        account= object.getString("account");

                        Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, PaymentSuccessInfoActivity.class);

                        intent1.putExtra("account", account);
                        intent1.putExtra("mode", pay_to);
                        intent1.putExtra("amount", amount);
                        intent1.putExtra("ref_no", ref_no);
                        intent1.putExtra("currency", currency);
                        if(fromBookingpage.equalsIgnoreCase("1")){
                            intent1.putExtra("fromBookingpage", "1");
                        }


                        intent1.putExtra("flag", "XenditbankTransfer");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();



                    } else {
                        String Sresponse = object.getString("response");
                        Alert1(getResources().getString(R.string.alert_label_title), Sresponse,00);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();


                }


            }

            @Override
            public void onErrorListener() {



            }
        });


    }

    private void transferToFriendAccount(String Url, HashMap<String, String> jsonParams) {

        System.out.println("--------------transferToFriendAccount Url-------------------" + Url);
        System.out.println("--------------transferToFriendAccount jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------transferToFriendAccount Response-------------------" + response);
                String status = "", sResponse = "", wallet_amount = "", user_name = "", user_image = "", city = "",
                        transfer_amount = "", transaction_number = "", currency = "", userStatus = "",time="";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("response");
                        if (status.equalsIgnoreCase("1")) {
                            user_name = object.getString("user_name");
                            user_image = object.getString("user_image");
                            userStatus = object.getString("user_status");
                            time = object.getString("time");
                            city = object.getString("city");
                            transfer_amount = object.getString("transfer_amount");
                            transaction_number = object.getString("transaction_number");
                            currency = object.getString("currency");
                        }
                        if (status.equalsIgnoreCase("1")) {

                            // NewCloudMoneyReloadHomePage.refreshEdittext();
                            //        NewCloudMoneyReloadPaymentOptionPage.redeemRefresh();
                            Intent broadcastIntent = new Intent();
                            broadcastIntent.setAction("com.package.ACTION_CLASS_CABILY_MONEY_REFRESH");
                            sendBroadcast(broadcastIntent);

                            Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, NewCloudMoneyTransferUrlResponse.class);
                            intent1.putExtra("reloadamount", reloadAmount);
                            intent1.putExtra("user_name", user_name);
                            intent1.putExtra("user_image", user_image);
                            intent1.putExtra("city", city);
                            intent1.putExtra("time", time);
                            intent1.putExtra("transaction_number", transaction_number);
                            intent1.putExtra("currency", currency);
                            intent1.putExtra("status", userStatus);
                            intent1.putExtra("crosstransfer", crosstransfer);
                            if(fromBookingpage.equalsIgnoreCase("1")){
                                intent1.putExtra("fromBookingpage", "1");
                            }

                            startActivity(intent1);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();


                            // Alert(getResources().getString(R.string.action_success), sResponse);
                        } else {
                            Alert1(getResources().getString(R.string.action_error), sResponse,0);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

               /* if (dialog != null) {
                    dialog.dismiss();
                }*/
            }

            @Override
            public void onErrorListener() {
                /*if (dialog != null) {
                    dialog.dismiss();
                }*/
            }
        });
    }

    @SuppressLint("WrongConstant")
    private void xenditcardMethod(String Url, HashMap<String, String> jsonParams) {

        System.out.println("--------------xenditcardMethod Url-------------------" + Url);
        System.out.println("--------------xenditcardMethod jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------xenditcardMethod Response-------------------" + response);
                String status = "", sResponse = "", wallet_amount = "",currency="",transaction_id="";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("msg");
                        if (status.equalsIgnoreCase("1")) {


                            currency= object.getString("currency_code");
                            date= object.getString("date");
                            time= object.getString("time");
                            transaction_id= object.getString("transaction_no");
                            if (object.has("recharge_amount")) {
                                wallet_amount = object.getString("recharge_amount");
                            }

                            //  NewCloudMoneyReloadHomePage.refreshEdittext();
                            //   NewCloudMoneyReloadPaymentOptionPage.redeemRefresh();

                            Intent broadcastIntent = new Intent();
                            broadcastIntent.setAction("com.package.ACTION_CLASS_CABILY_MONEY_REFRESH");
                            sendBroadcast(broadcastIntent);

                            Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, ReloadXenditPaymentSuccessActivity.class);

                            intent1.putExtra("date",date );
                            intent1.putExtra("time",time);

                            intent1.putExtra("amount", wallet_amount);
                            intent1.putExtra("ref_no", transaction_id);
                            intent1.putExtra("currency", currency);
                            intent1.putExtra("flag","xenditReaload");
                            if(fromBookingpage.equalsIgnoreCase("1")){
                                intent1.putExtra("fromBookingpage", "1");
                            }
                            startActivity(intent1);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();   // Alert(getResources().getString(R.string.action_success), sResponse);


                        } else {
                            Alert1(getResources().getString(R.string.action_error), sResponse, 2);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

               /* if (dialog != null) {
                    dialog.dismiss();
                }*/
            }

            @Override
            public void onErrorListener() {
               /* if (dialog != null) {
                    dialog.dismiss();
                }*/
            }
        });
    }

    @SuppressLint("WrongConstant")
    private void xenditBankMethod(String Url, HashMap<String, String> jsonParams) {

        System.out.println("--------------xenditBankMethod Url-------------------" + Url);
        System.out.println("--------------xenditBankMethod jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------ForgotPinRequest Response-------------------" + response);
                String status = "", sResponse = "", wallet_amount = "", payment_code = "",currency="",transaction_id="";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("msg");
                        if (object.has("payment_code")) {
                            payment_code = object.getString("payment_code");
                        }

                        if (object.has("wallet_amount")) {
                            wallet_amount = object.getString("amount");
                        }
                        date= object.getString("date");
                        time= object.getString("time");

                        currency= object.getString("currency");
                        transaction_id= object.getString("transaction_id");


                        if (status.equalsIgnoreCase("1")) {


                            Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, ReloadXenditPaymentSuccessActivity.class);

                            intent1.putExtra("date",date );
                            intent1.putExtra("time",time);

                            intent1.putExtra("amount", reloadAmount);
                            intent1.putExtra("ref_no", transaction_id);
                            intent1.putExtra("currency", currency);
                            intent1.putExtra("xenditPaymentId", payment_code);
                            intent1.putExtra("flag","xenditBankReaload");
                            if(fromBookingpage.equalsIgnoreCase("1")){
                                intent1.putExtra("fromBookingpage", "1");
                            }
                            startActivity(intent1);

                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();

                        } else {
                            Alert1(getResources().getString(R.string.action_error), sResponse,3);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {

            }
        });
    }

    //-----------------------Cabily Money Post Request-----------------
    private void postRequest_WalletMoneyPage1(String Url) {


        System.out.println("-------------WalletMoneyPage1 Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------WalletMoneyPage1 Response----------------" + response);

                String Sstatus = "", Scurrency_code = "", Scurrentbalance = "", Str_currentMonthCredit = "", Str_currentMonthDebit = "", Str_monthYear = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            Scurrentbalance = response_object.getString("current_balance");
                            Scurrency_code = CurrencySymbolConverter.getCurrencySymbol(response_object.getString("currency"));
                        }
                    } else {
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, ReloadXenditPaymentSuccessActivity.class);
                        intent1.putExtra("amount", Scurrentbalance);
                        intent1.putExtra("ref_no", "comming soon");
                        intent1.putExtra("currency", Scurrency_code);
                        intent1.putExtra("flag","paypalpayment");
                        if(fromBookingpage.equalsIgnoreCase("1")){
                            intent1.putExtra("fromBookingpage", "1");
                        }
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                        session.createWalletAmount(Scurrency_code +" "+ Scurrentbalance);

                    } else {

                        String Sresponse = object.getString("response");
                        Alert1(getResources().getString(R.string.alert_label_title), Sresponse, 1);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {

            }
        });
    }

    //--------------Alert Method-----------
    private void Alert1(String title, String alert, final int flag) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyTransferUrlProgress.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewCloudMoneyTransferUrlProgress.this, WalletMoneyPage1.class);
                if(fromBookingpage.equalsIgnoreCase("1")){
                    intent.putExtra("fromBookingpage", "1");
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                if (flag == 0) {

                } else if (flag == 1) {

                } else if (flag == 2) {

                } else if (flag == 3) {

                }

                mDialog.dismiss();

            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyTransferUrlProgress.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog.dismiss();

            }
        });
        mDialog.show();
    }
    //--------------Alert Method-----------
    private void Alert1(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyTransferUrlProgress.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }



}
