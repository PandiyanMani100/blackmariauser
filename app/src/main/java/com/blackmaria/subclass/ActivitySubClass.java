package com.blackmaria.subclass;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PowerManager;

import com.blackmaria.FareBreakUp;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.Navigation_new;
import com.blackmaria.R;
import com.blackmaria.iconstant.Iconstant;


/**
 */
public class ActivitySubClass extends ActivityHockeyApp {
    BroadcastReceiver receiver;
    protected PowerManager.WakeLock mWakeLock;



    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.pushnotification.finish.trackyourRide");
        filter.addAction("com.pushnotification.finish.PushNotificationAlert");
        filter.addAction("com.pushnotification.finish.TimerPage");
        filter.addAction("com.pushnotification.finish.FareBreakUp");
        filter.addAction("com.pushnotification.finish.FareBreakUpPaymentList");
        filter.addAction("com.pushnotification.finish.MyRidePaymentList");
        filter.addAction("com.pushnotification.finish.MyRideDetails");
        filter.addAction("com.pushnotification.updateBottom_view");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("com.pushnotification.finish.trackyourRide")) {
                    finish();
                } else if (intent.getAction().equals("com.pushnotification.finish.PushNotificationAlert")) {
                    finish();
                } else if (intent.getAction().equals("com.pushnotification.finish.TimerPage")) {
                    finish();
                } else if (intent.getAction().equals("com.pushnotification.finish.FareBreakUp")) {
                    finish();
                } else if (intent.getAction().equals("com.pushnotification.finish.FareBreakUpPaymentList")) {
                    finish();
                } else if (intent.getAction().equals("com.pushnotification.finish.MyRidePaymentList")) {
                    finish();
                    System.out.print("caperson xmpp sucess");
                } else if (intent.getAction().equals("com.pushnotification.finish.MyRideDetails")) {
                    finish();
                    System.out.print("casperson2 xmpp sucess ");
                }else  if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                    if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                        String mRideStatus = (String) intent.getExtras().get("rideStatus");
                        String mRideid = (String) intent.getExtras().get("ride_id");


                        if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                            Intent intent1 = new Intent(ActivitySubClass.this, Navigation_new.class);
                            startActivity(intent1);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                            Intent intent1 = new Intent(ActivitySubClass.this, FareBreakUp.class);
                            intent1.putExtra("RideID", mRideid);
                            intent1.putExtra("ratingflag", "1");
                            startActivity(intent1);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        }else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus) )
                        {
                            Intent intent1 = new Intent(ActivitySubClass.this, FareBreakUp.class);
                            intent1.putExtra("RideID", mRideid);
                            intent1.putExtra("ratingflag", "2");
                            startActivity(intent1);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        }
                    }
                }
            }
        };
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        this.mWakeLock.acquire();
        registerReceiver(receiver, filter);
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(receiver);
        this.mWakeLock.release();
        super.onDestroy();
    }

}
