package com.blackmaria;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;

import java.util.HashMap;

/**
 * Created by GANESH on 28-08-2017.
 */

public class LegalPageNew extends ActivityHockeyApp implements View.OnClickListener {

    private TextView Tv_privacyPolicy, Tv_termsOfService, Tv_endUserAgreement, RL_facebookPolicy, anysuggestions, sendfeedback;
    private ImageView RL_home;

    private ConnectionDetector cd;
    private SessionManager sessionManager;

    private String sDriveID = "", sSupportNumber = "";
    final int PERMISSION_REQUEST_CODE = 111;
    private String Ssupport_no = "";
    LanguageDb mhelper;
    private AppInfoSessionManager appInfoSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.termspage_contrain);
        mhelper = new LanguageDb(this);
        initialize();

    }

    private void initialize() {
        appInfoSessionManager = new AppInfoSessionManager(LegalPageNew.this);
        sessionManager = new SessionManager(LegalPageNew.this);
        cd = new ConnectionDetector(LegalPageNew.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_USERID);
        sSupportNumber = sessionManager.KEY_PHONENO;

        HashMap<String, String> app = appInfoSessionManager.getAppInfo();
        Ssupport_no = app.get(appInfoSessionManager.KEY_CUSTOMER_NUMBER);

        RL_home = findViewById(R.id.RL_home);
        Tv_privacyPolicy = (TextView) findViewById(R.id.txt_label_privacy_policy);
        Tv_privacyPolicy.setText(getkey("firsttime_login_privacy"));
        Tv_termsOfService = (TextView) findViewById(R.id.txt_label_terms_of_service);
        Tv_endUserAgreement = (TextView) findViewById(R.id.txt_label_end_user_agreement);
        Tv_endUserAgreement.setText(getkey("label_eula"));
        RL_facebookPolicy = findViewById(R.id.txt_label_facebook_policy);
        RL_facebookPolicy.setText(getkey("legal_page_label_facebook_policy"));
        anysuggestions = findViewById(R.id.anysuggestions);
        anysuggestions.setText(getkey("any_suggestions"));
        sendfeedback = findViewById(R.id.sendfeedback);
        sendfeedback.setText(getkey("send_feedback"));

        TextView terms = findViewById(R.id.terms);
        terms.setText(getkey("terms_lable"));


        Tv_termsOfService.setText(getkey("legal_page_label_terms_of_service"));
//        Iv_call = (ImageView) findViewById(R.id.img_call);

        RL_home.setOnClickListener(this);
        Tv_privacyPolicy.setOnClickListener(this);
        Tv_termsOfService.setOnClickListener(this);
        Tv_endUserAgreement.setOnClickListener(this);
        RL_facebookPolicy.setOnClickListener(this);
        sendfeedback.setOnClickListener(this);
        anysuggestions.setOnClickListener(this);
//        Iv_call.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if (view == RL_home) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == Tv_privacyPolicy) {
//            Intent intent = new Intent(LegalPageNew.this, LegalDynamicContent.class);
//            intent.putExtra("pageType", "privacy");
//            intent.putExtra("pageNo", "1");
//            startActivity(intent);
//            overridePendingTransition(R.anim.enter, R.anim.exit);
            String url = Iconstant.privacyurl;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if (view == Tv_termsOfService) {
//            Intent intent = new Intent(LegalPageNew.this, LegalDynamicContent.class);
//            intent.putExtra("pageType", "termsof-service");
//            intent.putExtra("pageNo", "2");
//            startActivity(intent);
            String url = Iconstant.termsurl;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);

//            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Tv_endUserAgreement) {
//            Intent intent = new Intent(LegalPageNew.this, LegalDynamicContent.class);
//            intent.putExtra("pageType", "user-agreement");
//            intent.putExtra("pageNo", "3");
//            startActivity(intent);
//            overridePendingTransition(R.anim.enter, R.anim.exit);
            String url = Iconstant.enduserurl;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);

        } else if (view == RL_facebookPolicy) {
//            Intent intent = new Intent(LegalPageNew.this, LegalDynamicContent.class);
//            intent.putExtra("pageType", "facebook-policy");
//            intent.putExtra("pageNo", "4");
//            startActivity(intent);
//            overridePendingTransition(R.anim.enter, R.anim.exit);
            String url = Iconstant.facebookurl;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);

        } else if (view == sendfeedback) {
            sendEmail();

        } else if (view == anysuggestions) {


        } /*else if (view == Iv_call) {
            getSupportPopup();
        }*/
    }

    //----------Method to Send Email--------
    protected void sendEmail() {
        appInfoSessionManager = new AppInfoSessionManager(LegalPageNew.this);
        HashMap<String, String> appInfo = appInfoSessionManager.getAppInfo();
        String toAddress = appInfo.get(AppInfoSessionManager.KEY_CONTACT_EMAIL);

        String[] TO = {toAddress};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Message");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(LegalPageNew.this, getkey("thers_is_no"), Toast.LENGTH_SHORT).show();
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
