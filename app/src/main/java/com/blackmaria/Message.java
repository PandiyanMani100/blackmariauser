package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.MessageListviewAdapter;
import com.blackmaria.hockeyapp.ActionBarActivityHockeyApp;
import com.blackmaria.pojo.MessagePojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class Message extends ActionBarActivityHockeyApp {
    MessageListviewAdapter adapter;
    private String[] Title;
    private ListView mMessageList;
    RelativeLayout messageNavigationBackIcon;
    ImageView messageHome,Iv_previous,Iv_next;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private ServiceRequest mRequest;
    ArrayList<MessagePojo> itemlist_all;
    String perPage = "5";
    private int currentPage = 1;
    private String UserID = "";
    TextView empty_text;
    private boolean isTransactionAvailable = false;
    String[] mStrings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_new);
        inilialize();
        messageNavigationBackIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(Message.this, Navigation_new.class);
                startActivity(intent);*/
                overridePendingTransition(R.anim.exit, R.anim.enter);
                finish();
            }
        });

        messageHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Message.this, Navigation_new.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit, R.anim.enter);
                finish();
            }
        });
        Iv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Iv_previous.setVisibility(View.VISIBLE);
                currentPage = currentPage + 1;
                postData();
            }
        });

        Iv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage - 1;
                postData();

            }
        });

        mMessageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("===============message selected postion================"+position);
                MessagePopUp(position);
            }
        });



    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
        System.out.println("************ GC Memory cleared***********");
    }



    private void inilialize() {
        session = new SessionManager(Message.this);
        cd = new ConnectionDetector(Message.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemlist_all = new ArrayList<MessagePojo>();

        mMessageList = (ListView) findViewById(R.id.message_listview);
        messageNavigationBackIcon = (RelativeLayout) findViewById(R.id.message_navigation_icon_layout);
        messageHome = (ImageView) findViewById(R.id.message_bottom_laypout_home);
        empty_text= (TextView) findViewById(R.id.emptytext);
        Iv_previous= (ImageView) findViewById(R.id.message_bottom_layout_prev);
        Iv_next=(ImageView) findViewById(R.id.message_bottom_layout_next);
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        postData();
    }






    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", UserID);
        jsonParams.put("type", "message");
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        if (isInternetPresent) {
            postRequest_walletMoney(Iconstant.mesggaeg_url, jsonParams);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

        }
    }
    //-----------------------wallet Money Post Request-----------------
    private void postRequest_walletMoney(String Url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(Message.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("-------------Message  Url----------------" + Url);


        mRequest = new ServiceRequest(Message.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Message  Response----------------" + response);

                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "",ScurrencySymbol="",Str_total_amount="",Str_total_transaction="";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");

                        if (response_object.length() > 0) {


                            Object check_trans_object = response_object.get("notification");
                            itemlist_all.clear();
                            mStrings=null;
                            if (check_trans_object instanceof JSONArray) {
                                JSONArray trans_array = response_object.getJSONArray("notification");
                                mStrings = new String[trans_array.length()];
                                if (trans_array.length() > 0) {
                                    for (int i = 0; i < trans_array.length(); i++) {
                                        JSONObject trans_object = trans_array.getJSONObject(i);
                                        MessagePojo pojo = new MessagePojo();
                                        pojo.setDate(trans_object.getString("date"));
                                        pojo.setIMage(trans_object.getString("image"));
                                        pojo.setTitle(trans_object.getString("title"));
                                        pojo.setDescription(trans_object.getString("description"));
                                        pojo.setNotification_id(trans_object.getString("notification_id"));
                                        mStrings[i] = trans_object.getString("title");
                                        itemlist_all.add(pojo);
                                    }
                                    isTransactionAvailable = true;
                                } else {
                                    isTransactionAvailable = false;
                                    Iv_next.setVisibility(View.GONE);
                                }
                            } else {
                                isTransactionAvailable = false;
                                Iv_next.setVisibility(View.GONE);
                            }
                            currentPage = Integer.parseInt(response_object.getString("current_page"));
                            str_NextPage = response_object.getString("next_page");
                            if(str_NextPage.equalsIgnoreCase("")){
                                Iv_next.setVisibility(View.GONE);
                            }else{
                                Iv_next.setVisibility(View.VISIBLE);
                            }
                            perPage = response_object.getString("perPage");
                        }
                    }

                    if (currentPage == 1) {
                        Iv_previous.setVisibility(View.GONE);
                    } else {
                        Iv_previous.setVisibility(View.VISIBLE);
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (isTransactionAvailable) {
                            adapter = new MessageListviewAdapter(Message.this, mStrings,0,itemlist_all);
                            mMessageList.setAdapter(adapter);
                          //  Iv_next.setVisibility(View.VISIBLE);
                        } else {
                            empty_text.setVisibility(View.VISIBLE);
                            mMessageList.setVisibility(View.GONE);
                          //  Iv_next.setVisibility(View.GONE);
                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }


            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

    }




    private void MessagePopUp(int postion) {
        final Dialog dialog = new Dialog(Message.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.message_popup_new);
        //RoundedImageView messageimage=(RoundedImageView)dialog.findViewById(R.id.messageimage);
        CustomTextView title = (CustomTextView) dialog.findViewById(R.id.title);
        //CustomTextView date = (CustomTextView) dialog.findViewById(R.id.date);
        CustomTextView description = (CustomTextView) dialog.findViewById(R.id.description);
        ImageView Ok_Profilechange = (ImageView) dialog.findViewById(R.id.ok_profilechange);
        ImageView   message_image= (ImageView) dialog.findViewById(R.id.message_image);
        title.setText(itemlist_all.get(postion).getTitle().toUpperCase());
      //  date.setText(itemlist_all.get(postion).getDate());
        description.setText(itemlist_all.get(postion).getDescription().toUpperCase());
        Picasso.with(Message.this).load(String.valueOf(itemlist_all.get(postion).getIMage())).into(message_image);
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog!=null){
                    dialog.dismiss();
                }

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));



        dialog.show();
    }



    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Message.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    //-----------------------Favourite Save Post Request-----------------

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.exit, R.anim.enter);
        finish();
    }
}