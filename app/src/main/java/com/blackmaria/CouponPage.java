package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.CouponAdapter;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.pojo.CouponPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * Created by user144 on 12/2/2016.
 */

public class  CouponPage extends FragmentActivityHockeyApp implements CouponAdapter.CouponClickInterface {

    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    LanguageDb mhelper;

//    private ExpandableHeightGridView Gv_coupons;

    private ListView Gv_coupons;
    ImageView /*next_btn,prev_btn,*/home_btn,menu_btn;
    CustomTextView no_coupancode;

    CouponAdapter couponAdapter;
    private ArrayList<CouponPojo> coupon_itemList;
    private RelativeLayout Rl_back, Rl_coupon_list;
    private CustomTextView Tv_have_coupon, Tv_claim;
    private CustomEdittext Et_coupon_code;
    private SessionManager session;
    private String UserID = "";
    Dialog dialog;
    private ServiceRequest mRequest;
    private String ScurrencySymbol = "";
    private boolean couponStatus = false;
    public static String Spage = "";
    private SimpleDateFormat coupon_mFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private String ScouponSource = "";
    private RefreshReceiver refreshReceiver;

    private int currentPage = 1;
    private String perPage = "3";
    private String nextPage;




    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(Fragment_HomePage.this, Fragment_HomePage.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        if (getkey("profile_label_menu").equalsIgnoreCase(Spage)){
                        }else{
                            BookingPage3.BookingPage3_class.finish();
                        }

                        Intent intent1 = new Intent(CouponPage.this, MyRideRating.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus) ){
                        if (getkey("profile_label_menu").equalsIgnoreCase(Spage)){
                        }else{
                            BookingPage3.BookingPage3_class.finish();
                        }
                        Intent intent1 = new Intent(CouponPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coupenlist_constrain);
        mhelper = new LanguageDb(this);
        initiaclize();







        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


            }
        });




    }

    private void initiaclize() {
        session = new SessionManager(CouponPage.this);
        cd = new ConnectionDetector(CouponPage.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        Intent intent = getIntent();
        if (intent.hasExtra("str_page")) {
            Spage = intent.getStringExtra("str_page");
        }

        coupon_itemList = new ArrayList<CouponPojo>();

//        next_btn=(ImageView)findViewById(R.id.coupon_msg_next);
//        prev_btn=(ImageView)findViewById(R.id.coupon_msg_perv);
        menu_btn=findViewById(R.id.coupon_menu_ic);
        no_coupancode=findViewById(R.id.no_couponcode_available);
        no_coupancode.setText(mhelper.getvalueforkey("no_coupon_available"));

        TextView selecttouser=findViewById(R.id.selecttouser);
        selecttouser.setText(getkey("selecttouse"));

        Gv_coupons=findViewById(R.id.coupon_code_page_coupon_list_gridview);


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);


        if (isInternetPresent) {
            PostRequest_couponList(Iconstant.get_coupon_list_url);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));
        }


    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(CouponPage.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    private void PostRequest_couponList(String Url) {
        dialog = new Dialog(CouponPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        System.out.println("-----------couponList jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(CouponPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------couponList reponse-------------------" + response);

                String Sstatus = "", Smessage = "", SuserName = "", Semail = "", Sgender = "", Sage = "", SstreetName = "", Sdistrict = "",
                        Sstate = "", Spincode = "", Slocality = "", sCountryCode = "", SmobNo = "", suserimage = "", Scurrencycode="",next_page="";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject jsonObject = object.getJSONObject("response");
                       next_page=jsonObject.getString("next_page");
//                        if(next_page.equalsIgnoreCase("")){
//                            next_btn.setVisibility(View.GONE);
//                        }else{
//                            next_btn.setVisibility(View.VISIBLE);
//                        }
                        Object check_object = jsonObject.get("coupen_list");

                        currentPage = Integer.parseInt(jsonObject.getString("current_page"));
                        nextPage = jsonObject.getString("next_page");

                        if (check_object instanceof JSONArray) {

                            JSONArray coupen_list_jsonArray = jsonObject.getJSONArray("coupen_list");
                            if (coupen_list_jsonArray.length() > 0) {
                                couponStatus = true;
                                coupon_itemList.clear();
                                for (int j = 0; j < coupen_list_jsonArray.length(); j++) {
                                    JSONObject coupon_object = coupen_list_jsonArray.getJSONObject(j);
                                    CouponPojo couponPojo = new CouponPojo();
                                    couponPojo.setCouponCode(coupon_object.getString("code"));
                                    couponPojo.setExpiryDate(coupon_object.getString("valid_to"));
                                    couponPojo.setCouponType(coupon_object.getString("discount_type"));
                                    couponPojo.setCouponValue(coupon_object.getString("discount_amount"));
//                                    ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(jsonObject.getString("currencyCode"));
//                                    Scurrencycode=jsonObject.getString("currencyCode");
                                    couponPojo.setCurrencyCode(coupon_object.getString("currency_code"));
                                    couponPojo.setSource(coupon_object.getString("source"));
                                    coupon_itemList.add(couponPojo);

                                }

                            } else {
                                couponStatus = false;
                                coupon_itemList.clear();
                            }
                        }else{

                            couponStatus = false;
                            coupon_itemList.clear();

                        }


                        dialogDismiss();

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {


//                    if (currentPage == 1) {
//                        prev_btn.setVisibility(View.GONE);
//                    } else {
//                        prev_btn.setVisibility(View.VISIBLE);
//                    }
                    //next_btn.setVisibility(View.VISIBLE);


                    if (couponStatus) {
                        Gv_coupons.setVisibility(View.VISIBLE);
                        no_coupancode.setVisibility(View.GONE);
                        couponAdapter = new CouponAdapter(CouponPage.this, coupon_itemList,CouponPage.this);
                        Gv_coupons.setAdapter(couponAdapter);
                        couponAdapter.notifyDataSetChanged();

                    } else {
                        Gv_coupons.setVisibility(View.GONE);
                        no_coupancode.setVisibility(View.VISIBLE);

                    }

                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }



    private void PostRequest_ApplyCoupon(String Url,  final HashMap<String, String> jsonParams) {
        dialog = new Dialog(CouponPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------ApplyCoupon jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(CouponPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------ApplyCoupon reponse-------------------" + response);

                String Sstatus = "", Smessage = "", Scode = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if ("1".equalsIgnoreCase(Sstatus)) {
                        JSONObject jsonObject = object.getJSONObject("response");
                        Scode = jsonObject.getString("code");
                        Smessage = jsonObject.getString("message");

                    }else{
                        Smessage = object.getString("response");
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {


                    if (getkey("profile_label_menu").equalsIgnoreCase(Spage)){

                        session.setCouponCode(Scode,ScouponSource);

                    }else{
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("couponSource", ScouponSource);
                        returnIntent.putExtra("couponCode", Scode);
                        setResult(RESULT_OK, returnIntent);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }

//

                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }



    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(CouponPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
    @Override
    public void onSelectClick(String code, String source) {
        System.out.println("=========code source=========="+code+source);
        String selectedDate = coupon_mFormatter.format(new Date());
        ScouponSource = source;//coupon_itemList.get(position).getSource().toString();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("code", code);
        jsonParams.put("pickup_date", selectedDate);
        jsonParams.put("source", ScouponSource);
        if (isInternetPresent) {
            if (getkey("profile_label_menu").equalsIgnoreCase(Spage)){
            }else {
                PostRequest_ApplyCoupon(Iconstant.apply_coupon_code_url, jsonParams);
            }
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
