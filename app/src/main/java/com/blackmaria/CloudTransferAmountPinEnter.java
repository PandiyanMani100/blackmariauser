package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CircularImageView;
import com.blackmaria.widgets.CustomEdittextCambrialItalic;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.TextRCLight;
import com.devspark.appmsg.AppMsg;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by user129 on 2/22/2018.
 */
@SuppressLint("Registered")
public class CloudTransferAmountPinEnter extends ActivityHockeyApp implements View.OnClickListener {

    private CircularImageView profile_img, dig_profile;
    private CustomTextView Tv_name, Tv_mobileno, Tv_current_city;
    private CustomEdittextCambrialItalic Ed_amount, Ed_pin;
    private ImageButton Imb_one, Imb_two, Imb_three, Imb_four, Imb_five, Imb_six, Imb_seven, Imb_tight, Imb_nine, Imb_aero, Imb_minus, Imb_plus;
    private ImageButton Imb_cancel, Imb_enter, Imb_forgetpin, Imb_clear;
    private LinearLayout Ky_bc1, Ky_bc2;
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String sSecurePin = "";
    private String UserID = "", numberamount = "", numberpin = "", user_name = "", user_img = "", user_city = "", user_satus = "", friend_id = "", strCrossBorderStatus = "", mobile_number = "";
    private boolean isAmountEnable = true;
    private boolean isPinEnable = false;

    String status = "", respose = "", user_image = "", city = "", user_status = "", transfer_amount = "", transfer_fee = "", debit_amount = "", currency_transfer = "", received_amount = "", currency_received = "", conversion_rate = "",transfer_fee_percentage="";

    private LinearLayout cross_value;
    private TextRCLight userNameTv1, userCityTv1, userStatusTv1, transfer_amount_tv, transferfee_tv, debit_amount_tv, received_amount_tv, conversion_tv, currency_conversion, tv_transfer_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cloud_transfer_amount_pin1);
        initialize();

    }

    private void initialize() {

        session = new SessionManager(CloudTransferAmountPinEnter.this);
        cd = new ConnectionDetector(CloudTransferAmountPinEnter.this);
        isInternetPresent = cd.isConnectingToInternet();
        sSecurePin = session.getSecurePin();
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);


        Intent in = getIntent();
        user_name = in.getStringExtra("user_name");
        user_img = in.getStringExtra("user_image");
        user_city = in.getStringExtra("city");
        user_satus = in.getStringExtra("status1");
        friend_id = in.getStringExtra("friendId");
        strCrossBorderStatus = in.getStringExtra("strCrossBorderStatus");
        mobile_number = in.getStringExtra("mobileno");

        Log.d("strCrossBorderStatus", strCrossBorderStatus);


        profile_img = (CircularImageView) findViewById(R.id.user_img);
        Tv_name = (CustomTextView) findViewById(R.id.user_name_tv);
        Tv_mobileno = (CustomTextView) findViewById(R.id.user_mobile_no);
        Tv_current_city = (CustomTextView) findViewById(R.id.user_current_city);
        Ed_amount = (CustomEdittextCambrialItalic) findViewById(R.id.trf_insert_amount);
        Ed_pin = (CustomEdittextCambrialItalic) findViewById(R.id.trf_enter_pin);
        Imb_one = (ImageButton) findViewById(R.id.btn_one);
        Imb_two = (ImageButton) findViewById(R.id.btn_two);
        Imb_three = (ImageButton) findViewById(R.id.btn_three);
        Imb_four = (ImageButton) findViewById(R.id.btn_four);
        Imb_five = (ImageButton) findViewById(R.id.btn_five);
        Imb_six = (ImageButton) findViewById(R.id.btn_six);
        Imb_seven = (ImageButton) findViewById(R.id.btn_seven);
        Imb_tight = (ImageButton) findViewById(R.id.btn_tight);
        Imb_nine = (ImageButton) findViewById(R.id.btn_nine);
        Imb_aero = (ImageButton) findViewById(R.id.btn_zero);
        Imb_minus = (ImageButton) findViewById(R.id.btn_minus);
        Imb_plus = (ImageButton) findViewById(R.id.btn_pluse);
        Imb_cancel = (ImageButton) findViewById(R.id.btn_cancel);
        Imb_clear = (ImageButton) findViewById(R.id.btn_clear);
        Imb_forgetpin = (ImageButton) findViewById(R.id.btn_forget);
        Imb_enter = (ImageButton) findViewById(R.id.btn_enter);
        Ky_bc1 = (LinearLayout) findViewById(R.id.keyborder_bc1);
        Ky_bc2 = (LinearLayout) findViewById(R.id.keyborder_bc2);




        Imb_one.setOnClickListener(this);
        Imb_two.setOnClickListener(this);
        Imb_three.setOnClickListener(this);
        Imb_four.setOnClickListener(this);
        Imb_five.setOnClickListener(this);
        Imb_six.setOnClickListener(this);
        Imb_seven.setOnClickListener(this);
        Imb_tight.setOnClickListener(this);
        Imb_nine.setOnClickListener(this);
        Imb_aero.setOnClickListener(this);
        Imb_cancel.setOnClickListener(this);
        Imb_clear.setOnClickListener(this);
        Imb_forgetpin.setOnClickListener(this);
        Imb_enter.setOnClickListener(this);
        Ed_amount.setOnClickListener(this);
        Ed_pin.setOnClickListener(this);
        Ky_bc1.setOnClickListener(this);
        Ky_bc2.setOnClickListener(this);


        if (!user_img.equalsIgnoreCase("")) {
            Picasso.with(CloudTransferAmountPinEnter.this).load(String.valueOf(user_img)).error(R.drawable.coud_icn_man).into(profile_img);
        }
        Tv_name.setText(user_name);
        Tv_mobileno.setText(mobile_number);
        Tv_current_city.setText(user_city);


        /*Ed_amount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                CloseKeyboard(Ed_amount);
                if (hasFocus) {

                    isAmountEnable = true;
                    isPinEnable = false;

                }


            }
        });


        Ed_pin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {


                    CloseKeyboard(Ed_pin);
                    isAmountEnable = false;
                    isPinEnable = true;

                }

            }
        });*/


    }

    @Override
    public void onClick(View v) {

        if (v == Ed_amount) {

            CloseKeyboard(Ed_amount);

            isAmountEnable = true;
            isPinEnable = false;

            System.out.println("--------kannan jjj-----------");


        } else if (v == Ed_pin) {


            System.out.println("-------kannan--------");

            CloseKeyboard(Ed_pin);

            isAmountEnable = false;
            isPinEnable = true;

        } else if (v == Ky_bc1) {

            CloseKeyboard(Ed_amount);

            isAmountEnable = true;
            isPinEnable = false;


        } else if (v == Ky_bc2) {

            CloseKeyboard(Ed_pin);

            isAmountEnable = false;
            isPinEnable = true;


        } else if (v == Imb_one) {


            if (isAmountEnable) {

                numberamount = numberamount + "1";
                Ed_amount.setText(numberamount);

            } else if (isPinEnable) {

                numberpin = numberpin + "1";
                Ed_pin.setText(numberpin);
            }


        } else if (v == Imb_two) {


            if (isAmountEnable) {
                numberamount = numberamount + "2";
                Ed_amount.setText(numberamount);

            } else if (isPinEnable) {
                numberpin = numberpin + "2";
                Ed_pin.setText(numberpin);
            }


        } else if (v == Imb_three) {


            if (isAmountEnable) {
                numberamount = numberamount + "3";
                Ed_amount.setText(numberamount);

            } else if (isPinEnable) {
                numberpin = numberpin + "3";
                Ed_pin.setText(numberpin);
            }


        } else if (v == Imb_four) {


            if (isAmountEnable) {
                numberamount = numberamount + "4";
                Ed_amount.setText(numberamount);

            } else if (isPinEnable) {
                numberpin = numberpin + "4";
                Ed_pin.setText(numberpin);
            }


        } else if (v == Imb_five) {


            if (isAmountEnable) {
                numberamount = numberamount + "5";
                Ed_amount.setText(numberamount);

            } else if (isPinEnable) {
                numberpin = numberpin + "5";
                Ed_pin.setText(numberpin);
            }

        } else if (v == Imb_six) {


            if (isAmountEnable) {
                numberamount = numberamount + "6";
                Ed_amount.setText(numberamount);

            } else if (isPinEnable) {
                numberpin = numberpin + "6";
                Ed_pin.setText(numberpin);
            }

        } else if (v == Imb_seven) {


            if (isAmountEnable) {
                numberamount = numberamount + "7";
                Ed_amount.setText(numberamount);

            } else if (isPinEnable) {
                numberpin = numberpin + "7";
                Ed_pin.setText(numberpin);
            }

        } else if (v == Imb_tight) {

            if (isAmountEnable) {
                numberamount = numberamount + "8";
                Ed_amount.setText(numberamount);

            } else if (isPinEnable) {
                numberpin = numberpin + "8";
                Ed_pin.setText(numberpin);
            }


        } else if (v == Imb_nine) {


            if (isAmountEnable) {
                numberamount = numberamount + "9";
                Ed_amount.setText(numberamount);

            } else if (isPinEnable) {
                numberpin = numberpin + "9";
                Ed_pin.setText(numberpin);
            }


        } else if (v == Imb_aero) {

            if (isAmountEnable) {
                numberamount = numberamount + "0";
                Ed_amount.setText(numberamount);

            } else if (isPinEnable) {
                numberpin = numberpin + "0";
                Ed_pin.setText(numberpin);
            }


        } else if (v == Imb_minus) {


        } else if (v == Imb_plus) {


        } else if (v == Imb_cancel) {


            AlertCancel(getResources().getString(R.string.are_you_to_cancels), getResources().getString(R.string.tap_to_confirm));


        } else if (v == Imb_clear) {


            if (isAmountEnable) {

                String Samount = Ed_amount.getText().toString();
                if (Samount != null && Samount.length() > 0) {
                    Ed_amount.setText(Samount.substring(0, Samount.length() - 1));
                    Ed_amount.setSelection(Ed_amount.getText().length());
                    numberamount = Ed_amount.getText().toString();
                }


            } else if (isPinEnable) {

                String Spin = Ed_pin.getText().toString();
                if (Spin != null && Spin.length() > 0) {
                    Ed_pin.setText(Spin.substring(0, Spin.length() - 1));
                    Ed_pin.setSelection(Ed_pin.getText().length());
                    numberpin = Ed_pin.getText().toString();
                }


            }


        } else if (v == Imb_forgetpin) {

            if (cd.isConnectingToInternet()) {
                ForgotPinRequest(Iconstant.forgot_pin_url);
            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }


//            Alert("", "PIN CODE HAS BEEN SENT Please check your email");


        } else if (v == Imb_enter) {

            try {
                String pin = Ed_pin.getText().toString().trim();
                int amount = 0;
                if (!Ed_amount.getText().toString().equalsIgnoreCase("")) {
                    amount = Integer.parseInt(Ed_amount.getText().toString().trim());
                }
                if (Ed_amount.getText().toString().trim().length() <= 0) {
//                erroredit(Ed_amount, getResources().getString(R.string.insert_transfer_amount));
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.insert_transfer_amount));
                } else if (Ed_amount.getText().toString().trim().equalsIgnoreCase(".")) {
//                erroredit(Ed_amount, getResources().getString(R.string.insert_transfer_amount));
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.insert_transfer_amount));
                } else if (!sSecurePin.equalsIgnoreCase(pin)) {
//                erroredit(sSecurePin, getResources().getString(R.string.label_incorrect_pin));
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.label_incorrect_pin));
                } else if (amount <= 0) {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.cont_transfer_amount));
                } else {
                    if (WalletMoneyPage1.Scurrency != null)
                        AlertEnter(getResources().getString(R.string.send_money) + " " + WalletMoneyPage1.Scurrency + " " + Ed_amount.getText().toString() + "?", getResources().getString(R.string.pls_tab_to_confirm));
                }
            }catch (NumberFormatException e){

            }catch (Exception e){

            }
        }

    }


    private void ConfirmTransferPopUp(Boolean iscrosstrue) {
        final Dialog dialog = new Dialog(CloudTransferAmountPinEnter.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloud_transfer_confirm_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RelativeLayout sendNow = (RelativeLayout) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        RelativeLayout cancel_btn = (RelativeLayout) dialog.findViewById(R.id.cancel_btn);

        cross_value = (LinearLayout) dialog.findViewById(R.id.cross_value);

        userNameTv1 = (TextRCLight) dialog.findViewById(R.id.user_name_tv);
        userCityTv1 = (TextRCLight) dialog.findViewById(R.id.user_city_tv);
        userStatusTv1 = (TextRCLight) dialog.findViewById(R.id.user_status_tv);

        transfer_amount_tv = (TextRCLight) dialog.findViewById(R.id.transfer_amount_tv);
        transferfee_tv = (TextRCLight) dialog.findViewById(R.id.transferfee_tv);
        debit_amount_tv = (TextRCLight) dialog.findViewById(R.id.debit_amount_tv);

        received_amount_tv = (TextRCLight) dialog.findViewById(R.id.received_amount_tv);
        conversion_tv = (TextRCLight) dialog.findViewById(R.id.conversion_tv);
        currency_conversion = (TextRCLight) dialog.findViewById(R.id.currency_conversion);
        tv_transfer_title = (TextRCLight) dialog.findViewById(R.id.trs_type_title);
        dig_profile = (CircularImageView) dialog.findViewById(R.id.user_img);

        Picasso.with(CloudTransferAmountPinEnter.this).load(String.valueOf(user_img)).error(R.drawable.coud_icn_man).into(dig_profile);

        userNameTv1.setText(user_name);
        userCityTv1.setText(mobile_number);
        userStatusTv1.setText(user_city);


        if (iscrosstrue) {
            cross_value.setVisibility(View.VISIBLE);
            tv_transfer_title.setText(getResources().getString(R.string.cross_border_rtrasn));
            if (!transfer_fee.toString().equals("")) {
                double amount = Double.parseDouble(transfer_fee);
                double res = (amount / 100.0f) * 10;
                transferfee_tv.setText(currency_transfer + " " + transfer_fee+"("+transfer_fee_percentage+"%)");
            }
            transfer_amount_tv.setText(currency_transfer + " " + transfer_amount);
            debit_amount_tv.setText(currency_transfer + " " + debit_amount);
            received_amount_tv.setText(currency_received + " " + received_amount);
            conversion_tv.setText("1 "+currency_transfer+"="+conversion_rate + " " + currency_received);

        } else {
            cross_value.setVisibility(View.GONE);
            tv_transfer_title.setText(getResources().getString(R.string.local_trans));
        }


        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        sendNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(CloudTransferAmountPinEnter.this, NewCloudMoneyTransferUrlProgress.class);
                intent.putExtra("flag", "0");
                intent.putExtra("reloadamount", Ed_amount.getText().toString().trim());
                intent.putExtra("friendId", friend_id);
                intent.putExtra("crosstransfer", strCrossBorderStatus);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    private void crossBorderCheck(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(CloudTransferAmountPinEnter.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("--------------crossBorderCheck Url-------------------" + Url);
        System.out.println("--------------crossBorderCheck jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(CloudTransferAmountPinEnter.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener()

        {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------crossBorderCheck Response-------------------" + response);
                String sResponse = "", wallet_amount = "", response1 = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        try {
                            status = object.getString("status");
                            if (status.equalsIgnoreCase("1")) {
                                respose = object.getString("response");
                                user_name = object.getString("user_name");
                                friend_id = object.getString("friend_id");
                                user_image = object.getString("user_image");
                                city = object.getString("city");
                                user_status = object.getString("user_status");

                                transfer_amount = object.getString("transfer_amount");
                                transfer_fee = object.getString("transfer_fee");
                                debit_amount = object.getString("debit_amount");
                                transfer_fee_percentage = object.getString("transfer_fee_percentage");

                                currency_transfer = object.getString("currency_transfer");
                                received_amount = object.getString("received_amount");
                                currency_received = object.getString("currency_received");
                                conversion_rate = object.getString("conversion_rate");
                            } else {
                                sResponse = object.getString("response");
                            }

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                    if (status.equalsIgnoreCase("1")) {


                        ConfirmTransferPopUp(true);

//                        cross_value.setVisibility(View.VISIBLE);
//                        userNameTv1.setText(user_name);
//                        userStatusTv1.setText(user_status);
//                        userCityTv1.setText(city);

//                        transfer_amount_tv.setText(currency_transfer+" "+transfer_amount);
//                        transferfee_tv.setText(currency_transfer+" "+transfer_fee);
//                        debit_amount_tv.setText(currency_transfer+" "+debit_amount);
//
//                        received_amount_tv.setText(currency_received+" "+received_amount);
//                        currency_conversion .setText("CONVERSION : 1"+currency_transfer);
//                        conversion_tv.setText(conversion_rate+" "+currency_received);

                    } else {
                        Alert(getResources().getString(R.string.action_error), sResponse);
                    }


                } catch (JSONException e)

                {
                    e.printStackTrace();
                }
                if (dialog != null)

                {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(CloudTransferAmountPinEnter.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudTransferAmountPinEnter.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog. setDialogUpper(false);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }


    //--------------Alert Method-----------
    private void AlertEnter(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudTransferAmountPinEnter.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog. setDialogUpper(false);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.setPositiveButton(getResources().getString(R.string.timer_label_alert_yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                if (strCrossBorderStatus.equalsIgnoreCase("1")) {

                    cd = new ConnectionDetector(CloudTransferAmountPinEnter.this);
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("user_id", UserID);
                        jsonParams.put("transfer_amount", Ed_amount.getText().toString().trim());
                        jsonParams.put("friend_id", friend_id);
                        crossBorderCheck(Iconstant.cloudmoney_crossbordervalue_Url, jsonParams);
                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }


                } else {

                    ConfirmTransferPopUp(false);


                }


            }
        });
        mDialog.show();
    }


    private void ForgotPinRequest(String Url) {

        dialog = new Dialog(CloudTransferAmountPinEnter.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);

        System.out.println("--------------ForgotPinRequest Url-------------------" + Url);
        System.out.println("--------------ForgotPinRequest jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(CloudTransferAmountPinEnter.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                System.out.println("--------------ForgotPinRequest Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("message");

                        if (status.equalsIgnoreCase("1")) {


                            Alert(getResources().getString(R.string.pin_sent), getResources().getString(R.string.pls_check_mail));
                        } else {
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //--------------Close KeyBoard Method-----------
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), 0);
    }


    //--------------Alert Method-----------
    private void AlertCancel(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudTransferAmountPinEnter.this);
        mDialog.setDialogTitle(title);
        mDialog. setDialogUpper(false);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(CloudTransferAmountPinEnter.this,WalletMoneyPage1.class);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        mDialog.show();
    }


    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(CloudTransferAmountPinEnter.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

}
