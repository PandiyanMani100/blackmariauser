package com.blackmaria;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextViewForm;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by Muruganantham on 12/28/2017.
 */

@SuppressLint("Registered")
public class NewProfile_Page_ProfileEdit extends ActivityHockeyApp implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private ServiceRequest mRequest;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private RefreshReceiver refreshReceiver;
    private SessionManager session;

    private RelativeLayout profileEditLyHeader;
    private ImageView cancelLayout;
    private CustomEdittext myprofileDialogFormNameEdittext;
    private Spinner profilePageGendeSpinner;
    private CustomTextViewForm profilePageFormAgeTextview;
    private CustomEdittext myprofileDialogFormAgeEdittext;
    private AutoCompleteTextView myprofileDialogFormAddressEdittext;
    private CustomEdittext myprofileDialogFormCityEdittext;
    private CustomEdittext myprofileDialogFormStateEdittext;
    private CustomEdittext myprofileDialogFormEmailEdittext, myprofileDialogFormCountryEdittext, myprofileDialogFormPicodeEdittext;
    private CheckBox agreeBox;
    private RelativeLayout userProfileUpdateBtn;
    private SmoothProgressBar profileLoadingProgressbar;


    private String sLatitude = "", sLongitude = "", sSelected_location = "";
    ArrayList<String> itemList_location = new ArrayList<String>();
    ArrayList<String> itemList_placeId = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    private Boolean isDatavailable = false;
    private boolean isDataAvailable = false;
    private boolean isAddressAvailable = false;

    AppInfoSessionManager appInfoSessionManager;
    private String UserID = "";
    private String gcmID = "";
    private String sAuthKey = "";

    String name="",email="",gender="",age="",address="",city="",state="",picode="",country;
    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewProfile_Page_ProfileEdit.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewProfile_Page_ProfileEdit.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            } else if (intent.getAction().equals("com.package.ACTION_CLASS_complaint_replay_notification")) {
                if (intent.getExtras() != null) {
                    String ticketId = (String) intent.getExtras().get("ticket_id");
                    String userID = (String) intent.getExtras().get("user_id");
                    String message = (String) intent.getExtras().get("message");
                    String dateTime = (String) intent.getExtras().get("date_time");

                    Intent intent1 = new Intent(NewProfile_Page_ProfileEdit.this, ComplaintPageViewDetailPage.class);
                    intent1.putExtra("ticket_id", ticketId);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_details_edit);

        initialize();
        OtherClickEvents();
    }


    private void initialize() {

        appInfoSessionManager=new AppInfoSessionManager(NewProfile_Page_ProfileEdit.this);
        session = new SessionManager(NewProfile_Page_ProfileEdit.this);
        cd = new ConnectionDetector(NewProfile_Page_ProfileEdit.this);
        isInternetPresent = cd.isConnectingToInternet();


        HashMap<String, String> info = session.getUserDetails();
        String tipstime = info.get(SessionManager.KEY_PHONENO);
        UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> app = appInfoSessionManager.getAppInfo();
        sAuthKey = app.get(appInfoSessionManager.KEY_APP_IDENTITY);

        HashMap<String, String> user = session.getUserDetails();
        gcmID = user.get(SessionManager.KEY_GCM_ID);


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);


        profileEditLyHeader = (RelativeLayout) findViewById(R.id.profile_edit_ly_header);
        cancelLayout = (ImageView) findViewById(R.id.cancel_layout);
        myprofileDialogFormNameEdittext = (CustomEdittext) findViewById(R.id.myprofile_dialog_form_name_edittext);
        profilePageGendeSpinner = (Spinner) findViewById(R.id.profile_page_gende_spinner);
        profilePageFormAgeTextview = (CustomTextViewForm) findViewById(R.id.profile_page_form_age_textview);
        myprofileDialogFormAgeEdittext = (CustomEdittext) findViewById(R.id.myprofile_dialog_form_age_edittext);
        myprofileDialogFormAddressEdittext = (AutoCompleteTextView) findViewById(R.id.myprofile_dialog_form_address_edittext);
        myprofileDialogFormCityEdittext = (CustomEdittext) findViewById(R.id.myprofile_dialog_form_city_edittext);
        myprofileDialogFormStateEdittext = (CustomEdittext) findViewById(R.id.myprofile_dialog_form_state_edittext);
        myprofileDialogFormCountryEdittext = (CustomEdittext) findViewById(R.id.myprofile_dialog_form_country_edittext);
        myprofileDialogFormEmailEdittext = (CustomEdittext) findViewById(R.id.myprofile_dialog_form_email_edittext);

        agreeBox = (CheckBox) findViewById(R.id.agree_box);
        userProfileUpdateBtn = (RelativeLayout) findViewById(R.id.user_profile_update_btn);
        profileLoadingProgressbar = (SmoothProgressBar) findViewById(R.id.profile_loading_progressbar);
        myprofileDialogFormPicodeEdittext = (CustomEdittext) findViewById(R.id.myprofile_dialog_form_postcode_edittext);
        myprofileDialogFormAddressEdittext.setThreshold(0);


        userProfileUpdateBtn.setOnClickListener(this);
        cancelLayout.setOnClickListener(this);
        agreeBox.setOnCheckedChangeListener(this);

        Intent in =getIntent();
        name=in.getStringExtra("name");
        email=in.getStringExtra("email");
        gender=in.getStringExtra("gender");
        age=in.getStringExtra("age");
        address=in.getStringExtra("address");
        city=in.getStringExtra("city");
        state=in.getStringExtra("state");
        picode=in.getStringExtra("picode");
        country=in.getStringExtra("country");


        myprofileDialogFormNameEdittext.setText(name);
        myprofileDialogFormEmailEdittext.setText(email);
        myprofileDialogFormAgeEdittext.setText(age);
        myprofileDialogFormAddressEdittext.setText(address);
        myprofileDialogFormCityEdittext.setText(city);
        myprofileDialogFormStateEdittext.setText(state);
        myprofileDialogFormPicodeEdittext.setText(picode);
        myprofileDialogFormCountryEdittext.setText(country);


    }


    @Override
    public void onClick(View v) {
        if (v == cancelLayout) {
            finish();
            overridePendingTransition(R.anim.exit, R.anim.enter);
        } else if (v == userProfileUpdateBtn) {
            ProfileUpdate();
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {

        } else {

        }
    }

    private void ProfileUpdate() {
        try {
            myprofileDialogFormNameEdittext.setText(Html.fromHtml(myprofileDialogFormNameEdittext.getText().toString()).toString());
            myprofileDialogFormEmailEdittext.setText(Html.fromHtml(myprofileDialogFormEmailEdittext.getText().toString()).toString());
            profilePageFormAgeTextview.setText(Html.fromHtml(profilePageFormAgeTextview.getText().toString()).toString());
            myprofileDialogFormAddressEdittext.setText(Html.fromHtml(myprofileDialogFormAddressEdittext.getText().toString()).toString());
            myprofileDialogFormCityEdittext.setText(Html.fromHtml(myprofileDialogFormCityEdittext.getText().toString()).toString());
            myprofileDialogFormStateEdittext.setText(Html.fromHtml(myprofileDialogFormStateEdittext.getText().toString()).toString());
            myprofileDialogFormPicodeEdittext.setText(Html.fromHtml(myprofileDialogFormPicodeEdittext.getText().toString()).toString());
            myprofileDialogFormCountryEdittext.setText(Html.fromHtml(myprofileDialogFormCountryEdittext.getText().toString()).toString());


                if (myprofileDialogFormNameEdittext.getText().toString().length() == 0) {
                    erroredit(myprofileDialogFormNameEdittext, getResources().getString(R.string.profile_label_alert_username));
                } else if (!isValidEmail(myprofileDialogFormEmailEdittext.getText().toString().trim().replace(" ", ""))) {
                    erroredit(myprofileDialogFormEmailEdittext, getResources().getString(R.string.profile_label_alert_email));
                } else if (profilePageGendeSpinner.getSelectedItem().equals("Gender")) {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_gender1));
                } else if (myprofileDialogFormAgeEdittext.getText().toString().length() == 0) {
                    erroredit(myprofileDialogFormAgeEdittext, getResources().getString(R.string.profile_label_alert_age));
                } else if (myprofileDialogFormAddressEdittext.getText().toString().length() == 0) {
                    erroredit(myprofileDialogFormAddressEdittext, getResources().getString(R.string.profile_label_alert_address));
                } else if (myprofileDialogFormCityEdittext.getText().toString().length() == 0) {
                    erroredit(myprofileDialogFormCityEdittext, getResources().getString(R.string.profile_label_alert_cityname));
                } else if (myprofileDialogFormStateEdittext.getText().toString().length() == 0) {
                    erroredit(myprofileDialogFormStateEdittext, getResources().getString(R.string.profile_label_alert_state));
                } else if (myprofileDialogFormPicodeEdittext.getText().toString().length() == 0) {
                    erroredit(myprofileDialogFormPicodeEdittext, getResources().getString(R.string.profile_label_alert_pincode));
                } else if (myprofileDialogFormCountryEdittext.getText().toString().length() == 0) {
                    erroredit(myprofileDialogFormCountryEdittext, getResources().getString(R.string.profile_label_alert_countryname));
                } else if(!agreeBox.isChecked()){
                    Alert(getResources().getString(R.string.action_error),  getResources().getString(R.string.pls_provide_info));
                }else {
                    cd = new ConnectionDetector(NewProfile_Page_ProfileEdit.this);
                    isInternetPresent = cd.isConnectingToInternet();

                    if (isInternetPresent) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("user_id", UserID);
                        jsonParams.put("user_name", myprofileDialogFormNameEdittext.getText().toString());
                        jsonParams.put("email", myprofileDialogFormEmailEdittext.getText().toString());
                        jsonParams.put("gender", profilePageGendeSpinner.getSelectedItem().toString());
                        jsonParams.put("age", myprofileDialogFormAgeEdittext.getText().toString());
                        jsonParams.put("locality", myprofileDialogFormAddressEdittext.getText().toString());
                        jsonParams.put("district", myprofileDialogFormCityEdittext.getText().toString());
                        jsonParams.put("state", myprofileDialogFormStateEdittext.getText().toString());
                        jsonParams.put("pincode", myprofileDialogFormPicodeEdittext.getText().toString());
                        jsonParams.put("country", myprofileDialogFormCountryEdittext.getText().toString());
                        jsonParams.put("mode", "update");

                        PostRequest_updateProfile(Iconstant.userprofile_update_user_info_url, jsonParams);

                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }
                }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void OtherClickEvents() {
        myprofileDialogFormAddressEdittext.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                myprofileDialogFormAddressEdittext.dismissDropDown();
                sSelected_location = itemList_location.get(position);
                cd = new ConnectionDetector(NewProfile_Page_ProfileEdit.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId.get(position), "user_profile_address");
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }
            }
        });

        myprofileDialogFormAddressEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cd = new ConnectionDetector(NewProfile_Page_ProfileEdit.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }

                    String sTypedPlace = "";
                    try {
                        sTypedPlace = URLEncoder.encode(myprofileDialogFormAddressEdittext.getText().toString().toLowerCase(), "utf-8");
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }

                    CitySearchRequest(Iconstant.place_search_url + sTypedPlace, "user_profile_address");
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        profilePageGendeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> passengers = new ArrayList<String>();
        passengers.add(getResources().getString(R.string.gender_lable));
        passengers.add(getResources().getString(R.string.male_lable));
        passengers.add(getResources().getString(R.string.female_lable));
        passengers.add(getResources().getString(R.string.others_lable));
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.layout, passengers);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        profilePageGendeSpinner.setAdapter(dataAdapter);


        if(gender.equalsIgnoreCase("male")){
            profilePageGendeSpinner.setSelection(1);
        }else if(gender.equalsIgnoreCase("female")){
            profilePageGendeSpinner.setSelection(2);
        }else if( gender.equalsIgnoreCase("others")){
            profilePageGendeSpinner.setSelection(3);
        }

    }


    //-------------------Get Latitude and Longitude from Address(Place ID) Request----------------
    private void LatLongRequest(String Url, final String addressforwho) {

        profileLoadingProgressbar.setVisibility(View.VISIBLE);

        System.out.println("--------------LatLong url-------------------" + Url);

        mRequest = new ServiceRequest(NewProfile_Page_ProfileEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------LatLong  reponse-------------------" + response);
                String status = "", sArea = "", sLocality = "", sCity = "", sState = "", sPostalCode = "", sRoute = "", sStreetName = "", sCountry = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONObject place_object = object.getJSONObject("result");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_object.length() > 0) {

                                sArea = place_object.getString("vicinity");
                                JSONArray addressArray = place_object.getJSONArray("address_components");
                                if (addressArray.length() > 0) {
                                    for (int i = 0; i < addressArray.length(); i++) {
                                        JSONObject address_object = addressArray.getJSONObject(i);

                                        JSONArray typesArray = address_object.getJSONArray("types");
                                        if (typesArray.length() > 0) {
                                            for (int j = 0; j < typesArray.length(); j++) {


                                                if (typesArray.get(j).toString().equalsIgnoreCase("locality")) {
                                                    sLocality = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("administrative_area_level_2")) {
                                                    sCity = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("administrative_area_level_1")) {
                                                    sState = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("postal_code")) {
                                                    sPostalCode = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("route")) {
                                                    sRoute = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("sublocality_level_1")) {
                                                    sStreetName = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("country")) {
                                                    sCountry = address_object.getString("long_name");
                                                }


                                            }


                                            isAddressAvailable = true;
                                        } else {
                                            isAddressAvailable = false;
                                        }
                                    }
                                } else {
                                    isAddressAvailable = false;
                                }

                                JSONObject geometry_object = place_object.getJSONObject("geometry");
                                if (geometry_object.length() > 0) {
                                    JSONObject location_object = geometry_object.getJSONObject("location");
                                    if (location_object.length() > 0) {
                                        sLatitude = location_object.getString("lat");
                                        sLongitude = location_object.getString("lng");
                                        isDataAvailable = true;
                                    } else {
                                        isDataAvailable = false;
                                    }
                                } else {
                                    isDataAvailable = false;
                                }
                            } else {
                                isDataAvailable = false;
                            }
                        } else {
                            isDataAvailable = false;
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isDataAvailable) {
                    System.out.println("-------------sLatitude---------------" + sLatitude);
                    System.out.println("-------------sLongitude---------------" + sLongitude);
                    System.out.println("-------------sSelected_location---------------" + sSelected_location);


                    if (addressforwho.equalsIgnoreCase("user_profile_address")) {

                        if (isAddressAvailable) {
                            myprofileDialogFormAddressEdittext.setText(sSelected_location);
//                        Et_StreetName.setText(sStreetName);
                            myprofileDialogFormCityEdittext.setText(sCity);
                            myprofileDialogFormStateEdittext.setText(sState);
                            myprofileDialogFormPicodeEdittext.setText(sPostalCode);
                            myprofileDialogFormCountryEdittext.setText(sCountry);
                            myprofileDialogFormAddressEdittext.dismissDropDown();

                        } else {
                            myprofileDialogFormAddressEdittext.setText(sSelected_location);
                            myprofileDialogFormAddressEdittext.dismissDropDown();
                        }

                    } else {


                    }


                    profileLoadingProgressbar.setVisibility(View.GONE);
                } else {
                    profileLoadingProgressbar.setVisibility(View.GONE);
                    Alert(getResources().getString(R.string.action_error), status);
                }
            }

            @Override
            public void onErrorListener() {
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
        });
    }

    //-------------------Search Place Request----------------
    private void CitySearchRequest(String Url, final String addressforwho) {

        System.out.println("--------------Search city url-------------------" + Url);

        mRequest = new ServiceRequest(NewProfile_Page_ProfileEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Search city  reponse-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONArray place_array = object.getJSONArray("predictions");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_array.length() > 0) {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                for (int i = 0; i < place_array.length(); i++) {
                                    JSONObject place_object = place_array.getJSONObject(i);
                                    itemList_location.add(place_object.getString("description"));
                                    itemList_placeId.add(place_object.getString("place_id"));
                                }
                                isDataAvailable = true;
                            } else {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                isDataAvailable = false;
                            }
                        } else {
                            itemList_location.clear();
                            itemList_placeId.clear();
                            isDataAvailable = false;
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isDataAvailable) {
                    if (addressforwho.equalsIgnoreCase("user_profile_address")) {
                        adapter = new ArrayAdapter<String>
                                (NewProfile_Page_ProfileEdit.this, R.layout.auto_complete_list_item, R.id.autoComplete_textView, itemList_location);
                        myprofileDialogFormAddressEdittext.setAdapter(adapter);
                    } else {
                        adapter = new ArrayAdapter<String>
                                (NewProfile_Page_ProfileEdit.this, R.layout.auto_complete_list_item, R.id.autoComplete_textView, itemList_location);
                        myprofileDialogFormAddressEdittext.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }
//    -----------------------------user profile info update-----------------


    private void PostRequest_updateProfile(String Url, final HashMap<String, String> jsonParams) {

        profileLoadingProgressbar.setVisibility(View.VISIBLE);
        System.out.println("--------------updateProfile-------------------" + Url);
        System.out.println("--------------updateProfile jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewProfile_Page_ProfileEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

//                Log.e("registr", response);

                System.out.println("--------------updateProfile reponse-------------------" + response);

                String Sstatus = "", Smessage = "", Up_user_name = "", Up_email = "", Up_unique_code = "", Up_gender = "", Up_age = "", Up_locality = "", Up_state = "", Up_district = "", Up_pincode = "", Up_country = "";


                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");


                        JSONObject jsonObject_userprofile = object.getJSONObject("user_profile");

                        if (jsonObject_userprofile.length() > 0) {


                            Up_user_name = jsonObject_userprofile.getString("user_name");
                            Up_email = jsonObject_userprofile.getString("email");
                            Up_unique_code = jsonObject_userprofile.getString("unique_code");
                            Up_gender = jsonObject_userprofile.getString("gender");
                            Up_age = jsonObject_userprofile.getString("age");
                            Up_locality = jsonObject_userprofile.getString("locality");
                            Up_state = jsonObject_userprofile.getString("state");
                            Up_district = jsonObject_userprofile.getString("district");
                            Up_pincode = jsonObject_userprofile.getString("pincode");
                            Up_country = jsonObject_userprofile.getString("country");
                        }
                    } else {
                        Smessage = object.getString("response");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    profileLoadingProgressbar.setVisibility(View.GONE);
                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    Alert1(getResources().getString(R.string.profile_changes), getResources().getString(R.string.profileupdate_thanks));


                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
            @Override
            public void onErrorListener() {
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
        });
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewProfile_Page_ProfileEdit.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_ProfileEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_ProfileEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.close_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent local = new Intent();
                local.setAction("com.app.PaymentListPage.refreshPage");
                local.putExtra("refresh", "yes");
                sendBroadcast(local);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }

}
