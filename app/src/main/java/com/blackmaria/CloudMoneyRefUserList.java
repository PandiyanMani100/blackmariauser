package com.blackmaria;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.blackmaria.utils.ImageLoader;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.widgets.CustomTextView;
import com.squareup.picasso.Picasso;

/**
 * Created by user144 and user144 on 5/18/2017.
 */

public class CloudMoneyRefUserList extends AppCompatActivity {

    GridView androidGridView;

    String[] imageIDs, mStringsName;
    RefUserImageAdapter agp;
    ImageView imageview_backarrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cloudmoney_referenceuser_list);


        Intent intent = getIntent();
        imageIDs = intent.getStringArrayExtra("stringarray");
        mStringsName = intent.getStringArrayExtra("stringarrayname");

        imageview_backarrow = (ImageView) findViewById(R.id.imageview_backarrow);
        imageview_backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CloudMoneyRefUserList.this, CloudMoneyHomePage.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });

        androidGridView = (GridView) findViewById(R.id.gridview_android_example);
        agp = new RefUserImageAdapter(CloudMoneyRefUserList.this, imageIDs, mStringsName);
        androidGridView.setAdapter(agp);
        agp.notifyDataSetChanged();
    }

    private class RefUserImageAdapter extends BaseAdapter {
        private ImageLoader imageLoader;
        private LayoutInflater mInflater;
        private Activity context;
        String[] image, mStringsName;

        public RefUserImageAdapter(Activity c, String[] image, String[] mStringsName) {
            context = c;
            mInflater = LayoutInflater.from(context);
            this.image = image;
            this.mStringsName = mStringsName;
            imageLoader = new ImageLoader(context);
        }

        @Override
        public int getCount() {
            return image.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        public class ViewHolder {
            private RoundedImageView image;
            private CustomTextView Rl_list_single;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            ViewHolder holder;
            if (convertView == null) {
                view = mInflater.inflate(R.layout.ref_user_list_single, parent, false);
                holder = new ViewHolder();
                holder.image = (RoundedImageView) view.findViewById(R.id.refuser_single_car_image);
                holder.Rl_list_single = (CustomTextView) view.findViewById(R.id.refusername);
                view.setTag(holder);
            } else {
                view = convertView;
                holder = (ViewHolder) view.getTag();
            }
            System.out.println("==========image user=========" + image[position]);
            Picasso.with(context).load(String.valueOf(image[position])).into(holder.image);
            holder.Rl_list_single.setText(mStringsName[position].toUpperCase());
            return view;
        }
    }

}
