package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by user14 on 1/4/2017.
 */

public class MyFavourites extends ActivityHockeyApp implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "";
    private String SselectedAddress = "", Slatitude = "", Slongitude = "", Stitle = "", SlocationKey = "", SidentityKey = "";

    private RelativeLayout Rl_back, Rl_save;
    private EditText Et_name;
    private CustomTextView Tv_search_address,Tv_alert,Tv_pickup_viewlist,Tv_drop_view_list;
    private ImageView currentLocation_image;
    private ServiceRequest mRequest, editRequest;
    private GoogleMap googleMap;
    GPSTracker gps;
    static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
    Dialog dialog;

    private RelativeLayout Rl_alert,Rl_add_pickup,Rl_add_drop;
    private boolean isAddressAvailable = false;
    private int placeSearch_request_code = 200;


    //-----Declaration For Enabling Gps-------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 299;
    private Dialog save_dialog;

    private CheckBox CB_home, CB_office, CB_club,CB_restaurant;
    private String sLocationName = "";
    private RefreshReceiver refreshReceiver;
    private CustomEdittext Et_lable_name;
MapFragment mapFragment;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(MyFavourites.this, MyFavourites.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
//                        FavoriteList.favoriteList_class.finish();
//                        MyFavourites.fragment_homePage_class.finish();
                        Intent intent1 = new Intent(MyFavourites.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus) ){
//                        FavoriteList.favoriteList_class.finish();
//                        MyFavourites.fragment_homePage_class.finish();
                        Intent intent1 = new Intent(MyFavourites.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favorites);
        initialize();
        initializeMap();



        Rl_add_pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Tv_search_address.getText().length() > 0 &&!Tv_search_address.getText().toString().equalsIgnoreCase(getResources().getString(R.string.myfavorites_label_address_location)) &&!Tv_search_address.getText().toString().equalsIgnoreCase(getResources().getString(R.string.favorite_add_label_gettingAddress))) {
                    saveDialog();
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.favorite_add_label_invalid_address));
                }

            }
        });

        Rl_add_drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Tv_search_address.getText().length() > 0 &&!Tv_search_address.getText().toString().equalsIgnoreCase(getResources().getString(R.string.myfavorites_label_address_location)) &&!Tv_search_address.getText().toString().equalsIgnoreCase(getResources().getString(R.string.favorite_add_label_gettingAddress))) {
                    saveDialog();
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.favorite_add_label_invalid_address));
                }
            }
        });

        Tv_search_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyFavourites.this, FavouriteLocationSearch.class);
                startActivityForResult(intent, placeSearch_request_code);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        currentLocation_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(MyFavourites.this);
                isInternetPresent = cd.isConnectingToInternet();
                gps = new GPSTracker(MyFavourites.this);

                if (gps.isgpsenabled() && gps.canGetLocation()) {

                    double Dlatitude = gps.getLatitude();
                    double Dlongitude = gps.getLongitude();

                    // Move the camera to last position with a zoom level
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                } else {
                    Toast.makeText(MyFavourites.this, getResources().getString(R.string.gpsnotenable), Toast.LENGTH_LONG).show();
                }
            }
        });



        GoogleMap.OnCameraChangeListener mOnCameraChangeListener = new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                double latitude = cameraPosition.target.latitude;
                double longitude = cameraPosition.target.longitude;

                cd = new ConnectionDetector(MyFavourites.this);
                isInternetPresent = cd.isConnectingToInternet();

                Log.e("latitude--on_camera_change---->", "" + latitude);
                Log.e("longitude--on_camera_change---->", "" + longitude);

                if (latitude != 0.0) {
                    googleMap.clear();

                    Slatitude = String.valueOf(latitude);
                    Slongitude = String.valueOf(longitude);

                    if (isInternetPresent) {

                        if (Slatitude != null && Slongitude != null) {
                            GetCompleteAddressAsyncTask asyncTask = new GetCompleteAddressAsyncTask();
                            asyncTask.execute();
                        } else {
                            Rl_alert.setVisibility(View.VISIBLE);
                            Tv_alert.setText(getResources().getString(R.string.favorite_add_label_no_address));
                        }

                    } else {
                        Rl_alert.setVisibility(View.VISIBLE);
                        Tv_alert.setText(getResources().getString(R.string.alert_nointernet));
                    }
                }
            }
        };

        if (CheckPlayService()) {
            googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String tittle = marker.getTitle();
                    Log.e("tittle--on_camera_change---->", "" + tittle);
                    return true;
                }
            });
        } else {
            Alert(getResources().getString(R.string.alert_label_title),  getResources().getString(R.string.install_googleplay_view_location));
        }


    }

    private void initialize() {
        session = new SessionManager(MyFavourites.this);
        cd = new ConnectionDetector(MyFavourites.this);
        isInternetPresent = cd.isConnectingToInternet();
        gps = new GPSTracker(MyFavourites.this);

//        Rl_back = (RelativeLayout) findViewById(R.id.favorite_add_header_back_layout);
//        Rl_save = (RelativeLayout) findViewById(R.id.favorite_add_header_save_layout);
//        Et_name = (EditText) findViewById(R.id.favorite_add_name_edittext);
        Tv_search_address = (CustomTextView) findViewById(R.id.my_favorites_navigation_search_address);
        Rl_alert = (RelativeLayout) findViewById(R.id.my_favorites_add_alert_layout);
        Tv_alert = (CustomTextView) findViewById(R.id.my_favorites_add_alert_textView);
        currentLocation_image = (ImageView) findViewById(R.id.my_favorites_add_current_location_imageview);
        Tv_pickup_viewlist = (CustomTextView) findViewById(R.id.my_favorites_pickup_view_list_textview);
        Tv_drop_view_list= (CustomTextView) findViewById(R.id.my_favorites_drop_view_list_textview);

        Rl_add_pickup = (RelativeLayout) findViewById(R.id.my_favorites_add_pickup_layout);
        Rl_add_drop = (RelativeLayout) findViewById(R.id.my_favorites_add_drop_layout);


        

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);



        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
    }

    private void initializeMap() {
        if (googleMap == null) {
            //googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.book_my_ride3_mapview)).getMap();
            mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.my_favorites_mapview));
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap arg) {
                    loadMap(arg);
                }
            });
        }
    }


    public void loadMap(GoogleMap arg) {
        googleMap = arg;
        // Changing map type
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Showing / hiding your current location
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        // Enable / Disable my location button
        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(false);
        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);



        if (gps.canGetLocation() && gps.isgpsenabled()) {
            double Dlatitude = gps.getLatitude();
            double Dlongitude = gps.getLongitude();


            // Move the camera to last position with a zoom level
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        } else {

            enableGpsService();
        }
    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(MyFavourites.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-----------Check Google Play Service--------
    private boolean CheckPlayService() {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(MyFavourites.this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        }
        return true;
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        runOnUiThread(new Runnable() {
            public void run() {
                final Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionStatusCode, MyFavourites.this, REQUEST_CODE_RECOVER_PLAY_SERVICES);
                if (dialog == null) {
                    Toast.makeText(MyFavourites.this,  getResources().getString(R.string.incompitable), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    //-------------AsyncTask to get Complete Address------------
    public class GetCompleteAddressAsyncTask extends AsyncTask<Void, Void, String> {
        String strAdd = "";

        @Override
        protected void onPreExecute() {
            Tv_search_address.setText(getResources().getString(R.string.favorite_add_label_gettingAddress));
        }

        @Override
        protected String doInBackground(Void... params) {

            Geocoder geocoder = new Geocoder(MyFavourites.this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(Slatitude), Double.parseDouble(Slongitude), 1);
                if (addresses != null) {
                    Address returnedAddress = addresses.get(0);
                    StringBuilder strReturnedAddress = new StringBuilder("");

                    for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    }
                    strAdd = strReturnedAddress.toString();
                    isAddressAvailable = true;
                } else {
                    Log.e("My Current loction address", "No Address returned!");
                    isAddressAvailable = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("My Current loction address", "Canont get Address!");
                isAddressAvailable = false;
            }

            return strAdd;
        }

        @Override
        protected void onPostExecute(String address) {

            if (isAddressAvailable) {
                Rl_alert.setVisibility(View.GONE);
                Tv_search_address.setText(address.replace("\n"," ").toString());
            } else {
                Rl_alert.setVisibility(View.VISIBLE);
                Tv_alert.setText(getResources().getString(R.string.favorite_add_label_no_address));
                Tv_search_address.setText("");
            }
        }
    }

    
    //----------------------------------------------------------------------------------------------


    private void saveDialog() {

        save_dialog = new Dialog(MyFavourites.this);
        save_dialog.getWindow();
        save_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        save_dialog.setContentView(R.layout.favorite_save_alert);
        save_dialog.setCanceledOnTouchOutside(true);
        save_dialog.show();
        save_dialog.getWindow().setGravity(Gravity.CENTER);

        final CustomTextView Tv_close = (CustomTextView) save_dialog.findViewById(R.id.favorite_save_alert_close_textview);
        RelativeLayout Rl_ok = (RelativeLayout) save_dialog.findViewById(R.id.favorite_save_alert_ok_layout);
        CB_home = (CheckBox) save_dialog.findViewById(R.id.favorite_save_alert_home_checkbox);
        CB_office = (CheckBox) save_dialog.findViewById(R.id.ffavorite_save_alert_office_checkbox);
        CB_club = (CheckBox) save_dialog.findViewById(R.id.favorite_save_alert_club_checkbox);
        CB_restaurant = (CheckBox) save_dialog.findViewById(R.id.favorite_save_alert_restaurant_checkbox);
        Et_lable_name = (CustomEdittext) save_dialog.findViewById(R.id.favorite_save_alert_name_edittext);


        CB_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CB_office.setChecked(false);
                CB_club.setChecked(false);
                CB_restaurant.setChecked(false);
            }
        });
        CB_office.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CB_restaurant.setChecked(false);
                CB_club.setChecked(false);
                CB_home.setChecked(false);

            }
        });
        CB_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CB_home.setChecked(false);
                CB_office.setChecked(false);
                CB_restaurant.setChecked(false);
            }
        });
        CB_restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CB_home.setChecked(false);
                CB_office.setChecked(false);
                CB_club.setChecked(false);
            }
        });

        Rl_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CB_home.isChecked()){
                    sLocationName = CB_home.getText().toString();
                    saveLocation(sLocationName);
                } else if(CB_office.isChecked()){
                    sLocationName = CB_office.getText().toString();
                    saveLocation(sLocationName);
                }else if(CB_club.isChecked()){
                    sLocationName = CB_club.getText().toString();
                    saveLocation(sLocationName);
                }else if(CB_restaurant.isChecked()){
                    sLocationName = CB_restaurant.getText().toString();
                    saveLocation(sLocationName);
                }else if (Et_lable_name.getText().toString().length() > 0){
                    sLocationName = Et_lable_name.getText().toString();
                    saveLocation(sLocationName);
                }else{
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.favorite_add_label_name_empty));
                }

            }
        });

        Tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save_dialog.dismiss();
            }
        });

    }

    private void saveLocation(String lName) {

        System.out.println("-------------lName-----------"+lName);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("title", lName);
        jsonParams.put("latitude", Slatitude);
        jsonParams.put("longitude", Slongitude);
        jsonParams.put("address", Tv_search_address.getText().toString());

        System.out.println("-------------mileage_reward_success url jsonParams----------------" + jsonParams);
        if (isInternetPresent) {
            postRequest_FavoriteSave(Iconstant.favoritelist_add_url, jsonParams);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

        }
    }


    //-----------------------Favourite Save Post Request-----------------
    private void postRequest_FavoriteSave(String Url,HashMap<String, String> jsonParams) {
        dialog = new Dialog(MyFavourites.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_saving));


        System.out.println("-------------Favourite Save Url----------------" + Url);
        System.out.println("-------------Favourite Save jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(MyFavourites.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Favourite Save Response----------------" + response);
                String Sstatus = "", Smessage = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        dialogDismiss();
                        final PkDialog mDialog = new PkDialog(MyFavourites.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.action_success));
                        mDialog.setDialogMessage(Smessage);
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                save_dialog.dismiss();
                            }
                        });
                        mDialog.show();

                    } else {
                        dialogDismiss();
                        Alert(getResources().getString(R.string.alert_label_title), Smessage);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    //-----------------------Favourite List Edit Post Request-----------------
    private void postRequest_FavoriteEdit(String Url) {
        dialog = new Dialog(MyFavourites.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_saving));

        System.out.println("-------------Favourite Edit Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("title", Et_name.getText().toString());
        jsonParams.put("latitude", Slatitude);
        jsonParams.put("longitude", Slongitude);
        jsonParams.put("address", Tv_search_address.getText().toString());
        jsonParams.put("location_key", SlocationKey);

        editRequest = new ServiceRequest(MyFavourites.this);
        editRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Favourite Edit Response----------------" + response);

                String Sstatus = "", Smessage = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    // close keyboard
                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(Et_name.getWindowToken(), 0);

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Intent local = new Intent();
                        local.setAction("com.favoriteList.refresh");
                        sendBroadcast(local);

                        final PkDialog mDialog = new PkDialog(MyFavourites.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.action_success));
                        mDialog.setDialogMessage(Smessage);
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                finish();
                                overridePendingTransition(R.anim.enter, R.anim.exit);
                            }
                        });
                        mDialog.show();
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), Smessage);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            // close keyboard
            InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            mgr.hideSoftInputFromWindow(Rl_back.getWindowToken(), 0);

            onBackPressed();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((requestCode == placeSearch_request_code && resultCode == Activity.RESULT_OK && data != null)) {

                String SselectedLatitude = data.getStringExtra("Selected_Latitude");
                String SselectedLongitude = data.getStringExtra("Selected_Longitude");
                String SselectedLocation = data.getStringExtra("Selected_Location");
                if (!SselectedLatitude.equalsIgnoreCase("") && SselectedLatitude.length() > 0 && !SselectedLongitude.equalsIgnoreCase("") && SselectedLongitude.length() > 0) {
                    // Move the camera to last position with a zoom level
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(SselectedLatitude), Double.parseDouble(SselectedLongitude))).zoom(17).build();
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
                Tv_search_address.setText(SselectedLocation.replace("\n"," ").toString());

        }

        super.onActivityResult(requestCode, resultCode, data);
    }



    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(MyFavourites.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MyFavourites.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }


}

