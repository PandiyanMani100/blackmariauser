package com.blackmaria;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.widgets.PkDialog;

/**
 * Created by user144 on 5/4/2017.
 */

public class CashBackFareShare extends ActivityHockeyApp {
    private ImageView invteImage;
    private String cashback_amount = "", currency = "", wallet_amount = "";
    private TextView bal_amount_credit, exit_lyt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cashback_offer2_layout);
        initialize();
        /*invteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cahbackIntent = new Intent(CashBackFareShare.this, InviteAndEarn.class);
                startActivity(cahbackIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });*/
        exit_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cahbackIntent = new Intent(CashBackFareShare.this, FareBreakUp.class);
                cahbackIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(cahbackIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                onBackPressed();
                finish();
            }
        });
        invteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_share = new Intent(CashBackFareShare.this, InviteAndEarn.class);
                intent_share.putExtra("str_page", getResources().getString(R.string.profile_label_menu));
                startActivity(intent_share);
                //   finish();
            }
        });
    }


    private void initialize() {
        //cashbackcreadited=(CustomTextView)findViewById(R.id.cashbackcreadited);
//        bal_amount_credit = findViewById(R.id.current_balance);
        exit_lyt = findViewById(R.id.Rl_close);
        Intent intent = getIntent();

        cashback_amount = intent.getStringExtra("cashback_amount");
        currency = intent.getStringExtra("currency");
        wallet_amount = intent.getStringExtra("wallet_amount");
        invteImage = findViewById(R.id.earn_and_invte_image);
        //cashbackcreadited.setText(currency+" "+cashback_amount+" "+getResources().getString(R.string.cashback_credited_balance));
//        bal_amount_credit.setText(currency + " " + cashback_amount);
//        invite_tv = findViewById(R.id.invite_tv);
//        ImageView Rl_drawer =  findViewById(R.id.drawer_icon);
//
//
//// get user data from session
//
//        final Animation animation = new AlphaAnimation(1, 0);
//        animation.setDuration(500);
//        animation.setInterpolator(new LinearInterpolator());
//        animation.setRepeatCount(Animation.INFINITE);
//        animation.setRepeatMode(Animation.REVERSE);
//        Rl_drawer.startAnimation(animation);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(CashBackFareShare.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }
}

