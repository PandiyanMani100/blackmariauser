package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.pojo.ViewComplaintPojo;
import com.blackmaria.R;
import com.blackmaria.utils.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 3/9/2018.
 */

public class ComplaintPageViewDetailAdapter extends BaseAdapter {

    private ArrayList<ViewComplaintPojo> data;
    LanguageDb mhelper;
    private LayoutInflater mInflater;
    private Context context;
    ReplyCloseInterface replycloseinterface;
    private String DriverImage = "", UserImage = "";
    String  userType="";
    public ComplaintPageViewDetailAdapter(Context c, ArrayList<ViewComplaintPojo> d, ReplyCloseInterface replycloseinterface, String driverimage, String userImage) {
        context = c;
        mhelper = new LanguageDb(context);
        mInflater = LayoutInflater.from(context);
        data = d;
        DriverImage = driverimage;
        UserImage = userImage;
        this.replycloseinterface = replycloseinterface;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView view_complaint_close_the_case_textview, view_complaint_page_replay_message_send_imageview, Tv_user_commmnet, Tv_user_commmnet_date, view_complaint_comment_list_you_wrote_label;
        private RoundedImageView complaint_page_user_image;
        private LinearLayout compview,complaint_submit_lyt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.complaintreply_single_constrain, parent, false);
            holder = new ViewHolder();

            holder.Tv_user_commmnet = view.findViewById(R.id.view_complaint_comment_list_user_complaint);
            holder.view_complaint_comment_list_you_wrote_label =  view.findViewById(R.id.view_complaint_comment_list_you_wrote_label);
            holder.Tv_user_commmnet_date = view.findViewById(R.id.view_complaint_comment_list_user_complaint_date);
            holder.complaint_page_user_image =  view.findViewById(R.id.complaint_page_user_image);
//            holder.compview =  view.findViewById(R.id.compview);

            holder.complaint_submit_lyt =  view.findViewById(R.id.complaint_submit_lyt);
            holder.view_complaint_page_replay_message_send_imageview = view.findViewById(R.id.view_complaint_page_replay_message_send_imageview);
            holder.view_complaint_close_the_case_textview = view.findViewById(R.id.view_complaint_close_the_case_textview);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        holder.view_complaint_page_replay_message_send_imageview.setText(getkey("reply"));
        holder.view_complaint_close_the_case_textview.setText(getkey("closelable"));
        holder.view_complaint_page_replay_message_send_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replycloseinterface.onReplyToDriver();
            }
        });
        holder.view_complaint_close_the_case_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replycloseinterface.onCloseIssue();
            }
        });


//        if ((position % 2) == 0) {
//            holder.compview.setBackgroundColor(Color.parseColor("#35FFFFFF"));
//        } else {
//            holder.compview.setBackgroundColor(Color.parseColor("#50FFFFFF"));
//        }
        if (position == data.size() - 1) {
            holder.complaint_submit_lyt.setVisibility(View.VISIBLE);
        } else {
            holder.complaint_submit_lyt.setVisibility(View.GONE);
        }

        holder.Tv_user_commmnet.setText(data.get(position).getComments());
        holder.view_complaint_comment_list_you_wrote_label.setText(data.get(position).getComment_by() +" "+ getkey("wrote"));
        holder.Tv_user_commmnet_date.setText("at "+data.get(position).getComment_date());

        userType=data.get(position).gettype();

        if (userType.equalsIgnoreCase("user")) {
            Picasso.with(context).load(UserImage).placeholder(R.drawable.user_newcomp).into(holder.complaint_page_user_image);
        } else if (userType.equalsIgnoreCase("driver") || userType.equalsIgnoreCase("admin")) {
            Picasso.with(context).load(DriverImage).placeholder(R.drawable.user_newcomp).into(holder.complaint_page_user_image);
        }
        return view;
    }


    public interface ReplyCloseInterface {
        void onReplyToDriver();

        void onCloseIssue();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
