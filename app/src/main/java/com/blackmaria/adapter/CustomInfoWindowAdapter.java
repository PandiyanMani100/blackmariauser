package com.blackmaria.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.blackmaria.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by muruganantham on 12/12/2017.
 */
public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    View mWindow,mContents;
    private Activity context;
    public CustomInfoWindowAdapter(Activity cxt) {
        this.context=cxt;
        mWindow = context.getLayoutInflater().inflate(R.layout.infowindow, null);
        mContents =context.getLayoutInflater().inflate(R.layout.infowindow, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        render(marker, mWindow);
        return mWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {


        render(marker, mContents);
        return mContents;
    }



    private void render(Marker marker, View view) {
        // Use the equals() method on a Marker to check for equals.  Do not use ==.
        String title = marker.getTitle();
        TextView titleUi = (TextView) view.findViewById(R.id.tv_title);
        if (title != null) {
            titleUi.setText(title);
        } else {
            titleUi.setText("");
        }
        String snippet = marker.getSnippet();
        TextView snippetUi = ((TextView) view.findViewById(R.id.tv_subtitle));
        if (snippet != null) {
            snippetUi.setText(snippet);
        } else {
            snippetUi.setText("");
        }
    }
}



