package com.blackmaria.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 9/26/2017.
 */

public class ReloadPaymentList extends BaseAdapter {

    private static HashMap<String, ArrayList<String>> data;
    private LayoutInflater mInflater;
    private Activity context;
    private ImageLoader imageLoader;
    RelativeLayout.LayoutParams params;

    public ReloadPaymentList(Activity c, HashMap<String, ArrayList<String>> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }


    @Override
    public int getCount() {
        return data.get("bankInactive").size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView Iv_e_wallet_payment_type;
        private LinearLayout Ll_car;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        final ReloadPaymentList.ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.ewallet_singal_list_item_singal_new, parent, false);
            holder = new ReloadPaymentList.ViewHolder();

            holder.Iv_e_wallet_payment_type = (ImageView) view.findViewById(R.id.e_wallet_page_single_payment_list_imageview);
            //holder.Ll_car = (LinearLayout) view.findViewById(R.id.bookmyride_single_car_layout);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ReloadPaymentList.ViewHolder) view.getTag();
        }
        try {
            if (!data.get("bankCode").get(position).equalsIgnoreCase("xendit-bankpayment")) {
                params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(20, 20, 20, 20);
                params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                holder.Iv_e_wallet_payment_type.setLayoutParams(params);


            }else{
                params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 10, -10, 10);
                params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                holder.Iv_e_wallet_payment_type.setLayoutParams(params);

            }
            String image = data.get("bankInactive").get(position);
            Picasso.with(context).load(image).into(holder.Iv_e_wallet_payment_type);
        } catch (Exception e) {
            e.printStackTrace();
        }


     /*   //Code to adjust car at center
        Display display = context.getWindowManager().getDefaultDisplay();
        if (data.size() == 1) {
            ViewGroup.LayoutParams params = holder.Ll_car.getLayoutParams();
            params.width = (display.getWidth()) - 8;
            holder.Ll_car.setLayoutParams(params);
        } else if (data.size() == 2) {
            ViewGroup.LayoutParams params = holder.Ll_car.getLayoutParams();
            params.width = (display.getWidth() / 2) - 8;
            holder.Ll_car.setLayoutParams(params);
        } else if (data.size() == 3) {
            ViewGroup.LayoutParams params = holder.Ll_car.getLayoutParams();
            params.width = (display.getWidth() / 3) - 8;
            holder.Ll_car.setLayoutParams(params);
        } else {
            ViewGroup.LayoutParams params = holder.Ll_car.getLayoutParams();
            params.width = (display.getWidth() / 4) - 8;
            holder.Ll_car.setLayoutParams(params);
        }*/


        return view;


    }
}
