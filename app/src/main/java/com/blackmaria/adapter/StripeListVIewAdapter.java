package com.blackmaria.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.blackmaria.pojo.BookingPaymentListPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.blackmaria.widgets.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 12/20/2017.
 */

public class StripeListVIewAdapter extends BaseAdapter {

    private ArrayList<BookingPaymentListPojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Activity context;

    public StripeListVIewAdapter(Activity c, ArrayList<BookingPaymentListPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private ImageView Iv_Delete_Card;
        private CustomTextView Tv_card_number;
        private CustomTextView Tv_Exp_date;
        private CustomTextView Tv_Exp_year;
        private CustomTextView Tv_Card_type;
        public ImageView Iv_Default_Card;
//        private CheckBox Iv_Default_Card;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.stripepayment_single, parent, false);
            holder = new ViewHolder();
            holder.Tv_card_number = (CustomTextView) view.findViewById(R.id.card_number);
            holder.Tv_Exp_date = (CustomTextView) view.findViewById(R.id.expirydate);
            holder.Iv_Default_Card = (ImageView) view.findViewById(R.id.card_image);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        String phone=data.get(position).getCard_number();
        String cardNumber = phone.substring(phone.length()-9,phone.length());
        cardNumber = cardNumber.substring(0,4) + "-" + cardNumber.substring(4, cardNumber.length());
        Picasso.with(context).load(String.valueOf(data.get(position).getCard_image())).into(holder.Iv_Default_Card);
        holder.Tv_card_number.setText(cardNumber);
        holder.Tv_Exp_date.setText(context.getResources().getString(R.string.exp)+data.get(position).getExp_month()+"/"+data.get(position).getExp_year());
        return view;
    }


}
