package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.blackmaria.pojo.WalletMoneyPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class WalletPaymentListAdapter extends BaseAdapter {

    private static ArrayList<WalletMoneyPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    private ImageLoader imageLoader;

    public WalletPaymentListAdapter(Context c, ArrayList<WalletMoneyPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private ImageView Iv_payment_type;
      private  View V_view;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;

        final ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.wallet_money_single_page, parent, false);
            holder = new ViewHolder();
            holder.V_view= (View) view.findViewById(R.id.view_dummy);
            holder.Iv_payment_type = (ImageView) view.findViewById(R.id.wallet_page_single_payment_list_imageview);


            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        if ("true".equalsIgnoreCase(data.get(position).getPayment_selected_payment_id())) {
            Picasso.with(context).load(data.get(position).getPayment_active_img()).into(holder.Iv_payment_type);
//            Picasso.with(context).load(R.drawable.strip).into(holder.Iv_payment_type);

        } else {

            Picasso.with(context).load(data.get(position).getPayment_normal_img()).into(holder.Iv_payment_type);
        }
        if (data.size() % 2 != 0) {

            if (position != 0) {

                if (data.size() != 3) {
                    if (position == data.size() - 2 || position == data.size() - 1) {
                        holder.V_view.setVisibility(View.INVISIBLE);
                    }
                } else {
                    if (position == data.size() - 1) {
                        holder.V_view.setVisibility(View.INVISIBLE);
                    }
                }

            }

        } else {
            if (position == data.size() - 1) {
                holder.V_view.setVisibility(View.INVISIBLE);
            }
        }




        return view;
    }


}
