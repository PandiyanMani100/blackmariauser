package com.blackmaria.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;

import java.util.List;
import java.util.Map;

/**
 * Created by user144 on 12/14/2017.
 */

public class NewCarListAdapter extends BaseAdapter {

    private Map<String,List<String> > data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Activity context;

    public NewCarListAdapter(Activity c, Map<String,List<String> > d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.get("distance").size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView Iv_carType;
        private TextView Tv_distance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.car_list_dialog_single, parent, false);
            holder = new ViewHolder();

            holder.Iv_carType = (ImageView) view.findViewById(R.id.car_list_single_car_type_imageview);
            holder.Tv_distance = (TextView)view.findViewById(R.id.car_list_single_distace_textview);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.Tv_distance.setText(data.get("distance").get(position));
        imageLoader.DisplayImage(String.valueOf(data.get("brand_image").get(position)), holder.Iv_carType);

        return view;
    }
}
