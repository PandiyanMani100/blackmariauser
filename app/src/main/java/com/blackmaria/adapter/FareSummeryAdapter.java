package com.blackmaria.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.pojo.RideDetails_DisplayPojo;
import com.blackmaria.R;

import java.util.ArrayList;

/**
 * Created by user144 on 5/12/2017.
 */

public class FareSummeryAdapter extends BaseAdapter {

    private ArrayList<RideDetails_DisplayPojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public FareSummeryAdapter(Context c, ArrayList<RideDetails_DisplayPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView title, value;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        FareSummeryAdapter.ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.driver_profile_page_single, parent, false);
            holder = new FareSummeryAdapter.ViewHolder();
            holder.title = (TextView) view.findViewById(R.id.driver_profile_single_title_textView);
            holder.value = (TextView) view.findViewById(R.id.driver_profile_single_title_value);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (FareSummeryAdapter.ViewHolder) view.getTag();
        }


        if (position == 0) {
            holder.title.setTypeface(null, Typeface.BOLD);
            holder.value.setTypeface(null, Typeface.BOLD);
        } else {
            holder.title.setTypeface(null, Typeface.NORMAL);
            holder.value.setTypeface(null, Typeface.NORMAL);
        }

        holder.title.setText(data.get(position).getFarerate_title().toUpperCase());
        holder.value.setText(data.get(position).getFarerate_value().toUpperCase());
        return view;
    }
}
