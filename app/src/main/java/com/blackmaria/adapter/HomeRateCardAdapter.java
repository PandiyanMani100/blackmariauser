package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.pojo.RateCard_CardDisplayPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;

import java.util.ArrayList;


public class HomeRateCardAdapter extends BaseAdapter
{

    private ArrayList<RateCard_CardDisplayPojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Context context;

    public HomeRateCardAdapter(Context c, ArrayList<RateCard_CardDisplayPojo> d)
    {
        context=c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount()
    {
        return 1;
    }


    public class ViewHolder
    {
        private TextView title,value;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View view;
        ViewHolder holder;
        if(convertView == null)
        {
            view = mInflater.inflate(R.layout.home_ratecard_display_single, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) view.findViewById(R.id.ratecard_display_single_title);
            holder.value = (TextView) view.findViewById(R.id.ratecard_display_single_price);

            view.setTag(holder);
        }
        else
        {
            view = convertView;
            holder = (ViewHolder)view.getTag();
        }
        holder.title.setText(data.get(position).getRate_title().toUpperCase());
        holder.value.setText(data.get(position).getRate_value().toUpperCase());

        return view;
    }
}


