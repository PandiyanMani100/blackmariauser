package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.pojo.Geteta;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.blackmaria.utils.SessionManager;

import java.util.ArrayList;

/**
 * Created by user144 on 2/23/2018.
 */

public class MultiStop_summary_Adapter extends BaseAdapter {

    private ArrayList<Geteta.Response.Eta.Summary_arr> summary_arr;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Context context;
    SessionManager sessionManager;


    public MultiStop_summary_Adapter(Context c, ArrayList<Geteta.Response.Eta.Summary_arr> summary_arr) {
        context = c;
        mInflater = LayoutInflater.from(context);
        this.summary_arr = summary_arr;
        imageLoader = new ImageLoader(context);
        sessionManager = new SessionManager(c);
    }

    @Override
    public int getCount() {
        return summary_arr.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView dropoff_lable, booking_drop_address_textview, minutes, distance, eta, fare, waitingcharge;
        private ImageView stopoverline;
        private View view;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.multidrop_single_summary, parent, false);
            holder = new ViewHolder();

            holder.dropoff_lable = view.findViewById(R.id.dropoff_lable);
            holder.booking_drop_address_textview = view.findViewById(R.id.booking_drop_address_textview);
            holder.minutes = view.findViewById(R.id.minutes);
            holder.distance = view.findViewById(R.id.distance);
            holder.eta = view.findViewById(R.id.eta);
            holder.fare = view.findViewById(R.id.fare);
            holder.waitingcharge = view.findViewById(R.id.waitingcharge);
            holder.stopoverline = view.findViewById(R.id.stopoverline);
            holder.view = view.findViewById(R.id.view);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.dropoff_lable.setText(summary_arr.get(position).getText());
        holder.booking_drop_address_textview.setText(summary_arr.get(position).getLocaton());

        if (!summary_arr.get(position).getDistance().equals("")) {
            holder.minutes.setText(summary_arr.get(position).getDistance() + context.getResources().getString(R.string.stop));
            holder.distance.setText(context.getResources().getString(R.string.distnace) + summary_arr.get(position).getDistance());
            holder.eta.setText(context.getResources().getString(R.string.eta_lable)+" : " + summary_arr.get(position).getDuration());
            holder.fare.setText(context.getResources().getString(R.string.fae) + sessionManager.getCurrency() + "" + summary_arr.get(position).getFare());
            holder.waitingcharge.setText(context.getResources().getString(R.string.waitingcharge) + sessionManager.getCurrency() + "" + summary_arr.get(position).getWait_time());
        }


        return view;
    }


    public interface deleteMultiDrop {
        void OnDeleteLocation(int deletePostion);
    }

}
