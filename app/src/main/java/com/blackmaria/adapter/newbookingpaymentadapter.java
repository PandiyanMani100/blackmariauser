package com.blackmaria.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.blackmaria.pojo.HomePojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 8/7/2017.
 */

public class newbookingpaymentadapter extends BaseAdapter {

    private static ArrayList<HomePojo> data;
    private LayoutInflater mInflater;
    private Activity context;
    private ImageLoader imageLoader;

    public newbookingpaymentadapter(Activity c, ArrayList<HomePojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private ImageView Iv_payment_type;
        private RadioButton radioButton1;
        private TextView payment_tv;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.newbookingpaymnetadapter, parent, false);
            holder = new ViewHolder();

            holder.Iv_payment_type = (ImageView) view.findViewById(R.id.payment_list_single_imageview);
            holder.payment_tv=(TextView)view.findViewById(R.id.payment_tv);
            holder.radioButton1 = (RadioButton) view.findViewById(R.id.radioButton1);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        if ("Cash".equalsIgnoreCase(data.get(position).getPayment_name())) {
            holder.payment_tv.setVisibility(View.GONE);
        } else {

            holder.payment_tv.setText(data.get(position).getPayment_name().toUpperCase());
            holder.payment_tv.setVisibility(View.GONE);
        }
        if ("false".equalsIgnoreCase(data.get(position).getSetPayment_selected_payment_id())) {
                Picasso.with(context).load(data.get(position).getPayment_icon()).into(holder.Iv_payment_type);
        } else {
            if(!data.get(position).getSetpaymentinactiveicon().equalsIgnoreCase(""))
                Picasso.with(context).load(data.get(position).getSetpaymentinactiveicon()).into(holder.Iv_payment_type);
        }

        if(data.get(position).getCat_id().equals("0"))
        {
            holder.radioButton1.setChecked(false);
        }
        else
        {
            holder.radioButton1.setChecked(true);
        }


        return view;
    }


}
