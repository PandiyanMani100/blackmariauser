package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.pojo.RideListPojo;
import com.blackmaria.R;
import com.blackmaria.utils.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by CAS58 on 12/9/2016.
 */
public class RideListAdapter extends BaseAdapter {

    private static ArrayList<RideListPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    private String drivername = "";
    LanguageDb mhelper;

    public RideListAdapter(Context c, ArrayList<RideListPojo> d, String drivernames) {
        context = c;
        mhelper = new LanguageDb(context);
        mInflater = LayoutInflater.from(context);
        data = d;
        drivername = drivernames;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private TextView Tv_date, Tv_cartype, Tv_distance, Tv_status, Tv_amount,timess,miless,crnno,orderno,statc,viewdetail_tv;
        private ImageView status_image;
        RelativeLayout viewall_list_layout;
        public TextView Tv_go, ridenumber, ridestatus;
        private LinearLayout onride_view_detail_layout, otherstatus_layout;
        private RoundedImageView user_image_view;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.ridelist_single_new, parent, false);
            holder = new ViewHolder();

            holder.Tv_date = (TextView) view.findViewById(R.id.ride_list_single_date_time_textview);
            holder.status_image = (ImageView) view.findViewById(R.id.status_image);
            holder.Tv_cartype = (TextView) view.findViewById(R.id.ride_list_single_cartype_textview);
            holder.Tv_amount = (TextView) view.findViewById(R.id.ride_list_single_amount);
            holder.Tv_distance = (TextView) view.findViewById(R.id.ride_list_single_distance_textview);
            holder.viewall_list_layout = (RelativeLayout) view.findViewById(R.id.viewall_list_layout);
            holder.user_image_view = (RoundedImageView) view.findViewById(R.id.user_image_view);


            holder.ridenumber = (TextView) view.findViewById(R.id.ridenumber);
            holder.ridestatus = (TextView) view.findViewById(R.id.ridestatus);

            holder.onride_view_detail_layout = (LinearLayout) view.findViewById(R.id.onride_view_detail_layout);
            holder.otherstatus_layout = (LinearLayout) view.findViewById(R.id.otherstatus_layout);
            holder.Tv_status = (TextView) view.findViewById(R.id.ride_list_single_ride_status_textview);

            holder.timess = (TextView) view.findViewById(R.id.timess);
            holder.timess.setText(getkey("time_with_sp"));

            holder.miless = (TextView) view.findViewById(R.id.miless);
            holder.miless.setText(getkey("miledd"));

            holder.crnno = (TextView) view.findViewById(R.id.crnno);
            holder.crnno.setText(getkey("crn_no"));

            holder.orderno = (TextView) view.findViewById(R.id.orderno);
            holder.orderno.setText(getkey("order_no"));

            holder.statc = (TextView) view.findViewById(R.id.statc);
            holder.statc.setText(getkey("status_s"));

            holder.viewdetail_tv = (TextView) view.findViewById(R.id.viewdetail_tv);
            holder.viewdetail_tv.setText(getkey("tracknow_home"));


            view.setTag(holder);


        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        holder.Tv_date.setText(/*data.get(position).getDatetime() + "\n" + */data.get(position).getRide_time());
        holder.Tv_cartype.setText(data.get(position).getRide_category());
        holder.Tv_amount.setText(data.get(position).getRide_id());


        holder.ridenumber.setText(data.get(position).getRide_id());
        holder.ridestatus.setText(data.get(position).getRide_status());
//        holder.ridestatus.setText("Driver : " + drivername);

        if (data.get(position).getDistance() != null || data.get(position).getDistance().length() > 0) {
            holder.Tv_distance.setText(data.get(position).getDistance());
        } else {
            holder.Tv_distance.setText("-");
        }

        if (!data.get(position).getDriverImage().equalsIgnoreCase("")) {
            Picasso.with(context).load(data.get(position).getDriverImage()).placeholder(R.drawable.new_no_user_img).error(R.drawable.new_no_user_img).into(holder.user_image_view);
        }

        holder.Tv_distance.setText(data.get(position).getDistance());
//        holder.Tv_status.setText(data.get(position).getDisplay_status());
        holder.Tv_status.setText(context.getResources().getString(R.string.driver_sapce) + data.get(position).getDriver_name());

        if ("Completed".equalsIgnoreCase(data.get(position).getRide_status())) {

            holder.onride_view_detail_layout.setVisibility(View.GONE);
            holder.otherstatus_layout.setVisibility(View.VISIBLE);
            holder.Tv_status.setVisibility(View.VISIBLE);

//            holder.viewall_list_layout.setBackground(context.getResources().getDrawable(R.drawable.curved_gradient_blue_bg1));
            holder.status_image.setBackground(context.getResources().getDrawable(R.drawable.right));
        } else if ("Cancelled".equalsIgnoreCase(data.get(position).getRide_status())) {

            holder.onride_view_detail_layout.setVisibility(View.GONE);
            holder.otherstatus_layout.setVisibility(View.VISIBLE);
            holder.Tv_status.setVisibility(View.VISIBLE);

            holder.status_image.setBackground(context.getResources().getDrawable(R.drawable.wrong));
//            holder.viewall_list_layout.setBackground(context.getResources().getDrawable(R.drawable.curved_gradient_blue_bg1));
        } else {
            holder.onride_view_detail_layout.setVisibility(View.VISIBLE);
            holder.otherstatus_layout.setVisibility(View.GONE);
            holder.Tv_status.setVisibility(View.GONE);

            holder.status_image.setBackground(context.getResources().getDrawable(R.drawable.info1));
//            holder.viewall_list_layout.setBackground(context.getResources().getDrawable(R.drawable.curved_gradient_yellow));
        }

        return view;
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}
