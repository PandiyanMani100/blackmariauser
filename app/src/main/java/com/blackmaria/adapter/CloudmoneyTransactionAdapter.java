package com.blackmaria.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.pojo.WalletMoneyTransactionPojo;
import com.blackmaria.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 6/6/2017.
 */

public class CloudmoneyTransactionAdapter extends BaseAdapter {

    private ArrayList<WalletMoneyTransactionPojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public CloudmoneyTransactionAdapter(Context c, ArrayList<WalletMoneyTransactionPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView type_image;
        private TextView type_name;
        private LinearLayout cloudmoneyearnings;
        private RelativeLayout credit_debit_date;
        private TextView trans_price, title, date, balance,creditdebit_tv;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        CloudmoneyTransactionAdapter.ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.cloud_earning_list_item, parent, false);
            holder = new CloudmoneyTransactionAdapter.ViewHolder();
            holder.type_image = (ImageView) view.findViewById(R.id.rv_profile);
            holder.trans_price = (TextView) view.findViewById(R.id.txt_amount);
            holder.date = (TextView) view.findViewById(R.id.txt_date);
            holder.cloudmoneyearnings = (LinearLayout) view.findViewById(R.id.dateamount_layot);
            holder.creditdebit_tv= (TextView) view.findViewById(R.id.creditdebit_tv);
            holder. credit_debit_date= (RelativeLayout) view.findViewById(R.id.credit_debit_date);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (CloudmoneyTransactionAdapter.ViewHolder) view.getTag();
        }

        if(!data.get(position).getRefereImage().equalsIgnoreCase("")) {
            Picasso.with(context).load(String.valueOf(data.get(position).getRefereImage())).into(holder.type_image);
        }
        if (data.get(position).getCredittype().equalsIgnoreCase("debit")) {
            System.out.println("===============cloud money debit================");
            holder. credit_debit_date.setBackgroundColor(Color.parseColor("#30ffffff"));
           //   holder.trans_price.setBackgroundResource(R.drawable.green_gradient_rv);
            holder.trans_price.setTextColor(context.getResources().getColor(R.color.yellow_color));
            holder.cloudmoneyearnings.setBackgroundResource(R.drawable.withoutradius_green);
            holder.creditdebit_tv.setText("Withdrawal");
        } else {
            System.out.println("===============cloud money credit================");
            holder. credit_debit_date.setBackgroundColor(Color.parseColor("#30ffffff"));
            holder.trans_price.setBackgroundColor(Color.parseColor("#2A62AC"));
            holder.trans_price.setTextColor(context.getResources().getColor(R.color.yellow_color));
            holder.cloudmoneyearnings.setBackgroundColor(Color.parseColor("#2A62AC"));
            holder.creditdebit_tv.setText(context.getResources().getString(R.string.Credit));
        }
        holder.trans_price.setText(data.get(position).getTrans_amount().toUpperCase());
        holder.date.setText(data.get(position).getStrDate().toUpperCase());


        return view;
    }
}
