package com.blackmaria.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.pojo.ComplaintsListPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;

import java.util.ArrayList;

/**
 * Created by user144 on 3/8/2018.
 */

public class ComplaintsListAdaptere extends BaseAdapter {

    private ArrayList<ComplaintsListPojo> data;
    LanguageDb mhelper;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Context context;
    private String typeOfComplaint = "";

    public ComplaintsListAdaptere(Context c, ArrayList<ComplaintsListPojo> d, String typeOfCase) {
        context = c;
        mhelper = new LanguageDb(context);
        mInflater = LayoutInflater.from(context);
        this.typeOfComplaint = typeOfCase;
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView subjectName, ticketNumber, dateAndStatus;
        private ImageView rewardImage, rightarrow;
        private LinearLayout cmplintlyt;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.complaintlistsingle, parent, false);

            holder = new ViewHolder();
            holder.subjectName = (TextView) view.findViewById(R.id.subname);
            holder.ticketNumber = (TextView) view.findViewById(R.id.ticketnumber);
            holder.dateAndStatus = (TextView) view.findViewById(R.id.datestatus);
            holder.cmplintlyt = (LinearLayout) view.findViewById(R.id.cmplintlyt);
            holder.rewardImage = (ImageView) view.findViewById(R.id.rewardimages);
            holder.rightarrow = (ImageView) view.findViewById(R.id.arrow);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }


        if ((position % 2) == 0) {
            holder.cmplintlyt.setBackgroundColor(context.getResources().getColor(R.color.transparant_color));
            holder.rightarrow.setImageResource(R.drawable.arrow_cmplnt);
        } else {
            holder.cmplintlyt.setBackgroundColor(Color.parseColor("#40000000"));
            holder.rightarrow.setImageResource(R.drawable.arrow_cmplnt);
        }

        if (typeOfComplaint.equalsIgnoreCase("open")) {

            holder.subjectName.setText(data.get(position).getSubName());
            holder.ticketNumber.setText(getkey("ticket_nos") + data.get(position).getTickNumer());
            if (!data.get(position).getDateTime().equalsIgnoreCase("")) {
                holder.dateAndStatus.setText(data.get(position).getDateTime());
                holder.dateAndStatus.setTextSize(9);
            }
            holder.rewardImage.setImageResource(R.drawable.folderopen);

        } else if (typeOfComplaint.equalsIgnoreCase("closed")) {

            holder.ticketNumber.setVisibility(View.GONE);
            holder.rightarrow.setVisibility(View.INVISIBLE);
            holder.subjectName.setText(data.get(position).getSubName());
            if (!data.get(position).getDateTime().equalsIgnoreCase("")) {
                holder.dateAndStatus.setText(data.get(position).getDateTime());
                holder.dateAndStatus.setTextSize(9);
            }
            holder.rewardImage.setImageResource(R.drawable.folderclose);

        } else {
            holder.ticketNumber.setText(getkey("ticket_nos") + data.get(position).getTickNumer());
            holder.subjectName.setText(data.get(position).getSubName());
            holder.dateAndStatus.setText(data.get(position).getTicketStatus());

            if (data.get(position).getTicketStatus().equalsIgnoreCase("open")) {
                holder.rightarrow.setVisibility(View.VISIBLE);
                holder.rewardImage.setImageResource(R.drawable.folderopen);
            } else {
                holder.rightarrow.setVisibility(View.INVISIBLE);
                holder.rewardImage.setImageResource(R.drawable.folderclose);
            }
        }


        return view;
    }

    public interface redeemRewards {
        void onClickRedeemList(int postion);
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}