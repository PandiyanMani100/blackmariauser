package com.blackmaria.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.blackmaria.CardListPage;
import com.blackmaria.pojo.CardDetailsPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.blackmaria.widgets.CustomTextView;

import java.util.ArrayList;

/**
 * Created by user14 on 7/10/2017.
 */
public class CardDetailsAdapter extends BaseAdapter {

    private ArrayList<CardDetailsPojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Activity context;

    public CardDetailsAdapter(Activity c, ArrayList<CardDetailsPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private ImageView Iv_Delete_Card;
        private CustomTextView Tv_card_number;
        private CustomTextView Tv_Exp_date;
        public ImageView Iv_Default_Card;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.profile_card_info_single_layout, parent, false);
            holder = new ViewHolder();
            holder.Tv_card_number = (CustomTextView) view.findViewById(R.id.card_number_lable);
            holder.Tv_Exp_date = (CustomTextView) view.findViewById(R.id.card_exp_date_lable);
            holder.Iv_Delete_Card = (ImageView) view.findViewById(R.id.card_info_delete_imageview);
            holder.Iv_Default_Card = (ImageView) view.findViewById(R.id.card_info_default_card_imageview);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        if (!data.get(position).getCard_number().equalsIgnoreCase("")) {
            holder.Iv_Delete_Card.setVisibility(View.VISIBLE);

            String cardNum=data.get(position).getCard_number().toString();
            String cardNumber = cardNum.substring(cardNum.length()-9,cardNum.length());
            cardNumber = cardNumber.substring(0,4) + "-" + cardNumber.substring(4, cardNumber.length());
            holder.Tv_card_number.setText(cardNumber);

        } else {
            holder.Iv_Delete_Card.setVisibility(View.GONE);
            holder.Tv_card_number.setText(context.getResources().getString(R.string.card_number));
        }
        if (!data.get(position).getExp_month().equalsIgnoreCase("")) {

            holder.Tv_Exp_date.setText(context.getResources().getString(R.string.exp)+data.get(position).getExp_month().toString()+"/"+data.get(position).getExp_year().toString());
        } else {
            holder.Tv_Exp_date.setText(context.getResources().getString(R.string.na));
        }
        if (!data.get(position).getCard_default().equalsIgnoreCase("yes")) {
            holder.Iv_Default_Card.setBackground(context.getResources().getDrawable(R.drawable.fare_breakup_unchecked));
        } else {
            holder.Iv_Default_Card.setBackground(context.getResources().getDrawable(R.drawable.fare_breakup_checked));
        }

        holder.Iv_Delete_Card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardListPage newProfile_page = (CardListPage) context;
                newProfile_page.deleteCard(data.get(position).getCard_id(), data.get(position).getCustomer_id(), position);
            }
        });

        holder.Iv_Default_Card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!data.get(position).getCard_default().equalsIgnoreCase("yes")) {
                    holder.Iv_Default_Card.setBackground(context.getResources().getDrawable(R.drawable.fare_breakup_checked));
                    CardListPage cardlist_page = (CardListPage) context;
                    cardlist_page.defaultCard(data.get(position).getCard_id(), position);
                }
            }
        });
        return view;
    }


}
