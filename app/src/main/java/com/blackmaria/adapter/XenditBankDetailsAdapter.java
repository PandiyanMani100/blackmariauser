package com.blackmaria.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.blackmaria.pojo.WalletMoneyPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;

import java.util.ArrayList;

/**
 * Created by user144 on 9/25/2017.
 */

public class XenditBankDetailsAdapter extends BaseAdapter {
    private ArrayList<WalletMoneyPojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Activity context;
    int zigZagColor=1;
    public XenditBankDetailsAdapter(Activity c, ArrayList<WalletMoneyPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView image;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        XenditBankDetailsAdapter.ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.xenditbank_details_single, parent, false);
            holder = new XenditBankDetailsAdapter.ViewHolder();
            holder.image = (ImageView) view.findViewById(R.id.wallet_page_single_payment_list_imageview);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (XenditBankDetailsAdapter.ViewHolder) view.getTag();
        }
            imageLoader.DisplayImage(String.valueOf(data.get(position).getXendiActiveImage()), holder.image);


        return view;
    }
}
