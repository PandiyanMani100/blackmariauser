package com.blackmaria.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.LanguageDb;
import com.blackmaria.pojo.FavoriteListPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.PkDialogtryagain;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Prem Kumar and Anitha on 11/12/2015.
 */
public class FavoriteListAdapter extends BaseAdapter {

    private Dialog dialog;
    LanguageDb mhelper;
    private ArrayList<FavoriteListPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    private ServiceRequest mRequest;
    SessionManager session;
    String UserID = "";
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    String pages = "";

    public FavoriteListAdapter(Context c,String sPage, ArrayList<FavoriteListPojo> d) {
        context = c;
        mhelper = new LanguageDb(context);
        mInflater = LayoutInflater.from(context);
        data = d;
        pages = sPage;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView title;
        private TextView locationName,go;
        private ImageView removeLocation;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.favourite_list_single_constrain, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) view.findViewById(R.id.favourite_list_single_title);
            holder.go = (TextView) view.findViewById(R.id.go);
            holder.locationName = (TextView) view.findViewById(R.id.favourite_list_single_location_name);
            holder.removeLocation = (ImageView) view.findViewById(R.id.favourite_list_single_forward_arrow_icon);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }


        session = new SessionManager(context);
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);


        holder.removeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                cd = new ConnectionDetector(context);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    Alertremove("CONFIRMATION", getkey("deletemy")+" " + data.get(position).getTitle() + " ?", data.get(position).getLocation_key(), position);

//                    postRequest_FavoriteDelete(Iconstant.favoritelist_delete_url, data.get(position).getLocation_key(), position);

                } else {

                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));

                }


            }
        });


        if(pages.equals("booking3"))
        {
holder.go.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.go.setVisibility(View.INVISIBLE);
        }

        holder.title.setText(data.get(position).getTitle());
        holder.locationName.setText(data.get(position).getAddress());

        return view;
    }


    private void Alertremove(final String confirmation, String title, final String alert, final int position) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(context);
        mDialog.setDialogTitle(confirmation);
        mDialog.setDialogMessage(title);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                postRequest_FavoriteDelete(Iconstant.favoritelist_delete_url, alert, position);
            }
        });
        mDialog.show();
    }


    private void postRequest_FavoriteDelete(String Url, final String locationKey, final int position) {

        dialog = new Dialog(context);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));
        System.out.println("-------------Favourite List Url----------------" + Url);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("location_key", locationKey);

        mRequest = new ServiceRequest(context);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("-------------Favourite Delete Response----------------" + response);

                String Sstatus = "", Smessage = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        data.remove(position);

                        notifyDataSetChanged();

                        //code to show empty layout
                        if (data.size() == 0) {

                            Intent refresh_list = new Intent();
                            refresh_list.setAction("com.favoriteList.refresh");
                            context.sendBroadcast(refresh_list);

                        }

                        Alert(getkey("action_success"), Smessage);

                    } else {
                        Alert(getkey("alert_label_title"), Smessage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}


