package com.blackmaria.adapter;

import android.app.Activity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.pojo.RateCard_CarPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;

import java.util.ArrayList;

/**
 * Created by user14 on 2/8/2017.
 */

public class RatecardCategoryAdapter extends BaseAdapter {

    private ArrayList<RateCard_CarPojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Activity context;

    public RatecardCategoryAdapter(Activity c, ArrayList<RateCard_CarPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView image;
        private TextView name;
        private RelativeLayout select;
        private LinearLayout Ll_car;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.ratecard_category_single, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.ratecard_single_carname);
            holder.image = (ImageView) view.findViewById(R.id.ratecard_single_car_image);
            holder.Ll_car = (LinearLayout) view.findViewById(R.id.ratecard_single_car_layout);
            holder.select = (RelativeLayout) view.findViewById(R.id.ratecard_category_layout);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        holder.name.setText(data.get(position).getCategory());
        imageLoader.DisplayImage(String.valueOf(data.get(position).getIcon_car_image()), holder.image);

        if ("1".equalsIgnoreCase(data.get(position).getSelectStatus().toString())){
            holder.select.setBackgroundColor(context.getResources().getColor(R.color.drawer_secondary_color));
        }else{
            holder.select.setBackground(context.getResources().getDrawable(R.drawable.edittext_background_border1));
        }

        //Code to adjust car at center
        Display display = context.getWindowManager().getDefaultDisplay();
        if (data.size() == 1) {
            ViewGroup.LayoutParams params = holder.Ll_car.getLayoutParams();
            params.width = (display.getWidth()) - 8;
            holder.Ll_car.setLayoutParams(params);
        } else if (data.size() == 2) {
            ViewGroup.LayoutParams params = holder.Ll_car.getLayoutParams();
            params.width = (display.getWidth() / 2) - 8;
            holder.Ll_car.setLayoutParams(params);
        } else if(data.size() == 3) {
            ViewGroup.LayoutParams params = holder.Ll_car.getLayoutParams();
            params.width = (display.getWidth() / 3) - 8;
            holder.Ll_car.setLayoutParams(params);
        }else{
            ViewGroup.LayoutParams params = holder.Ll_car.getLayoutParams();
            params.width = (display.getWidth() / 4) - 8;
            holder.Ll_car.setLayoutParams(params);
        }

        return view;
    }
}
