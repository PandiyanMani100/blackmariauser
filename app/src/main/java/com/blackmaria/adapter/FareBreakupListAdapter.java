package com.blackmaria.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.pojo.FareBreakupPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;

import java.util.ArrayList;

/**
 * Created by user14 on 12/22/2016.
 */

public class FareBreakupListAdapter extends BaseAdapter {

    private ArrayList<FareBreakupPojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Context context;
//    private Typeface face;

    public FareBreakupListAdapter(Context c, ArrayList<FareBreakupPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView title, value;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {

            view = mInflater.inflate(R.layout.farebreakup_list_single, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) view.findViewById(R.id.farebreakup_list_single_title);
            holder.value = (TextView) view.findViewById(R.id.farebreakup_list_single_price);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
//        face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
        if (position == data.size() - 1) {

            if (position != 0) {
                holder.title.setSingleLine(false);
                holder.title.setTextColor(Color.BLACK);
                holder.value.setTextColor(Color.BLACK);
//
//                holder.title.setTypeface(face, Typeface.BOLD);
//                holder.value.setTypeface(face, Typeface.BOLD);
            } else {
                holder.title.setSingleLine(true);
                holder.title.setTextColor(Color.BLACK);
                holder.value.setTextColor(Color.BLACK);

//                holder.title.setTypeface(face, Typeface.NORMAL);
//                holder.value.setTypeface(face, Typeface.NORMAL);
            }
        } else {
            holder.title.setTextColor(Color.BLACK);
            holder.value.setTextColor(Color.BLACK);
            holder.title.setSingleLine(true);
//            holder.title.setTypeface(face, Typeface.NORMAL);
//            holder.value.setTypeface(face, Typeface.NORMAL);
        }
        if (position == data.size() - 1) {
            holder.title.setText(data.get(position).getTitle().toUpperCase());
            holder.value.setText(data.get(position).getValue().toUpperCase());
        } else {
            holder.title.setText(data.get(position).getTitle().toUpperCase() + ".....................................................");
            holder.value.setText(data.get(position).getValue().toUpperCase());
        }


      /*  if(position==data.size()-1 && position!=0){
            Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/roboto-condensed.bold.ttf");
            holder.title.setTypeface(face,Typeface.BOLD);
            holder.value.setTypeface(face,Typeface.BOLD);

        }
            holder.title.setText(data.get(position).getTitle().toUpperCase());
            holder.value.setText(data.get(position).getValue().toUpperCase());*/


        return view;
    }
}


