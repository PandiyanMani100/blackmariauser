package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.blackmaria.pojo.RatingPojo;
import com.blackmaria.R;

import java.util.ArrayList;


/**
 * Created by GANESH on 23/08/2017.
 */
public class RatingAdapterNew extends RecyclerView.Adapter<RatingAdapterNew.MyViewHolder> {

    private ArrayList<RatingPojo> data;
    private LayoutInflater mInflater;
    private Context context;


    public RatingAdapterNew(Context c, ArrayList<RatingPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.myride_rating_single, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.title.setText(data.get(position).getRatingName());
        holder.rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                data.get(position).setRatingcount(String.valueOf(rating));
//                notifyItemChanged(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private RatingBar rating;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.myride_rating_single_title);
            rating = (RatingBar) view.findViewById(R.id.myride_rating_single_ratingbar);
        }

    }


}


