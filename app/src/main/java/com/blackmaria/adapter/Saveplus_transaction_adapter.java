package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.blackmaria.pojo.Trans;
import com.blackmaria.R;
import com.blackmaria.utils.SessionManager;

import java.util.ArrayList;


/**
 * Created by user129 on 3/10/2017.
 */
public class Saveplus_transaction_adapter extends RecyclerView.Adapter<Saveplus_transaction_adapter.MyViewHolder> {
    private ArrayList<Trans> data;
    private LayoutInflater mInflater;
    private Context context;
    private SessionManager sessionManager;


    public Saveplus_transaction_adapter(Context c, ArrayList<Trans> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        sessionManager = new SessionManager(c);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.saveplus_childview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Trans contact = data.get(position);
        holder.tv_date.setText(contact.getTrans_date());
        if(contact.getFrom().equalsIgnoreCase("")){
            holder.tv_from.setText("--");
        }else{
            holder.tv_from.setText(contact.getFrom());
        }

        holder.tv_type.setText(contact.getType());
        holder.tv_amount.setText(sessionManager.getCurrency() + " " + contact.getAmount());
        if ((position & 1) == 0) {
            holder.childview.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.childview.setBackgroundColor(context.getResources().getColor(R.color.saveplus_bottombg));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_date, tv_from, tv_type, tv_amount;
        private LinearLayout childview;

        public MyViewHolder(View view) {
            super(view);
            tv_date = view.findViewById(R.id.tv_date);
            tv_from = view.findViewById(R.id.tv_from);
            tv_type = view.findViewById(R.id.tv_type);
            tv_amount = view.findViewById(R.id.tv_amount);
            childview = view.findViewById(R.id.childview);
        }

    }


}