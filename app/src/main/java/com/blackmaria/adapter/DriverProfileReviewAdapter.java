package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;

import com.blackmaria.pojo.DriverProfileReviewPojo;
import com.blackmaria.R;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.widgets.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user14 on 12/27/2016.
 */

public class DriverProfileReviewAdapter extends BaseAdapter
{

    private ArrayList<DriverProfileReviewPojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public DriverProfileReviewAdapter(Context c, ArrayList<DriverProfileReviewPojo> d)
    {
        context=c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount()
    {
        return 1;
    }


    public class ViewHolder
    {
        private CustomTextView name,date,commant;
        private RoundedImageView user_img;
        private RatingBar user_rating;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.driver_profile_reviewlist_single, parent, false);
            holder = new ViewHolder();
            holder.name = (CustomTextView) view.findViewById(R.id.driver_profile_username_textview);
            holder.date = (CustomTextView) view.findViewById(R.id.driver_profile_user_rating_date_textview);
            holder.commant = (CustomTextView) view.findViewById(R.id.driver_profile_user_commant_textview);
            holder.user_img = (RoundedImageView) view.findViewById(R.id.driver_profile_user_imageview);
            holder.user_rating = (RatingBar) view.findViewById(R.id.driver_profile_user_ratingbar);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        Picasso.with(context)
                .load(data.get(position).getUser_image())
                .into(holder.user_img);
        holder.name.setText(data.get(position).getUser_name());
        holder.date.setText(data.get(position).getDate());
        holder.commant.setText(data.get(position).getComment());
        holder.user_rating.setRating(Float.parseFloat(data.get(position).getRating()));

       /* view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DriverProfilePage driverProfilePage = (DriverProfilePage) context;
                driverProfilePage.reviewDialog(data.get(position).getUser_name(),data.get(position).getUser_image(),data.get(position).getDate(),data.get(position).getRating(),data.get(position).getComment());
            }
        });*/

        return view;
    }
}

