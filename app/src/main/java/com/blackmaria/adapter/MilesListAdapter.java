package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.blackmaria.pojo.MilesListPojo;
import com.blackmaria.R;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.CustomTextViewForm;

import java.util.ArrayList;

/**
 * Created by user129 on 5/22/2017.
 */
public class MilesListAdapter extends BaseAdapter {


    private static ArrayList<MilesListPojo> data;
    private LayoutInflater mInflater;
    private Context context;


    public MilesListAdapter(Context c, ArrayList<MilesListPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private CustomTextViewForm Tv_date, Tv_details, Tv_refId;
        private CustomTextView Tv_miles;
//        private LinearLayout Ll_background;
        private RelativeLayout Ll_background;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        final ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.miles_listview_item_single, parent, false);
            holder = new ViewHolder();

            holder.Tv_date = (CustomTextViewForm) view.findViewById(R.id.miles_listitem_datetime);
            holder.Tv_details = (CustomTextViewForm) view.findViewById(R.id.miles_listitem_details);
            holder.Tv_miles = (CustomTextView) view.findViewById(R.id.miles_listitem_miles);
//            holder.Tv_refId = (CustomTextViewForm) view.findViewById(R.id.miles_listitem_refId);
            holder.Ll_background = (RelativeLayout) view.findViewById(R.id.list_content);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

//        if((position % 2)==0){
        holder.Ll_background.setBackgroundColor(context.getResources().getColor(R.color.miles_list_shadow_color));
//        }else{
//
//            holder.Ll_background.setBackgroundColor(context.getResources().getColor(R.color.transparant_color));
//
//        }
        holder.Tv_date.setText(data.get(position).getRide_date() + " - " + data.get(position).getRide_time());

//      holder.Tv_date.setText(data.get(position).getRide_datetime());
        holder.Tv_details.setText(data.get(position).getRide_details().replace("Ride Id#", "CRN "));
        holder.Tv_miles.setText(data.get(position).getRide_distance());
//        holder.Tv_refId.setText(data.get(position).getRide_refname());
        return view;
    }
}
