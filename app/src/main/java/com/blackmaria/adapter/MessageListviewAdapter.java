package com.blackmaria.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.pojo.MessagePojo;
import com.blackmaria.R;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


/**
 * Created by CAS64 on 4/21/2017.
 */

public class MessageListviewAdapter extends BaseAdapter {
    Context context;
    String[] mTitle;
    LayoutInflater inflater;
    View itemView;
    int flag;

    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private Boolean isInternetPresent = false;
    private SessionManager sessionManager;
    private String UserId="";
    ArrayList<MessagePojo> data;
    public MessageListviewAdapter(Context context, String[] title, int flag,ArrayList<MessagePojo> d) {
        this.context = context;
        this.mTitle = title;
        this.flag = flag;
        this.data=d;

        cd = new ConnectionDetector(context);
        isInternetPresent = cd.isConnectingToInternet();
        sessionManager=new SessionManager(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        UserId = user.get(SessionManager.KEY_USERID);
    }

    @Override
    public int getCount() {
        return mTitle.length;
    }

    @Override
    public Object getItem(int i) {
        return mTitle.length;
    }

    @Override
    public long getItemId(int i) {

        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        TextView Smessage_title_textview,message_date;
        LinearLayout remove_rl;
        RelativeLayout Smessage_listview_title_layout;

                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemView = inflater.inflate(R.layout.message_listview_adaper, viewGroup, false);
        remove_rl = (LinearLayout) itemView.findViewById(R.id.remove_rl);
        Smessage_title_textview = (TextView) itemView.findViewById(R.id.message_title_textview);
        Smessage_listview_title_layout = (RelativeLayout) itemView.findViewById(R.id.message_listview_title_layout);

        message_date = (TextView) itemView.findViewById(R.id.message_date);

            remove_rl.setVisibility(View.VISIBLE);
          remove_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postData2(data.get(i).getNotification_id(),i);
            }
        });
      /*  if (i % 2 != 0) {
            Smessage_listview_title_layout.setBackgroundResource(R.color.app_bg_dark_blue);
        }*/
        message_date.setText(data.get(i).getDate());
        Smessage_title_textview.setText(mTitle[i]);
        return itemView;
    }

    private void postData2(final String notifiId,final int postion) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", UserId);
        jsonParams.put("notification_id", notifiId);

        if (isInternetPresent) {
            postRequest_MessageDelete(Iconstant.message_delete, jsonParams,postion);
        } else {
            Alert(context.getResources().getString(R.string.action_error), context.getResources().getString(R.string.alert_nointernet));

        }
    }
    private void postRequest_MessageDelete(String Url, HashMap<String, String> jsonParams,final int postion) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(context.getResources().getString(R.string.action_loading));
        System.out.println("-------------Message  Url----------------" + Url);
        mRequest = new ServiceRequest(context);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Message  Response----------------" + response);
                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "", ScurrencySymbol = "", Str_total_amount = "", Str_total_transaction = "";
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        String Sresponse = object.getString("response");

                        data.remove(postion);
                        List<String> list = new ArrayList<String>(Arrays.asList(mTitle));
                        list.remove(postion);
                        mTitle = list.toArray(new String[0]);
                        notifyDataSetChanged();


                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    } else {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        String Sresponse = object.getString("response");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(context.getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
}
