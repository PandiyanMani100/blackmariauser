package com.blackmaria.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.LanguageDb;
import com.blackmaria.pojo.MenuHomeratingPojo;
import com.blackmaria.R;
import com.blackmaria.RideDetailPage;
import com.blackmaria.utils.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 9/2/2017.
 */

public class MenuHomeListRatingAdapter extends BaseAdapter {
    Context cxt;
    private LayoutInflater mInflater;
    LanguageDb mhelper;
    ArrayList<MenuHomeratingPojo> itemlist;
    String UserId = "";

    public MenuHomeListRatingAdapter(Context cxt, ArrayList<MenuHomeratingPojo> itemlist, String userId) {
        this.cxt = cxt;
        this.itemlist = itemlist;
        mInflater = LayoutInflater.from(cxt);
        this.UserId = userId;
        mhelper = new LanguageDb(cxt);
    }

    @Override
    public int getCount() {
        return itemlist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private TextView title, comments,tapInfo;
        private RoundedImageView rating_page_profilephoto1;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.menu_ride_rating_list_single, parent, false);
            holder = new ViewHolder();

            holder.title =  view.findViewById(R.id.date_name);
            holder.comments =  view.findViewById(R.id.comments);
            holder.rating_page_profilephoto1 =  view.findViewById(R.id.rating_page_profilephoto1);
            holder.tapInfo =  view.findViewById(R.id.info_btn);
            holder.tapInfo.setText(getkey("trip_infos"));
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        Picasso.with(cxt).load(itemlist.get(position).getDriverImage()).into(holder.rating_page_profilephoto1);
        holder.title.setText(itemlist.get(position).getDriverName() + cxt.getResources().getString(R.string.on)+ itemlist.get(position).getReviewDate1());
        holder.comments.setText(itemlist.get(position).getDriveromments());

        holder.tapInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String credittype = itemlist.get(position).getRideId1();
                if (!credittype.equalsIgnoreCase("")) {
                    Intent intent = new Intent(cxt, RideDetailPage.class);
                    intent.putExtra("ride_id", itemlist.get(position).getRideId1());
                    intent.putExtra("user_id", UserId);
                    intent.putExtra("class", "MenuUserratingList");
                    cxt.startActivity(intent);
                    ((Activity) cxt).finish();
                    ((Activity) cxt).overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });

        return view;
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}