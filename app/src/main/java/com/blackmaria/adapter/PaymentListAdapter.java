package com.blackmaria.adapter;

import android.app.Activity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.blackmaria.pojo.HomePojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user14 on 3/14/2017.
 */

public class PaymentListAdapter extends BaseAdapter {

    private static ArrayList<HomePojo> data;
    private LayoutInflater mInflater;
    private Activity context;
    private ImageLoader imageLoader;

    public PaymentListAdapter(Activity c, ArrayList<HomePojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private ImageView Iv_payment_type;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.payment_list_single_page, parent, false);
            holder = new ViewHolder();

            holder.Iv_payment_type = (ImageView) view.findViewById(R.id.payment_list_single_imageview);


            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        if ("false".equalsIgnoreCase(data.get(position).getSetPayment_selected_payment_id())) {
            Picasso.with(context).load(data.get(position).getPayment_icon()).into(holder.Iv_payment_type);
        } else {
            Picasso.with(context).load(data.get(position).getSetpaymentinactiveicon()).into(holder.Iv_payment_type);
        }
        //Code to adjust car at center
        Display display = context.getWindowManager().getDefaultDisplay();
        ViewGroup.LayoutParams params = holder.Iv_payment_type.getLayoutParams();
        System.out.println("======display.getWidth====="+display.getWidth());

       /* if (data.size() == 1) {
            params.width = (display.getWidth()) - 8;
            holder.Iv_payment_type.setLayoutParams(params);
        } else if (data.size() == 2) {
            params.width = (display.getWidth() / 2) - 8;
            holder.Iv_payment_type.setLayoutParams(params);
        }
        else if (data.size() == 3) {
            params.width = (display.getWidth() / 3) - 8;
            holder.Iv_payment_type.setLayoutParams(params);
        } else if (data.size() == 4) {
            params.width = (display.getWidth() / 4) - 8;
            holder.Iv_payment_type.setLayoutParams(params);
        }
        else {
            params.width = (display.getWidth() / 5) - 8;
            holder.Iv_payment_type.setLayoutParams(params);
        }*/

        return view;
    }


}

