package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.blackmaria.pojo.CloudMoneyHomePojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.widgets.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 5/18/2017.
 */

public class ReferanceUserAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    private static ArrayList<CloudMoneyHomePojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public ReferanceUserAdapter(Context c, ArrayList<CloudMoneyHomePojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private RoundedImageView IV_refere_user;
        private CustomTextView Rl_list_single;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.cloudmoney_referance_adapter, parent, false);
            holder = new ViewHolder();
            holder.IV_refere_user = (RoundedImageView) view.findViewById(R.id.ref_user_image);
            holder.Rl_list_single = (CustomTextView) view.findViewById(R.id.refusername);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        try {
            Picasso.with(context).load(String.valueOf(data.get(position).getUserimage())).error(context.getResources().getDrawable(R.drawable.usericon_getpaid)).into(holder.IV_refere_user);
            holder.Rl_list_single.setText(data.get(position).getUsername().toUpperCase());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

}
