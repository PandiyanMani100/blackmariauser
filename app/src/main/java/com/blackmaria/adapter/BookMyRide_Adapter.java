package com.blackmaria.adapter;

/**
 */

import android.app.Activity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.pojo.HomePojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;

import java.util.ArrayList;


public class BookMyRide_Adapter extends BaseAdapter {

    private ArrayList<HomePojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Activity context;

    public BookMyRide_Adapter(Activity c, ArrayList<HomePojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView image;
        private TextView name;
        private TextView time;
        private LinearLayout Ll_car;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.bookmyride_single, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.bookmyride_single_carname);
            holder.time = (TextView) view.findViewById(R.id.bookmyride_single_time);
            holder.image = (ImageView) view.findViewById(R.id.bookmyride_single_car_image);
            holder.Ll_car = (LinearLayout) view.findViewById(R.id.bookmyride_single_car_layout);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        holder.name.setText(data.get(position).getCat_name());
         holder.time.setText(data.get(position).getCat_time());

        if (data.get(position).getSelected_Cat().equalsIgnoreCase(data.get(position).getCat_id())) {
            imageLoader.DisplayImage(String.valueOf(data.get(position).getIcon_active()), holder.image);
        } else {
            imageLoader.DisplayImage(String.valueOf(data.get(position).getIcon_normal()), holder.image);
        }


        //Code to adjust car at center
        Display display = context.getWindowManager().getDefaultDisplay();
        ViewGroup.LayoutParams params = holder.Ll_car.getLayoutParams();
        System.out.println("======display.getWidth====="+display.getWidth());

        if (data.size() == 1) {
            params.width = (display.getWidth()) - 8;
            holder.Ll_car.setLayoutParams(params);
        } else if (data.size() == 2) {
            params.width = (display.getWidth() / 2) - 18;
            holder.Ll_car.setLayoutParams(params);
        } else if (data.size() == 3){
            if (display.getWidth() >= 1000) {
                params.width = (display.getWidth() / 3) - 24;
            }else{
                params.width = (display.getWidth() / 3) - 20;
            }
            holder.Ll_car.setLayoutParams(params);

        }else /*if (data.size() == 4)*/{
            if (display.getWidth() >= 1000) {
                params.width = (display.getWidth() / 4) - 32;
            }else{
                params.width = (display.getWidth() / 4) - 20;
            }
            holder.Ll_car.setLayoutParams(params);
        }
        return view;
    }
}