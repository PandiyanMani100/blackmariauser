package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;

import com.blackmaria.pojo.MenuHomeratingPojo;
import com.blackmaria.R;
import com.blackmaria.widgets.CustomTextView;

import java.util.ArrayList;

/**
 * Created by user144 on 9/2/2017.
 */

public class MenuHomeRateAdapter extends BaseAdapter {
    Context cxt;
    private LayoutInflater mInflater;
    ArrayList<MenuHomeratingPojo> itemlist ;
    public MenuHomeRateAdapter(Context cxt, ArrayList<MenuHomeratingPojo> itemlist) {
        this.cxt=cxt;
        this.itemlist=itemlist;
        mInflater = LayoutInflater.from(cxt);
    }

    @Override
    public int getCount() {
        return itemlist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount()
    {
        return 1;
    }
    public class ViewHolder {
        private CustomTextView title;
        private RatingBar ratingbar;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.menu_rating_list_single_home, parent, false);
            holder = new ViewHolder();
            holder.title = (CustomTextView) view.findViewById(R.id.myride_rating_single_title);
            holder.ratingbar=(RatingBar)view.findViewById(R.id.myride_rating_single_ratingbar4);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.title.setText(itemlist.get(position).getRatingTitle());
        holder.ratingbar.setRating(Float.parseFloat(itemlist.get(position).SetRatingAvarage()));
        return view;
    }
}
