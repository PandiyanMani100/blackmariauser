package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.pojo.BookingPojo1;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

/**
 * Created by user14 on 3/14/2017.
 */

public class CarListAdapter extends BaseAdapter {

    private ArrayList<BookingPojo1> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Context context;

    public CarListAdapter(Context c, ArrayList<BookingPojo1> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView Iv_carType;
        private TextView Tv_distance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.car_list_dialog_single, parent, false);
            holder = new ViewHolder();

            holder.Iv_carType = (ImageView) view.findViewById(R.id.car_list_single_car_type_imageview);
            holder.Tv_distance = (TextView)view.findViewById(R.id.car_list_single_distace_textview);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.Tv_distance.setText(data.get(position).getDistance());


        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.car_map_icon)
                .error(R.drawable.car_map_icon)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(data.get(position).getBrand_image()).apply(options).into(holder.Iv_carType);
//        imageLoader.DisplayImage(String.valueOf(data.get(position).getBrand_image()), holder.Iv_carType);

        return view;
    }
}
