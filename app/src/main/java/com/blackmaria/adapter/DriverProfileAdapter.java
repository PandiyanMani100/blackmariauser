package com.blackmaria.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.pojo.DriverProfilePojo;
import com.blackmaria.R;

import java.util.ArrayList;

/**
 * Created by Prem Kumar and Anitha on 12/23/2016.
 */

public class DriverProfileAdapter extends BaseAdapter {
    Typeface tf;
    private ArrayList<DriverProfilePojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public DriverProfileAdapter(Context c, ArrayList<DriverProfilePojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView title, value, driver_profile_driver_profile_lable;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.driver_profile_page_single, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) view.findViewById(R.id.driver_profile_single_title_textView);
            holder.value = (TextView) view.findViewById(R.id.driver_profile_single_title_value);


            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.title.setText(data.get(position).getTitle().toUpperCase());
        if(data.get(position).getValue().equalsIgnoreCase("-")){
        }else{
            holder.value.setText(":"+data.get(position).getValue().toUpperCase());
        }

        return view;
    }
}

