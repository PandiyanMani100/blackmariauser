package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.pojo.MultiDropPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;

import java.util.ArrayList;

/**
 * Created by user144 on 2/23/2018.
 */

public class MultiDrop_Adapter extends BaseAdapter {

    private ArrayList<MultiDropPojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Context context;
    deleteMultiDrop deleteDropLocation;

    public MultiDrop_Adapter(Context c, ArrayList<MultiDropPojo> d, deleteMultiDrop deleteDropLocation) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
        this.deleteDropLocation = deleteDropLocation;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView dropoff_lable, booking_drop_address_textview;
        private ImageView deleteLocation;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.multidrop_single, parent, false);
            holder = new ViewHolder();
            holder.deleteLocation = (ImageView) view.findViewById(R.id.home_image);
            holder.dropoff_lable = (TextView) view.findViewById(R.id.dropoff_lable);
            holder.booking_drop_address_textview = (TextView) view.findViewById(R.id.booking_drop_address_textview);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        int pos=position+1;
        holder.dropoff_lable.setText("STOP OVER "+pos);
        holder.booking_drop_address_textview.setText(data.get(position).getPlaceName());

        holder.deleteLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDropLocation.OnDeleteLocation(position);
            }
        });
        return view;
    }


    public interface deleteMultiDrop {
        void OnDeleteLocation(int deletePostion);
    }

}
