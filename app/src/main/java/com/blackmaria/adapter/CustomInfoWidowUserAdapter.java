package com.blackmaria.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.blackmaria.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by user144 on 12/15/2017.
 */

public class CustomInfoWidowUserAdapter implements GoogleMap.InfoWindowAdapter {
    View mWindow, mContents;
    private Activity context;

    public CustomInfoWidowUserAdapter(Activity cxt ) {
        this.context = cxt;
        mWindow = context.getLayoutInflater().inflate(R.layout.info_window_userdetails, null);
        mContents = context.getLayoutInflater().inflate(R.layout.info_window_userdetails, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        render(marker, mWindow);
        return mWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {
        render(marker, mContents);
        return mContents;
    }


    private void render(Marker marker, View view) {
        // Use the equals() method on a Marker to check for equals.  Do not use ==.
      //  String title = marker.getTitle();

        String snippet = marker.getSnippet();

        TextView userName = ((TextView) view.findViewById(R.id.username));
        TextView tripType = ((TextView) view.findViewById(R.id.tripType));
        TextView vehicleType = ((TextView) view.findViewById(R.id.vehicle_type));

        TextView tripType_tv = ((TextView) view.findViewById(R.id.tripType_tv));
        TextView vehicleType_tv = ((TextView) view.findViewById(R.id.vehicle_type_tv));


        if (snippet != null) {
           String[] userDetails=snippet.split(",");
            userName.setText(userDetails[0]);
            tripType.setText(userDetails[1]);
            vehicleType.setText(userDetails[2]);
            tripType_tv.setText(context.getResources().getString(R.string.trip_lable)+"         :");
            vehicleType_tv.setText("VEHICLE  :");
        } else {
            userName.setText("");
            tripType.setText("");
            vehicleType.setText("");
            tripType_tv.setText("");
            vehicleType_tv.setText("");
        }
    }
}
