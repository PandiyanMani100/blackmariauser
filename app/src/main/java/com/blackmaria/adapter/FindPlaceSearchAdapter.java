package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 6/27/2017.
 */

public class FindPlaceSearchAdapter extends BaseAdapter {

    private ArrayList<String> data;
    private ArrayList<String> iconImage;
    private ArrayList<String> distances;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Context context;

    public FindPlaceSearchAdapter(Context c, ArrayList<String> d, ArrayList<String> icon,ArrayList<String> distance) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        iconImage = icon;
        imageLoader = new ImageLoader(context);
        distances=distance;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {

        private TextView name,place_search_single_textview_km;
        private ImageView iconImage;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        FindPlaceSearchAdapter.ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.place_search_single_find, parent, false);
            holder = new FindPlaceSearchAdapter.ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.place_search_single_textview);
            holder.iconImage = (ImageView) view.findViewById(R.id.place_search_imageview);
            holder. place_search_single_textview_km= (TextView) view.findViewById(R.id.place_search_single_textview_km);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (FindPlaceSearchAdapter.ViewHolder) view.getTag();
        }
        Picasso.with(context).load(iconImage.get(position)).into(holder.iconImage);
        holder.name.setText(data.get(position).toUpperCase());
        holder.place_search_single_textview_km.setText(distances.get(position)+context.getResources().getString(R.string.homepage_km));
        return view;
    }
}
