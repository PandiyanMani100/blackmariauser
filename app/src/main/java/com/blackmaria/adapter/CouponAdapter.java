package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.pojo.CouponPojo;
import com.blackmaria.R;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.VerticalTextView;

import java.util.ArrayList;

import static com.blackmaria.CouponPage.Spage;

/**
 * Created by user14 on 12/2/2016.
 */

public class CouponAdapter extends BaseAdapter {

    private static ArrayList<CouponPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    ViewHolder holder;
    int positionSelect;
    CouponClickInterface couponImterface;

    public CouponAdapter(Context c, ArrayList<CouponPojo> d, CouponClickInterface couponImterface) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        this.couponImterface = couponImterface;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private TextView Tv_couponCode, Tv_expiryDate, Tv_couponFlat, Tv_offLable, Tv_couponAmount, Tv_amount, Tv_expDate, Redeemnow;
        private ImageView Iv_barCode;
        private RelativeLayout Rl_flat, Rl_amount;


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;

        positionSelect = position;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.coupon_code_singlepage_new, parent, false);
            holder = new ViewHolder();

            holder.Rl_flat = (RelativeLayout) view.findViewById(R.id.coupon_single_flatt_layout);
            holder.Rl_amount = (RelativeLayout) view.findViewById(R.id.coupon_single_amount_layout);

            holder.Tv_expiryDate = (VerticalTextView) view.findViewById(R.id.coupon_code_single_coupon_expiry_textview);

            holder.Tv_couponCode = (CustomTextView) view.findViewById(R.id.coupon_code_coupon_textview);

//            holder.Tv_couponFlat = (CustomTextView) view.findViewById(R.id.coupon_single_flat_textview);

            holder.Tv_couponAmount = (TextView) view.findViewById(R.id.coupon_code_single_amount_textview);

//            holder.Tv_offLable = (CustomTextView) view.findViewById(R.id.coupon_single_flat_off_lable);
            holder.Iv_barCode = (ImageView) view.findViewById(R.id.coupon_single_bar_code_imageview);


            holder.Tv_amount = (TextView) view.findViewById(R.id.couponamount);
            holder.Tv_expDate = (TextView) view.findViewById(R.id.expdate);
            holder.Redeemnow = (TextView) view.findViewById(R.id.redeemnow);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        holder.Tv_couponCode.setText(data.get(position).getCouponCode());
        holder.Tv_expiryDate.setText(data.get(position).getExpiryDate());
        holder.Tv_expDate.setText(data.get(position).getExpiryDate());
        if ("coupon".equalsIgnoreCase(data.get(position).getSource())) {

            if ("Percent".equalsIgnoreCase(data.get(position).getCouponType())) {
//                holder.Tv_offLable.setVisibility(View.VISIBLE);
                holder.Rl_flat.setVisibility(View.VISIBLE);
                holder.Rl_amount.setVisibility(View.GONE);
                holder.Rl_flat.setBackground(context.getResources().getDrawable(R.drawable.new_orange_percentage));
                holder.Iv_barCode.setBackground(context.getResources().getDrawable(R.drawable.new_barcode_orange));
                holder.Tv_expiryDate.setTextColor(context.getResources().getColor(R.color.couponcode_flat_txtcolor));
//                holder.Tv_couponFlat.setText(data.get(position).getCouponValue() + "%");
                holder.Tv_couponAmount.setText(data.get(position).getCouponValue() + context.getResources().getString(R.string.percentage));

                holder.Tv_amount.setText(data.get(position).getCouponValue() + context.getResources().getString(R.string.percentage));


            } else if ("Flat".equalsIgnoreCase(data.get(position).getCouponType())) {
                holder.Rl_flat.setVisibility(View.GONE);
                holder.Rl_amount.setVisibility(View.VISIBLE);
                holder.Rl_amount.setBackground(context.getResources().getDrawable(R.drawable.new_blue_dollar));
                holder.Iv_barCode.setBackground(context.getResources().getDrawable(R.drawable.new_bar_code_blue));
                holder.Tv_expiryDate.setTextColor(context.getResources().getColor(R.color.couponcode_amount_txtcolor));

                String amount = data.get(position).getCurrencyCode() + " " + data.get(position).getCouponValue();

                holder.Tv_couponAmount.setText(data.get(position).getCurrencyCode() + " " + data.get(position).getCouponValue());
                holder.Tv_amount.setText(data.get(position).getCurrencyCode() + " " + data.get(position).getCouponValue());


            }
        } else if ("welcome".equalsIgnoreCase(data.get(position).getSource())) {

            if ("percent".equalsIgnoreCase(data.get(position).getCouponType())) {
                holder.Rl_flat.setVisibility(View.VISIBLE);
                holder.Rl_amount.setVisibility(View.GONE);
                holder.Rl_flat.setBackground(context.getResources().getDrawable(R.drawable.new_orange_percentage));
                holder.Iv_barCode.setBackground(context.getResources().getDrawable(R.drawable.new_barcode_orange));
                holder.Tv_expiryDate.setTextColor(context.getResources().getColor(R.color.couponcode_flat_txtcolor));
//                holder.Tv_couponFlat.setText(data.get(position).getCouponValue() + "%");
                holder.Tv_couponAmount.setText(data.get(position).getCouponValue() + context.getResources().getString(R.string.percentage));
                holder.Tv_amount.setText(data.get(position).getCouponValue() + context.getResources().getString(R.string.percentage));
            } else if ("flat".equalsIgnoreCase(data.get(position).getCouponType())) {
                holder.Rl_flat.setVisibility(View.GONE);
                holder.Rl_amount.setVisibility(View.VISIBLE);
                holder.Rl_amount.setBackground(context.getResources().getDrawable(R.drawable.new_blue_dollar));
                holder.Iv_barCode.setBackground(context.getResources().getDrawable(R.drawable.new_bar_code_blue));
                holder.Tv_expiryDate.setTextColor(context.getResources().getColor(R.color.couponcode_amount_txtcolor));

                String amount = data.get(position).getCurrencyCode() + " " + data.get(position).getCouponValue();
                holder.Tv_couponAmount.setText(data.get(position).getCurrencyCode() + " " + data.get(position).getCouponValue());
                holder.Tv_amount.setText(data.get(position).getCurrencyCode() + " " + data.get(position).getCouponValue());
            }

        } else {
            if ("percent".equalsIgnoreCase(data.get(position).getCouponType())) {
                holder.Rl_flat.setVisibility(View.VISIBLE);
                holder.Rl_amount.setVisibility(View.GONE);
                holder.Rl_flat.setBackground(context.getResources().getDrawable(R.drawable.new_orange_percentage));
                holder.Iv_barCode.setBackground(context.getResources().getDrawable(R.drawable.new_barcode_orange));
                holder.Tv_expiryDate.setTextColor(context.getResources().getColor(R.color.couponcode_flat_txtcolor));

                holder.Tv_couponAmount.setText(data.get(position).getCouponValue() + context.getResources().getString(R.string.percentage));
                holder.Tv_amount.setText(data.get(position).getCouponValue() + context.getResources().getString(R.string.percentage));
            } else if ("flat".equalsIgnoreCase(data.get(position).getCouponType())) {
                holder.Rl_flat.setVisibility(View.GONE);
                holder.Rl_amount.setVisibility(View.VISIBLE);
                holder.Rl_amount.setBackground(context.getResources().getDrawable(R.drawable.new_blue_dollar));
                holder.Iv_barCode.setBackground(context.getResources().getDrawable(R.drawable.new_bar_code_blue));
                holder.Tv_expiryDate.setTextColor(context.getResources().getColor(R.color.couponcode_amount_txtcolor));

                String amount = data.get(position).getCurrencyCode() + " " + data.get(position).getCouponValue();
                holder.Tv_couponAmount.setText(data.get(position).getCurrencyCode() + " " + data.get(position).getCouponValue());
                holder.Tv_amount.setText(data.get(position).getCurrencyCode() + " " + data.get(position).getCouponValue());
            }
        }
        holder.Redeemnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String code = data.get(position).getCouponCode().toString();
                    String source = data.get(position).getSource().toString();
                    couponImterface.onSelectClick(code, source);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if(context.getResources().getString(R.string.profile_label_menu).equalsIgnoreCase(Spage)){
          holder.Redeemnow.setVisibility(View.GONE);
        }

        return view;
    }

    public interface CouponClickInterface {

        void onSelectClick(String code, String source);

    }
}
