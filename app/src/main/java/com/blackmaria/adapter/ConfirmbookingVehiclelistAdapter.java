package com.blackmaria.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.blackmaria.pojo.BookingPojo1;
import com.blackmaria.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;


/**
 * Created by user129 on 3/10/2017.
 */
public class ConfirmbookingVehiclelistAdapter extends RecyclerView.Adapter<ConfirmbookingVehiclelistAdapter.MyViewHolder> {
    private ArrayList<BookingPojo1> data;
    private LayoutInflater mInflater;
    private Context context;


    public ConfirmbookingVehiclelistAdapter(Context c, ArrayList<BookingPojo1> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.confirmbookingvehiclelist_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.car_map_icon)
                .error(R.drawable.car_map_icon)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(data.get(position).getBrand_image()).apply(options).into(holder.iv_car);


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_car;

        public MyViewHolder(View view) {
            super(view);
            iv_car = view.findViewById(R.id.iv_image);
        }

    }


}