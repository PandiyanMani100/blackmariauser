package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.blackmaria.pojo.ViewComplaintPojo;
import com.blackmaria.R;
import com.blackmaria.widgets.CustomTextView;

import java.util.ArrayList;

/**
 * Created by user144 on 1/27/2017.
 */
public class ViewComplaintListAdapter extends BaseAdapter {

    private ArrayList<ViewComplaintPojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public ViewComplaintListAdapter(Context c, ArrayList<ViewComplaintPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private CustomTextView Tv_user_commmnet,Tv_admin_comment,Tv_user_commmnet_date,Tv_admin_comment_date,view_complaint_comment_list_you_wrote_label;
        private RelativeLayout Tv_user_commmnet_layout,Tv_admin_comment_layout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.view_complaint_comment_list_single, parent, false);
            holder = new ViewHolder();

            holder.Tv_user_commmnet = (CustomTextView) view.findViewById(R.id.view_complaint_comment_list_user_complaint);
            holder.Tv_admin_comment = (CustomTextView) view.findViewById(R.id.view_complaint_comment_list_blackmaria_complaint);
            holder.Tv_user_commmnet_layout = (RelativeLayout) view.findViewById(R.id.view_complaint_comment_list_user_comment_layout);
            holder.Tv_admin_comment_layout = (RelativeLayout) view.findViewById(R.id.view_complaint_comment_list_admin_comment_layout);
            holder.Tv_user_commmnet_date = (CustomTextView) view.findViewById(R.id.view_complaint_comment_list_user_complaint_date);
            holder.Tv_admin_comment_date = (CustomTextView) view.findViewById(R.id.view_complaint_comment_list_blackmaria_complaint_date);
            holder.view_complaint_comment_list_you_wrote_label = (CustomTextView) view.findViewById(R.id.view_complaint_comment_list_you_wrote_label);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }



        if ("blackmaria".equalsIgnoreCase(data.get(position).getComment_by())){
            holder.Tv_admin_comment_layout.setVisibility(View.VISIBLE);
            holder.Tv_user_commmnet_layout.setVisibility(View.GONE);
            holder.Tv_admin_comment.setText(data.get(position).getComments());
            holder.Tv_admin_comment_date.setText(data.get(position).getComment_date());
        }else{
            holder.Tv_user_commmnet_layout.setVisibility(View.VISIBLE);
            holder.Tv_admin_comment_layout.setVisibility(View.GONE);
            holder.Tv_user_commmnet.setText(data.get(position).getComments());
            holder.view_complaint_comment_list_you_wrote_label.setText(data.get(position).getComment_by()+context.getResources().getString(R.string.wrote));
            holder.Tv_user_commmnet_date.setText(data.get(position).getComment_date());
        }

        return view;
    }
}

