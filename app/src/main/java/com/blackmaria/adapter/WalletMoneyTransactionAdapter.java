package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.pojo.WalletMoneyTransactionPojo;
import com.blackmaria.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class WalletMoneyTransactionAdapter extends BaseAdapter {

    private ArrayList<WalletMoneyTransactionPojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public WalletMoneyTransactionAdapter(Context c, ArrayList<WalletMoneyTransactionPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView type_image;
        private TextView type_name,time;
        LinearLayout listHeadDebit;
        private TextView trans_price, title, date, balance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.wallet_money_transaction_single1, parent, false);
            holder = new ViewHolder();
           //   holder.type_name = (TextView) view.findViewById(R.id.wallet_money_transaction_single_type_textview);
            holder.type_image = (ImageView) view.findViewById(R.id.wallet_money_transaction_single_logo);
            holder.trans_price = (TextView) view.findViewById(R.id.wallet_money_transaction_single_price);
                holder.title = (TextView) view.findViewById(R.id.wallet_money_transaction_single_description);
            holder.time = (TextView) view.findViewById(R.id.wallet_money_transaction_single_time);
            holder.date = (TextView) view.findViewById(R.id.wallet_money_transaction_single_date);
            holder.listHeadDebit=(LinearLayout)view.findViewById(R.id.list_head_debit);
        //    holder.balance = (TextView) view.findViewById(R.id.wallet_money_transaction_single_balance_price);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        if (data.get(position).getTrans_type().equalsIgnoreCase("CREDIT")) {
        } else {
        }
//        if (position % 2 == 0) {
//            holder.listHeadDebit.setBackgroundColor(Color.parseColor("#10ffffff"));
//        } else {
//            holder.listHeadDebit.setBackgroundColor(Color.parseColor("#50ffffff"));
//        }
//        holder.title.setText(/*data.get(position).getCurrencySymbol() +*/ data.get(position).getTrans_amount().toUpperCase());
        holder.trans_price.setText(data.get(position).getCurrencySymbol()+" "+data.get(position).getTrans_amount().toUpperCase());
        holder.date.setText(data.get(position).getTitle()+" - "+data.get(position).getTrans_date().toUpperCase());
//        holder.time.setText(data.get(position).getTransactionTime().toUpperCase());
       // holder.balance.setText(context.getResources().getString(R.string.wallet_money_lable_transaction_list_balance) + " " + data.get(position).getCurrencySymbol() + data.get(position).getBalance_amount());
        Picasso.with(context).load(data.get(position).getImage()).into(holder.type_image);
        return view;
    }
}


