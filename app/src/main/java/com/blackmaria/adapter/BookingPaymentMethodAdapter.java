package com.blackmaria.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blackmaria.pojo.BookingPaymentListPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 12/20/2017.
 */

public class BookingPaymentMethodAdapter extends BaseAdapter {

    private static ArrayList<BookingPaymentListPojo> data;
    private LayoutInflater mInflater;
    private Activity context;
    private ImageLoader imageLoader;


    public BookingPaymentMethodAdapter(Activity c, ArrayList<BookingPaymentListPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView Iv_e_wallet_payment_type;
        private RelativeLayout Ll_car;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        final ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.checkwalletmoney_single, parent, false);
            holder = new ViewHolder();

            holder.Iv_e_wallet_payment_type = (ImageView) view.findViewById(R.id.e_wallet_page_single_payment_list_imageview);
            holder.Ll_car = (RelativeLayout) view.findViewById(R.id.bookmyride_single_car_layout);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        if (!data.get(position).getInactiveIcon().equalsIgnoreCase("")) {
            Picasso.with(context).load(data.get(position).getInactiveIcon()).into(holder.Iv_e_wallet_payment_type);
        }

        return view;


    }
}