package com.blackmaria.adapter;

import android.app.Activity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.blackmaria.pojo.HomePojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by user144 on 8/7/2017.
 */

public class BookingPage4Adapter extends BaseAdapter {

    private static ArrayList<HomePojo> data;
    private LayoutInflater mInflater;
    private Activity context;
    private ImageLoader imageLoader;

    public BookingPage4Adapter(Activity c, ArrayList<HomePojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private ImageView Iv_payment_type;
        private TextView payment_tv;
        private RadioButton radia_id2;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.bookinpage4adapter, parent, false);
            holder = new ViewHolder();

            holder.Iv_payment_type = (ImageView) view.findViewById(R.id.payment_list_single_imageview);
            holder.payment_tv=(TextView)view.findViewById(R.id.payment_tv);
            holder.radia_id2=(RadioButton)view.findViewById(R.id.radia_id2);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        if(data.get(position).getCat_image().equals("1"))
        {
            holder.radia_id2.setChecked(true);
        }
        else
        {
            holder.radia_id2.setChecked(false);
        }
        holder.payment_tv.setText(data.get(position).getPayment_name().toUpperCase());
        if ("Cash".equalsIgnoreCase(data.get(position).getPayment_name())) {
           // holder.payment_tv.setVisibility(View.GONE);
        } else {

           // holder.payment_tv.setText(data.get(position).getPayment_name().toUpperCase());
           // holder.payment_tv.setVisibility(View.GONE);
        }
        if ("false".equalsIgnoreCase(data.get(position).getSetPayment_selected_payment_id())) {
                Picasso.with(context).load(data.get(position).getPayment_icon()).into(holder.Iv_payment_type);
        } else {
            if(!data.get(position).getSetpaymentinactiveicon().equalsIgnoreCase(""))
                Picasso.with(context).load(data.get(position).getSetpaymentinactiveicon()).into(holder.Iv_payment_type);
        }

        holder.radia_id2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(""+position);
            }
        });

        holder.Iv_payment_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(""+position);
            }
        });

        //Code to adjust car at center
        Display display = context.getWindowManager().getDefaultDisplay();
        ViewGroup.LayoutParams params = holder.Iv_payment_type.getLayoutParams();
        System.out.println("======display.getWidth====="+display.getWidth());

        return view;
    }


}
