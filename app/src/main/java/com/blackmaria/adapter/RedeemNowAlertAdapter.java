package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.blackmaria.pojo.RedeemListPojo;
import com.blackmaria.R;
import com.blackmaria.widgets.CustomTextView;

import java.util.ArrayList;

/**
 * Created by user14 on 12/19/2016.
 */

public class RedeemNowAlertAdapter extends BaseAdapter {

    private ArrayList<RedeemListPojo> listData;

    private LayoutInflater layoutInflater;

    public RedeemNowAlertAdapter(Context context, ArrayList<RedeemListPojo> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.redeem_alert_single, null);
            holder = new ViewHolder();
            holder.redeem_check = (ImageView) convertView.findViewById(R.id.redeem_alert_single_imageview);
            holder.redeem_gift_range = (CustomTextView) convertView.findViewById(R.id.redeem_alert_single_gift_range_textview);
            holder.redeem_gift = (CustomTextView) convertView.findViewById(R.id.redeem_alert_single_gift_textview);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if ("1".equalsIgnoreCase(listData.get(position).getCheck_status())){
            holder.redeem_check.setImageResource(R.drawable.checkbox_checked);
        }else{
            holder.redeem_check.setImageResource(R.drawable.checkbox_unchecked);
        }

        holder.redeem_gift_range.setText(listData.get(position).getDistance_range());
        holder.redeem_gift.setText(listData.get(position).getGift_name());

        return convertView;
    }

    static class ViewHolder {
        ImageView redeem_check;
        CustomTextView redeem_gift_range,redeem_gift;
    }

}