package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.pojo.NewCategoryListPojo;
import com.blackmaria.R;
import com.blackmaria.widgets.CustomTextView1;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 7/21/2017.
 */
public class NewCategoryAdapter extends BaseAdapter {
    private ArrayList<NewCategoryListPojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public NewCategoryAdapter(Context c, ArrayList<NewCategoryListPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {

        private ImageView Iv_Cat_type_img, hot_image, Iv_Cat_hot, Iv_Cat_online_indication, Iv_Cat_deliver_indication, category_online_status_indicate1;
        private LinearLayout Ll_car_layout, ll_bike_layout;
        private CustomTextView1 Tv_Cat_min_away, Tv_Cat_max_person, category_bike_away_min_txt, category_bike_max_person_txt;
        private TextView Tv_Cat_type_txt;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.new_categort_single_item, parent, false);
            holder = new ViewHolder();

            holder.Tv_Cat_type_txt = (TextView) view.findViewById(R.id.category_type_title_txt);
            holder.Tv_Cat_min_away = (CustomTextView1) view.findViewById(R.id.category_away_min_txt);
            holder.Tv_Cat_max_person = (CustomTextView1) view.findViewById(R.id.category_max_person_txt);
            holder.Iv_Cat_hot = (ImageView) view.findViewById(R.id.cat_hot_offer_img);
            holder.hot_image = (ImageView) view.findViewById(R.id.hot_image);
            holder.Ll_car_layout = (LinearLayout) view.findViewById(R.id.car_cat);

            holder.ll_bike_layout = (LinearLayout) view.findViewById(R.id.bike_cat);
            holder.Iv_Cat_type_img = (ImageView) view.findViewById(R.id.category_type_icon);
            holder.Iv_Cat_online_indication = (ImageView) view.findViewById(R.id.category_online_status_indicate);
            holder.Iv_Cat_deliver_indication = (ImageView) view.findViewById(R.id.cat_deliver_indicater_img);
            holder.category_bike_away_min_txt = (CustomTextView1) view.findViewById(R.id.category_bike_away_min_txt);
            holder.category_bike_max_person_txt = (CustomTextView1) view.findViewById(R.id.category_bike_max_person_txt);
            holder.category_online_status_indicate1 = (ImageView) view.findViewById(R.id.category_online_status_indicate1);
            view.setTag(holder);

        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        if (data.get(position).getCat_eta().equalsIgnoreCase("Not Available")) {
            holder.Iv_Cat_online_indication.setImageResource(R.drawable.cancel_red_tick);
        } else {
            holder.Iv_Cat_online_indication.setImageResource(R.drawable.tick_green_square);
        }

        if (data.get(position).getCat_eta().equalsIgnoreCase("Not Available") && data.get(position).getStrOfferType().equalsIgnoreCase("motorbike")) {
            holder.category_online_status_indicate1.setImageResource(R.drawable.cancel_red_tick);
        } else {
            holder.category_online_status_indicate1.setImageResource(R.drawable.tick_green_square);
        }


        if (data.get(position).getOffer_type().equalsIgnoreCase("hot")) {

            holder.Iv_Cat_hot.setVisibility(View.VISIBLE);
        } else {

            holder.Iv_Cat_hot.setVisibility(View.GONE);
        }
        if (data.get(position).getOffer_type().equalsIgnoreCase("hot") && data.get(position).getStrOfferType().equalsIgnoreCase("motorbike")) {
            holder.hot_image.setVisibility(View.VISIBLE);
        } else {
            holder.hot_image.setVisibility(View.GONE);
        }


        if (data.get(position).getCat_type().equalsIgnoreCase("delivery")) {

            holder.Iv_Cat_deliver_indication.setVisibility(View.VISIBLE);

        } else {
            holder.Iv_Cat_deliver_indication.setVisibility(View.GONE);
        }

        if (data.get(position).getStrOfferType().equalsIgnoreCase("motorbike")) {

            holder.ll_bike_layout.setVisibility(View.VISIBLE);
            holder.Ll_car_layout.setVisibility(View.GONE);
            holder.category_bike_away_min_txt.setText(data.get(position).getCat_eta());
            holder.category_bike_max_person_txt.setText(data.get(position).getMax_person());

        } else {
            Picasso.with(context).load(data.get(position).getCat_normal_img()).into(holder.Iv_Cat_type_img);
            holder.Tv_Cat_type_txt.setText(data.get(position).getCategory_name());
            holder.Tv_Cat_min_away.setText(data.get(position).getCat_eta());
            holder.Tv_Cat_max_person.setText(data.get(position).getMax_person());
            holder.ll_bike_layout.setVisibility(View.GONE);
            holder.Ll_car_layout.setVisibility(View.VISIBLE);
        }










    /*    if (position == data.size() - 1) {

            holder.Ll_car_layout.setVisibility(View.GONE);
            holder.ll_bike_layout.setVisibility(View.VISIBLE);
//            holder.title4.setText(data.get(position).getCat_type());
            holder.bike_min.setText(data.get(position).getCat_min());
            holder.bike_person.setText(data.get(position).getPerson());


        } else {


            if (position == 0) {

                holder.hot.setVisibility(View.VISIBLE);

            } else {

                holder.hot.setVisibility(View.GONE);


            }

            holder.ll_bike_layout.setVisibility(View.GONE);
            holder.Ll_car_layout.setVisibility(View.VISIBLE);
            holder.title1.setText(data.get(position).getCat_type());
            holder.title2.setText(data.get(position).getCat_min());
            holder.title3.setText(data.get(position).getPerson());


        }*/


        return view;
    }
}
