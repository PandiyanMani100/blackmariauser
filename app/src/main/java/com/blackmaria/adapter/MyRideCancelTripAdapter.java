package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.pojo.CancelTripPojo;
import com.blackmaria.R;

import java.util.ArrayList;
/**
 * Created by Prem Kumar and Anitha on 11/2/2015.
 */
public class MyRideCancelTripAdapter extends BaseAdapter {

    private ArrayList<CancelTripPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    public MyRideCancelTripAdapter(Context c, ArrayList<CancelTripPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }
    public class ViewHolder {
        private TextView reason;
        private ImageView cancel_select_image;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.myride_cancel_trip_single, parent, false);
            holder = new ViewHolder();
            holder.reason = (TextView) view.findViewById(R.id.myride_cancel_trip_reason_textview);
            holder.cancel_select_image = (ImageView) view.findViewById(R.id.cancel_select_image);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        if ("true".equalsIgnoreCase(data.get(position).getSetPayment_selected_cancel_id())) {
            holder.cancel_select_image.setVisibility(View.VISIBLE);
        } else {
            holder.cancel_select_image.setVisibility(View.GONE);
        }
        holder.reason.setText(data.get(position).getReason());
        return view;
    }
}

