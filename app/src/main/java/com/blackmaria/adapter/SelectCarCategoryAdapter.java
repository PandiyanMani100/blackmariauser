package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.blackmaria.pojo.RegisterDriverLocationCatCarPojo;
import com.blackmaria.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by GANESH on 27-09-2017.
 */

public class SelectCarCategoryAdapter extends RecyclerView.Adapter<SelectCarCategoryAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<RegisterDriverLocationCatCarPojo> data;
    public RecyclerItemClickInterface itemClickInterface;

    public SelectCarCategoryAdapter(Context context, ArrayList<RegisterDriverLocationCatCarPojo> data, RecyclerItemClickInterface itemClickInterface) {
        this.context = context;
        this.data = data;
        this.itemClickInterface = itemClickInterface;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.row_car_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        String imageUrl = data.get(position).getCatIcon();
        holder.Tv_vehicleDescription.setText(data.get(position).getDescription());
        holder.Tv_categoryName.setText(data.get(position).getCatName());
        if (imageUrl.length() > 0) {
            Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(holder.Iv_vehicle);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView Iv_hot, Iv_vehicle;
        public TextView Tv_categoryName, Tv_vehicleDescription;
        public Button Btn_select;

        public MyViewHolder(View view) {
            super(view);
            Iv_hot = (ImageView) view.findViewById(R.id.img_hot);
            Iv_vehicle = (ImageView) view.findViewById(R.id.img_vehicle);
            Tv_categoryName = (TextView) view.findViewById(R.id.txt_category_name);
            Tv_vehicleDescription = (TextView) view.findViewById(R.id.txt_vehicle_description);
            Btn_select = (Button) view.findViewById(R.id.btn_select);

            Btn_select.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            int position = getLayoutPosition();

            if (view == Btn_select) {
                itemClickInterface.onSelectClick(position);



            }

        }
    }

    public interface RecyclerItemClickInterface {

        public void onSelectClick(int position);

    }

}
