package com.blackmaria.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.blackmaria.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by user144 on 12/26/2017.
 */

public class Custom_infowindowTrackRide implements GoogleMap.InfoWindowAdapter {
    View mWindow, mContents;
    private Activity context;

    public Custom_infowindowTrackRide(Activity cxt ) {
        this.context = cxt;
        mWindow = context.getLayoutInflater().inflate(R.layout.infowindow_track, null);
        mContents = context.getLayoutInflater().inflate(R.layout.infowindow_track, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        render(marker, mWindow);
        return mWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {
        render(marker, mContents);
        return mContents;
    }


    private void render(Marker marker, View view) {
        // Use the equals() method on a Marker to check for equals.  Do not use ==.
        //  String title = marker.getTitle();

        String snippet = marker.getTitle();
        TextView userName = ((TextView) view.findViewById(R.id.username));
            userName.setText(snippet);

        }
    }
