package com.blackmaria.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.pojo.HomePojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;

import java.util.ArrayList;


/**
 * Created by user129 on 3/10/2017.
 */
public class SelectVechileAdapter extends BaseAdapter {
    private ArrayList<HomePojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Activity context;
    int zigZagColor=1;
    public SelectVechileAdapter(Activity c, ArrayList<HomePojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView image;
        private TextView name;
        private TextView time;
        private RelativeLayout layt_gridItem;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.select_vechile_single, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.bookmyride_single_carname);
            holder.time = (TextView) view.findViewById(R.id.bookmyride_single_time);
            holder.image = (ImageView) view.findViewById(R.id.bookmyride_single_car_image);
            holder.layt_gridItem=(RelativeLayout)view.findViewById(R.id.bookmyride_single_car_layout) ;
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        if(zigZagColor==1 || zigZagColor==4){

            holder.layt_gridItem.setBackgroundResource(R.drawable.car_cat_odd);
        }else{
            holder.layt_gridItem.setBackgroundResource(R.drawable.car_cat_even);

        }
        if(zigZagColor==4){
            zigZagColor=1;
        }else {
            zigZagColor++;
        }
         holder.name.setText(data.get(position).getCat_name().toUpperCase());
         holder.time.setText(data.get(position).getCat_time().toUpperCase());

        if (data.get(position).getSelected_Cat().equalsIgnoreCase(data.get(position).getCat_id())) {
            imageLoader.DisplayImage(String.valueOf(data.get(position).getIcon_active()), holder.image);
        } else {
            imageLoader.DisplayImage(String.valueOf(data.get(position).getIcon_normal()), holder.image);
        }


        return view;
    }
}
