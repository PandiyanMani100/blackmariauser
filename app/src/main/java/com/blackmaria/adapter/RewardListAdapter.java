package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.pojo.MilesRewardsPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 3/7/2018.
 */

public class RewardListAdapter extends BaseAdapter {

    private ArrayList<MilesRewardsPojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Context context;
    redeemRewards redeemrewards;

    public RewardListAdapter(Context c, ArrayList<MilesRewardsPojo> d, redeemRewards redeemrewards) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
        this.redeemrewards = redeemrewards;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView itemnumber, description, miles, redeembtn;
        private ImageView rewardImage;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.rewardlist_new_single, parent, false);

            holder = new ViewHolder();
            holder.itemnumber = (TextView) view.findViewById(R.id.itemno);
            holder.description = (TextView) view.findViewById(R.id.description);
            holder.miles = (TextView) view.findViewById(R.id.miles);
            holder.redeembtn = (TextView) view.findViewById(R.id.redeemcode);
            holder.rewardImage = (ImageView) view.findViewById(R.id.rewardimages);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.itemnumber.setText(": " + data.get(position).getGift_name());
        holder.description.setText(": " + data.get(position).getValid_to_second());
        holder.miles.setText(": " + data.get(position).getMiles_distance());

//        Picasso.with(context).load(data.get(position).getGift_image()).into( holder.rewardImage);
        Picasso.with(context).load(data.get(position).getGift_image()).placeholder(context.getResources().getDrawable(R.drawable.nogift_coupen_code)).error(context.getResources().getDrawable(R.drawable.nogift_coupen_code)).into(holder.rewardImage);

        holder.redeembtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redeemrewards.onClickRedeemList(position);
            }
        });

        return view;
    }

    public interface redeemRewards {
        void onClickRedeemList(int postion);
    }

}