package com.blackmaria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.pojo.ComplaintsPojo;
import com.blackmaria.R;

import java.util.ArrayList;

/**
 * Created by Jayachandran on 1/27/2017.
 */

public class ComplaintAdapter extends BaseAdapter {

    private ArrayList<ComplaintsPojo> listData;

    private LayoutInflater layoutInflater;

    public ComplaintAdapter(Context context, ArrayList<ComplaintsPojo> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.complaint_list_single, null);
            holder = new ViewHolder();
            holder.cancel_check = (ImageView) convertView.findViewById(R.id.cancel_reason_check_imageview);
            holder.cancel_reason = (TextView) convertView.findViewById(R.id.cancel_alert_single_gift_range_textview);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if ("1".equalsIgnoreCase(listData.get(position).getReasonStatus())){
            holder.cancel_check.setImageResource(R.drawable.checkbox_checked);
        }else{
            holder.cancel_check.setImageResource(R.drawable.checkbox_unchecked);
        }


        holder.cancel_reason.setText(listData.get(position).getsubjectName());

        return convertView;
    }

    static class ViewHolder {
        ImageView cancel_check;
        TextView cancel_reason;
    }

}