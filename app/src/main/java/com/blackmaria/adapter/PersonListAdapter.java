package com.blackmaria.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.blackmaria.pojo.PersonPojo;
import com.blackmaria.R;
import com.blackmaria.utils.ImageLoader;

import java.util.ArrayList;

/**
 * Created by user14 on 3/9/2017.
 */
public class PersonListAdapter extends BaseAdapter {

    private ArrayList<PersonPojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Activity context;

    public PersonListAdapter(Activity c, ArrayList<PersonPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private LinearLayout Ll_person;
        private RatingBar Rb_driverRating;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.person_listsingle, parent, false);
            holder = new ViewHolder();

//            holder.Ll_person = (LinearLayout) view.findViewById(R.id.linearLayout1);
            holder.Rb_driverRating = (RatingBar)view.findViewById(R.id.rating_page_driver_ratingbar12);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.Rb_driverRating.setNumStars(Integer.parseInt(data.get(position).getCount()));

       /* for(int i = 1; i <= Integer.parseInt(data.get(position).getCount()); i++) {
            System.out.println("------person ---"+i);
            ImageView image = new ImageView(context);
            image.setImageResource(R.drawable.red_dot_circle);
            holder.Ll_person.addView(image);
        }*/


        return view;
    }
}
