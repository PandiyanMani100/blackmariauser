package com.blackmaria.xmpp;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.ping.PingManager;
import org.jxmpp.jid.DomainBareJid;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Random;


public class MyXMPP {

    private static final String TAG = "RoosterConnection";

    private final Context mApplicationContext;
    private final String mUsername;
    private final String mPassword;
    XMPPConnectionListener connectionListener;
    public static XMPPTCPConnection mConnection = null;
    private String hostAddress = "";
    private BroadcastReceiver uiThreadMessageReceiver;//Receives messages from the ui thread.
    private SessionManager session;
    private ChatHandler xmpp_messagehandler;
    private ConnectionDetector cd;
    private boolean isinternetpresent;
    private boolean isconnecting = false;
    public static MyXMPP instance = null;
    public static boolean instanceCreated = false;
    static boolean isChatEnabled = false;
    private String serviceName="";

    public MyXMPP(Context context, String mServiceName, String mHostAddress, String user, String pass) {
        session = new SessionManager(context);
        mApplicationContext = context;
        this.serviceName = mServiceName;
        HashMap<String, String> domain = session.getXmpp();

        HashMap<String, String> users = session.getUserDetails();
        mUsername = users.get(SessionManager.KEY_USERID);
        mPassword = users.get(SessionManager.KEY_XMPP_SEC_KEY);
        hostAddress = domain.get(SessionManager.KEY_HOST_URL);
        String xmppHostName = domain.get(SessionManager.KEY_HOST_NAME);

        cd = new ConnectionDetector(mApplicationContext);
        isinternetpresent = cd.isConnectingToInternet();

        mConnection = null;

    }



    public static MyXMPP getInstance(Context context, String mServiceName, String mHostAddress, String user, String pass) {

        if (instance == null) {
            instance = new MyXMPP(context, mServiceName, mHostAddress, user, pass);
            instanceCreated = true;
        }
        return instance;

    }

    static {
        try {
            Class.forName("org.jivesoftware.smack.ReconnectionManager");
        } catch (ClassNotFoundException ex) {
            System.out.println("Class Error" + ex.toString());
        }
    }

    public void connect(String caller) throws IOException, XMPPException, SmackException {

        AsyncTask<Void, Void, Boolean> connectionThread = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected synchronized Boolean doInBackground(Void... arg0) {

                isconnecting = true;


               /* InetAddress addr = null;

                try {
                    addr = InetAddress.getByName(hostAddress);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }*/


                DomainBareJid serviceNames = null;

                try {
                    serviceNames = JidCreate.domainBareFrom(hostAddress);
                } catch (XmppStringprepException e) {
                    e.printStackTrace();
                }

                XMPPTCPConnectionConfiguration conf = XMPPTCPConnectionConfiguration.builder()
                        .setXmppDomain(serviceNames)
                        .setHost(hostAddress)
                        .setDebuggerEnabled(true)
                       //.setHostAddress(addr)
                        .setKeystoreType(null) //This line seems to get rid of the problem
                        //  .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
                        .setSendPresence(true)
                        .setConnectTimeout(50000)
                        .setCompressionEnabled(true).build();


                if (mConnection == null) {

                    Log.d(TAG, "CONNECT TRY........");

                    mConnection = new XMPPTCPConnection(conf);

                    PingManager pingManager = PingManager.getInstanceFor(mConnection);
                    pingManager.setPingInterval(300);

                    XMPPTCPConnection.setUseStreamManagementResumptiodDefault(true);
                    XMPPTCPConnection.setUseStreamManagementDefault(true);

                    connectionListener = new XMPPConnectionListener();
                    mConnection.addConnectionListener(connectionListener);
                    XmppMessageReceived messageListener = new XmppMessageReceived();
                    org.jivesoftware.smack.chat2.ChatManager.getInstanceFor(mConnection).addIncomingListener(messageListener);

                    try {

                        cd = new ConnectionDetector(mApplicationContext);
                        isinternetpresent = cd.isConnectingToInternet();

                        if (session.isLoggedIn()) {

                            if (isinternetpresent) {

                                if (mUsername != null && mPassword != null && !mUsername.equalsIgnoreCase("") && !mPassword.equalsIgnoreCase("")) {

                                    Log.d(TAG, "Calling connect() ");
                                    mConnection.connect();
                                    mConnection.login(mUsername, mPassword);
                                    Log.d(TAG, " login() Called ");

                                } else {


                                }

                            }
                        }

                        ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(mConnection);
                        reconnectionManager.setEnabledPerDefault(true);
                        reconnectionManager.enableAutomaticReconnection();
                        reconnectionManager.setReconnectionPolicy(ReconnectionManager.ReconnectionPolicy.FIXED_DELAY);
                        reconnectionManager.setFixedDelay(3);


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (XMPPException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (SmackException e) {
                        e.printStackTrace();
                    }

                } else {

                    Log.d(TAG, "CONNECTION_CHECKING :" + mConnection.isConnected());

                    if (!mConnection.isConnected()) {

                        cd = new ConnectionDetector(mApplicationContext);
                        isinternetpresent = cd.isConnectingToInternet();

                        try {

                            if (session.isLoggedIn()) {

                                if (isinternetpresent) {

                                    if (mUsername != null && mPassword != null && !mUsername.equalsIgnoreCase("") && !mPassword.equalsIgnoreCase("")) {

                                        Log.d(TAG, "Calling connect() ");
                                        mConnection.connect();
                                        mConnection.login(mUsername, mPassword);
                                        Log.d(TAG, " login() Called ");

                                    }

                                }
                            }

                        } catch (Exception e) {

                        }

                    }
                }

                return isconnecting = false;
            }
        };


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            connectionThread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            connectionThread.execute();
    }

    public int sendMessage(String toJid, String body) {

        cd = new ConnectionDetector(mApplicationContext);
        isinternetpresent = cd.isConnectingToInternet();

        if (isinternetpresent) {

            Log.d(TAG, "Sending message to :" + toJid);

            EntityBareJid jid = null;

            org.jivesoftware.smack.chat2.ChatManager chatManager = org.jivesoftware.smack.chat2.ChatManager.getInstanceFor(mConnection);

            try {
                jid = JidCreate.entityBareFrom(toJid);
            } catch (XmppStringprepException e) {
                e.printStackTrace();
            }

            try {

                Chat chat = chatManager.chatWith(jid);

                Message message = new Message(jid, Message.Type.chat);
                message.setBody(body);
                message.setStanzaId(String.format("%02d", new Random().nextInt(1000)));

                if (mConnection.isAuthenticated()) {

                    chat.send(message);
                    System.out.println("xmpp_touser_succss");

                    return 1;

                } else {
                    System.out.println("xmpp_to_user_fail");

                    return 0;

                }

            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
                return 0;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return 0;
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0;
            }
        }

        return 0;
    }


    public int sendUserTwo(String toJid, String body) {

        cd = new ConnectionDetector(mApplicationContext);
        isinternetpresent = cd.isConnectingToInternet();

        if (isinternetpresent) {

            Log.d(TAG, "Sending message to :" + toJid);

            EntityBareJid jid = null;

            org.jivesoftware.smack.chat2.ChatManager chatManager = org.jivesoftware.smack.chat2.ChatManager.getInstanceFor(mConnection);

            try {
                jid = JidCreate.entityBareFrom(toJid);
            } catch (XmppStringprepException e) {
                e.printStackTrace();
            }

            try {

                Chat chat = chatManager.chatWith(jid);

                Message message = new Message(jid, Message.Type.chat);
                message.setBody(body);
                message.setStanzaId(String.format("%02d", new Random().nextInt(1000)));

                if (mConnection.isAuthenticated()) {

                    chat.send(message);

                    return 1;
                } else {

                    return 0;
                }

            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
                return 0;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return 0;
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0;
            }
        }
        return 0;
    }

    public int sendMessageServer(String toJid, String body) {

        cd = new ConnectionDetector(mApplicationContext);
        isinternetpresent = cd.isConnectingToInternet();

        if (isinternetpresent) {

            Log.d(TAG, "Sending message to :" + toJid);

            EntityBareJid jid = null;

            org.jivesoftware.smack.chat2.ChatManager chatManager = ChatManager.getInstanceFor(mConnection);

            try {
                jid = JidCreate.entityBareFrom(toJid);
            } catch (XmppStringprepException e) {
                e.printStackTrace();
            }

            try {

                Chat chat = chatManager.chatWith(jid);

                Message message = new Message(jid, Message.Type.chat);
                message.setBody(body);
                message.setStanzaId(String.format("%02d", new Random().nextInt(1000)));

                if (mConnection.isAuthenticated()) {

                    chat.send(message);

                    return 1;
                } else {

                    return 0;
                }


            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
                return 0;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return 0;
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0;
            }
        }
        return 0;
    }


    public void disconnect() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mApplicationContext);
        prefs.edit().putBoolean("xmpp_logged_in", false).commit();

        if (mConnection != null) {
            mConnection.disconnect();
        }

        mConnection = null;
        // Unregister the message broadcast receiver.
        if (uiThreadMessageReceiver != null) {
            mApplicationContext.unregisterReceiver(uiThreadMessageReceiver);
            uiThreadMessageReceiver = null;
        }

    }

    public boolean connectioncheck() {
        if (mConnection == null || !mConnection.isConnected()) {
            System.out.println("XM----Disconneted");
            return true;
        } else {
            System.out.println("XM----Conneted");
            return false;
        }
    }


    public static enum ConnectionState {
        CONNECTED, AUTHENTICATED, CONNECTING, DISCONNECTING, DISCONNECTED;
    }


    public static enum LoggedInState {
        LOGGED_IN, LOGGED_OUT;
    }

    private class XMPPConnectionListener implements ConnectionListener {

        @SuppressLint("TimberArgCount")
        @Override
        public void connected(XMPPConnection connection) {

            Log.d(TAG, " XMPP Connected Successfully ");


        }

        @SuppressLint("TimberArgCount")
        @Override
        public void authenticated(XMPPConnection connection, boolean resumed) {

            Log.d(TAG, " XMPP Authenticated Successfully ");


        }

        @SuppressLint("TimberArgCount")
        @Override
        public void connectionClosed() {

            Log.d(TAG, " XMPP Connectionclosed ");

            if (connectionListener != null && mConnection != null) {

                mConnection.removeConnectionListener(connectionListener);
            }

        }

        @SuppressLint("TimberArgCount")
        @Override
        public void connectionClosedOnError(Exception e) {

            Log.d(TAG, " XMPP Connectionclosed Error");

            if (connectionListener != null && mConnection != null) {

                mConnection.removeConnectionListener(connectionListener);
            }


        }

        @SuppressLint("TimberArgCount")
        @Override
        public void reconnectingIn(int seconds) {

            Log.d(TAG, " XMPP Reconnecting.. ");
        }

        @SuppressLint("TimberArgCount")
        @Override
        public void reconnectionSuccessful() {

            Log.d(TAG, " XMPP Reconnecting Successfull ");

        }

        @SuppressLint("TimberArgCount")
        @Override
        public void reconnectionFailed(Exception e) {

            Log.d(TAG, " XMPP Reconnecting Failed ");


        }


    }


    private class XmppMessageReceived implements IncomingChatMessageListener {


        @Override
        public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {

            ///ADDED
            Log.d(TAG, "message.getBody() :" + message.getBody());
            Log.d(TAG, "message.getFrom() :" + message.getFrom());

            if (message.getType() == Message.Type.chat
                    && message.getBody() != null) {

                try {

                    if (xmpp_messagehandler == null) {

                        xmpp_messagehandler = new ChatHandler(mApplicationContext);

                    }
                    session = new SessionManager(mApplicationContext);
                    if (session.isLoggedIn() == true)
                    {
                        xmpp_messagehandler.onHandleChatMessage(message);
                    }


                } catch (Exception e) {

                }
            }
        }
    }


    public static boolean ConnectionChecking() {

        boolean connectivity = false;

        if (mConnection != null) {

            connectivity = mConnection.isConnected();
        }

        return connectivity;
    }


    public static void enableChat() {
        isChatEnabled = true;
    }

    public static void disableChat() {
        isChatEnabled = false;
    }


}
