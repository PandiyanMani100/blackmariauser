package com.blackmaria.xmpp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;

import com.blackmaria.utils.SessionManager;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;
import java.util.HashMap;


/**
 * Created by Raghavendra and Anitha on 12/15/2018.
 */

public class XmppService extends Service {

    public static MyXMPP xmpp;
    private String ServiceName = "", HostAddress = "";
    private String USERNAME = "";
    private String PASSWORD = "";
    private SessionManager sessionManager;


    @Override
    public IBinder onBind(final Intent intent) {
        return new LocalBinder<XmppService>(this);
    }

    @Override
    public boolean onUnbind(final Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {

        sessionManager = new SessionManager(XmppService.this);

        // get user data from session
        HashMap<String, String> domain = sessionManager.getXmpp();
        ServiceName = domain.get(SessionManager.KEY_HOST_NAME);
        HostAddress = domain.get(SessionManager.KEY_HOST_URL);

        HashMap<String, String> user = sessionManager.getUserDetails();
        USERNAME = user.get(SessionManager.KEY_USERID);
        PASSWORD = user.get(SessionManager.KEY_XMPP_SEC_KEY);


    }

    @Override
    public int onStartCommand(final Intent intent, final int flags,
                              final int startId) {
        xmpp = MyXMPP.getInstance(XmppService.this, ServiceName, HostAddress, USERNAME, PASSWORD);
        try {
            xmpp.connect("onCreate");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        }
        System.out.println("--------------Xmpp Service Started-----------");
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() throws NoClassDefFoundError{
            super.onDestroy();
    }

    public void onTaskRemoved(Intent rootIntent) {
        System.out.println("--------------Xmpp Service Stopped-----------");
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 500,
                restartServicePendingIntent);
    }

}
