package com.blackmaria.xmpp;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.text.format.DateFormat;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.blackmaria.AdsPage;
import com.blackmaria.FareBreakUp;
import com.blackmaria.FetchingDataActivity;
import com.blackmaria.InfoPage;
import com.blackmaria.pojo.adsupdatepojo;
import com.blackmaria.PushNotificationAlert;
import com.blackmaria.PushNotificationAlert_new;
import com.blackmaria.R;
import com.blackmaria.ThankYouActivity;
import com.blackmaria.TrackArrivePage;
import com.blackmaria.TrackRideAcceptPage;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.WaitingTimePage;
import com.blackmaria.XenditRidePaymentActivity;
import com.blackmaria.chatmodule.chathandlingintentservice;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.redirectingtchatpage;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.Calendar;
import java.util.Locale;

import static com.facebook.GraphRequest.TAG;

/**
 * Created by user88 on 11/4/2015.
 */
public class ChatHandler {
    private Context context;
    private SessionManager sessionManager;
    private String SAvailable = "";
    private NotificationManagerCompat notificationManagerCompat;
    private static final String CHANNEL_ID = "channel_id";
    private static final String PRIORITY_CHANNEL_ID = "pr_channel_id";
    private Bitmap bitmap;
    String overallaction="";

    public ChatHandler(Context context) {
        this.context = context;
        sessionManager = new SessionManager(context);
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.app_icon);
    }

    public void onHandleChatMessage(Message message) {
        try {
            String data = URLDecoder.decode(message.getBody(), "UTF-8");
            JSONObject messageObject = new JSONObject(data);
            System.out.println("-------------User Xmpp response---------------------" + data);
            createNotificationChannels();
            notificationManagerCompat = NotificationManagerCompat.from(context);

            String actionss = (String) messageObject.get(Iconstant.Push_Action);

             if (Iconstant.ACTION_TAG_CHATMESSAGE.equalsIgnoreCase(actionss)) {
                 String rideID = (String) messageObject.get(Iconstant.rideID);
                 if(messageObject.getString("message").startsWith("http"))
                 {
                     chatmessagetest("You have received new image",rideID);
                 }
                 else
                 {
                     chatmessagetest(messageObject.getString("message"),rideID);
                 }
            }
            if (messageObject.length() > 0)
            {
                if(sessionManager.getModeupdate().equalsIgnoreCase("unavailable"))
                {
                    if(messageObject.has("action"))
                    {

                        String action = messageObject.getString("action");
                        overallaction = action;
                        if(action.equals("ads"))
                        {
                            String key2s = messageObject.getString("key2");
                            dummytestads(messageObject.getString("key2"),action,key2s);
                        }
                        else if (action.equalsIgnoreCase(Iconstant.pushNotificationBeginTrip)) {
                            dummytest(messageObject.getString("message"));
                        }
                        else if (action.equalsIgnoreCase(Iconstant.PushNotification_RideCompleted_Key)) {
                            dummytest(messageObject.getString("message"));
                        }
                        else if (action.equalsIgnoreCase(Iconstant.PushNotification_PaymentPaid_Key)) {
                            dummytest(messageObject.getString("message"));
                        }
                        else if (action.equalsIgnoreCase(Iconstant.PushNotification_AcceptRide_Key)) {
                            dummytest(messageObject.getString("message"));
                        }
                        else if (action.equalsIgnoreCase(Iconstant.PushNotification_CabArrived_Key)) {
                            dummytest(messageObject.getString("message"));
                        }
                        else if (action.equalsIgnoreCase(Iconstant.wallet_credit)) {
                            dummytest(messageObject.getString("message"));
                        }
                        else if (action.equalsIgnoreCase(Iconstant.rating_submitted)) {
                            dummytest(messageObject.getString("message"));
                        }
                        else if (action.equalsIgnoreCase(Iconstant.PushNotification_RideCancelled_Key)) {
                            dummytest(messageObject.getString("message"));
                        }
                        else if (action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_Key)) {
                            dummytest(messageObject.getString("message"));
                        } else if (action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key)) {
                            dummytest(messageObject.getString("message"));
                        }   else if (action.equalsIgnoreCase(Iconstant.pushNotificationBeginTripReturn)) {
                            dummytest(messageObject.getString("message"));
                        }
                        else if (action.equalsIgnoreCase(Iconstant.pushNotificationMultistop)) {
                            dummytest(messageObject.getString("message"));
                        }

                        else
                        {
                             if (action.equalsIgnoreCase(Iconstant.pushNotificationDriverLoc))
                             {
                             }
                             else
                             {
                                 if (Iconstant.ACTION_TAG_CHATMESSAGE.equalsIgnoreCase(actionss)) {
                                     String rideID = (String) messageObject.get(Iconstant.rideID);
                                     if(messageObject.getString("message").startsWith("http"))
                                     {
                                         chatmessagetest("You have received new image",rideID);
                                     }
                                     else
                                     {
                                         chatmessagetest(messageObject.getString("message"),rideID);
                                     }
                                 }
                                 else
                                 {
                                     dummytest(messageObject.getString("message"));
                                 }
                             }
                       }
                    }
                }



                String action = (String) messageObject.get(Iconstant.Push_Action);
                if (action.equalsIgnoreCase(Iconstant.PushNotification_AcceptRide_Key)) {
                    sendBroadCastToRideConfirm(messageObject);
                }

                else if (action.equalsIgnoreCase(Iconstant.PushNotification_CabArrived_Key)) {
                    showCabArrivedAlert(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.PushNotification_RideCancelled_Key)) {
                    rideCancelledAlert(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.PushNotification_RideCompleted_Key)) {
                    rideCompletedAlert(messageObject);
                }
                else if (Iconstant.ACTION_TAG_CHATMESSAGE.equalsIgnoreCase(actionss)) {
                    chatmessage(messageObject);
                }
                else if (action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_Key)) {
                    requestPayment(messageObject);
                }
                else if (action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key)) {
                    rideCompletedAlert(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.PushNotification_PaymentPaid_Key)) {
                    paymentPaid(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.pushNotificationBeginTrip)) {
                    beginTripMessage(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.pushNotificationBeginTripReturn)) {
                    beginTripReturnMessage(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.pushNotificationMultistop)) {
                    multiplestopMessage(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.PushNotification_ride_later_confirmed)) {

                } else if (action.equalsIgnoreCase(Iconstant.pushNotificationDriverLoc)) {
                    updateDriverLocation_TrackRide(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.pushNotification_Ads)) {
                    display_Ads(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.complaint_replay_notification)) {
                    replayNotification(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.PushNotification_ride_completed_thankyou)) {
                    sendThankyou(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.complementry_time_closed)) {
                    complementryTimeClosed(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.drop_user_key)) {
                    sessionManager.setWaitedTime(0, 0, "", "");
                    dropUserComplementry(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.Drop_Waiting_time_closed_key)) {
                    Drop_Waiting_time_closed_key(messageObject);
                }
                if (action.equalsIgnoreCase(Iconstant.referalcredit_amount)) {
                    referalCredit(messageObject);
                }
                else if (action.equalsIgnoreCase(Iconstant.bank_success)) {
                    bankSuccess(messageObject);
                }
                else if (action.equalsIgnoreCase(Iconstant.user_wallet_credited) || action.equalsIgnoreCase(Iconstant.referral_info) || action.equalsIgnoreCase(Iconstant.coupon_info) || action.equalsIgnoreCase(Iconstant.complaint_reply) || action.equalsIgnoreCase(Iconstant.complaint_closed) || action.equalsIgnoreCase(Iconstant.rating_submitted) || action.equalsIgnoreCase(Iconstant.withdrawal_success)) {
                    walletcreditedfromadmin(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.wallet_success)) {
                    walletSuccess(messageObject);
                } else if (action.equalsIgnoreCase(Iconstant.wallet_credit)) {
                    walletcreditamount(messageObject);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bankSuccess(JSONObject messageObject) throws JSONException {
        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Cancelled));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Cancelled));

        i1.putExtra("user_id", messageObject.getString("key1"));
        i1.putExtra("Amount", messageObject.getString("key5"));
        i1.putExtra("Currencycode", messageObject.getString("key6"));
        i1.putExtra("paymenttype", messageObject.getString("key2"));
        i1.putExtra("payment_via", messageObject.getString("key3"));
        i1.putExtra("from_number", messageObject.getString("key4"));

        i1.putExtra("from", messageObject.getString("key7"));
        i1.putExtra("date", messageObject.getString("key8"));
        i1.putExtra("time", messageObject.getString("key9"));
        i1.putExtra("trans_id", messageObject.getString("key10"));

        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }
    private void walletSuccess(JSONObject messageObject) throws JSONException {
        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Cancelled));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Cancelled));


        i1.putExtra("user_id", messageObject.getString("key1"));
        i1.putExtra("Amount", messageObject.getString("key4"));
        i1.putExtra("Currencycode", messageObject.getString("key5"));
        i1.putExtra("payment_via", messageObject.getString("key2"));
        i1.putExtra("from_number", messageObject.getString("key3"));
        i1.putExtra("paymenttype", "");
        i1.putExtra("from", messageObject.getString("key6"));
        i1.putExtra("date", messageObject.getString("key7"));
        i1.putExtra("time", messageObject.getString("key8"));
        i1.putExtra("trans_id", messageObject.getString("key9"));

        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }
    private void walletCredit(JSONObject messageObject) throws JSONException {
        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Cancelled));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Cancelled));
        i1.putExtra("user_id", messageObject.getString("key1"));
        i1.putExtra("Amount", messageObject.getString("key2"));
        i1.putExtra("Currencycode", messageObject.getString("key3"));
        i1.putExtra("paymenttype", messageObject.getString("key4"));
        i1.putExtra("payment_via", messageObject.getString("key5"));
        i1.putExtra("from_number", messageObject.getString("key6"));
        i1.putExtra("from", messageObject.getString("key7"));
        i1.putExtra("date", messageObject.getString("key8"));
        i1.putExtra("time", messageObject.getString("key9"));
        i1.putExtra("trans_id", messageObject.getString("key10"));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }
    private void walletcreditamount(JSONObject messageObject) throws JSONException {
        Intent i1 = new Intent(context, PushNotificationAlert_new.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Cancelled));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Cancelled));
        i1.putExtra("user_id", messageObject.getString("key1"));
        i1.putExtra("Amount", messageObject.getString("key2"));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }
    private void referalCredit(JSONObject messageObject) throws JSONException {
        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Cancelled));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Cancelled));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }
    private void walletcreditedfromadmin(JSONObject messageObject) throws JSONException {
        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Cancelled));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Cancelled));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }
    private void sendThankyou(JSONObject messageObject) throws JSONException {
        try {

            Intent i1 = new Intent(context, ThankYouActivity.class);
            i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Cancelled));
            i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Cancelled));
            i1.putExtra("driver_review", messageObject.getString("key5"));
            i1.putExtra("driver_name", messageObject.getString("key2"));
            i1.putExtra("driver_image", messageObject.getString("key4"));
            i1.putExtra("vehicle_number", messageObject.getString("key8"));
            i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i1);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void sendBroadCastToRideConfirm(JSONObject messageObject) throws Exception {

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.app.timerPage.finish");
        context.sendBroadcast(broadcastIntent);
        Intent broadcastIntent1 = new Intent();
        broadcastIntent1.setAction("com.package.finish.BookingPage");
        context.sendBroadcast(broadcastIntent1);

        sessionManager.setCouponCode("", "");
        sessionManager.setReferralCode("", "");
        Intent i = new Intent(context, TrackRideAcceptPage.class);
        i.putExtra("driverID", messageObject.getString(Iconstant.DriverID));
        i.putExtra("driverName", messageObject.getString(Iconstant.DriverName));
        i.putExtra("driverImage", messageObject.getString(Iconstant.DriverImage));
        i.putExtra("driverRating", messageObject.getString(Iconstant.DriverRating));
        i.putExtra("driverTime", messageObject.getString(Iconstant.DriverTime));
        i.putExtra("rideID", messageObject.getString(Iconstant.RideID));
        i.putExtra("driverMobile", messageObject.getString(Iconstant.DriverMobile));
        i.putExtra("driverCar_no", messageObject.getString(Iconstant.DriverCar_No));
        i.putExtra("driverCar_model", messageObject.getString(Iconstant.DriverCar_Model));
        i.putExtra("flag", "1");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
//        }
    }
    private void showCabArrivedAlert(JSONObject messageObject) throws Exception {

        Intent intent1 = new Intent(context, TrackArrivePage.class);
        intent1.putExtra("ride_id", messageObject.getString("key1"));
        intent1.putExtra("car_no", messageObject.getString(Iconstant.DriverRating));
        intent1.putExtra("freewatingtime", messageObject.getString(Iconstant.DriverLat));
        intent1.putExtra("model", messageObject.getString("key7"));
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent1);
//        Intent broadcastIntent = new Intent();
//        broadcastIntent.setAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver");
//        broadcastIntent.putExtra("RideID", messageObject.getString("key1"));
//        context.sendBroadcast(broadcastIntent);
    }
    private void rideCancelledAlert(JSONObject messageObject) throws Exception {
//        refreshMethod();

        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Cancelled));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Cancelled));
        i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Cancelled));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }
    private void complementryTimeClosed(JSONObject messageObject) throws Exception {
//        refreshMethod();
        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Complementrytime_Completed));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_ComplementrytimeAction_Completed));
        i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Cancelled));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }
    private void Drop_Waiting_time_closed_key(JSONObject messageObject) throws Exception {
//        refreshMethod();
        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Complementrytime_Completed));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_ComplementrytimeAction_Completed));
        i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Cancelled));
        i1.putExtra("userId", messageObject.getString("key2"));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }
    private void dropUserComplementry(JSONObject messageObject) throws Exception {
//        refreshMethod();
        sessionManager.setContinueTrip("no");
        Intent intent = new Intent(context, WaitingTimePage.class);
        intent.putExtra("ride_id", messageObject.getString(Iconstant.RideID_Cancelled));
        intent.putExtra("FreeWaitingTime", messageObject.getString(Iconstant.drop_waitingtime));
        intent.putExtra("IsNormalOrreturnMulti", messageObject.getString(Iconstant.Ride_type));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        Log.d(TAG, "ChatHandler: " + "waitingtime");
        /*
        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Complementrytime_Completed));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_ComplementrytimeAction_Completed));
        i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Cancelled));
        i1.putExtra("FreeWaitingTime", messageObject.getString(Iconstant.drop_waitingtime));
        i1.putExtra("IsNormalOrreturnMulti", messageObject.getString(Iconstant.Ride_type));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);*/
    }
    private void rideCompletedAlert(JSONObject messageObject) throws Exception {
//        refreshMethod();
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.pushnotification.updateBottom_view");
        broadcastIntent.putExtra("rideStatus", messageObject.getString(Iconstant.Push_Action_Completed));
        broadcastIntent.putExtra("ride_id", messageObject.getString(Iconstant.RideID_Cancelled));
        context.sendBroadcast(broadcastIntent);

       /* Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", context.getResources().getString(R.string.pushnotification_alert_label_ride_completed));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Completed));
        i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Cancelled));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);*/

    }
    private void requestPayment(JSONObject messageObject) throws Exception {
//        refreshMethod();

        Intent i1 = null;
        if (messageObject.getString(Iconstant.Payment_type).equalsIgnoreCase("xendit-card")) {
            i1 = new Intent(context, XenditRidePaymentActivity.class);
            i1.putExtra("UserID", messageObject.getString(Iconstant.UserID_Request_Payment));
            i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Request_Payment));
            i1.putExtra("xendit_key", messageObject.getString(Iconstant.xendit_public_key));
            i1.putExtra("conversionkey", messageObject.getString(Iconstant.currencyconverionValue));
            i1.putExtra("ride_amount", messageObject.getString(Iconstant.TotalAmount_Request_Payment));
            i1.putExtra("currency", messageObject.getString(Iconstant.CurrencyCode_Request_Payment));

        } else {
            i1 = new Intent(context, FareBreakUp.class);
            i1.putExtra("UserID", messageObject.getString(Iconstant.UserID_Request_Payment));
            i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Request_Payment));
        }
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);

       /* i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Request_Payment));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Request_Payment));
        i1.putExtra("CurrencyCode", messageObject.getString(Iconstant.CurrencyCode_Request_Payment));
        i1.putExtra("TotalAmount", messageObject.getString(Iconstant.TotalAmount_Request_Payment));
        i1.putExtra("TravelDistance", messageObject.getString(Iconstant.TravelDistance_Request_Payment));
        i1.putExtra("Duration", messageObject.getString(Iconstant.Duration_Request_Payment));
        i1.putExtra("WaitingTime", messageObject.getString(Iconstant.WaitingTime_Request_Payment));
        i1.putExtra("DriverName", messageObject.getString(Iconstant.DriverName_Request_Payment));
        i1.putExtra("DriverImage", messageObject.getString(Iconstant.DriverImage_Request_Payment));
        i1.putExtra("DriverRating", messageObject.getString(Iconstant.DriverRating_Request_Payment));
        i1.putExtra("DriverLatitude", messageObject.getString(Iconstant.Driver_Latitude_Request_Payment));
        i1.putExtra("DriverLongitude", messageObject.getString(Iconstant.Driver_Longitude_Request_Payment));
        i1.putExtra("UserName", messageObject.getString(Iconstant.UserName_Request_Payment));
        i1.putExtra("UserLatitude", messageObject.getString(Iconstant.User_Latitude_Request_Payment));
        i1.putExtra("UserLongitude", messageObject.getString(Iconstant.User_Longitude_Request_Payment));
        i1.putExtra("SubTotal", messageObject.getString(Iconstant.subTotal_Request_Payment));
        i1.putExtra("ServiceTax", messageObject.getString(Iconstant.serviceTax_Request_Payment));
        i1.putExtra("TotalPayment", messageObject.getString(Iconstant.Total_Request_Payment));*/

    }
    private void paymentPaid(JSONObject messageObject) throws Exception {
      /* refreshMethod();

        Intent finish_fareBreakUp = new Intent();
        finish_fareBreakUp.setAction("com.pushnotification.finish.FareBreakUpPaymentList");
        context.sendBroadcast(finish_fareBreakUp);

        Intent finish_MyRidePaymentList = new Intent();
        finish_fareBreakUp.setAction("com.pushnotification.finish.MyRidePaymentList");
        context.sendBroadcast(finish_MyRidePaymentList);*/
        Intent intent1 = new Intent(context, FareBreakUp.class);
        intent1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Payment_paid));
        intent1.putExtra("ratingflag", "1");
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent1);
        /*Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Payment_paid));
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Payment_paid));
        i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Payment_paid));
        i1.putExtra("UserID", messageObject.getString(Iconstant.UserID_Payment_paid));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);*/
    }
    private void updateDriverLocation_TrackRide(JSONObject messageObject) throws Exception {

        System.out.println("--------chat handler driver update----------------");

        Intent local = new Intent();
        local.setAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver");
        local.putExtra("isContinousRide", messageObject.getString(Iconstant.latitude));
        local.putExtra("latitude", messageObject.getString(Iconstant.latitude));
        local.putExtra("longitude", messageObject.getString(Iconstant.longitude));
        local.putExtra("Bearing", messageObject.getString(Iconstant.Bearing));
        local.putExtra("ride_id", messageObject.getString(Iconstant.ride_id));
        context.sendBroadcast(local);
    }
    private void beginTripMessage(JSONObject messageObject) throws Exception {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip");
        broadcastIntent.putExtra("ride_id", messageObject.getString("key1"));
        broadcastIntent.putExtra("drop_lat", messageObject.getString("key3"));
        broadcastIntent.putExtra("drop_lng", messageObject.getString("key4"));
        broadcastIntent.putExtra("pickUp_lat", messageObject.getString("key5"));
        broadcastIntent.putExtra("pickUp_lng", messageObject.getString("key6"));
        context.sendBroadcast(broadcastIntent);
    }
    private void beginTripReturnMessage(JSONObject messageObject) throws Exception {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip_Return");
        broadcastIntent.putExtra("ride_id", messageObject.getString("key1"));
        broadcastIntent.putExtra("drop_lat", messageObject.getString("key6"));
        broadcastIntent.putExtra("drop_lng", messageObject.getString("key7"));
        broadcastIntent.putExtra("pickUp_lat", messageObject.getString("key2"));
        broadcastIntent.putExtra("pickUp_lng", messageObject.getString("key3"));
        context.sendBroadcast(broadcastIntent);
    }
    private void multiplestopMessage(JSONObject messageObject) throws Exception {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Multiplestop");
        broadcastIntent.putExtra("drop_lat", messageObject.getString("key7"));
        broadcastIntent.putExtra("drop_lng", messageObject.getString("key8"));
        broadcastIntent.putExtra("pickUp_lat", messageObject.getString("key3"));
        broadcastIntent.putExtra("pickUp_lng", messageObject.getString("key4"));
        broadcastIntent.putExtra("record_id", messageObject.getString("key2"));

        context.sendBroadcast(broadcastIntent);
    }
    private void makePaymentStripAni(JSONObject messageObject) throws Exception {
        refreshMethod();

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_MakePayment");
        context.sendBroadcast(broadcastIntent);

       /* Intent i1 = new Intent(context, FareBreakUp.class);
        i1.putExtra("RideID", messageObject.getString(Iconstant.Make_Payment));
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);*/
    }
    private void display_Ads(JSONObject messageObject) throws Exception {

        adsupdatepojo obj = new adsupdatepojo();
        obj.setIsupdate(true);
        EventBus.getDefault().post(obj);

        Intent i1 = new Intent(context, AdsPage.class);
        i1.putExtra("AdsTitle", messageObject.getString(Iconstant.Ads_title));
        i1.putExtra("AdsMessage", messageObject.getString(Iconstant.Ads_Message));
        if (messageObject.has(Iconstant.Ads_image)) {
            i1.putExtra("AdsBanner", messageObject.getString(Iconstant.Ads_image));
        }
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }
    private void replayNotification(JSONObject messageObject) throws Exception {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_CLASS_complaint_replay_notification");
        broadcastIntent.putExtra("ticket_id", messageObject.getString(Iconstant.ticketId));
        broadcastIntent.putExtra("user_id", messageObject.getString(Iconstant.userId));
        broadcastIntent.putExtra("message", messageObject.getString(Iconstant.replay_message));
        broadcastIntent.putExtra("date_time", messageObject.getString(Iconstant.dateTime));
        context.sendBroadcast(broadcastIntent);
    }
    private void refreshMethod() {
        Intent finish_fareBreakUp = new Intent();
        finish_fareBreakUp.setAction("com.pushnotification.finish.FareBreakUp");
        context.sendBroadcast(finish_fareBreakUp);

        Intent finish_timerPage = new Intent();
        finish_timerPage.setAction("com.pushnotification.finish.TimerPage");
        context.sendBroadcast(finish_timerPage);

        Intent finish_pushAlert = new Intent();
        finish_pushAlert.setAction("com.pushnotification.finish.PushNotificationAlert");
        context.sendBroadcast(finish_pushAlert);

        Intent finish_MyRideDetails = new Intent();
        finish_MyRideDetails.setAction("com.pushnotification.finish.MyRideDetails");
        context.sendBroadcast(finish_MyRideDetails);

        Intent local = new Intent();
        local.setAction("com.pushnotification.finish.trackyourRide");
        context.sendBroadcast(local);

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.pushnotification.updateBottom_view");
        context.sendBroadcast(broadcastIntent);

    }
    private void trackRideRefreshMethod() {
        Intent finish_fareBreakUp = new Intent();
        finish_fareBreakUp.setAction("com.pushnotification.finish.FareBreakUp");
        context.sendBroadcast(finish_fareBreakUp);

        Intent finish_timerPage = new Intent();
        finish_timerPage.setAction("com.pushnotification.finish.TimerPage");
        context.sendBroadcast(finish_timerPage);

        Intent finish_pushAlert = new Intent();
        finish_pushAlert.setAction("com.pushnotification.finish.PushNotificationAlert");
        context.sendBroadcast(finish_pushAlert);

        Intent finish_MyRideDetails = new Intent();
        finish_MyRideDetails.setAction("com.pushnotification.finish.MyRideDetails");
        context.sendBroadcast(finish_MyRideDetails);

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.pushnotification.updateBottom_view");
        context.sendBroadcast(broadcastIntent);

    }
    private void chatmessage(JSONObject response) throws JSONException {

        //sendNotification(response.getString("message"));


        Intent intent1 = new Intent(context, chathandlingintentservice.class);
        try {
            intent1.putExtra("desc", response.getString("message"));
            intent1.putExtra("sender_ID", response.getString("senderID"));
            intent1.putExtra("timestamp", response.getString("timeStamp"));
            intent1.putExtra("ride_id", response.getString("rideID"));
            if(response.has("datetime"))
            {
                intent1.putExtra("datetime", response.getString("datetime"));
            }
            else
            {
                intent1.putExtra("datetime", getDate(Long.parseLong(response.getString("timeStamp"))));
            }
            context.startService(intent1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("dd MMM yy hh:mm a", cal).toString();
        return date;
    }

    public void showNotification(Context context, String title, String body, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(title)
                .setContentText(body);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(notificationId, mBuilder.build());
    }


    @SuppressWarnings("deprecation")
    private void dummytest(String msg)
    {
        Intent intent = null;
        String titilee = context.getString(R.string.app_name);
        String subtitilee =msg;

         if(overallaction.equals("coupon_info"))
        {
            titilee = "Coupon";
            intent = new Intent(context, FetchingDataActivity.class);
        }
        else if(overallaction.equals("ride_later_confirmed"))
        {
            titilee = "Ride Request";
            intent = new Intent(context, FetchingDataActivity.class);
        }
        else if(overallaction.equals("cab_arrived") || overallaction.equals("trip_begin") || overallaction.equals("make_payment"))
        {
            titilee = "Trip Info";
            intent = new Intent(context, FetchingDataActivity.class);
        }
        else if(overallaction.equals("payment_paid"))
        {
            titilee = "Payment Paid";
            intent = new Intent(context, FetchingDataActivity.class);
        }
        else if(overallaction.equals("blackmariachat"))
        {
            titilee = "New Message From Driver";
            intent = new Intent(context, FetchingDataActivity.class);
        }
        else
        {
            intent = new Intent(context, FetchingDataActivity.class);
        }

        System.out.println("first came---1FetchingDataActivity");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        showNotification(context,titilee,subtitilee,intent);

      /*  PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(titilee)
                .setLargeIcon(bitmap)
                .setContentIntent(pendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(subtitilee));

        notificationManagerCompat.notify(5, builder.build());*/
    }

    @SuppressWarnings("deprecation")
    private void dummytestads(String msg,String action,String subtitle)
    {

        Intent intent = null;
        String titilee = context.getString(R.string.app_name);
        String subtitilee =msg;
        if(action.equals("ads"))
        {
            titilee = msg;
            subtitilee= subtitle;
            intent = new Intent(context, InfoPage.class);
        }
        else
        {
            intent = new Intent(context, FetchingDataActivity.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        androidx.core.app.NotificationCompat.Builder builder = new androidx.core.app.NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(titilee)
                .setLargeIcon(bitmap)
                .setContentIntent(pendingIntent)
                .setStyle(new androidx.core.app.NotificationCompat.BigTextStyle()
                        .bigText(subtitilee));

        notificationManagerCompat.notify(5, builder.build());
    }
    @SuppressWarnings("deprecation")
    private void chatmessagetest(String msg,String rideID)
    {

        Intent intent = new Intent(context, redirectingtchatpage.class);
        intent.putExtra("RideID", rideID);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(context.getString(R.string.app_name))
                .setLargeIcon(bitmap)
                .setContentIntent(pendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(msg));

        notificationManagerCompat.notify(5, builder.build());
    }


    private void createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel basicNotificationChanel = new NotificationChannel(CHANNEL_ID,
                    "basic notification chanel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            NotificationChannel priorityNotificationChannel = new NotificationChannel(PRIORITY_CHANNEL_ID,
                    "priority notification channel",
                    NotificationManager.IMPORTANCE_HIGH);

            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(basicNotificationChanel);
            notificationManager.createNotificationChannel(priorityNotificationChannel);
        }
    }
}
