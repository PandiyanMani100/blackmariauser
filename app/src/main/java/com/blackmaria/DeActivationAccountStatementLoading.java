package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 8/29/2017.
 */

public class DeActivationAccountStatementLoading extends ActivityHockeyApp {
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private SessionManager session;
    LanguageDb mhelper;
    private boolean isDataPresent = false;
    private String sDriverID = "", sCurrency = "", sTotalAmount = "", UserID = "";
    private Dialog dialog;
    ArrayList<String> arrTitle = null;
    ArrayList<String> arrValue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deactivationloading);
        cd = new ConnectionDetector(DeActivationAccountStatementLoading.this);
        mhelper=new LanguageDb(this);
        if (cd.isConnectingToInternet()) {
            PostLogout(Iconstant.deactivate_account_statement_url);
        }

        TextView procee=(TextView)findViewById(R.id.procee);
        procee.setText(getkey("reqporocessing"));
    }


    private void PostLogout(String url) {

        dialog = new Dialog(DeActivationAccountStatementLoading.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        session = new SessionManager(DeActivationAccountStatementLoading.this);
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);

        System.out.println("-------------Deactivation Statement Url----------------" + url);
        System.out.println("-------------Deactivation Statement Params----------------" + jsonParams);

        mRequest = new ServiceRequest(DeActivationAccountStatementLoading.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Deactivation Statement Response----------------" + response);

                String Sstatus = "", titles = "", values = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jobjResponse = object.getJSONObject("response");

                        if (jobjResponse.length() > 0) {
                            sCurrency = jobjResponse.getString("currency");
                            session.setaccountcloseamount(jobjResponse.getString("tot_amount"));
                            sTotalAmount = sCurrency + " " + jobjResponse.getString("tot_amount");
                            values = sTotalAmount;
                            Object objStatement = jobjResponse.get("statement");
                            if (objStatement instanceof JSONArray) {
                                JSONArray jarStatement = jobjResponse.getJSONArray("statement");

                                if (jarStatement.length() > 0) {

                                    arrTitle = new ArrayList<>();
                                    arrValue = new ArrayList<>();
                                    for (int i = 0; i < jarStatement.length(); i++) {

                                        JSONObject jobjValue = jarStatement.getJSONObject(i);

                                        String title = jobjValue.getString("title");
                                        String value = sCurrency + " " + jobjValue.getString("value");
                                        arrTitle.add(title);
                                        arrValue.add(value);


                                    }

                                    isDataPresent = true;

                                } else {
                                    isDataPresent = false;
                                }

                            } else {
                                isDataPresent = false;
                            }

                        } else {
                            isDataPresent = false;
                        }
                        dialogDismiss();

                    } else {
                        isDataPresent = false;
                    }
                    dialogDismiss();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isDataPresent) {
                    if (arrTitle.size() > 0) {
                        Intent closeDataIntent = new Intent(DeActivationAccountStatementLoading.this, DeActiviationAccount.class);
                        closeDataIntent.putExtra("strTotalAmountValues", values);
                        closeDataIntent.putStringArrayListExtra("arrDeActTitle", arrTitle);
                        closeDataIntent.putStringArrayListExtra("arrDeActValue", arrValue);
                        startActivity(closeDataIntent);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        dialogDismiss();
                    }
                } else {

                }

            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });


    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
