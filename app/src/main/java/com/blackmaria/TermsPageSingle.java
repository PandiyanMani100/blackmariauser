package com.blackmaria;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Ganesh on 27-06-2017.
 */

public class TermsPageSingle extends ActivityHockeyApp {

    private String sUserId = "", sPageType = "", sPageTitle = "", sPageNo = "";

    private TextView Tv_dynamicContent, Tv_dynamicTitle, Tv_dynamicCount;
    private ImageView Iv_back;

    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_single);

        initialize();
        Iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter,R.anim.exit);
            }
        });

    }

    private void initialize() {
        sessionManager = new SessionManager(TermsPageSingle.this);
        cd = new ConnectionDetector(TermsPageSingle.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sUserId = user.get(SessionManager.KEY_USERID);

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Tv_dynamicContent = (TextView) findViewById(R.id.txt_dynamic_content);
        Tv_dynamicTitle = (TextView) findViewById(R.id.txt_title);
        Tv_dynamicCount = (TextView) findViewById(R.id.txt_count);

        getIntentData();

        if (cd.isConnectingToInternet()) {
            dynamicContentRequest(Iconstant.Dynamic_content_Url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            sPageType = intent.getStringExtra("pageType");
            sPageTitle = intent.getStringExtra("pageTitle");
            sPageNo = intent.getStringExtra("pageNo");
        }
    }


    //-------------------dynamic Content Request----------------
    private void dynamicContentRequest(String Url) {

        dialog = new Dialog(TermsPageSingle.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", sUserId);
        jsonParams.put("page_type", sPageType);
        jsonParams.put("app_type", "user");

        System.out.println("--------------dynamic Content Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(TermsPageSingle.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "", sTitle = "", sContent = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {
                                JSONObject pageObject = jsonObject.getJSONObject("pages_details");
                                if (pageObject.length() > 0) {
                                    sTitle = pageObject.getString("page_title");
                                    sContent = pageObject.getString("page_description");
                                }
                            }
                        } else {
                            sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    Tv_dynamicTitle.setText(sPageTitle);
                    Tv_dynamicCount.setText(sPageNo);
                    Tv_dynamicContent.setText(Html.fromHtml(sContent));
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TermsPageSingle.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

}
