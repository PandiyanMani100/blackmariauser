package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ParseException;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.RideListAdapter;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.pojo.RideListPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by user14 on 5/11/2017.
 */

public class RideList1 extends FragmentActivityHockeyApp

    {

        private CaldroidFragment dialogCaldroidFragment;
        private boolean isInternetPresent = false;
        private SessionManager session;
        private ConnectionDetector cd;
        private ServiceRequest mRequest;
        private boolean searchstatus = false;
        private ListView Lv_ride;
        private RideListAdapter rideListAdapter;
        private ArrayList<RideListPojo> RideList_itemList;
        private RelativeLayout R1_ongoing_ride,R1_search_ride;
        private TextView Tv_next, Tv_go, Tv_previous,Tv_no_rides;
        private ImageView Iv_back;
        private String UserID = "",FilterDate = "",perPage = "5";
        private Dialog dialog;
        private TextView Tv_ongoing_ride_date,Tv_ongoing_ride_lable,Tv_ongoing_ride_carno,Tv_ongoing_ride_status,Tv_ongoing_ride_cartype;
        private String SongoningRideDate = "", Scategory = "",Svechile_no = "", Sridestatus = "",Seta_text = "",Scompleted_rides = "",
                Stotal_rides = "",Scancelled_rides = "",Stotal_distance = "",SonGoingRideId = "" ;
        private TextView Tv_completed_ride, Tv_cancelled_rides,Tv_mileage;
        private int currentPage = 1;
        private EditText Et_search_date;
        private String nextPage;
        private boolean ongoing_arrstatus = false;
        private RelativeLayout R1_bottom;
        public static Activity rideList_class;
        private String type = "";


        private RideList1.RefreshReceiver refreshReceiver;
        //------------------------------Broadcost reciver---------------------------------------------
        public class RefreshReceiver extends BroadcastReceiver {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                    if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                        String mRideStatus = (String) intent.getExtras().get("rideStatus");
                        String mRideid = (String) intent.getExtras().get("ride_id");


                        if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                            Intent intent1 = new Intent(RideList1.this, Navigation_new.class);
                            startActivity(intent1);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                            Intent intent1 = new Intent(RideList1.this, FareBreakUp.class);
                            intent1.putExtra("RideID", mRideid);
                            intent1.putExtra("ratingflag", "1");
                            startActivity(intent1);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        }else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus) ){
                            Intent intent1 = new Intent(RideList1.this, FareBreakUp.class);
                            intent1.putExtra("RideID", mRideid);
                            intent1.putExtra("ratingflag", "2");
                            startActivity(intent1);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        }
                    }

                }


            }


        }


        @Override
        protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_list_page);
        rideList_class = RideList1.this;
        initialize();

        Tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage + 1;
                postData();
            }
        });

        Tv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage - 1;
                postData();

            }
        });

        Et_search_date.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                datePicker(savedInstanceState);
            }
        });

        Iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type.equals("Complaint"))
                {

                    onBackPressed();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();

                }
                else if(type.equals("Home")) {

                    Intent intent = new Intent(RideList1.this, Navigation_new.class);
                    startActivity(intent);
                    finish();
                }
            }

        });

        Lv_ride.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(type.equals("Complaint"))
                {

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("ride_id",RideList_itemList.get(position).getRide_id());
                    setResult(RESULT_OK, returnIntent);
                    onBackPressed();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();

                }
                else if(type.equals("Home")) {

                    Intent intent = new Intent(RideList1.this,RideDetailPage.class);
                    intent.putExtra("ride_id",RideList_itemList.get(position).getRide_id());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }

            }

        });

        R1_ongoing_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RideList1.this,RideDetailPage.class);
                intent.putExtra("ride_id",SonGoingRideId);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        Tv_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postData();
            }

        });

    }

    private void initialize() {
        session = new SessionManager(RideList1.this);
        cd = new ConnectionDetector(RideList1.this);
        isInternetPresent = cd.isConnectingToInternet();
        RideList_itemList = new ArrayList<RideListPojo>();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        Iv_back = (ImageView) findViewById(R.id.ride_list_back_imageview);
        R1_ongoing_ride = (RelativeLayout) findViewById(R.id.ride_list_current_ride_main_textview);
        Tv_ongoing_ride_lable = (TextView) findViewById(R.id.ride_list_current_ride_textview);
        Tv_ongoing_ride_date = (TextView) findViewById(R.id.ride_list_current_ride_date_textview);
        Tv_ongoing_ride_cartype = (TextView) findViewById(R.id.ride_list_current_ride_car_type_textview);
        Tv_ongoing_ride_carno = (TextView) findViewById(R.id.ride_list_current_ride_carno_textview);
        Tv_ongoing_ride_status = (TextView) findViewById(R.id.ride_list_current_ride_ride_status_textview);
        Tv_completed_ride = (TextView) findViewById(R.id.ride_list_completed_ride_textview);
        Tv_cancelled_rides = (TextView) findViewById(R.id.ride_list_canceled_ride_textview);
        Tv_mileage = (TextView) findViewById(R.id.ride_list_total_mileage_textview);
        R1_search_ride = (RelativeLayout) findViewById(R.id.ride_list_search_ride_layout);
        Et_search_date = (EditText) findViewById(R.id.ride_list_date_edittext);
        Tv_go = (TextView) findViewById(R.id.ride_list_go_textview);
        R1_bottom = (RelativeLayout) findViewById(R.id.ride_list_bottom_main_layout);
        Lv_ride = (ListView) findViewById(R.id.ride_list_listview);
        Tv_no_rides = (TextView) findViewById(R.id.ride_list_no_rides_textview);
        Tv_previous = (TextView) findViewById(R.id.ride_list_previous_textview);
        Tv_next = (TextView) findViewById(R.id.ride_list_next_textview);

        R1_search_ride.setVisibility(View.GONE);

        Intent intent = getIntent();

        if (intent.hasExtra("Class")) {
            type = intent.getStringExtra("Class");
        }

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RideList1.RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        postData();

    }

    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("filter_date", FilterDate);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        if (isInternetPresent) {
            PostRequest_myrideList(Iconstant.myrides_url,jsonParams);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

        }
    }


    private void datePicker(Bundle savedInstanceState) {

        dialogCaldroidFragment = new CaldroidFragment();
        dialogCaldroidFragment.setCaldroidListener(caldroidListener);

        // If activity is recovered from rotation
        final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
        if (savedInstanceState != null) {
            dialogCaldroidFragment.restoreDialogStatesFromKey(getSupportFragmentManager(), savedInstanceState,
                    "DIALOG_CALDROID_SAVED_STATE", dialogTag);
            Bundle args = dialogCaldroidFragment.getArguments();
            if (args == null) {
                args = new Bundle();
                dialogCaldroidFragment.setArguments(args);
            }
        } else {
            // Setup arguments
            Bundle bundle = new Bundle();
            // Setup dialogTitle
            dialogCaldroidFragment.setArguments(bundle);
        }


        Calendar cal = Calendar.getInstance();
        Date currentDate = null;
        Date maximumDate = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            String formattedDate = df.format(cal.getTime());
            currentDate = df.parse(formattedDate);

            // Max date is next 7 days
            cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 7);
            maximumDate = cal.getTime();

        } catch (ParseException e1) {
            e1.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }


//        dialogCaldroidFragment.setMinDate(currentDate);
        dialogCaldroidFragment.setMaxDate(currentDate);
        dialogCaldroidFragment.show(getSupportFragmentManager(), dialogTag);
        dialogCaldroidFragment.refreshView();
    }


    // Setup CaldroidListener
    final CaldroidListener caldroidListener = new CaldroidListener() {
        @Override
        public void onSelectDate(Date date, View view) {


            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = df.format(date);
            FilterDate = formattedDate;
            Et_search_date.setText(formattedDate);
            dialogCaldroidFragment.dismiss();
        }


        @Override
        public void onChangeMonth(int month, int year) {
            String text = "month: " + month + " year: " + year;
        }

        @Override
        public void onLongClickDate(Date date, View view) {

        }

        @Override
        public void onCaldroidViewCreated() {
        }
    };



    private void PostRequest_myrideList(String url,HashMap<String, String> jsonParams) {

        System.out.println("----------rideList--json-------------" + jsonParams);
        mRequest = new ServiceRequest(RideList1.this);

        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-----------rideList---reponse-------------------" + response);

                String Sstatus = "",Smessage = "";

                try {
                    JSONObject obj = new JSONObject(response);
                    Sstatus = obj.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject obj2 = obj.getJSONObject("response");
                        currentPage = Integer.parseInt(obj2.getString("current_page"));
                        nextPage = obj2.getString("next_page");
                        Stotal_rides = obj2.getString("total_rides");
                        Scompleted_rides = obj2.getString("completed_rides");
                        Scancelled_rides = obj2.getString("cancelled_rides");
                        Stotal_distance = obj2.getString("total_distance");
                        Object check_object = obj2.get("rides");
                        if (check_object instanceof JSONArray) {
                            JSONArray RideList_list_jsonArray = obj2.getJSONArray("rides");
                            if (RideList_list_jsonArray.length() > 0) {

                                RideList_itemList.clear();
                                for (int i = 0; i < RideList_list_jsonArray.length(); i++) {
                                    JSONObject search_obj = RideList_list_jsonArray.getJSONObject(i);
                                    RideListPojo RideListPojo = new RideListPojo();
                                    RideListPojo.setDatetime(search_obj.getString("datetime"));
                                    RideListPojo.setDisplay_status(search_obj.getString("display_status"));
                                    RideListPojo.setDistance(search_obj.getString("distance"));
                                    RideListPojo.setGroup(search_obj.getString("group"));
                                    RideListPojo.setPickup(search_obj.getString("pickup"));
                                    RideListPojo.setRide_category(search_obj.getString("ride_category"));
                                    RideListPojo.setRide_date(search_obj.getString("ride_date"));
                                    RideListPojo.setRide_id(search_obj.getString("ride_id"));
                                    RideListPojo.setRide_status(search_obj.getString("ride_status"));
                                    RideListPojo.setRide_time(search_obj.getString("ride_time"));
                                    RideListPojo.setRide_type(search_obj.getString("ride_type"));


                                    RideList_itemList.add(RideListPojo);
                                }
                                searchstatus = true;
                            }
                            else {
                                searchstatus = false;
                                RideList_itemList.clear();
                            }
                        }else{
                            searchstatus = false;
                            RideList_itemList.clear();
                        }

                        Object check_object1 = obj2.get("ongoing_arr");
                        if (check_object1 instanceof JSONObject) {

                            JSONObject jsonObject = obj2.getJSONObject("ongoing_arr");
                            if (jsonObject.length() > 0) {
                                ongoing_arrstatus = true;
                                SongoningRideDate = jsonObject.getString("date");
                                Scategory = jsonObject.getString("category");
                                Svechile_no = jsonObject.getString("vechile_no");
                                Sridestatus = jsonObject.getString("ride_status");
                                Seta_text = jsonObject.getString("eta_text");
                                SonGoingRideId = jsonObject.getString("ride_id");


                            }
                            else {
                                ongoing_arrstatus = false;
                            }


                        }

                    }else{
                        Smessage = obj.getString("response");
                    }

                    dialogDismiss();

                } catch (JSONException e) {

                    e.printStackTrace();
                    dialogDismiss();

                }
                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {

                    if (currentPage == 1) {
                        Tv_previous.setVisibility(View.GONE);
                    } else {
                        Tv_previous.setVisibility(View.VISIBLE);
                    }

                    Tv_completed_ride.setText(Scompleted_rides);
                    Tv_cancelled_rides.setText(Scancelled_rides);
                    Tv_mileage.setText(Stotal_distance);

                    if (searchstatus) {
                        Tv_next.setVisibility(View.VISIBLE);
                        R1_bottom.setVisibility(View.VISIBLE);
                        Tv_no_rides.setVisibility(View.GONE);

                    }else{
                        Tv_next.setVisibility(View.GONE);
                        if (currentPage == 1) {
                            R1_bottom.setVisibility(View.GONE);
                        }else{
                            R1_bottom.setVisibility(View.VISIBLE);
                        }
                        Tv_no_rides.setVisibility(View.VISIBLE);
                    }
//                    rideListAdapter = new RideListAdapter(RideList1.this, RideList_itemList);
                    Lv_ride.setAdapter(rideListAdapter);
                    rideListAdapter.notifyDataSetChanged();

                    if (ongoing_arrstatus) {
                        R1_ongoing_ride.setVisibility(View.VISIBLE);

                        Tv_ongoing_ride_date.setText(SongoningRideDate);
                        Tv_ongoing_ride_cartype.setText(Scategory);
                        Tv_ongoing_ride_carno.setText(Svechile_no);
                        Tv_ongoing_ride_status.setText(Sridestatus);
                        if(Sridestatus.equalsIgnoreCase("Confirmed")) {
                            Tv_ongoing_ride_lable.setVisibility(View.VISIBLE);
                            Tv_ongoing_ride_lable.setText(Seta_text);
                        }
                        else {
                            Tv_ongoing_ride_lable.setVisibility(View.GONE);
                        }

                    } else {
                        R1_ongoing_ride.setVisibility(View.GONE);
                    }

                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();

            }

        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(RideList1.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(type.equals("Complaint"))
        {
            onBackPressed();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        }
        else if(type.equals("Home")) {
            Intent intent = new Intent(RideList1.this, Navigation_new.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

}