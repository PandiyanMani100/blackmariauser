package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.CustomSpinnerAdapter;
import com.blackmaria.adapter.WalletPaymentListAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.MultiDropPojo;
import com.blackmaria.pojo.WalletMoneyPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.ExpandableHeightGridView;
import com.blackmaria.utils.ImageLoader;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.CustomTextView1;
import com.blackmaria.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.blackmaria.HomePage.isValidEmail;

public class WalletMoneyPage1 extends ActivityHockeyApp/*ActivitySubClass*/ {
    private RelativeLayout back, Rl_enter_pin, Rl_otp;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private static Context context;
    private SessionManager session;
    private String UserID = "";
    private ServiceRequest mRequest;
    HashMap<String, String> info;
    private static TextView Tv_available_balnce, Tv_currency;
    CustomTextView tv_rideamount, credit_tv_rideamount; /*mopnth_tv, creditt_mopnth_tv*/

    RelativeLayout Tv_confirm;
    private static EditText Et_enteramount;
    private RelativeLayout Rl_debit, Rl_credit;
    public ExpandableHeightGridView GV_payment_list;


    Dialog dialog;
    private boolean isRechargeAvailable = false;
    private String Sauto_charge_status = "";
    private String Str_currentbalance = "", Str_minimum_amt = "", Str_maximum_amt = "", Str_midle_amt = "", ScurrencySymbol = "";

    ArrayList<WalletMoneyPojo> paymnet_list = new ArrayList<WalletMoneyPojo>();

    WalletPaymentListAdapter walletAdapter;
    private ImageLoader imageLoader;
    private ImageView Iv_home, popup_close_wallet, Im_backpress;
    CustomTextView1 payment_proceed;
    private String SpaymentName = "", fromBookingpage = "", frompage = "";

    public static Activity WalletMoneyPage1_class;
    private String sSecurePin = "";

    public static String Scurrency="";
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.package.ACTION_CLASS_CABILY_MONEY_REFRESH")) {
                if (isInternetPresent) {
                    postRequest_WalletMoneyPage1(Iconstant.wallet_money_url);
                }
            }
        }
    }

    private RefreshReceiver refreshReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_wallet_money_page);

        context = WalletMoneyPage1.this;
        WalletMoneyPage1_class = WalletMoneyPage1.this;
        initialize();

        //Start XMPP Chat Service
//        ChatService.startUserAction(WalletMoneyPage1.this);

        Im_backpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (frompage.equalsIgnoreCase("fetching")) {
                    Intent intent = new Intent(WalletMoneyPage1.this, Navigation_new.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else if(session.getbookingonProgress().equalsIgnoreCase("1")){
                    String  object = session.getCurrentBooking();
                    try {
                        JSONObject object1 = new JSONObject(object);
                        Intent intent = new Intent(WalletMoneyPage1.this, BookingPage3.class);
                        intent.putExtra("pickupAddress", object1.getString("pickupAddress"));
                        intent.putExtra("pickupLatitude", object1.getString("pickupLatitude"));
                        intent.putExtra("pickupLongitude",  object1.getString("pickupLongitude"));

                        intent.putExtra("destinationAddress",  object1.getString("destinationAddress"));
                        intent.putExtra("destinationLatitude",  object1.getString("destinationLatitude"));
                        intent.putExtra("destinationLongitude",  object1.getString("destinationLongitude"));
                        intent.putExtra("CategoryID",  object1.getString("CategoryID"));

                        intent.putExtra("pickup_date",  object1.getString("pickup_date"));
                        intent.putExtra("pickup_time",  object1.getString("pickup_time"));

                        intent.putExtra("type",  object1.getString("type"));
                        intent.putExtra("tracking",  object1.getString("tracking"));
                        intent.putExtra("mode",  object1.getString("mode"));

                        intent.putExtra("est_cost",  object1.getString("est_cost"));
                        intent.putExtra("est_cost_booking",  object1.getString("est_cost_booking"));

                        intent.putExtra("approx_dist",  object1.getString("approx_dist"));
                        intent.putExtra("eta",  object1.getString("eta"));
                        intent.putExtra("waitingTime",  object1.getString("waitingTime"));
                        intent.putExtra("extra_check",  "WalletMoneyPage1");
                        intent.putExtra("page", "BookingPage2");
                        if(object1.has("multistop")) {
                            if (object1.getString("multistop").equalsIgnoreCase("multistop")) {

                                Gson gson = new Gson();
                                Type type = new TypeToken<List<MultiDropPojo>>() {
                                }.getType();
                                ArrayList<MultiDropPojo> multiDropList = gson.fromJson(object1.getString("MultipleDropLocation_List"), type);
                                Bundle bundleObject = new Bundle();
                                bundleObject.putSerializable("MultipleDropLocation_List", multiDropList);
                                intent.putExtras(bundleObject);
                            }
                        }
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                } else {
                    Intent intent = new Intent(WalletMoneyPage1.this, Navigation_new.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });



        Rl_debit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WalletMoneyPage1.this, WalletMoneyTransaction1.class);
                intent.putExtra("type", "debit");
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        Rl_credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WalletMoneyPage1.this, WalletMoneyTransaction1.class);
                intent.putExtra("type", "credit");
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        Tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WalletMoneyPage1.this, NewCloudMoneyReloadHomePage.class);
                if(fromBookingpage.equalsIgnoreCase("1")){
                    intent.putExtra("fromBookingpage","1");
                }
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                //WalletPaymentMehtod();
            }
        });


        Rl_enter_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                info = session.getUserCompleteProfile();
                String UserProfileUpdateStatus = info.get(SessionManager.COMPLETE_PROFILE);

                if (UserProfileUpdateStatus.equalsIgnoreCase("0")) {
                    System.out.println("==============Muruga UserProfileUpdateStatus==========" + UserProfileUpdateStatus);
                    UpdateProfilePopUp();
                } else {
                    withdrawOrTransfer();
                }
                //TransferWithDrawlChoosePopuP();
                //confirmPin();
               /* OtpPopUP();*/

            }

        });


    }


    private void UpdateProfilePopUp() {
        final Dialog dialog = new Dialog(WalletMoneyPage1.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup1);
        CustomTextView Tv_Later = (CustomTextView) dialog.findViewById(R.id.later_tv);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_profilechange);
        CheckBox profile_checkbox = (CheckBox) dialog.findViewById(R.id.profile_checkbox);
        String checkBoxStatus = "No";
        if (profile_checkbox.isChecked()) {
            checkBoxStatus = "Yes";
        }
        TextView Profilechange = (TextView) dialog.findViewById(R.id.txt_updaste);
        Typeface tf = Typeface.createFromAsset(WalletMoneyPage1.this.getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        final String finalCheckBoxStatus = checkBoxStatus;
        Tv_Later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                info = session.getUserBasicProfile();
                String basicUserProfileUpdateStatus = info.get(SessionManager.BASIC_PROFILE);
                if (basicUserProfileUpdateStatus.equalsIgnoreCase("0")) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    UpdateProfilePopUp2(finalCheckBoxStatus);
                } else {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    System.out.println("==============Muruga basicUserProfileUpdateStatus==========" + basicUserProfileUpdateStatus);
                    withdrawOrTransfer();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WalletMoneyPage1.this, NewProfile_Page.class);
                intent.putExtra("Register", "1");
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void UpdateProfilePopUp2(final String needAssistance) {

        final Dialog dialog = new Dialog(WalletMoneyPage1.this,  R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup2);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_tv);

        final CustomEdittext userName = (CustomEdittext) dialog.findViewById(R.id.name_title);
        final CustomEdittext userEmail = (CustomEdittext) dialog.findViewById(R.id.email_title);
        final CustomEdittext user_age_et = (CustomEdittext) dialog.findViewById(R.id.user_age_et);
        final ImageView closeimage = (ImageView) dialog.findViewById(R.id.img_close);
        CustomTextView Profilechange = (CustomTextView) dialog.findViewById(R.id.profile_text1);
        Typeface tf = Typeface.createFromAsset(WalletMoneyPage1.this.getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        Spinner spinnerCustom = (Spinner) dialog.findViewById(R.id.gender_spinner1);
        final TextView Tv_city = (TextView) dialog.findViewById(R.id.ratecard_city_textview);
        // Spinner Drop down elements
        ArrayList<String> spinnerItems = new ArrayList<>();
        spinnerItems.add(getResources().getString(R.string.seletc_gender));
        spinnerItems.add(getResources().getString(R.string.male));
        spinnerItems.add(getResources().getString(R.string.female));
        spinnerItems.add(getResources().getString(R.string.others));

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(WalletMoneyPage1.this, spinnerItems);
        spinnerCustom.setAdapter(customSpinnerAdapter);
        spinnerCustom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // close keyboard
                return false;
            }
        });
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                cd = new ConnectionDetector(WalletMoneyPage1.this);
                isInternetPresent = cd.isConnectingToInternet();
                Tv_city.setText(item);
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Tv_city.setText(getResources().getString(R.string.seletc_gender));
            }
        });
        user_age_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    user_age_et.setCursorVisible(false);
                    user_age_et.clearFocus();
                    InputMethodManager mgr = (InputMethodManager)WalletMoneyPage1.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(user_age_et.getWindowToken(), 0);
                }
                return false;
            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userName.getText().toString().length() == 0) {
                    erroredit(userName, getResources().getString(R.string.profile_label_alert_username));
                } else if (!isValidEmail(userEmail.getText().toString().trim().replace(" ", ""))) {
                    erroredit(userEmail, getResources().getString(R.string.profile_label_alert_email));
                } else if (Tv_city.getText().toString().trim().equalsIgnoreCase("SELECT GENDER")) {
                    AlertError(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.profile_label_alert_validgender));
                } else if (user_age_et.getText().toString().length() == 0) {
                    erroredit(user_age_et, getResources().getString(R.string.profile_label_alert_validage));
                } else if (user_age_et.getText().toString().trim().equalsIgnoreCase("0")) {
                    erroredit(user_age_et, getResources().getString(R.string.profile_label_alert_validage));
                } else {
                    if (isInternetPresent) {
                        String username = userName.getText().toString().trim().replace(" ", "");
                        String userEmail1 = userEmail.getText().toString().trim().replace(" ", "");
                        String gender = Tv_city.getText().toString().trim().replace(" ", "");
                        String age = user_age_et.getText().toString().trim().replace(" ", "");
                        postrequestUpdateProfile(Iconstant.get_basic_profile_url, username, userEmail1, gender, needAssistance, age);
                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }
                    dialog.dismiss();
                }
            }
        });
        closeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(user_age_et);
                dialog.dismiss();
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }


    private void postrequestUpdateProfile(String Url, String name, String email, String gender, String needAssistance, String Age) {

        dialog = new Dialog(WalletMoneyPage1.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("user_name", name);
        jsonParams.put("email", email);
        jsonParams.put("gender", gender);
        jsonParams.put("age", Age);
        jsonParams.put("assistance", needAssistance);

        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        mRequest = new ServiceRequest(WalletMoneyPage1.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        session.setUserCompleteProfile("1");
                        Smessage = object.getString("response");
                        Alert1(WalletMoneyPage1.this.getResources().getString(R.string.action_success), Smessage, Sstatus);
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Smessage = object.getString("response");
                        Alert1(WalletMoneyPage1.this.getResources().getString(R.string.action_error), Smessage, Sstatus);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert1(String title, String alert, final String status) {

        final PkDialog mDialog = new PkDialog(WalletMoneyPage1.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);

        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")) {
                    mDialog.dismiss();
                } else {
                    mDialog.dismiss();
                }

            }
        });
        mDialog.show();
    }




    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(WalletMoneyPage1.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }


    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(WalletMoneyPage1.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    private void initialize() {
        session = new SessionManager(WalletMoneyPage1.this);
        cd = new ConnectionDetector(WalletMoneyPage1.this);
        isInternetPresent = cd.isConnectingToInternet();
        imageLoader = new ImageLoader(WalletMoneyPage1.this);


        Tv_available_balnce = (TextView) findViewById(R.id.wallet_page_available_balance_textview);
        Rl_debit = (RelativeLayout) findViewById(R.id.wallet_page_debit_layout);
        Rl_credit = (RelativeLayout) findViewById(R.id.wallet_page_credit_layout);
        Im_backpress = (ImageView) findViewById(R.id.img_back);

        Tv_confirm = (RelativeLayout) findViewById(R.id.wallet_page_confirm_textview);
//        Iv_home = (ImageView) findViewById(R.id.img_home);
        tv_rideamount = (CustomTextView) findViewById(R.id.tv_rideamount);
        credit_tv_rideamount = (CustomTextView) findViewById(R.id.credit_tv_rideamount);
//        mopnth_tv = (CustomTextView) findViewById(R.id.mopnth_tv);
//        creditt_mopnth_tv = (CustomTextView) findViewById(R.id.creditt_mopnth_tv);
       /* CustomTextView debit_tv = (CustomTextView) findViewById(R.id.debit_tv);
        CustomTextView credit_tv = (CustomTextView) findViewById(R.id.credit_tv);*/
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
//        credit_tv.setTypeface(tf);
//        debit_tv.setTypeface(tf);
        Typeface tf1 = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.regular.ttf");
        credit_tv_rideamount.setTypeface(tf1, Typeface.NORMAL);
        tv_rideamount.setTypeface(tf1, Typeface.NORMAL);
        Rl_enter_pin = (RelativeLayout) findViewById(R.id.wallet_enter_pin);

        ImageView Rl_drawer = (ImageView) findViewById(R.id.indicating_image);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);

        if (getIntent().hasExtra("frombookingpage")) {
            fromBookingpage = getIntent().getStringExtra("frombookingpage");
            session.setbookingonProgress(fromBookingpage);
        }
        if (getIntent().hasExtra("frompage")) {
            frompage = getIntent().getStringExtra("frompage");
        }
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        sSecurePin = session.getSecurePin();
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_CLASS_CABILY_MONEY_REFRESH");
        registerReceiver(refreshReceiver, intentFilter);
        if (isInternetPresent) {
            postRequest_WalletMoneyPage1(Iconstant.wallet_money_url);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }
    }

    private void withdrawOrTransfer() {

        final Dialog withdrawOrTransferDialog = new Dialog(WalletMoneyPage1.this, R.style.DialogAnimation);
        withdrawOrTransferDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawOrTransferDialog.setContentView(R.layout.new_cloud_transfer_withdraw_popup);
        withdrawOrTransferDialog.setCanceledOnTouchOutside(true);
        withdrawOrTransferDialog.setCancelable(false);
        withdrawOrTransferDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final RelativeLayout RL_withdraw = (RelativeLayout) withdrawOrTransferDialog.findViewById(R.id.RL_withdrawal);
        final RelativeLayout RL_transfer = (RelativeLayout) withdrawOrTransferDialog.findViewById(R.id.RL_transfer);
        final Button Btn_close = (Button) withdrawOrTransferDialog.findViewById(R.id.btn_close);
        final ImageView close_Iv = (ImageView) withdrawOrTransferDialog.findViewById(R.id.closeimage);
        close_Iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawOrTransferDialog.dismiss();
            }
        });
        RL_withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawOrTransferDialog.dismiss();

                Intent intent = new Intent(WalletMoneyPage1.this, NewCloudMoneyWithDrawHome.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                //dialog.dismiss();
            }
        });

        RL_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawOrTransferDialog.dismiss();
                Intent intent = new Intent(WalletMoneyPage1.this, NewCloudMoneyTransferHome.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                // dialog.dismiss();
            }
        });

        Btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawOrTransferDialog.dismiss();
            }
        });

        withdrawOrTransferDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Rl_enter_pin.setClickable(true);
            }
        });

        withdrawOrTransferDialog.show();
        Rl_enter_pin.setClickable(false);

    }
    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WalletMoneyPage1.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {
        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }

    //--------------Close KeyBoard Method-----------
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //-----------------------Cabily Money Post Request-----------------
    private void postRequest_WalletMoneyPage1(String Url) {
        final Dialog dialog = new Dialog(WalletMoneyPage1.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------WalletMoneyPage1 Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        mRequest = new ServiceRequest(WalletMoneyPage1.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------WalletMoneyPage1 Response----------------" + response);

                String Sstatus = "", Scurrency_code = "", Scurrentbalance = "", Str_currentMonthCredit = "", Str_currentMonthDebit = "", Str_monthYear = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Sauto_charge_status = object.getString("auto_charge_status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {

                            Scurrentbalance = response_object.getString("current_balance");
                            Str_currentbalance = response_object.getString("current_balance");
                            ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(response_object.getString("currency"));
                            Scurrency=response_object.getString("currency");
                            Str_currentMonthCredit = response_object.getString("current_month_credit");
                            Str_currentMonthDebit = response_object.getString("current_month_debit");
                            Str_monthYear = response_object.getString("month_year");
                            Object check_recharge_boundary_object = response_object.get("recharge_boundary");
                            if (check_recharge_boundary_object instanceof JSONObject) {
                                JSONObject recharge_object = response_object.getJSONObject("recharge_boundary");
                                if (recharge_object.length() > 0) {
                                    //  Str_minimum_amt = recharge_object.getString("min_amount");
                                    // Str_maximum_amt = recharge_object.getString("max_amount");
                                    //  Str_midle_amt = recharge_object.getString("middle_amount");
                                    isRechargeAvailable = true;
                                } else {
                                    isRechargeAvailable = false;
                                }
                            }

                            Object check_payment_list_object = response_object.get("payment_list");
                            if (check_payment_list_object instanceof JSONArray) {
                                JSONArray payment_list_array = response_object.getJSONArray("payment_list");

                                if (payment_list_array.length() > 0) {
                                    paymnet_list.clear();
                                    for (int i = 0; i < payment_list_array.length(); i++) {
                                        JSONObject payment_list_object = payment_list_array.getJSONObject(i);
                                        WalletMoneyPojo walletMoneyPojo = new WalletMoneyPojo();
                                        walletMoneyPojo.setPayment_code(payment_list_object.getString("code"));
                                        walletMoneyPojo.setPaymnet_name(payment_list_object.getString("name"));
                                        walletMoneyPojo.setPayment_active_img(payment_list_object.getString("icon"));
                                        walletMoneyPojo.setPayment_normal_img(payment_list_object.getString("inactive_icon"));
                                        //  walletMoneyPojo.setPayment_selected_payment_id("false");
                                        paymnet_list.add(walletMoneyPojo);
                                    }
                                }
                            }

                        }
                    } else {
                        isRechargeAvailable = false;
                    }
                    if (Sstatus.equalsIgnoreCase("1") && isRechargeAvailable) {
                        session.createWalletAmount(ScurrencySymbol +" "+ Str_currentbalance);

                        Tv_available_balnce.setText(ScurrencySymbol + " " + Scurrentbalance);
                        tv_rideamount.setText(ScurrencySymbol + " " + Str_currentMonthDebit);
                        credit_tv_rideamount.setText(ScurrencySymbol + " " + Str_currentMonthCredit);

                        if (dialog != null) {
                            dialog.dismiss();
                        }


                    } else {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            return true;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        if (frompage.equalsIgnoreCase("fetching")) {
            Intent intent = new Intent(WalletMoneyPage1.this, Navigation_new.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }

    }

    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }
}
