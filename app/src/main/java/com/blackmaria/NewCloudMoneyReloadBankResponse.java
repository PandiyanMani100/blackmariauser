package com.blackmaria;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;

/**
 * Created by user144 on 9/18/2017.
 */

public class NewCloudMoneyReloadBankResponse extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView imgBack;
    private RelativeLayout verifylayout;
    private TextView tvAmountcode;
    private TextView dueAmount;
    private String reloadAmoun = "", paymentCode = "";
    private RefreshReceiver refreshReceiver;

    SessionManager session;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadBankResponse.this, Navigation_new.class);
                        startActivity(intent1);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadBankResponse.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadBankResponse.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_money_payment_due);
        initialize();
    }

    private void initialize() {
        session = new SessionManager(NewCloudMoneyReloadBankResponse.this);
        imgBack = (ImageView) findViewById(R.id.img_back);
        dueAmount = (TextView) findViewById(R.id.due_amount);
        verifylayout = (RelativeLayout) findViewById(R.id.verifylayout);
        tvAmountcode = (TextView) findViewById(R.id.tv_amountcode);

        imgBack.setOnClickListener(this);
        verifylayout.setOnClickListener(this);


        reloadAmoun = getIntent().getStringExtra("reloadamount");
        paymentCode = getIntent().getStringExtra("paymentcode");
        String currency = session.getCurrency();
        tvAmountcode.setText(paymentCode);
        dueAmount.setText(getResources().getString(R.string.amountduo) + " " + currency + reloadAmoun);
// -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);
    }

    @Override
    public void onClick(View v) {
        if (v == imgBack) {
            Intent intent = new Intent(NewCloudMoneyReloadBankResponse.this, WalletMoneyPage1.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        } else if (v == verifylayout) {
            Intent intent = new Intent(NewCloudMoneyReloadBankResponse.this, WalletMoneyPage1.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }
}
