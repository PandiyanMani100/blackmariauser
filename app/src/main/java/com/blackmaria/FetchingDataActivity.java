package com.blackmaria;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.IdentifyAppKilled;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.PkDialogWithoutButton;
import com.blackmaria.xmpp.XmppService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;


public class FetchingDataActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int REQUEST_WRITE_STORAGE_REQUEST_CODE = 201;
    //    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    Context context;
    GPSTracker gps;
    private LanguageDb mHelper;
    double MyCurrent_lat = 0.0;
    double MyCurrent_long = 0.0;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    private String userID = "", sLatitude = "", sLongitude = "";
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    private String Str_HashKey = "";
    String termsCondition = "";
    Handler handler;
    Handler handler_serlocationuser;

    private boolean isAppInfoAvailable = false;

    final int PERMISSION_REQUEST_CODE = 111;

    String sPendingRideId = "", sRatingStatus = "", sCategoryImage = "", sOngoingRide = "", sOngoingRideId = "";
    private SessionManager sessionManager;
    private PkDialogWithoutButton mInfoDialog;
    String appPackageName = "";
    private ServiceRequest mRequest;
    private String currentVersion = "";
    AppInfoSessionManager appInfo_Session;
    private CustomTextView txt_label_please_wait;
    private String server_mode, site_mode, site_string, site_url, gender = "", referalStatus = "", userRating, app_identity_name = "", Language_code = "", profile_complete = "", phone_masking_status = "";
    JSONObject extra;
    private String Str_action = "";

    Location mLocation;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private long UPDATE_INTERVAL = 15000;  /* 15 secs */
    private long FASTEST_INTERVAL = 5000; /* 5 secs */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fetching_data);
        mHelper=new LanguageDb(this);
        handler = new Handler();
        handler_serlocationuser = new Handler();
        context = getApplicationContext();
        cd = new ConnectionDetector(FetchingDataActivity.this);
        isInternetPresent = cd.isConnectingToInternet();
        final ImageView Rl_drawer = (ImageView) findViewById(R.id.fetchinggif);
        txt_label_please_wait = (CustomTextView) findViewById(R.id.txt_label_please_wait);
        try {
            /*GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(Rl_drawer);
            Glide.with(this).load(R.drawable.loading_gif).into(imageViewTarget);*/

            Glide.with(FetchingDataActivity.this)
                    .load(R.drawable.loading_gif)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            Rl_drawer.setImageDrawable(resource);
                        }
                    });
        } catch (Exception e) {
        }
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));

                Str_HashKey = (Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("Str_HashKey--------------" + Str_HashKey);
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        // Session class instance

        sessionManager = new SessionManager(FetchingDataActivity.this);
        // sessionManager.logoutUser();
        appInfo_Session = new AppInfoSessionManager(FetchingDataActivity.this);
        gps = new GPSTracker(FetchingDataActivity.this);



        try {
            if(getIntent().hasExtra("json")){
                String getvalue = getIntent().getStringExtra("json");
                extra = new JSONObject(getvalue);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        txt_label_please_wait.setText(getkey("connecting_data"));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission() || !checkCameraPermission()) {
//                        requestPermission();
                        requestAppPermissions();
                    } else {
                        removerunnable();
                        handler.postDelayed(runnable, 2000);
                    }
                } else {

                    removerunnable();
                    handler.postDelayed(runnable, 2000);
                }
            }
        }, SPLASH_TIME_OUT);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }


    public void removerunnable() {
        if (runnable != null) {
            handler.removeCallbacks(runnable);
        }
    }

    public void removerunnable_setlocationuser() {
        if (runnable_setlocationuser != null) {
            handler_serlocationuser.removeCallbacks(runnable_setlocationuser);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        startLocationUpdates();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else
                finish();

            return false;
        }
        return true;
    }

    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
          }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }


    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            setLocation();
        }
    };

    Runnable runnable_setlocationuser = new Runnable() {
        @Override
        public void run() {
            if (MyCurrent_lat != 0.0) {
                postRequest_AppInformation(Iconstant.getAppInfo);
            } else {
                removerunnable_setlocationuser();
                handler_serlocationuser.postDelayed(runnable_setlocationuser, 2000);
            }
        }
    };


    private void setLocation() {
        cd = new ConnectionDetector(FetchingDataActivity.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            if (MyCurrent_lat != 0.0) {
                sLatitude = String.valueOf(MyCurrent_lat);
                sLongitude = String.valueOf(MyCurrent_long);
                try {
                    currentVersion = FetchingDataActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
//                    FetchingDataActivity.GetVersionCode getVersionCode = new FetchingDataActivity.GetVersionCode();
//                    getVersionCode.execute();
                    postRequest_AppInformation(Iconstant.getAppInfo);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            } else {
//                handler.postDelayed(runnable, 2000);
                enableGpsService();
            }
        } else {

            final PkDialog mDialog = new PkDialog(FetchingDataActivity.this);
            mDialog.setDialogTitle(getkey("alert_nointernet"));
            mDialog.setDialogMessage(getkey("alert_nointernet_message"));
            mDialog.setPositiveButton(getkey("timer_label_alert_retry"), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    setLocation();
                }
            });
            mDialog.setNegativeButton(getkey("timer_label_alert_cancel"), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();

        }

    }

    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        gps = new GPSTracker(FetchingDataActivity.this);
                        removerunnable();
                        handler.postDelayed(runnable, 2000);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(FetchingDataActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
//                        Toast.makeText(FetchingDataActivity.this, "Location enabled!", Toast.LENGTH_LONG).show();
                        removerunnable();
                        handler.postDelayed(runnable, 2000);
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        finish();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }





    @Override
    public void onLocationChanged(Location location) {
        MyCurrent_lat = location.getLatitude();
        MyCurrent_long = location.getLongitude();
        sLatitude = String.valueOf(MyCurrent_lat);
        sLongitude = String.valueOf(MyCurrent_long);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }




    //-----------------------User Current Location Post Request-----------------
    private void postRequest_SetUserLocation(String Url) {

        try {
            removerunnable_setlocationuser();
            HashMap<String, String> user = sessionManager.getUserDetails();
            userID = user.get(SessionManager.KEY_USERID);
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", userID);
            jsonParams.put("latitude", sLatitude);
            jsonParams.put("longitude", sLongitude);

            txt_label_please_wait.setText(getkey("updstelocation"));
            mRequest = new ServiceRequest(FetchingDataActivity.this);
            mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {

                    System.out.println("-------------Splash UserLocation Response----------------" + response);

                    String Str_status = "", sCategoryID = "", sTripProgress = "", sRideId = "", sRideStage = "";
                    try {
                        JSONObject object = new JSONObject(response);
                        Str_status = object.getString("status");
                        if (object.has("category_id")) {
                            sCategoryID = object.getString("category_id");
                        } else {
                            sCategoryID = "";
                        }

//                    sTripProgress = object.getString("trip_in_progress");
//                    sRideId = object.getString("ride_id");
//                    sRideStage = object.getString("ride_stage");
                        if (Str_status.equalsIgnoreCase("1")) {
                            sessionManager.setCategoryID(sCategoryID);
//                        sessionManager.setRidePendingStatus(sTripProgress,sRideId,sRideStage);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (sessionManager.isLoggedIn()) {
                        //Starting the xmpp service
                        if (!isMyServiceRunning(XmppService.class)) {
                            System.out.println("-----------OtpPage xmpp service start7---------");
                            startService(new Intent(FetchingDataActivity.this, XmppService.class));
                        }
                        if (extra != null && extra.has("action")) {
                            if (extra.has("action")) {
                                try {
                                    Str_action = extra.getString("action").toString();
                                } catch (JSONException e) {
                                }
                            }
                            System.out.println("-----------pushnotification action-----------------" + Str_action);
                            if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_AcceptRide_Key)) {
                                sessionManager.setCouponCode("", "");
                                sessionManager.setReferralCode("", "");
                                try {
                                    Intent i = new Intent(FetchingDataActivity.this, TrackRideAcceptPage.class);
                                    i.putExtra("driverID", extra.getString(Iconstant.DriverID));
                                    i.putExtra("driverName", extra.getString(Iconstant.DriverName));
                                    i.putExtra("driverImage", extra.getString(Iconstant.DriverImage));
                                    i.putExtra("driverRating", extra.getString(Iconstant.DriverRating));
                                    i.putExtra("driverTime", extra.getString(Iconstant.DriverTime));
                                    i.putExtra("rideID", extra.getString(Iconstant.RideID));
                                    i.putExtra("driverMobile", extra.getString(Iconstant.DriverMobile));
                                    i.putExtra("driverCar_no", extra.getString(Iconstant.DriverCar_No));
                                    i.putExtra("driverCar_model", extra.getString(Iconstant.DriverCar_Model));
                                    i.putExtra("flag", "1");
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    finish();
                                } catch (JSONException e) {
                                }
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_CabArrived_Key)) {
                                try {
                                    Intent intent1 = new Intent(FetchingDataActivity.this, TrackArrivePage.class);
                                    intent1.putExtra("ride_id", extra.getString("key1"));
                                    intent1.putExtra("car_no", extra.getString(Iconstant.DriverRating));
                                    intent1.putExtra("freewatingtime", extra.getString(Iconstant.DriverLat));
                                    startActivity(intent1);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                    finish();
                                } catch (JSONException e) {
                                }
                            } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCancelled_Key)) {
                                Intent intent = new Intent(FetchingDataActivity.this, Navigation_new.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCompleted_Key) || Str_action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key)) {
                                try {
                                    Intent intent1 = new Intent(FetchingDataActivity.this, FareBreakUp.class);
                                    intent1.putExtra("RideID", extra.getString(Iconstant.RideID_Cancelled));
                                    intent1.putExtra("ratingflag", "2");
                                    startActivity(intent1);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                    finish();
                                } catch (JSONException e) {
                                }
                            } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_Key)) {
                                try {
                                    Intent intent1 = new Intent(FetchingDataActivity.this, FareBreakUp.class);
                                    intent1.putExtra("RideID", extra.getString(Iconstant.RideID_Request_Payment));
                                    intent1.putExtra("ratingflag", "2");
                                    startActivity(intent1);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                    finish();
                                } catch (JSONException e) {
                                }
                            } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_PaymentPaid_Key)) {
                                try {
                                    Intent intent1 = new Intent(FetchingDataActivity.this, FareBreakUp.class);
                                    intent1.putExtra("RideID", extra.getString(Iconstant.RideID_Payment_paid));
                                    intent1.putExtra("ratingflag", "1");
                                    startActivity(intent1);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                    finish();
                                } catch (JSONException e) {
                                }
                            } else if (Str_action.equalsIgnoreCase(Iconstant.pushNotificationBeginTrip)) {
                                try {
                                    Intent intent1 = new Intent(FetchingDataActivity.this, TrackRidePage.class);
                                    intent1.putExtra("ride_id", extra.getString("key1"));
                                    startActivity(intent1);
                                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                    finish();
                                } catch (JSONException e) {
                                }
                            } else if (Str_action.equalsIgnoreCase(Iconstant.pushNotificationBeginTripReturn)) {
                                try {
                                    Intent intent1 = new Intent(FetchingDataActivity.this, TrackRidePage.class);
                                    intent1.putExtra("ride_id", extra.getString("key1"));
                                    startActivity(intent1);
                                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                    finish();
                                } catch (JSONException e) {
                                }
                            } else if (Str_action.equalsIgnoreCase(Iconstant.pushNotificationMultistop)) {
                                try {
                                    Intent intent1 = new Intent(FetchingDataActivity.this, TrackRidePage.class);
                                    intent1.putExtra("ride_id", extra.getString("key1"));
                                    startActivity(intent1);
                                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                    finish();
                                } catch (JSONException e) {
                                }
                            } else if (Str_action.equalsIgnoreCase(Iconstant.Drop_Waiting_time_closed_key)) {
                                try {
                                    Intent intent1 = new Intent(FetchingDataActivity.this, TrackRidePage.class);
                                    intent1.putExtra("ride_id", extra.getString("key1"));
                                    startActivity(intent1);
                                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                    finish();
                                } catch (JSONException e) {
                                }
                            } else if (Str_action.equalsIgnoreCase(Iconstant.complaint_replay_notification)) {
                                try {
                                    Intent intent1 = new Intent(FetchingDataActivity.this, ComplaintPageViewDetailPage.class);
                                    intent1.putExtra("ticket_id", extra.getString(Iconstant.ticketId));
                                    startActivity(intent1);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                    finish();
                                } catch (JSONException e) {
                                }
                            }
                            //new
                            else if (Str_action.equalsIgnoreCase(Iconstant.wallet_success)) {

                                Intent intent = new Intent(FetchingDataActivity.this, Navigation_new.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            } else if (Str_action.equalsIgnoreCase(Iconstant.bank_success)) {
                                Intent intent = new Intent(FetchingDataActivity.this, Navigation_new.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else if (Str_action.equalsIgnoreCase(Iconstant.wallet_credit)) {
                                Intent intent = new Intent(FetchingDataActivity.this, Navigation_new.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else if (Str_action.equalsIgnoreCase(Iconstant.drop_user_key)) {
                                try {
                                    sessionManager.setContinueTrip("no");
                                    sessionManager.setWaitedTime(0, 0, "", "");
                                    Intent intent = new Intent(FetchingDataActivity.this, WaitingTimePage.class);
                                    intent.putExtra("ride_id", extra.getString("key1"));
                                    intent.putExtra("FreeWaitingTime", extra.getString("key2"));
                                    intent.putExtra("IsNormalOrreturnMulti", extra.getString("key3"));
                                    startActivity(intent);
                                    finish();
                                } catch (JSONException e) {
                                }
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else if (Str_action.equalsIgnoreCase(Iconstant.referalcredit_amount)) {
                                Intent intent = new Intent(FetchingDataActivity.this, CloudMoneyHomePage.class);
                                intent.putExtra("frompage", "fetching");
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else {
//                            if (termsCondition.equalsIgnoreCase("0")) {
//                                FirstTimeLoginCheck();
//                            } else {
                                Intent intent = new Intent(FetchingDataActivity.this, Navigation_new.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                            }
                            }
                        } else {

                            if ("1".equalsIgnoreCase(profile_complete)) {

//                            if (termsCondition.equalsIgnoreCase("0")) {
//                                FirstTimeLoginCheck();
//                            } else {
                                Intent intent = new Intent(FetchingDataActivity.this, Navigation_new.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                            }
                            } else {
//                            if (termsCondition.equalsIgnoreCase("0")) {
//                                FirstTimeLoginCheck();
//                            } else {
                                Intent intent = new Intent(FetchingDataActivity.this, Navigation_new.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                            }
                            }
                        }
                    } else {
                        Intent i = new Intent(FetchingDataActivity.this, SignUpPage.class);
                        startActivity(i);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }

                }

                @Override
                public void onErrorListener() {
                    //Starting the xmpp service
                    if (!isMyServiceRunning(XmppService.class)) {
                        System.out.println("-----------OtpPage xmpp service start8---------");
                        startService(new Intent(FetchingDataActivity.this, XmppService.class));
                    }
                }
            });
        } catch (Exception e) {
        }
    }





    //-----------------------App Information Post Request-----------------
    private void postRequest_AppInformation(String Url) {
        System.out.println("-------------Splash App Information Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "user");
        jsonParams.put("id", userID);
        jsonParams.put("lat", sLatitude);
        jsonParams.put("lon", sLongitude);
        System.out.println("--------App Information jsonParams--------" + jsonParams);
        txt_label_please_wait.setText(getkey("fetching_data_label_fetching_data"));
        mRequest = new ServiceRequest(FetchingDataActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Splash App Information Response----------------" + response);
                String Str_status = "", SuserImage = "", userGuide = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                sessionManager.setCustomerServiceNo(sCustomerServiceNumber);
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                              /*  sFacebookId = info_object.getString("facebook_app_id");
                                sGooglePlusId = info_object.getString("google_plus_app_id");
                                sPhoneMasking = info_object.getString("phone_masking_status");*/
                                app_identity_name = info_object.getString("app_identity_name");
                                server_mode = info_object.getString("server_mode");
                                site_mode = info_object.getString("site_mode");
                                site_string = info_object.getString("site_mode_string");
                                site_url = info_object.getString("site_url");
                                profile_complete = info_object.getString("profile_complete");
                                phone_masking_status = info_object.getString("phone_masking_status");
                                String phantomcar = info_object.getString("phantom_car");
                                sessionManager.setPhantomcar(phantomcar);
                                termsCondition = info_object.getString("terms_condition");
                                if(info_object.has("s3_status"))
                                {
                                     if(info_object.getString("s3_status").equals("1"))
                                     {
                                         JSONObject s3_info = info_object.getJSONObject("s3_info");

                                         String access_key = s3_info.getString("access_key");
                                         String secret_key = s3_info.getString("secret_key");
                                         String bucket_name = s3_info.getString("bucket_name");
                                         String bucket_url = s3_info.getString("bucket_url");
                                         sessionManager.setAmazondetails(info_object.getString("s3_status"),access_key,secret_key,bucket_name,bucket_url);
                                     }
                                     else
                                     {
                                         sessionManager.setImagestatus("0");
                                     }
                                }


                                SuserImage = info_object.getString("user_image");
                                if(!SuserImage.equals("")){
                                    sessionManager.setUserImageUpdate(SuserImage);
                                }

                                try {
                                    JSONArray jsonarray_emergendy_no = info_object.getJSONArray("emergencyNumbers");
                                    for (int i = 0; i <= jsonarray_emergendy_no.length() - 1; i++) {
                                        String value = jsonarray_emergendy_no.getJSONObject(i).getString("title");
                                        if (value.equalsIgnoreCase("Police")) {
                                            sessionManager.setPolice(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Fire")) {
                                            sessionManager.setFire(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Ambulance")) {
                                            sessionManager.setAmbulance(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        }
                                    }

                                } catch (Exception e) {

                                }
                                if (info_object.has("pattern_code_status")) {
                                    if (info_object.has("pattern_code")) {
                                        sessionManager.setpattern(info_object.getString("pattern_code"));
                                    }
                                }

                                if (info_object.has("user_name")) {
                                    sessionManager.setUserNameUpdate(info_object.getString("user_name"));
                                }


                                if (info_object.has("cashback_mode")) {
                                    sessionManager.setCashbackStatus(info_object.getString("cashback_mode"));
                                }

                                if (info_object.has("with_pincode")) {
                                    sessionManager.setSecurePin(info_object.getString("with_pincode"));
                                }
                                if (info_object.has("currency")) {
                                    sessionManager.setCurrency(info_object.getString("currency"));
                                }
                                if (info_object.has("avg_review")) {
                                    userRating = info_object.getString("avg_review");
                                }
                                if (info_object.has("referral_staus")) {
                                    referalStatus = info_object.getString("referral_staus");
                                    sessionManager.setReferalStatus(referalStatus);
                                }

                                if (info_object.has("gender")) {
                                    gender = info_object.getString("gender");
                                    sessionManager.setGender(gender);
                                }

                                if (info_object.has("ride_earn")) {

                                    sessionManager.setReferelStatus(info_object.getString("ride_earn"));
                                }

                                if (info_object.has("user_guide")) {
                                    userGuide = info_object.getString("user_guide");
                                    sessionManager.setUserGuidePdf(userGuide);
                                }
                                /* Language_code="ta";*/
                                Language_code = info_object.getString("lang_code");
                                Language_code="in";

                                isAppInfoAvailable = true;
                            } else {
                                isAppInfoAvailable = false;
                            }

                           /* sPendingRideId= response_object.getString("pending_rideid");
                            sRatingStatus= response_object.getString("ride_status");*/

                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }
                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {

                        appInfo_Session.setAppInfo(sContact_mail, sCustomerServiceNumber, sSiteUrl, sXmppHostUrl, sHostName, sFacebookId, sGooglePlusId, sCategoryImage, sOngoingRide, sOngoingRideId, sPendingRideId, sRatingStatus, app_identity_name, userRating, SuserImage);

                        HashMap<String, String> language = sessionManager.getLanaguage();
                        Locale locale = null;

                        locale = new Locale("in");
                        sessionManager.setlamguage("in", "in");

                        sessionManager.setXmpp(sXmppHostUrl, sHostName);
                        sessionManager.setAgent(app_identity_name);

                        sessionManager.setPhoneMaskingStatus(phone_masking_status);

                        postRequest_SetUserLocation(Iconstant.setUserLocation);


                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
//                Toast.makeText(context, Iconstant.Url, Toast.LENGTH_SHORT).show();
            }
        });


    }




    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            int result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

            return true;
        } else {
            return false;
        }
    }

    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();

                    removerunnable();
                    handler.postDelayed(runnable, 2000);
//                    setLocation();
                } else {
                    finish();
                }
                break;
        }
    }

//permission read and write storage

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions() && hasLocatePermissions() && hasCameraPermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.CAMERA

                }, REQUEST_WRITE_STORAGE_REQUEST_CODE); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasLocatePermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasCameraPermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    }


    //----------------------------------------------

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mInfoDialog != null) {
            mInfoDialog.dismiss();
        }

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

        removerunnable();
        removerunnable_setlocationuser();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mInfoDialog != null) {
            mInfoDialog.dismiss();
        }
        //--------Start Service to identify app killed or not---------
        if (!isMyServiceRunning(IdentifyAppKilled.class)) {
            System.out.println("-----------OtpPage xmpp service start6---------");
            startService(new Intent(FetchingDataActivity.this, IdentifyAppKilled.class));
        }

        if (!checkPlayServices()) {
            Toast.makeText(this, getkey("pls_install_play"), Toast.LENGTH_SHORT).show();
        }

    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            return true;
        }
        return false;
    }

    private String getkey(String key)
    {
        return mHelper.getvalueforkey(key);
    }


}