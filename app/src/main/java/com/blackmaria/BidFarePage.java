package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.CustomSpinnerAdapter;
import com.blackmaria.newdesigns.fastwallet.Fastpaypincheck;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 3/15/2017.
 */

public class BidFarePage extends Activity implements View.OnClickListener {

    private TextView Tv_bookNow,Tv_LearnMore;
    private ImageView Iv_callSos,Tv_close;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;
    private String sSelectedPanicNumber = "";
    final int PERMISSION_REQUEST_CODE = 111;
    private String sDriveID = "", basicUserProfileUpdateStatus = "", UserProfileUpdateStatus = "", UserID = "";
    private RefreshReceiver refreshReceiver;
    private ServiceRequest mRequest;
    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(MileageDetails.this, Fragment_HomePage.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(BidFarePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(BidFarePage.this, FareBreakUp.class);
                        intent1.putExtra("ratingflag", "2");
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bidfare1);
        initialize();
    }

    private void initialize() {
        Tv_close = (ImageView) findViewById(R.id.bid_fare_cancel_textview);
        Tv_bookNow = (TextView) findViewById(R.id.bid_fare_booknow_textview);
        Tv_LearnMore= (TextView) findViewById(R.id.learnmorepopup);

        sessionManager = new SessionManager(BidFarePage.this);
        cd = new ConnectionDetector(BidFarePage.this);
        isInternetPresent = cd.isConnectingToInternet();

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        HashMap<String, String> info = sessionManager.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> user1 = sessionManager.getUserBasicProfile();
        basicUserProfileUpdateStatus = user1.get(SessionManager.BASIC_PROFILE);

        HashMap<String, String> user2 = sessionManager.getUserCompleteProfile();
        UserProfileUpdateStatus = user2.get(SessionManager.COMPLETE_PROFILE);
        Tv_close.setOnClickListener(this);
        Tv_bookNow.setOnClickListener(this);
        Tv_LearnMore.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == Tv_close) {
            Intent intent = new Intent(BidFarePage.this, Navigation_new.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();

        } else  if (view == Tv_bookNow) {
            if (UserProfileUpdateStatus.equalsIgnoreCase("0")) {
                System.out.println("==============Muruga UserProfileUpdateStatus==========" + UserProfileUpdateStatus);
                UpdateProfilePopUp();
            } else {
                System.out.println("==============Muruga UserProfileUpdateStatus==========" + UserProfileUpdateStatus);
                Intent intent = new Intent(BidFarePage.this, Navigation_new.class);
                intent.putExtra("str_page", "bidfare");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        }else  if (view == Tv_LearnMore) {
            HelpPopUP();
        }
    }
    private void UpdateProfilePopUp() {
        final Dialog dialog = new Dialog(BidFarePage.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup1);
        CustomTextView Tv_Later = (CustomTextView) dialog.findViewById(R.id.later_tv);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_profilechange);

        CheckBox profile_checkbox=(CheckBox) dialog.findViewById(R.id.profile_checkbox);
        String checkBoxStatus="No";
        if(profile_checkbox.isChecked()){
            checkBoxStatus="Yes";
        }
        TextView Profilechange = (TextView) dialog.findViewById(R.id.txt_updaste);
        Typeface tf= Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        final String finalCheckBoxStatus = checkBoxStatus;
        Tv_Later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (basicUserProfileUpdateStatus.equalsIgnoreCase("0")) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    UpdateProfilePopUp2(finalCheckBoxStatus);
                } else {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    System.out.println("==============Muruga basicUserProfileUpdateStatus==========" + basicUserProfileUpdateStatus);
                    Intent intent = new Intent(BidFarePage.this, Navigation_new.class);
                    intent.putExtra("str_page", "bidfare");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_profile = new Intent(BidFarePage.this, Fastpaypincheck.class);
                intent_profile.putExtra("frommenu", "1");
                startActivity(intent_profile);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // remove background dim
        // dialog.getWindow().setDimAmount(0);

        dialog.show();
    }

    private void UpdateProfilePopUp2(final String needAssistance) {

        final Dialog dialog = new Dialog(BidFarePage.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.updateprofile_popup2);
        CustomTextView Ok_Profilechange = (CustomTextView) dialog.findViewById(R.id.ok_tv);

        final CustomEdittext user_age_et = (CustomEdittext) dialog.findViewById(R.id.user_age_et);
        final CustomEdittext userName = (CustomEdittext) dialog.findViewById(R.id.name_title);
        final CustomEdittext userEmail = (CustomEdittext) dialog.findViewById(R.id.email_title);

        CustomTextView Profilechange = (CustomTextView) dialog.findViewById(R.id.profile_text1);
        Typeface tf= Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
        Profilechange.setTypeface(tf);

        Spinner spinnerCustom = (Spinner) dialog.findViewById(R.id.gender_spinner1);
        final TextView Tv_city = (TextView) dialog.findViewById(R.id.ratecard_city_textview);
        // Spinner Drop down elements
        ArrayList<String> spinnerItems = new ArrayList<>();
        spinnerItems.add(getResources().getString(R.string.myprofile_dialog_form_gender_label));
        spinnerItems.add(getResources().getString(R.string.male));
        spinnerItems.add(getResources().getString(R.string.female));
        spinnerItems.add(getResources().getString(R.string.others));

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(BidFarePage.this, spinnerItems);
        spinnerCustom.setAdapter(customSpinnerAdapter);

        spinnerCustom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // close keyboard
                return false;
            }
        });
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                Tv_city.setText(item);
                cd = new ConnectionDetector(BidFarePage.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (!item.equalsIgnoreCase("GENDER")) {
                    if (isInternetPresent) {
                        System.out.println("-------------selected gender-----------------" + item);
                    } }}
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Tv_city.setText(getResources().getString(R.string.myprofile_dialog_form_gender_label));
            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userName.getText().toString().length() == 0) {
                    erroredit(userName, getResources().getString(R.string.profile_label_alert_username));
                } else if (!isValidEmail(userEmail.getText().toString().trim().replace(" ", ""))) {
                    erroredit(userEmail, getResources().getString(R.string.profile_label_alert_email));
                }  else if (user_age_et.getText().toString().length() == 0) {
                    erroredit(userName, getResources().getString(R.string.profile_label_alert_username));
                }else {
                    if (isInternetPresent) {
                        String username = userName.getText().toString().trim().replace(" ", "");
                        String userEmail1 = userEmail.getText().toString().trim().replace(" ", "");
                        String gender=Tv_city.getText().toString().trim().replace(" ","");
                        String age=user_age_et.getText().toString().trim().replace(" ","");
                        postrequestUpdateProfile(Iconstant.get_basic_profile_url, username, userEmail1,gender,needAssistance,age);

                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }
                    dialog.dismiss();
                }


            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);

        dialog.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(BidFarePage.this, Navigation_new.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();


    }

    private void postrequestUpdateProfile(String Url, String name, String email, String gender, String needAssistance, String Age) {
        final Dialog dialog2 = new Dialog(BidFarePage.this);
        dialog2.getWindow();
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.custom_loading);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();

        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("user_name", name);
        jsonParams.put("email", email);
        jsonParams.put("gender", gender);
        jsonParams.put("age", Age);
        jsonParams.put("assistance", needAssistance);
        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        mRequest = new ServiceRequest(BidFarePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");
                        Alert1(getResources().getString(R.string.action_success), Smessage, Sstatus);
                        if (dialog2 != null) {
                            dialog2.dismiss();
                        }
                    } else {
                        if (dialog2 != null) {
                            dialog2.dismiss();
                        }
                        Smessage = object.getString("response");
                        Alert1(getResources().getString(R.string.action_error), Smessage, Sstatus);
                    }
                    if (dialog2 != null) {
                        dialog2.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog2 != null) {
                    dialog2.dismiss();
                }
            }
        });
        if (dialog2 != null) {
            dialog2.dismiss();
        }
    }
    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(BidFarePage.this, R.anim.shake);
        editname.startAnimation(shake);
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }
    private void Alert1(String title, String alert, final String status) {
        final PkDialog mDialog = new PkDialog(BidFarePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);

        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")) {
                    Intent intent = new Intent(BidFarePage.this, Navigation_new.class);
                    intent.putExtra("str_page", "bidfare");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                    mDialog.dismiss();
                } else {
                    mDialog.dismiss();
                }

            }
        });
        mDialog.show();
    }
    private void HelpPopUP() {
        final Dialog dialog = new Dialog(BidFarePage.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bidfare_popup);

        CustomTextView text1=(CustomTextView)dialog.findViewById(R.id.custom_text1);
        CustomTextView text2=(CustomTextView)dialog.findViewById(R.id.custom_text2);
        CustomTextView text3=(CustomTextView)dialog.findViewById(R.id.custom_text4);
        Typeface tf= Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
        text1.setTypeface(tf);
        text2.setTypeface(tf);
        text3.setTypeface(tf);
        ImageView closeImage;
        closeImage=(ImageView)dialog.findViewById(R.id.image_close);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
           /* Intent intent = new Intent(BidFarePage.this, Navigation_new.class);
            startActivity(intent);*/
            overridePendingTransition(R.anim.exit, R.anim.enter);
            finish();
            return true;
        }
        return false;
    }
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(BidFarePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }
}

