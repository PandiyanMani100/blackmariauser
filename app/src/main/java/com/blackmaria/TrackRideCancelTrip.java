package com.blackmaria;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.MyRideCancelTripAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.CancelTripPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TrackRideCancelTrip extends ActivityHockeyApp {
    private RelativeLayout back;
    private TextView home_button_layout,cancelimage;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private static Context context;
    private SessionManager session;
    private String UserID = "";
    LanguageDb mhelper;
    private ServiceRequest mRequest;
    Dialog dialog;
    ArrayList<CancelTripPojo> itemlist;
    MyRideCancelTripAdapter adapter;
    private ListView listview;
    private String SrideId_intent = "", driverImage, sShare_ride_status = "", sShare_pool_ride_id = "";
    private String cancelReason = "";
    private RefreshReceiver refreshReceiver;
    private RoundedImageView cancel_tripDriverImage;
    private ImageView blinking_image;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        if(TrackRidePage.trackyour_ride_class!= null){
                            TrackRidePage.trackyour_ride_class.finish();
                        }

                        Intent intent1 = new Intent(TrackRideCancelTrip.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        if(TrackRidePage.trackyour_ride_class!= null){
                            TrackRidePage.trackyour_ride_class.finish();
                        }
                        Intent intent1 = new Intent(TrackRideCancelTrip.this, FareBreakUp.class);
                        intent1.putExtra("RideID", SrideId_intent);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        if(TrackRidePage.trackyour_ride_class!= null){
                            TrackRidePage.trackyour_ride_class.finish();
                        }
                        Intent intent1 = new Intent(TrackRideCancelTrip.this, FareBreakUp.class);
                        intent1.putExtra("RideID", SrideId_intent);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancelreasonride_layout);
        mhelper = new LanguageDb(this);
        context = getApplicationContext();
        initialize();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
        home_button_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(TrackRideCancelTrip.this,Navigation_new.class);
                startActivity(in);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
        cancelimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(TrackRideCancelTrip.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    if (!cancelReason.equalsIgnoreCase("")) {
                        cancel_MyRide(Iconstant.cancel_myride_url, cancelReason);
                    } else {
                        Alert(getkey("alert"), getkey("alert_label"));
                    }
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }

            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cancelReason = itemlist.get(position).getReasonId();
                for (int i = 0; i < itemlist.size(); i++) {
                    if (i == position) {
                        itemlist.get(i).setSetPayment_selected_cancel_id("true");
                    } else {
                        itemlist.get(i).setSetPayment_selected_cancel_id("false");
                    }
                }
//                cd = new ConnectionDetector(TrackRideCancelTrip.this);
//                isInternetPresent = cd.isConnectingToInternet();
//                if (isInternetPresent) {
//                    if (!cancelReason.equalsIgnoreCase("")) {
//                        cancel_MyRide(Iconstant.cancel_myride_url, cancelReason);
//                    } else {
//                            }
//                } else {
//                   }

                adapter.notifyDataSetChanged();
            }
        });

    }

    private void initialize() {
        session = new SessionManager(TrackRideCancelTrip.this);
        cd = new ConnectionDetector(TrackRideCancelTrip.this);
        isInternetPresent = cd.isConnectingToInternet();
        home_button_layout=  findViewById(R.id.home_button_layout);
        home_button_layout.setText(getkey("nothanks"));
        back = findViewById(R.id.cancel_trip_trackme_layout);
        listview =  findViewById(R.id.my_rides_cancel_trip_listView);
        cancelimage =  findViewById(R.id.cancelimage_layout);
        cancelimage.setText(getkey("cancelthistrip"));
        cancel_tripDriverImage = findViewById(R.id.cancel_trip_user_imageview);

        TextView iamon =  findViewById(R.id.iamon);
        iamon.setText(getkey("hiontheway"));

        TextView maxth =  findViewById(R.id.maxth);
        maxth.setText(getkey("maximumcancelation"));

        TextView tracme =  findViewById(R.id.tracme);
        tracme.setText(getkey("trackme_retry"));

        blinking_image = (ImageView) findViewById(R.id.blinking_image);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        blinking_image.startAnimation(animation);

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        Intent intent = getIntent();
        SrideId_intent = intent.getStringExtra("RideID");
        driverImage = intent.getStringExtra("driverImage");
        if (!driverImage.equalsIgnoreCase("")) {
            Picasso.with(getApplicationContext()).load(String.valueOf(driverImage)).placeholder(R.drawable.user_icon).into(cancel_tripDriverImage);
        }
        try {
            Bundle bundleObject = getIntent().getExtras();
            itemlist = (ArrayList<CancelTripPojo>) bundleObject.getSerializable("Reason");
            adapter = new MyRideCancelTripAdapter(TrackRideCancelTrip.this, itemlist);
            listview.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(TrackRideCancelTrip.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-----------------------Cancel Myride Post Request-----------------
    private void cancel_MyRide(String Url, final String reasonId) {
        dialog = new Dialog(TrackRideCancelTrip.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("my_rides_cancel_trip_action_cancel"));


        System.out.println("-------------Cancel Myride Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", SrideId_intent);
        jsonParams.put("reason", reasonId);
        System.out.println("-------------Cancel Myride jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(TrackRideCancelTrip.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Cancel Myride Response----------------" + response);

                String Sstatus = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            String message = response_object.getString("message");


                            final PkDialog mDialog = new PkDialog(TrackRideCancelTrip.this);
                            mDialog.setDialogTitle(getkey("action_success"));
                            mDialog.setDialogMessage(message);
                            mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                    TrackRidePage.trackyour_ride_class.finish();
                                    Intent intent = new Intent(TrackRideCancelTrip.this, Navigation_new.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    finish();
                                }
                            });
                            mDialog.show();

                        }
                    } else if (Sstatus.equalsIgnoreCase("2")) {

                    } else if (Sstatus.equalsIgnoreCase("3")) {
                        String Sresponse = object.getString("response");
                        Alert1(getkey("alert_label_title"), Sresponse);
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(TrackRideCancelTrip.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                TrackRidePage.trackyour_ride_class.finish();
                Intent intent = new Intent(TrackRideCancelTrip.this, Navigation_new.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
        mDialog.show();

    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}

