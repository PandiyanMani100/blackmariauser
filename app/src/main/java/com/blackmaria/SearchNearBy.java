package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.FindPlaceSearchAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.EstimateDetailPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by user144 on 6/23/2017.
 */

public class SearchNearBy extends ActivityHockeyApp implements SeekBar.OnSeekBarChangeListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private ImageView back;
    private EditText et_search;
    private ListView listview;
    private RelativeLayout alert_layout;
    private TextView alert_textview;
    private TextView tv_emptyText;


    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    private ServiceRequest mRequest;
    Context context;
    ArrayList<String> itemList_location = new ArrayList<String>();
    ArrayList<String> itemList_location_image = new ArrayList<String>();
    ArrayList<String> itemList_location_distance = new ArrayList<String>();
    ArrayList<String> itemList_location_rating = new ArrayList<String>();
    ArrayList<String> itemList_place_lat = new ArrayList<String>();
    ArrayList<String> itemList_place_lon = new ArrayList<String>();
    ArrayList<String> itemList_placeId = new ArrayList<String>();

    FindPlaceSearchAdapter adapter;
    private boolean isdataAvailable = false;
    private boolean isEstimateAvailable = false;
    final static int REQUEST_LOCATION = 299;
    private String Slatitude = "";
    LanguageDb mhelper;
    private String Slongitude = "";
    private String Sselected_location = "";
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    private double currentLat = 0.0;
    private double currentLon = 0.0;
    private String searchRadius = "5000";
    String searchType = "atm";
    int serachFlag = 0;
    Dialog dialog;
    GPSTracker gps;
    ArrayList<EstimateDetailPojo> ratecard_list = new ArrayList<EstimateDetailPojo>();
    SmoothProgressBar horizantalProgressBar;
    private RefreshReceiver refreshReceiver;

    String placeLat = "";
    String placeLon = "";
    private String Savailable_count = "", Pickup_location = "", Pickup_lat = "", Pickup_lon = "";
    private TextView listcount, seekRange;
    private ArrayList<String> mylist = new ArrayList<>();

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(BookingPage1.this, BookingPage1.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(SearchNearBy.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(SearchNearBy.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.placesearch_find_constrain);
        mhelper = new LanguageDb(this);
        context = getApplicationContext();
        initialize();
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String placeId = itemList_placeId.get(position);

                cd = new ConnectionDetector(SearchNearBy.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + placeId, position);
                } else {
                    alert_layout.setVisibility(View.VISIBLE);
                    alert_textview.setText(getkey("alert_nointernet"));
                }



                /**/

            }
        });


        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchType = et_search.getText().toString().toLowerCase().replace("%", "").replace(" ", "%20").trim();
//                  Postdata();

            }
        });

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(et_search);
                }
                return false;
            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                cd = new ConnectionDetector(SearchNearBy.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }
                    if (!searchType.equalsIgnoreCase("")) {
                        Postdata();
                    }
                }

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(back.getWindowToken(), 0);

                SearchNearBy.this.finish();
                SearchNearBy.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        et_search.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(et_search, 0);
            }
        }, 200);
    }

    private void initialize() {
        gps = new GPSTracker(SearchNearBy.this);
        alert_layout = (RelativeLayout) findViewById(R.id.location_search_alert_layout);
        alert_textview = (TextView) findViewById(R.id.location_search_alert_textView);
        alert_textview.setText(getkey("alert_nointernet"));
        back = findViewById(R.id.location_search_back_layout1);
        listcount = findViewById(R.id.listcount);
        et_search = findViewById(R.id.location_search_editText);
        et_search.setHint(getkey("estimate_price_label_search1"));
        listview = (ListView) findViewById(R.id.location_search_listView);
        horizantalProgressBar = (SmoothProgressBar) findViewById(R.id.google_now);
        tv_emptyText = (TextView) findViewById(R.id.location_search_empty_textview);
        tv_emptyText.setText(getkey("estimate_price_label_place_notAvailable"));

        TextView find_place_txt= (TextView) findViewById(R.id.find_place_txt);
        find_place_txt.setText(getkey("booking_page_findplaces"));
        TextView tv_radius= (TextView) findViewById(R.id.tv_radius);
        tv_radius.setText(getkey("distance"));


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        registerReceiver(refreshReceiver, intentFilter);

        Intent intent = getIntent();
        Savailable_count = intent.getStringExtra("Savailable_count");
        Pickup_location = intent.getStringExtra("Pickup_location");
        Pickup_lat = getIntent().getStringExtra("pickuplat");
        Pickup_lon = getIntent().getStringExtra("pickuplon");

        if(getIntent().hasExtra("list")){
            mylist.clear();
            mylist = getIntent().getStringArrayListExtra("list");

        }

        if (intent.hasExtra("placeType")) {
            searchType = intent.getStringExtra("placeType");
        }
        if (intent.hasExtra("placeTypeflag")) {
            serachFlag = intent.getIntExtra("placeTypeflag", 0);
        }
        if (intent.hasExtra("searchRange")) {
            searchRadius = intent.getStringExtra("searchRange");
        }
        if (serachFlag == 0) {
            searchType = "cafe||atm||shopping||spa||food||hotel";
        }
        if (serachFlag == 10) {
            searchType = "";
        }
        if (gps.canGetLocation() && gps.isgpsenabled()) {
            currentLat = gps.getLatitude();
            currentLon = gps.getLongitude();

        } else {
            enableGpsService();
        }
        if (!searchType.equalsIgnoreCase("")) {
            Postdata();
        }

        String radiuslim = "";
        if(searchRadius.endsWith(".0"))
        {
            radiuslim=searchRadius.replace(".0","");
        }


        seekRange = findViewById(R.id.radius_range);
        SeekBar seekBar1 = findViewById(R.id.seekBarDistance);
        seekBar1.setMax(10000);
        if(!radiuslim.equals(""))
        {
            seekBar1.setProgress(Integer.parseInt(radiuslim));
        }
        else
        {
            seekBar1.setProgress(5000);
        }
        seekBar1.setOnSeekBarChangeListener(this);
        seekRange.setText((seekBar1.getProgress() / 1000) + getkey("km"));
//        searchRadius = seekBar1.getProgress() + "";

    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        seekRange.setText((progress / 1000) + getkey("km"));
        searchRadius = String.valueOf((progress / 1000) * 1000.0);

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (!searchType.equalsIgnoreCase("")) {
            Postdata();
        }
    }


    private void Postdata() {
        cd = new ConnectionDetector(SearchNearBy.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            if (mRequest != null) {
                mRequest.cancelRequest();
            }
            String type = "";

            String baseUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
            String latLAn = "location=" + currentLat + "," + currentLon;
            String radius = "&radius=" + searchRadius;
            if (!searchType.equalsIgnoreCase("")) {
                type = "&types=" + searchType;
            } else {
                type = "&types=" + "atm";
            }
            String sensor = "&sensor=true";
            String key = "&key=AIzaSyCTUEtptTc_iVc8EVhLfmL1qDHYaEo6iHQ";
            //String input = "&input=";
            String findNearestPlace = (baseUrl + latLAn + radius + type + sensor + key).trim();// "https://maps.googleapis.com/maps/api/place/autocomplete/json?" +"types=geocode||establishment&key=AIzaSyCTUEtptTc_iVc8EVhLfmL1qDHYaEo6iHQ&input=";
            CitySearchRequest(findNearestPlace);
            System.out.println("======================findNearestPlace==============" + findNearestPlace);
        } else {
            alert_layout.setVisibility(View.VISIBLE);
            alert_textview.setText(getkey("alert_nointernet"));
        }
    }

    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(SearchNearBy.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SearchNearBy.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(SearchNearBy.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    //-------------------Search Place Request----------------
    private void CitySearchRequest(String Url) {
        horizantalProgressBar.setVisibility(View.VISIBLE);
        System.out.println("--------------nearBySearch  url-------------------" + Url);

        mRequest = new ServiceRequest(SearchNearBy.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------nearBySearch   reponse-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String placeName = "-NA-";
                        String vicinity = "-NA-";
                        String latitude = "";
                        String longitude = "";
                        String reference = "";
                        String Icon = "";
                        String rating = "";
                        String placeid = "";
                        status = object.getString("status");
                        if (status.equalsIgnoreCase("OK")) {
                            JSONArray jPlaces = object.getJSONArray("results");
                            itemList_location.clear();
                            itemList_location_image.clear();
                            itemList_location_distance.clear();
                            itemList_location_rating.clear();
                            itemList_placeId.clear();
                            for (int i = 0; i < jPlaces.length(); i++) {
                                try {
                                    /** Call getPlace with place JSON object to parse the place */
                                    JSONObject place = jPlaces.getJSONObject(i);
                                    if (!place.isNull("name")) {
                                        placeName = place.getString("name");
                                    }
                                    // Extracting Place Vicinity, if available
                                    if (!place.isNull("vicinity")) {
                                        vicinity = place.getString("vicinity");
                                    }



                                    System.out.println("pla--->"+place.getJSONArray("types").toString());

                                    latitude = place.getJSONObject("geometry").getJSONObject("location").getString("lat");
                                    longitude = place.getJSONObject("geometry").getJSONObject("location").getString("lng");

                                    reference = place.getString("reference");
                                    Icon = place.getString("icon");

                                    float distance = findDistance(currentLat, currentLon, Double.parseDouble(latitude), Double.parseDouble(longitude));
                                    String Sdistance = String.format("%.2f", distance);

                                    if (serachFlag == 0) {

                                        if (!place.isNull("rating")) {
                                            rating = place.getString("rating");
                                            itemList_location_rating.add(rating);
                                        } else {
                                            itemList_location_rating.add("0");
                                        }

                                        if (!place.isNull("place_id")) {
                                            placeid = place.getString("place_id");
                                            itemList_placeId.add(placeid);
                                        }
                                        itemList_location.add(placeName + "," + vicinity);
                                        itemList_location_image.add(Icon);
                                        itemList_location_distance.add(Sdistance);
                                        isdataAvailable = true;
                                    }
                                    else
                                    {
                                        if(place.getJSONArray("types").toString().contains(searchType))
                                        {
                                            if (!place.isNull("rating")) {
                                                rating = place.getString("rating");
                                                itemList_location_rating.add(rating);
                                            } else {
                                                itemList_location_rating.add("0");
                                            }

                                            if (!place.isNull("place_id")) {
                                                placeid = place.getString("place_id");
                                                itemList_placeId.add(placeid);
                                            }
                                            itemList_location.add(placeName + "," + vicinity);
                                            itemList_location_image.add(Icon);
                                            itemList_location_distance.add(Sdistance);
                                            isdataAvailable = true;
                                        }
                                    }




                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }


                        } else {

                            itemList_location.clear();
                            isdataAvailable = false;
                        }
                        if (isdataAvailable) {
                            tv_emptyText.setVisibility(View.GONE);
                        } else {
                            tv_emptyText.setVisibility(View.VISIBLE);
                        }

                        horizantalProgressBar.setVisibility(View.GONE);
                        alert_layout.setVisibility(View.GONE);
                        if(searchType.contains("||"))
                        {
                            searchType = getkey("places");
                        }
                        listcount.setText(getkey("we_foud") + " " + itemList_location.size() + " " + searchType + "" + "!");
                        adapter = new FindPlaceSearchAdapter(SearchNearBy.this, itemList_location, itemList_location_image, itemList_location_distance);
                        listview.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                horizantalProgressBar.setVisibility(View.GONE);

                alert_layout.setVisibility(View.GONE);

                // close keyboard
                CloseKeyboard(et_search);
            }
        });
    }

    private float findDistance(double lat1, double lon1, double lat2, double lon2) {
        Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(lon1);
        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lon2);
        float distanceInMeters = loc1.distanceTo(loc2);
        distanceInMeters = (distanceInMeters / 1000);
        return distanceInMeters;
    }

    //-------------------Get Latitude and Longitude from Address(Place ID) Request----------------
    private void LatLongRequest(String Url, final int position) {
        horizantalProgressBar.setVisibility(View.VISIBLE);
        System.out.println("--------------LatLong url-------------------" + Url);
        mRequest = new ServiceRequest(SearchNearBy.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------LatLong  reponse-------------------" + response);
                String status = "", phoneNumber = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONObject place_object = object.getJSONObject("result");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_object.length() > 0) {
                                if (place_object.has("formatted_phone_number")) {
                                    phoneNumber = place_object.getString("formatted_phone_number");
                                }
                                JSONObject geometry_object = place_object.getJSONObject("geometry");
                                if (geometry_object.length() > 0) {
                                    JSONObject location_object = geometry_object.getJSONObject("location");
                                    itemList_place_lat.clear();
                                    itemList_place_lon.clear();
                                    if (location_object.length() > 0) {
                                        Slatitude = location_object.getString("lat");
                                        Slongitude = location_object.getString("lng");
                                        placeLat = Slatitude;
                                        placeLon = Slongitude;
                                        isdataAvailable = true;
                                    } else {
                                        isdataAvailable = false;
                                    }
                                } else {
                                    isdataAvailable = false;
                                }
                            } else {
                                isdataAvailable = false;
                            }
                        } else {
                            isdataAvailable = false;
                        }


                        if (status.equalsIgnoreCase("OK")) {
                            if (isdataAvailable) {
                                String placeName = itemList_location.get(position);
                                String placeImage = itemList_location_image.get(position);
                                String placeDistamnce = itemList_location_distance.get(position);
                                String placerating = itemList_location_rating.get(position);
                                Sselected_location = itemList_location.get(position);
                                Intent detailIntent = new Intent(getApplicationContext(), SearchNearBytoBooking.class);
                                detailIntent.putExtra("Savailable_count", Savailable_count);
                                detailIntent.putExtra("Pickup_location", Pickup_location);
                                detailIntent.putExtra("pickuplat", Pickup_lat);
                                detailIntent.putExtra("pickuplon", Pickup_lon);
                                detailIntent.putExtra("placeName", placeName);
                                detailIntent.putExtra("placeImage", placeImage);
                                detailIntent.putExtra("placeDistamnce", placeDistamnce);
                                detailIntent.putExtra("placerating", placerating);
                                detailIntent.putExtra("currentLat", currentLat + "");
                                detailIntent.putExtra("currentLon", currentLon + "");
                                detailIntent.putExtra("placeLat", Slatitude);
                                detailIntent.putExtra("placeLon", Slongitude);
                                detailIntent.putExtra("placeNumber", phoneNumber);
                                detailIntent.putExtra("placeType", searchType);
                                detailIntent.putStringArrayListExtra("list", mylist);
                                startActivity(detailIntent);
                                finish();
                                overridePendingTransition(R.anim.enter, R.anim.exit);
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                horizantalProgressBar.setVisibility(View.GONE);
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            // close keyboard
            InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            mgr.hideSoftInputFromWindow(back.getWindowToken(), 0);

            SearchNearBy.this.finish();
            SearchNearBy.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            return true;
        }
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.find_button:
                searchType = et_search.getText().toString().toLowerCase().replace("%", "").replace(" ", "%20").trim();

                if (!searchType.equalsIgnoreCase("")) {
                    Postdata();
                } else {
                    Alert(getkey("alert"), getkey("alert_enter_label"));
                }
                break;

        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}

