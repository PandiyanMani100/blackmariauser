package com.blackmaria;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.blackmaria.adapter.SelectCarCategoryAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.RegisterDriverLocationCatCarPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CustomLinearLayoutManager;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by GANESH on 27-09-2017.
 */

public class RegistrationSelectCategory extends ActivityHockeyApp implements View.OnClickListener, SelectCarCategoryAdapter.RecyclerItemClickInterface {

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private Dialog dialog;
    private CustomLinearLayoutManager mLayoutManager;
    private SelectCarCategoryAdapter adapter;

    private String sDriverID = "";
    private ArrayList<Integer> dummyList;
    private int currentPosition = 0;

    private RecyclerView Rv_carCategory;
    private EditText Edt_locationSearch;
    private RelativeLayout RL_exit;
    private ImageView Iv_prev, Iv_next;
    private boolean isInternetPresent = false;
    private String locationLat = "", locationLon = "", driverLocationPlaceId = "";
    private ArrayList<RegisterDriverLocationCatCarPojo> listCarCatDetails;
    private boolean isDataAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_select_category);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(RegistrationSelectCategory.this);
        cd = new ConnectionDetector(RegistrationSelectCategory.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_USERID);

        listCarCatDetails = new ArrayList<RegisterDriverLocationCatCarPojo>();
        RL_exit = (RelativeLayout) findViewById(R.id.RL_exit);
        Rv_carCategory = (RecyclerView) findViewById(R.id.Rv_car_category);
        Iv_prev = (ImageView) findViewById(R.id.img_prev);
        Iv_next = (ImageView) findViewById(R.id.img_next);

        mLayoutManager = new CustomLinearLayoutManager(RegistrationSelectCategory.this, LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager.setScrollEnabled(true);
        Rv_carCategory.setLayoutManager(mLayoutManager);
        Rv_carCategory.setItemAnimator(new DefaultItemAnimator());


        Intent locInt = getIntent();
        if (locInt != null) {
            locationLat = locInt.getStringExtra("lat");
            locationLon = locInt.getStringExtra("lon");
            driverLocationPlaceId = locInt.getStringExtra("placeId");
        }


        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            LocationBasedCarCategory(Iconstant.DriverReg_Locationwise_Carcategory_URL);

        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
        RL_exit.setOnClickListener(this);
        Iv_prev.setOnClickListener(this);
        Iv_next.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

        if (view == RL_exit) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == Iv_prev) {

            currentPosition = mLayoutManager.findFirstVisibleItemPosition();
            if (currentPosition > 0) {
                currentPosition = currentPosition - 1;
                mLayoutManager.scrollToPositionWithOffset(currentPosition, 0);
            }

        } else if (view == Iv_next) {

            currentPosition = mLayoutManager.findFirstVisibleItemPosition();
            if (currentPosition != listCarCatDetails.size() - 1) {
                currentPosition = currentPosition + 1;
                mLayoutManager.scrollToPositionWithOffset(currentPosition, 0);
            }

        }

    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(RegistrationSelectCategory.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.ok_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    private void AlertConfirmation(String title, String message,final int pos) {/*

        final Dialog confirmDialog = new Dialog(RegistrationSelectCategory.this);
        confirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmDialog.setContentView(R.layout.alert_yes_or_no);
        confirmDialog.setCanceledOnTouchOutside(false);
        confirmDialog.setCancelable(false);
        confirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final TextView Tv_title = (TextView) confirmDialog.findViewById(R.id.txt_label_title);
        final TextView Tv_message = (TextView) confirmDialog.findViewById(R.id.txt_label_message);
        final Button Btn_yes = (Button) confirmDialog.findViewById(R.id.btn_yes);
        final Button Btn_no = (Button) confirmDialog.findViewById(R.id.btn_no);

        Tv_title.setText(title);
        Tv_message.setText(message);
        Btn_yes.setText(getResources().getString(R.string.drawer_lable_logout_yes));
        Btn_no.setText(getResources().getString(R.string.drawer_lable_logout_no));

        Btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationSelectCategory.this, RegistrationLoginDetails.class);
                intent.putExtra("vehicleId",listCarCatDetails.get(pos).getCatId());
                intent.putExtra("driverLocationPlaceId",driverLocationPlaceId);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

                confirmDialog.dismiss();

            }
        });

        Btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog.dismiss();
            }
        });

        confirmDialog.show();*/

    }


    private void LocationBasedCarCategory(String Url) {

        dialog = new Dialog(RegistrationSelectCategory.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("lat", locationLat);
        jsonParams.put("lon", locationLon);

        System.out.println("--------------LocationBasedCarCategory Url-------------------" + Url);
        System.out.println("--------------LocationBasedCarCategory jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationSelectCategory.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------LocationBasedCarCategory Response-------------------" + response);
                String status = "", message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        message = object.getString("message");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            driverLocationPlaceId = jsonObject.getString("location_id");
                            if (jsonObject.length() > 0) {
                                Object intervention = jsonObject.get("category");
                                if (intervention instanceof JSONArray) {
                                    JSONArray categoryArray = jsonObject.getJSONArray("category");
                                    if (categoryArray.length() > 0) {
                                        listCarCatDetails = new ArrayList<RegisterDriverLocationCatCarPojo>();
                                        for (int i = 0; i < categoryArray.length(); i++) {
                                            JSONObject locCtaListObj = categoryArray.getJSONObject(i);
                                            String id = locCtaListObj.getString("id");
                                            String name = locCtaListObj.getString("name");
                                            String no_of_seats = locCtaListObj.getString("no_of_seats");
                                            String offer_type = locCtaListObj.getString("offer_type");
                                            String vehicle_type = locCtaListObj.getString("vehicle_type");
                                            String description = locCtaListObj.getString("description");
                                            String icon_normal = locCtaListObj.getString("icon_normal");
                                            listCarCatDetails.add(new RegisterDriverLocationCatCarPojo(id, name, no_of_seats, offer_type, vehicle_type, description, icon_normal,""));
                                        }
                                        isDataAvailable = true;
                                    } else {
                                        isDataAvailable = false;

                                    }
                                }

                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    if (isDataAvailable) {
                        adapter = new SelectCarCategoryAdapter(RegistrationSelectCategory.this, listCarCatDetails, RegistrationSelectCategory.this);
                        Rv_carCategory.setAdapter(adapter);

                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @Override
    public void onSelectClick(int position) {
//        Toast.makeText(RegistrationSelectCategory.this, "Clicked Position = " + position, Toast.LENGTH_SHORT).show();
if(!listCarCatDetails.get(position).getCatId().equalsIgnoreCase("")) {
    AlertConfirmation(getResources().getString(R.string.confirmation), getResources().getString(R.string.signup_for) +getResources().getString(R.string.categoryprocess), position);
}

    }
}
