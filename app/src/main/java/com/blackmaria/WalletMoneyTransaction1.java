package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.WalletMoneyTransactionAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.WalletMoneyTransactionPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.CustomTextView1;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;

public class WalletMoneyTransaction1 extends ActivityHockeyApp {
    private ImageView back, walletback;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private static Context context;
    private SessionManager session;
    private String UserID = "";
    LanguageDb mhelper;
    TextView creditDebitStatement,Tv_wallet_type;
    private ServiceRequest mRequest;
    Dialog dialog;
    private boolean isTransactionAvailable = false;
    ArrayList<WalletMoneyTransactionPojo> itemlist_all;
    ArrayList<WalletMoneyTransactionPojo> itemlist_credit;
    ArrayList<WalletMoneyTransactionPojo> itemlist_debit;
    WalletMoneyTransactionAdapter adapter;



    private ListView listview;
    private CustomTextView1 empty_text;
    private String Stype = "";
    String mnth="";
    private RefreshReceiver refreshReceiver;

    String perPage = "5", ScurrentYear = "", ScurrentMonth;
    private int currentPage = 1;
    ImageView Iv_previous, Iv_next, Iv_prevMonth, Iv_nextMonth,image_creditDebit;
    int currentYear, currentMonth,currentMonthStatic,ScurrentYearStatic;
    CustomTextView ewalletAmount,creditDebitMonth,tv_select_mnth,tv_select_mnth1,creditDebitText;
private LinearLayout li_creditdebit;
    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                WalletMoneyPage1.WalletMoneyPage1_class.finish();
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        WalletMoneyPage1.WalletMoneyPage1_class.finish();
                        Intent intent1 = new Intent(WalletMoneyTransaction1.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        WalletMoneyPage1.WalletMoneyPage1_class.finish();
                        Intent intent1 = new Intent(WalletMoneyTransaction1.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        WalletMoneyPage1.WalletMoneyPage1_class.finish();
                        Intent intent1 = new Intent(WalletMoneyTransaction1.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_money_transaction1);
        mhelper = new LanguageDb(this);
        context = getApplicationContext();
        try {
            initialize();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WalletMoneyTransaction1.this, Navigation_new.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        walletback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        Iv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Iv_previous.setVisibility(View.VISIBLE);
                currentPage = currentPage + 1;
                postData();
            }
        });

        Iv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                currentPage = currentPage - 1;
                postData();

            }
        });
        Iv_nextMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                currentMonth = currentMonth+1;

                System.out.println("=============muruga NextMonth1=======" + currentMonth);
                if (currentMonth >12) {
                    currentMonth =1;
                    System.out.println("=============muruga NextMonth2=======" + currentMonth);
                    currentYear = currentYear+1;
                    System.out.println("=============muruga NextYear=======" + currentYear);
                }
                ScurrentYear = currentYear + "";
                ScurrentMonth = currentMonth + "";
                currentPage = /*currentPage +*/ 1;
                try {

                     mnth=formatMonth(ScurrentMonth,ScurrentYear);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                postData();
            }
        });

        Iv_prevMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentMonth = currentMonth - 1;
                System.out.println("=============muruga PrevMonth1=======" + currentMonth);
                if (currentMonth == 0) {
                    currentMonth = 12;
                    System.out.println("=============muruga PrevMonth2=======" + currentMonth);
                    currentYear = currentYear - 1;
                    System.out.println("=============muruga PrevYear=======" + currentYear);
                }
                ScurrentYear = currentYear + "";
                ScurrentMonth = currentMonth + "";
                try {
                     mnth=formatMonth(ScurrentMonth,ScurrentYear);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                currentPage = 1;
                postData();

            }
        });

    }
    private void initialize() throws ParseException {
        session = new SessionManager(WalletMoneyTransaction1.this);
        cd = new ConnectionDetector(WalletMoneyTransaction1.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemlist_all = new ArrayList<WalletMoneyTransactionPojo>();
        Intent intent = getIntent();
        Stype = intent.getStringExtra("type");

        back = (ImageView) findViewById(R.id.wallet_money_transaction_header_back_layout);
        walletback = (ImageView) findViewById(R.id.walletback);
        listview = (ListView) findViewById(R.id.wallet_money_transaction_listview);
        empty_text = (CustomTextView1) findViewById(R.id.wallet_money_transaction_listview_empty_text);
        empty_text.setText(mhelper.getvalueforkey("wallet_money_lable_no_transaction_yet"));
        creditDebitStatement = (TextView) findViewById(R.id.credit_debit_statement);
        Iv_next = (ImageView) findViewById(R.id.next_page);
        Iv_previous = (ImageView) findViewById(R.id.prev_page);
        ewalletAmount= (CustomTextView) findViewById(R.id.ewallet_amount_text);
        creditDebitMonth=(CustomTextView) findViewById(R.id.credit_debit_month);
        Iv_nextMonth = (ImageView) findViewById(R.id.next_month);
        Iv_prevMonth = (ImageView) findViewById(R.id.prev_month);
        tv_select_mnth=(CustomTextView) findViewById(R.id.tv_select_mnth);
        tv_select_mnth1=(CustomTextView) findViewById(R.id.tv_select_mnth1);
        image_creditDebit=(ImageView) findViewById(R.id.folderimage);
        li_creditdebit=(LinearLayout)findViewById(R.id.li_creditdebit);
        Tv_wallet_type = (TextView) findViewById(R.id.textView2);
        if (Stype.equalsIgnoreCase("debit")) {
            Tv_wallet_type.setText(mhelper.getvalueforkey("wallet_moneyout"));
            creditDebitStatement.setText(mhelper.getvalueforkey("wallet_debit_statement"));
            image_creditDebit.setBackgroundDrawable(getResources().getDrawable(R.drawable.folder3));
          //  creditDebitStatement.setTextColor(getResources().getColor(R.color.yellow_color1));
            li_creditdebit.setBackgroundDrawable(getResources().getDrawable(R.drawable.red_gradient));
            tv_select_mnth1.setText(mhelper.getvalueforkey("totaldebit_thismonth"));
        } else {
            li_creditdebit.setBackgroundDrawable(getResources().getDrawable(R.drawable.find_place_new_bg));
            image_creditDebit.setBackgroundDrawable(getResources().getDrawable(R.drawable.folder2));
            creditDebitStatement.setText(mhelper.getvalueforkey("wallet_credit_statement"));
            tv_select_mnth1.setText(mhelper.getvalueforkey("totalcredit_thismonth"));
            Tv_wallet_type.setText(mhelper.getvalueforkey("wallet_moneyin"));
         //   creditDebitStatement.setTextColor(getResources().getColor(R.color.white_color));
        }


        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        Calendar c = Calendar.getInstance();

        currentMonthStatic= c.get(Calendar.MONTH)+1;
        ScurrentYearStatic=c.get(Calendar.YEAR);

        currentYear = c.get(Calendar.YEAR);
        currentMonth = c.get(Calendar.MONTH)+1;
        ScurrentYear = currentYear + "";
        ScurrentMonth = (currentMonth)+"";
         mnth=formatMonth(ScurrentMonth,ScurrentYear);
        System.out.println("=============muruga currentYear=======" + currentYear);
        System.out.println("=============muruga currentMonth=======" + ScurrentMonth+" "+mnth);
        postData();
        ImageView Rl_drawer = (ImageView) findViewById(R.id.indicating_image);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);

    }
    public String formatMonth(String month,String Year) throws ParseException {
        SimpleDateFormat monthParse = new SimpleDateFormat("MM");
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMMM");
        String month1=monthDisplay.format(monthParse.parse(month)).toUpperCase();
        creditDebitMonth.setText(month1+" "+Year);
        tv_select_mnth.setText(/*""+month1+" "+Year*/"");
        return monthDisplay.format(monthParse.parse(month));
    }

    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("type", Stype);
        jsonParams.put("month", ScurrentMonth);
        jsonParams.put("year", ScurrentYear);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
    if (isInternetPresent) {
            postRequest_walletMoney(Iconstant.wallet_money_transaction_url, jsonParams);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WalletMoneyTransaction1.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {
        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }

    //-----------------------wallet Money Post Request-----------------
    private void postRequest_walletMoney(String Url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(WalletMoneyTransaction1.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("-------------walletMoney Transaction Url----------------" + Url);


        mRequest = new ServiceRequest(WalletMoneyTransaction1.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------walletMoney Transaction Response----------------" + response);

                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "",ScurrencySymbol="",Str_total_amount="",Str_total_transaction="";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(response_object.getString("currency"));
                        Str_total_amount = response_object.getString("total_amount");
                        Str_total_transaction = response_object.getString("total_transaction");
                        str_NextPage = response_object.getString("next_page");
                        if(str_NextPage.equalsIgnoreCase("")){
                            Iv_next.setVisibility(View.GONE);
                        }else{
                            Iv_next.setVisibility(View.VISIBLE);
                        }
                        if (response_object.length() > 0) {
                            Currency currencycode = Currency.getInstance(getLocale(response_object.getString("currency")));

                            Object check_trans_object = response_object.get("trans");
                            itemlist_all.clear();
                            if (check_trans_object instanceof JSONArray) {
                                JSONArray trans_array = response_object.getJSONArray("trans");
                                if (trans_array.length() > 0) {
                                    for (int i = 0; i < trans_array.length(); i++) {
                                        JSONObject trans_object = trans_array.getJSONObject(i);

                                        WalletMoneyTransactionPojo pojo = new WalletMoneyTransactionPojo();
                                        pojo.setTrans_type(trans_object.getString("type"));
                                        pojo.setTrans_amount(trans_object.getString("trans_amount"));
                                        pojo.setTitle(trans_object.getString("title"));
                                        pojo.setTrans_date(trans_object.getString("trans_date"));
                                        pojo.setStrDate(trans_object.getString("date"));
                                        pojo.setBalance_amount(trans_object.getString("balance_amount"));
                                        pojo.setCurrencySymbol(currencycode.getSymbol());
                                        pojo.setTransactionTime(trans_object.getString("trans_time"));
                                        pojo.setImage(trans_object.getString("image"));
                                        itemlist_all.add(pojo);

                                    }
                                    isTransactionAvailable = true;
                                } else {
                                    isTransactionAvailable = false;
                                }
                            } else {
                                isTransactionAvailable = false;
                            }
                            currentPage = Integer.parseInt(response_object.getString("current_page"));
                            str_NextPage = response_object.getString("next_page");
                            perPage = response_object.getString("perPage");
                        }
                    }
                    if(Integer.parseInt(ScurrentMonth)>=currentMonthStatic && Integer.parseInt(ScurrentYear)>=ScurrentYearStatic ){
                        Iv_nextMonth.setVisibility(View.GONE);
                    }else{
                        Iv_nextMonth.setVisibility(View.VISIBLE);
                    }
                    if (currentPage == 1) {
                        Iv_previous.setVisibility(View.GONE);
                    } else {
                        Iv_previous.setVisibility(View.VISIBLE);
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (isTransactionAvailable) {
                            ewalletAmount.setText(getResources().getString(R.string.totals)+ScurrencySymbol+" "+Str_total_amount);
                            empty_text.setVisibility(View.GONE);
                          //  Iv_next.setVisibility(View.VISIBLE);
                            adapter = new WalletMoneyTransactionAdapter(WalletMoneyTransaction1.this, itemlist_all);
                            listview.setAdapter(adapter);
                        } else {
                            empty_text.setVisibility(View.VISIBLE);
                            listview.setEmptyView(empty_text);
                         //   Iv_next.setVisibility(View.GONE);
                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }


            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

    }


    /*//-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //do nothing
            return true;
        }
        return false;
    }*/



    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
