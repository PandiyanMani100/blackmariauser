package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.blackmaria.adapter.RideListAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.RideListPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user14 on 1/5/2017.
 */

public class ReviewPage extends ActivityHockeyApp {


    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private SessionManager session;
    private String UserID = "";

    private RideListAdapter rideListAdapter;
    private ArrayList<RideListPojo> RideList_itemList;

    private RefreshReceiver refreshReceiver;
    private ListView Lv_ride;
    private ImageView Iv_back;
    private CustomTextView Tv_no_rides,Tv_my_review,Tv_review_for_me;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(MileageDetails.this, Fragment_HomePage.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(ReviewPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(ReviewPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reviwe_page);

        initialize();
        Iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ReviewPage.this, Navigation_new.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit, R.anim.enter);
                finish();

            }
        });
    }

    private void initialize() {
        cd = new ConnectionDetector(ReviewPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(ReviewPage.this);
        RideList_itemList = new ArrayList<RideListPojo>();


        Iv_back = (ImageView) findViewById(R.id.review_page_back_imageview);
        Lv_ride = (ListView) findViewById(R.id.review_page_trip_list);
        Tv_no_rides = (CustomTextView) findViewById(R.id.review_page_no_data_textview);
        Tv_my_review = (CustomTextView) findViewById(R.id.review_page_myreview_textview);
        Tv_review_for_me = (CustomTextView) findViewById(R.id.review_page_review_for_me_textview);



// -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

//        postData();

    }


    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(ReviewPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }


    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }

}
