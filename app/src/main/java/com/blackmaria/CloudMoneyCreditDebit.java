package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.CloudmoneyTransactionAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.WalletMoneyTransactionPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.CustomTextView1;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by user144 and user144 on 5/18/2017.
 */

public class CloudMoneyCreditDebit extends ActivityHockeyApp {
    private ImageView back, walletback;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private static Context context;
    private SessionManager session;
    private String UserID = "";
  //  CustomTextView creditDebitStatement;
    private ServiceRequest mRequest;
    Dialog dialog;
    private boolean isTransactionAvailable = false;
    ArrayList<WalletMoneyTransactionPojo> itemlist_all;
    ArrayList<WalletMoneyTransactionPojo> itemlist_credit;
    ArrayList<WalletMoneyTransactionPojo> itemlist_debit;
    CloudmoneyTransactionAdapter adapter;



    private ListView listview;
     private CustomTextView1 empty_text;
    private String Stype = "";
    String mnth="";
    private RefreshReceiver refreshReceiver;
    int currentYear, currentMonth,currentMonthStatic,ScurrentYearStatic;
    String perPage = "5", ScurrentYear = "", ScurrentMonth;
    private int currentPage = 1;
    ImageView Iv_previous, Iv_next,Iv_prevMonth, Iv_nextMonth;
    CustomTextView ewalletAmount,creditDebitMonth,totalTrips;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                CloudMoneyHomePage.cloudmoneyhomepage.finish();
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        CloudMoneyHomePage.cloudmoneyhomepage.finish();
                        Intent intent1 = new Intent(CloudMoneyCreditDebit.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        CloudMoneyHomePage.cloudmoneyhomepage.finish();
                        Intent intent1 = new Intent(CloudMoneyCreditDebit.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        CloudMoneyHomePage.cloudmoneyhomepage.finish();
                        Intent intent1 = new Intent(CloudMoneyCreditDebit.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cloudmoneycreaditdebit);
        context = getApplicationContext();
        try {
            initialize();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent=new Intent(CloudMoneyCreditDebit.this,Navigation_new.class);
                startActivity(homeIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        walletback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent=new Intent(CloudMoneyCreditDebit.this,CloudMoneyHomePage.class);
                startActivity(homeIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
//New code
        Iv_nextMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                itemlist_all.clear();
                currentMonth = currentMonth+1;

                System.out.println("=============muruga NextMonth1=======" + currentMonth);
                if (currentMonth >12) {
                    currentMonth =1;
                    System.out.println("=============muruga NextMonth2=======" + currentMonth);
                    currentYear = currentYear+1;
                    System.out.println("=============muruga NextYear=======" + currentYear);
                }
                ScurrentYear = currentYear + "";
                ScurrentMonth = currentMonth + "";
                currentPage = /*currentPage +*/ 1;
                try {

                    mnth=formatMonth(ScurrentMonth,ScurrentYear);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                postData();
            }
        });

        Iv_prevMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                itemlist_all.clear();
                currentMonth = currentMonth - 1;
                System.out.println("=============muruga PrevMonth1=======" + currentMonth);
                if (currentMonth == 0) {
                    currentMonth = 12;
                    System.out.println("=============muruga PrevMonth2=======" + currentMonth);
                    currentYear = currentYear - 1;
                    System.out.println("=============muruga PrevYear=======" + currentYear);
                }
                ScurrentYear = currentYear + "";
                ScurrentMonth = currentMonth + "";
                try {
                    mnth=formatMonth(ScurrentMonth,ScurrentYear);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                currentPage = 1;
                postData();

            }
        });


        Iv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Iv_previous.setVisibility(View.VISIBLE);
                currentPage = currentPage + 1;
                postData();
            }
        });

        Iv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                currentPage = currentPage - 1;
                postData();

            }
        });

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

             String credittype=itemlist_all.get(position).getCredittype();
             if(!itemlist_all.get(position).getRefereRideId().equalsIgnoreCase(""))
                if(!credittype.equalsIgnoreCase("")){
                    Intent intent = new Intent(CloudMoneyCreditDebit.this, RideDetailPage.class);
                    intent.putExtra("ride_id", itemlist_all.get(position).getRefereRideId());
                    intent.putExtra("user_id", itemlist_all.get(position).getReferalUserId());
                    intent.putExtra("class", "CloudMoneyCreditDebit");

                    intent.putExtra("hidedetails", "yes");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });
    }
    private void initialize() throws ParseException {
        session = new SessionManager(CloudMoneyCreditDebit.this);
        cd = new ConnectionDetector(CloudMoneyCreditDebit.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemlist_all = new ArrayList<WalletMoneyTransactionPojo>();
        Intent intent = getIntent();
        Stype = intent.getStringExtra("type");

        back = (ImageView) findViewById(R.id.wallet_money_transaction_header_back_layout);
        walletback = (ImageView) findViewById(R.id.walletback);
        listview = (ListView) findViewById(R.id.wallet_money_transaction_listview);
        empty_text = (CustomTextView1) findViewById(R.id.wallet_money_transaction_listview_empty_text);
      //  creditDebitStatement = (CustomTextView) findViewById(R.id.credit_debit_statement);
        Iv_next = (ImageView) findViewById(R.id.next_page);
        Iv_previous = (ImageView) findViewById(R.id.prev_page);
        ewalletAmount= (CustomTextView) findViewById(R.id.ewallet_amount_text);
        creditDebitMonth=(CustomTextView) findViewById(R.id.credit_debit_month);
        totalTrips=(CustomTextView) findViewById(R.id.totaltripsvalue);
        Iv_nextMonth = (ImageView) findViewById(R.id.next_month);
        Iv_prevMonth = (ImageView) findViewById(R.id.prev_month);

        /*if (Stype.equalsIgnoreCase("debit")) {
            creditDebitStatement.setText(getResources().getString(R.string.wallet_debit_statement));
            creditDebitStatement.setTextColor(getResources().getColor(R.color.yellow_color1));
        } else {
            creditDebitStatement.setText(getResources().getString(R.string.wallet_credit_statement));
            creditDebitStatement.setTextColor(getResources().getColor(R.color.white_color));
        }*/
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

//new code
        Calendar c = Calendar.getInstance();

        currentMonthStatic= c.get(Calendar.MONTH)+1;
        ScurrentYearStatic=c.get(Calendar.YEAR);

        currentYear = c.get(Calendar.YEAR);
        currentMonth = c.get(Calendar.MONTH)+1;
        ScurrentYear = currentYear + "";
        ScurrentMonth = (currentMonth)+"";
        mnth=formatMonth(ScurrentMonth,ScurrentYear);
        System.out.println("=============muruga currentYear=======" + currentYear);
        System.out.println("=============muruga currentMonth=======" + ScurrentMonth+" "+mnth);


        postData();

    }
    public String formatMonth(String month,String Year) throws ParseException {
        SimpleDateFormat monthParse = new SimpleDateFormat("MM");
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMMM");
        String month1=monthDisplay.format(monthParse.parse(month)).toUpperCase();
        creditDebitMonth.setText(month1+" "+Year);
        return monthDisplay.format(monthParse.parse(month));
    }

    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        //jsonParams.put("mode", Stype);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        jsonParams.put("month", ScurrentMonth);
        jsonParams.put("year", ScurrentYear);
        if (isInternetPresent) {
            postRequest_CloudMoney(Iconstant.Earnings_list_url, jsonParams);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudMoneyCreditDebit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {
        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }

    //-----------------------wallet Money Post Request-----------------
    private void postRequest_CloudMoney(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(CloudMoneyCreditDebit.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        mRequest = new ServiceRequest(CloudMoneyCreditDebit.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------EarningsPage Response----------------" + response);

                String  Scurrency_code = "", Scurrentbalance = "", Smessage = "", Scurrency = "";
                String Sstatus = "", SreferralCode="",Stotal_earnings="",Stotal_record="",Str_CurrentPage = "", str_NextPage = "", perPage = "",ScurrencySymbol="",Str_total_amount="",Str_total_transaction="";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    JSONObject jobject = object.getJSONObject("response");
                    str_NextPage = jobject.getString("next_page");
                    if(str_NextPage.equalsIgnoreCase("")){
                        Iv_next.setVisibility(View.GONE);
                    }else{
                        Iv_next.setVisibility(View.VISIBLE);
                    }
                    if (jobject.length() > 0) {
                        currentPage = Integer.parseInt(jobject.getString("current_page"));
                        Stotal_record = jobject.getString("total_record");
                        Stotal_earnings = jobject.getString("referral_earning");
                        SreferralCode = jobject.getString("referral_code");
                        Scurrency = jobject.getString("currency");
                        Scurrency_code = CurrencySymbolConverter.getCurrencySymbol(Scurrency);


                        Object check_object = jobject.get("earning_list");
                        if (check_object instanceof JSONArray) {
                            JSONArray earning_list_jsonArray = jobject.getJSONArray("earning_list");
                            if (earning_list_jsonArray.length() > 0) {
                                itemlist_all.clear();
                                for (int i = 0; i < earning_list_jsonArray.length(); i++) {
                                    JSONObject earning_list_obj = earning_list_jsonArray.getJSONObject(i);
                                    WalletMoneyTransactionPojo earningsPojo = new WalletMoneyTransactionPojo();
                                    earningsPojo.setStrDate(earning_list_obj.getString("date"));
                                    earningsPojo.setTitle(earning_list_obj.getString("referrer_name"));
                                    earningsPojo.setTrans_type(earning_list_obj.getString("ref_type"));
                                    earningsPojo.setCredittype(earning_list_obj.getString("type"));
                                    earningsPojo.setTrans_amount(Scurrency_code +" "+ earning_list_obj.getString("earning_amount"));
                                    earningsPojo.setRefereRideId(earning_list_obj.getString("ride_id"));
                                    earningsPojo.setRefereImage(earning_list_obj.getString("image"));
                                    earningsPojo.setReferalUserId(earning_list_obj.getString("user_id"));


                                    itemlist_all.add(earningsPojo);
                                }
                                isTransactionAvailable = true;
                            } else {
                                isTransactionAvailable = false;
                            }
                        } else {
                            isTransactionAvailable = false;
                        }


                    } else {
                        Smessage = jobject.getString("response");
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    totalTrips.setText(Stotal_record);
                    if(Integer.parseInt(ScurrentMonth)>=currentMonthStatic && Integer.parseInt(ScurrentYear)>=ScurrentYearStatic ){
                        Iv_nextMonth.setVisibility(View.GONE);
                    }else{
                        Iv_nextMonth.setVisibility(View.VISIBLE);
                    }

                    if (currentPage == 1) {
                        Iv_previous.setVisibility(View.GONE);
                    } else {
                        Iv_previous.setVisibility(View.VISIBLE);
                    }

                    ewalletAmount.setText(Scurrency + " " + Stotal_earnings);

                    if (isTransactionAvailable) {
                        empty_text.setVisibility(View.GONE);
                       // Iv_next.setVisibility(View.VISIBLE);
                        adapter = new CloudmoneyTransactionAdapter(CloudMoneyCreditDebit.this, itemlist_all);
                        listview.setAdapter(adapter);
                    } else {
                        empty_text.setVisibility(View.VISIBLE);
                        listview.setEmptyView(empty_text);
                       // Iv_next.setVisibility(View.GONE);
                    }


                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

    }


    //-----------------Move Back on pressed phone back button------------------
  /*  @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //do nothing
            return true;
        }
        return false;
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent homeIntent=new Intent(CloudMoneyCreditDebit.this,CloudMoneyHomePage.class);
        startActivity(homeIntent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }
}
