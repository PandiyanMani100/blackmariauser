package com.blackmaria;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;

import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.DriverProfileAdapter;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.pojo.CancelTripPojo;
import com.blackmaria.pojo.DriverProfilePojo;
import com.blackmaria.pojo.MultiDropPojo;
import com.blackmaria.pojo.Trackrideafteraccept_pojo;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;


import com.blackmaria.chatmodule.ChatIntentServiceResult;
import com.blackmaria.chatmodule.DbHelper;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.latlnginterpolation.GMapV2GetRouteDirection;
import com.blackmaria.latlnginterpolation.LatLngInterpolator;
import com.blackmaria.latlnginterpolation.MarkerAnimation;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by user130 on 11/21/2016.
 */

public class TrackRidePage extends FragmentActivityHockeyApp implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, RoutingListener, com.google.android.gms.location.LocationListener {


    Timer timer;
    LanguageDb mhelper;
    String driver_fcm_token = "";
    TimerTask timerTask;
    TextView countofchat;
    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "", UserName = "";
    String savingStr_ride_status = "";
    ImageView currentLocation_image, car_cat_image, timer_image, milege_image;
    public static Activity trackyour_ride_class;
    int ride_accept_only = 0;
    private static final String TAG = "Test>>>";
    String strCallassist = "";
    private RelativeLayout Rl_bottom;
    private TextView Tv_Distance, sos_tv, calldriver, Tv_BottomText;
    Typeface face;
    private Dialog dialog = null;
    private Dialog dialog1;
    private ServiceRequest mRequest;
    String Str_driver_id = "", carTypeImage = "", Str_driver_name = "", Str_driver_email = "", Str_driver_image = "", Str_driver_review = "",
            Str_driver_lat = "", Str_driver_lon = "", Str_est_arrival = "", Str_rider_lat = "", Str_rider_lon = "",
            Str_min_pickup_duration = "", Str_ride_id = "", Str_phone_number = "", Str_vehicle_number = "", Str_vehicle_model = "",
            Str_ride_status = "", Str_mode = "", Str_approx_dist = "", Str_est_cost = "", Str_currency = "", Str_brand_image = "";
    String isContinue = "";

    private String Str_drop_location = "", Str_drop_lon = "", Str_drop_lat = "", Str_pickup_location = "", Str_pickup_lon = "",
            Str_pickup_lat = "";
    private ArrayList<MultiDropPojo> dropList;
    private boolean isDataAvailable = false;

    private GoogleMap googleMap;
    private GPSTracker gps;
    private static Marker curentDriverMarker;
    MapFragment mapFragment;

    private List<Polyline> polylines;
    Polyline polyline = null;
    private ArrayList<LatLng> wayPointList;
    private LatLngBounds.Builder wayPointBuilder;
    private LatLng startWayPoint, endWayPoint, carPoint;
    private String sWayPointStatus = "Confirmed";
    private String sMultipleDropStatus = "0";
    public ArrayList<LatLng> wayLatLng_global;

    LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    final int PERMISSION_REQUEST_CODE = 111;
    final int PERMISSION_REQUEST_CODES = 222;
    private String sSelectedPanicNumber = "";
    AppInfoSessionManager appInfoSessionManager;
    private ArrayList<CancelTripPojo> itemlist_reason;
    private boolean isReasonAvailable = false;

    //  private MaterialDialog completejob_dialog;
    private EditText Et_share_trip_mobileno;
    private RelativeLayout Rl_countryCode;
    private TextView Tv_countryCode;
    private double MyCurrent_lat = 0.0, MyCurrent_long = 0.0;
    private RefreshReceiver refreshReceiver;
    private String sCurLat = "", sCurLon = "";
    private String Srecord_id = "";
    private int sPosition;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 299;
    private LinearLayout Ll_call;
    private LinearLayout Ll_sos, chatfully;
    private TextView Tv_share_car_no, drivername, carNumber;
    private TextView car_brand_name;
    private RelativeLayout Ll_shareLocation;
    private TextView Tv_time;
    private CardView Cv_ontrip;
    private RelativeLayout rl_ontrip;
    TextView onTripTv, ontrip_tv1;
    private String driverCur_Lat = "0.0", driverCur_Lng = "0.0";
    private Typeface tf;
    private String ScountryCode = "";
    LatLng pickupLatLag, dropLatLon;

    CountryPicker picker;
    String CountryCode = "";

    GMapV2GetRouteDirection v2GetRouteDirection;
    Document document;
    Polyline newPolyLine, newPolyline2;
    private String trackingCarimage = "";

    String base64 = "";
    Bitmap bmp;
    private String CategoryImage = "";
    private Handler trackingHandler = new Handler();
    private RelativeLayout track_share_rl;
    private TextView multistoptext;

    //new design
    TextView tv_trackdriver_name, trackdriver_profile_textview;
    RelativeLayout driver_detailpage;
    private Trackrideafteraccept_pojo trackpojo;
    private RelativeLayout rl_viewprofile;
    private ArrayList<DriverProfilePojo> itemList;
    private ImageView iv_driverimg;
    private FrameLayout chatlayout;


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver")) {

                if (intent.getExtras() != null) {
                    Str_ride_id = (String) intent.getExtras().get("RideID");
                    Intent intent1 = new Intent(TrackRidePage.this, TrackArrivePage.class);
                    intent1.putExtra("ride_id", Str_ride_id);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }

            } else if (intent.getAction().equals("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip")) {
                try {

                    car_brand_name.setVisibility(View.GONE);
                    car_cat_image.setImageDrawable(null);
                    car_cat_image.setBackgroundResource(R.drawable.share_my_trip);
                    ride_accept_only = 1;

                    if (intent.getExtras() != null) {
                        String pic_lat = (String) intent.getExtras().get("pickUp_lat");
                        String pic_lng = (String) intent.getExtras().get("pickUp_lng");
                        String dro_lat = (String) intent.getExtras().get("drop_lat");
                        String dro_lng = (String) intent.getExtras().get("drop_lng");
                        String ride_id = (String) intent.getExtras().get("ride_id");

                        if (googleMap != null) {
                            googleMap.clear();
                        }
                        if (mRequest != null) {
                            mRequest.cancelRequest();
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        if (mapHandler != null) {
                            mapHandler.removeCallbacks(mapRunnable);
                        }
                        if (isInternetPresent) {
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("ride_id", ride_id);
                            postRequest_TrackRide(Iconstant.track_your_ride_url, jsonParams);
                        } else {
                            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                        }
                    }
                } catch (Exception e) {
                    System.out.println("-------track begin page------------");
                }
//                }
            } else if (intent.getAction().equals("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip_Return")) {
                try {
                    car_brand_name.setVisibility(View.GONE);
                    car_cat_image.setImageDrawable(null);
                    car_cat_image.setBackgroundResource(R.drawable.icn_sharemytrip);
                    Str_ride_status = "Onride";
                    if (intent.getExtras() != null) {
                        String pic_lat = (String) intent.getExtras().get("pickUp_lat");
                        String pic_lng = (String) intent.getExtras().get("pickUp_lng");
                        String dro_lat = (String) intent.getExtras().get("drop_lat");
                        String dro_lng = (String) intent.getExtras().get("drop_lng");
                        String ride_id = (String) intent.getExtras().get("ride_id");

                        if (googleMap != null) {
                            googleMap.clear();
                        }
                        if (mRequest != null) {
                            mRequest.cancelRequest();
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        if (mapHandler != null) {
                            mapHandler.removeCallbacks(mapRunnable);
                        }
                        if (isInternetPresent) {
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("ride_id", ride_id);
                            postRequest_TrackRide(Iconstant.track_your_ride_url, jsonParams);
                        } else {
                            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                        }
                    }
                } catch (Exception e) {
                    System.out.println("-------track begin page------------");
                }
            } else if (intent.getAction().equals("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Multiplestop")) {
                try {
                    if (intent.getExtras() != null) {
                        String pic_lat = (String) intent.getExtras().get("pickUp_lat");
                        String pic_lng = (String) intent.getExtras().get("pickUp_lng");
                        String dro_lat = (String) intent.getExtras().get("drop_lat");
                        String dro_lng = (String) intent.getExtras().get("drop_lng");
                        Srecord_id = (String) intent.getExtras().get("record_id");

                        System.out.println("-------------prabu-pickUp_lat-----------" + pic_lat + "," + pic_lng);
                        System.out.println("-------------prabu-pickUp_lat-----------" + dro_lat + "," + dro_lng);
                        if (googleMap != null) {
                            googleMap.clear();
                        }
                        if (mRequest != null) {
                            mRequest.cancelRequest();
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        if (mapHandler != null) {
                            mapHandler.removeCallbacks(mapRunnable);
                        }

                        if (isInternetPresent) {
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("ride_id", Str_ride_id);
                            postRequest_TrackRide(Iconstant.track_your_ride_url, jsonParams);
                        } else {
                            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                        }

                    }
                } catch (Exception e) {
                    System.out.println("-------track begin page------------");
                }

            } else if (intent.getAction().equals("com.package.track.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver")) {

            } else if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {

                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(TrackRidePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", Str_ride_id);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(TrackRidePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", Str_ride_id);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }

                }
            }
            if (intent.getExtras() != null && intent.getExtras().containsKey(Iconstant.isContinousRide)) {
                final String lat = (String) intent.getExtras().get("latitude");
                final String lng = (String) intent.getExtras().get("longitude");
                String bearing = (String) intent.getExtras().get("Bearing");
                String ride_id = (String) intent.getExtras().get("ride_id");
                driverCur_Lat = lat;
                driverCur_Lng = lng;
                System.out.println("================= Muruga driver location update==============" + driverCur_Lat + "/" + driverCur_Lng);

                try {
                    v2GetRouteDirection = new GMapV2GetRouteDirection();
                    double lat_decimal = Double.parseDouble(lat);
                    double lng_decimal = Double.parseDouble(lng);
                    float bearing_float = Float.parseFloat(bearing);

                    updateDriverOnMap(lat_decimal, lng_decimal, bearing_float);

                   /* trackingHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("============ muruga Trackingpage handler call==============");
                            new GetRouteTask().execute(lat, lng);

                        }
                    }, 15000);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }


    }

    LatLngInterpolator mLatLngInterpolator;

    private void updateDriverOnMap(double lat_decimal, double lng_decimal, float bearing) {
        trackingCarimage = session.getCarImage();
        System.out.println("==================muruga tracking car image================" + bearing);
        /*try {
            List<LatLng> points = polyline.getPoints();
            points.add(new LatLng(lat_decimal, lng_decimal));
            polyline.setPoints(points);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }*/



     /*   if (base64.equalsIgnoreCase("")) {
            HashMap<String, String> bit_map = session.getVehicleBitmap();
            base64 = bit_map.get(SessionManager.KEY_VEHICLE_BitMap_IMAGE);
            System.out.println("============= vehicle image" + SessionManager.KEY_VEHICLE_BitMap_IMAGE);
            bmp = StringToBitMap(base64);
        }*/


        final LatLng latLng = new LatLng(lat_decimal, lng_decimal);
        if (mLatLngInterpolator == null) {
            mLatLngInterpolator = new LatLngInterpolator.Linear();
        }


        if (curentDriverMarker != null) {
            System.out.println("---------Muruga inside marker != null-------------" + bearing);
            rotateMarker(curentDriverMarker, bearing, googleMap);

            MarkerAnimation.animateMarkerToGB(curentDriverMarker, latLng, mLatLngInterpolator);

            float zoom = googleMap.getCameraPosition().zoom;
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
            CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            googleMap.moveCamera(camUpdate);

        } else {
            mLatLngInterpolator = new LatLngInterpolator.Linear();
            curentDriverMarker = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(bmp))
                    .anchor(0.5f, 0.5f)
                    .rotation(bearing)
                    .flat(true));

            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(17).build();
            CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            googleMap.moveCamera(camUpdate);

        }

    }

    //Method to smooth turn marker
    static public void rotateMarker(final Marker marker, final float toRotation, GoogleMap map) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    private Handler mapHandler = new Handler();

    Runnable mapRunnable = new Runnable() {
        @Override
        public void run() {

            if ((driverCur_Lat != null && driverCur_Lat.length() > 0 && !driverCur_Lat.equals("0.0")) && (driverCur_Lng != null && driverCur_Lng.length() > 0 && !driverCur_Lng.equals("0.0"))) {
                if (mRequest != null) {
                    mRequest.cancelRequest();
                }

                if (("Confirmed".equalsIgnoreCase(Str_ride_status)) || ("Arrived".equalsIgnoreCase(Str_ride_status))) {

                    DeleteRideRequest("http://maps.googleapis.com/maps/api/directions/json?origin=" + driverCur_Lat + "," + driverCur_Lng + "&destination=" + Str_pickup_lat + "," + Str_pickup_lon + "&sensor=false");

                } else {

                    if ("multistop".equalsIgnoreCase(Str_mode)) {
                        for (int i = 0; i < dropList.size(); i++) {
                            System.out.println("-----------dropList size------------" + dropList.size());
                            if (dropList.get(i).getIsEnd().equalsIgnoreCase("0")) {
                                if (i == 0) {
                                    DeleteRideRequest("http://maps.googleapis.com/maps/api/directions/json?origin=" + driverCur_Lat + "," + driverCur_Lng + "&destination=" + dropList.get(0).getPlaceLat() + "," + dropList.get(0).getPlaceLong() + "&sensor=false");
                                    break;
                                } else if (i == dropList.size() - 1) {
                                    DeleteRideRequest("http://maps.googleapis.com/maps/api/directions/json?origin=" + driverCur_Lat + "," + driverCur_Lng + "&destination=" + dropList.get(i).getPlaceLat() + "," + dropList.get(i).getPlaceLong() + "&sensor=false");
                                    break;
                                } else {
                                    DeleteRideRequest("http://maps.googleapis.com/maps/api/directions/json?origin=" + driverCur_Lat + "," + driverCur_Lng + "&destination=" + dropList.get(i).getPlaceLat() + "," + dropList.get(i).getPlaceLong() + "&sensor=false");
                                    break;
                                }
                            } else if (i == dropList.size() - 1) {
                                if (dropList.get(i).getIsEnd().equalsIgnoreCase("1")) {
                                    DeleteRideRequest("http://maps.googleapis.com/maps/api/directions/json?origin=" + driverCur_Lat + "," + driverCur_Lng + "&destination=" + Str_drop_lat + "," + Str_drop_lon + "&sensor=false");
                                    break;
                                }
                            }
                        }
                    } else {
                        DeleteRideRequest("http://maps.googleapis.com/maps/api/directions/json?origin=" + driverCur_Lat + "," + driverCur_Lng + "&destination=" + Str_drop_lat + "," + Str_drop_lon + "&sensor=false");
                    }

                }
            }
            mapHandler.postDelayed(this, 30000);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_ride);
        mhelper = new LanguageDb(this);
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 10000, 10000); //
        Log.d(TAG, "onCreate: ");
        trackyour_ride_class = TrackRidePage.this;
        initialize();
        try {
            setLocationRequest();
            buildGoogleApiClient();
        } catch (Exception e) {
            e.printStackTrace();
        }
        initializeMap();

        Ll_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TrackRidePage.this, TrackRideAcceptPage.class);
                i.putExtra("flag", "2");
                i.putExtra("mobile_number", Str_phone_number);
                i.putExtra("driverID", Str_driver_id);
                i.putExtra("rideID", Str_ride_id);
                i.putExtra("driverImage", Str_driver_image);
                i.putExtra("driverCar_no", Str_vehicle_number);
                i.putExtra("driverCar_model", Str_vehicle_model);
                startActivity(i);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });

        currentLocation_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                gps = new GPSTracker(getApplicationContext());

                if (gps.canGetLocation() && gps.isgpsenabled()) {

                    MyCurrent_lat = gps.getLatitude();
                    MyCurrent_long = gps.getLongitude();

                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }

                    // Move the camera to last position with a zoom level
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(MyCurrent_lat, MyCurrent_long)).zoom(17).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                } else {
                    enableGpsService();
                    //Toast.makeText(getActivity(), "GPS not Enabled !!!", Toast.LENGTH_LONG).show();
                }
            }
        });

        Ll_sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                panic();
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        SOS();
                    }
                }, 100);


            }
        });
        Ll_shareLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getResources().getString(R.string.share_mytrip_lable).equalsIgnoreCase(Tv_share_car_no.getText().toString())) {
                    if (Str_ride_status.equalsIgnoreCase("Onride"))
                        ShareTripPopUp();
                    //shareTrip();
                }

            }
        });

        Rl_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cancelTripPopUp(getkey("close_booking_cancel"));
            }
        });

        trackdriver_profile_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileDialog();
            }
        });

        trackdriver_profile_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileDialog();
            }
        });
    }

    private void initialize() {
        v2GetRouteDirection = new GMapV2GetRouteDirection();
        session = new SessionManager(TrackRidePage.this);
        cd = new ConnectionDetector(TrackRidePage.this);
        appInfoSessionManager = new AppInfoSessionManager(TrackRidePage.this);
        isInternetPresent = cd.isConnectingToInternet();
        gps = new GPSTracker(TrackRidePage.this);
        dropList = new ArrayList<MultiDropPojo>();
        itemlist_reason = new ArrayList<CancelTripPojo>();
        polylines = new ArrayList<Polyline>();
        wayPointList = new ArrayList<LatLng>();
        picker = CountryPicker.newInstance(getkey("select_country_lable"));
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        UserName = user.get(SessionManager.KEY_USERNAME);
        ScountryCode = user.get(SessionManager.KEY_COUNTRYCODE);
        HashMap<String, String> app = appInfoSessionManager.getAppInfo();
        strCallassist = app.get(appInfoSessionManager.KEY_CUSTOMER_NUMBER);
        countofchat = findViewById(R.id.countofchat);
        chatlayout = findViewById(R.id.chatlayout);
        iv_driverimg = findViewById(R.id.driver_image);
        rl_viewprofile = findViewById(R.id.rl_viewprofile);
        Ll_call = findViewById(R.id.tracking_page_details_call_layout);
        Ll_sos = findViewById(R.id.tracking_page_details_sos_call_layout);
        chatfully = findViewById(R.id.chatfully);
        Ll_shareLocation = findViewById(R.id.track_ride_share_and_car_no_layout);
        Rl_bottom = findViewById(R.id.tracking_page_bottom_layout);
        currentLocation_image = findViewById(R.id.book_current_location_imageview);
        driver_detailpage = findViewById(R.id.driver_detailpage);
        Tv_share_car_no = findViewById(R.id.track_ride_share_and_car_no_textview);
        car_brand_name = findViewById(R.id.car_brand_name);
        drivername = findViewById(R.id.drivername);
        tv_trackdriver_name = findViewById(R.id.trackdriver_name);
        trackdriver_profile_textview = findViewById(R.id.trackdriver_profile_textview);
        trackdriver_profile_textview.setText(getkey("viewprofile"));
        carNumber = findViewById(R.id.track_ride_car_no);
        car_cat_image = findViewById(R.id.car_cat_image);
        timer_image = findViewById(R.id.timer_image);
        milege_image = findViewById(R.id.milege_image);
        calldriver = findViewById(R.id.calldriver);
        calldriver.setText(getkey("freecall"));
        sos_tv = findViewById(R.id.sos_tv);
        sos_tv.setText(getkey("home_emergency"));


        TextView tapmehere = findViewById(R.id.tapmehere);
        tapmehere.setText(getkey("tap_here_lable"));


        Tv_Distance = findViewById(R.id.tracking_page_details_distance_textview);
        Tv_time = findViewById(R.id.track_ride_time_textview);
        Tv_BottomText = findViewById(R.id.tracking_page_bottom_layout_textview);
        Tv_BottomText.setText(getkey("ride_list_label_cancel_trip"));
        Cv_ontrip = findViewById(R.id.tracking_page_ontrip_cardview);
        rl_ontrip = findViewById(R.id.tracking_page_ontrip_cardview1);
        track_share_rl = findViewById(R.id.track_share_rl);
        multistoptext = findViewById(R.id.multistoptext);
        onTripTv = findViewById(R.id.ontrip_tv);
        onTripTv.setText(getkey("onthewaynow"));
        ontrip_tv1 = findViewById(R.id.ontrip_tv1);
        face = Typeface.createFromAsset(getAssets(), "fonts/vladimir.ttf");
        tf = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
        carNumber.setTypeface(tf);
        Tv_time.setTypeface(tf);
        Tv_Distance.setTypeface(tf);
        calldriver.setTypeface(tf);
        sos_tv.setTypeface(tf);

        chatlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrackRidePage.this, MessagesActivity.class);
                intent.putExtra("name", Str_driver_name);
                intent.putExtra("userimage", Str_driver_image);
                intent.putExtra("rideid", Str_ride_id);
                intent.putExtra("useridtosend", Str_driver_id);
                intent.putExtra("userphonenumber", Str_phone_number);
                intent.putExtra("driver_fcm_token", driver_fcm_token);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        if (intent.hasExtra("ride_id")) {
            Str_ride_id = intent.getStringExtra("ride_id");
        }
        HashMap<String, String> bit_map = session.getVehicleBitmap();
        base64 = bit_map.get(SessionManager.KEY_VEHICLE_BitMap_IMAGE);
        System.out.println("=============Muruga TrackingPage vehicle image" + SessionManager.KEY_VEHICLE_BitMap_IMAGE);
        //  bmp = StringToBitMap(base64);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver");
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip");
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip_Return");
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver");
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Multiplestop");
        registerReceiver(refreshReceiver, intentFilter);

    }

    private void initializeMap() {
        if (googleMap == null) {
            //googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.book_my_ride3_mapview)).getMap();
            mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.tracking_page_mapview));
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap arg) {
                    loadMap(arg);
                }
            });
        }
    }

    public void loadMap(GoogleMap arg) {
        googleMap = arg;
        // Changing map type
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Showing / hiding your current location
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(false);

        }
        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(false);
        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        if (session.getNightMode()) {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.mapstyle));
        } else {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style));
        }

        googleMap.setMinZoomPreference(6.0f);
        googleMap.setMaxZoomPreference(18.0f);
        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        if (gps.canGetLocation() && gps.isgpsenabled()) {
            double Dlatitude = gps.getLatitude();
            double Dlongitude = gps.getLongitude();

            sCurLat = String.valueOf(Dlatitude);
            sCurLon = String.valueOf(Dlongitude);

            // Move the camera to last position with a zoom level
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(15).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            Alert(getkey("action_error"), getResources().getString(R.string.common_google_play_services_enable_text));
        }
        if (mapHandler != null) {
            mapHandler.removeCallbacks(mapRunnable);
        }

        if (isInternetPresent) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("ride_id", Str_ride_id);
            postRequest_TrackRide(Iconstant.track_your_ride_url, jsonParams);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }
    }

  /*  //---------------------------------------------------------------------
    private void postRequest_PhoneMasking(String Url) {

        dialog = new Dialog(TrackRidePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_ride_id);
        jsonParams.put("user_type", "user");

        System.out.println("-------------PhoneMasking Url----------------" + Url);
        mRequest = new ServiceRequest(TrackRidePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------PhoneMasking Response----------------" + response);

                String Str_status = "", SResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    SResponse = object.getString("response");
                    if (Str_status.equalsIgnoreCase("1")) {

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }*/

    private void cancelTripPopUp(String message) {
        final Dialog dialog = new Dialog(TrackRidePage.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cancel_popup_new);
        final Button Ok_Tv = (Button) dialog.findViewById(R.id.custom_dialog_library_cancel_button);
        final Button no = (Button) dialog.findViewById(R.id.no);

        Ok_Tv.setText(getkey("yes_text"));
        no.setText(getkey("no"));

        final CustomTextView balance_amount = (CustomTextView) dialog.findViewById(R.id.custom_dialog_library_message_textview);
        balance_amount.setText(message);
        Ok_Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(TrackRidePage.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    postRequest_CancelRides_Reason(Iconstant.cancel_myride_reason_url);
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }

                dialog.dismiss();

            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    //----------------------PostRequest for Track Ride---------------------------------------

    private void postRequest_TrackRide(String Url, final HashMap<String, String> jsonParams) {
        dialog = new Dialog(TrackRidePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_processing"));

        System.out.println("-------------Track Ride Url----------------" + Url);
        System.out.println("-------------Track Ride jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(TrackRidePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                trackpojo = new Trackrideafteraccept_pojo();
                itemList = new ArrayList<>();
                itemList.clear();
                System.out.println("-------------Track Ride Response----------------" + response);

                String Sstatus = "";
                String return_mode = "", is_return_over = "", closing_status = "", wait_time = "";
                try {

                    JSONObject object = new JSONObject(response);

                    Type listType = new TypeToken<Trackrideafteraccept_pojo>() {
                    }.getType();

                    try {
                        trackpojo = new GsonBuilder().create().fromJson(object.toString(), listType);
                    } catch (JsonSyntaxException e) {
                        System.out.println("sout" + e);
                    }

                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {

                            Object check_driver_profile_object = response_object.get("driver_profile");
                            if (check_driver_profile_object instanceof JSONObject) {
                                JSONObject driver_profile_object = response_object.getJSONObject("driver_profile");
                                if (driver_profile_object.length() > 0) {
                                    session.setDriverprofile(driver_profile_object.toString());
                                    Str_driver_id = trackpojo.getResponse().getDriver_profile().getDriver_id();
                                    Str_driver_name = trackpojo.getResponse().getDriver_profile().getDriver_name();
                                    tv_trackdriver_name.setText(Str_driver_name);
                                    Str_driver_email = trackpojo.getResponse().getDriver_profile().getDriver_email();
                                    Str_driver_image = trackpojo.getResponse().getDriver_profile().getDriver_image();
                                    Str_driver_review = trackpojo.getResponse().getDriver_profile().getDriver_review();
                                    Str_driver_lat = trackpojo.getResponse().getDriver_profile().getDriver_lat();
                                    Str_driver_lon = trackpojo.getResponse().getDriver_profile().getDriver_lon();
                                    Str_est_arrival = trackpojo.getResponse().getDriver_profile().getEst_arrival();
                                    Str_rider_lat = trackpojo.getResponse().getDriver_profile().getRider_lat();
                                    Str_rider_lon = trackpojo.getResponse().getDriver_profile().getRider_lon();
                                    Str_min_pickup_duration = trackpojo.getResponse().getDriver_profile().getMin_pickup_duration();
                                    Str_ride_id = trackpojo.getResponse().getDriver_profile().getRide_id();
                                    Str_phone_number = trackpojo.getResponse().getDriver_profile().getPhone_number();
                                    session.setKeyDriverPhonenumber(Str_phone_number);
                                    Str_vehicle_number = trackpojo.getResponse().getDriver_profile().getVehicle_number();
                                    Str_vehicle_model = trackpojo.getResponse().getDriver_profile().getVehicle_model();

                                    Str_ride_status = trackpojo.getResponse().getDriver_profile().getRide_status();
                                    savingStr_ride_status = Str_ride_status;
                                    Str_mode = trackpojo.getResponse().getDriver_profile().getMode();
                                    Str_approx_dist = trackpojo.getResponse().getDriver_profile().getApprox_dist();
                                    Str_est_cost = trackpojo.getResponse().getDriver_profile().getEst_cost();
                                    Str_currency = trackpojo.getResponse().getDriver_profile().getCurrency();
                                    Str_brand_image = trackpojo.getResponse().getDriver_profile().getBrand_image();

                                    driverCur_Lat = Str_driver_lat;
                                    driverCur_Lng = Str_driver_lon;

                                    return_mode = trackpojo.getResponse().getDriver_profile().getReturn_mode();
                                    is_return_over = trackpojo.getResponse().getDriver_profile().getIs_return_over();
                                    closing_status = trackpojo.getResponse().getDriver_profile().getClosing_status();
                                    wait_time = trackpojo.getResponse().getDriver_profile().getWait_time();

                                    Glide.with(TrackRidePage.this).load(trackpojo.getResponse().getDriver_profile().getDriver_image()).into(iv_driverimg);

                                    itemList.addAll(trackpojo.getResponse().getDriver_profile().getDriver_array_view());


                                    session.setFreeWaitingTime(trackpojo.getResponse().getDriver_profile().getComp_waiting_time());

                                    isContinue = session.getContinueTrip();
                                    System.out.println("==========Muruga isContinue drop waitingtime=======" + isContinue);
                                    if (!isContinue.equalsIgnoreCase("yes")) {
                                        if (Str_mode.equalsIgnoreCase("return")) {
                                            if (return_mode.equalsIgnoreCase("start") && is_return_over.equalsIgnoreCase("0") && closing_status.equalsIgnoreCase("open")) {
                                                callWaitingTime(trackpojo.getResponse().getDriver_profile().getWait_time(), "return");
                                            }
                                        } /*else if (Str_mode.equalsIgnoreCase("multistop")) {

                                        }*/
                                    }

                                    Object check_drop_object = driver_profile_object.get("drop");
                                    if (check_drop_object instanceof JSONObject) {
                                        JSONObject drop_object = driver_profile_object.getJSONObject("drop");
                                        if (drop_object.length() > 0) {
                                            Str_drop_location = trackpojo.getResponse().getDriver_profile().getDrop().getLocation();
                                            JSONObject drop_latlong_object = drop_object.getJSONObject("latlong");
                                            if (drop_latlong_object.length() > 0) {
                                                Str_drop_lon = trackpojo.getResponse().getDriver_profile().getDrop().getLatlong().getLon();
                                                Str_drop_lat = trackpojo.getResponse().getDriver_profile().getDrop().getLatlong().getLat();
                                            }
                                        }
                                    }
                                    carTypeImage = trackpojo.getResponse().getDriver_profile().getCategory_icon();
                                    CategoryImage = trackpojo.getResponse().getDriver_profile().getCategory_icon_map();
                                    new LoadBitmap().execute(CategoryImage);
                                    Object check_pickup_object = driver_profile_object.get("pickup");
                                    if (check_pickup_object instanceof JSONObject) {
                                        JSONObject pickup_object = driver_profile_object.getJSONObject("pickup");
                                        if (pickup_object.length() > 0) {
                                            Str_pickup_location = trackpojo.getResponse().getDriver_profile().getPickup().getLocation();
                                            JSONObject pickup_latlong_object = pickup_object.getJSONObject("latlong");
                                            if (pickup_latlong_object.length() > 0) {
                                                Str_pickup_lon = trackpojo.getResponse().getDriver_profile().getPickup().getLatlong().getLon();
                                                Str_pickup_lat = trackpojo.getResponse().getDriver_profile().getPickup().getLatlong().getLat();
                                            }
                                        }
                                    }


                                    Object notification_details = driver_profile_object.get("notification_details");
                                    if (notification_details instanceof JSONObject) {
                                        JSONObject notification_detailsjson = driver_profile_object.getJSONObject("notification_details");
                                        if (notification_detailsjson.length() > 0) {
                                            driver_fcm_token = notification_detailsjson.getString("driver_fcm_token");
                                            System.out.println("driver_fcm_token---"+driver_fcm_token);
                                        }
                                    }


                                    if ("1".equalsIgnoreCase(trackpojo.getResponse().getDriver_profile().getIs_return_over())) {

                                        Object check_next_drop_object = driver_profile_object.get("next_drop_location");
                                        if (check_next_drop_object instanceof JSONObject) {
                                            JSONObject next_drop_object = driver_profile_object.getJSONObject("next_drop_location");
                                            if (next_drop_object.length() > 0) {
                                                Str_drop_location = trackpojo.getResponse().getDriver_profile().getNext_drop_location().get(0).getLocation();
                                                JSONObject next_drop_latlong_object = next_drop_object.getJSONObject("latlong");
                                                if (next_drop_latlong_object.length() > 0) {
                                                    Str_drop_lon = trackpojo.getResponse().getDriver_profile().getNext_drop_location().get(0).getLatlongObject().getLon();
                                                    Str_drop_lat = trackpojo.getResponse().getDriver_profile().getNext_drop_location().get(0).getLatlongObject().getLat();
                                                }

                                            }
                                        }
                                        Object check_next_pickup_object = driver_profile_object.get("drop_location_array");
                                        if (check_next_pickup_object instanceof JSONObject) {
                                            JSONObject next_pickup_object = driver_profile_object.getJSONObject("drop_location_array");
                                            if (next_pickup_object.length() > 0) {
                                                Str_pickup_location = trackpojo.getResponse().getDriver_profile().getDrop_location_array().get(0).getLocation();
                                                JSONObject next_pickup_latlong_object = next_pickup_object.getJSONObject("latlong");
                                                if (next_pickup_latlong_object.length() > 0) {
                                                    Str_pickup_lon = trackpojo.getResponse().getDriver_profile().getDrop_location_array().get(0).getLatlongObject().getLon();
                                                    Str_pickup_lat = trackpojo.getResponse().getDriver_profile().getDrop_location_array().get(0).getLatlongObject().getLat();
                                                }
                                            }
                                        }
                                    }
                                    if ("multistop".equalsIgnoreCase(Str_mode)) {

                                        Object check_multipath_loc_object = driver_profile_object.get("multistop_array");
                                        if (check_multipath_loc_object instanceof JSONArray) {
                                            JSONArray multipath_loc_array = driver_profile_object.getJSONArray("multistop_array");
                                            if (multipath_loc_array.length() > 0) {
                                                dropList.clear();
                                                for (int j = 0; j < multipath_loc_array.length(); j++) {
                                                    JSONObject multipath_loc_object = multipath_loc_array.getJSONObject(j);
                                                    MultiDropPojo pojo = new MultiDropPojo();
                                                    pojo.setPlaceName(trackpojo.getResponse().getDriver_profile().getMultistop_array().get(j).getLocation());
                                                    pojo.setIsEnd(trackpojo.getResponse().getDriver_profile().getMultistop_array().get(j).getIs_end());
                                                    pojo.setMode(trackpojo.getResponse().getDriver_profile().getMultistop_array().get(j).getMode());

                                                    JSONObject multipath_loc_latlong_object = multipath_loc_object.getJSONObject("latlong");
                                                    if (multipath_loc_latlong_object.length() > 0) {
                                                        pojo.setPlaceLat(trackpojo.getResponse().getDriver_profile().getMultistop_array().get(j).getLatlongObject().getLat());
                                                        pojo.setPlaceLong(trackpojo.getResponse().getDriver_profile().getMultistop_array().get(j).getLatlongObject().getLon());
                                                    }
                                                    if (Srecord_id.equalsIgnoreCase(trackpojo.getResponse().getDriver_profile().getMultistop_array().get(j).getLocation())) {
                                                        sPosition = j;
                                                    }

                                                    dropList.add(pojo);
                                                }
                                                sMultipleDropStatus = "0";
                                            } else {
                                                sMultipleDropStatus = "0";
                                            }
                                        }
                                    } else {
                                        sMultipleDropStatus = "0";
                                    }
                                }
                            }
                            isDataAvailable = true;

                        } else {
                            isDataAvailable = false;
                        }
                    } else {
                        isDataAvailable = false;
                    }


                    if (Sstatus.equalsIgnoreCase("1") && isDataAvailable) {
                        if ("Cancelled".equalsIgnoreCase(Str_ride_status)) {
                            Intent intent = new Intent(TrackRidePage.this, Navigation_new.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        } else if ("Finished".equalsIgnoreCase(Str_ride_status)) {
                            Intent intent = new Intent(TrackRidePage.this, FareBreakUp.class);
                            intent.putExtra("RideID", Str_ride_id);
                            intent.putExtra("ratingflag", "2");
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        } else if ("Completed".equalsIgnoreCase(Str_ride_status)) {
                            Intent intent = new Intent(TrackRidePage.this, FareBreakUp.class);
                            intent.putExtra("RideID", Str_ride_id);
                            intent.putExtra("ratingflag", "1");
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        } else {
                            carPoint = new LatLng(Double.parseDouble(Str_driver_lat), Double.parseDouble(Str_driver_lon));
                            System.out.println("-----------Str_ride_status---------" + Str_ride_status);
                            System.out.println("-----------prem Str_pickup_lon---------" + Double.parseDouble(Str_pickup_lon) + "," + Double.parseDouble(Str_pickup_lat));
                            System.out.println("-----------prem Str_ride_status---------" + Double.parseDouble(Str_drop_lon) + "," + Double.parseDouble(Str_drop_lat));

                            googleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(Double.parseDouble(Str_pickup_lat), Double.parseDouble(Str_pickup_lon)))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_pointer_round)));


                            if (Str_ride_status.equalsIgnoreCase("Onride")) {

                                MarkerOptions mrOptions = new MarkerOptions()
                                        .position(new LatLng(Double.parseDouble(Str_drop_lat), Double.parseDouble(Str_drop_lon)))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)).anchor(0.5f, 0.3f);
                                googleMap.addMarker(mrOptions);

                                addIcon(getkey("dropoff"), new LatLng(Double.parseDouble(Str_drop_lat), Double.parseDouble(Str_drop_lon)), 2);

                            }
                            pickupLatLag = new LatLng(Double.parseDouble(Str_pickup_lat), Double.parseDouble(Str_pickup_lon));
                            dropLatLon = new LatLng(Double.parseDouble(Str_drop_lat), Double.parseDouble(Str_drop_lon));


                            if (("Confirmed".equalsIgnoreCase(Str_ride_status)) || ("Arrived".equalsIgnoreCase(Str_ride_status))) {
                                Ll_sos.setVisibility(View.GONE);
                                Ll_call.setVisibility(View.VISIBLE);
                                if (("Confirmed".equalsIgnoreCase(Str_ride_status))) {
                                    Cv_ontrip.setVisibility(View.VISIBLE);
                                    rl_ontrip.setVisibility(View.VISIBLE);
                                    onTripTv.setText(getkey("trackdriver_label_ondway"));
                                    ontrip_tv1.setText(getkey("trackdriver_label_ondway"));
                                } else {
                                    Cv_ontrip.setVisibility(View.GONE);
                                    rl_ontrip.setVisibility(View.GONE);
                                }

                                car_cat_image.setImageDrawable(null);
//                                track_share_rl.setBackgroundDrawable(getResources().getDrawable(R.drawable.trackcar_vg));
                                Picasso.with(getApplicationContext()).load(String.valueOf(carTypeImage)).placeholder(R.drawable.transparant_car_img).into(car_cat_image);
                                /*visible*/
                                Rl_bottom.setVisibility(View.VISIBLE);
                                rl_ontrip.setVisibility(View.GONE);
                                car_brand_name.setVisibility(View.GONE);
                                car_brand_name.setText(Str_vehicle_model);
                                Tv_share_car_no.setVisibility(View.GONE);
                                carNumber.setText(Str_vehicle_number);
                                drivername.setText(Str_driver_name);
                                Tv_Distance.setText(Str_approx_dist.toUpperCase() + " " + getkey("away"));
                                Tv_time.setText(getkey("in") +" "+ Str_est_arrival.toUpperCase());
                                sMultipleDropStatus = "0";
                                route(Str_ride_status, Str_driver_lat, Str_driver_lon, Str_pickup_lat, Str_pickup_lon);
                            } else {
                                car_cat_image.setImageDrawable(null);
                                car_cat_image.setBackgroundResource(R.drawable.icn_sharemytrip);
                                track_share_rl.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_navi));
                                timer_image.setBackgroundDrawable(getResources().getDrawable(R.drawable.green_clock));
                                milege_image.setBackgroundDrawable(getResources().getDrawable(R.drawable.meter_green_old));
                                Ll_sos.setVisibility(View.VISIBLE);
                                chatfully.setVisibility(View.GONE);
                                Ll_call.setVisibility(View.GONE);
                                Rl_bottom.setVisibility(View.GONE);
//                                Cv_ontrip.setVisibility(View.VISIBLE);
                                rl_ontrip.setVisibility(View.VISIBLE);
                                carNumber.setVisibility(View.GONE);
                                onTripTv.setText(getkey("on_trip_lable"));
                                ontrip_tv1.setText(getkey("on_trip_lable"));
                                Cv_ontrip.setVisibility(View.GONE);
                                Tv_share_car_no.setVisibility(View.VISIBLE);
                                Tv_share_car_no.setTextColor(getResources().getColor(R.color.white));
                                Tv_share_car_no.setText(getkey("share_mytrip_lable"));
                                Tv_share_car_no.setTextSize(12);
//                                carNumber.setText("FIND ME!");
//                                carNumber.setBackgroundResource(R.color.app_color2);
                                Ll_shareLocation.setBackgroundResource(R.color.app_color2);//setBackgroundDrawable(getResources().getDrawable(R.drawable.gray_gradient_new_track));
                                Tv_share_car_no.setTypeface(face);
                                Tv_Distance.setText(Str_approx_dist.toUpperCase());
                                Tv_time.setText(getkey("in")+" " + Str_est_arrival.toUpperCase());
                                drivername.setText(Str_driver_name);
                                System.out.println("------------sMultipleDropStatus---------------" + Str_mode);

                                if ("multistop".equalsIgnoreCase(Str_mode)) {
                                    sMultipleDropStatus = "0";
                                    boolean isDropoff_selected = false;
                                    for (int j = 0; j < dropList.size(); j++) {
                                        // test
                                        if (!isDropoff_selected) {
                                            if (dropList.get(j).getIsEnd().equalsIgnoreCase("0")) {
                                                MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.parseDouble(dropList.get(j).getPlaceLat()), Double.parseDouble(dropList.get(j).getPlaceLong())));
                                                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.red));
                                                googleMap.addMarker(marker);
                                                addIcon(getkey("dropoff"), new LatLng(Double.parseDouble(dropList.get(j).getPlaceLat()), Double.parseDouble(dropList.get(j).getPlaceLong())), 2);

                                                if (j > 0) {
                                                    MarkerOptions markers = new MarkerOptions().position(new LatLng(Double.parseDouble(dropList.get(j - 1).getPlaceLat()), Double.parseDouble(dropList.get(j - 1).getPlaceLong())));
                                                    markers.icon(BitmapDescriptorFactory.fromResource(R.drawable.red));
                                                    googleMap.addMarker(markers);
                                                    addIcon(getkey("pikcup"), new LatLng(Double.parseDouble(dropList.get(j - 1).getPlaceLat()), Double.parseDouble(dropList.get(j - 1).getPlaceLong())), 2);
                                                }
                                                isDropoff_selected = true;
                                            }
                                        } else {
                                            MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.parseDouble(dropList.get(j).getPlaceLat()), Double.parseDouble(dropList.get(j).getPlaceLong())));
                                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.red));
                                            googleMap.addMarker(marker);
                                            addIcon(getkey("dropoff"), new LatLng(Double.parseDouble(dropList.get(j).getPlaceLat()), Double.parseDouble(dropList.get(j).getPlaceLong())), 2);
                                        }
                                    }

                                    for (int i = 0; i < dropList.size(); i++) {
                                        System.out.println("-----------dropList size------------" + dropList.size());
                                        if (dropList.get(i).getIsEnd().equalsIgnoreCase("0")) {
                                            if (i == 0) {
                                                System.out.println("--------ip if contdition-getIsEnd--------------" + i + " " + dropList.get(0).getPlaceLat() + "," + dropList.get(0).getPlaceLong());
                                                System.out.println("-----------Murugan stopover 1------------" + (i + 1));
                                                multistoptext.setVisibility(View.VISIBLE);
                                                multistoptext.setText(getkey("ontripstopo") + (i + 1));

                                                CallWaitingTimeMultiStop(dropList.get(i).getIsEnd(), dropList.get(i).getMode(), closing_status, wait_time);

                                                route(Str_ride_status, Str_pickup_lat, Str_pickup_lon, dropList.get(0).getPlaceLat(), dropList.get(0).getPlaceLong());
                                                break;
                                            } else if (i == dropList.size() - 1) {
                                                multistoptext.setVisibility(View.VISIBLE);
                                                multistoptext.setText(getkey("ontripstopo")+ (i + 1));
                                                System.out.println("-----------Murugan stopover 2------------" + (i + 1));
                                                System.out.println("---------i else if contdition---getIsEnd------------" + i + " " + dropList.get(i).getPlaceLat() + "," + dropList.get(i).getPlaceLong());
                                                CallWaitingTimeMultiStop(dropList.get(i).getIsEnd(), dropList.get(i).getMode(), closing_status, wait_time);
                                                route(Str_ride_status, dropList.get(i - 1).getPlaceLat(), dropList.get(i - 1).getPlaceLong(), dropList.get(i).getPlaceLat(), dropList.get(i).getPlaceLong());
                                                break;
                                            } else {
                                                multistoptext.setVisibility(View.VISIBLE);
                                                multistoptext.setText(getkey("ontripstopo") + (i + 1));
                                                System.out.println("----------multiple stop else contdition---getIsEnd------------" + i + " " + dropList.get(i).getPlaceLat() + "," + dropList.get(i).getPlaceLong());
                                                System.out.println("-----------Murugan stopover 3------------" + (i + 1));
                                                CallWaitingTimeMultiStop(dropList.get(i).getIsEnd(), dropList.get(i).getMode(), closing_status, wait_time);
                                                route(Str_ride_status, dropList.get(i - 1).getPlaceLat(), dropList.get(i - 1).getPlaceLong(), dropList.get(i).getPlaceLat(), dropList.get(i).getPlaceLong());
                                                break;
                                            }
                                        } else if (i == dropList.size() - 1) {
                                            if (dropList.get(i).getIsEnd().equalsIgnoreCase("1")) {
                                                multistoptext.setVisibility(View.GONE);
                                                System.out.println("----------multiple stop else if contdition---------------" + i + " " + dropList.get(i).getPlaceLat() + "," + dropList.get(i).getPlaceLong());
                                                System.out.println("-----------Murugan stopover 4------------" + i);
                                                CallWaitingTimeMultiStop(dropList.get(i).getIsEnd(), dropList.get(i).getMode(), closing_status, wait_time);
                                                route(Str_ride_status, dropList.get(i).getPlaceLat(), dropList.get(i).getPlaceLong(), Str_drop_lat, Str_drop_lon);
                                                break;
                                            }
                                        }

                                    }

                                } else {
                                    sMultipleDropStatus = "0";
                                    route(Str_ride_status, Str_pickup_lat, Str_pickup_lon, Str_drop_lat, Str_drop_lon);
                                }

                            }

                        }
                        mapHandler.postDelayed(mapRunnable, 0);

                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void CallWaitingTimeMultiStop(String isEnd, String mode, String closingStatus, String wait_time) {

        System.out.println("==============Muruga MultiStopWating Call=============  " + isEnd + " " + mode + " " + closingStatus + " " + wait_time);

        if (!isContinue.equalsIgnoreCase("yes")) {
            if (Str_mode.equalsIgnoreCase("multistop")) {
                if (mode.equalsIgnoreCase("end") && isEnd.equalsIgnoreCase("0") && closingStatus.equalsIgnoreCase("open")) {
                    callWaitingTime(wait_time, "multistop");
                }

            }
        }

    }

    private void callWaitingTime(String wait_time, String returnOrMulti) {
        Intent intent = new Intent(TrackRidePage.this, WaitingTimePage.class);
        intent.putExtra("ride_id", Str_ride_id);
        intent.putExtra("FreeWaitingTime", wait_time);
        intent.putExtra("IsNormalOrreturnMulti", returnOrMulti);
        intent.putExtra("isContinue", true);
        Calendar calendar = Calendar.getInstance();
        startActivity(intent);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    //--------------------Post Request for Cancel Request-------------------------------------

    private void postRequest_CancelRides_Reason(String Url) {
        dialog = new Dialog(TrackRidePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));
        System.out.println("-------------MyRide Cancel Reason Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", Str_ride_id);
        System.out.println("-------------MyRide Cancel Reason jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(TrackRidePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------MyRide Cancel Reason Response----------------" + response);
                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONArray reason_array = response_object.getJSONArray("reason");
                            if (reason_array.length() > 0) {
                                itemlist_reason.clear();
                                for (int i = 0; i < reason_array.length(); i++) {
                                    JSONObject reason_object = reason_array.getJSONObject(i);
                                    CancelTripPojo pojo = new CancelTripPojo();
                                    pojo.setReason(reason_object.getString("reason"));
                                    pojo.setReasonId(reason_object.getString("id"));
                                    itemlist_reason.add(pojo);
                                }
                                isReasonAvailable = true;
                            } else {
                                isReasonAvailable = false;
                            }
                        }
                    } else if (Sstatus.equalsIgnoreCase("2")) {
                        String Sresponse = object.getString("response");
                        Alert1(getkey("alert_label_title"), Sresponse);
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getkey("alert_label_title"), Sresponse);
                    }


                    if (Sstatus.equalsIgnoreCase("1") && isReasonAvailable) {
                        Intent passIntent = new Intent(TrackRidePage.this, TrackRideCancelTrip.class);
                        Bundle bundleObject = new Bundle();
                        bundleObject.putSerializable("Reason", itemlist_reason);
                        passIntent.putExtras(bundleObject);

                        passIntent.putExtra("driverImage", Str_driver_image);
                        passIntent.putExtra("RideID", Str_ride_id);
                        startActivity(passIntent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //--------------------Post Request for Sos alert-------------------------------------

    private void postRequest_Sos(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(TrackRidePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_pleasewait"));
        System.out.println("-------------Sos alert Url----------------" + Url);
        System.out.println("-------------Sos alert jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(TrackRidePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Sos alert Response----------------" + response);
                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    String Sresponse = object.getString("response");
                    if ("1".equalsIgnoreCase(Sstatus)) {
                        Alert(getkey("action_success"), Sresponse);
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        SOS1();

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(TrackRidePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(TrackRidePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(TrackRidePage.this, Navigation_new.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
        mDialog.show();

    }


    //------------------------Draw Route on map------------------------------------

  /*  private void route(String mStatus) {
        double lat_decimal = Double.parseDouble(Str_drop_lat);
        double lng_decimal = Double.parseDouble(Str_drop_lon);
        double pick_lat_decimal = Double.parseDouble(Str_pickup_lat);
        double pick_lng_decimal = Double.parseDouble(Str_pickup_lon);
        updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal, mStatus);
    }*/


    private void route(String mStatus, String startLat, String startLon, String endLat, String endLon) {
        double lat_decimal = Double.parseDouble(endLat);
        double lng_decimal = Double.parseDouble(endLon);
        double pick_lat_decimal = Double.parseDouble(startLat);
        double pick_lng_decimal = Double.parseDouble(startLon);
        updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal, mStatus);
    }


    private void updateGoogleMapTrackRide(double lat_decimal, double lng_decimal, double pick_lat_decimal, double pick_lng_decimal, String nStatus) {
        LatLng dropLatLng = new LatLng(lat_decimal, lng_decimal);
        LatLng pickUpLatLng = new LatLng(pick_lat_decimal, pick_lng_decimal);

        GetNewRouteTask draw_route_asyncTask = new GetNewRouteTask();
        draw_route_asyncTask.setToAndFromLocation(pickUpLatLng, dropLatLng, nStatus);
        draw_route_asyncTask.execute();

    }

    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    private class GetNewRouteTask extends AsyncTask<String, Void, String> {
        String response = "";
        private LatLng currentLocation;
        private LatLng endLocation;
        private ArrayList<LatLng> wayLatLng_global;

        public void setToAndFromLocation(LatLng currentLocation, LatLng endLocation, String status) {
            this.currentLocation = currentLocation;
            this.endLocation = endLocation;
            startWayPoint = currentLocation;
            endWayPoint = endLocation;
            sWayPointStatus = status;
            wayLatLng_global = addWayPointPoint(currentLocation, endLocation, dropList); //multipleDropLatLng);
//            this.wayLatLng = addWayPointPoint(currentLocation, endLocation, dropList); //multipleDropLatLng);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... urls) {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(endLocation, currentLocation, GMapV2GetRouteDirection.MODE_DRIVING);
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(TrackRidePage.this)
                    .alternativeRoutes(true)
                    .waypoints(wayLatLng_global)
                    .key(Iconstant.Routekey)
                    .build();
            routing.execute();
            response = "Success";
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("Success")) {

            }
        }
    }

    private ArrayList<LatLng> addWayPointPoint(LatLng start, LatLng end, ArrayList<MultiDropPojo> mMultipleDropLatLng)//  String[] mMultipleDropLatLng) {
    {

        wayPointList.clear();

        wayPointBuilder = new LatLngBounds.Builder();

        System.out.println("RIDE_STATUS----------------------jai--------" + Str_ride_status);
        if (getResources().getString(R.string.return_lable).equals(Str_mode) && Str_ride_status.equals("Onride")) {


            wayPointBuilder.include(start);
            wayPointBuilder.include(end);
            wayPointList.add(start);
            wayPointList.add(end);
            wayPointList.add(start);
        } else {
            wayPointBuilder.include(start);
            wayPointList.add(start);

            if (sMultipleDropStatus.equalsIgnoreCase("1")) {
                for (int i = 0; i < mMultipleDropLatLng.size(); i++) {

//                String sLatLng = mMultipleDropLatLng[i];
//                String[] latLng = sLatLng.split(",");

                    double Dlatitude = Double.parseDouble(mMultipleDropLatLng.get(i).getPlaceLat());
                    double Dlongitude = Double.parseDouble(mMultipleDropLatLng.get(i).getPlaceLong());

                    wayPointList.add(new LatLng(Dlatitude, Dlongitude));
                    wayPointBuilder.include(new LatLng(Dlatitude, Dlongitude));

                }
            }

            wayPointBuilder.include(end);

            wayPointList.add(end);
        }


        return wayPointList;
    }

    @Override
    public void onRoutingFailure(RouteException e) {

        if (e != null) {
            System.out.println("------------onRoutingFailure ---------------" + e.getMessage());
            // Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            System.out.println("------------onRoutingFailure-  ddfd--------------");
            // Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }

        if (wayLatLng_global != null) {
            try {
                Routing routing = new Routing.Builder()
                        .travelMode(AbstractRouting.TravelMode.DRIVING)
                        .withListener(TrackRidePage.this)
                        .alternativeRoutes(true)
                        .waypoints(wayLatLng_global)
                        .key(Iconstant.Routekey)
                        .build();
                routing.execute();
            } catch (NullPointerException e1) {

            } catch (Exception e2) {
            }

        }


    }

    @Override
    public void onRoutingStart() {
        System.out.println("------------onRoutingStart---------------");
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int i) {
        trackingCarimage = session.getCarImage();
        System.out.println("==================muruga tracking car image================" + trackingCarimage + carPoint);
       /* if (base64.equalsIgnoreCase("")) {
            HashMap<String, String> bit_map = session.getVehicleBitmap();
            base64 = bit_map.get(SessionManager.KEY_VEHICLE_BitMap_IMAGE);
            System.out.println("=============muruga trackingPage vehicle image" + SessionManager.KEY_VEHICLE_BitMap_IMAGE);
            bmp = StringToBitMap(base64);
        }*/

        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        System.out.println("=========" + route.get(0).getDistanceText());
        System.out.println("=========" + route.get(0));

        polylines = new ArrayList<>();
        //add route(s) to the map.
//        for (int j = 0; i < route.size(); i++) {
        //In case of more than 5 alternative routes
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(R.color.btn_green_color));
        polyOptions.width(10);
        polyOptions.addAll(route.get(0).getPoints());
        polyline = googleMap.addPolyline(polyOptions);
        polylines.add(polyline);
//        }

        if (sWayPointStatus.equalsIgnoreCase("Confirmed")) {
            if (curentDriverMarker != null) {
                curentDriverMarker.remove();
            }

            curentDriverMarker = googleMap.addMarker(new MarkerOptions()
                    .position(carPoint)
                    .icon(BitmapDescriptorFactory.fromBitmap(bmp)));

        } else {
            if (curentDriverMarker != null) {
                curentDriverMarker.remove();
            }
            curentDriverMarker = googleMap.addMarker(new MarkerOptions()
                    .position(carPoint)
                    .icon(BitmapDescriptorFactory.fromBitmap(bmp)));
        }
/* if (!base64.equalsIgnoreCase("")) {
                curentDriverMarker = googleMap.addMarker(new MarkerOptions()
                        .position(carPoint)
                        .icon(BitmapDescriptorFactory.fromBitmap(bmp)));

            } */
        if (wayPointBuilder != null) {
//            LatLngBounds bounds = wayPointBuilder.build();
//            int padding = 30; // offset from edges of the map in pixels
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//            googleMap.moveCamera(cu);

            int padding, paddingH, paddingW;
            LatLngBounds bounds = wayPointBuilder.build();

//            if(mapFragment == null){
//                mapFragment = ((MapFragment) getFragmentManager().findFragmentById(
//                        R.id.book_my_ride3_mapview));
//            }

            if (mapFragment != null) {

                if (mapFragment.getView() != null) {

                    final View mapview = mapFragment.getView();
                    float maxX = mapview.getMeasuredWidth();
                    float maxY = mapview.getMeasuredHeight();

                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    int height = displayMetrics.heightPixels;
                    int width = displayMetrics.widthPixels;

                    System.out.println("***************************height and width" + height + "," + width);

                    float percentageH = 50.0f;
                    float percentageW = 80.0f;
                    paddingH = (int) (maxY * (percentageH / 100.0f));
                    paddingW = (int) (maxX * (percentageW / 100.0f));


                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, paddingW, paddingH, 100);
                    googleMap.animateCamera(cu);
                } else {
                    LatLngBounds boundss = wayPointBuilder.build();
                    int paddings = 30; // offset from edges of the map in pixels
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(boundss, paddings);
                    googleMap.animateCamera(cu);

                }
            }
        }

    }

    @Override
    public void onRoutingCancelled() {
        System.out.println("------------muruga trackingPage onRoutingCancelled---------------");
    }

    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    protected void startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
//        this.currentLocation = location;

        sCurLat = String.valueOf(location.getLatitude());
        sCurLon = String.valueOf(location.getLongitude());
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
        startLocationUpdates();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        getchatunreadcount(getApplicationContext());
    }

    private void getchatunreadcount(Context mContext)
    {
        int unreadcount=0;
        DbHelper mHelper= null;
        SQLiteDatabase dataBase = null;
        mHelper= new DbHelper(mContext);
        ContentValues values  = new ContentValues();
        values.put(DbHelper.status,"1");
        dataBase = mHelper.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME+" WHERE status='0'", null);
        if (mCursor.moveToFirst()) {
            do {
                unreadcount++;
            } while (mCursor.moveToNext());
        }
        mCursor.close();
        if(unreadcount == 0)
        {
            countofchat.setText("");
        }
        else
        {
            countofchat.setText(""+unreadcount);
        }

    }


    private void ShareTripPopUp() {
        final Dialog dialog = new Dialog(TrackRidePage.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.share_trip_popup_new);

        Et_share_trip_mobileno = (EditText) dialog.findViewById(R.id.sharetrip_mobilenoEt);
        Et_share_trip_mobileno.setHint(getkey("share_label_alert_enter_your_mobileno_with_code"));
        TextView Bt_Submit = (TextView) dialog.findViewById(R.id.confirm_lable_tv);
        Bt_Submit.setText(getkey("confirm_lable"));
        ImageView Bt_Cancel = (ImageView) dialog.findViewById(R.id.close_image);

        TextView Tv_share = (TextView) dialog.findViewById(R.id.sharemytrip_text);
        Tv_share.setText(getkey("share_mytrip_lable1"));

        TextView su = (TextView) dialog.findViewById(R.id.su);
        su.setText(getkey("privacy_policy_trip"));

        TextView sharemytrip_text1 = (TextView) dialog.findViewById(R.id.sharemytrip_text1);
        sharemytrip_text1.setText(getkey("insertcountry_code_lable"));


        final TextView Tv_countryCode = (TextView) dialog.findViewById(R.id.countrycode_tv);
        Tv_countryCode.setText(ScountryCode);

        Bt_Submit.setTypeface(tf);

        Tv_countryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                Tv_countryCode.setText(dialCode);
                CountryCode = dialCode;
                // String drawableName = "flag_"+ code.toLowerCase(Locale.ENGLISH);
                // Iv_flag.setImageResource(getResId(drawableName));
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Tv_countryCode.getWindowToken(), 0);
            }
        });

        Bt_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Et_share_trip_mobileno.setText(Html.fromHtml(Et_share_trip_mobileno.getText().toString()).toString());

                if (!isValidPhoneNumber(Et_share_trip_mobileno.getText().toString())) {
                    erroredit(Et_share_trip_mobileno,getkey("share_label_alert_mobileno"));
                } else {
                    if (isInternetPresent) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("ride_id", Str_ride_id);
                        jsonParams.put("mobile_no", Et_share_trip_mobileno.getText().toString().trim());
                        jsonParams.put("country_code", Tv_countryCode.getText().toString().trim());

                        share_trip_postRequest_MyRides(Iconstant.share_trip_url, jsonParams);
                        dialog.dismiss();
                    } else {
                        Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                    }

                }


            }
        });
        Bt_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }


    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(TrackRidePage.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    //----------------------------------Share Trip post reques------------------------
    private void share_trip_postRequest_MyRides(String url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(TrackRidePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        System.out.println("------------- Share Trip url----------------" + url);
        System.out.println("------------- Share Trip jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(TrackRidePage.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("share trip", response);

                String Str_status = "", Str_response = "";
                System.out.println("------------------sharetrip response-------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    Str_response = object.getString("response");

                    if (Str_status.equalsIgnoreCase("1")) {
                        if (dialog1 != null) {
                            dialog1.dismiss();
                        }
                        Alert(getkey("action_success"), Str_response);

                    } else {
                        Alert(getkey("action_error"), Str_response);

                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {

                dialog.dismiss();

            }


        });

    }


    //-------------------prabu-----------------------

    private void DeleteRideRequest(String Url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        System.out.println("------------muruga trackingPage--Timer Delete Ride url-------------------" + Url);

        mRequest = new ServiceRequest(TrackRidePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("----------muruga trackingPage----Timer Delete Ride reponse-------------------" + response);

                try {

                    final JSONObject json = new JSONObject(response);
                    JSONArray routeArray = json.getJSONArray("routes");

                    if (routeArray.length() > 0) {
                        JSONObject routes = routeArray.getJSONObject(0);

                        JSONArray newTempARr = routes.getJSONArray("legs");
                        JSONObject newDisTimeOb = newTempARr.getJSONObject(0);

                        JSONObject distOb = newDisTimeOb.getJSONObject("distance");
                        JSONObject timeOb = newDisTimeOb.getJSONObject("duration");

                        if (("Confirmed".equalsIgnoreCase(Str_ride_status)) || ("Arrived".equalsIgnoreCase(Str_ride_status))) {
                            Tv_Distance.setText(distOb.getString("text").toUpperCase() + " " + getkey("away"));
                            Tv_time.setText(getkey("in") +" "+ timeOb.getString("text").toUpperCase());
                        } else {
                            Tv_Distance.setText(distOb.getString("text").toUpperCase());
                            Tv_time.setText(getkey("in")+" " + timeOb.getString("text").toUpperCase());
                        }


                        System.out.println("----------muruga trackingPage--Diatance---------------" + distOb.getString("text"));
                        System.out.println("---------muruga trackingPage---Time---------------" + timeOb.getString("text"));

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
            }
        });
    }

    private void SOS() {
        System.out.println("----------------muruga trackingPage sos-----------------");
        final Dialog dialog = new Dialog(TrackRidePage.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.sos_popup1);

        ImageView bkng_closeImge = (ImageView) dialog.findViewById(R.id.label_close_imageview);
        bkng_closeImge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView cnformTv = (TextView) dialog.findViewById(R.id.tv_call_emergency);
        cnformTv.setText(getkey("send"));

        TextView emee = (TextView) dialog.findViewById(R.id.emee);
        emee.setText(getkey("trip_emergency"));

        TextView lemergency_textview = (TextView) dialog.findViewById(R.id.lemergency_textview);
        lemergency_textview.setText(getkey("are_you__emergency_new"));
        cnformTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", UserID);
                    jsonParams.put("latitude", sCurLat);
                    jsonParams.put("longitude", sCurLon);
                    postRequest_Sos(Iconstant.sos_alert_url, jsonParams);
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }
            }
        });
        dialog.show();
    }

    private void SOS1() {
        System.out.println("----------------muruga trackingPage sos-----------------");
        final Dialog dialog = new Dialog(TrackRidePage.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(R.layout.sos_popup2);
        dialog.show();
        TextView tv_callassist = (TextView) dialog.findViewById(R.id.tv_callassist);
        tv_callassist.setText(strCallassist);

        TextView lemergency_textview = (TextView) dialog.findViewById(R.id.lemergency_textview);
        lemergency_textview.setText(getkey("sos__emergency_norecordnew"));
        TextView lemergency_assitance = (TextView) dialog.findViewById(R.id.lemergency_assitance);
        lemergency_assitance.setText(getkey("sos__emergency_assitance"));



        ImageView bkng_closeImge = (ImageView) dialog.findViewById(R.id.label_close_imageview);
        bkng_closeImge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        RelativeLayout callAssistance = (RelativeLayout) dialog.findViewById(R.id.lyt_assistant_call);
        callAssistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + strCallassist));
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + strCallassist));
                    startActivity(intent);
                }
                dialog.dismiss();
            }
        });

    }

    //------------------------------------------------------------------------

   


//----------------------------Run time permission for call--------------------------------------------

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODES);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_phone_number));
                    startActivity(intent);
                }
                break;
            case PERMISSION_REQUEST_CODES:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + strCallassist));
                    startActivity(intent);
                }
                break;


        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mapHandler != null) {
            mapHandler.removeCallbacks(mapRunnable);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void doThis(ChatIntentServiceResult intentServiceResult )
    {
        getchatunreadcount(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stoptimertask();
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        unregisterReceiver(refreshReceiver);
        if (mapHandler != null) {
            mapHandler.removeCallbacks(mapRunnable);
        }
        Log.d(TAG, "onDestroy: ");
        if (mGoogleApiClient.isConnected()) {
            Log.d(TAG, "onDestroy: 2");
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(TrackRidePage.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    public Bitmap StringToBitMap(String encodedString) {
        System.out.println("base 64" + encodedString);
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }


    private void addIcon(String text, LatLng position, int flag) {
        View marker = null;
        TextView numTxt;
        if (flag == 1) {
            marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout1, null);
            numTxt = (TextView) marker.findViewById(R.id.num_txt);
        } else {
            marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.infowindow_track, null);
            numTxt = (TextView) marker.findViewById(R.id.username);
        }

        numTxt.setText(text);
        MarkerOptions markerOptions = new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))).
                        position(position);
        googleMap.addMarker(markerOptions);
    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


    class LoadBitmap extends AsyncTask<String, String, String> {
        Bitmap bmps;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... aurl) {
            String car64 = "";

            try {
                URL url = new URL(aurl[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                bmps = BitmapFactory.decodeStream(input);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";

        }

        @Override
        protected void onPostExecute(String unused) {

            String s = BitMapToString(bmps);
            System.out.println("session bitmap" + s);
            session.setVehicle_BitmapImage(s);
            bmp = bmps;


        }


    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    private void profileDialog() {
        DriverProfileAdapter adapter;
        final MaterialDialog dialog = new MaterialDialog(TrackRidePage.this);
        View view = LayoutInflater.from(TrackRidePage.this).inflate(R.layout.driver_profile_dialog, null);

        final TextView Tv_close = (TextView) view.findViewById(R.id.driver_profile_close_textview);
        CustomTextView Tv_driver_name = (CustomTextView) view.findViewById(R.id.driver_profile_driver_name);
        ExpandableHeightListView overview_listView = (ExpandableHeightListView) view.findViewById(R.id.driver_profile_expandable_listview);
        RoundedImageView Iv_profileImage = (RoundedImageView) view.findViewById(R.id.driver_profile_imageview);
        RatingBar Rb_driver_rating = (RatingBar) view.findViewById(R.id.driver_profile_ratingbar);
//        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
        TextView driver_profile_driver_profile_lable = (TextView) view.findViewById(R.id.driver_profile_driver_profile_lable);

       driver_profile_driver_profile_lable.setText(getkey("profile_label_driverprofile"));

        Tv_close.setText(getkey("profile_label_close"));

        Drawable drawable = Rb_driver_rating.getProgressDrawable();
        drawable.setColorFilter(Color.parseColor("#FF9900"), PorterDuff.Mode.SRC_ATOP);
        CustomTextView Tv_listempty = (CustomTextView) view.findViewById(R.id.driver_profile_list_empty_textview);

        Tv_driver_name.setText(trackpojo.getResponse().getDriver_profile().getDriver_name());
        Picasso.with(TrackRidePage.this)
                .load(trackpojo.getResponse().getDriver_profile().getDriver_image())
                .placeholder(R.drawable.no_user_img)   // optional
                .error(R.drawable.no_user_img)      // optional
                .into(Iv_profileImage);
        Rb_driver_rating.setRating(Float.parseFloat(trackpojo.getResponse().getDriver_profile().getDriver_review()));


        if (itemList.size() > 0) {
            Tv_listempty.setVisibility(View.GONE);
            adapter = new DriverProfileAdapter(TrackRidePage.this, itemList);
            overview_listView.setAdapter(adapter);
        } else {
            Tv_listempty.setVisibility(View.VISIBLE);
        }


        Tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.setBackground(getResources().getDrawable(R.drawable.transparant_bg));
        dialog.setView(view).show();
    }

    private void checkrideacceptornot(String Url) {




        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", Str_ride_id);

        mRequest = new ServiceRequest(TrackRidePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject response_object = object.getJSONObject("response");
                            if (response_object.length() > 0) {

                                Object check_driver_profile_object = response_object.get("driver_profile");
                                if (check_driver_profile_object instanceof JSONObject) {
                                    JSONObject driver_profile_object = response_object.getJSONObject("driver_profile");
                                    if (driver_profile_object.length() > 0) {
                                        if(!savingStr_ride_status.equals(""))
                                        {
                                            if(!savingStr_ride_status.equals(driver_profile_object.getString("ride_status")))
                                            {
                                                if(dialog != null && !dialog.isShowing())
                                                {
                                                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                                                    jsonParams.put("ride_id", Str_ride_id);
                                                    jsonParams.put("user_id", UserID);
                                                    postRequest_TrackRide(Iconstant.track_your_ride_url, jsonParams);
                                                }

                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {

            }
        });
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        checkrideacceptornot(Iconstant.track_your_ride_url);
                        //Toast.makeText(getApplicationContext(),"hitted",Toast.LENGTH_LONG).show();
                    }
                });
            }
        };
    }


    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}

