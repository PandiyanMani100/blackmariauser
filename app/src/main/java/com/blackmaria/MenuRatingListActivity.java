package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.adapter.MenuHomeListRatingAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.MenuHomeratingPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by user144 on 9/2/2017.
 */

public class MenuRatingListActivity extends ActivityHockeyApp {
    private SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
LanguageDb mhelper;
    private String UserID = "";
    ArrayList<MenuHomeratingPojo> itemlist;
    private boolean isDataAvailable = false;
    private String rideId1 = "";
    ImageView Iv_previous, Iv_next;
    String perPage = "5";
    private int currentPage = 1;
    MenuHomeListRatingAdapter adapter;
    private ListView listview;
    TextView noreview_tv;
    private ImageView img_back;
    private RefreshReceiver refreshReceiver;
    private RoundedImageView img_page_logo;
    private TextView totalreviewscount, yearmonth;


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(MenuRatingListActivity.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(MenuRatingListActivity.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_ride_rating_list_constrain);
        mhelper = new LanguageDb(this);
        Initialize();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(MenuRatingListActivity.this, MenuUserratingHOme.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        Iv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Iv_previous.setVisibility(View.VISIBLE);
                currentPage = currentPage + 1;
                postData();
            }
        });

        Iv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                currentPage = currentPage - 1;
                postData();

            }
        });
    }

    private void Initialize() {
        session = new SessionManager(MenuRatingListActivity.this);
        cd = new ConnectionDetector(MenuRatingListActivity.this);
        isInternetPresent = cd.isConnectingToInternet();


        itemlist = new ArrayList<MenuHomeratingPojo>();
        totalreviewscount = findViewById(R.id.totalreviewscount);
        yearmonth = findViewById(R.id.yearmonth);
        img_page_logo = findViewById(R.id.img_page_logo);
        Iv_next = (ImageView) findViewById(R.id.next_page);
        Iv_previous = (ImageView) findViewById(R.id.prev_page);
        listview = (ListView) findViewById(R.id.menulistview);
        img_back = (ImageView) findViewById(R.id.img_back);// get user data from session
        noreview_tv= (TextView) findViewById(R.id.noreview_tv);
        noreview_tv.setText(getkey("no_reviews_yet"));
        TextView rating_page_driver_cartype_textview= (TextView) findViewById(R.id.rating_page_driver_cartype_textview);
        rating_page_driver_cartype_textview.setText(getkey("reviews"));



        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);

        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        String month_name = month_date.format(c.getTime());


        yearmonth.setText(month_name + " " + year);


        Picasso.with(MenuRatingListActivity.this)
                .load(session.getUserImageUpdate())
                .into(img_page_logo);

        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        registerReceiver(refreshReceiver, intentFilter);

        postData();
    }

    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        if (isInternetPresent) {
            postRequest_MenuratingHome(Iconstant.menupage_ratinglist, jsonParams);
        } else {
            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
        }
    }

    //-----------------------Rating List Post Request-----------------
    private void postRequest_MenuratingHome(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(MenuRatingListActivity.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));


        System.out.println("-------------MenuratingHomeList  Url----------------" + Url);


        mRequest = new ServiceRequest(MenuRatingListActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String userName = "", userEmail = "", userGender = "", userAge = "", userImage = "", userAvgReview = "", driverName = "", driveromments = "", DriverImage = "", reviewDate1 = "";
                Log.e("rateing", response);

                System.out.println("-------------MenuratingHomeList Response----------------" + response);

                String Sstatus = "", str_NextPage;
                String SRating_status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject jsonObj = object.getJSONObject("response");
                    currentPage = Integer.parseInt(jsonObj.getString("current_page"));
                    str_NextPage = jsonObj.getString("next_page");
                    if (str_NextPage.equalsIgnoreCase("")) {
                        Iv_next.setVisibility(View.GONE);
                    } else {
                        Iv_next.setVisibility(View.VISIBLE);
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONArray objLastreview1 = jsonObj.getJSONArray("review");
                        if (objLastreview1.length() > 0) {
                            itemlist.clear();
                            for (int i = 0; i < objLastreview1.length(); i++) {
                                MenuHomeratingPojo pojo = new MenuHomeratingPojo();
                                JSONObject objLastreview = objLastreview1.getJSONObject(i);
                                pojo.setDriverName(objLastreview.getString("driver_name"));
                                if(objLastreview.getString("comments").startsWith("Optional"))
                                {
                                    pojo.setDriveromments(getkey("nocomments"));
                                }
                                else
                                {
                                    pojo.setDriveromments(objLastreview.getString("comments"));
                                }

                                pojo.setDriverImage(objLastreview.getString("image"));
                                pojo.setRideId1(objLastreview.getString("ride_id"));
                                pojo.setReviewDate1(objLastreview.getString("review_date"));
                                itemlist.add(pojo);
                            }
                            totalreviewscount.setText(getkey("total") + " " +objLastreview1.length() +" " +getkey("review"));
                            isDataAvailable = true;
                        } else {
                            isDataAvailable = false;
                        }
                    }

                    if (Sstatus.equalsIgnoreCase("1") && isDataAvailable) {
                        if (currentPage == 1) {
                            Iv_previous.setVisibility(View.GONE);
                        } else {
                            Iv_previous.setVisibility(View.VISIBLE);
                        }

                        if (SRating_status.equalsIgnoreCase("1")) {

                            Toast.makeText(getApplicationContext(), getkey("allready_submited"), Toast.LENGTH_SHORT).show();

                        } else {
                            adapter = new MenuHomeListRatingAdapter(MenuRatingListActivity.this, itemlist, UserID);
                            listview.setAdapter(adapter);
                        }

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(MenuRatingListActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
