package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.subclass.ActivitySubClass;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

public class TrackArrivePage extends ActivitySubClass {


//    private TextView Tv_comp_wating;
    private LinearLayout complementry_Tv;
    private TextView tv_carno,tv_freewaiting,T_ok;
    private String Str_ride_id = "",Scarno="",Sfreewaitingtime="";
    private RefreshReceiver refreshReceiver;
    LanguageDb mhelper;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    String UserID = "", selectdCarmage;
    Dialog dialog;
    ImageView trackdriver_carphoto;
    private ServiceRequest mRequest;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip")) {

                Intent intent1 = new Intent(TrackArrivePage.this, TrackRidePage.class);
                intent1.putExtra("ride_id", Str_ride_id);
                startActivity(intent1);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();

            } else if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {

                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(TrackArrivePage.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(TrackArrivePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", Str_ride_id);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(TrackArrivePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", Str_ride_id);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }

                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.arrivenow_layout);
        mhelper= new LanguageDb(this);
        initialize();


        complementry_Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, String> jsonParams = new HashMap<String, String>();

                jsonParams.put("ride_id", Str_ride_id);
                jsonParams.put("user_id", UserID);
                jsonParams.put("mode", "start");
                //"584aa27ccae2aa741a00002f");

                System.out.println("-------------Complementry waiting jsonParams----------------" + jsonParams);
                if (isInternetPresent) {
                    postRequestComplementryWaiting(Iconstant.complementry_waiting, jsonParams);
                } else {
                    Alert(getkey("action_error"), getkey("alert_nointernet"));

                }


            }
        });

        T_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TrackArrivePage.this, TrackRidePage.class);
                intent.putExtra("ride_id", Str_ride_id);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();

            }
        });

    }

    private void postRequestComplementryWaiting(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(TrackArrivePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        mRequest = new ServiceRequest(TrackArrivePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Complementry waiting Response----------------" + response);

                String Sstatus = "", ticket_no = "", Scurrency_code = "", user_image = "", Scurrentbalance = "", complWaitingTime = "", Smessage = "", Scurrency = "", user_name = "", user_location = "", user_id = "", member_since = "", Date = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");
                        complWaitingTime = object.getString("comp_waiting_time");
                        session.setFreeWaitingTime(complWaitingTime);
                        String message =getkey("youhave") +" " +complWaitingTime + getkey("minfreewait")+" "+ getkey("willcharge");
                        AlertWaitingTIme(getkey("action_success"), message);
                        dialogDismiss();
                    } else {
                        Smessage = object.getString("response");
                        Alert(getkey("action_error"), Smessage);
                        dialogDismiss();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogDismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void initialize() {

        session = new SessionManager(TrackArrivePage.this);
        cd = new ConnectionDetector(TrackArrivePage.this);
        isInternetPresent = cd.isConnectingToInternet();

        TextView Tv_oks = findViewById(R.id.arrive_trip_ok_textview);
        Tv_oks.setText(getkey("arrive_now"));
        TextView subt = findViewById(R.id.subt);
        subt.setText(getkey("needextratime"));
        TextView waa = findViewById(R.id.waa);
        waa.setText(getkey("waitss"));



        Tv_oks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TrackArrivePage.this, TrackRidePage.class);
                intent.putExtra("ride_id", Str_ride_id);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();

            }
        });
        T_ok = findViewById(R.id.Tv_ok);
        T_ok.setText(getkey("action_ok"));
        tv_carno = findViewById(R.id.tv_carno);
        tv_freewaiting = findViewById(R.id.tv_freewaitingtime);

        ImageView Rl_drawer = (ImageView) findViewById(R.id.loading);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);

        complementry_Tv = findViewById(R.id.complementry_yes_textview);
        HashMap<String, String> carImage = session.getSelectedCarCatImage();
        selectdCarmage = carImage.get(SessionManager.SELECTED_CAR_IMAGE);
        trackdriver_carphoto = findViewById(R.id.trackdriver_carphoto);
        try {
            if(selectdCarmage != null)
            Picasso.with(getApplicationContext()).load(String.valueOf(selectdCarmage)).into(trackdriver_carphoto);
        }catch (NullPointerException e){

        }
        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip");
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);
//        ImageView Rl_drawer = (ImageView) findViewById(R.id.drawer_icon);
//       /* GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(Rl_drawer);
//        Glide.with(this).load(R.drawable.alertgif).into(imageViewTarget);*/
//
//        final Animation animation = new AlphaAnimation(1, 0);
//        animation.setDuration(500);
//        animation.setInterpolator(new LinearInterpolator());
//        animation.setRepeatCount(Animation.INFINITE);
//        animation.setRepeatMode(Animation.REVERSE);
//        Rl_drawer.startAnimation(animation);


        Intent intent = getIntent();
        if (intent.hasExtra("ride_id")) {
            Str_ride_id = intent.getStringExtra("ride_id");
            Scarno = intent.getStringExtra("car_no");
            Sfreewaitingtime = intent.getStringExtra("freewatingtime");
            if (intent.hasExtra("model")) {
                tv_carno.setText(getkey("carnos")+Scarno+"\n"+intent.getStringExtra("model"));
            }
            else
            {
                tv_carno.setText(getkey("carnos")+Scarno);
            }

            tv_freewaiting.setText(getkey("youhave")+" " +Sfreewaitingtime+" " +getkey("minfreewait"));
        }


    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(TrackArrivePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void AlertWaitingTIme(String title, String alert) {

        final PkDialog mDialog = new PkDialog(TrackArrivePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent waitingIntent = new Intent(TrackArrivePage.this, WaitingTimePage.class);
                waitingIntent.putExtra("ride_id", Str_ride_id);
                waitingIntent.putExtra("arivepage", "page");
                startActivity(waitingIntent);
                Calendar calendar = Calendar.getInstance();
                session.setWaitedTime(0, 0, "0", String.valueOf(calendar.getTime()));
                finish();

                overridePendingTransition(R.anim.enter, R.anim.exit);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
