package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.blackmaria.adapter.HomeMenuListAdapter;
import com.blackmaria.hockeyapp.ActionBarActivityHockeyApp;
import com.blackmaria.newdesigns.profileInfo.ProfileConstain_activity;
import com.blackmaria.newdesigns.Homepage_fragment;
import com.blackmaria.pojo.clickdisabledashboardpojo;
import com.blackmaria.pojo.setuserimageindashboard;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.ExpandableHeightGridView;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.viewmodel.RegistrationTerms_Constrain;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.xmpp.XmppService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

public class Navigation_new extends ActionBarActivityHockeyApp implements View.OnClickListener,MyDialogFragment.Communicator {
    ActionBarDrawerToggle actionBarDrawerToggle;
    public static DrawerLayout drawerLayout;
    private static RelativeLayout mDrawer;
    private Context context;
    private ExpandableHeightGridView mDrawerList;
    private ImageView Iv_home;
    private RelativeLayout Rl_coupons, Rl_favourits, Rl_referal;
LanguageDb mhelper;

    RelativeLayout Bt_logout;
    RatingBar userRatingBar;
    // private Button Bt_be_partner, Bt_feedback;
    private static HomeMenuListAdapter mMenuAdapter;
    private String[] title;
    private int[] icon;
    int count = 1;
    ImageView fileimage;
    RelativeLayout drawer_fileimage1, drawer_fileimage2, drawer_fileimage3;
    private SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private String UserID = "", userRating;
    AppInfoSessionManager SappInfo_Session;
    private RelativeLayout tv_referalltext;
    BroadcastReceiver updateReceiver;
    private RefreshReceiver refreshReceiver;
    String referalCode = "";
    String referalCodeUser = "", phoneNumberReferal = "", ReferalStatus = "";
    private RelativeLayout Tv_Close_Account;
TextView changelanguagee;
    public static Activity activity;

    private RoundedImageView iv_userbg;

    private LinearLayout lv_profile, lv_getpaid, lv_coupuns, lv_myplaces, lv_fav, lv_ratecard, lv_aboutus, lv_terms, _lv_close;
    private RelativeLayout rl_drivewihtus;
    private TextView tv_logout, tv_coupon_countt;

    @Override
    public void message(String data) {
        String languageToLoad  = data; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        session.setCurrentlanguage(data);

        Intent intent = new Intent(Navigation_new.this, SplashPage1.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.close.myaccount")) {
                if (NewProfile_Page.newProfile_page != null) {
                    NewProfile_Page.newProfile_page.finish();
                }
                finish();
            } else if (intent.getAction().equals("com.blackmaria.couponupdate")) {
                String coupon = intent.getStringExtra("coupon");
                updateCoupencount(coupon);
            }
        }
    }

    public Navigation_new() {
        activity = Navigation_new.this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.dasboard_sidemenu);
        mhelper = new LanguageDb(this);
        EventBus.getDefault().register(this);

        activity = Navigation_new.this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }

        cd = new ConnectionDetector(Navigation_new.this);
        isInternetPresent = cd.isConnectingToInternet();

        // -----code to refresh drawer using broadcast receiver----
        // -
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.close.myaccount");
        intentFilter.addAction("com.blackmaria.couponupdate");
        registerReceiver(refreshReceiver, intentFilter);
        changelanguagee = (TextView) findViewById(R.id.changelanguagee);


        TextView getafriends= (TextView) findViewById(R.id.getafriends);
        getafriends.setText(getkey("get_a_friends"));

        TextView aboutss= (TextView) findViewById(R.id.aboutss);
        aboutss.setText(getkey("about_uss"));

        TextView terms_of_use= (TextView) findViewById(R.id.terms_of_use);
        terms_of_use.setText(getkey("terms_of_use"));

        TextView closeacc= (TextView) findViewById(R.id.closeacc);
        closeacc.setText(getkey("closeaccount"));

        TextView mypro= (TextView) findViewById(R.id.mypro);
        mypro.setText(getkey("myprofile"));


        TextView my_places= (TextView) findViewById(R.id.my_places);
        my_places.setText(getkey("my_places"));

        TextView navi_myratings_menu= (TextView) findViewById(R.id.navi_myratings_menu);
        navi_myratings_menu.setText(getkey("navi_myratings_menu"));

        TextView coupons_lable_with_space= (TextView) findViewById(R.id.coupons_lable_with_space);
        coupons_lable_with_space.setText(getkey("coupons_lable_with_space"));

        TextView rates_card_lable= (TextView) findViewById(R.id.rates_card_lable);
        rates_card_lable.setText(getkey("rates_card_lable"));




        ImageView Rl_drawer = (ImageView) findViewById(R.id.loading);
       /*GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(Rl_drawer);
        Glide.with(this).load(R.drawable.alertgif).into(imageViewTarget);*/
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);


        SappInfo_Session = new AppInfoSessionManager(Navigation_new.this);
        context = getApplicationContext();
        session = new SessionManager(Navigation_new.this);
        drawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mDrawer = (RelativeLayout) findViewById(R.id.drawer);


        actionBarDrawerToggle = new ActionBarDrawerToggle(Navigation_new.this, drawerLayout, R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.syncState();
            }
        });

        HashMap<String, String> app = SappInfo_Session.getAppInfo();
        userRating = app.get(SappInfo_Session.KEY_USER_RATING);

        lv_profile = findViewById(R.id.lv_profile);
        rl_drivewihtus = findViewById(R.id.rl_drivewihtus);
        lv_getpaid = findViewById(R.id.lv_getpaid);
        lv_coupuns = findViewById(R.id.lv_coupens);
        lv_myplaces = findViewById(R.id.lv_myplaces);
        lv_fav = findViewById(R.id.lv_fav);
        lv_ratecard = findViewById(R.id.lv_ratescard);
        lv_aboutus = findViewById(R.id.lv_aboutus);
        lv_terms = findViewById(R.id.lv_terms);
        _lv_close = findViewById(R.id.lv_closeaccount);
        tv_logout = findViewById(R.id.tv_logout);
        tv_logout.setText(getkey("logout"));
        tv_coupon_countt = findViewById(R.id.tv_coupon_count);
        iv_userbg = findViewById(R.id.iv_userbg);

        try {
            if (getIntent().hasExtra("image")) {
                String image = getIntent().getStringExtra("image");
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.new_no_user_img)
                        .error(R.drawable.new_no_user_img)
                        .diskCacheStrategy(DiskCacheStrategy.ALL);
                Glide.with(this).load(image).apply(options).into(iv_userbg);
            } else {
                String userImage = session.getUserImageUpdate();
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.new_no_user_img)
                        .error(R.drawable.new_no_user_img)
                        .diskCacheStrategy(DiskCacheStrategy.ALL);
                Glide.with(this).load(userImage).apply(options).into(iv_userbg);
            }

        } catch (NullPointerException e) {
        }
        Iv_home = (ImageView) findViewById(R.id.iv_back);

        rl_drivewihtus.setOnClickListener(this);
        lv_profile.setOnClickListener(this);
        lv_getpaid.setOnClickListener(this);
        lv_coupuns.setOnClickListener(this);
        lv_myplaces.setOnClickListener(this);
        lv_fav.setOnClickListener(this);
        lv_ratecard.setOnClickListener(this);
        lv_aboutus.setOnClickListener(this);
        lv_terms.setOnClickListener(this);
        tv_logout.setOnClickListener(this);
        _lv_close.setOnClickListener(this);


        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        HashMap<String, String> user1 = session.getReferalStatus();
        ReferalStatus = user1.get(SessionManager.KEY_IS_REFERAL_STATUS);


        if (savedInstanceState == null) {
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.content_frame, new Homepage_fragment());
            tx.commit();
            System.out.println("*************************savedInstanceState*********************NULL" + savedInstanceState);
        }


        Iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    drawerLayout.closeDrawer(mDrawer);
                    clickdisabledashboardpojo pojo = new clickdisabledashboardpojo();
                    pojo.setIsenabled(false);
                    EventBus.getDefault().post(pojo);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        String languageToLoad  = session.getCurrentlanguage(); // your language
        if(languageToLoad.equals("en"))
        {
            changelanguagee.setText(getkey("change_language")+getkey("english"));
        }
        else if(languageToLoad.equals("ta"))
        {
            changelanguagee.setText(getkey("change_language")+getkey("india_tamil"));
        }
        else
        {
            changelanguagee.setText(getkey("change_language")+getkey("indonesian"));
        }
        changelanguagee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyDialogFragment addPhotoBottomDialogFragment =
                        MyDialogFragment.newInstance();
                addPhotoBottomDialogFragment.show(getSupportFragmentManager(),
                        "add_photo_dialog_fragment");

             /*   FragmentManager manager = getFragmentManager();
                MyDialogFragment mydialog = new MyDialogFragment();
                mydialog.show(manager, "mydialog");*/
            }
        });


        _lv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccoutnClosePopup();
            }
        });


        enableSwipeDrawer();

        postData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setimage(setuserimageindashboard obj) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.new_no_user_img)
                .error(R.drawable.new_no_user_img)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(this).load(obj.getImage()).apply(options).into(iv_userbg);
    }


    private void postData() {
        HashMap<String, String> user = session.getUserDetails();
        String UserId = user.get(SessionManager.KEY_USERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", UserId);
        jsonParams.put("type", "notification");
        jsonParams.put("page", String.valueOf("1"));
        jsonParams.put("perPage", "5");
        if (isInternetPresent) {
            postRequest_walletMoney(Iconstant.mesggaeg_url, jsonParams);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));

        }
    }

    private void postRequest_walletMoney(String Url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(Navigation_new.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));


        System.out.println("-------------Message  Url----------------" + Url);


        mRequest = new ServiceRequest(Navigation_new.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Message  Response----------------" + response);

                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "", ScurrencySymbol = "", Str_total_amount = "", Str_total_transaction = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");

                        if (response_object.length() > 0) {
                            String totlanotification = response_object.getString("total_record");
                            session.setInfoBadge(totlanotification);
//                            MainPage.navigationNotifyChange();

                        }
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });

    }

    private void AccoutnClosePopup() {
        final Dialog dialog = new Dialog(Navigation_new.this, R.style.DialogSlideAnim2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.accountclosepopup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Button closeImage = (Button) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        Button InRTv = (Button) dialog.findViewById(R.id.custom_dialog_library_cancel_button);

        TextView custom_title = (TextView) dialog.findViewById(R.id.custom_title);
        custom_title.setText(getkey("titlecloseaccount"));

        TextView custom_dialog_library_message_textview1 = (TextView) dialog.findViewById(R.id.custom_dialog_library_message_textview1);
        custom_dialog_library_message_textview1.setText(getkey("info_lable"));

        TextView custom_dialog_library_message_textview = (TextView) dialog.findViewById(R.id.custom_dialog_library_message_textview);
        custom_dialog_library_message_textview.setText(getkey("content"));

        closeImage.setText(getkey("drawer_lable_logout_no"));
        InRTv.setText(getkey("drawer_lable_logout_yes"));


        InRTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent closeIntent = new Intent(Navigation_new.this, DeActivationAccountStatementLoading.class);
                startActivity(closeIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                dialog.dismiss();
            }
        });

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void showReferralAlert() {

        //--------Adjusting Dialog width-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen

        final Dialog referralDialog = new Dialog(Navigation_new.this, R.style.DialogAnimation2);
        referralDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        referralDialog.setContentView(R.layout.alert_enter_referral_code);
        referralDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        referralDialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        final EditText Ed_referral = (EditText) referralDialog.findViewById(R.id.edt_referral_code);
        final Button Btn_confirm = (Button) referralDialog.findViewById(R.id.btn_confirm);
        final ImageView Iv_close = (ImageView) referralDialog.findViewById(R.id.img_close);

        Btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referalCodeUser = Ed_referral.getText().toString().trim();


                if (referalCodeUser.length() > 0) {
                    Ed_referral.setError(null);
                    if (isInternetPresent) {
                        if (Ed_referral.getText().toString().equalsIgnoreCase("")) {
                            erroredit(Ed_referral, getkey("login_page_alert_referal"));
                        } else {
                           /* // close keyboard
                            InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            mgr.hideSoftInputFromWindow(Et_Otp1.getWindowToken(), 0);*/
                            PostRequest_referal(Iconstant.referal_url);
                            System.out.println("referal---------" + Iconstant.referal_url);

                        }
                    } else {
                        Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                    }
                    referralDialog.dismiss();
                } else {
                    Ed_referral.setError(getkey("otp_page_referral_error"));
                }

            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referralDialog.dismiss();
            }
        });

        referralDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //tv_referalltext.setClickable(true);
                CloseKeyboardNew();
            }
        });

        Ed_referral.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(Ed_referral);
                }
                return false;
            }
        });

        referralDialog.show();
        //   tv_referalltext.setClickable(false);

    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PostRequest_referal(String referal_url) {
        final Dialog dialog = new Dialog(Navigation_new.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_verifying"));
        System.out.println("--------------ReferalSubmit  url-------------------" + referal_url);
//        HashMap<String, String> jsonParams = new HashMap<String, String>();
//        jsonParams.put("user_id", UserID);
//        jsonParams.put("referral_code", referalCodeUser);
//        System.out.println("------Referal jsonParams---------" + jsonParams);
        HashMap<String, String> info = session.getUserDetails();
        String countrycode = info.get(SessionManager.KEY_COUNTRYCODE);
        String Sphone = info.get(SessionManager.KEY_PHONENO);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("country_code", countrycode);
        jsonParams.put("phone_number", Sphone);
        jsonParams.put("code", referalCodeUser);


        mRequest = new ServiceRequest(Navigation_new.this);
        mRequest.makeServiceRequest(referal_url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------Referal reponse-------------------" + response);
                String Sstatus = "", Smessage = "", userName = "", userImage = "", refPer = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");
//
                        if (dialog != null) {
                            dialog.dismiss();
                            PostRequest_referal_submit(Iconstant.referal_code_submit_url);
                        }
                    } else {
                        Smessage = object.getString("response");
                        referalCodeUser = "";
                        Alert(getkey("alert"), Smessage);
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void PostRequest_referal_submit(String referal_url) {
        final Dialog dialog = new Dialog(Navigation_new.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_verifying"));
        System.out.println("--------------ReferalSubmit  url-------------------" + referal_url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("referral_code", referalCodeUser);
        System.out.println("------Referal jsonParams---------" + jsonParams);

        mRequest = new ServiceRequest(Navigation_new.this);
        mRequest.makeServiceRequest(referal_url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------Referal reponse-------------------" + response);
                String Sstatus = "", Smessage = "", userName = "", userImage = "", refPer = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        tv_referalltext.setVisibility(View.GONE);
                        session.setReferalStatus("yes");
                        Smessage = object.getString("response");
                        Alert(getkey("action_success"), Smessage);
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    } else {
                        tv_referalltext.setVisibility(View.VISIBLE);
                        Smessage = object.getString("response");
                        referalCodeUser = "";
                        Alert(getkey("alert"), Smessage);
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {


        if (isInternetPresent) {


            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();


            if (view == lv_profile) {

                Intent intent_profile = new Intent(Navigation_new.this, ProfileConstain_activity.class);
                intent_profile.putExtra("frommenu", "1");
                startActivity(intent_profile);
               // f//inish();
//                finish();

            } else if (view == rl_drivewihtus) {

                Intent intent_bepartner = new Intent(Navigation_new.this, RegistrationTerms_Constrain.class);
                startActivity(intent_bepartner);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            } else if (view == lv_getpaid) {

                Intent intent = new Intent(Navigation_new.this, CloudMoneyHomePage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (view == lv_coupuns) {

                Intent intent_coupon = new Intent(Navigation_new.this, CouponPage.class);
                intent_coupon.putExtra("str_page", getResources().getString(R.string.profile_label_menu));
                startActivity(intent_coupon);
                //  finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            } else if (view == lv_myplaces) {

                Intent intent_favourite = new Intent(Navigation_new.this, FavoriteList.class);
                intent_favourite.putExtra("str_page", getkey("profile_label_menu"));
                startActivity(intent_favourite);
                //  finish();

            } else if (view == lv_ratecard) {

                Intent intent_sh = new Intent(Navigation_new.this, RateCardPage.class);
                startActivity(intent_sh);
                // finish();

            } else if (view == lv_fav) {

                Intent intent_sh = new Intent(Navigation_new.this, MenuUserratingHOme.class);
                intent_sh.putExtra("frompage", "Home");
                startActivity(intent_sh);

            } else if (view == lv_aboutus) {

                Intent intent1 = new Intent(Navigation_new.this, AboutUs.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.slideup, R.anim.exit);

            } else if (view == lv_terms) {

                Intent intent1 = new Intent(Navigation_new.this, LegalPageNew.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.slideup, R.anim.exit);

            }/*else if (view == drawer_fileimage2) {

                Intent intent_sh = new Intent(Navigation_new.this, MenuUserratingHOme.class);
                intent_sh.putExtra("frompage","Home");
                startActivity(intent_sh);

            }else if (view == drawer_fileimage3) {

                Intent intent_sh = new Intent(Navigation_new.this, MenuUserratingHOme.class);
                intent_sh.putExtra("frompage","Home");
                startActivity(intent_sh);

            }*/
            /*else if (view == Bt_be_partner) {
                Intent intent_bepartner = new Intent(Navigation_new.this, BePartner.class);
                startActivity(intent_bepartner);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (view == Bt_feedback) {
                sendEmail();

            } */
            else if (view == tv_logout) {

                logout();

            }

            ft.commit();
            //drawerLayout.closeDrawer(mDrawer);


        } else {

            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));

        }


    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(Navigation_new.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    //----------Method to Send Email--------
    protected void sendEmail() {
        SappInfo_Session = new AppInfoSessionManager(Navigation_new.this);
        HashMap<String, String> appInfo = SappInfo_Session.getAppInfo();
        String toAddress = appInfo.get(AppInfoSessionManager.KEY_CONTACT_EMAIL);
        String[] TO = {toAddress};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Message");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(Navigation_new.this, getkey("thers_is_no"), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public static void openDrawer() {
        drawerLayout.openDrawer(mDrawer);
        clickdisabledashboardpojo pojo = new clickdisabledashboardpojo();
        pojo.setIsenabled(true);
        EventBus.getDefault().post(pojo);
    }


    private void logout() {

        final PkDialog mDialog = new PkDialog(Navigation_new.this);
        mDialog.setDialogTitle(getkey("logout_lable"));
        mDialog.setDialogMessage(getkey("drawer_lable_logout_message"));
        mDialog.setPositiveButton(getkey("drawer_lable_logout_yes"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                if (isInternetPresent) {
                    postRequest_Logout(Iconstant.logout_url);
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }
            }
        });
        mDialog.setNegativeButton(getkey("drawer_lable_logout_no"), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }



    //--------------Show Dialog Method-----------
    private void showDialog(String data) {
        dialog = new Dialog(Navigation_new.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(data);
    }

    //-----------------------Logout Request-----------------
    private void postRequest_Logout(String Url) {
        showDialog(getkey("action_logging_out"));
        System.out.println("---------------LogOut Url-----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("device", "ANDROID");

        mRequest = new ServiceRequest(Navigation_new.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------------LogOut Response-----------------" + response);
                String Sstatus = "", Sresponse = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (Sstatus.equalsIgnoreCase("1")) {
                    session.logoutUser();
                    stopService(new Intent(Navigation_new.this, XmppService.class));
                    postRequest_AppInformation(Iconstant.getAppInfo);

                } else {
                    session.logoutUser();
                    Intent intent = new Intent(Navigation_new.this, SignUpPage.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                   // Alert(getkey("action_error"), Sresponse);
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //-----------------------App Information Post Request-----------------
    private void postRequest_AppInformation(String Url) {
        System.out.println("-------------Splash App Information Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "user");
        jsonParams.put("id", "");
        jsonParams.put("lat", "");
        jsonParams.put("lon", "");
        System.out.println("--------App Information jsonParams--------" + jsonParams);
        mRequest = new ServiceRequest(Navigation_new.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Splash App Information Response----------------" + response);
                String Str_status = "", SuserImage = "", userGuide = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                dialog.dismiss();
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                session.setCustomerServiceNo(sCustomerServiceNumber);
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");

                                if(info_object.has("s3_status"))
                                {
                                    if(info_object.getString("s3_status").equals("1"))
                                    {
                                        JSONObject s3_info = info_object.getJSONObject("s3_info");

                                        String access_key = s3_info.getString("access_key");
                                        String secret_key = s3_info.getString("secret_key");
                                        String bucket_name = s3_info.getString("bucket_name");
                                        String bucket_url = s3_info.getString("bucket_url");
                                        session.setAmazondetails(info_object.getString("s3_status"),access_key,secret_key,bucket_name,bucket_url);
                                    }
                                    else
                                    {
                                        session.setImagestatus("0");
                                    }
                                }
                            } else {

                            }

                            Intent intent = new Intent(Navigation_new.this, SignUpPage.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();

                           /* sPendingRideId= response_object.getString("pending_rideid");
                            sRatingStatus= response_object.getString("ride_status");*/

                        } else {

                        }
                    } else {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
//                Toast.makeText(context, Iconstant.Url, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Navigation_new.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }


    public static void navigationNotifyChange() {
        mMenuAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }


    public void enableSwipeDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        if (dialog != null) {
            dialog.dismiss();
        }
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    public void updateCoupencount(String count) {
        try {
            if (Integer.parseInt(count) > 0) {
                tv_coupon_countt.setText(String.valueOf(count));
               tv_coupon_countt.setVisibility(View.VISIBLE);
            } else {
                tv_coupon_countt.setVisibility(View.INVISIBLE);
            }
        } catch (NumberFormatException e) {
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}
