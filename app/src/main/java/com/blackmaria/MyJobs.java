/*
package com.blackmaria;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.plumbal.R;
import com.plumbal.adapter.MyJobsListAdapter;
import com.plumbal.hockeyapp.ActivityHockeyApp;
import com.plumbal.iconstant.Iconstant;
import com.plumbal.mylibrary.dialog.LoadingDialog;
import com.plumbal.mylibrary.dialog.PkDialog;
import com.plumbal.mylibrary.dialog.PkLoadingDialog;
import com.plumbal.mylibrary.volley.ServiceRequest;
import com.plumbal.mylibrary.xmpp.ChatService;
import com.plumbal.pojo.CancelJobPojo;
import com.plumbal.pojo.MyJobsListPojo;
import com.plumbal.utils.ConnectionDetector;
import com.plumbal.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;

*
 * Created by Prem Kumar and Anitha on 1/11/2016.


public class MyJobs extends ActivityHockeyApp {
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;

    private RelativeLayout Rl_back;
    private ImageView Im_backIcon;
    private TextView Tv_headerTitle;

    private ServiceRequest mRequest;
    private PkLoadingDialog mLoadingDialog;

    private ListView listView;
    private RelativeLayout Rl_NoInternet, Rl_Main;
    private WaveSwipeRefreshLayout swipeToRefresh;
    private String Str_Refresh_Name = "normal";
    private TextView Tv_empty;
    private LinearLayout Ll_All, Ll_Closed, Ll_Cancelled;
    private TextView Tv_All, Tv_Closed, Tv_Cancelled;

    private String sTabSelectedCheck = "all";
    private String sUserID = "";
    private boolean isJobAvailable = false;
    private ArrayList<MyJobsListPojo> jobsList;
    private MyJobsListAdapter adapter;

    private RelativeLayout Rl_LoadMore;
    private boolean loadingMore = false;
    private int checkPagePos = 0;

    ArrayList<CancelJobPojo> itemList_reason;
    private boolean isReasonAvailable = false;

    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.package.ACTION_CLASS_MY_JOBS_REFRESH")) {
                if (isInternetPresent) {
                    postJobRequest(Iconstant.MyJobsList_Url, sTabSelectedCheck);
                }
            }
        }
    }

    private RefreshReceiver refreshReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myjobs_list);
        initializeHeaderBar();
        initialize();

        Rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                int threshold = 1;
                int count = listView.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (listView.getLastVisiblePosition() >= count - threshold && !(loadingMore)) {

                        if (swipeToRefresh.isRefreshing()) {
                            //nothing happen(code to block loadMore functionality when swipe to refresh is loading)
                        } else {

                            if (isJobAvailable) {
                                ConnectionDetector cd = new ConnectionDetector(MyJobs.this);
                                boolean isInternetPresent = cd.isConnectingToInternet();

                                if (isInternetPresent) {
                                    postJobLoadMoreRequest(Iconstant.MyJobsList_Url, sTabSelectedCheck, (checkPagePos + 1));
                                } else {
                                    alert(getResources().getString(R.string.action_no_internet_title), getResources().getString(R.string.action_no_internet_message));
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    swipeToRefresh.setEnabled(true);
                } else {
                    swipeToRefresh.setEnabled(false);
                }
            }
        });


        swipeToRefresh.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (loadingMore) {
                    //nothing happen(code to block swipe functionality when loadmore is loading)
                    swipeToRefresh.setRefreshing(false);
                } else {
                    cd = new ConnectionDetector(MyJobs.this);
                    isInternetPresent = cd.isConnectingToInternet();

                    if (isInternetPresent) {
                        Rl_Main.setVisibility(View.VISIBLE);
                        Rl_NoInternet.setVisibility(View.GONE);
                        Str_Refresh_Name = "swipe";
                        postJobRequest(Iconstant.MyJobsList_Url, sTabSelectedCheck);
                    } else {
                        swipeToRefresh.setEnabled(true);
                        swipeToRefresh.setRefreshing(false);
                        Rl_Main.setVisibility(View.GONE);
                        Rl_NoInternet.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        Ll_All.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sTabSelectedCheck = "all";
                Ll_All.setBackgroundColor(0xFF00897B);
                Ll_Closed.setBackgroundColor(0xFFFFFFFF);
                Ll_Cancelled.setBackgroundColor(0xFFFFFFFF);
                Tv_All.setTextColor(0xFFFFFFFF);
                Tv_Closed.setTextColor(0xFF00897B);
                Tv_Cancelled.setTextColor(0xFF00897B);

                ConnectionDetector cd = new ConnectionDetector(MyJobs.this);
                boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    Str_Refresh_Name = "normal";
                    postJobRequest(Iconstant.MyJobsList_Url, sTabSelectedCheck);
                } else {
                    alert(getResources().getString(R.string.action_no_internet_title), getResources().getString(R.string.action_no_internet_message));
                }

            }
        });

        Ll_Closed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sTabSelectedCheck = "closed";
                Ll_All.setBackgroundColor(0xFFFFFFFF);
                Ll_Closed.setBackgroundColor(0xFF00897B);
                Ll_Cancelled.setBackgroundColor(0xFFFFFFFF);
                Tv_All.setTextColor(0xFF00897B);
                Tv_Closed.setTextColor(0xFFFFFFFF);
                Tv_Cancelled.setTextColor(0xFF00897B);

                ConnectionDetector cd = new ConnectionDetector(MyJobs.this);
                boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    Str_Refresh_Name = "normal";
                    postJobRequest(Iconstant.MyJobsList_Url, sTabSelectedCheck);
                } else {
                    alert(getResources().getString(R.string.action_no_internet_title), getResources().getString(R.string.action_no_internet_message));
                }

            }
        });

        Ll_Cancelled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sTabSelectedCheck = "cancelled";
                Ll_All.setBackgroundColor(0xFFFFFFFF);
                Ll_Closed.setBackgroundColor(0xFFFFFFFF);
                Ll_Cancelled.setBackgroundColor(0xFF00897B);
                Tv_All.setTextColor(0xFF00897B);
                Tv_Closed.setTextColor(0xFF00897B);
                Tv_Cancelled.setTextColor(0xFFFFFFFF);

                ConnectionDetector cd = new ConnectionDetector(MyJobs.this);
                boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    Str_Refresh_Name = "normal";
                    postJobRequest(Iconstant.MyJobsList_Url, sTabSelectedCheck);
                } else {
                    alert(getResources().getString(R.string.action_no_internet_title), getResources().getString(R.string.action_no_internet_message));
                }

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                cd = new ConnectionDetector(MyJobs.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    Intent intent = new Intent(MyJobs.this, MyJobDetail.class);
                    intent.putExtra("JOB_ID_INTENT", jobsList.get(position).getJob_id());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    alert(getResources().getString(R.string.action_no_internet_title), getResources().getString(R.string.action_no_internet_message));
                }

            }
        });
    }

    private void initializeHeaderBar() {
        RelativeLayout headerBar = (RelativeLayout) findViewById(R.id.headerBar_noShadow_layout);
        Rl_back = (RelativeLayout) headerBar.findViewById(R.id.headerBar_noShadow_left_layout);
        Im_backIcon = (ImageView) headerBar.findViewById(R.id.headerBar_noShadow_imageView);
        Tv_headerTitle = (TextView) headerBar.findViewById(R.id.headerBar_noShadow_title_textView);

        Tv_headerTitle.setText(getResources().getString(R.string.myJobs_label_title));
        Im_backIcon.setImageResource(R.drawable.back_arrow);
    }

    private void initialize() {
        cd = new ConnectionDetector(MyJobs.this);
        isInternetPresent = cd.isConnectingToInternet();
        sessionManager = new SessionManager(MyJobs.this);
        mRequest = new ServiceRequest(MyJobs.this);
        jobsList = new ArrayList<MyJobsListPojo>();
        itemList_reason = new ArrayList<CancelJobPojo>();

        listView = (ListView) findViewById(R.id.myJobs_listView);
        swipeToRefresh = (WaveSwipeRefreshLayout) findViewById(R.id.myJobs_swipeToRefresh_layout);
        Rl_NoInternet = (RelativeLayout) findViewById(R.id.myJobs_noInternet_layout);
        Rl_Main = (RelativeLayout) findViewById(R.id.myJobs_main_layout);
        Tv_empty = (TextView) findViewById(R.id.myJobs_empty_textView);
        Ll_All = (LinearLayout) findViewById(R.id.myJobs_all_layout);
        Ll_Closed = (LinearLayout) findViewById(R.id.myJobs_closed_layout);
        Ll_Cancelled = (LinearLayout) findViewById(R.id.myJobs_cancelled_layout);
        Tv_All = (TextView) findViewById(R.id.myJobs_all_textView);
        Tv_Closed = (TextView) findViewById(R.id.myJobs_closed_textView);
        Tv_Cancelled = (TextView) findViewById(R.id.myJobs_cancelled_textView);
        Rl_LoadMore = (RelativeLayout) findViewById(R.id.myJobs_loadMore_layout);

        // Configure the refreshing colors
        swipeToRefresh.setColorSchemeColors(Color.WHITE, Color.WHITE);
        swipeToRefresh.setWaveColor(getResources().getColor(R.color.app_color));
        swipeToRefresh.setMaxDropHeight(350);//should Give in Hundreds
        swipeToRefresh.setEnabled(true);

        Ll_All.setBackgroundColor(0xFF00897B);
        Tv_All.setTextColor(0xFFFFFFFF);


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_CLASS_MY_JOBS_REFRESH");
        registerReceiver(refreshReceiver, intentFilter);


        // get user data from session
        HashMap<String, String> user = sessionManager.getUserDetails();
        sUserID = user.get(SessionManager.KEY_USER_ID);

        if (isInternetPresent) {
            Rl_Main.setVisibility(View.VISIBLE);
            Rl_NoInternet.setVisibility(View.GONE);
            postJobRequest(Iconstant.MyJobsList_Url, sTabSelectedCheck);
        } else {
            swipeToRefresh.setEnabled(true);
            Rl_Main.setVisibility(View.GONE);
            Rl_NoInternet.setVisibility(View.VISIBLE);
        }

    }

    //------Alert Method-----
    private void alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(MyJobs.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //------Cancel Job Reason Method-----
    public void cancelJobReason(final String jobID) {
        final PkDialog mDialog = new PkDialog(MyJobs.this);
        mDialog.setDialogTitle(getResources().getString(R.string.myJobs_cancel_job_alert_title));
        mDialog.setDialogMessage(getResources().getString(R.string.myJobs_cancel_job_alert));
        mDialog.setPositiveButton(getResources().getString(R.string.myJobs_cancel_job_alert_yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                cd = new ConnectionDetector(MyJobs.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    postRequest_CancelJob_Reason(Iconstant.MyJobs_Cancel_Reason_Url, jobID);
                } else {
                    alert(getResources().getString(R.string.action_no_internet_title), getResources().getString(R.string.action_no_internet_message));
                }
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.myJobs_cancel_job_alert_no), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void startLoading() {
        if (Str_Refresh_Name.equalsIgnoreCase("normal")) {
            mLoadingDialog = new PkLoadingDialog(MyJobs.this);
            mLoadingDialog.show();
        } else {
            swipeToRefresh.setRefreshing(true);
        }
    }

    private void stopLoading() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Str_Refresh_Name.equalsIgnoreCase("normal")) {
                    mLoadingDialog.dismiss();
                } else {
                    swipeToRefresh.setRefreshing(false);
                }
            }
        }, 500);
    }


    //-------------My Jobs Post Request---------------
    private void postJobRequest(String url, String sType) {

        startLoading();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", sUserID);
        jsonParams.put("type", sType);
        jsonParams.put("page", "1");
        jsonParams.put("perPage", "20");

        System.out.println("---------My Jobs Type------------" + sType);
        System.out.println("---------My Jobs Page Pos------------" + "1");
        System.out.println("---------My Jobs url------------" + url);

        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------My Jobs response------------" + response);

                String Str_status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_Object = object.getJSONObject("response");
                        if (response_Object.length() > 0) {

                            checkPagePos = response_Object.getInt("current_page");

                            Object check_jobs_object = response_Object.get("jobs");
                            if (check_jobs_object instanceof JSONArray) {

                                JSONArray jobs_Array = response_Object.getJSONArray("jobs");
                                if (jobs_Array.length() > 0) {
                                    jobsList.clear();
                                    for (int i = 0; i < jobs_Array.length(); i++) {
                                        JSONObject jobs_Object = jobs_Array.getJSONObject(i);
                                        MyJobsListPojo pojo = new MyJobsListPojo();

                                        pojo.setJob_id(jobs_Object.getString("job_id"));
                                        pojo.setJob_time(jobs_Object.getString("job_time"));
                                        pojo.setService_type(jobs_Object.getString("service_type"));
                                        pojo.setService_icon(jobs_Object.getString("service_icon"));
                                        pojo.setBooking_date(jobs_Object.getString("booking_date"));
                                        pojo.setJob_date(jobs_Object.getString("job_date"));
                                        pojo.setJob_status(jobs_Object.getString("job_status"));
                                        pojo.setContact_number(jobs_Object.getString("contact_number"));
                                        pojo.setDoCall(jobs_Object.getString("doCall"));
                                        pojo.setIsSupport(jobs_Object.getString("isSupport"));
                                        pojo.setDoMsg(jobs_Object.getString("doMsg"));
                                        pojo.setDoCancel(jobs_Object.getString("doCancel"));

                                        jobsList.add(pojo);
                                    }
                                    isJobAvailable = true;
                                } else {
                                    isJobAvailable = false;
                                    jobsList.clear();
                                }
                            } else {
                                isJobAvailable = false;
                                jobsList.clear();
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    stopLoading();
                    System.out.println("-------JSONException-----------------" + e);
                }

                if (Str_status.equalsIgnoreCase("1") && isJobAvailable) {
                    adapter = new MyJobsListAdapter(MyJobs.this, jobsList);
                    listView.setAdapter(adapter);
                    Tv_empty.setVisibility(View.GONE);
                } else if (Str_status.equalsIgnoreCase("1") && !isJobAvailable) {
                    adapter = new MyJobsListAdapter(MyJobs.this, jobsList);
                    listView.setAdapter(adapter);
                    Tv_empty.setVisibility(View.VISIBLE);
                }

                stopLoading();
            }

            @Override
            public void onErrorListener() {
                stopLoading();
            }
        });
    }


    //-------------My Jobs Load More Post Request---------------
    private void postJobLoadMoreRequest(String url, String sType, int sPage) {

        Rl_LoadMore.setVisibility(View.VISIBLE);
        loadingMore = true;

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", sUserID);
        jsonParams.put("type", sType);
        jsonParams.put("page", String.valueOf(sPage));
        jsonParams.put("perPage", "20");

        System.out.println("---------My Jobs LoadMore Type------------" + sType);
        System.out.println("---------My Jobs LoadMore Page Pos------------" + sPage);
        System.out.println("---------My Jobs LoadMore url------------" + url);

        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------My Jobs LoadMore response------------" + response);

                String Str_status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_Object = object.getJSONObject("response");
                        if (response_Object.length() > 0) {

                            checkPagePos = response_Object.getInt("current_page");

                            Object check_jobs_object = response_Object.get("jobs");
                            if (check_jobs_object instanceof JSONArray) {

                                JSONArray jobs_Array = response_Object.getJSONArray("jobs");
                                if (jobs_Array.length() > 0) {
                                    for (int i = 0; i < jobs_Array.length(); i++) {
                                        JSONObject jobs_Object = jobs_Array.getJSONObject(i);
                                        MyJobsListPojo pojo = new MyJobsListPojo();

                                        pojo.setJob_id(jobs_Object.getString("job_id"));
                                        pojo.setJob_time(jobs_Object.getString("job_time"));
                                        pojo.setService_type(jobs_Object.getString("service_type"));
                                        pojo.setService_icon(jobs_Object.getString("service_icon"));
                                        pojo.setBooking_date(jobs_Object.getString("booking_date"));
                                        pojo.setJob_date(jobs_Object.getString("job_date"));
                                        pojo.setJob_status(jobs_Object.getString("job_status"));
                                        pojo.setContact_number(jobs_Object.getString("contact_number"));
                                        pojo.setDoCall(jobs_Object.getString("doCall"));
                                        pojo.setIsSupport(jobs_Object.getString("isSupport"));
                                        pojo.setDoMsg(jobs_Object.getString("doMsg"));
                                        pojo.setDoCancel(jobs_Object.getString("doCancel"));

                                        jobsList.add(pojo);
                                    }
                                    isJobAvailable = true;
                                } else {
                                    isJobAvailable = false;
                                }
                            } else {
                                isJobAvailable = false;
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    loadingMore = false;
                    Rl_LoadMore.setVisibility(View.GONE);
                    System.out.println("-------JSONException-----------------" + e);
                }

                if (Str_status.equalsIgnoreCase("1") && isJobAvailable) {
                    adapter.notifyDataSetChanged();
                }

                loadingMore = false;
                Rl_LoadMore.setVisibility(View.GONE);
            }

            @Override
            public void onErrorListener() {
                loadingMore = false;
                Rl_LoadMore.setVisibility(View.GONE);
            }
        });
    }


    //-----------------------MyRide Cancel Reason Post Request-----------------
    private void postRequest_CancelJob_Reason(String Url, final String sJobId) {

        final LoadingDialog mLoading = new LoadingDialog(MyJobs.this);
        mLoading.setLoadingTitle(getResources().getString(R.string.action_pleaseWait));
        mLoading.show();


        System.out.println("-------------Cancel Job Reason Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", sUserID);

        mRequest = new ServiceRequest(MyJobs.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Cancel Job Reason Response----------------" + response);

                String sStatus = "";

                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {

                            Object check_reason_object = response_object.get("reason");
                            if (check_reason_object instanceof JSONArray) {

                                JSONArray reason_array = response_object.getJSONArray("reason");
                                if (reason_array.length() > 0) {
                                    itemList_reason.clear();
                                    for (int i = 0; i < reason_array.length(); i++) {
                                        JSONObject reason_object = reason_array.getJSONObject(i);
                                        CancelJobPojo pojo = new CancelJobPojo();
                                        pojo.setReason(reason_object.getString("reason"));
                                        pojo.setReasonId(reason_object.getString("id"));

                                        itemList_reason.add(pojo);
                                    }

                                    isReasonAvailable = true;
                                } else {
                                    isReasonAvailable = false;
                                }
                            }
                        }
                    } else {
                        String sResponse = object.getString("response");
                        alert(getResources().getString(R.string.action_sorry), sResponse);
                    }


                    if (sStatus.equalsIgnoreCase("1") && isReasonAvailable) {
                        Intent passIntent = new Intent(MyJobs.this, CancelJob.class);
                        Bundle bundleObject = new Bundle();
                        bundleObject.putSerializable("Reason", itemList_reason);
                        passIntent.putExtras(bundleObject);
                        passIntent.putExtra("JOB_ID", sJobId);
                        startActivity(passIntent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                mLoading.dismiss();
            }

            @Override
            public void onErrorListener() {
                mLoading.dismiss();
            }
        });
    }


    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }


    @Override
    public void onResume() {
        super.onResume();
        //starting XMPP service
        if (!ChatService.isConnected) {
            ChatService.startUserAction(MyJobs.this);
        }
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            return true;
        }
        return false;
    }
}
*/
