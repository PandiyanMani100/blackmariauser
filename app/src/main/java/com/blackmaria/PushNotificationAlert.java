package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Prem Kumar and Anitha on 11/6/2015.
 */
public class PushNotificationAlert extends Activity {
    TextView  Tv_title;
    TextView Rl_ok,retryurl;
    private String response_time = "";
    TextView message;
    private Boolean moveNext = true;
    private String riderId = "", Str_driver_image = "", carCatImage = "";
    private Dialog dialog;
    private SessionManager session;
    private ServiceRequest mRequest;
    private String Str_message, Str_action, SrideId_intent, IsNormalOrreturnMulti = "", FreeWaitingTime = "";
    private SessionManager sessionManager;
    private TimerTask timerTask;
    private Handler handlerNew;
    LanguageDb mhelper;
    private Timer timer;
    private int timer_request_code = 100;
    String currency_code = "", Amount = "";
    String user_id = "", paymenttype = "", payment_via = "", from_number = "", from = "", date = "", time = "", trans_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pushnotification_alert);
        mhelper = new LanguageDb(this);
        session = new SessionManager(PushNotificationAlert.this);
        initialize();
        if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCancelled_Key))
        {
            retryurl.setVisibility(View.VISIBLE);
         }
        retryurl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmRideRequest(Iconstant.retryurl, "");
            }
        });

        Rl_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//|| Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCompleted_Key)
                if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCancelled_Key) || Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCompleted_Key) || Str_action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key)) {
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction("com.pushnotification.updateBottom_view");
                    broadcastIntent.putExtra("rideStatus", Str_action);
                    broadcastIntent.putExtra("ride_id", SrideId_intent);
                    sendBroadcast(broadcastIntent);

                }

                if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_PaymentPaid_Key)) {
                    System.out.println("--------------------------------------niramjan------------------alert----------------payment paid");
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction("com.pushnotification.updateBottom_view");
                    broadcastIntent.putExtra("rideStatus", Iconstant.PushNotification_PaymentPaid_Key);
                    broadcastIntent.putExtra("ride_id", SrideId_intent);
                    broadcastIntent.putExtra("payment", "success");
                    sendBroadcast(broadcastIntent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }


                if (Str_action.equalsIgnoreCase(Iconstant.wallet_success)) {


//                    Intent intent = new Intent(PushNotificationAlert.this, Reload_Transfer_PushNotification.class);
//                    intent.putExtra("Str_action", Str_action);
//                    intent.putExtra("user_id", user_id);
//                    intent.putExtra("Amount", Amount);
//                    intent.putExtra("Currencycode", currency_code);
//                    intent.putExtra("paymenttype", paymenttype);
//                    intent.putExtra("payment_via", payment_via);
//                    intent.putExtra("from_number", from_number);
//                    intent.putExtra("from", from);
//                    intent.putExtra("date", date);
//                    intent.putExtra("time", time);
//                    intent.putExtra("trans_id", trans_id);
//
//                    startActivity(intent);
//                    finish();

//                    Intent intent = new Intent(PushNotificationAlert.this, Navigation_new.class);
//                    intent.putExtra("Str_action",Str_action);
//                    intent.putExtra("user_id", user_id);
//                    intent.putExtra("Amount", Amount);
//                    intent.putExtra("Currencycode", currency_code);
//                    intent.putExtra("paymenttype", paymenttype);
//                    intent.putExtra("payment_via", payment_via);
//                    intent.putExtra("from_number", from_number);
//                    intent.putExtra("from", from);
//                    intent.putExtra("date", date);
//                    intent.putExtra("time", time);
//                    intent.putExtra("trans_id", trans_id);
//                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    //         overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


                }
                if (Str_action.equalsIgnoreCase(Iconstant.bank_success)) {

//                    Intent intent = new Intent(PushNotificationAlert.this, Reload_Transfer_PushNotification.class);
//                    intent.putExtra("Str_action", Str_action);
//                    intent.putExtra("user_id", user_id);
//                    intent.putExtra("Amount", Amount);
//                    intent.putExtra("Currencycode", currency_code);
//                    intent.putExtra("paymenttype", paymenttype);
//                    intent.putExtra("payment_via", payment_via);
//                    intent.putExtra("from_number", from_number);
//                    intent.putExtra("from", from);
//                    intent.putExtra("date", date);
//                    intent.putExtra("time", time);
//                    intent.putExtra("trans_id", trans_id);
//                    startActivity(intent);
//                    finish();

                    Intent intent = new Intent(PushNotificationAlert.this, Navigation_new.class);
//                    intent.putExtra("Str_action",Str_action);
//                    intent.putExtra("user_id", user_id);
//                    intent.putExtra("Amount", Amount);
//                    intent.putExtra("Currencycode", currency_code);
//                    intent.putExtra("paymenttype", paymenttype);
//                    intent.putExtra("payment_via", payment_via);
//                    intent.putExtra("from_number", from_number);
//                    intent.putExtra("from", from);
//                    intent.putExtra("date", date);
//                    intent.putExtra("time", time);
//                    intent.putExtra("trans_id", trans_id);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                    //   overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//
                }
                if (Str_action.equalsIgnoreCase(Iconstant.wallet_credit)) {

//                    Intent intent = new Intent(PushNotificationAlert.this, Navigation_new.class);
//                    intent.putExtra("Str_action",Str_action);
//                    intent.putExtra("user_id", user_id);
//                    intent.putExtra("Amount", Amount);
//                    intent.putExtra("Currencycode", currency_code);
//                    intent.putExtra("paymenttype", paymenttype);
//                    intent.putExtra("payment_via", payment_via);
//                    intent.putExtra("from_number", from_number);
//                    intent.putExtra("from", from);
//                    intent.putExtra("date", date);
//                    intent.putExtra("time", time);
//                    intent.putExtra("trans_id", trans_id);
//                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }


                if (Str_action.equalsIgnoreCase(Iconstant.complementry_time_closed) || Str_action.equalsIgnoreCase(Iconstant.Drop_Waiting_time_closed_key)) {
                    sessionManager.setWaitedTime(0, 0, "", "");
                    Intent intent = new Intent(PushNotificationAlert.this, TrackRidePage.class);
                    intent.putExtra("ride_id", SrideId_intent);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    try{
                        sessionManager.setWaitedTime(0, 0, "", "");
                        if(WaitingTimePage.activity != null){
                            WaitingTimePage.activity.finish();
                        }}catch (Exception e){}

                } else if (Str_action.equalsIgnoreCase(Iconstant.referalcredit_amount)) {
                    Intent intent = new Intent(PushNotificationAlert.this, CloudMoneyHomePage.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                } else {
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }

            }
        });
    }

    private void initialize() {
        sessionManager = new SessionManager(PushNotificationAlert.this);

        Tv_title = (TextView) findViewById(R.id.pushnotification_alert_messge_label);
        message = (TextView) findViewById(R.id.pushnotification_alert_messge_textview);
        Rl_ok = (TextView) findViewById(R.id.pushnotification_alert_ok_textview);
        Rl_ok.setText(getkey("ok_lable"));
        retryurl = (TextView) findViewById(R.id.retryurl);

        Intent intent = getIntent();
        Str_message = intent.getStringExtra("message");
        Str_action = intent.getStringExtra("Action");
        if (getIntent().getExtras().containsKey("RideID")) {
            SrideId_intent = intent.getStringExtra("RideID");
        }


        if (intent.hasExtra("Currencycode")) {
            currency_code = intent.getStringExtra("Currencycode");
        }
        if (intent.hasExtra("Amount")) {
            Amount = intent.getStringExtra("Amount");
        }


        if (getIntent().getExtras().containsKey("FreeWaitingTime")) {
            FreeWaitingTime = intent.getStringExtra("FreeWaitingTime");
        }
        if (getIntent().getExtras().containsKey("IsNormalOrreturnMulti")) {
            IsNormalOrreturnMulti = intent.getStringExtra("IsNormalOrreturnMulti");
        }

        if (Str_action.equalsIgnoreCase(Iconstant.wallet_credit) || Str_action.equalsIgnoreCase(Iconstant.wallet_success) || Str_action.equalsIgnoreCase(Iconstant.bank_success)) {

            user_id = intent.getStringExtra("user_id");
            paymenttype = intent.getStringExtra("paymenttype");
            payment_via = intent.getStringExtra("payment_via");
            from_number = intent.getStringExtra("from_number");
            from = intent.getStringExtra("from");
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            trans_id = intent.getStringExtra("trans_id");
        }

        message.setText(Str_message);

        if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCancelled_Key)) {
            System.out.println("pushnotification");
        } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_CabArrived_Key)) {
        } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_RideCompleted_Key) || Str_action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key)) {
         } else if (Str_action.equalsIgnoreCase(Iconstant.PushNotification_PaymentPaid_Key)) {
         } else if (Str_action.equalsIgnoreCase(Iconstant.complementry_time_closed)) {
        }

    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }


    private void ConfirmRideRequest(String Url, final String try_value) {
        dialog = new Dialog(PushNotificationAlert.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView loading = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        loading.setText(getkey("action_pleasewait"));

        System.out.println("--------------Confirm Ride url----bpp---------------" + Url);

        HashMap<String, String> info = session.getUserDetails();
        String UserID = info.get(SessionManager.KEY_USERID);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", SrideId_intent);

        mRequest = new ServiceRequest(PushNotificationAlert.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------Confirm Ride reponse-------------------" + response);

                String selected_type = "", Sacceptance = "";
                String Str_driver_id = "", Str_driver_name = "", Str_driver_email = "", Str_driver_review = "",
                        Str_driver_lat = "", Str_driver_lon = "", Str_min_pickup_duration = "", Str_ride_id = "", Str_phone_number = "",
                        Str_vehicle_number = "", Str_vehicle_model = "", Str_photo = "", Str_photo_type;
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String status = object.getString("status");
                        if (status.equalsIgnoreCase("1")) {
                            JSONObject response_object = object.getJSONObject("response");

                            Str_photo = object.getString("photo_video");
                            Str_photo_type = object.getString("photo_video_type");

                            selected_type = response_object.getString("type");

                            response_time = response_object.getString("response_time");
                            riderId = response_object.getString("ride_id");




                            Intent intent = new Intent(PushNotificationAlert.this, TimerPage.class);
                            intent.putExtra("Time", response_time);
                            intent.putExtra("retry_count", try_value);
                            intent.putExtra("ride_ID", riderId);
                            intent.putExtra("Str_photo", Str_photo);
                            intent.putExtra("Str_photo_type", Str_photo_type);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();

                        } else {
                            String Sresponse = object.getString("response");
                            Toast.makeText(getApplicationContext(),Sresponse,Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialogDismiss();

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //To stop timer
    private void stopTimer() {

        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
    }



    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
