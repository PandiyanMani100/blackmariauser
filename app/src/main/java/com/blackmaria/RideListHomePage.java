package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.ComplaintsPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


public class RideListHomePage extends ActivityHockeyApp {

    private TextView viewdetail_tv1, hightlights, paymentsummary, tv_bidding_trip, myTrip_totalTrip_thisMonth_textView, myTrip_today_highlight_complaint_TextView, myTrip_today_highlight_sosTrip_TextView, tv_total_bidding_value, myTrip_today_highlight_cancelTrip_TextView, tv_total_trip_value, tv_complaint_trip, tv_sos_trip, tv_last_time, tv_last_date, tv_cancel_trip, tv_total_trip, myTrip_totalTrip_thisMonth_km_textView, tv_month, myTrip_trip_topTrip_profile_name_textView, tv_last_cartype, myTrip_totalTrip_thisWeek_km_textView, myTrip_totalTrip_thisWeek_textView, viewdetail_tv, tv_week, tv_last_distance, myTrip_totalTrip_today_km_textView, myTrip_totalTrip_today_textView, tv_today, tv_last_amount, view_all_ride_list, tv_last_ride_stztus, myTrip_trip_topTrip_location_textView;
    private RelativeLayout header_lyt, myTrip_trip_topTrip_content_layout, myTrip_trip_meter_monthTrip_linear_layout, myTrip_trip_meter_weekTrip_linear_layout, myTrip_trip_meter_totalTrip_linear_layout, all_ride_list_lyt, lyt_highlight;
    private ImageView backbtn, lastdrivercarimage;
    private RoundedImageView myTrip_trip_topTrip_profile_imageView;
    private RefreshReceiver refreshReceiver;

    //payment summary
    private TextView cashvalue, cardvalue, walletvalue, couponsvalue;
    //last driver
    private ImageView lastdriverimage;
    private TextView lastdriver_name, lastdriver_carno, reportcomplaint;
    LanguageDb mhelper;
    private boolean isInternetPresent = false;
    private SessionManager session;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    String UserID = "";
    private String type = "";
    private LinearLayout lasttrip_layout;
    String lastRideID = "";


    String userName = "", userImage = "", userLocation = "", todayDate;
    private ArrayList<ComplaintsPojo> complaint_itemList;
    private ArrayList<String> complaint_itemList1;
    private ArrayList<String> complaint_itemList2;
    private boolean isComplaintsAvailable = false;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(RideListHomePage.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(RideListHomePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(RideListHomePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newridelisthomepage);
        mhelper=new LanguageDb(this);
        initialize();

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RideListHomePage.this, Navigation_new.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);


            }
        });
        view_all_ride_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RideListHomePage.this, RideList.class);
                intent.putExtra("drivername", lastdriver_name.getText().toString());
                //  intent.putExtra("Class", "Home");
                startActivity(intent);
//                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
//        viewdetail_tv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(RideListHomePage.this, RideDetailPage.class);
//                intent.putExtra("ride_id", lastRideID);
//                intent.putExtra("class", "RideListHomepage");
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);
//            }
//        });

    }

    private void initialize() {
        session = new SessionManager(RideListHomePage.this);
        cd = new ConnectionDetector(RideListHomePage.this);
        isInternetPresent = cd.isConnectingToInternet();
        myTrip_totalTrip_today_km_textView = findViewById(R.id.myTrip_totalTrip_today_km_textView);
        myTrip_totalTrip_today_textView = findViewById(R.id.myTrip_totalTrip_today_textView);
        myTrip_totalTrip_thisWeek_textView = findViewById(R.id.myTrip_totalTrip_thisWeek_textView);
        myTrip_totalTrip_thisWeek_km_textView = findViewById(R.id.myTrip_totalTrip_thisWeek_km_textView);
        myTrip_totalTrip_thisMonth_textView = findViewById(R.id.myTrip_totalTrip_thisMonth_textView);
        myTrip_totalTrip_thisMonth_km_textView = findViewById(R.id.myTrip_totalTrip_thisMonth_km_textView);
        hightlights = findViewById(R.id.hightlights);
        paymentsummary = findViewById(R.id.paymentsummary);
        complaint_itemList = new ArrayList<ComplaintsPojo>();
        complaint_itemList1 = new ArrayList<String>();
        complaint_itemList2 = new ArrayList<String>();


        TextView mytrips = findViewById(R.id.mytrips);
        mytrips.setText(getkey("my_trips"));
        TextView tda = findViewById(R.id.tda);
        tda.setText(getkey("today_with_space"));
        TextView thisweek = findViewById(R.id.thisweek);
        thisweek.setText(getkey("this_week"));
        TextView thismonth = findViewById(R.id.thismonth);
        thismonth.setText(getkey("this_month"));
        TextView lasttriplabel = findViewById(R.id.lasttriplabel);
        lasttriplabel.setText(getkey("last_trip"));


        //payment summary
        cashvalue = findViewById(R.id.cashvalue);
        cardvalue = findViewById(R.id.cardvalue);
        walletvalue = findViewById(R.id.walletvalue);
        couponsvalue = findViewById(R.id.couponsvalue);

        TextView cash = findViewById(R.id.cash);
        cash.setText(getkey("cash"));

        TextView card = findViewById(R.id.card);
        card.setText(getkey("cardss"));

        TextView wallet = findViewById(R.id.wallet);
        wallet.setText(getkey("wallet"));

        TextView coupons = findViewById(R.id.coupons);
        coupons.setText(getkey("coupons"));

        TextView lastdriver = findViewById(R.id.lastdriver);
        lastdriver.setText(getkey("last_driver"));

        //lastdriver
        lastdriverimage = findViewById(R.id.lastdriverimage);
        lastdriver_name = findViewById(R.id.lastdriver_name);
        lastdriver_carno = findViewById(R.id.lastdriver_carno);
        reportcomplaint = findViewById(R.id.reportcomplaint);
        reportcomplaint.setText(getkey("report_complaint"));
        reportcomplaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> jsonParams1 = new HashMap<String, String>();
                jsonParams1.put("user_id", UserID);//"584aa27ccae2aa741a00002f");
                if (isInternetPresent) {
                    postRequestComplaintPage(Iconstant.complaint_list_url, jsonParams1);
                } else {
                    Alert(getkey("action_error"), getkey("alert_nointernet"));

                }
            }
        });



        /*total trip*/
        tv_total_trip = (TextView) findViewById(R.id.tv_total_trip);
        tv_total_trip.setText(getkey("completed"));
        tv_cancel_trip = (TextView) findViewById(R.id.tv_cancel_trip);
        tv_cancel_trip.setText(getkey("cancel_trip"));
        tv_bidding_trip = (TextView) findViewById(R.id.tv_bidding_trip);
        tv_bidding_trip.setText(getkey("bidding_trip"));
        tv_sos_trip = (TextView) findViewById(R.id.tv_sos_trip);
        tv_sos_trip.setText(getkey("sos_call"));




        tv_complaint_trip = (TextView) findViewById(R.id.tv_complaint_trip);
        tv_complaint_trip.setText(getkey("total_spent"));
        /*values*/
        tv_total_trip_value = (TextView) findViewById(R.id.tv_total_trip_value);
        myTrip_today_highlight_cancelTrip_TextView = (TextView) findViewById(R.id.myTrip_today_highlight_cancelTrip_TextView);
        tv_total_bidding_value = (TextView) findViewById(R.id.tv_total_bidding_value);
        myTrip_today_highlight_sosTrip_TextView = (TextView) findViewById(R.id.myTrip_today_highlight_sosTrip_TextView);
        myTrip_today_highlight_complaint_TextView = (TextView) findViewById(R.id.myTrip_today_highlight_complaint_TextView);

        lastdrivercarimage = (ImageView) findViewById(R.id.lastdrivercarimage);
        /*profile*/
//        myTrip_trip_topTrip_profile_name_textView = findViewById(R.id.myTrip_trip_topTrip_profile_name_textView);
//        myTrip_trip_topTrip_location_textView =  findViewById(R.id.myTrip_trip_topTrip_location_textView);
        myTrip_trip_topTrip_profile_imageView = findViewById(R.id.myTrip_trip_topTrip_profile_imageView);
        /*lasttrip*/
        tv_last_date = findViewById(R.id.tv_last_date);
        tv_last_time = findViewById(R.id.tv_last_time);
        tv_last_cartype = findViewById(R.id.tv_last_cartype);
//        tv_last_distance = findViewById(R.id.tv_last_distance);
//        tv_last_amount =  findViewById(R.id.tv_last_amount);
//        tv_last_ride_stztus =  findViewById(R.id.tv_last_ride_stztus);
//        lasttrip_layout = (LinearLayout) findViewById(R.id.lasttrip_layout);
//        viewdetail_tv =  findViewById(R.id.viewdetail_tv);
        /*View All rides*/
        view_all_ride_list = findViewById(R.id.view_all_ride_list);
        view_all_ride_list.setText(getkey("view_all_past_trips"));
        /*backbt*/
        backbtn = findViewById(R.id.backbtn);

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        hightlights.setText(year + " " +getkey("higlits"));
        paymentsummary.setText(getkey("payment_summary")+" " + year);


        Intent intent = getIntent();

        if (intent.hasExtra("Class")) {
            type = intent.getStringExtra("Class");
        }
        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        postData();
    }

    private void postRequestComplaintPage(String Url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(RideListHomePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        ServiceRequest mRequest = new ServiceRequest(RideListHomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------ComplaintPage Response----------------" + response);

                String Sstatus = "", Scurrency_code = "", user_image = "", Scurrentbalance = "", Smessage = "", Scurrency = "", user_name = "", user_location = "", user_id = "", member_since = "", Date = "", StotalTicketCount = "", SopenTicketCount = "", SclosedTicketCount = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject jobject = object.getJSONObject("response");
                    StotalTicketCount = jobject.getString("total_ticket_Count");
                    SopenTicketCount = jobject.getString("open_ticket_Count");
                    SclosedTicketCount = jobject.getString("closed_ticket_Count");


                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (jobject.length() > 0) {
                            Date = jobject.getString("today_datetime");
                            todayDate = Date;
                            Object check_object1 = jobject.get("user_profile");
                            if (check_object1 instanceof JSONObject) {
                                JSONObject user_detail_object = jobject.getJSONObject("user_profile");
                                if (user_detail_object.length() > 0) {
                                    user_name = user_detail_object.getString("user_name");
                                    user_id = user_detail_object.getString("user_id");

                                    member_since = user_detail_object.getString("member_since");
                                    user_location = user_detail_object.getString("location_name");
                                    user_image = user_detail_object.getString("user_image");

                                    userImage = user_image;
                                    userName = user_name;
                                    userLocation = user_location;
                                    session.setcomplaintUserDetail(userName, userImage, userLocation);

                                }
                            }

                            Object check_object = jobject.get("subject");
                            if (check_object instanceof JSONArray) {
                                JSONArray complaint_list_jsonArray = jobject.getJSONArray("subject");
                                if (complaint_list_jsonArray.length() > 0) {
                                    isComplaintsAvailable = true;
                                    complaint_itemList.clear();
                                    complaint_itemList1.clear();
                                    complaint_itemList1.add("SELECT SUBJECT");
                                    complaint_itemList2.add("0");

                                    for (int i = 0; i < complaint_list_jsonArray.length(); i++) {
                                        JSONObject earning_list_obj = complaint_list_jsonArray.getJSONObject(i);
                                        ComplaintsPojo complaintsPojo = new ComplaintsPojo();
                                        complaintsPojo.setoptionsId(earning_list_obj.getString("option_id"));
                                        complaintsPojo.setsubjectId(earning_list_obj.getString("subject_id"));
                                        complaintsPojo.setsubjectName(earning_list_obj.getString("subject_name"));
                                        complaintsPojo.setReasonStatus("0");
                                        complaint_itemList.add(complaintsPojo);
                                        complaint_itemList1.add(earning_list_obj.getString("subject_name"));
                                        complaint_itemList2.add(earning_list_obj.getString("option_id"));
                                    }
                                    isComplaintsAvailable = true;
                                } else {
                                    isComplaintsAvailable = false;
                                }
                            } else {
                                isComplaintsAvailable = false;
                            }
                            if (dialog != null) {
                                dialog.dismiss();
                            }

                            Intent intent = new Intent(RideListHomePage.this, CreateNew_Complaint.class);
                            intent.putExtra("user_image", userImage);
                            intent.putExtra("location_name", userLocation);
                            intent.putExtra("user_name", userName);
                            intent.putExtra("today_datetime", todayDate);
                            intent.putExtra("ride_id", lastRideID);
                            intent.putExtra("valuesfrom", "RideDetailPage");
                            intent.putStringArrayListExtra("complaintsubjects", complaint_itemList1);
                            intent.putStringArrayListExtra("complaintoptionid", complaint_itemList2);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                        }
                    } else {
                        Smessage = jobject.getString("response");
                    }
                    if (dialog != null) {
                        dialog.dismiss();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (Sstatus.equalsIgnoreCase("1")) {


                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        if (isInternetPresent) {
            PostRequest_myrideHome(Iconstant.myride_DashBoard, jsonParams);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));

        }
    }

    private void PostRequest_myrideHome(String myrides_url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(RideListHomePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));
        System.out.println("----------myrideHome--json-------------" + jsonParams);
        mRequest = new ServiceRequest(RideListHomePage.this);

        mRequest.makeServiceRequest(myrides_url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-----------Request_myrideHome---reponse-------------------" + response);

                String Sstatus = "", Smessage = "", currency = "", rides_count = "", rides_distance = "", week_rides_count = "", week_rides_distance = "",
                        month_rides_count = "", month_rides_distance = "", completed_rides = "", cancelled_ride = "", bidding_trip = "", sos_trip = "", complaint_trip = "",
                        tot_spent = "", last_trip_datetime = "", user_name = "", last_trip_ride_id = "", user_image = "", user_review = "", city = "", last_trip_dt = "", last_paid_mode = "", catgeory = "", last_paid_amount = "", last_ride_status = "", last_ride_distance = "";

                try {
                    JSONObject obj = new JSONObject(response);
                    Sstatus = obj.getString("status");


                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject obj_response = obj.getJSONObject("response");

                        if (obj_response.length() > 0) {
                            JSONObject obj_today = obj_response.getJSONObject("today");

                            rides_count = obj_today.getString("rides_count");
                            rides_distance = obj_today.getString("rides_distance");

                            JSONObject obj_week = obj_response.getJSONObject("week");

                            week_rides_count = obj_week.getString("rides_count");
                            week_rides_distance = obj_week.getString("rides_distance");

                            JSONObject obj_month = obj_response.getJSONObject("month");

                            month_rides_count = obj_month.getString("rides_count");
                            month_rides_distance = obj_month.getString("rides_distance");


                            JSONObject obj_today_highlight = obj_response.getJSONObject("today_highlight");

                            completed_rides = obj_today_highlight.getString("completed_rides");
                            cancelled_ride = obj_today_highlight.getString("cancelled_ride");
                            bidding_trip = obj_today_highlight.getString("bidding_trip");
                            sos_trip = obj_today_highlight.getString("sos_trip");
                            complaint_trip = obj_today_highlight.getString("complaint");
                            tot_spent = obj_today_highlight.getString("tot_spent");

                            JSONObject obj_user_profile = obj_response.getJSONObject("user_profile");

                            user_name = obj_user_profile.getString("user_name");
                            user_image = obj_user_profile.getString("user_image");
                            user_review = obj_user_profile.getString("user_review");
                            city = obj_user_profile.getString("city");

                            JSONObject obj_last_trip = obj_response.getJSONObject("last_trip");
                            if (obj_last_trip.length() > 0) {
                                if(!obj_last_trip.getString("cat_image").equalsIgnoreCase("")){
                                    RequestOptions options = new RequestOptions();
                                    options.centerCrop();
                                    Glide.with(RideListHomePage.this)
                                            .load(obj_last_trip.getString("cat_image"))
                                            .placeholder(R.drawable.car_new1).
                                            error(R.drawable.car_new1)
                                            .apply(options)
                                            .fitCenter().into(lastdrivercarimage);
                                }
                                last_trip_dt = obj_last_trip.getString("last_trip_dt");
                                last_paid_mode = obj_last_trip.getString("last_paid_mode");
                                catgeory = obj_last_trip.getString("catgeory");
                                last_paid_amount = obj_last_trip.getString("last_paid_amount");
                                last_ride_status = obj_last_trip.getString("last_ride_status");
                                last_ride_distance = obj_last_trip.getString("last_ride_distance");
                                last_trip_ride_id = obj_last_trip.getString("last_trip_ride_id");
                                lastRideID = last_trip_ride_id;
                                last_trip_datetime = obj_last_trip.getString("last_trip_datetime");

                                tv_last_date.setText(obj_last_trip.getString("last_trip_date"));
                                tv_last_time.setText(obj_last_trip.getString("last_trip_time"));

                                JSONObject obj_paymetnsummary = obj_response.getJSONObject("paysummery");

                                if (obj_paymetnsummary.length() > 0) {
                                    cashvalue.setText(obj_paymetnsummary.getString("cash"));
                                    cardvalue.setText(obj_paymetnsummary.getString("card"));
                                    walletvalue.setText(obj_paymetnsummary.getString("wallet"));
                                    couponsvalue.setText(obj_paymetnsummary.getString("coupon"));
                                }

                                JSONObject obj_lastdriver = obj_last_trip.getJSONObject("last_driver");
                                if (obj_lastdriver.length() > 0) {
                                    Picasso.with(RideListHomePage.this).load(obj_lastdriver.getString("image")).placeholder(R.drawable.new_no_user_img).into(lastdriverimage);
                                    lastdriver_name.setText(obj_lastdriver.getString("name"));
                                    lastdriver_carno.setText(obj_lastdriver.getString("vehicle_number"));
                                }
                            }

                        }
                        currency = obj_response.getString("currency");
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {

                        myTrip_totalTrip_today_km_textView.setText(rides_distance.toUpperCase());
                        myTrip_totalTrip_today_textView.setText(rides_count);
                        myTrip_totalTrip_thisWeek_textView.setText(week_rides_count);
                        myTrip_totalTrip_thisWeek_km_textView.setText(week_rides_distance.toUpperCase());
                        myTrip_totalTrip_thisMonth_textView.setText(month_rides_count);
                        myTrip_totalTrip_thisMonth_km_textView.setText(month_rides_distance.toUpperCase());

                        tv_total_trip_value.setText(completed_rides);
                        myTrip_today_highlight_cancelTrip_TextView.setText(cancelled_ride);
                        tv_total_bidding_value.setText(bidding_trip);
                        myTrip_today_highlight_sosTrip_TextView.setText(sos_trip);
                        myTrip_today_highlight_complaint_TextView.setText(currency + " " + tot_spent);

//                        myTrip_trip_topTrip_profile_name_textView.setText(user_name);
//                        myTrip_trip_topTrip_location_textView.setText(city);
                        Picasso.with(RideListHomePage.this).load(user_image).placeholder(R.drawable.thanku_profile).into(myTrip_trip_topTrip_profile_imageView);


                        tv_last_cartype.setText(/*"CRN NO" + last_trip_ride_id + "\n" + */catgeory);
//                        tv_last_distance.setText(last_ride_distance);
//                        tv_last_amount.setText(currency + " " + last_paid_amount);
//                        tv_last_ride_stztus.setText(last_ride_status);

//                        viewdetail_tv.setText("VIEW DETAIL");
//                        if (last_ride_status.equalsIgnoreCase("COMPLETED")) {
//                            viewdetail_tv.setBackgroundResource(R.drawable.blue_gradient);
//                        } else if (last_ride_status.equalsIgnoreCase("CANCELLED")) {
//                            viewdetail_tv.setBackgroundResource(R.drawable.red_gradient);
//                        } else {
//                            viewdetail_tv.setBackgroundResource(R.drawable.yellow_gradient);
//                        }
                        if (last_trip_dt.equalsIgnoreCase("") && last_trip_dt.equalsIgnoreCase("")) {
//                            lasttrip_layout.setVisibility(View.GONE);
                        } else {
//                            lasttrip_layout.setVisibility(View.VISIBLE);
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    } else {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (type.equals("Home")) {
           /* Intent intent = new Intent(RideListHomePage.this, Navigation_new.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);*/
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(RideListHomePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}
