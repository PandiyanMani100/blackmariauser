package com.blackmaria;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextViewForm;
import com.blackmaria.widgets.PkDialog;

import java.util.HashMap;

/**
 * Created by user129 on 3/1/2018.
 */
@SuppressLint("Registered")
public class CashOutActivity extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView img_back;
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String UserID = "", sSecurePin = "", mode = "", amount = "", currency = "", current_username = "", mobileno = "", country_code = "";

    private RelativeLayout Rl_cancel, Rl_cashExchange, Rl_proceed, Rl_help;
    private CustomTextViewForm Tv_mobileno, Tv_currentUsername, Tv_transfer_amount, Tv_networkFee, Tv_received_amount, cashout_transf_currecy, cashout_networkfee_currency, cashout_received_curency;


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CashOutActivity.this, Navigation_new.class);
                        startActivity(intent1);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CashOutActivity.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CashOutActivity.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cashout_proceed_layout);

        initization();

    }


    private void initization() {

        session = new SessionManager(CashOutActivity.this);
        cd = new ConnectionDetector(CashOutActivity.this);
        isInternetPresent = cd.isConnectingToInternet();
        sSecurePin = session.getSecurePin();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        mobileno = info.get(SessionManager.KEY_PHONENO);
        country_code = info.get(SessionManager.KEY_COUNTRYCODE);

        HashMap<String, String> user_name = session.getUserDetails();
        current_username = user_name.get(SessionManager.KEY_USERNAME);



        Intent in = getIntent();
        mode = in.getStringExtra("payment_code");
        amount = in.getStringExtra("amount");
        currency = in.getStringExtra("currency");


        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        Tv_mobileno = (CustomTextViewForm) findViewById(R.id.csahout_mobile_no);
        Tv_currentUsername = (CustomTextViewForm) findViewById(R.id.cashout_name);
        Tv_transfer_amount = (CustomTextViewForm) findViewById(R.id.cashout_transf_amount);
        Tv_networkFee = (CustomTextViewForm) findViewById(R.id.cashout_networkfee);
        Tv_received_amount = (CustomTextViewForm) findViewById(R.id.cashout_received_amount);
        Rl_cancel = (RelativeLayout) findViewById(R.id.cashout_transf_cancel);
        Rl_help = (RelativeLayout) findViewById(R.id.cashout_help);
        Rl_cashExchange = (RelativeLayout) findViewById(R.id.cash_excahnge_rl);
        Rl_proceed = (RelativeLayout) findViewById(R.id.cashout_proceed);
        cashout_transf_currecy = (CustomTextViewForm) findViewById(R.id.cashout_transf_currecy);
        cashout_networkfee_currency = (CustomTextViewForm) findViewById(R.id.cashout_networkfee_currency);
        cashout_received_curency = (CustomTextViewForm) findViewById(R.id.cashout_received_curency);


        Tv_mobileno.setText(getResources().getString(R.string.mylogin_dialog_form_mobile_no_label)+" "+":"+" "+country_code + " " + mobileno);
        Tv_currentUsername.setText(current_username);
        cashout_transf_currecy.setText(currency);
        cashout_networkfee_currency.setText(currency);
        cashout_received_curency.setText(currency);
        Tv_transfer_amount.setText(String.format("%.2f",Double.parseDouble(amount)));
        Tv_networkFee.setText("0.00");
        Tv_received_amount.setText(String.format("%.2f",Double.parseDouble(amount)));


        Rl_cancel.setOnClickListener(this);
        Rl_cashExchange.setOnClickListener(this);
        Rl_proceed.setOnClickListener(this);
        Rl_help.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {


        if (v == Rl_cancel) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (v == Rl_help) {

            CashOutHelpDialog();

        }else if (v == img_back) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (v == Rl_cashExchange) {


            Alert(getResources().getString(R.string.sry_cash_nt_available),getResources().getString(R.string.pls_try_later));

        } else if (v == Rl_proceed) {


            Intent in = new Intent(CashOutActivity.this, CloudTransferAmountPinEnter1.class);
            in.putExtra("flag", "cash_withdrawal");
            in.putExtra("amount", amount);
            in.putExtra("payment_code", mode);
            startActivity(in);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        }


    }


    private void CashOutHelpDialog(){

        final Dialog dialog = new Dialog(CashOutActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cashout_help_dialog);


        final RelativeLayout cancel=(RelativeLayout) dialog.findViewById(R.id.dig_cancel);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });





        dialog.show();



    }



    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CashOutActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }



}



