package com.blackmaria;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.blackmaria.adapter.BookMyRide_Adapter;
import com.blackmaria.adapter.BookingPage4Adapter;
import com.blackmaria.adapter.BookingPaymentMethodAdapter;
import com.blackmaria.adapter.ConfirmbookingVehiclelistAdapter;
import com.blackmaria.adapter.CustomInfoWindowAdapter;
import com.blackmaria.adapter.PaymentListAdapter1;
import com.blackmaria.adapter.PersonListAdapter;
import com.blackmaria.adapter.PlaceSearchAdapter;
import com.blackmaria.adapter.SelectVechileAdapter;
import com.blackmaria.adapter.StripeListVIewAdapter;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.newdesigns.RecentTrips;
import com.blackmaria.newdesigns.fastwallet.Fastpayhome;
import com.blackmaria.newdesigns.fastwallet.Fastpaypincheck;
import com.blackmaria.pojo.BookingPaymentListPojo;
import com.blackmaria.pojo.BookingPojo1;
import com.blackmaria.pojo.Getmapview;
import com.blackmaria.pojo.HomePojo;
import com.blackmaria.pojo.MultiDropPojo;
import com.blackmaria.pojo.PersonPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CountryDialCode;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.waypoint.AbstractRouting;
import com.blackmaria.waypoint.RoutingListener;
import com.blackmaria.waypoint.SAmple;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.latlnginterpolation.GMapV2GetRouteDirection;
import com.blackmaria.latlnginterpolation.LatLngInterpolator;
import com.blackmaria.latlnginterpolation.MarkerAnimation;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextCambriaItalic;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.devspark.appmsg.AppMsg;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.xendit.Models.Card;
import com.xendit.Models.Token;
import com.xendit.Models.XenditError;
import com.xendit.TokenCallback;
import com.xendit.Xendit;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.blackmaria.R.id.book_my_ride_trip_details_carview;
import static com.google.android.gms.maps.model.JointType.ROUND;

/*user144*/
public class BookingPage4 extends FragmentActivityHockeyApp implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, RoutingListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    //// testing phantom
    private List<LatLng> polyLineList_us;
    private Marker marker_us;
    private float v_us;
    private double lat_us, lng_us;
    private Handler handler_us;
     Dialog showdsialog = null;
    private LatLng startPosition_us, endPosition_us;
    private int index_us, next_us;
    TextView coponadded;
    private LatLng sydney_us;
    private Button button_us;
    private EditText destinationEditText_us;
    private String destination_us = "guindy";
    private PolylineOptions polylineOptions_us, blackPolylineOptions_us;
    private Polyline blackPolyline_us, greyPolyLine_us;

    // testing phantom
    private LinearLayout bottom_sheet;


    LanguageDb mhelper;
    private Context context;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private GoogleMap googleMap;
    private static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
    private SessionManager session;
    GPSTracker gps;
    private Bitmap bmp;
    private LinearLayout Ll_SelectVechile, Ll_OneWay, Ll_Return, Ll_MultipleStop, Ll_Tracking;
    private RelativeLayout Rl_Back, Rl_PickupNow;
    public boolean trip_selected = false;
    String book_ref,user_name,gender,mobile_number,country_code;
    // private CustomTextView Tv_PickupLater;
    private TextView Tv_PickupNow;
    private ImageView Iv_Back;
    private TextView Tv_TripType, Tv_Distance, Tv_CarArriveTime;
    private RelativeLayout Rl_PickUpLayout, Rl_ConfirmCancelLayout, Rl_destination_layout;
    private RelativeLayout lytpayment_method, lyt_book_now, book_my_ride_selectcar_triptype_layout, book_my_ride_rideNow_layout;
    private RelativeLayout layout_home_iamge2;
    private RelativeLayout layout_home_iamge, Rl_Alert_layout, Rl_roadimgelayout;
    private RelativeLayout Rl_Bottom_layout;
    private TextView TV_Alert_textview;
    private ImageView Iv_OneWay, Iv_Return, Iv_MultipleStop, Iv_Tracking, Iv_home;
    private LinearLayout Ll_Coupon, Ll_TripSummary, Ll_Person,layout_booking_confirm;

    private ServiceRequest mRequest;
    private BookMyRide_Adapter adapter;
    private PlaceSearchAdapter PlaceSearchAdapter;
    private View inflateRetry, inflateuserinfo;
    private boolean is_stopover_value = false;
    private ArrayList<HomePojo> driver_list = new ArrayList<HomePojo>();
    private ArrayList<HomePojo> category_list = new ArrayList<HomePojo>();
    private boolean driver_status = false;
    private boolean category_status = false;
    private boolean ratecard_status = false;
    private boolean main_response_status = false;
    private boolean isLoading = false;
    private String ScurrencySymbol = "";
    private String UserID = "", CategoryID = "";
    private String CarAvailable = "", fareAmount = "", fareAmount1 = "";
    private String ScarType = "";
    private String selectedType = "", bookforLater = "0";
    private String SdestinationLatitude = "";
    public String SdestinationLongitude = "";
    private String stopOverCount = "";
    public String SdestinationLocation = "";
    private Typeface face;
    private String bookReference = "No";
    private String someOneNmame = "";
    private String someOneGender = "", callingFrom = "", tripTypeMode = "";
    private String SpickupLatitude = "", SpickupLongitude = "", SpickupLocation = "", Sextracheck = "";
    private ArrayList<MultiDropPojo> multiple_dropList;
    private int timer_request_code = 100;
    private int placeSearch_request_code = 200;
    private int multiplePlaceSearch_request_code = 300;
    private int coupon_request_code = 400;
    private int favoriteList_request_code = 500;
    private static final AndroidHttpClient ANDROID_HTTP_CLIENT = AndroidHttpClient.newInstance(BookingPage4.class.getName());
    private List<Polyline> polylines;
    private List<Polyline> polylines1;
    private ArrayList<LatLng> wayPointList;
    private LatLngBounds.Builder wayPointBuilder;
    private LatLng startWayPoint, endWayPoint;
    private String sWayPointStatus = "Confirmed";
    private String[] multipleDropLatLng;
    private SimpleDateFormat mFormatter = new SimpleDateFormat("MMM/dd,hh:mm aa");
    private SimpleDateFormat coupon_mFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat coupon_time_mFormatter = new SimpleDateFormat("hh:mm aa");
    private SimpleDateFormat mTime_Formatter = new SimpleDateFormat("HH");
    //-----Declaration For Enabling Gps-------
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    private String sMultipleDropStatus = "0";
    private String STrip_Type = "";
    private int SListPosition;
    private int NoOfSeats;
    private String StrackingPercent = "";

    private TextView bookMyRidTripFare;
    //------Declaration for Coupon code-----
    private TextView Tv_EstimateAmount, vechileName;
    private String Str_referralCode = "", str_FreeWaitingTime = "", str_FreeWaitingRate = "";
    private Dialog dialog;
    Dialog xenditDialog;
    private ListView listview;
    private String selectedDate = "", UserName = "";
    private String selectedTime = "";
    LinearLayout lay1,lay2,lay3,lay4,lay5,lay6,lay7,lay8;
    TextView text1,text2,text3,text4,text5,text6,text7,text8;
    private String response_time = "";

    private String riderId = "", Str_driver_image = "", carCatImage = "";
    private String ScouponSource = "", ScouponCode = "";
    private String Stracking = "0";
    private String SetaAmount = "", SetaDistance = "";
    public static Activity BookingPage3_class;
    private Dialog admin_track_dialog;
    private CheckBox CB_terms;
    RelativeLayout selectCarCatImage, book_my_ride_bottom_main_layout1;
    private String isRetrunOrMulti = "Return Trip";
    private PersonListAdapter personListAdapter;
    private ArrayList<PersonPojo> person_itemList;
    private String sCarImg = "";
    private ArrayList<HomePojo> paymnet_list;
    private PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 299;
    private double MyCurrent_lat = 0.0, MyCurrent_long = 0.0;
    private ImageView currentLocation_image;
    private RefreshReceiver refreshReceiver;
    private String NoOfPerson = "1";
    private ListView person_listview;
    private SelectVechileAdapter ag;
    private boolean payment_list_status = false;
    private ExpandableHeightListView Hlv_paymentList;
    private BookingPage4Adapter paymentListAdapter;
    private String SpaymentMode = "";
    private RelativeLayout Rl_Payment_layout, fav_lyout, back_lyout;
    private TextView tv_setdropofflocation;
    private LinearLayout bookforsomeone;
    private CardView CV_TripType;

    LinearLayout alertlayout;
    int count = 1;
    String paymenttype = "";
    private Boolean moveNext = true;
    private Double Dlatitude = 0.0, Dlongitude = 0.0;
    //    private CustomTextView book_my_ride_set_destination_textview;
    private ProgressBar loading_spinner;
    private String WaitingTime = "";
    private String tripTypeSpinner = "";
    String bookingReference = "No";
    private CustomEdittext label_text1, mobileNumber;
    private CustomTextCambriaItalic Tv_countryCode;
    private TextView Tv_city;
    private CountryPicker picker;
    private ArrayList<String> itemList_location = new ArrayList<String>();
    private ArrayList<String> itemList_placeId = new ArrayList<String>();
    private boolean isdataAvailable = false;
    private String Str_Page = "";
    private String Str_stopOverStatus = "no";
    private String SmultiDropLocation = "", SmultiDropLatitude = "", SmultiDropLongitude = "", Sselected_location = "";
    private String Slocation = "", SLatitude = "", SLongitude = "";
    private int ResultCode = 300;
    private MapFragment mapFragment;
    private GMapV2GetRouteDirection v2GetRouteDirection;
    private Document document;
    private Marker stopOverMarkerNew;
    private boolean hideleftmarker = false;

    private Marker newMovingCarMarkers[];


    float bearing, bearing1, bearing2;
    Location from, from2, from3 = null;
    Location to, to2, to3 = null;

    Location fromNew = null;
    Location toNew = null;

    private HashMap<String, List<Location>> movingLocation;
    private ArrayList<Location> fromLocation;
    private ArrayList<Location> ToLocation;

    Location newFromLocation[] = null;
    Location newDropLocation[] = null;

    private URL url = null;
    private Bitmap image = null;
    private String ScurrentLat = "", ScurrentLan = "";
    private LatLng latlan = null;
    private Double phatomRadius = 6378.14;
    private Double latPhantom, latPhantom1 = 0.0;
    private Double lanPhantom, lanPhantom1 = 0.0;
    private Double anglePhantm = 0.0;
    private Map<String, List<Double>> phantomMap;
    private List<Double> phantomLat;
    private List<Double> phantomLan;
    private LatLngInterpolator mLatLngInterpolator = null, mLatLngInterpolator1 = null, mLatLngInterpolator2 = null;
    private String fail_response = "";
    private Runnable runnable = null;
    private boolean isRunnable = true;
    private Runnable myRunnable = null;
    private String sLatitude = "";
    private String sLongitude = "";
    private RelativeLayout centerMArker;
    private ImageView Iv_CenterMarker, IV_selectcategoryImage;
    private boolean dropclick = true;
    private LinearLayout Rl_droplocation;
    private CustomEdittext book_my_ride_set_destination_edittextview;
    String desAddress = "";
    private boolean isCameraChange = true;
    private String placeId = "";
    private String cityName = "", cityNameNew = "";
    private TimerTask timerTask;
    TextView havecoupp;
    private Handler handlerNew;
    private Timer timer;
    private TextView dragdropTv;
    private boolean isNightMode = true;
    //    private CustomTextView paymentname, person_count_tv;
    private ImageView IV_schedule, IV_dayNight;
    private LinearLayout.LayoutParams params = null;
    private String userDetails = "";
    private MarkerOptions markerDefault, markerDefault1;
    private String selectedCarType = "";
    private Marker DefaultMarkerNew, DefaultMarkerNew1;
    private boolean isInfoWindowEnable = true;
    private TextView dateChoose;
    private TextView timesChoose;
    private String currentTime = "", coupon_selectedDate = "", coupon_selectedTime = "", strWalletAmount = "", strCurrency = "";
    private String sTodayDate = "", sSelectedDate = "";
    private int CurrentMinute, SelectedMinutes;

    private int Year, Month, DayOfMonth, Hours, Minitues;
    private SpinKitView spinKitView;
    private ArrayList<BookingPaymentListPojo> paymentListBooking = null;
    private ArrayList<BookingPaymentListPojo> paymentCardListBooking = null;
    private BookingPaymentMethodAdapter Eadapter;

    private Spinner spinner_month, spinner_year;
    private String expYear = "", expMonth = "", cardNumber = "", card_info = "", card_id = "";
    private int expYearNum = 0, expMnthNUm = 0;
    private Xendit xendit = null;
    private PkDialog mDialog = null;
    private Double amount = 0.0;
    public static String PUBLISHABLE_KEY = "";
    public static String CURRENCYCONVERSIONKEY_KEY = "";
    private SmoothProgressBar smoothbar, stripSmoothbar;
    private EditText cardCvv;

    private StripeListVIewAdapter stripeAdapter;
    private ListView stripeListView;
    private String selectedStripeCard = "";
    EditText cardNumberEt;
    private ServiceRequest stripRequest;

    private TextView loadingTv, yearss, months, viewpastrips;
    private String CountryCodeStr = "", mobileNumberstr = "";
    private GPSTracker gpsTracker;
    private View MarkerView = null;
    private String bookingSomeElseflag = "1";
    private int currentMarker = 0;


    private boolean isFromFindPlace = false;
    private String noOfMale = "", noOfFemale = "", noOfChildren = "";
    private String xendit_globalcheck = "";
    private String proceed_status_global = "", mode_global = "";

    //new designs
    private TextView tv_bookforsome;
    private Getmapview mapview_pojo;
    private TextView tv_username_marker, tv_tripType_tv, tv_vehicle_type_tv, droppofftv;
    //private RecyclerView listview_rc;
    private ConfirmbookingVehiclelistAdapter vehiclelistada;
    private float bearingValue = 0;
    private double firstlattitudewhencomes = 0, firstlongitudewhencomes = 0;
    private boolean isfirstlocationupdated = false;
    private PkDialog picksome;
    private int pastrips = 110;


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {


                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(BookingPage4.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(BookingPage4.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            } else if (intent.getAction().equals("com.package.finish.BookingPage")) {
                if (dialog != null) {
                    dialog.dismiss();
                }

                if (mRequest != null) {
                    mRequest.cancelRequest();
                }
                moveNext = false;
                finish();
            } else if (intent.getAction().equals("com.app.pushnotification.RideAccept")) {

                session.setCouponCode("", "");
                session.setReferralCode("", "");

                Intent i = new Intent(BookingPage4.this, TrackRideAcceptPage.class);
                i.putExtra("driverID", intent.getStringExtra("driverID"));
                i.putExtra("driverName", intent.getStringExtra("driverName"));
                i.putExtra("driverImage", intent.getStringExtra("driverImage"));
                i.putExtra("driverRating", intent.getStringExtra("driverRating"));
                i.putExtra("driverTime", intent.getStringExtra("driverTime"));
                i.putExtra("rideID", intent.getStringExtra("rideID"));
                i.putExtra("driverMobile", intent.getStringExtra("driverMobile"));
                i.putExtra("driverCar_no", intent.getStringExtra("driverCar_no"));
                i.putExtra("driverCar_model", intent.getStringExtra("driverCar_model"));

                i.putExtra("flag", "1");

                startActivity(i);

                if (mRequest != null) {
                    mRequest.cancelRequest();
                }


                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("com.app.timerPage.finish");
                sendBroadcast(broadcastIntent);

                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        System.out.println("***********************GC Cleared************");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.newbookingpages4);
        mhelper= new LanguageDb(this);
        initialize();
        context = BookingPage4.this;
        BookingPage3_class = BookingPage4.this;

        initializeMap();

        viewpastrips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BookingPage4.this, RecentTrips.class);
                startActivityForResult(intent, pastrips);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        alertlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roadRulesPopUp();
            }
        });


        currentLocation_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CurrentLocationMethod();
            }
        });

        layout_home_iamge2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GohomePopUp(getResources().getString(R.string.close_booking));

            }
        });

        layout_home_iamge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GohomePopUp(getResources().getString(R.string.close_booking));
            }
        });

//        IV_schedule.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                book_my_ride_selectcar_triptype_layout.setVisibility(View.VISIBLE);
//                select_RideLater_Dialog();
//                //pickupLaterPoUp();
//            }
//        });
        tv_bookforsome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookSomeOne();
            }
        });
        IV_dayNight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (googleMap != null) {
                        boolean isNightMode = session.getNightMode();
                        if (isNightMode == true) {
                            NightModeAlert("", getResources().getString(R.string.lable_daymode1), false);
                        } else {
                            NightModeAlert("", getResources().getString(R.string.lable_nightmode1), true);
                        }

                    } else {
                        initializeMap();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        Rl_PickupNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bookforLater.equalsIgnoreCase("1")) {
                    selectedType = "1";
                } else {
                    selectedType = "0";
                    selectedDate = coupon_mFormatter.format(new Date());
                    selectedTime = coupon_time_mFormatter.format(new Date());
                }
                System.out.println("========Tv_PickupNow=======" + Tv_PickupNow.getText().toString());

//                String value = book_my_ride_set_destination_textview.getText().toString();
                String value2 = book_my_ride_set_destination_edittextview.getText().toString();
                if (book_my_ride_set_destination_edittextview.getText().length() > 0/*equalsIgnoreCase("")*/) {
                    if (getResources().getString(R.string.ok_lable).equalsIgnoreCase(Tv_PickupNow.getText().toString())) {

                        if (bookforLater.equalsIgnoreCase("1")) {
                            selectedDate = coupon_selectedDate;
                            selectedTime = coupon_selectedTime;
                        } else {
                            selectedDate = coupon_mFormatter.format(new Date());
                            selectedTime = coupon_time_mFormatter.format(new Date());
                        }

                        if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(STrip_Type)) {

                            Intent intent = new Intent(BookingPage4.this, MultiDropLocation_New.class);
                            intent.putExtra("pickupAddress", SpickupLocation);
                            intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                            intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));

                            intent.putExtra("destinationAddress", SdestinationLocation);
                            intent.putExtra("destinationLatitude", SdestinationLatitude);
                            intent.putExtra("destinationLongitude", SdestinationLongitude);

                            intent.putExtra("pickup_date", selectedDate);
                            intent.putExtra("pickup_time", selectedTime);
                            intent.putExtra("CategoryID", CategoryID);
                            System.out.println("=================Category_Selected_id1==============" + CategoryID);
                            intent.putExtra("type", selectedType);
                            intent.putExtra("tracking", Stracking);
                            intent.putExtra("mode", STrip_Type);
                            intent.putExtra("waitingTime", WaitingTime);
                            System.out.println("======WaitingTime2=========" + WaitingTime);
                            boolean isstopover = false;
                            if ("yes".equalsIgnoreCase(Str_stopOverStatus)) {
                                isstopover = true;
//                                String value = book_my_ride_set_destination_edittextview.getText().toString().trim();
//                                String value2 = book_my_ride_set_destination_textview.getText().toString();
//                                Log.d("calue,",value2);
//                                if(!value.equalsIgnoreCase("")){
                                if (!SmultiDropLocation.trim().equalsIgnoreCase("")) {
                                    MultiDropPojo pojo = new MultiDropPojo();
                                    pojo.setPlaceName(SmultiDropLocation);
                                    pojo.setPlaceLat(SmultiDropLatitude);
                                    pojo.setPlaceLong(SmultiDropLongitude);
                                    pojo.setwaitingTimet(WaitingTime);
                                    multiple_dropList.add(pojo);
                                    Bundle bundleObject = new Bundle();
                                    bundleObject.putSerializable("MultipleDropLocation_List", multiple_dropList);
                                    intent.putExtras(bundleObject);
                                } else {
                                    Alert1(getkey("action_error"), getResources().getString(R.string.enter_stopover));
                                }
//                                }else{
//                                    multistop_stopover = false;
//                                    Alert1("Sorry", "Kindly enter stop over location");
//                                }

                            }
                            if (isstopover) {
                                if (!SmultiDropLocation.trim().equalsIgnoreCase("")) {
//                                    if (is_stopover_value) {
                                    startActivity(intent);
                                    finish();
//                                    } else {
//                                        Alert1("Sorry", "Kindly enter stop over location");
//                                    }
                                } else {
                                    Alert1(getkey("action_error"), getResources().getString(R.string.enter_stopover));
                                }
                            } else {
                                startActivity(intent);
                                finish();
                            }

                        } else {
                            stopTimer();
                            Intent intent = new Intent(BookingPage4.this, BookingPage2.class);
                            intent.putExtra("pickupAddress", SpickupLocation);
                            intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                            intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));

                            intent.putExtra("destinationAddress", SdestinationLocation);
                            intent.putExtra("destinationLatitude", SdestinationLatitude);
                            intent.putExtra("destinationLongitude", SdestinationLongitude);

                            intent.putExtra("pickup_date", selectedDate);
                            intent.putExtra("pickup_time", selectedTime);
                            intent.putExtra("CategoryID", CategoryID);
                            System.out.println("=================Category_Selected_id6==============" + CategoryID);
                            intent.putExtra("type", selectedType);
                            intent.putExtra("tracking", Stracking);
                            intent.putExtra("mode", STrip_Type);
                            intent.putExtra("waitingTime", WaitingTime);
                            System.out.println("======WaitingTime3=========" + WaitingTime);
                            startActivity(intent);
                            //finish();
                        }
                    }
                } else {
                    Alert1(getkey("action_error"), getResources().getString(R.string.enter_drop_location));
                }
            }
        });
        fav_lyout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent favIntent = new Intent(BookingPage4.this, FavoriteList.class);
                favIntent.putExtra("str_page", "booking3");
                startActivityForResult(favIntent, favoriteList_request_code);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        Rl_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        back_lyout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BackClickHandle();
            }
        });
        Ll_OneWay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OneWay();
            }
        });

        Ll_Return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReturnTrip();
            }
        });
        Ll_MultipleStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiTrip();
            }
        });

        Ll_Tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                TrackMe();

                select_RideLater_Dialog();
            }
        });
        Ll_Coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("-----------coupons--------------");
                Intent intent_coupon = new Intent(BookingPage4.this, CouponPage.class);
                intent_coupon.putExtra("str_page", getResources().getString(R.string.profile_label_bookingmenu));
                startActivityForResult(intent_coupon, coupon_request_code);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        Ll_TripSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);
                jsonParams.put("pickup", SpickupLocation);
                jsonParams.put("pickup_lat", SpickupLatitude);
                jsonParams.put("pickup_lon", SpickupLongitude);

                jsonParams.put("drop", SdestinationLocation);
                jsonParams.put("drop_lat", SdestinationLatitude);
                jsonParams.put("drop_lon", SdestinationLongitude);

                jsonParams.put("category", CategoryID);
                System.out.println("=================Category_Selected_id7==============" + CategoryID);
                jsonParams.put("type", selectedType);
                jsonParams.put("pickup_date", selectedDate);
                jsonParams.put("pickup_time", selectedTime);
                jsonParams.put("tracking", Stracking);//tracking:1    => 0/1
                jsonParams.put("mode", STrip_Type);//: =>oneway/return/multistop

                if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(STrip_Type)) {

                    for (int i = 0; i < multiple_dropList.size(); i++) {
                        jsonParams.put("location_arr[" + i + "][drop_loc]", multiple_dropList.get(i).getPlaceName());
                        jsonParams.put("location_arr[" + i + "][drop_lon]", multiple_dropList.get(i).getPlaceLong());
                        jsonParams.put("location_arr[" + i + "][drop_lat]", multiple_dropList.get(i).getPlaceLat());
                    }
                }
                Intent intent = new Intent(BookingPage4.this, Home_RatecardPage.class);
                intent.putExtra("jsonParam", jsonParams);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });




        Ll_Person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PassangerTravelCount_choose();
//                select_person_Dialog();
            }
        });
//        Iv_Back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (Sextracheck != null) {
//                    if (!Sextracheck.equalsIgnoreCase("WalletMoneyPage1")) {
//                        stopTimer();
//                        finish();
//                        overridePendingTransition(R.anim.enter, R.anim.exit);
//                    } else {
//                        stopTimer();
//                        Intent intent = new Intent(BookingPage3.this, Navigation_new.class);
////                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        finish();
//                        overridePendingTransition(R.anim.enter, R.anim.exit);
//                    }
//                } else {
//                    stopTimer();
//                    finish();
//                    overridePendingTransition(R.anim.enter, R.anim.exit);
//                }
//
//            }
//        });

//        book_my_ride_set_destination_edittextview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                book_my_ride_set_destination_edittextview.requestFocus();
//                InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                keyboard.showSoftInput(book_my_ride_set_destination_edittextview, 0);
//                book_my_ride_set_destination_edittextview.setText(book_my_ride_set_destination_edittextview.getText().toString());
//
//                book_my_ride_set_destination_edittextview.setVisibility(View.VISIBLE);
//                book_my_ride_set_destination_edittextview.setFocusable(true);
//
//            }
//        });

        book_my_ride_set_destination_edittextview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                String c = String.valueOf(s);
//                if(c.equals(" ")){
//                    //backspace pressed
//                    book_my_ride_set_destination_textview.setText("");
//                    Log.d("Count", String.valueOf(c));
//                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                System.out.println("------------------dropclick------------------" + dropclick);
                cd = new ConnectionDetector(BookingPage4.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }
                    String data = book_my_ride_set_destination_edittextview.getText().toString().toLowerCase().replace("%", "").replace(" ", "%20").trim();
                    CitySearchRequest(Iconstant.place_search_url + data);

                } else {
                    //alert_layout.setVisibility(View.VISIBLE);
                    //alert_textview.setText(getResources().getString(R.string.alert_nointernet));
                }

            }
        });

//        book_my_ride_set_destination_edittextview.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_DEL) {
//                    book_my_ride_set_destination_textview.setText("");
//                }
//                return false;
//            }
//        });


//        book_my_ride_set_destination_textview.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                String data = book_my_ride_set_destination_textview.getText().toString();
//                if (data.equalsIgnoreCase("")) {
//                    is_stopover_value = false;
//                } else {
//                    is_stopover_value = true;
//                }
//            }
//        });

        book_my_ride_set_destination_edittextview.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(book_my_ride_set_destination_edittextview, 0);
            }
        }, 200);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dropclick = false;
                Sselected_location = itemList_location.get(position);

                cd = new ConnectionDetector(BookingPage4.this);
                isInternetPresent = cd.isConnectingToInternet();
                listview.setVisibility(GONE);
                CloseKeyboard(book_my_ride_set_destination_edittextview);
                centerMArker.setVisibility(View.VISIBLE);
                if (isInternetPresent) {
                    placeId = itemList_placeId.get(position);
                    EnableOnCameraChange();
                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId.get(position), "1");
                } else {
                    //alert_layout.setVisibility(View.VISIBLE);
                    // alert_textview.setText(getResources().getString(R.string.alert_nointernet));
                }

            }
        });

//        MyCurrent_lat = gps.getLatitude();
//        MyCurrent_long = gps.getLongitude();
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//                phantomcaranimation(MyCurrent_lat, MyCurrent_long);
//            }
//        }, 6000);

    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void setpPointer() {
        DestLocFromFindPlace();
    }

    private void getDestinationAddress(Double dlatitude, Double dlongitude) {


        if (isInternetPresent) {
            System.out.println("========= Map_movingTask1=================");
            Map_movingTask asynTask = new Map_movingTask(dlatitude, dlongitude);
            asynTask.execute();
        } else {
            Alert(getkey("action_error"), getResources().getString(R.string.alert_nointernet));
        }
    }

    private class Map_movingTask extends AsyncTask<String, Void, String> {
        String response = "";
        private double dLatitude = 0.0;
        private double dLongitude = 0.0;

        Map_movingTask(double lat, double lng) {
            dLatitude = lat;
            dLongitude = lng;
        }

        @Override
        protected void onPreExecute() {
            loading_spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {
//            String address = fetchCityName(dLatitude, dLongitude);
            String address = getCompleteAddressString(dLatitude, dLongitude);

            //  LocationAddress locationAddress = new LocationAddress();
            //  address= locationAddress.getAddressFromLocation(dLatitude, dLongitude, getApplicationContext(), callBack);
            System.out.println("=============first fetch address=======" + address);
            // String address = new GeocoderHelper().fetchCityName(BookingPage3.this, dLatitude, dLongitude, callBack);////getCompleteAddressString(dLatitude, dLongitude);
            return address;
        }

        @Override
        protected void onPostExecute(String result) {
            loading_spinner.setVisibility(View.GONE);
            Slocation = result;
            if (result.equalsIgnoreCase("") || result.equalsIgnoreCase(null)) {
                Slocation = cityName;
                System.out.println("=============first fetch address 1 =======" + cityName);
            }

            System.out.println("----result-----------" + result);
            if (Slocation != null && Slocation.length() > 0) {
                if (Slocation.equalsIgnoreCase("")) {
                    Rl_PickupNow.setEnabled(true);
                    if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
                        SdestinationLatitude = dLatitude + "";
                        SdestinationLongitude = dLongitude + "";
                        SdestinationLocation = Slocation;
                    }
                } else {
                    if ("MultiDropLocation".equalsIgnoreCase(Str_Page)) {
                        SmultiDropLocation = Slocation;
                        SmultiDropLatitude = dLatitude + "";
                        SmultiDropLongitude = dLongitude + "";

                    } else if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
                        SdestinationLatitude = dLatitude + "";
                        SdestinationLongitude = dLongitude + "";
                        SdestinationLocation = Slocation;
                    } else if ("SomeOnePickup".equalsIgnoreCase(Str_Page)) {
                        SdestinationLatitude = dLatitude + "";
                        SdestinationLongitude = dLongitude + "";
                        SdestinationLocation = Slocation;
                    }
                }
                System.out.println("=========set value2=================");

                book_my_ride_set_destination_edittextview.setText(Slocation);
            } else {
                System.out.println("=========set value3=================");
                book_my_ride_set_destination_edittextview.setText(Slocation);
            }
        }
    }

    private void initialize() {
        cd = new ConnectionDetector(BookingPage4.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(BookingPage4.this);
        gps = new GPSTracker(BookingPage4.this);
        multiple_dropList = new ArrayList<MultiDropPojo>();
        polylines = new ArrayList<Polyline>();
        polylines1 = new ArrayList<Polyline>();
        wayPointList = new ArrayList<LatLng>();
        person_itemList = new ArrayList<PersonPojo>();
        paymnet_list = new ArrayList<HomePojo>();
        paymentListBooking = new ArrayList<BookingPaymentListPojo>();
        paymentCardListBooking = new ArrayList<BookingPaymentListPojo>();
        v2GetRouteDirection = new GMapV2GetRouteDirection();

        bottom_sheet =  findViewById(R.id.bottom_sheet);

        TextView notav =  findViewById(R.id.notav);
        notav.setText(getkey("novehicles"));

        TextView tra =  findViewById(R.id.tra);
        tra.setText(getkey("tryagain"));

        Hlv_paymentList = (ExpandableHeightListView)findViewById(R.id.book_my_ride_payment_type_listview1);


        TextView selectpa= (TextView) findViewById(R.id.selectpa);
        selectpa.setText(getkey("select_payment"));

        TextView ruess= (TextView) findViewById(R.id.ruess);
        ruess.setText(getkey("rules_of_the_road"));

         havecoupp= (TextView) findViewById(R.id.havecoupp);
        havecoupp.setText(getkey("have_coupon"));

        TextView tripsumm= (TextView) findViewById(R.id.tripsumm);
        tripsumm.setText(getkey("trip_summary"));

        TextView estimeatef= (TextView) findViewById(R.id.estimeatef);
        estimeatef.setText(getkey("estimated_fare"));

        TextView howman= (TextView) findViewById(R.id.howman);
        howman.setText(getkey("how_many_person_to_this_trip"));

        TextView boono= (TextView) findViewById(R.id.boono);
        boono.setText(getkey("book_now_"));

        lay1 = (LinearLayout) findViewById(R.id.lay1);
        lay2 = (LinearLayout) findViewById(R.id.lay2);
        lay3 = (LinearLayout) findViewById(R.id.lay3);
        lay4 = (LinearLayout) findViewById(R.id.lay4);
        lay5 = (LinearLayout) findViewById(R.id.lay5);
        lay6 = (LinearLayout) findViewById(R.id.lay6);
        lay7 = (LinearLayout) findViewById(R.id.lay7);
        lay8 = (LinearLayout) findViewById(R.id.lay8);

        layout_booking_confirm = (LinearLayout) findViewById(R.id.layout_booking_confirm);

        layout_booking_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!paymenttype.equalsIgnoreCase("Cash")) {
                    if (mode_global.equalsIgnoreCase("xendit-card")) {
                        if (xendit_globalcheck.equalsIgnoreCase("1")) {
                            session.setbookingonProgress("0");
                            ConfirmBookling();

                        } else {
                            Alert1(getkey("action_error"),getResources().getString(R.string.card_details_un));
                        }
                    } else if (mode_global.equalsIgnoreCase("stripe")) {
                        if (proceed_status_global.equalsIgnoreCase("1")) {
                            session.setbookingonProgress("0");
                            ConfirmBookling();

                        } else {
                            Alert1(getkey("action_error"),getkey("card_details_un"));
                        }
                    } else if (mode_global.equalsIgnoreCase("wallet")) {
                        if (proceed_status_global.equalsIgnoreCase("1")) {
                            session.setbookingonProgress("0");
                            ConfirmBookling();

                        } else {
                            Alert1(getkey("action_error"),getkey("insufficiant"));
                        }
                    } else {
                        Alert1(getkey("action_error"), getkey("choose_payment"));
                    }
                } else {
                    session.setbookingonProgress("0");
                    ConfirmBookling();
                }
            }
        });

        text1 = (TextView) findViewById(R.id.text1);
        text2 = (TextView) findViewById(R.id.text2);
        text3 = (TextView) findViewById(R.id.text3);
        text4 = (TextView) findViewById(R.id.text4);
        text5 = (TextView) findViewById(R.id.text5);
        text6 = (TextView) findViewById(R.id.text6);
        text7 = (TextView) findViewById(R.id.text7);
        text8 = (TextView) findViewById(R.id.text8);


        int maxperon = Integer.parseInt(session.getMaxperson());
        if(maxperon == 0)
        {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(View.GONE);
            lay3.setVisibility(View.GONE);
            lay4.setVisibility(View.GONE);
            lay5.setVisibility(View.GONE);
            lay6.setVisibility(View.GONE);
            lay7.setVisibility(View.GONE);
            lay8.setVisibility(View.GONE);
        }
        else if(maxperon == 1)
        {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(View.GONE);
            lay3.setVisibility(View.GONE);
            lay4.setVisibility(View.GONE);
            lay5.setVisibility(View.GONE);
            lay6.setVisibility(View.GONE);
            lay7.setVisibility(View.GONE);
            lay8.setVisibility(View.GONE);
        }
        else if(maxperon == 2)
        {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(VISIBLE);
            lay3.setVisibility(View.GONE);
            lay4.setVisibility(View.GONE);
            lay5.setVisibility(View.GONE);
            lay6.setVisibility(View.GONE);
            lay7.setVisibility(View.GONE);
            lay8.setVisibility(View.GONE);
        }
        else if(maxperon == 3)
        {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(VISIBLE);
            lay3.setVisibility(VISIBLE);
            lay4.setVisibility(View.GONE);
            lay5.setVisibility(View.GONE);
            lay6.setVisibility(View.GONE);
            lay7.setVisibility(View.GONE);
            lay8.setVisibility(View.GONE);
        }
        else if(maxperon == 4)
        {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(VISIBLE);
            lay3.setVisibility(VISIBLE);
            lay4.setVisibility(VISIBLE);
            lay5.setVisibility(View.GONE);
            lay6.setVisibility(View.GONE);
            lay7.setVisibility(View.GONE);
            lay8.setVisibility(View.GONE);
        }
        else if(maxperon == 5)
        {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(VISIBLE);
            lay3.setVisibility(VISIBLE);
            lay4.setVisibility(View.VISIBLE);
            lay5.setVisibility(View.VISIBLE);
            lay6.setVisibility(View.GONE);
            lay7.setVisibility(View.GONE);
            lay8.setVisibility(View.GONE);
        }
        else if(maxperon == 6)
        {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(View.VISIBLE);
            lay3.setVisibility(View.VISIBLE);
            lay4.setVisibility(View.VISIBLE);
            lay5.setVisibility(View.VISIBLE);
            lay6.setVisibility(View.VISIBLE);
            lay7.setVisibility(View.GONE);
            lay8.setVisibility(View.GONE);
        }
        else if(maxperon == 7)
        {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(View.VISIBLE);
            lay3.setVisibility(View.VISIBLE);
            lay4.setVisibility(View.VISIBLE);
            lay5.setVisibility(View.VISIBLE);
            lay6.setVisibility(View.VISIBLE);
            lay7.setVisibility(View.VISIBLE);
            lay8.setVisibility(View.GONE);
        }
        else if(maxperon == 8)
        {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(View.VISIBLE);
            lay3.setVisibility(View.VISIBLE);
            lay4.setVisibility(View.VISIBLE);
            lay5.setVisibility(View.VISIBLE);
            lay6.setVisibility(View.VISIBLE);
            lay7.setVisibility(View.VISIBLE);
            lay8.setVisibility(View.VISIBLE);
        }
        else
        {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(View.VISIBLE);
            lay3.setVisibility(View.VISIBLE);
            lay4.setVisibility(View.VISIBLE);
            lay5.setVisibility(View.VISIBLE);
            lay6.setVisibility(View.GONE);
            lay7.setVisibility(View.GONE);
            lay8.setVisibility(View.GONE);
        }

        lay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NoOfPerson = "1";
                text1.setTextColor(getResources().getColor(R.color.white));
                text2.setTextColor(getResources().getColor(R.color.gray1));
                text3.setTextColor(getResources().getColor(R.color.gray1));
                text4.setTextColor(getResources().getColor(R.color.gray1));
                text5.setTextColor(getResources().getColor(R.color.gray1));
                text6.setTextColor(getResources().getColor(R.color.gray1));
                text7.setTextColor(getResources().getColor(R.color.gray1));
                text8.setTextColor(getResources().getColor(R.color.gray1));

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    lay1.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.selectedseat) );
                } else {
                    lay1.setBackground(ContextCompat.getDrawable(context, R.drawable.selectedseat));
                }
                lay2.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay3.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay4.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay5.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay6.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay7.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay8.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
            }
        });


        lay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoOfPerson = "2";
                text2.setTextColor(getResources().getColor(R.color.white));
                text1.setTextColor(getResources().getColor(R.color.gray1));
                text3.setTextColor(getResources().getColor(R.color.gray1));
                text4.setTextColor(getResources().getColor(R.color.gray1));
                text5.setTextColor(getResources().getColor(R.color.gray1));
                text6.setTextColor(getResources().getColor(R.color.gray1));
                text7.setTextColor(getResources().getColor(R.color.gray1));
                text8.setTextColor(getResources().getColor(R.color.gray1));

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    lay2.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.selectedseat) );
                } else {
                    lay2.setBackground(ContextCompat.getDrawable(context, R.drawable.selectedseat));
                }
                lay1.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay3.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay4.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay5.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay6.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay7.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay8.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );

            }
        });



        lay3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoOfPerson = "3";
                text3.setTextColor(getResources().getColor(R.color.white));
                text1.setTextColor(getResources().getColor(R.color.gray1));
                text2.setTextColor(getResources().getColor(R.color.gray1));
                text4.setTextColor(getResources().getColor(R.color.gray1));
                text5.setTextColor(getResources().getColor(R.color.gray1));
                text6.setTextColor(getResources().getColor(R.color.gray1));
                text7.setTextColor(getResources().getColor(R.color.gray1));
                text8.setTextColor(getResources().getColor(R.color.gray1));

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    lay3.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.selectedseat) );
                } else {
                    lay3.setBackground(ContextCompat.getDrawable(context, R.drawable.selectedseat));
                }
                lay1.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay2.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay4.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay5.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay6.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay7.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay8.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );

            }
        });


        lay4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoOfPerson = "4";
                text4.setTextColor(getResources().getColor(R.color.white));
                text1.setTextColor(getResources().getColor(R.color.gray1));
                text2.setTextColor(getResources().getColor(R.color.gray1));
                text3.setTextColor(getResources().getColor(R.color.gray1));
                text5.setTextColor(getResources().getColor(R.color.gray1));
                text6.setTextColor(getResources().getColor(R.color.gray1));
                text7.setTextColor(getResources().getColor(R.color.gray1));
                text8.setTextColor(getResources().getColor(R.color.gray1));

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    lay4.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.selectedseat) );
                } else {
                    lay4.setBackground(ContextCompat.getDrawable(context, R.drawable.selectedseat));
                }
                lay1.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay3.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay2.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay5.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay6.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay7.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay8.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );

            }
        });


        lay5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoOfPerson = "5";
                text5.setTextColor(getResources().getColor(R.color.white));
                text1.setTextColor(getResources().getColor(R.color.gray1));
                text2.setTextColor(getResources().getColor(R.color.gray1));
                text3.setTextColor(getResources().getColor(R.color.gray1));
                text4.setTextColor(getResources().getColor(R.color.gray1));
                text6.setTextColor(getResources().getColor(R.color.gray1));
                text7.setTextColor(getResources().getColor(R.color.gray1));
                text8.setTextColor(getResources().getColor(R.color.gray1));

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    lay5.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.selectedseat) );
                } else {
                    lay5.setBackground(ContextCompat.getDrawable(context, R.drawable.selectedseat));
                }
                lay1.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay3.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay4.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay2.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay6.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay7.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay8.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );

            }
        });

        lay6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoOfPerson = "6";
                text6.setTextColor(getResources().getColor(R.color.white));
                text1.setTextColor(getResources().getColor(R.color.gray1));
                text2.setTextColor(getResources().getColor(R.color.gray1));
                text3.setTextColor(getResources().getColor(R.color.gray1));
                text4.setTextColor(getResources().getColor(R.color.gray1));
                text5.setTextColor(getResources().getColor(R.color.gray1));
                text7.setTextColor(getResources().getColor(R.color.gray1));
                text8.setTextColor(getResources().getColor(R.color.gray1));

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    lay6.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.selectedseat) );
                } else {
                    lay6.setBackground(ContextCompat.getDrawable(context, R.drawable.selectedseat));
                }
                lay1.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay3.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay4.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay5.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay2.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay7.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay8.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );

            }
        });

        lay7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoOfPerson = "7";
                text7.setTextColor(getResources().getColor(R.color.white));
                text1.setTextColor(getResources().getColor(R.color.gray1));
                text2.setTextColor(getResources().getColor(R.color.gray1));
                text3.setTextColor(getResources().getColor(R.color.gray1));
                text4.setTextColor(getResources().getColor(R.color.gray1));
                text6.setTextColor(getResources().getColor(R.color.gray1));
                text5.setTextColor(getResources().getColor(R.color.gray1));
                text8.setTextColor(getResources().getColor(R.color.gray1));

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    lay7.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.selectedseat) );
                } else {
                    lay7.setBackground(ContextCompat.getDrawable(context, R.drawable.selectedseat));
                }
                lay1.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay3.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay4.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay5.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay6.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay2.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay8.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );

            }
        });

        lay8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoOfPerson = "8";
                text8.setTextColor(getResources().getColor(R.color.white));
                text1.setTextColor(getResources().getColor(R.color.gray1));
                text2.setTextColor(getResources().getColor(R.color.gray1));
                text3.setTextColor(getResources().getColor(R.color.gray1));
                text4.setTextColor(getResources().getColor(R.color.gray1));
                text6.setTextColor(getResources().getColor(R.color.gray1));
                text7.setTextColor(getResources().getColor(R.color.gray1));
                text5.setTextColor(getResources().getColor(R.color.gray1));

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    lay8.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.selectedseat) );
                } else {
                    lay8.setBackground(ContextCompat.getDrawable(context, R.drawable.selectedseat));
                }
                lay1.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay3.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay4.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay5.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay6.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay7.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );
                lay2.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.seatunselectdrawable) );

            }
        });



        coponadded= (TextView) findViewById(R.id.coponadded);

        coponadded.setText(getkey("added_coupon_success"));







        fromNew = new Location(LocationManager.GPS_PROVIDER);
        toNew = new Location(LocationManager.GPS_PROVIDER);

        movingLocation = new HashMap<>();
        fromLocation = new ArrayList<>();
        ToLocation = new ArrayList<>();

        from = new Location(LocationManager.GPS_PROVIDER);
        to = new Location(LocationManager.GPS_PROVIDER);
        from2 = new Location(LocationManager.GPS_PROVIDER);
        to2 = new Location(LocationManager.GPS_PROVIDER);
        from3 = new Location(LocationManager.GPS_PROVIDER);
        to3 = new Location(LocationManager.GPS_PROVIDER);
        HashMap<String, String> info = session.getUserDetails();
        handlerNew = new Handler();
        UserID = info.get(SessionManager.KEY_USERID);
        UserName = info.get(SessionManager.KEY_USERNAME);
        HashMap<String, String> coupon = session.getCouponCode();
        ScouponSource = coupon.get(SessionManager.KEY_COUPON_CODE_SOURCE);
        ScouponCode = coupon.get(SessionManager.KEY_COUPON_CODE);
        HashMap<String, String> refferal = session.getReferralCode();
        Str_referralCode = refferal.get(SessionManager.KEY_REFERAL_CODE);


        HashMap<String, String> freeWaitingTime = session.getFreeWaitingTimeRate();
        str_FreeWaitingTime = freeWaitingTime.get(SessionManager.FREE_WAITING_TIME);
        str_FreeWaitingRate = freeWaitingTime.get(SessionManager.FREE_WAITING_RATE);
        session.setTimerPage("");
        viewpastrips = findViewById(R.id.viewpastrips);
        droppofftv = findViewById(R.id.droppofftv);
        tv_bookforsome = findViewById(R.id.book_someone);
        lyt_book_now = (RelativeLayout) findViewById(R.id.lyt_book_now);
        lytpayment_method = (RelativeLayout) findViewById(R.id.lytpayment_method);
        book_my_ride_selectcar_triptype_layout = (RelativeLayout) findViewById(R.id.book_my_ride_selectcar_triptype_layout);
        listview = (ListView) findViewById(R.id.location_search_listView_booking);
        dragdropTv = (TextView) findViewById(R.id.dragdrop_text);
        IV_selectcategoryImage = (ImageView) findViewById(R.id.selectcategoryimage);
        book_my_ride_rideNow_layout = (RelativeLayout) findViewById(R.id.book_my_ride_rideNow_layout);

        layout_home_iamge = (RelativeLayout) findViewById(R.id.layout_home_iamge);
        layout_home_iamge2 = (RelativeLayout) findViewById(R.id.layout_home_iamge2);
        inflateRetry = findViewById(R.id.retry_again);
        inflateuserinfo = findViewById(R.id.info_tripuser);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_lable));
        countryCode();
        Ll_SelectVechile = (LinearLayout) findViewById(R.id.book_my_ride_select_vechile_layout);
        Ll_OneWay = (LinearLayout) findViewById(R.id.book_my_ride_one_way_layout);
        Ll_Return = (LinearLayout) findViewById(R.id.book_my_ride_return_layout);
        Ll_MultipleStop = (LinearLayout) findViewById(R.id.book_my_ride_multiple_stop_layout);
        Ll_Tracking = (LinearLayout) findViewById(R.id.book_my_ride_tracking_layout);
        Iv_OneWay = (ImageView) findViewById(R.id.book_my_ride_one_way_imgview);
        Iv_Return = (ImageView) findViewById(R.id.book_my_ride_return_imgview);
        Iv_MultipleStop = (ImageView) findViewById(R.id.book_my_ride_multiple_stop_imageview);
        Iv_Tracking = (ImageView) findViewById(R.id.book_my_ride_tracking_imageview);
        spinKitView = (SpinKitView) findViewById(R.id.spin_kit);
        Rl_Back = (RelativeLayout) findViewById(R.id.book_my_ride_rideLater_layout);
//        IV_schedule = (ImageView) findViewById(R.id.schedule_iv);
//        IV_booksomeOne = (ImageView) findViewById(R.id.book_someone);
        IV_dayNight = (ImageView) findViewById(R.id.daymight_iv);
        Iv_CenterMarker = (ImageView) findViewById(R.id.book_my_ride_center_marker);
        // Tv_PickupLater = (CustomTextView) findViewById(R.id.book_my_ride_rideLater_textView);
        Rl_PickupNow = (RelativeLayout) findViewById(R.id.book_my_ride_rideNow_layout);
        Tv_PickupNow = (TextView) findViewById(R.id.book_my_ride_rideNow_textview);
//        Iv_Back = (ImageView) findViewById(R.id.book_my_ride_bottom_home_imageview);
        Iv_home = (ImageView) findViewById(R.id.imageview_home);
        Ll_TripSummary = (LinearLayout) findViewById(R.id.book_my_ride_trip_summary_layout);
        Ll_Coupon = (LinearLayout) findViewById(R.id.book_my_ride_coupon_layout);

        Ll_Person = (LinearLayout) findViewById(R.id.book_my_ride_person_layout);
        Tv_EstimateAmount = (CustomTextView) findViewById(R.id.book_my_ride_estimate_amount_textview);
        vechileName = (CustomTextView) findViewById(R.id.vechile_name);
        centerMArker = (RelativeLayout) findViewById(R.id.book_my_ride_center_marker_RelativeLayout);

        Rl_PickUpLayout = (RelativeLayout) findViewById(R.id.book_my_ride_pickupnow_bottom_layout);
        Rl_ConfirmCancelLayout = (RelativeLayout) findViewById(R.id.book_my_ride_confirm_bottom_layout);
        Rl_Bottom_layout = (RelativeLayout) findViewById(R.id.book_my_ride_bottom_layout);
        Rl_Alert_layout = (RelativeLayout) findViewById(R.id.book_my_ride_alert_layout);
        TV_Alert_textview = (TextView) findViewById(R.id.book_my_ride_alert_textView);
        Rl_Payment_layout = (RelativeLayout) findViewById(R.id.book_my_ride_payment_type_layout);

        alertlayout = (LinearLayout) findViewById(R.id.alertlayout);
        loading_spinner = (ProgressBar) findViewById(R.id.loading_spinner);
        Rl_droplocation = (LinearLayout) findViewById(R.id.droplocation_address_layout);
        book_my_ride_set_destination_edittextview = (CustomEdittext) findViewById(R.id.book_my_ride_set_destination_edittextview);
//        book_my_ride_set_destination_textview = (CustomTextView) findViewById(R.id.book_my_ride_set_destination_textview);


        Rl_roadimgelayout = (RelativeLayout) findViewById(R.id.roadimgelayout);
        fav_lyout = (RelativeLayout) findViewById(R.id.fav_lyout);
        back_lyout = (RelativeLayout) findViewById(R.id.back_lyout);
        tv_setdropofflocation = (TextView) findViewById(R.id.tv_setdropofflocation);
        bookforsomeone = (LinearLayout) findViewById(R.id.bookforsomeone);
        Rl_destination_layout = (RelativeLayout) findViewById(R.id.destination_layout);

        selectCarCatImage = (RelativeLayout) findViewById(R.id.book_my_ride_select_way_plus_img);
        currentLocation_image = (ImageView) findViewById(R.id.book_current_location_imageview);
        CV_TripType = (CardView) findViewById(book_my_ride_trip_details_carview);
        Tv_TripType = (TextView) findViewById(R.id.book_my_ride_trip_type_textview);
        Tv_Distance = (TextView) findViewById(R.id.book_my_ride_trip_distance_textview);
        Tv_CarArriveTime = (TextView) findViewById(R.id.book_my_ride_trip_time_textview);
        bookMyRidTripFare = (TextView) findViewById(R.id.book_my_ride_trip_fare);
        book_my_ride_bottom_main_layout1 = (RelativeLayout) findViewById(R.id.book_my_ride_bottom_main_layout1);
//        paymentname = (CustomTextView) findViewById(R.id.paymentname);
//        person_count_tv = (CustomTextView) findViewById(R.id.person_count_tv);

        // new marker design
        inflateuserinfo = findViewById(R.id.info_tripuser);
        tv_username_marker = inflateuserinfo.findViewById(R.id.username);
        tv_tripType_tv = inflateuserinfo.findViewById(R.id.tripType_tv);
        tv_vehicle_type_tv = inflateuserinfo.findViewById(R.id.vehicle_type_tv);

        //vehicles list
       //listview_rc = findViewById(R.id.rc_vehicles);
      /*  LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false);
        listview_rc.setLayoutManager(linearLayoutManager);
        listview_rc.setItemAnimator(new DefaultItemAnimator());*/

        String vehiclelistjson = session.getDriverslist();
        Type type = new TypeToken<List<BookingPojo1>>() {
        }.getType();
        ArrayList<BookingPojo1> vehiclelist = new GsonBuilder().create().fromJson(vehiclelistjson, type);

       /* vehiclelistada = new ConfirmbookingVehiclelistAdapter(BookingPage4.this, vehiclelist);
        listview_rc.setAdapter(vehiclelistada);
*/

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new BookingPage4.RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        intentFilter.addAction("com.package.finish.BookingPage");
        registerReceiver(refreshReceiver, intentFilter);
        face = Typeface.createFromAsset(getAssets(), "fonts/vladimir.ttf");

        params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );


        if (isInternetPresent) {
            if (mRequest != null) {
                mRequest.cancelRequest();
            }
            //   postRequest_PhantomCar(Iconstant.getPhantomCar_url);
        }
        Intent intent = getIntent();
        Str_Page = intent.getStringExtra("page");

        if (getIntent().hasExtra("hideleftmarker")) {
            hideleftmarker = true;
        } else {
            hideleftmarker = false;
        }

        System.out.println("-----------Str_Page-------------" + Str_Page);
        if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
//            centerMArker.setVisibility(VISIBLE);
            isCameraChange = false;
            SpickupLocation = intent.getStringExtra("pickupAddress");
            SpickupLatitude = intent.getStringExtra("pickupLatitude");
            SpickupLongitude = intent.getStringExtra("pickupLongitude");
            SdestinationLocation = intent.getStringExtra("destinationAddress");
            SdestinationLatitude = intent.getStringExtra("destinationLatitude");
            SdestinationLongitude = intent.getStringExtra("destinationLongitude");
            driver_status = intent.getBooleanExtra("driver_status", true);

            if (driver_status) {


                bookforsomeone.setEnabled(true);
            } else {
                book_my_ride_selectcar_triptype_layout.setVisibility(View.GONE);
                bookforsomeone.setEnabled(false);
            }

            if (intent.hasExtra("CategoryID")) {
                CategoryID = intent.getStringExtra("CategoryID");
            } else {
                //  CategoryID = info.get(SessionManager.KEY_CATEGORY);
            }
            System.out.println("=================Category_Selected_id8==============" + CategoryID);
            if (intent.hasExtra("intentCall")) {
                callingFrom = intent.getStringExtra("intentCall");
                if (callingFrom.equalsIgnoreCase("findplaceBooking")) {
                    System.out.println("=========set value4=================");
                    book_my_ride_set_destination_edittextview.setText(SdestinationLocation);
                }
            }
        } else if ("MultiDropLocation".equalsIgnoreCase(Str_Page)) {


            book_my_ride_set_destination_edittextview.setText("");
            book_my_ride_set_destination_edittextview.setText("");


            isRetrunOrMulti = "Stop Over";
            isCameraChange = true;


            Str_stopOverStatus = "yes";
            SpickupLocation = intent.getStringExtra("pickupAddress");
            SpickupLatitude = intent.getStringExtra("pickupLatitude");
            SpickupLongitude = intent.getStringExtra("pickupLongitude");
            SdestinationLocation = intent.getStringExtra("destinationAddress");
            SdestinationLatitude = intent.getStringExtra("destinationLatitude");
            SdestinationLongitude = intent.getStringExtra("destinationLongitude");
            stopOverCount = intent.getStringExtra("stopOverCount");
//            book_my_ride_set_destination_edittextview.setHint("ENTER STOP OVER" + stopOverCount + " LOCATION");

            droppofftv.setText(getResources().getString(R.string.dropoff)+" " + stopOverCount);
            if (intent.hasExtra("CategoryID")) {
                CategoryID = intent.getStringExtra("CategoryID");
            } else {
                // CategoryID = info.get(SessionManager.KEY_CATEGORY);
            }
            System.out.println("=================Category_Selected_id9==============" + CategoryID);
            if (stopOverCount.equalsIgnoreCase("1")) {
                book_my_ride_set_destination_edittextview.setHint(getResources().getString(R.string.enter_first_drop));
            } else if (stopOverCount.equalsIgnoreCase("2")) {
                book_my_ride_set_destination_edittextview.setHint(getResources().getString(R.string.enter_nxt_drop));
            } else if (stopOverCount.equalsIgnoreCase("3")) {
                book_my_ride_set_destination_edittextview.setHint(getResources().getString(R.string.enter_nxt_drop));
            }

            selectedDate = intent.getStringExtra("pickup_date");
            selectedTime = intent.getStringExtra("pickup_time");
            selectedType = intent.getStringExtra("type");
            if(selectedType.equals("1")){
                bookforLater = "1";
            }
            Stracking = intent.getStringExtra("tracking");
            if (intent.hasExtra("mode")) {
                STrip_Type = intent.getStringExtra("mode");
                tripTypeMode = STrip_Type;
                Tv_TripType.setText(":" + STrip_Type);
            }

            if (intent.hasExtra("waiting_time")) {
                WaitingTime = intent.getStringExtra("waiting_time");
                System.out.println("======WaitingTime4=========" + WaitingTime);
            }
            if (intent.hasExtra("MultipleDropLocation_List")) {
                try {
                    Bundle bundleObject = getIntent().getExtras();
                    multiple_dropList = (ArrayList<MultiDropPojo>) bundleObject.getSerializable("MultipleDropLocation_List");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Tv_PickupNow.setText(getResources().getString(R.string.ok_lable));
            Tv_PickupNow.setEnabled(true);
            book_my_ride_rideNow_layout.setVisibility(View.VISIBLE);
            fav_lyout.setVisibility(View.VISIBLE);
            back_lyout.setVisibility(View.VISIBLE);
            bookforsomeone.setVisibility(View.GONE);
            tv_setdropofflocation.setVisibility(View.VISIBLE);
            Rl_Back.setVisibility(View.GONE);
            Rl_destination_layout.setVisibility(View.VISIBLE);
            Rl_PickupNow.setEnabled(true);
            layout_home_iamge2.setVisibility(View.VISIBLE);
            layout_home_iamge.setVisibility(View.GONE);

            dragdropTv.setVisibility(View.GONE);
            IV_selectcategoryImage.setVisibility(VISIBLE);
            inflateuserinfo.setVisibility(VISIBLE);
            tv_username_marker.setText(UserName);
            tv_tripType_tv.setText(context.getResources().getString(R.string.trip_lable)+"          : " + STrip_Type);
            tv_vehicle_type_tv.setText(getResources().getString(R.string.vehicle)+ selectedCarType);

            book_my_ride_selectcar_triptype_layout.setVisibility(View.GONE);
            sampleapopaup();

        } else if ("BookingPage2".equalsIgnoreCase(Str_Page)) {
            isCameraChange = false;
            dragdropTv.setVisibility(View.GONE);
            IV_selectcategoryImage.setVisibility(GONE);
            inflateuserinfo.setVisibility(GONE);
            // PaymentChoosePopUp();
            Sextracheck = intent.getStringExtra("extra_check");
            SpickupLocation = intent.getStringExtra("pickupAddress");
            SpickupLatitude = intent.getStringExtra("pickupLatitude");
            SpickupLongitude = intent.getStringExtra("pickupLongitude");
            SdestinationLocation = intent.getStringExtra("destinationAddress");
            SdestinationLatitude = intent.getStringExtra("destinationLatitude");
            SdestinationLongitude = intent.getStringExtra("destinationLongitude");

            if (intent.hasExtra("book_ref")) {
                book_ref = intent.getStringExtra("book_ref");
                user_name = intent.getStringExtra("user_name");
                gender = intent.getStringExtra("gender");
                mobile_number = intent.getStringExtra("mobile_number");
                country_code = intent.getStringExtra("country_code");
            }

            if (intent.hasExtra("book_ref")) {
                bookReference = intent.getStringExtra("book_ref");
                if (bookReference.equalsIgnoreCase("Yes")) {
                    someOneNmame = intent.getStringExtra("user_name");
                    someOneGender = intent.getStringExtra("gender");


                    mobileNumberstr = intent.getStringExtra("mobile_number");
                    CountryCodeStr = intent.getStringExtra("country_code");

                    System.out.println("==============Muruga someOneNmame============" + someOneNmame);
                    System.out.println("==============Muruga someOneGender============" + someOneGender);
                }
            }

            if (intent.hasExtra("CategoryID")) {
                CategoryID = intent.getStringExtra("CategoryID");
            } else {
                //   CategoryID = info.get(SessionManager.KEY_CATEGORY);
            }
            System.out.println("=================Category_Selected_id10==============" + CategoryID);
            selectedDate = intent.getStringExtra("pickup_date");
            selectedTime = intent.getStringExtra("pickup_time");
            selectedType = intent.getStringExtra("type");
            if(selectedType.equals("1")){
                bookforLater = "1";
            }
            Stracking = intent.getStringExtra("tracking");
            if (intent.hasExtra("mode")) {
                STrip_Type = intent.getStringExtra("mode");
                tripTypeMode = STrip_Type;

                Tv_TripType.setText(":" + STrip_Type);
            }
            if (intent.hasExtra("waitingTime")) {
                WaitingTime = intent.getStringExtra("waitingTime");
                System.out.println("======WaitingTime1=========" + WaitingTime);
            }
            if (intent.hasExtra("MultipleDropLocation_List")) {
                try {
                    Bundle bundleObject = getIntent().getExtras();
                    multiple_dropList = (ArrayList<MultiDropPojo>) bundleObject.getSerializable("MultipleDropLocation_List");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            ScurrencySymbol = intent.getStringExtra("currency_code");
            if (intent.hasExtra("est_cost")) {
                fareAmount = intent.getStringExtra("est_cost");
                fareAmount1 = intent.getStringExtra("est_cost_booking");
                SetaAmount = fareAmount1;
                bookMyRidTripFare.setText(fareAmount);
            }
            if (intent.hasExtra("approx_dist")) {
                SetaDistance = intent.getStringExtra("approx_dist");
                Tv_Distance.setText(":" + SetaDistance);
            }
            if (intent.hasExtra("eta")) {
                CarAvailable = intent.getStringExtra("eta");
                Tv_CarArriveTime.setText(":" + CarAvailable);
            }
            if (!SdestinationLocation.equalsIgnoreCase("") && SdestinationLocation.length() > 0 && !callingFrom.equalsIgnoreCase("findplaceBooking")) {
                BookingMethodStatus();
            }
        } else if ("SomeOnePickup".equalsIgnoreCase(Str_Page)) {
            isCameraChange = true;
            SpickupLocation = intent.getStringExtra("pickupAddress");
            SpickupLatitude = intent.getStringExtra("pickupLatitude");
            SpickupLongitude = intent.getStringExtra("pickupLongitude");
            SdestinationLocation = intent.getStringExtra("destinationAddress");
            SdestinationLatitude = intent.getStringExtra("destinationLatitude");
            SdestinationLongitude = intent.getStringExtra("destinationLongitude");
            centerMArker.setVisibility(View.VISIBLE);

            if (intent.hasExtra("mode")) {
                tripTypeMode = intent.getStringExtra("mode").toLowerCase();
                // STrip_Type=tripTypeMode;
                System.out.println("===========tripTypeMode=============" + tripTypeMode);
                if (tripTypeMode.equalsIgnoreCase("oneway")) {
                    Tv_PickupNow.setText(getResources().getString(R.string.ok_lable));
                    Tv_PickupNow.setEnabled(true);
                    book_my_ride_rideNow_layout.setVisibility(View.VISIBLE);
                    fav_lyout.setVisibility(View.VISIBLE);
                    back_lyout.setVisibility(View.VISIBLE);
                    bookforsomeone.setVisibility(View.GONE);
                    tv_setdropofflocation.setVisibility(View.VISIBLE);
                    Rl_Back.setVisibility(View.GONE);
                    Rl_destination_layout.setVisibility(View.VISIBLE);
                    Rl_PickupNow.setEnabled(true);
                    book_my_ride_selectcar_triptype_layout.setVisibility(View.GONE);

                    isCameraChange = true;
                    centerMArker.setVisibility(View.GONE);
                    bookingSomeElseflag = "2";
                    layout_home_iamge.setVisibility(View.GONE);
                    layout_home_iamge2.setVisibility(View.VISIBLE);

                    STrip_Type = getResources().getString(R.string.one_way_lable);
                    Tv_TripType.setText(getResources().getString(R.string.oneway_textview));
                    sMultipleDropStatus = "0";

                    DestLocFromFindPlace();

                } else if (tripTypeMode.equalsIgnoreCase("return")) {
                    STrip_Type = getResources().getString(R.string.return_lable);
                    Tv_TripType.setText(getResources().getString(R.string.return_textview));
                    sMultipleDropStatus = "0";
                   /* Ll_OneWay.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_grad_topto_botton));
                    Ll_Return.setBackground(getResources().getDrawable(R.drawable.green_grad_topto_bottom));
                    Ll_MultipleStop.setBackground(getResources().getDrawable(R.drawable.blue_grad_topto_botton));
                  */
                    layout_home_iamge.setVisibility(View.GONE);
                    layout_home_iamge2.setVisibility(View.VISIBLE);
                    sampleapopaup();
                } else if (tripTypeMode.equalsIgnoreCase("multiple")) {
                    STrip_Type = getResources().getString(R.string.multiplestop_lable);
                    Tv_PickupNow.setText(getResources().getString(R.string.ok_lable));
                    Tv_PickupNow.setEnabled(true);
                    book_my_ride_rideNow_layout.setVisibility(View.VISIBLE);
                    fav_lyout.setVisibility(View.VISIBLE);
                    back_lyout.setVisibility(View.VISIBLE);
                    bookforsomeone.setVisibility(View.GONE);
                    tv_setdropofflocation.setVisibility(View.VISIBLE);
                    // Rl_Back.setVisibility(View.GONE);
                    Rl_destination_layout.setVisibility(View.VISIBLE);
                    Rl_PickupNow.setEnabled(true);
                    book_my_ride_selectcar_triptype_layout.setVisibility(View.GONE);
                    layout_home_iamge.setVisibility(View.GONE);
                    layout_home_iamge2.setVisibility(View.VISIBLE);
                    DestLocFromFindPlace();
                }
            }

            if (intent.hasExtra("CategoryID")) {
                CategoryID = intent.getStringExtra("CategoryID");
            } else {
                // CategoryID = info.get(SessionManager.KEY_CATEGORY);
            }
            System.out.println("=================Category_Selected_id11==============" + CategoryID);
            if (callingFrom.equalsIgnoreCase("SomeOnePickup")) {
                bookingSomeOneViews();
            }

        }

        if (!session.getPhantomcar().equalsIgnoreCase("no")) {
            startTimer();
        }

    }

    //To stop timer
    private void stopTimer() {

        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
    }

    //To start timer
    private void startTimer() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                phantomRandromLatLan();
                //Do something after 100ms
            }
        }, 10000);

    }

    private void phantomRandromLatLan() {
        LatLng randromLatLan = null;
        phantomLat = new ArrayList<>();
        phantomLan = new ArrayList<>();
        phantomMap = new HashMap<String, List<Double>>();
        MyCurrent_lat = gps.getLatitude();
        MyCurrent_long = gps.getLongitude();


        try {
            for (int i = 1; i <= driver_list.size(); i++) {
                randromLatLan = phantomLatLan(MyCurrent_lat, MyCurrent_long, 22.5 * i);

                phantomLat.add(randromLatLan.latitude);
                phantomLan.add(randromLatLan.longitude);

            }
            phantomMap.put("phantomLat", phantomLat);
            phantomMap.put("phantomLan", phantomLan);

            updatePhantomCars1(phantomMap);
            // updatePhantomCars(phantomMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bookingSomeOneViews() {
        if ("oneway".equalsIgnoreCase(STrip_Type)) {
            Tv_PickupNow.setText(getResources().getString(R.string.ok_lable));
            Tv_PickupNow.setEnabled(true);
            book_my_ride_rideNow_layout.setVisibility(View.VISIBLE);
            fav_lyout.setVisibility(View.VISIBLE);
            back_lyout.setVisibility(View.VISIBLE);
            bookforsomeone.setVisibility(View.GONE);
            tv_setdropofflocation.setVisibility(View.VISIBLE);
            Rl_Back.setVisibility(View.GONE);
            Rl_destination_layout.setVisibility(View.VISIBLE);
            Rl_PickupNow.setEnabled(true);
            book_my_ride_selectcar_triptype_layout.setVisibility(View.GONE);

            STrip_Type = getResources().getString(R.string.one_way_lable);
            Tv_TripType.setText(getResources().getString(R.string.oneway_textview));
            sMultipleDropStatus = "0";
        } else if ("return".equalsIgnoreCase(STrip_Type)) {
            STrip_Type = getResources().getString(R.string.return_lable);
            Tv_TripType.setText(getResources().getString(R.string.return_textview));
            sMultipleDropStatus = "0";
           /* Ll_OneWay.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_grad_topto_botton));
            Ll_Return.setBackground(getResources().getDrawable(R.drawable.green_grad_topto_bottom));
            Ll_MultipleStop.setBackground(getResources().getDrawable(R.drawable.blue_grad_topto_botton));
            */
            sampleapopaup();
        } else if ("multistop".equalsIgnoreCase(STrip_Type)) {
            Rl_PickupNow.setEnabled(true);
            book_my_ride_rideNow_layout.setVisibility(View.VISIBLE);
            Tv_PickupNow.setText(getResources().getString(R.string.ok_lable));
            fav_lyout.setVisibility(View.VISIBLE);
            back_lyout.setVisibility(View.VISIBLE);
            bookforsomeone.setVisibility(View.GONE);
            tv_setdropofflocation.setVisibility(View.VISIBLE);
            //  Rl_Back.setVisibility(View.GONE);
            book_my_ride_selectcar_triptype_layout.setVisibility(View.GONE);


            Intent intent1 = new Intent(BookingPage4.this, MultiDropLocation_New.class);/*MultipleDropLocation*/
            if (multiple_dropList.size() > 0) {
                Bundle bundleObject = new Bundle();
                bundleObject.putSerializable("MultipleDropLocation_List", multiple_dropList);
                intent1.putExtras(bundleObject);
            }
            startActivityForResult(intent1, multiplePlaceSearch_request_code);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    private void BookingMethodStatus() {
        // EstimateParam_method(1);
        Rl_PickUpLayout.setVisibility(GONE);
        book_my_ride_selectcar_triptype_layout.setVisibility(GONE);
        Rl_roadimgelayout.setVisibility(View.GONE);
        book_my_ride_rideNow_layout.setVisibility(GONE);
        layout_home_iamge.setVisibility(GONE);
        // Rl_Back.setVisibility(View.GONE);
        bookforsomeone.setVisibility(View.GONE);
        tv_setdropofflocation.setVisibility(View.GONE);
        Rl_Bottom_layout.setVisibility(View.GONE);
        Rl_destination_layout.setVisibility(GONE);
        book_my_ride_bottom_main_layout1.setVisibility(GONE);
        Rl_ConfirmCancelLayout.setVisibility(View.VISIBLE);
        lyt_book_now.setVisibility(View.VISIBLE);
        lytpayment_method.setVisibility(View.VISIBLE);

        layout_home_iamge2.setVisibility(View.VISIBLE);
        // Rl_Payment_layout.setVisibility(View.VISIBLE);
        CV_TripType.setVisibility(View.VISIBLE);
        CV_TripType.setVisibility(View.VISIBLE);
        Tv_PickupNow.setText(getResources().getString(R.string.book_now_lable));
        // Tv_PickupLater.setText(getResources().getString(R.string.cancel_lable));
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private void initializeMap() {
        try {
            if (googleMap == null) {
                //googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.book_my_ride3_mapview)).getMap();
                mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.book_my_ride3_mapview));

                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap arg) {
                        loadMap(arg);

                        currentMarker = 1;
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(event.getKeyCode() == KeyEvent.KEYCODE_BACK)
        {
            GohomePopUp(getkey("close_booking_cancel1"));
        }
        return super.dispatchKeyEvent(event);
    }

    public void loadMap(GoogleMap arg) {
        googleMap = arg;
        if (CheckPlayService()) {
            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);
            // Changing map type
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleMap.setMyLocationEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            // Enable / Disable zooming controls
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            // Enable / Disable my location button
            // Enable / Disable Compass icon
            googleMap.getUiSettings().setCompassEnabled(false);
            // Enable / Disable Rotate gesture
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);
            googleMap.getUiSettings().setAllGesturesEnabled(true);
            // Move the camera to last position with a zoom level

            //setInfoWidow(UserName, "", 1);

            if (!"MultiDropLocation".equalsIgnoreCase(Str_Page)) {
                addIcon(UserName.toUpperCase(), new LatLng(Double.parseDouble(SpickupLatitude), Double.parseDouble(SpickupLongitude)), 3);
            }


            System.out.println("===========usernamemarker====");
            if (session.getNightMode()) {
                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.mapstyle));
            } else {
                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style));
            }

            if (CheckPlayService()) {

                googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        return true;
                    }
                });

                if (isRetrunOrMulti.equalsIgnoreCase("Stop Over")) {

                    isFromFindPlace = false;
                    googleMap.setOnCameraChangeListener(null);
                }

            } else {
                Toast.makeText(BookingPage4.this,  getResources().getString(R.string.install_googleplay_view_location), Toast.LENGTH_LONG).show();
            }
            if (gps.canGetLocation() && gps.isgpsenabled()) {
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(SpickupLatitude), Double.parseDouble(SpickupLongitude))).zoom(18).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                if (isInternetPresent) {
                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }

                    Rl_PickupNow.setEnabled(true);
                    Rl_Back.setEnabled(true);
                    isLoading = true;
                    PostRequest(Iconstant.BookMyRide_url, SpickupLatitude, SpickupLongitude);
                } else {
                    Rl_Alert_layout.setVisibility(View.VISIBLE);
                    TV_Alert_textview.setText(getResources().getString(R.string.alert_nointernet));
                    Rl_Bottom_layout.setVisibility(GONE);
                }
                if (!SdestinationLocation.equalsIgnoreCase("") && SdestinationLocation.length() > 0 && "BookingPage2".equalsIgnoreCase(Str_Page)) {
                    System.out.println("==================polyline 2=============");
                    route();
                }
            } else {
                enableGpsService();
            }
        } else {

            final PkDialog mDialog = new PkDialog(BookingPage4.this);
            mDialog.setDialogTitle(getResources().getString(R.string.alert_label_title));
            mDialog.setDialogMessage(getResources().getString(R.string.action_unable_to_create_map));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();
        }
    }

    //-------------------AsynTask To get the current Address----------------
    private void PostRequest(String Url, String latitude, String longitude) {
        spinKitView.setVisibility(VISIBLE);
        Rl_PickupNow.setEnabled(true);
        Rl_Back.setEnabled(true);
        isLoading = true;
        System.out.println("--------------Book My ride url-------------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);//"57c6a827cae2aa580c00002d");
        jsonParams.put("lat", latitude);
        jsonParams.put("lon", longitude);
        jsonParams.put("category", CategoryID);
        jsonParams.put("mode", tripTypeMode);
        System.out.println("--------------Book My ride jsonParams-------------------" + jsonParams);
        mRequest = new ServiceRequest(BookingPage4.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                spinKitView.setVisibility(GONE);
                String status = "";
                System.out.println("--------------Book My ride reponse-------------------" + response);
                mapview_pojo = new Getmapview();
                try {
                    JSONObject object = new JSONObject(response);
                    Type listType = new TypeToken<Getmapview>() {
                    }.getType();
                    mapview_pojo = new GsonBuilder().create().fromJson(object.toString(), listType);

                    if (object.length() > 0) {
                        if (mapview_pojo.getStatus().equalsIgnoreCase("1")) {

                            JSONObject jobject = object.getJSONObject("response");
                            if (jobject.length() > 0) {

                                StrackingPercent = mapview_pojo.getResponse().getTracking_percentage();
                                NoOfSeats = Integer.parseInt(mapview_pojo.getResponse().getNo_of_seats());
                                strWalletAmount = mapview_pojo.getResponse().getWallet_amount();
                                strCurrency = mapview_pojo.getResponse().getCurrency();
                                person_itemList.clear();
                                for (int i = 1; i <= NoOfSeats; i++) {
                                    PersonPojo RideListPojo = new PersonPojo();
                                    RideListPojo.setCount(String.valueOf(i));
                                    person_itemList.add(RideListPojo);
                                }

                                Object check_driver_object = jobject.get("drivers");
                                if (check_driver_object instanceof JSONArray) {

                                    JSONArray driver_array = jobject.getJSONArray("drivers");
                                    if (mapview_pojo.getResponse().getDrivers().size() > 0) {
                                        driver_list.clear();
                                        for (int j = 0; j < mapview_pojo.getResponse().getDrivers().size(); j++) {
                                            JSONObject driver_object = driver_array.getJSONObject(j);
                                            HomePojo pojo = new HomePojo();
                                            pojo.setDriver_lat(mapview_pojo.getResponse().getDrivers().get(j).getLat());
                                            pojo.setDriver_long(mapview_pojo.getResponse().getDrivers().get(j).getLon());
                                            pojo.setCarType(mapview_pojo.getResponse().getDrivers().get(j).getType());
                                            pojo.setPoint(mapview_pojo.getResponse().getDrivers().get(j).getPoint());

                                            driver_list.add(pojo);
                                        }
                                        driver_status = true;
                                    } else {
                                        driver_list.clear();
                                        driver_status = false;
                                    }
                                } else {
                                    driver_status = false;
                                }
                                Object check_payment_list_object = jobject.get("payment");
                                if (check_payment_list_object instanceof JSONArray) {
                                    JSONArray payment_list_array = jobject.getJSONArray("payment");
                                    if (mapview_pojo.getResponse().getPayment().size() > 0) {
                                        paymnet_list.clear();
                                        for (int j = 0; j < mapview_pojo.getResponse().getPayment().size(); j++) {
                                            JSONObject payment_list_object = payment_list_array.getJSONObject(j);
                                            HomePojo pojo = new HomePojo();
                                            /*   if (j == 0) {*/
                                            SpaymentMode = mapview_pojo.getResponse().getPayment().get(j).getCode();
                                            /* }*/
                                            pojo.setCat_image("0");

                                            pojo.setPayment_code(mapview_pojo.getResponse().getPayment().get(j).getCode());
                                            pojo.setPayment_name(mapview_pojo.getResponse().getPayment().get(j).getName());
                                            pojo.setPayment_icon(mapview_pojo.getResponse().getPayment().get(j).getIcon());
                                            if (payment_list_object.has("inactive_icon")) {
                                                pojo.setSetpaymentinactiveicon(mapview_pojo.getResponse().getPayment().get(j).getInactive_icon());
                                            }
                                            status = mapview_pojo.getResponse().getPayment().get(j).getStatus();
                                            if (status.equalsIgnoreCase("active")) {
                                                paymnet_list.add(pojo);
                                            }
                                        }
                                        payment_list_status = true;
                                    } else {
                                        paymnet_list.clear();
                                        payment_list_status = false;
                                    }
                                }
                                PaymentChoosePopUp();
                                Object check_category_object = jobject.get("category");
                                if (check_category_object instanceof JSONArray) {

                                    JSONArray cat_array = jobject.getJSONArray("category");
                                    if (cat_array.length() > 0) {
                                        category_list.clear();

                                        for (int k = 0; k < cat_array.length(); k++) {

                                            HomePojo pojo = new HomePojo();
                                            pojo.setCat_name(mapview_pojo.getResponse().getCategory().get(k).getName());
                                            pojo.setCat_time(mapview_pojo.getResponse().getCategory().get(k).getEta());
                                            pojo.setCat_id(mapview_pojo.getResponse().getCategory().get(k).getId());
                                            pojo.setIcon_normal(mapview_pojo.getResponse().getCategory().get(k).getIcon_normal());
                                            pojo.setIcon_active(mapview_pojo.getResponse().getCategory().get(k).getIcon_active());
                                            pojo.setSelected_Cat(mapview_pojo.getResponse().getSelected_category());
                                            if (mapview_pojo.getResponse().getCategory().get(k).getId().equals(mapview_pojo.getResponse().getSelected_category())) {
                                                CarAvailable = mapview_pojo.getResponse().getCategory().get(k).getEta();
                                                ScarType = mapview_pojo.getResponse().getCategory().get(k).getName();
                                                sCarImg = mapview_pojo.getResponse().getCategory().get(k).getIcon_car_image();
                                                session.setCarImage(sCarImg);
                                                selectedCarType = ScarType;
                                                SListPosition = k;
                                            }
                                            category_list.add(pojo);
                                        }
                                        category_status = true;
                                    } else {
                                        category_list.clear();
                                        category_status = false;
                                    }
                                } else {
                                    category_status = false;
                                }

                            }
                            main_response_status = true;
                        } else {
                            fail_response = object.getString("response");
                            main_response_status = false;
                        }

                    }
                    dialogDismiss();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogDismiss();
                }
                if (payment_list_status && "BookingPage2".equalsIgnoreCase(Str_Page)) {
                    PaymentChoosePopUp();
                    if (!SdestinationLocation.equalsIgnoreCase("") && SdestinationLocation.length() > 0 && "BookingPage2".equalsIgnoreCase(Str_Page)) {
                        System.out.println("==================polyline 1=============");
                        route();
                    }
                }
                if (main_response_status) {
                    dialogDismiss();
                    Rl_Alert_layout.setVisibility(GONE);
                    Rl_Bottom_layout.setVisibility(View.VISIBLE);
                    Tv_CarArriveTime.setText(":" + CarAvailable);
                    bookMyRidTripFare.setText(fareAmount);
//                    new LoadBitmap().execute(sCarImg);
                    HashMap<String, String> info = session.getCarCatImage();
                    carCatImage = info.get(SessionManager.KEY_CAR_CAT_IAMGE);
                    if (!sCarImg.equalsIgnoreCase("")) {
                        Picasso.with(getApplicationContext()).load(String.valueOf(carCatImage)).into(IV_selectcategoryImage);
                    }
                    if (category_status) {
                        session.isFirstimeBook(count + "");
                        String FirstimeBook = session.isFirsttimeBook();
                        if (!driver_status) {
                            Tv_PickupNow.setTextColor(Color.parseColor("#ffffff"));
                            Rl_PickupNow.setClickable(true);
                        } else {
                            Tv_PickupNow.setTextColor(Color.parseColor("#ffffff"));
                            Rl_PickupNow.setClickable(true);
                        }

                    } else {
                    }
                } else {
                    dialogDismiss();
                    Rl_Alert_layout.setVisibility(View.VISIBLE);
                    Rl_Bottom_layout.setVisibility(GONE);
                    TV_Alert_textview.setText(fail_response);
                }
                //just // route();
                Rl_PickupNow.setEnabled(true);
                Rl_Back.setEnabled(true);
                isLoading = false;


                if (driver_list.size() > 0)
                    MyCurrent_lat = gps.getLatitude();
                MyCurrent_long = gps.getLongitude();
                ScurrentLat = MyCurrent_lat + "";
                ScurrentLan = MyCurrent_long + "";
                newMovingCarMarkers = new Marker[driver_list.size()];
                for (int i = 0; i < driver_list.size(); i++) {

                    double Dlongitude1 = Double.parseDouble(driver_list.get(i).getDriver_long());
                    double Dlatitude1 = Double.parseDouble(driver_list.get(i).getDriver_lat());


                    System.out.println("==================phantom latlon=============  " + Dlongitude1 + "   " + Dlatitude1);
                    final LatLng latlanNew = new LatLng(Dlongitude1, Dlatitude1);

                    fromNew.setLatitude(Dlatitude1);
                    fromNew.setLongitude(Dlongitude1);


                    toNew.setLatitude(Dlatitude + 0.002);
                    toNew.setLongitude(Dlongitude + 0.002);

                    fromLocation.add(fromNew);
                    ToLocation.add(toNew);
                    movingLocation.put("FromLocation", fromLocation);
                    movingLocation.put("ToLocation", ToLocation);

                    movingCarNewMarkers1(latlanNew, i);

                }


            }

            @Override
            public void onErrorListener() {
                spinKitView.setVisibility(GONE);
                Rl_PickupNow.setEnabled(true);
                Rl_Back.setEnabled(true);
                Rl_Alert_layout.setVisibility(View.VISIBLE);
                TV_Alert_textview.setText(fail_response);
                Rl_Bottom_layout.setVisibility(GONE);
                isLoading = false;
            }
        });
    }





    public String BitMapToString(Bitmap bitmap) {
        String temp = "";
        try {
            if (bitmap != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] b = baos.toByteArray();
                temp = Base64.encodeToString(b, Base64.DEFAULT);
            }
        } catch (Exception e) {

        }

        return temp;
    }

    //-----------Check Google Play Service--------
    private boolean CheckPlayService() {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(BookingPage4.this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        }
        return true;
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        BookingPage4.this.runOnUiThread(new Runnable() {
            public void run() {
                final Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionStatusCode, BookingPage4.this, REQUEST_CODE_RECOVER_PLAY_SERVICES);
                if (dialog == null) {
                    Toast.makeText(BookingPage4.this,  getResources().getString(R.string.incompitable), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(BookingPage4.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("ok_lable"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                bookforsomeone.setVisibility(View.VISIBLE);
                tv_setdropofflocation.setVisibility(View.GONE);
                Rl_Back.setVisibility(View.GONE);
                //global
                layout_home_iamge.setVisibility(View.VISIBLE);
                Rl_Bottom_layout.setVisibility(View.VISIBLE);
                book_my_ride_bottom_main_layout1.setVisibility(View.VISIBLE);

                book_my_ride_rideNow_layout.setVisibility(View.GONE);
                layout_home_iamge2.setVisibility(View.GONE);
                fav_lyout.setVisibility(View.GONE);
                back_lyout.setVisibility(View.GONE);

            }
        });
        mDialog.show();
    }

    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(BookingPage4.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("ok_lable"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }

    private void pickuptoofar(String title, String alert) {

        if (picksome != null) {
            picksome.dismiss();
        }

        picksome = new PkDialog(BookingPage4.this);
        picksome.setDialogTitle(title);
        picksome.setDialogMessage(alert);
        picksome.setImagehelp();
        picksome.setPositiveButton(getResources().getString(R.string.alert_yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picksome.dismiss();
                BookSomeOne();
            }
        });
        picksome.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picksome.dismiss();
            }
        });

        picksome.show();


    }

    private void NightModeAlert(String title, String alert, final boolean isNightMod) {

        final PkDialog mDialog = new PkDialog(BookingPage4.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getResources().getString(R.string.timer_label_alert_yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNightMod == true) {
                    Iv_CenterMarker.setImageResource(R.drawable.pickuptoofar);
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.mapstyle));
                    isNightMode = false;
                    session.setnightMode(true);
                    DestLocFromFindPlace();
                    centerMArker.setVisibility(View.VISIBLE);

                } else {
                    Iv_CenterMarker.setImageResource(R.drawable.icn_pointer_dot1);
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style));
                    isNightMode = true;
                    session.setnightMode(false);
                    DestLocFromFindPlace();
                    centerMArker.setVisibility(View.VISIBLE);

                }
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(BookingPage4.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(BookingPage4.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((requestCode == multiplePlaceSearch_request_code && resultCode == Activity.RESULT_OK && data != null)) {
            String sMultipleDrop = data.getStringExtra("MultipleDropName");
            multiple_dropList.clear();
            try {
                Bundle bundleObject = data.getExtras();
                multiple_dropList = (ArrayList<MultiDropPojo>) bundleObject.getSerializable("MultipleDropLocation_List");
                System.out.println("-----------multiple_dropList size-----------" + multiple_dropList.size());

                if (multiple_dropList.size() > 0) {
                    sMultipleDropStatus = "1";
                    STrip_Type = getResources().getString(R.string.multiplestop_lable);
                    Tv_TripType.setText(getResources().getString(R.string.multiplestop_textview));
                   /* Ll_OneWay.setBackground(getResources().getDrawable(R.drawable.blue_grad_topto_botton));
                    Ll_Return.setBackground(getResources().getDrawable(R.drawable.blue_grad_topto_botton));
                    Ll_MultipleStop.setBackground(getResources().getDrawable(R.drawable.green_grad_topto_bottom));
                    */// just     //route();
                }
                //                refreshLocation();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ((requestCode == pastrips && resultCode == Activity.RESULT_OK && data != null)) {

            Slocation = data.getStringExtra("Selected_Location");
            SLatitude = data.getStringExtra("Selected_Lattitude");
            SLongitude = data.getStringExtra("Selected_Longitude");

            if (Slocation.equalsIgnoreCase("")) {
                Rl_PickupNow.setEnabled(true);
                if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
                    SdestinationLatitude = SLatitude + "";
                    SdestinationLongitude = SLongitude + "";
                    SdestinationLocation = Slocation;
                }
            } else {
                if ("MultiDropLocation".equalsIgnoreCase(Str_Page)) {
                    SmultiDropLocation = Slocation;
                    SmultiDropLatitude = SLatitude + "";
                    SmultiDropLongitude = SLongitude + "";

                } else if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
                    SdestinationLatitude = SLatitude + "";
                    SdestinationLongitude = SLongitude + "";
                    SdestinationLocation = Slocation;
                } else if ("SomeOnePickup".equalsIgnoreCase(Str_Page)) {
                    SdestinationLatitude = SLatitude + "";
                    SdestinationLongitude = SLongitude + "";
                    SdestinationLocation = Slocation;
                }
            }

            System.out.println("=========set value1=================");
            book_my_ride_set_destination_edittextview.setText(Slocation);
            book_my_ride_set_destination_edittextview.setVisibility(View.VISIBLE);
            listview.setVisibility(GONE);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(SLatitude), Double.parseDouble(SLongitude))).zoom(17).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


        } else if ((requestCode == coupon_request_code && resultCode == Activity.RESULT_OK && data != null)) {
            ScouponSource = data.getStringExtra("couponSource");
            ScouponCode = data.getStringExtra("couponCode");
            session.setCouponCode(ScouponCode, ScouponSource);
            coponadded.setVisibility(VISIBLE);
            havecoupp.setVisibility(GONE);
        } else if ((requestCode == favoriteList_request_code && resultCode == Activity.RESULT_OK && data != null)) {
            SLatitude = data.getStringExtra("Selected_Latitude");
            SLongitude = data.getStringExtra("Selected_Longitude");
            Slocation = data.getStringExtra("Selected_Location");

            centerMArker.setVisibility(View.VISIBLE);

            SdestinationLatitude = SLatitude + "";
            SdestinationLongitude = SLongitude + "";
            SdestinationLocation = Slocation;

//            SdestinationLatitude = dLatitude + "";
//            SdestinationLongitude = dLongitude + "";
//            SdestinationLocation = Slocation;

            //  getDestinationAddress(Double.parseDouble(SLatitude), Double.parseDouble(SLongitude));

            System.out.println("=========set value5=================");
            book_my_ride_set_destination_edittextview.setVisibility(View.VISIBLE);

            book_my_ride_set_destination_edittextview.setText(Slocation);

            book_my_ride_set_destination_edittextview.setText(Slocation);

            isFromFindPlace = true;

            EnableOnCameraChange();

            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(SdestinationLatitude), Double.parseDouble(SdestinationLongitude))).zoom(17).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


        } else if (requestCode == timer_request_code && resultCode == Activity.RESULT_OK && data != null) {
            String ride_accepted = data.getStringExtra("Accepted_or_Not");
            String retry_count = data.getStringExtra("Retry_Count");

            if (retry_count.equalsIgnoreCase("1") && ride_accepted.equalsIgnoreCase("not")) {

                final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.newretry_layout);

                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

                TextView yes_btn = dialog.findViewById(R.id.cancel_trip_trackme_layout);
                TextView exit_yes = dialog.findViewById(R.id.retry_trip_exit_layout);
                yes_btn.setText(getkey("yes"));
                exit_yes.setText(getkey("cancel_lable"));

                TextView notav =  dialog.findViewById(R.id.notav);
                notav.setText(getkey("novehicles"));

                TextView tra = dialog. findViewById(R.id.tra);
                tra.setText(getkey("tryagain"));

                ImageView Rl_drawer = dialog.findViewById(R.id.blinking_image);

                final Animation animation = new AlphaAnimation(1, 0);
                animation.setDuration(500);
                animation.setInterpolator(new LinearInterpolator());
                animation.setRepeatCount(Animation.INFINITE);
                animation.setRepeatMode(Animation.REVERSE);
                Rl_drawer.startAnimation(animation);

                ImageView trackdriver_carphoto = dialog.findViewById(R.id.trackdriver_carphoto1);
                HashMap<String, String> info = session.getCarCatImage();
                carCatImage = info.get(SessionManager.KEY_CAR_CAT_IAMGE);
                System.out.println("======carCatImage======" + carCatImage);
                if (!carCatImage.equalsIgnoreCase("")) {
                    Picasso.with(getApplicationContext()).load(String.valueOf(carCatImage)).into(trackdriver_carphoto);
                }


                yes_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        ConfirmBookling1();
                    }
                });


                exit_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        cd = new ConnectionDetector(BookingPage4.this);
                        isInternetPresent = cd.isConnectingToInternet();
//                        bookforsomeone.setVisibility(View.VISIBLE);
//                        Rl_Back.setVisibility(View.VISIBLE);
//                        Rl_Bottom_layout.setVisibility(View.VISIBLE);
//                        book_my_ride_bottom_main_layout1.setVisibility(View.VISIBLE);
//

//                        book_my_ride_rideNow_layout.setVisibility(View.GONE);
//                        layout_home_iamge2.setVisibility(View.GONE);
//                        fav_lyout.setVisibility(View.GONE);
                        if (isInternetPresent) {
                            DeleteRideRequest(Iconstant.delete_ride_url);
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                        }
                    }
                });

                dialog.show();

            } else if (retry_count.equalsIgnoreCase("2") && ride_accepted.equalsIgnoreCase("not")) {
                DeleteRideRequest(Iconstant.delete_ride_url);
            } else if ((retry_count.equalsIgnoreCase("1") || retry_count.equalsIgnoreCase("2")) && ride_accepted.equalsIgnoreCase("Cancelled")) {


                riderId = "";

                //---------Hiding the bottom layout after cancel request--------
                if (googleMap != null) {
                    googleMap.getUiSettings().setAllGesturesEnabled(true);
                }
                Animation animFadeOut = AnimationUtils.loadAnimation(BookingPage4.this, R.anim.fade_out);
                Rl_ConfirmCancelLayout.startAnimation(animFadeOut);
                // Rl_ConfirmCancelLayout.setVisibility(View.GONE);
                lyt_book_now.setVisibility(GONE);

                Rl_roadimgelayout.setVisibility(View.GONE);
                lytpayment_method.setVisibility(GONE);
                // Rl_Payment_layout.setVisibility(GONE);
                CV_TripType.setVisibility(GONE);
                CV_TripType.setVisibility(GONE);
                Rl_PickUpLayout.setVisibility(View.VISIBLE);


//              Hlv_carTypes.setVisibility(View.VISIBLE);
                System.out.println("==================cancel 2================");
                Tv_PickupNow.setText(getResources().getString(R.string.confirm_lable));
                // Tv_PickupLater.setText(getResources().getString(R.string.schedule_lable));
                /*NEw Changes*/

                bookforsomeone.setVisibility(View.VISIBLE);
                tv_setdropofflocation.setVisibility(View.GONE);
                Rl_Back.setVisibility(View.VISIBLE);
                Rl_Bottom_layout.setVisibility(View.VISIBLE);
                book_my_ride_bottom_main_layout1.setVisibility(View.VISIBLE);

                book_my_ride_rideNow_layout.setVisibility(View.GONE);
                layout_home_iamge2.setVisibility(View.GONE);
                fav_lyout.setVisibility(View.GONE);

               /* if(markerpicUp!=null) {
                    markerpicUp.remove();
                    markerpicUp=null;
                }
                if(markerDrop!=null) {
                    markerDrop.remove();
                    markerDrop=null;
                }*/

           /*     if(MapAnimator.backgroundPolyline!=null) {
                    MapAnimator.backgroundPolyline.remove();
                }

                if(MapAnimator.foregroundPolyline!=null) {
                    MapAnimator.foregroundPolyline.remove();
                }*/

                Intent intent = new Intent(BookingPage4.this, Navigation_new.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();


            }
        } else if ((requestCode == placeSearch_request_code && resultCode == Activity.RESULT_OK && data != null)) {
            SLatitude = data.getStringExtra("Selected_Latitude");
            SLongitude = data.getStringExtra("Selected_Longitude");
            Slocation = data.getStringExtra("Selected_Location");

            System.out.println("-----------SdestinationLatitude book3----------------" + SLatitude);
            System.out.println("-----------SdestinationLongitude book3----------------" + SLongitude);
            System.out.println("-----------SdestinationLocation book3----------------" + Slocation);


            if (Slocation.equalsIgnoreCase("")) {
                Rl_PickupNow.setEnabled(true);
                if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
                    SdestinationLatitude = SLatitude + "";
                    SdestinationLongitude = SLongitude + "";
                    SdestinationLocation = Slocation;
                }
            } else {
                if ("MultiDropLocation".equalsIgnoreCase(Str_Page)) {
                    SmultiDropLocation = Slocation;
                    SmultiDropLatitude = SLatitude + "";
                    SmultiDropLongitude = SLongitude + "";

                } else if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
                    SdestinationLatitude = SLatitude + "";
                    SdestinationLongitude = SLongitude + "";
                    SdestinationLocation = Slocation;
                }
            }

            System.out.println("=========set value5=================");
            book_my_ride_set_destination_edittextview.setText(Slocation);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void route() {
        double lat_decimal = Double.parseDouble(SdestinationLatitude);
        double lng_decimal = Double.parseDouble(SdestinationLongitude);
        double pick_lat_decimal = Double.parseDouble(SpickupLatitude);
        double pick_lng_decimal = Double.parseDouble(SpickupLongitude);
        updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal);
    }


    private void updateGoogleMapTrackRide(double lat_decimal, double lng_decimal, double pick_lat_decimal, double pick_lng_decimal) {
       /* if (googleMap_confirm != null) {
            googleMap_confirm.clear();
        }*/
        if (googleMap != null) {
            googleMap.clear();
        }
        LatLng dropLatLng = new LatLng(lat_decimal, lng_decimal);
        LatLng pickUpLatLng = new LatLng(pick_lat_decimal, pick_lng_decimal);

        GetNewRouteTask draw_route_asyncTask = new GetNewRouteTask();
        draw_route_asyncTask.setToAndFromLocation(pickUpLatLng, dropLatLng, "BeginTrip");
        draw_route_asyncTask.execute();

    }

    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    private class GetNewRouteTask extends AsyncTask<String, Void, String> {

        String response = "";
        private LatLng currentLocation;
        private LatLng endLocation;

        private ArrayList<LatLng> wayLatLng;

        public void setToAndFromLocation(LatLng currentLocation, LatLng endLocation, String status) {
            this.currentLocation = currentLocation;
            this.endLocation = endLocation;
            startWayPoint = currentLocation;
            endWayPoint = endLocation;
            sWayPointStatus = status;

            /////
            Location startLocation = new Location(LocationManager.GPS_PROVIDER);
            startLocation.setLatitude(currentLocation.latitude);
            startLocation.setLongitude(currentLocation.longitude);
            Location endLocationBearing = new Location(LocationManager.GPS_PROVIDER);
            endLocationBearing.setLatitude(endLocation.latitude);
            endLocationBearing.setLongitude(endLocation.longitude);
            bearingValue = startLocation.bearingTo(endLocationBearing);
            /////

            this.wayLatLng = addWayPointPoint(currentLocation, endLocation, multiple_dropList); //multipleDropLatLng);
            System.out.println("====================wayLatLng===============" + this.wayLatLng);

        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... urls) {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(currentLocation, endLocation, GMapV2GetRouteDirection.MODE_DRIVING);
            SAmple routing = new SAmple.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(BookingPage4.this)
                    .alternativeRoutes(true)
                    .waypoints(wayLatLng)
                    .key(Iconstant.Routekey)
                    .build();
            routing.execute();
            response = "Success";
            return response;

        }

        @Override
        protected void onPostExecute(String result) {

            if (result.equalsIgnoreCase("Success")) {
            }
        }
    }

    private ArrayList<LatLng> addWayPointPoint(LatLng start, LatLng end, ArrayList<MultiDropPojo> mMultipleDropLatLng)//  String[] mMultipleDropLatLng) {
    {
//        googleMap_confirm.clear();
        googleMap.clear();
        wayPointList.clear();

        wayPointBuilder = new LatLngBounds.Builder();

        if (getResources().getString(R.string.return_lable).equals(STrip_Type)) {
            wayPointBuilder.include(start);
            wayPointBuilder.include(end);
            wayPointList.add(start);
            wayPointList.add(end);
            wayPointList.add(start);
        } else {
            wayPointBuilder.include(start);
            wayPointList.add(start);

//            if (sMultipleDropStatus.equalsIgnoreCase("1")) {
            if (mMultipleDropLatLng.size() > 0) {
                for (int i = 0; i < mMultipleDropLatLng.size(); i++) {

//                String sLatLng = mMultipleDropLatLng[i];
//                String[] latLng = sLatLng.split(",");
                    double Dlatitude = Double.parseDouble(mMultipleDropLatLng.get(i).getPlaceLat());
                    double Dlongitude = Double.parseDouble(mMultipleDropLatLng.get(i).getPlaceLong());
                    wayPointList.add(new LatLng(Dlatitude, Dlongitude));

                    wayPointBuilder.include(new LatLng(Dlatitude, Dlongitude));
                    addIcon(getResources().getString(R.string.stopover) + (i + 1), (new LatLng(Dlatitude, Dlongitude)), 2);


                }
            }

            wayPointBuilder.include(end);
            wayPointList.add(end);
        }


        return wayPointList;
    }

    @Override
    public void onRoutingFailure(com.blackmaria.waypoint.RouteException e) {
        if (e != null) {
            Toast.makeText(this, getResources().getString(R.string.error) + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, getResources().getString(R.string.somthing_went_wrong), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<com.blackmaria.waypoint.Route> route, int var2) {

    }


    @Override
    public void onRoutingCancelled() {

    }

    @Override
    public void onPostExecute(ArrayList<LatLng> result) {


    }


    private void updateCameraBearing(GoogleMap googleMap, float bearing) {
        if (googleMap == null) return;
        CameraPosition camPos = CameraPosition
                .builder(googleMap.getCameraPosition())
                .bearing(bearing)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
    }


    private void ConfirmRideRequest(String Url, final HashMap<String, String> jsonParams, final String try_value) {
        dialog = new Dialog(BookingPage4.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView loading = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        loading.setText(getResources().getString(R.string.action_pleasewait));

        System.out.println("--------------Confirm Ride url----bpp---------------" + Url);

        System.out.println("--------------Confirm Ride jsonParams-------------------" + jsonParams);


        mRequest = new ServiceRequest(BookingPage4.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------Confirm Ride reponse-------------------" + response);

                String selected_type = "", Sacceptance = "";
                String Str_driver_id = "", Str_driver_name = "", Str_driver_email = "", Str_driver_review = "",
                        Str_driver_lat = "", Str_driver_lon = "", Str_min_pickup_duration = "", Str_ride_id = "", Str_phone_number = "",
                        Str_vehicle_number = "", Str_vehicle_model = "", Str_photo = "", Str_photo_type;
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String status = object.getString("status");
                        if (status.equalsIgnoreCase("1"))
                        {

                            if(object.has("request_type"))
                            {
                                String request_type = object.getString("request_type");
                                if(request_type.equals("auto_assign"))
                                {
                                    checkrideacceptornot(Iconstant.track_your_ride_url,object.getString("ride_id"));
                                }
                                else
                                {
                                    JSONObject response_object = object.getJSONObject("response");

                                    Str_photo = object.getString("photo_video");
                                    Str_photo_type = object.getString("photo_video_type");

                                    selected_type = response_object.getString("type");
                                    Sacceptance = object.getString("acceptance");
                                    if (Sacceptance.equalsIgnoreCase("No")) {
                                        response_time = response_object.getString("response_time");
                                    }
                                    riderId = response_object.getString("ride_id");


                                    if (Sacceptance.equalsIgnoreCase("Yes")) {
                                        JSONObject driverObject = response_object.getJSONObject("driver_profile");

                                        Str_driver_id = driverObject.getString("driver_id");
                                        Str_driver_name = driverObject.getString("driver_name");
                                        Str_driver_email = driverObject.getString("driver_email");
                                        Str_driver_image = driverObject.getString("driver_image");
                                        Str_driver_review = driverObject.getString("driver_review");
                                        Str_driver_lat = driverObject.getString("driver_lat");
                                        Str_driver_lon = driverObject.getString("driver_lon");
                                        Str_min_pickup_duration = driverObject.getString("min_pickup_duration");

                                        Str_ride_id = driverObject.getString("ride_id");

                                        Str_phone_number = driverObject.getString("phone_number");
                                        session.setKeyDriverPhonenumber(Str_phone_number);
                                        Str_vehicle_number = driverObject.getString("vehicle_number");
                                        Str_vehicle_model = driverObject.getString("vehicle_model");
                                    }

                                    if (selected_type.equalsIgnoreCase("1")) {
                                        stopTimer();
                                        dialogDismiss();
                                        final PkDialog mDialog = new PkDialog(BookingPage4.this);
                                        mDialog.setDialogTitle(getResources().getString(R.string.action_success));
                                        mDialog.setDialogMessage(getResources().getString(R.string.ridenow_label_confirm_success));
                                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mDialog.dismiss();
                                                session.setCouponCode("", "");
                                                session.setReferralCode("", "");
                                                Intent intent = new Intent(BookingPage4.this, Navigation_new.class);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.enter, R.anim.exit);
                                                finish();
                                            }
                                        });
                                        mDialog.show();

                                    } else if (selected_type.equalsIgnoreCase("0")) {

                                        //  isRunnable = false;
                                        stopTimer();
                                        if (Sacceptance.equalsIgnoreCase("Yes")) {
                                            dialogDismiss();

                                            if (Sacceptance.equalsIgnoreCase("Yes")) {
                                                session.setCouponCode("", "");
                                                session.setReferralCode("", "");
                                            }
                                            stopTimer();
                                            //Move to ride Detail page
                                            Intent i = new Intent(BookingPage4.this, TrackRideAcceptPage.class);
                                            i.putExtra("driverID", Str_driver_id);
                                            i.putExtra("driverName", Str_driver_name);
                                            i.putExtra("driverImage", Str_driver_image);
                                            i.putExtra("driverRating", Str_driver_review);
                                            i.putExtra("driverTime", Str_min_pickup_duration);
                                            i.putExtra("rideID", Str_ride_id);
                                            i.putExtra("driverMobile", Str_phone_number);
                                            i.putExtra("driverCar_no", Str_vehicle_number);
                                            i.putExtra("driverCar_model", Str_vehicle_model);

                                            i.putExtra("flag", "1");
                                            startActivity(i);
                                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                            finish();

                                        }
                                        else {
                                            dialogDismiss();
                                            if (moveNext) {

                                                //  isRunnable = false;
                                                stopTimer();

                                                Intent intent = new Intent(BookingPage4.this, TimerPage.class);
                                                intent.putExtra("Time", response_time);
                                                intent.putExtra("retry_count", try_value);
                                                intent.putExtra("ride_ID", riderId);
                                                intent.putExtra("Str_photo", Str_photo);
                                                intent.putExtra("Str_photo_type", Str_photo_type);
                                                startActivityForResult(intent, timer_request_code);
                                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                                                book_my_ride_rideNow_layout.setVisibility(View.VISIBLE);

                                                layout_home_iamge.setVisibility(View.VISIBLE);
                                                layout_home_iamge2.setVisibility(View.GONE);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                JSONObject response_object = object.getJSONObject("response");

                                Str_photo = object.getString("photo_video");
                                Str_photo_type = object.getString("photo_video_type");

                                selected_type = response_object.getString("type");
                                Sacceptance = object.getString("acceptance");
                                if (Sacceptance.equalsIgnoreCase("No")) {
                                    response_time = response_object.getString("response_time");
                                }
                                riderId = response_object.getString("ride_id");


                                if (Sacceptance.equalsIgnoreCase("Yes")) {
                                    JSONObject driverObject = response_object.getJSONObject("driver_profile");

                                    Str_driver_id = driverObject.getString("driver_id");
                                    Str_driver_name = driverObject.getString("driver_name");
                                    Str_driver_email = driverObject.getString("driver_email");
                                    Str_driver_image = driverObject.getString("driver_image");
                                    Str_driver_review = driverObject.getString("driver_review");
                                    Str_driver_lat = driverObject.getString("driver_lat");
                                    Str_driver_lon = driverObject.getString("driver_lon");
                                    Str_min_pickup_duration = driverObject.getString("min_pickup_duration");

                                    Str_ride_id = driverObject.getString("ride_id");

                                    Str_phone_number = driverObject.getString("phone_number");
                                    session.setKeyDriverPhonenumber(Str_phone_number);
                                    Str_vehicle_number = driverObject.getString("vehicle_number");
                                    Str_vehicle_model = driverObject.getString("vehicle_model");
                                }

                                if (selected_type.equalsIgnoreCase("1")) {
                                    stopTimer();
                                    dialogDismiss();
                                    final PkDialog mDialog = new PkDialog(BookingPage4.this);
                                    mDialog.setDialogTitle(getResources().getString(R.string.action_success));
                                    mDialog.setDialogMessage(getResources().getString(R.string.ridenow_label_confirm_success));
                                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                            session.setCouponCode("", "");
                                            session.setReferralCode("", "");
                                            Intent intent = new Intent(BookingPage4.this, Navigation_new.class);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.enter, R.anim.exit);
                                            finish();
                                        }
                                    });
                                    mDialog.show();

                                } else if (selected_type.equalsIgnoreCase("0")) {

                                    //  isRunnable = false;
                                    stopTimer();
                                    if (Sacceptance.equalsIgnoreCase("Yes")) {
                                        dialogDismiss();

                                        if (Sacceptance.equalsIgnoreCase("Yes")) {
                                            session.setCouponCode("", "");
                                            session.setReferralCode("", "");
                                        }
                                        stopTimer();
                                        //Move to ride Detail page
                                        Intent i = new Intent(BookingPage4.this, TrackRideAcceptPage.class);
                                        i.putExtra("driverID", Str_driver_id);
                                        i.putExtra("driverName", Str_driver_name);
                                        i.putExtra("driverImage", Str_driver_image);
                                        i.putExtra("driverRating", Str_driver_review);
                                        i.putExtra("driverTime", Str_min_pickup_duration);
                                        i.putExtra("rideID", Str_ride_id);
                                        i.putExtra("driverMobile", Str_phone_number);
                                        i.putExtra("driverCar_no", Str_vehicle_number);
                                        i.putExtra("driverCar_model", Str_vehicle_model);

                                        i.putExtra("flag", "1");
                                        startActivity(i);
                                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                        finish();

                                    }
                                    else {
                                        dialogDismiss();
                                        if (moveNext) {

                                            //  isRunnable = false;
                                            stopTimer();

                                            Intent intent = new Intent(BookingPage4.this, TimerPage.class);
                                            intent.putExtra("Time", response_time);
                                            intent.putExtra("retry_count", try_value);
                                            intent.putExtra("ride_ID", riderId);
                                            intent.putExtra("Str_photo", Str_photo);
                                            intent.putExtra("Str_photo_type", Str_photo_type);
                                            startActivityForResult(intent, timer_request_code);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                                            book_my_ride_rideNow_layout.setVisibility(View.VISIBLE);

                                            layout_home_iamge.setVisibility(View.VISIBLE);
                                            layout_home_iamge2.setVisibility(View.GONE);
                                        }
                                    }
                                }
                            }

                        } else if (status.equalsIgnoreCase("2")) {

                            String Sresponse = object.getString("response");
                            checkBalanceWhileBookingPopUp();
                            //Alert1(getResources().getString(R.string.alert_label_title), Sresponse, "1");

                        } else if (status.equalsIgnoreCase("3")) {

                            String Sresponse = object.getString("response");
                            checkBalanceWhileBookingPopUp();
                            //Alert1(getResources().getString(R.string.alert_label_title), Sresponse, "2");

                        } else {
                            String Sresponse = object.getString("response");
                            Alert1(getResources().getString(R.string.alert_label_title), Sresponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialogDismiss();

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }
    //-------------------Delete Ride Post Request----------------

    private void DeleteRideRequest(String Url) {
        System.out.println("==================cancel 3================");
        dialog = new Dialog(BookingPage4.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView loading = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        loading.setText(getResources().getString(R.string.action_pleasewait));

        System.out.println("--------------Delete Ride url-------------------" + Url);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", riderId);

        mRequest = new ServiceRequest(BookingPage4.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Delete Ride reponse-------------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String status = object.getString("status");
                        String response_value = object.getString("response");
                        if (status.equalsIgnoreCase("1")) {
                            riderId = "";
                            Intent intent = new Intent(BookingPage4.this, Navigation_new.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                            //Alert(getResources().getString(R.string.action_success), response_value);
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), response_value);
                        }


                        //---------Hiding the bottom layout after cancel request--------
                        googleMap.getUiSettings().setAllGesturesEnabled(true);
                        Animation animFadeOut = AnimationUtils.loadAnimation(BookingPage4.this, R.anim.fade_out);
                        Rl_ConfirmCancelLayout.startAnimation(animFadeOut);
                        //      Rl_ConfirmCancelLayout.setVisibility(View.GONE);
                        lyt_book_now.setVisibility(GONE);

                        Rl_roadimgelayout.setVisibility(GONE);
                        lytpayment_method.setVisibility(GONE);
                        //   Rl_Payment_layout.setVisibility(GONE);
                        CV_TripType.setVisibility(GONE);

                        Rl_PickUpLayout.setVisibility(View.VISIBLE);
                        book_my_ride_selectcar_triptype_layout.setVisibility(View.VISIBLE);
                        CV_TripType.setVisibility(GONE);

//                        Hlv_carTypes.setVisibility(View.VISIBLE);

                        Tv_PickupNow.setText(getResources().getString(R.string.confirm_lable));
                        // Tv_PickupLater.setText(getResources().getString(R.string.schedule_lable));

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void sampleapopaup() {
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.waitingtime_layout);

        final SeekBar seekBar1 = (SeekBar) dialog.findViewById(R.id.seekBarDistance1);
        final TextView min_distance = (TextView) dialog.findViewById(R.id.min_distance);
        final TextView max_distance = (TextView) dialog.findViewById(R.id.max_distance);
        final TextView waitingtime = (TextView) dialog.findViewById(R.id.waitingtime);
        final TextView text_minute = (TextView) dialog.findViewById(R.id.text_minute);
        final TextView waitingtime_rate = (TextView) dialog.findViewById(R.id.waitingtime_rate);
        RelativeLayout confrim_layout = (RelativeLayout) dialog.findViewById(R.id.confrim_layout);
        final ImageView closeImage = (ImageView) dialog.findViewById(R.id.close_imaage);
        final TextView isretrunmulti = (TextView) dialog.findViewById(R.id.isretrunmulti);

        isretrunmulti.setText(isRetrunOrMulti);

        seekBar1.setMax(60);
        seekBar1.setProgress(2);
        WaitingTime = seekBar1.getProgress() + "";
        waitingtime.setText(seekBar1.getProgress() + "");
        min_distance.setText(seekBar1.getProgress() +  getResources().getString(R.string.min));
        ScurrencySymbol = session.getCurrency();

        try {
            if (seekBar1.getProgress() > 0) {

                Double freetime = Double.parseDouble(str_FreeWaitingTime);
                Double freeMin = Double.parseDouble(seekBar1.getProgress() + "") - freetime;
                Double freeRate = (Double.parseDouble(str_FreeWaitingRate));
                Double waitingRate = freeMin * freeRate;
                if (String.valueOf(waitingRate).contains("-")) {
                    waitingtime_rate.setText("0.0");
                } else {
                    waitingtime_rate.setText(ScurrencySymbol + " " + waitingRate);
                }
//                } else {
//                    Double freetime = Double.parseDouble("0");
//                    Double freeMin = Double.parseDouble(seekBar1.getProgress() + "") - freetime;
//                    Double freeRate = (Double.parseDouble("0"));
//                    Double waitingRate = freeMin * freeRate;
//                    if (String.valueOf(waitingRate).contains("-")) {
//                        waitingtime_rate.setText("0.0");
//                    } else {
//                        waitingtime_rate.setText(ScurrencySymbol + " " + waitingRate);
//                    }
//                }
            }
        } catch (NumberFormatException e) {
        }
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (inflateuserinfo.getVisibility() == VISIBLE) {
                    IV_selectcategoryImage.setVisibility(GONE);
                    inflateuserinfo.setVisibility(GONE);
                }

                isCameraChange = false;
                if (googleMap != null) {
                    googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
                }
                Iv_CenterMarker.setImageResource(R.drawable.pickuptoofar);
                Iv_CenterMarker.setVisibility(VISIBLE);
                centerMArker.setVisibility(VISIBLE);
            }
        });
        confrim_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //New
//                centerMArker.setVisibility(View.VISIBLE);
                Iv_CenterMarker.setImageResource(R.drawable.pickuptoofar);
                Iv_CenterMarker.setVisibility(VISIBLE);
                centerMArker.setVisibility(VISIBLE);
                Rl_PickupNow.setEnabled(true);
                book_my_ride_rideNow_layout.setVisibility(View.VISIBLE);
                Tv_PickupNow.setText(getResources().getString(R.string.ok_lable));
                Tv_PickupNow.setEnabled(true);
                fav_lyout.setVisibility(View.VISIBLE);
                bookforsomeone.setVisibility(View.GONE);
                tv_setdropofflocation.setVisibility(View.VISIBLE);

                Rl_Back.setVisibility(View.GONE);
                back_lyout.setVisibility(View.VISIBLE);

                Rl_destination_layout.setVisibility(View.VISIBLE);
                book_my_ride_selectcar_triptype_layout.setVisibility(View.GONE);
                DestLocFromFindPlace();
                dialog.dismiss();
                System.out.println("....." + WaitingTime);

            }
        });

        seekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, final int progress, boolean fromUser) {
                waitingtime.setText(seekBar1.getProgress() + "");
                if (seekBar1.getProgress() > 1) {
                    text_minute.setText( getResources().getString(R.string.miniuts));
                } else {
                    text_minute.setText("MINUTE");
                }
                min_distance.setText(seekBar1.getProgress() + "MIN");
                WaitingTime = progress + "";
                WaitingTime = waitingtime.getText().toString().trim();

                if (progress > 0) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            Double waitingRate = getprogressvalue(str_FreeWaitingTime, str_FreeWaitingRate, seekBar1.getProgress());
//                            WaitingTime = waitingRate + "";
                            //  WaitingTime = freeMin + "";
                            WaitingTime = waitingtime.getText().toString().trim();
                            if (String.valueOf(waitingRate).contains("-")) {
                                waitingtime_rate.setText(ScurrencySymbol + " " + "0.0");
                            } else {
                                waitingtime_rate.setText(ScurrencySymbol + " " + waitingRate);
                            }
                        }
                    }, 100);

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public Double getprogressvalue(String str_FreeWaitingTime, String str_FreeWaitingRate, int progress) {

        Double waitingRate = 0.0;
        try {
            Double freetime = Double.parseDouble(str_FreeWaitingTime);
            Double freeMin = Double.parseDouble(progress + "") - freetime;
            Double freeRate = (Double.parseDouble(str_FreeWaitingRate));
            waitingRate = freeMin * freeRate;
        } catch (Exception e) {
        }

        return waitingRate;
    }





    private void GohomePopUp(String message) {
        if(showdsialog != null && showdsialog.isShowing())
        {

        }
        else
        {
         showdsialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        showdsialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        showdsialog.setContentView(R.layout.go_home_popup_constrain);
        final TextView Ok_Tv = showdsialog.findViewById(R.id.custom_dialog_library_cancel_button);
        final ImageView cancel_payment = (ImageView) showdsialog.findViewById(R.id.custom_dialog_library_ok_button);
        final TextView balance_amount = showdsialog.findViewById(R.id.custom_dialog_library_message_textview);
        Ok_Tv.setText(getkey("yes_text"));

        balance_amount.setText(message);
        cancel_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showdsialog.dismiss();
            }
        });

        Ok_Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   isRunnable = false;
                stopTimer();
                Intent intent = new Intent(BookingPage4.this, Navigation_new.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                showdsialog.dismiss();

            }
        });

        showdsialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        showdsialog.show();
        }

    }

    private void PaymentChoosePopUp() {
        isCameraChange = false;








        Hlv_paymentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
            }
        });

        if (payment_list_status) {
            paymentListAdapter = new BookingPage4Adapter(BookingPage4.this, paymnet_list);
            Hlv_paymentList.setAdapter(paymentListAdapter);
            paymentListAdapter.notifyDataSetChanged();
            Hlv_paymentList.setExpanded(true);
        }


    }

    private void PaymentChoosePopUp_wallet(final String eta_amount, final String errormessage, String mode) {
        isCameraChange = false;
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.paymentselectiondialog_wallet);
        final TextView close_trck = dialog.findViewById(R.id.xmark);
        close_trck.setText(getkey("cancel_trip"));
        ListView Hlv_paymentListinsuf = dialog.findViewById(R.id.book_my_ride_payment_type_listview1);
        final TextView estimate_amount = dialog.findViewById(R.id.estimate_amount);
        final TextView tv_reloadnow_wallet = dialog.findViewById(R.id.tv_reloadnow_wallet);
        tv_reloadnow_wallet.setText(getkey("reload_now"));
        final TextView tv_errormessage = dialog.findViewById(R.id.tv_errormessage);
        final ImageView iv_fastwallet = dialog.findViewById(R.id.iv_fastwallet);
        final ImageView closeme = dialog.findViewById(R.id.closeme);

        final TextView pleac = dialog.findViewById(R.id.pleac);
        pleac.setText(getkey("or_please_select_other_payments"));

        estimate_amount.setText(this.getResources().getString(R.string.fare_lable)+" - " + fareAmount);
        tv_errormessage.setText(errormessage);


        //add withoutcard list
        final ArrayList<HomePojo> homePojos = new ArrayList<>();

        for (int i = 0; i <= paymnet_list.size() - 1; i++) {
            if (!paymnet_list.get(i).getPayment_code().equalsIgnoreCase(mode)) {
                homePojos.add(paymnet_list.get(i));
            } else {
              //  Glide.with(this).load(paymnet_list.get(i).getPayment_icon()).into(iv_fastwallet);
            }
        }

        close_trck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GohomePopUp(getResources().getString(R.string.close_booking_cancel1));
            }
        });

        closeme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tv_reloadnow_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
               /* ReloadAmountWallet(eta_amount, errormessage);*/
                session.setbookingonProgress("1");
                Intent intent = new Intent(BookingPage4.this, Fastpayhome.class);
                intent.putExtra("frombookingpage", "1");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                dialog.dismiss();
            }
        });

        Hlv_paymentListinsuf.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int positions, long id) {
                if (homePojos.size() > 0) {
                    for (int i = 0; i < homePojos.size(); i++) {
                        if (i == positions) {
                            homePojos.get(i).setSetPayment_selected_payment_id("true");
                        } else {
                            homePojos.get(i).setSetPayment_selected_payment_id("false");
                        }
                    }
                    paymentListAdapter.notifyDataSetChanged();
                    SpaymentMode = homePojos.get(positions).getPayment_code();
                    paymenttype = homePojos.get(positions).getPayment_name();
                    System.out.println("============muruga paymenttype==============" + paymenttype + " " + SpaymentMode);
                    isCameraChange = false;
                    dialog.dismiss();
                    if (!SpaymentMode.equalsIgnoreCase("Cash")) {
                        String postionofcash = "";


                        for (int i = 0; i < paymnet_list.size(); i++) {
                            HomePojo pojo = new HomePojo();
                            pojo.setCat_image("0");
                            if(paymnet_list.get(i).getPayment_code().equalsIgnoreCase("xendit-card"))
                            {
                                postionofcash = ""+i;
                            }
                            pojo.setPayment_code(paymnet_list.get(i).getPayment_code());
                            pojo.setPayment_name(paymnet_list.get(i).getPayment_name());
                            pojo.setPayment_icon(paymnet_list.get(i).getPayment_icon());
                            pojo.setSetpaymentinactiveicon(paymnet_list.get(i).getSetpaymentinactiveicon());
                            paymnet_list.set(i,pojo);
                        }

                        if(!postionofcash.equals(""))
                        {
                            int position = Integer.parseInt(postionofcash);
                            HomePojo pojo = new HomePojo();
                            pojo.setCat_image("1");
                            pojo.setPayment_code(paymnet_list.get(position).getPayment_code());
                            pojo.setPayment_name(paymnet_list.get(position).getPayment_name());
                            pojo.setPayment_icon(paymnet_list.get(position).getPayment_icon());
                            pojo.setSetpaymentinactiveicon(paymnet_list.get(position).getSetpaymentinactiveicon());
                            paymnet_list.set(position,pojo);


                            if (paymnet_list.size() > 0) {
                                for (int i = 0; i < paymnet_list.size(); i++) {
                                    if (i == position) {
                                        paymnet_list.get(i).setSetPayment_selected_payment_id("true");
                                    } else {
                                        paymnet_list.get(i).setSetPayment_selected_payment_id("false");
                                    }
                                }
                                paymentListAdapter = new BookingPage4Adapter(BookingPage4.this, paymnet_list);
                                Hlv_paymentList.setAdapter(paymentListAdapter);
                                paymentListAdapter.notifyDataSetChanged();
                                SpaymentMode = paymnet_list.get(position).getPayment_code();
                                paymenttype = paymnet_list.get(position).getPayment_name();
                                isCameraChange = false;
                            }
                        }

                        if (isInternetPresent) {
                            PaymentMethodList(Iconstant.User_Booking_PaymentChoose_Url);
                        } else {
                            Alert(getResources().getString(R.string.alert), getResources().getString(R.string.alert_nointernet));
                        }
                    }
                    else
                    {

                        String postionofcash = "";


                        for (int i = 0; i < paymnet_list.size(); i++) {
                            HomePojo pojo = new HomePojo();
                            pojo.setCat_image("0");
                            if(paymnet_list.get(i).getPayment_code().equalsIgnoreCase("cash"))
                            {
                                postionofcash = ""+i;
                            }
                            pojo.setPayment_code(paymnet_list.get(i).getPayment_code());
                            pojo.setPayment_name(paymnet_list.get(i).getPayment_name());
                            pojo.setPayment_icon(paymnet_list.get(i).getPayment_icon());
                            pojo.setSetpaymentinactiveicon(paymnet_list.get(i).getSetpaymentinactiveicon());
                            paymnet_list.set(i,pojo);
                        }

                        if(!postionofcash.equals(""))
                        {
                            int position = Integer.parseInt(postionofcash);
                            HomePojo pojo = new HomePojo();
                            pojo.setCat_image("1");
                            pojo.setPayment_code(paymnet_list.get(position).getPayment_code());
                            pojo.setPayment_name(paymnet_list.get(position).getPayment_name());
                            pojo.setPayment_icon(paymnet_list.get(position).getPayment_icon());
                            pojo.setSetpaymentinactiveicon(paymnet_list.get(position).getSetpaymentinactiveicon());
                            paymnet_list.set(position,pojo);


                            if (paymnet_list.size() > 0) {
                                for (int i = 0; i < paymnet_list.size(); i++) {
                                    if (i == position) {
                                        paymnet_list.get(i).setSetPayment_selected_payment_id("true");
                                    } else {
                                        paymnet_list.get(i).setSetPayment_selected_payment_id("false");
                                    }
                                }
                                paymentListAdapter = new BookingPage4Adapter(BookingPage4.this, paymnet_list);
                                Hlv_paymentList.setAdapter(paymentListAdapter);
                                paymentListAdapter.notifyDataSetChanged();
                                SpaymentMode = paymnet_list.get(position).getPayment_code();
                                paymenttype = paymnet_list.get(position).getPayment_name();
                                 isCameraChange = false;
                                if (!SpaymentMode.equalsIgnoreCase("Cash")) {
                                    if (isInternetPresent) {
                                        PaymentMethodList(Iconstant.User_Booking_PaymentChoose_Url);
                                    } else {
                                        Alert(getResources().getString(R.string.alert), getResources().getString(R.string.alert_nointernet));
                                    }
                                }
                            }
                        }



                    }
                }
            }
        });

        if (payment_list_status) {
            PaymentListAdapter1 paymentListAdapter = new PaymentListAdapter1(BookingPage4.this, homePojos);
            Hlv_paymentListinsuf.setAdapter(paymentListAdapter);
            paymentListAdapter.notifyDataSetChanged();
        }

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void PaymentChoosePopUp_card(final String eta_amount, final String errormessage, String mode) {
        isCameraChange = false;
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.paymentselectiondialog_card);
        final TextView close_trck = dialog.findViewById(R.id.xmark);
        ListView Hlv_paymentList = dialog.findViewById(R.id.book_my_ride_payment_type_listview1);
        final TextView estimate_amount = dialog.findViewById(R.id.estimate_amount);
        final TextView useothercard = dialog.findViewById(R.id.useothercard);
        estimate_amount.setText(this.getResources().getString(R.string.fare_lable)+" - " + fareAmount);

        //add withoutcard list
        final ArrayList<HomePojo> homePojos = new ArrayList<>();

        for (int i = 0; i <= paymnet_list.size() - 1; i++) {
            if (!paymnet_list.get(i).getPayment_code().equalsIgnoreCase(mode)) {
                homePojos.add(paymnet_list.get(i));
            }
        }
        close_trck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                isCameraChange = false;
                dialog.dismiss();
            }
        });

        useothercard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                XenditiPayment(eta_amount);
            }
        });

        Hlv_paymentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (homePojos.size() > 0) {
                    for (int i = 0; i < homePojos.size(); i++) {
                        if (i == position) {
                            homePojos.get(i).setSetPayment_selected_payment_id("true");
                        } else {
                            homePojos.get(i).setSetPayment_selected_payment_id("false");
                        }
                    }
                    paymentListAdapter.notifyDataSetChanged();
                    SpaymentMode = homePojos.get(position).getPayment_code();
                    paymenttype = homePojos.get(position).getPayment_name();
                    System.out.println("============muruga paymenttype==============" + paymenttype + " " + SpaymentMode);
                    isCameraChange = false;
                    dialog.dismiss();
                    if (!SpaymentMode.equalsIgnoreCase("Cash")) {
                        if (isInternetPresent) {
                            PaymentMethodList(Iconstant.User_Booking_PaymentChoose_Url);
                        } else {
                            Alert(getResources().getString(R.string.alert), getResources().getString(R.string.alert_nointernet));
                        }
                    }
                }
            }
        });

        if (payment_list_status) {
            PaymentListAdapter1 paymentListAdapter = new PaymentListAdapter1(BookingPage4.this, homePojos);
            Hlv_paymentList.setAdapter(paymentListAdapter);
            paymentListAdapter.notifyDataSetChanged();
        }

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void TrackMePopUp() {
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.tracking_popup);
        final TextView close_trck = (TextView) dialog.findViewById(R.id.close_trck);
        final ImageView close_image = (ImageView) dialog.findViewById(R.id.close_image);
        close_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        close_trck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void roadRulesPopUp() {
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.road_rules_layout);
        final TextView close_trck = (TextView) dialog.findViewById(R.id.close_trck);
        final TextView Tv_bottom = (TextView) dialog.findViewById(R.id.bottom_tv);
        final ImageView close_image = (ImageView) dialog.findViewById(R.id.close_image);

        final TextView conddd = (TextView) dialog.findViewById(R.id.conddd);
        conddd.setText(getkey("roadrules_inc1"));
        final TextView conddd2 = (TextView) dialog.findViewById(R.id.conddd2);
        conddd2.setText(getkey("roadrules_inc2"));
        final TextView conddd3 = (TextView) dialog.findViewById(R.id.conddd3);
        conddd3.setText(getkey("roadrules_inc3"));
        final TextView conddd4 = (TextView) dialog.findViewById(R.id.conddd4);
        conddd4.setText(getkey("roadrules_inc4"));

        final TextView rulesor = (TextView) dialog.findViewById(R.id.rulesor);
        rulesor.setText(getkey("help_page_label_user_guides"));


        final TextView conddd5 = (TextView) dialog.findViewById(R.id.conddd5);
        conddd5.setText(getkey("roadrules_inc5"));
        final TextView conddd6 = (TextView) dialog.findViewById(R.id.conddd6);
        conddd6.setText(getkey("roadrules_inc6"));
        Tv_bottom.setText(getkey("roadrules_inc7"));
        close_trck.setText(getkey("close_lable"));
        Tv_bottom.setTypeface(face);
        close_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        close_trck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void checkBalanceWhileBookingPopUp() {
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.check_bal_wallet_scan_card_);
        final Button reloadWallet = (Button) dialog.findViewById(R.id.reload_wallet_money);
        final Button scan_card = (Button) dialog.findViewById(R.id.scan_card);
        final Button cancel_payment = (Button) dialog.findViewById(R.id.cancel_payment);
        final CustomTextView balance_amount = (CustomTextView) dialog.findViewById(R.id.balance_amount);
        cancel_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        scan_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_profile = new Intent(BookingPage4.this, Fastpaypincheck.class);
                intent_profile.putExtra("frommenu", "1");
                startActivity(intent_profile);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                dialog.dismiss();

            }
        });
        reloadWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookingPage4.this, WalletMoneyPage1.class);
                intent.putExtra("frombookingpage", "1");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                dialog.dismiss();

            }
        });
        balance_amount.setText(getResources().getString(R.string.remailning_balance) + session.getWalletAmount());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void BookSomeOne() {
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.someone_else_booking_new);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // inflateBookSomeOne.setVisibility(View.VISIBLE);
        ImageView bkng_closeImge = (ImageView) dialog.findViewById(R.id.label_close_imageview);
        Spinner spinnerCustom = (Spinner) dialog.findViewById(R.id.gender_spinner);
        Tv_city = (TextView) dialog.findViewById(R.id.ratecard_city_textview);
        TextView label_btn = (TextView) dialog.findViewById(R.id.confirmtext);
        label_text1 = (CustomEdittext) dialog.findViewById(R.id.label_text1);
        mobileNumber = (CustomEdittext) dialog.findViewById(R.id.mobilenumber);
        Tv_countryCode = (CustomTextCambriaItalic) dialog.findViewById(R.id.txt_country_code);
        Tv_countryCode.setText(CountryCodeStr);


        final LinearLayout oneWay = (LinearLayout) dialog.findViewById(R.id.book_my_ride_one_way_layout);
        final LinearLayout returnTrip = (LinearLayout) dialog.findViewById(R.id.book_my_ride_return_layout);
        final LinearLayout multiTrip = (LinearLayout) dialog.findViewById(R.id.book_my_ride_multiple_stop_layout);


        Tv_countryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                Tv_countryCode.setText(dialCode);
                CountryCodeStr = dialCode;
            }
        });


        oneWay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tripTypeSpinner = "ONEWAY";
                oneWay.setBackgroundDrawable(getResources().getDrawable(R.drawable.green_grad_topto_bottom));
                returnTrip.setBackground(getResources().getDrawable(R.drawable.blue_grad_topto_botton));
                multiTrip.setBackground(getResources().getDrawable(R.drawable.blue_grad_topto_botton));

            }
        });
        returnTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tripTypeSpinner = "RETURN";
                oneWay.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_grad_topto_botton));
                returnTrip.setBackground(getResources().getDrawable(R.drawable.green_grad_topto_bottom));
                multiTrip.setBackground(getResources().getDrawable(R.drawable.blue_grad_topto_botton));

            }
        });
        multiTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tripTypeSpinner = "MULTIPLE";
                oneWay.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_grad_topto_botton));
                returnTrip.setBackground(getResources().getDrawable(R.drawable.blue_grad_topto_botton));
                multiTrip.setBackground(getResources().getDrawable(R.drawable.green_grad_topto_bottom));
            }
        });

//        label_text1.setCursorVisible(false);
        // Spinner Drop down elements
        ArrayList<String> spinnerItems = new ArrayList<>();
        spinnerItems.add(getResources().getString(R.string.gender));
        spinnerItems.add(getResources().getString(R.string.male));
        spinnerItems.add(getResources().getString(R.string.female));
        spinnerItems.add(getResources().getString(R.string.others));
        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(BookingPage4.this, spinnerItems);
        spinnerCustom.setAdapter(customSpinnerAdapter);

        spinnerCustom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(label_text1.getWindowToken(), 0);
                return false;
            }
        });
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                Tv_city.setText(item);
                cd = new ConnectionDetector(BookingPage4.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (!item.equalsIgnoreCase("GENDER")) {
                    if (isInternetPresent) {
                        System.out.println("-------------Sselected_cityID-----------------" + item);
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Tv_city.setText(getResources().getString(R.string.myprofile_dialog_form_gender_label));
            }
        });


        bkng_closeImge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   inflateBookSomeOne.setVisibility(View.GONE);
                CloseKeyboard(label_text1);
                bookingReference = "No";
                dialog.dismiss();
            }
        });

        label_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   inflateBookSomeOne.setVisibility(View.GONE);
                if (label_text1.getText().toString().length() == 0) {
                    erroredit(label_text1, getResources().getString(R.string.profile_label_alert_username));
                } else if (Tv_city.getText().toString().equalsIgnoreCase("GENDER")) {
                    WIthDrawlResponsePopUP1("Alert", getResources().getString(R.string.profile_label_alert_gender));
                } else if (tripTypeSpinner.equalsIgnoreCase("")) {
                    WIthDrawlResponsePopUP1("Alert", getResources().getString(R.string.profile_label_alert_trip));
                } else if (!isValidPhoneNumber(mobileNumber.getText().toString())) {
                    erroredit(mobileNumber, getResources().getString(R.string.login_page_alert_mobileNo));
                } else {
                    bookingReference = "Yes";
                    Intent intSomeOne = new Intent(BookingPage4.this, BookSomeOnePickupLocation.class);
                    intSomeOne.putExtra("book_ref", bookingReference);
                    intSomeOne.putExtra("someOneNmame", label_text1.getText().toString());
                    intSomeOne.putExtra("someOneGender", Tv_city.getText().toString());
                    intSomeOne.putExtra("mode", tripTypeSpinner);
                    intSomeOne.putExtra("CategoryID", CategoryID);


                    intSomeOne.putExtra("mobileNumber", mobileNumber.getText().toString().trim());
                    intSomeOne.putExtra("CountryCode", CountryCodeStr.trim());

                    System.out.println("=================Category_Selected_id13==============" + CategoryID);
                    startActivity(intSomeOne);
                    // finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    if (dialog != null) {
                        dialog.dismiss();

                    }
                }
            }
        });

        dialog.show();
    }


    private void PassangerTravelCount_choose() {
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.familymembers_popup);

        TextView Ok_Profilechange = (TextView) dialog.findViewById(R.id.confirm_text);
        Ok_Profilechange.setText(getResources().getString(R.string.ok_lable));
        final ImageView closeimage = (ImageView) dialog.findViewById(R.id.closeimage);

        Spinner spinnerMale = (Spinner) dialog.findViewById(R.id.male_spinner1);
        Spinner spinnerFemale = (Spinner) dialog.findViewById(R.id.female_spinner1);
        Spinner spinnerChildren = (Spinner) dialog.findViewById(R.id.children_spinner1);

        final TextView male_tv, female_tv, child_tv;
        male_tv = (TextView) dialog.findViewById(R.id.tv_malenth);
        female_tv = (TextView) dialog.findViewById(R.id.tv_female);
        child_tv = (TextView) dialog.findViewById(R.id.tv_chid);

        ArrayList<String> Passenger1 = new ArrayList<>();
        for (int i = 0; i <= NoOfSeats; i++) {
            if (i == 0) {
                Passenger1.add("none");
            } else {
                Passenger1.add(String.valueOf(i));
            }
        }

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(BookingPage4.this, Passenger1);
        spinnerMale.setAdapter(customSpinnerAdapter);

        CustomSpinnerAdapter customSpinnerAdapter1 = new CustomSpinnerAdapter(BookingPage4.this, Passenger1);
        spinnerFemale.setAdapter(customSpinnerAdapter1);

        CustomSpinnerAdapter customSpinnerAdapter2 = new CustomSpinnerAdapter(BookingPage4.this, Passenger1);
        spinnerChildren.setAdapter(customSpinnerAdapter2);

        // Spinner Drop down elements

        spinnerMale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                cd = new ConnectionDetector(BookingPage4.this);
                if (item.equalsIgnoreCase("none")) {
                    male_tv.setText(item);
                } else {
                    male_tv.setText(item + getResources().getString(R.string.person));
                }

                isInternetPresent = cd.isConnectingToInternet();
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                male_tv.setText("01");
            }
        });

        spinnerFemale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                cd = new ConnectionDetector(BookingPage4.this);
                if (item.equalsIgnoreCase("none")) {
                    female_tv.setText(item);
                } else {
                    female_tv.setText(item + getResources().getString(R.string.person));
                }
                isInternetPresent = cd.isConnectingToInternet();
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                female_tv.setText(getResources().getString(R.string.none));
            }
        });
        spinnerChildren.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                cd = new ConnectionDetector(BookingPage4.this);
                if (item.equalsIgnoreCase("none")) {
                    child_tv.setText(item);
                } else {
                    child_tv.setText(item + getResources().getString(R.string.person));
                }
                isInternetPresent = cd.isConnectingToInternet();
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                child_tv.setText(getResources().getString(R.string.none));
            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int male = 0, female = 0, cild = 0, totalPassengerCont = 0, totalSheat = 0;
                if (!male_tv.getText().toString().trim().equalsIgnoreCase("None")) {
                    String male_text = "0" + male_tv.getText().toString().trim().replace("Person", "").trim();
                    if (male_text.equalsIgnoreCase("0none")) {
                        male_text = "0" + male_tv.getText().toString().trim().replace("0none", "").trim();
                    }
                    male = Integer.parseInt(male_text);
                } else {
                    male = 0;
                }
                if (!female_tv.getText().toString().trim().equalsIgnoreCase("None")) {
                    String female_text = "0" + female_tv.getText().toString().trim().replace("Person", "").trim();
                    if (female_text.equalsIgnoreCase("0none")) {
                        female_text = "0" + female_tv.getText().toString().trim().replace("0none", "").trim();
                    }
                    female = Integer.parseInt(female_text);
                } else {
                    female = 0;
                }
                if (!child_tv.getText().toString().trim().equalsIgnoreCase("None")) {
                    String cild_text = "0" + child_tv.getText().toString().trim().replace("Person", "").trim();
                    if (cild_text.equalsIgnoreCase("0none")) {
                        cild_text = "0" + child_tv.getText().toString().trim().replace("0none", "").trim();
                    }
                    cild = Integer.parseInt(cild_text);
                } else {
                    cild = 0;
                }
                totalPassengerCont = male + female + cild;
                totalSheat = Integer.parseInt(NoOfPerson);

                if (male_tv.getText().toString().trim().equalsIgnoreCase("None") && female_tv.getText().toString().trim().equalsIgnoreCase("None") && child_tv.getText().toString().trim().equalsIgnoreCase("None")) {
                    AlertError(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.profile_label_alert_validperson));
                } else if (totalPassengerCont > NoOfSeats) {
                    AlertError(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.profile_label_alert_validperson1));
                } else {
                    if (isInternetPresent) {
                        if (male_tv.getText().toString().trim().equalsIgnoreCase("none")) {
                            noOfMale = "0";
                        } else {
                            noOfMale = male_tv.getText().toString().trim().replace("Person", "");
                        }
                        if (female_tv.getText().toString().trim().equalsIgnoreCase("none")) {
                            noOfFemale = "0";
                        } else {
                            noOfFemale = "0" + female_tv.getText().toString().trim().replace("Person", "");
                        }
                        if (child_tv.getText().toString().trim().equalsIgnoreCase("none")) {
                            noOfChildren = "0";
                        } else {
                            noOfChildren = "0" + child_tv.getText().toString().trim().replace("Person", "");
                        }

                        if (totalPassengerCont == 1 || totalPassengerCont == 0) {
//                            person_count_tv.setText(totalPassengerCont + " Person");
                        } else {
//                            person_count_tv.setText(totalPassengerCont + " Persons");
                        }

//                        ConfirmBookling();
                    } else {
                        Alert(getkey("action_error"), getResources().getString(R.string.alert_nointernet));
                    }
                    dialog.dismiss();
                }
            }
        });
        closeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void PassangerTravelCount() {
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.familymembers_popup);

        TextView Ok_Profilechange = (TextView) dialog.findViewById(R.id.confirm_text);
        final ImageView closeimage = (ImageView) dialog.findViewById(R.id.closeimage);

        Spinner spinnerMale = (Spinner) dialog.findViewById(R.id.male_spinner1);
        Spinner spinnerFemale = (Spinner) dialog.findViewById(R.id.female_spinner1);
        Spinner spinnerChildren = (Spinner) dialog.findViewById(R.id.children_spinner1);

        final TextView male_tv, female_tv, child_tv;
        male_tv = (TextView) dialog.findViewById(R.id.tv_malenth);
        female_tv = (TextView) dialog.findViewById(R.id.tv_female);
        child_tv = (TextView) dialog.findViewById(R.id.tv_chid);

        ArrayList<String> Passenger1 = new ArrayList<>();
        for (int i = 0; i <= NoOfSeats; i++) {
            if (i == 0) {
                Passenger1.add("none");
            } else {
                Passenger1.add(String.valueOf(i));
            }
        }

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(BookingPage4.this, Passenger1);
        spinnerMale.setAdapter(customSpinnerAdapter);

        CustomSpinnerAdapter customSpinnerAdapter1 = new CustomSpinnerAdapter(BookingPage4.this, Passenger1);
        spinnerFemale.setAdapter(customSpinnerAdapter1);

        CustomSpinnerAdapter customSpinnerAdapter2 = new CustomSpinnerAdapter(BookingPage4.this, Passenger1);
        spinnerChildren.setAdapter(customSpinnerAdapter2);

        // Spinner Drop down elements

        spinnerMale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                cd = new ConnectionDetector(BookingPage4.this);
                if (item.equalsIgnoreCase("none")) {
                    male_tv.setText(item);
                } else {
                    male_tv.setText(item + getResources().getString(R.string.person));
                }

                isInternetPresent = cd.isConnectingToInternet();
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                male_tv.setText("01");
            }
        });

        spinnerFemale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                cd = new ConnectionDetector(BookingPage4.this);
                if (item.equalsIgnoreCase("none")) {
                    female_tv.setText(item);
                } else {
                    female_tv.setText(item + getResources().getString(R.string.person));
                }
                isInternetPresent = cd.isConnectingToInternet();
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                female_tv.setText(getResources().getString(R.string.none));
            }
        });
        spinnerChildren.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("spinner-----------------------------");
                String item = parent.getItemAtPosition(position).toString();
                cd = new ConnectionDetector(BookingPage4.this);
                if (item.equalsIgnoreCase("none")) {
                    child_tv.setText(item);
                } else {
                    child_tv.setText(item + getResources().getString(R.string.person));
                }
                isInternetPresent = cd.isConnectingToInternet();
                System.out.println("-------------selected gender-----------------" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                child_tv.setText(getResources().getString(R.string.none));
            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int male = 0, female = 0, cild = 0, totalPassengerCont = 0, totalSheat = 0;
                if (!male_tv.getText().toString().trim().equalsIgnoreCase("None")) {
                    String male_text = "0" + male_tv.getText().toString().trim().replace("Person", "").trim();
                    if (male_text.equalsIgnoreCase("0none")) {
                        male_text = "0" + male_tv.getText().toString().trim().replace("0none", "").trim();
                    }
                    male = Integer.parseInt(male_text);
                } else {
                    male = 0;
                }
                if (!female_tv.getText().toString().trim().equalsIgnoreCase("None")) {
                    String female_text = "0" + female_tv.getText().toString().trim().replace("Person", "").trim();
                    if (female_text.equalsIgnoreCase("0none")) {
                        female_text = "0" + female_tv.getText().toString().trim().replace("0none", "").trim();
                    }
                    female = Integer.parseInt(female_text);
                } else {
                    female = 0;
                }
                if (!child_tv.getText().toString().trim().equalsIgnoreCase("None")) {
                    String cild_text = "0" + child_tv.getText().toString().trim().replace("Person", "").trim();
                    if (cild_text.equalsIgnoreCase("0none")) {
                        cild_text = "0" + child_tv.getText().toString().trim().replace("0none", "").trim();
                    }
                    cild = Integer.parseInt(cild_text);
                } else {
                    cild = 0;
                }
                totalPassengerCont = male + female + cild;
                totalSheat = Integer.parseInt(NoOfPerson);

                if (male_tv.getText().toString().trim().equalsIgnoreCase("None") && female_tv.getText().toString().trim().equalsIgnoreCase("None") && child_tv.getText().toString().trim().equalsIgnoreCase("None")) {
                    AlertError(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.profile_label_alert_validperson));
                } else if (totalPassengerCont > NoOfSeats) {
                    AlertError(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.profile_label_alert_validperson1));
                } else {
                    if (isInternetPresent) {
                        if (male_tv.getText().toString().trim().equalsIgnoreCase("none")) {
                            noOfMale = "0";
                        } else {
                            noOfMale = male_tv.getText().toString().trim().replace("Person", "");
                        }
                        if (female_tv.getText().toString().trim().equalsIgnoreCase("none")) {
                            noOfFemale = "0";
                        } else {
                            noOfFemale = "0" + female_tv.getText().toString().trim().replace("Person", "");
                        }
                        if (child_tv.getText().toString().trim().equalsIgnoreCase("none")) {
                            noOfChildren = "0";
                        } else {
                            noOfChildren = "0" + child_tv.getText().toString().trim().replace("Person", "");
                        }

                        ConfirmBookling();
                    } else {
                        Alert(getkey("action_error"), getResources().getString(R.string.alert_nointernet));
                    }
                    dialog.dismiss();
                }
            }
        });
        closeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        // dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void ConfirmBookling1() {
        cd = new ConnectionDetector(BookingPage4.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            HashMap<String, String> code = session.getCouponCode();
            String coupon = code.get(SessionManager.KEY_COUPON_CODE);

            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", UserID);
            jsonParams.put("pickup", SpickupLocation);
            jsonParams.put("pickup_lat", SpickupLatitude);
            jsonParams.put("pickup_lon", SpickupLongitude);
            jsonParams.put("drop_loc", SdestinationLocation.toString());
            jsonParams.put("drop_lat", SdestinationLatitude);
            jsonParams.put("drop_lon", SdestinationLongitude);


            jsonParams.put("category", CategoryID);
            System.out.println("=================Category_Selected_id12==============" + CategoryID);
            jsonParams.put("type", selectedType);
            jsonParams.put("pickup_date", selectedDate);
            jsonParams.put("pickup_time", selectedTime);

            jsonParams.put("source", ScouponSource);//coupon/redeem
            jsonParams.put("code", ScouponCode);
            jsonParams.put("tracking", Stracking);//tracking:1    => 0/1
            jsonParams.put("mode", STrip_Type);//: =>oneway/return/multistop

            jsonParams.put("no_of_passenger", NoOfPerson);

            jsonParams.put("referal_code", Str_referralCode);

            jsonParams.put("est_cost", SetaAmount);
            jsonParams.put("approx_dist", SetaDistance);
            jsonParams.put("payment_mode", SpaymentMode);
            jsonParams.put("ride_id", riderId);
            jsonParams.put("amount", SetaAmount);

            jsonParams.put("stripe_card_id", selectedStripeCard);

            jsonParams.put("male_passenger", noOfMale.replace("Person", ""));
            jsonParams.put("female_passenger", noOfFemale);
            jsonParams.put("no_of_children", noOfChildren);
            System.out.println("======WaitingTime5=========" + WaitingTime);
            if (!WaitingTime.equalsIgnoreCase("")) {
                if (WaitingTime.contains("-")) {
                    WaitingTime = WaitingTime.replace("-", "").trim();
                }
                jsonParams.put("wait_time", WaitingTime);
            }
            if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(STrip_Type)) {

                for (int i = 0; i < multiple_dropList.size(); i++) {
                    jsonParams.put("multipath_loc[" + i + "][drop_loc]", multiple_dropList.get(i).getPlaceName());
                    jsonParams.put("multipath_loc[" + i + "][drop_lon]", multiple_dropList.get(i).getPlaceLong());
                    jsonParams.put("multipath_loc[" + i + "][drop_lat]", multiple_dropList.get(i).getPlaceLat());
                    if (multiple_dropList.get(i).getwaitingTime().contains("-")) {
                        WaitingTime = multiple_dropList.get(i).getwaitingTime().replace("-", "").trim();
                    }

                    jsonParams.put("multipath_loc[" + i + "][wait_time]", WaitingTime);
                }
            }
            if (bookReference.equalsIgnoreCase("Yes")) {
                jsonParams.put("book_ref", bookReference);
                jsonParams.put("user_name", someOneNmame);
                jsonParams.put("gender", someOneGender);

                jsonParams.put("mobile_number", mobileNumberstr);
                jsonParams.put("country_code", CountryCodeStr);

                System.out.println("==============Muruga someOneNmame1============" + CountryCodeStr + " " + mobileNumberstr + " " + someOneNmame);
                System.out.println("==============Muruga someOneGender1============" + someOneGender);
            }
            System.out.println(".................+ " + jsonParams.toString());
            ConfirmRideRequest(Iconstant.confirm_ride_url, jsonParams, "2");

//                          ConfirmRideRequest(Iconstant.confirm_ride_url, coupon, coupon_selectedDate, coupon_selectedTime, selectedType, CategoryID, map_address.getText().toString(), SpickupLatitude, SpickupLongitude, "2", destination_address.getText().toString(), SdestinationLatitude, SdestinationLongitude);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }

    }

    private void ConfirmBookling() {
        try {


            //isRunnable = false;
            stopTimer();
            cd = new ConnectionDetector(BookingPage4.this);
            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);
                jsonParams.put("pickup", SpickupLocation);
                jsonParams.put("pickup_lat", SpickupLatitude);
                jsonParams.put("pickup_lon", SpickupLongitude);
                jsonParams.put("drop_loc", SdestinationLocation.toString());
                jsonParams.put("drop_lat", SdestinationLatitude);
                jsonParams.put("drop_lon", SdestinationLongitude);
                jsonParams.put("category", CategoryID);
                System.out.println("=================Category_Selected_id==============" + CategoryID);
                jsonParams.put("type", selectedType);
                jsonParams.put("pickup_date", selectedDate);
                jsonParams.put("pickup_time", selectedTime);
                jsonParams.put("source", ScouponSource);//coupon/redeem
                jsonParams.put("code", ScouponCode);

                jsonParams.put("tracking", Stracking);   //tracking:1    => 0/1
                jsonParams.put("mode", STrip_Type);      //: =>oneway/return/multistop

                jsonParams.put("no_of_passenger", NoOfPerson);
                jsonParams.put("referal_code", Str_referralCode);
                jsonParams.put("est_cost", SetaAmount);
                jsonParams.put("approx_dist", SetaDistance);
                jsonParams.put("amount", SetaAmount);/*fareAmount1*/


                jsonParams.put("payment_mode", SpaymentMode);
                jsonParams.put("stripe_card_id", selectedStripeCard);


                jsonParams.put("male_passenger", NoOfPerson);
                jsonParams.put("female_passenger", noOfFemale);
                jsonParams.put("no_of_children", noOfChildren);
                System.out.println("======WaitingTime=========" + WaitingTime);

                if (!WaitingTime.equalsIgnoreCase("")) {
                    if (WaitingTime.contains("-")) {
                        WaitingTime = WaitingTime.replace("-", "").trim();
                    }

                    jsonParams.put("wait_time", WaitingTime);
                }
                if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(STrip_Type)) {
                    for (int i = 0; i < multiple_dropList.size(); i++) {
                        jsonParams.put("multipath_loc[" + i + "][drop_loc]", multiple_dropList.get(i).getPlaceName());
                        jsonParams.put("multipath_loc[" + i + "][drop_lon]", multiple_dropList.get(i).getPlaceLong());
                        jsonParams.put("multipath_loc[" + i + "][drop_lat]", multiple_dropList.get(i).getPlaceLat());

                        if (multiple_dropList.get(i).getwaitingTime().contains("-")) {
                            WaitingTime = multiple_dropList.get(i).getwaitingTime().replace("-", "").trim();
                        }

                        jsonParams.put("multipath_loc[" + i + "][wait_time]", multiple_dropList.get(i).getwaitingTime());
                    }
                }
                if (bookReference.equalsIgnoreCase("Yes")) {
                    jsonParams.put("book_ref", bookReference);
                    jsonParams.put("user_name", someOneNmame);
                    jsonParams.put("gender", someOneGender);
                    jsonParams.put("mobile_number", mobileNumberstr);
                    jsonParams.put("country_code", CountryCodeStr);

                    System.out.println("==============Muruga someOneNmame1============" + CountryCodeStr + " " + mobileNumberstr + " " + someOneNmame);
                    System.out.println("==============Muruga someOneGender1============" + someOneGender);
                }
                if (!paymenttype.equals("")) {
                    ConfirmRideRequest(Iconstant.confirm_ride_url, jsonParams, "");
                } else {
                    final PkDialog mDialog = new PkDialog(BookingPage4.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.alert_label_title));
                    mDialog.setDialogMessage(getResources().getString(R.string.walletmoney_lable_payment_type_alert));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });
                    mDialog.show();
                }
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
            }
        } catch (Exception e) {

        }
    }

    private void WIthDrawlResponsePopUP1(String title, String message) {
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.someone_alert);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView closeImage = (ImageView) dialog.findViewById(R.id.img_close1);
        CustomTextView withdrawResponse = (CustomTextView) dialog.findViewById(R.id.min_withdrawal_tv1);
        CustomTextView InRTv = (CustomTextView) dialog.findViewById(R.id.withdraw_amt1);

        withdrawResponse.setText(message);

        InRTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // Et_enteramount.setHint(getResources().getString(R.string.walletmoney_lable_rechargemoney_edittext_hint) + " " + ScurrencySymbol + Str_minimum_amt + " " + "-" + " " + ScurrencySymbol + Str_maximum_amt);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }

        });

        dialog.show();
    }

    //-------------------Get Latitude and Longitude from Address(Place ID) Request----------------
    private void LatLongRequest(String Url, final String flag) {

        dialog = new Dialog(BookingPage4.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        System.out.println("--------------LatLong url-------------------" + Url);

        mRequest = new ServiceRequest(BookingPage4.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------LatLong  reponse-------------------" + response);
                String status = "", SLatitude1 = "", SLongitude1 = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONObject place_object = object.getJSONObject("result");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_object.length() > 0) {
                                JSONObject geometry_object = place_object.getJSONObject("geometry");
                                if (geometry_object.length() > 0) {
                                    JSONObject location_object = geometry_object.getJSONObject("location");
                                    if (location_object.length() > 0) {
                                        SLatitude1 = location_object.getString("lat");
                                        SLongitude1 = location_object.getString("lng");
                                        isdataAvailable = true;
                                    } else {
                                        isdataAvailable = false;
                                    }
                                } else {
                                    isdataAvailable = false;
                                }
                            } else {
                                isdataAvailable = false;
                            }
                        } else {
                            isdataAvailable = false;
                        }
                    }

                    if (isdataAvailable) {


                        System.out.println("-----------SdestinationLatitude book3----------------" + SLatitude);
                        System.out.println("-----------SdestinationLongitude book3----------------" + SLongitude);
                        System.out.println("-----------SdestinationLocation book3----------------" + Slocation);

                        Slocation = Sselected_location;
                        SLatitude = SLatitude1;
                        SLongitude = SLongitude1;
                        if (Slocation.equalsIgnoreCase("")) {
                            Rl_PickupNow.setEnabled(true);
                            if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
                                SdestinationLatitude = SLatitude1 + "";
                                SdestinationLongitude = SLongitude1 + "";
                                SdestinationLocation = Slocation;
                            }
                        } else {
                            if ("MultiDropLocation".equalsIgnoreCase(Str_Page)) {
                                SmultiDropLocation = Slocation;
                                SmultiDropLatitude = SLatitude1 + "";
                                SmultiDropLongitude = SLongitude1 + "";

                            } else if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
                                SdestinationLatitude = SLatitude1 + "";
                                SdestinationLongitude = SLongitude1 + "";
                                SdestinationLocation = Slocation;
                            } else if ("SomeOnePickup".equalsIgnoreCase(Str_Page)) {
                                SdestinationLatitude = SLatitude1 + "";
                                SdestinationLongitude = SLongitude1 + "";
                                SdestinationLocation = Slocation;
                            }
                        }

                        System.out.println("=========set value1=================");
                        book_my_ride_set_destination_edittextview.setText(Slocation);
                        book_my_ride_set_destination_edittextview.setVisibility(View.VISIBLE);
                        listview.setVisibility(GONE);

// Move the camera to last position with a zoom level
                        if (flag.equalsIgnoreCase("1")) {
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(SLatitude), Double.parseDouble(SLongitude))).zoom(17).build();
                            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            listview.setVisibility(GONE);
                        } else if (flag.equalsIgnoreCase("2")) {
                            placeId = "";
                        }


                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Alert(/*getResources().getString(R.string.ale)*/"sorry", status);
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-------------------Search Place Request----------------
    private void CitySearchRequest(String Url) {

        if (Rl_destination_layout.getVisibility() == VISIBLE) {
            listview.setVisibility(VISIBLE);
        }
        loading_spinner.setVisibility(View.VISIBLE);
        System.out.println("--------------Search city url-------------------" + Url);

        mRequest = new ServiceRequest(BookingPage4.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Search city  reponse-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");
                        JSONArray place_array = object.getJSONArray("predictions");
                        if (status.equalsIgnoreCase("OK")) {
                            is_stopover_value = true;
                            if (place_array.length() > 0) {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                for (int i = 0; i < place_array.length(); i++) {
                                    JSONObject place_object = place_array.getJSONObject(i);
                                    itemList_location.add(place_object.getString("description"));
                                    itemList_placeId.add(place_object.getString("place_id"));
                                }
                                isdataAvailable = true;
                            } else {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                isdataAvailable = false;
                            }
                        } else {
                            itemList_location.clear();
                            itemList_placeId.clear();
                            isdataAvailable = false;
                            is_stopover_value = false;
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                loading_spinner.setVisibility(View.INVISIBLE);
                //alert_layout.setVisibility(View.GONE);
               /* if (isdataAvailable) {
                    tv_emptyText.setVisibility(View.GONE);
                } else {
                    tv_emptyText.setVisibility(View.VISIBLE);
                }*/
                PlaceSearchAdapter = new PlaceSearchAdapter(BookingPage4.this, itemList_location);
                listview.setAdapter(PlaceSearchAdapter);
                PlaceSearchAdapter.notifyDataSetChanged();

            }

            @Override
            public void onErrorListener() {
                loading_spinner.setVisibility(View.INVISIBLE);
                //alert_layout.setVisibility(View.GONE);

                // close keyboard
                // CloseKeyboard(book_my_ride_set_destination_textview);
            }
        });

    }

    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<String> asr;

        public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
            this.asr = asr;
            activity = context;
        }


        public int getCount() {
            return asr.size();
        }

        public Object getItem(int i) {
            return asr.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }


        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(BookingPage4.this);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(16);
            txt.setGravity(Gravity.CENTER);
            txt.setText(asr.get(position));
            txt.setBackground(getResources().getDrawable(R.drawable.spinner_drop_down_background));
            txt.setTextColor(Color.parseColor("#ffffff"));
            return txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(BookingPage4.this);
            txt.setGravity(Gravity.CENTER);
            txt.setPadding(10, 0, 0, 0);
            txt.setTextSize(14);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt.setText(asr.get(i));
            txt.setTextColor(Color.parseColor("#ffffff"));
            txt.setVisibility(View.INVISIBLE);
            return txt;
        }

    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void gettingpostion(String posstion)
    {

           int position= Integer.parseInt(posstion);

        for (int i = 0; i < paymnet_list.size(); i++) {
            HomePojo pojo = new HomePojo();
            pojo.setCat_image("0");
            pojo.setPayment_code(paymnet_list.get(i).getPayment_code());
            pojo.setPayment_name(paymnet_list.get(i).getPayment_name());
            pojo.setPayment_icon(paymnet_list.get(i).getPayment_icon());
            pojo.setSetpaymentinactiveicon(paymnet_list.get(i).getSetpaymentinactiveicon());
            paymnet_list.set(i,pojo);
        }

        HomePojo pojo = new HomePojo();
        pojo.setCat_image("1");
        pojo.setPayment_code(paymnet_list.get(position).getPayment_code());
        pojo.setPayment_name(paymnet_list.get(position).getPayment_name());
        pojo.setPayment_icon(paymnet_list.get(position).getPayment_icon());
        pojo.setSetpaymentinactiveicon(paymnet_list.get(position).getSetpaymentinactiveicon());
        paymnet_list.set(position,pojo);


        if (paymnet_list.size() > 0) {
            for (int i = 0; i < paymnet_list.size(); i++) {
                if (i == position) {
                    paymnet_list.get(i).setSetPayment_selected_payment_id("true");
                } else {
                    paymnet_list.get(i).setSetPayment_selected_payment_id("false");
                }
            }
            paymentListAdapter = new BookingPage4Adapter(BookingPage4.this, paymnet_list);
            Hlv_paymentList.setAdapter(paymentListAdapter);
            paymentListAdapter.notifyDataSetChanged();
            SpaymentMode = paymnet_list.get(position).getPayment_code();
            paymenttype = paymnet_list.get(position).getPayment_name();
            // selectedStripeCard=SpaymentMode;
//                    paymentname.setText(paymnet_list.get(position).getPayment_name());
            System.out.println("============muruga paymenttype==============" + paymenttype + " " + SpaymentMode);
            isCameraChange = false;
            if (!SpaymentMode.equalsIgnoreCase("Cash")) {
                if (isInternetPresent) {
                    PaymentMethodList(Iconstant.User_Booking_PaymentChoose_Url);
                } else {
                    Alert(getResources().getString(R.string.alert), getResources().getString(R.string.alert_nointernet));
                }
            }
        }
    }


    @Override
    public void onDestroy() {
        stopTimer();
        super.onDestroy();

        if (dialog != null) {
            dialog.dismiss();
        }
        if (mRequest != null) {
            mRequest.cancelRequest();
        }
        unregisterReceiver(refreshReceiver);
        if(EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().unregister(this);
        }
    }

    //Method to smooth turn marker
    static public void rotateMarker(final Marker marker, final float toRotation, GoogleMap map) {


        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    public void movingCarNewMarkers(final LatLng startPosition, int carNumber) {
        try {
            if (mLatLngInterpolator1 == null) {
                mLatLngInterpolator1 = new LatLngInterpolator.Linear();
            }
            if (newMovingCarMarkers[carNumber] != null) {
                System.out.println("=========phantom car1=========  !=null");
                rotateMarker(newMovingCarMarkers[carNumber], bearing, googleMap);
                MarkerAnimation.animateMarkerToGB1(newMovingCarMarkers[carNumber], startPosition, mLatLngInterpolator1);
            } else {
                System.out.println("=========phantom car1=========null");
                Bitmap smallMarker = Bitmap.createScaledBitmap(bmp, 140, 140, false);
                bmp = smallMarker;
                newMovingCarMarkers[carNumber] = googleMap.addMarker(new MarkerOptions()
                        .position(startPosition)
                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                        .anchor(0.5f, 0.5f)
                        .rotation(bearing)
                        .flat(true));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void movingCarNewMarkers1(final LatLng startPosition, final int carNumber) {
        try {
            mLatLngInterpolator1 = new LatLngInterpolator.Linear();


            System.out.println("=========phantom car1=========null");
            Picasso.with(BookingPage4.this)
                    .load(sCarImg)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            String s = BitMapToString(bitmap);
                            System.out.println("session bitmap" + s);
                            session.setVehicle_BitmapImage(s);
                            bmp = bitmap;
                            Bitmap smallMarker = Bitmap.createScaledBitmap(bmp, 140, 140, false);
                            bmp = smallMarker;
                            newMovingCarMarkers[carNumber] = googleMap.addMarker(new MarkerOptions()
                                    .position(startPosition)
                                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                                    .anchor(0.5f, 0.5f)
                                    .rotation(bearing)
                                    .flat(true));
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class LoadBitmap extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... aurl) {
            String car64 = "";

            try {
                URL url = new URL(aurl[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                image = BitmapFactory.decodeStream(input);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";

        }

        @Override
        protected void onPostExecute(String unused) {

            String s = BitMapToString(image);
            System.out.println("session bitmap" + s);
            session.setVehicle_BitmapImage(s);
            bmp = image;
            double Dlatitude = gps.getLatitude();
            double Dlongitude = gps.getLongitude();

            Bitmap smallMarker = Bitmap.createScaledBitmap(bmp, 140, 140, false);
            bmp = smallMarker;
            MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
            marker.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));

            if (driver_status) {
                googleMap.addMarker(marker);
            } else {
//                googleMap.clear();
            }
            if (bookingSomeElseflag.equalsIgnoreCase("2")) {
                currentMarker = 2;
                setInfoWidow(STrip_Type, selectedCarType, 2);
            }
        }

    }

    private void updatePhantomCars1(Map<String, List<Double>> phantomMap) {

        if (driver_list.size() > 0)
            for (int i = 0; i < driver_list.size(); i++) {
                System.out.println("============phantm 1==========");
                Double lat = phantomMap.get("phantomLat").get(i);
                Double lan = phantomMap.get("phantomLan").get(i);
                final LatLng latLng = new LatLng(lat, lan);

                toNew.setLatitude(lat);
                toNew.setLongitude(lan);

                ToLocation.add(toNew);
                Location from = movingLocation.get("FromLocation").get(i);
                Location to = movingLocation.get("ToLocation").get(i);

                bearing = from.bearingTo(to);

                fromNew.setLatitude(lat);
                fromNew.setLongitude(lan);

                fromLocation.add(fromNew);
                movingLocation.put("FromLocation", fromLocation);
                movingLocation.put("ToLocation", ToLocation);

                movingCarNewMarkers(latLng, i);
            }

    }


    //-------------------------------code for map marker moving-------------------------------
    GoogleMap.OnCameraChangeListener mOnCameraChangeListener = new GoogleMap.OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition cameraPosition) {

            if (isCameraChange) {
                double latitude = cameraPosition.target.latitude;
                double longitude = cameraPosition.target.longitude;

                cd = new ConnectionDetector(BookingPage4.this);
                isInternetPresent = cd.isConnectingToInternet();

                Log.e("camerachange lat-->", "" + latitude);
                Log.e("on_camera_change lon-->", "" + longitude);
                System.out.println("================ON CAMERA CHANGE CALL===============");
                if (googleMap != null) {
                    googleMap.clear();
                    if (isInternetPresent) {

                        sLatitude = String.valueOf(latitude);
                        sLongitude = String.valueOf(longitude);

                        System.out.println("========= Map_movingTask2=================");
                        if (isFromFindPlace == true) {
                            if (Double.parseDouble(SdestinationLatitude) != (latitude) && Double.parseDouble(SdestinationLongitude) != (longitude)) {
                                if (!"BookingPage1".equalsIgnoreCase(Str_Page)) {
                                    if (!placeId.equalsIgnoreCase("")) {
                                        LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + placeId, "2");
                                    } else {
                                        getDestinationAddress(Double.parseDouble(SdestinationLatitude), Double.parseDouble(SdestinationLongitude));
                                    }
                                }
                                addIcon("", new LatLng(latitude, longitude), 2);
                            }
                        } else {
                            setInfoWidow(STrip_Type, selectedCarType, 2);
                            if (!"BookingPage1".equalsIgnoreCase(Str_Page)) {
                                if (!placeId.equalsIgnoreCase("")) {
                                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + placeId, "2");
                                } else {
                                    getDestinationAddress(latitude, longitude);
                                }
                            }
                        }
                    } else {
                        Alert(getkey("action_error"), getResources().getString(R.string.alert_nointernet));
                    }
                }

                if (lyt_book_now.getVisibility() == View.GONE && !Str_Page.equalsIgnoreCase("SomeOnePickup")) {

                    centerMArker.setVisibility(VISIBLE);
                    Iv_CenterMarker.setVisibility(VISIBLE);
                    Iv_CenterMarker.setImageResource(R.drawable.pickuptoofar);
//                    if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
//                        centerMArker.setVisibility(VISIBLE);
//                        Iv_CenterMarker.setVisibility(VISIBLE);
//                        Iv_CenterMarker.setImageResource(R.drawable.pickuptoofar);
//                    } else {
//                        centerMArker.setVisibility(GONE);
//                        Iv_CenterMarker.setVisibility(GONE);
//                    }
                    double latitudes = cameraPosition.target.latitude;
                    double longitudes = cameraPosition.target.longitude;

                    cd = new ConnectionDetector(BookingPage4.this);
                    isInternetPresent = cd.isConnectingToInternet();

                    sLatitude = String.valueOf(latitudes);
                    sLongitude = String.valueOf(longitudes);
                    if (!isfirstlocationupdated) {
                        firstlattitudewhencomes = latitudes;
                        firstlongitudewhencomes = longitudes;
                        isfirstlocationupdated = true;
                    }
                    if (CV_TripType.getVisibility() == View.GONE && Rl_destination_layout.getVisibility() == View.GONE) {
                        showsomeonelsedialog_whenakilometeraway(firstlattitudewhencomes, firstlongitudewhencomes, latitudes, longitudes);
                    }
                } else if (lyt_book_now.getVisibility() == View.GONE) {
                    centerMArker.setVisibility(VISIBLE);
                    Iv_CenterMarker.setVisibility(VISIBLE);
                }

            } else {
                if (/*Rl_Bottom_layout.getVisibility() == VISIBLE && */lyt_book_now.getVisibility() == View.GONE && !Str_Page.equalsIgnoreCase("SomeOnePickup")) {

                    centerMArker.setVisibility(VISIBLE);
                    Iv_CenterMarker.setVisibility(VISIBLE);
                    Iv_CenterMarker.setImageResource(R.drawable.pickuptoofar);
//                    if ("BookingPage1".equalsIgnoreCase(Str_Page)) {
//                        centerMArker.setVisibility(VISIBLE);
//                        Iv_CenterMarker.setVisibility(VISIBLE);
//                        Iv_CenterMarker.setImageResource(R.drawable.pickuptoofar);
//                    } else {
//                        centerMArker.setVisibility(GONE);
//                        Iv_CenterMarker.setVisibility(GONE);
//                    }
                    double latitude = cameraPosition.target.latitude;
                    double longitude = cameraPosition.target.longitude;

                    cd = new ConnectionDetector(BookingPage4.this);
                    isInternetPresent = cd.isConnectingToInternet();

                    sLatitude = String.valueOf(latitude);
                    sLongitude = String.valueOf(longitude);
                    if (!isfirstlocationupdated) {
                        firstlattitudewhencomes = latitude;
                        firstlongitudewhencomes = longitude;
                        isfirstlocationupdated = true;
                    }

                    if (CV_TripType.getVisibility() == View.GONE && Rl_destination_layout.getVisibility() == View.GONE) {
                        showsomeonelsedialog_whenakilometeraway(firstlattitudewhencomes, firstlongitudewhencomes, latitude, longitude);
                    }
                } else if (lyt_book_now.getVisibility() == View.GONE) {
                    centerMArker.setVisibility(VISIBLE);
                    Iv_CenterMarker.setVisibility(VISIBLE);
                }
            }


        }
    };

    private void showsomeonelsedialog_whenakilometeraway(double firstlattitudewhencomes, double firstlongitudewhencomes, double sLatitude, double sLongitude) {
        Location locationA = new Location("point A");
        locationA.setLatitude(firstlattitudewhencomes);

        locationA.setLongitude(firstlongitudewhencomes);
        Location locationB = new Location("point B");
        locationB.setLatitude(sLatitude);
        locationB.setLongitude(sLongitude);

        double distance = locationA.distanceTo(locationB);
        double roundmeter = Math.round(distance);
        System.out.println("meter" + roundmeter);
        if (roundmeter > 1000) {
            pickuptoofar(getString(R.string.pickuptoofar), getString(R.string.booksomeelse));
        }

    }


    private LatLng phantomLatLan(Double lat, Double lan, Double angle) throws Exception {

        Random random = new Random();
        int randumNum = random.nextInt(3 - 1 + 1) + 1;
        try {
            anglePhantm = angle * (Math.PI / 180);
            latPhantom1 = lat * (Math.PI / 180);
            lanPhantom1 = lan * (Math.PI / 180);

            latPhantom = Math.asin(((Math.sin(latPhantom1)) * Math.cos(randumNum / phatomRadius)) + (Math.cos(latPhantom1) * Math.sin(randumNum / phatomRadius) * Math.cos(anglePhantm)));
            lanPhantom = lanPhantom1 + Math.atan2(Math.sin(anglePhantm) * Math.sin(randumNum / phatomRadius) * Math.cos(latPhantom1), Math.cos(randumNum / phatomRadius) - Math.sin(latPhantom1) * Math.sin(latPhantom1));

            latPhantom = latPhantom * (180 / Math.PI);
            lanPhantom = lanPhantom * (180 / Math.PI);

            lat = (double) Math.round(latPhantom * 1000000d) / 1000000d;
            lan = (double) Math.round(lanPhantom * 1000000d) / 1000000d;
            latlan = new LatLng(lat, lan);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return latlan;
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("==================Runnable======Handler is working======== Paused");
        stopTimer();
        if(EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().unregister(this);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("==================Runnable======Handler is working======== Resumed");
        if (!session.getPhantomcar().equalsIgnoreCase("no")) {
            startTimer();
        }
        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this);
        }
    }


    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5 || target.length() > 12) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }


    public String fetchCityName(final double lat, final double log) {

        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
            }

            ;

            @Override
            protected String doInBackground(Void... params) {


                if (Geocoder.isPresent()) {
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(lat, log, 1);
                        System.out.println("========List================addresses" + addresses);

                        if (addresses.size() > 0) {
                            Address returnedAddress = addresses.get(0);
                            StringBuilder strReturnedAddress = new StringBuilder("");
                            System.out.println("------------getMaxAddressLineIndex--------------------" + returnedAddress.getMaxAddressLineIndex());
                            if (returnedAddress.getMaxAddressLineIndex() > 0) {

                                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                                }
                                cityName = strReturnedAddress.toString();
                                cityNameNew = cityName;
                            } else {

                                cityName = returnedAddress.getAddressLine(0);
                                cityNameNew = cityName;
                            }

                            //    System.out.println("-------------------returnedAddress-----------------------"+returnedAddress.getAddressLine(0));


                            //
                        }
                    } catch (Exception ignored) {
                        ignored.printStackTrace();
                        // after a while, Geocoder start to trhow "Service not availalbe" exception. really weird since it was working before (same device, same Android version etc..
                    }
                }

                if (cityName != null && !cityName.equalsIgnoreCase("")) // i.e., Geocoder succeed
                {
                    // cityNameNew = cityName;
                    System.out.println("-------------------cityName----------------------" + cityName);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            book_my_ride_set_destination_edittextview.setText(cityName);
                        }
                    });

                    return cityName;
                } else {
                    cityNameNew = cityName;
                    System.out.println("--------fetchCityNameUsingGoogleMap-----------cityName----------------------" + cityName);
                    String cityname = fetchCityNameUsingGoogleMap();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            book_my_ride_set_destination_edittextview.setText(cityName);
                        }
                    });
                    return cityname;
                }
            }

            // Geocoder failed :-(
            // Our B Plan : Google Map
            private String fetchCityNameUsingGoogleMap() {
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + ","
                        + log + "&sensor=false";
                System.out.println("-----------------get address -url----------------------------" + googleMapUrl);
                try {
                    JSONObject googleMapResponse = new JSONObject(ANDROID_HTTP_CLIENT.execute(new HttpGet(googleMapUrl),
                            new BasicResponseHandler()));

                    // many nested loops.. not great -> use expression instead
                    // loop among all results
                    JSONArray results = (JSONArray) googleMapResponse.get("results");
                    for (int i = 0; i < results.length(); i++) {
                        // loop among all addresses within this result
                        JSONObject result = results.getJSONObject(i);
                        if (result.has("address_components")) {
                            JSONArray addressComponents = result.getJSONArray("address_components");
                            // loop among all address component to find a 'locality' or 'sublocality'
                            for (int j = 0; j < addressComponents.length(); j++) {
                                JSONObject addressComponent = addressComponents.getJSONObject(j);
                                if (result.has("types")) {
                                    JSONArray types = addressComponent.getJSONArray("types");

                                    // search for locality and sublocality
                                    String cityName = null;

                                    for (int k = 0; k < types.length(); k++) {
                                        if ("locality".equals(types.getString(k)) && cityName == null) {
                                            if (addressComponent.has("long_name")) {
                                                cityName = addressComponent.getString("long_name");
                                            } else if (addressComponent.has("short_name")) {
                                                cityName = addressComponent.getString("short_name");
                                            }
                                        }
                                        if ("sublocality".equals(types.getString(k))) {
                                            if (addressComponent.has("long_name")) {
                                                cityName = addressComponent.getString("long_name");
                                            } else if (addressComponent.has("short_name")) {
                                                cityName = addressComponent.getString("short_name");
                                            }
                                        }
                                    }
                                    if (cityName != null) {
                                        // City_name1=cityName;
                                        return cityName;
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
                return null;
            }


            @Override
            protected void onPostExecute(String cityName) {
                if (cityName != null) {
                    // Do something with cityName
                    Log.i("GeocoderHelper", cityName);
                }

            }

            ;
        }.execute();
        return cityName;
    }


    private void setInfoWidow(String triptype, String vehicleType, int flag) {

        System.out.println("===========Muruga infowindow values==============" + triptype + vehicleType + UserName);
        try {
            if (googleMap != null) {

                if (isInfoWindowEnable) {
                    if (DefaultMarkerNew != null) {
                        DefaultMarkerNew.remove();
                    }
                    markerDefault = new MarkerOptions().position(new LatLng(Double.parseDouble(SpickupLatitude), Double.parseDouble(SpickupLongitude))).
                            icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_pointer_round));

                    if (flag == 2) {
                        markerDefault.title("");
                        userDetails = UserName + "," + triptype + "," + vehicleType;
                        markerDefault.snippet(userDetails);
//                        if (!"BookingPage1".equalsIgnoreCase(Str_Page)) {
//                        googleMap.setInfoWindowAdapter(new CustomInfoWidowUserAdapter(BookingPage3.this));
                        if (lyt_book_now.getVisibility() == View.GONE && !Str_Page.equalsIgnoreCase("SomeOnePickup")) {
                            inflateuserinfo.setVisibility(VISIBLE);
                            tv_username_marker.setText(UserName);
                            tv_tripType_tv.setText(context.getResources().getString(R.string.trip_lable)+"          : " + triptype);
                            tv_vehicle_type_tv.setText(getResources().getString(R.string.vehicle)+ vehicleType);
                        }
//                        }

                    } else {
                        inflateuserinfo.setVisibility(GONE);
                        markerDefault.anchor(0.5f, 0.3f);
                        if (UserName.length() > 7) {
                            markerDefault.infoWindowAnchor(3f, 1f);
                        } else {
                            markerDefault.infoWindowAnchor(1.5f, 0.5f);
                        }
                        markerDefault.title(UserName);
                        markerDefault.snippet("");
                        googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(BookingPage4.this));
                    }

                    DefaultMarkerNew = googleMap.addMarker(markerDefault);

                    DefaultMarkerNew.showInfoWindow();
                    if (flag != 1) {
                        setpPointer();
                    }
                } else {
                    cd = new ConnectionDetector(getApplicationContext());
                    isInternetPresent = cd.isConnectingToInternet();
                    gps = new GPSTracker(getApplicationContext());
                    if (gps.canGetLocation() && gps.isgpsenabled()) {

                        MyCurrent_lat = gps.getLatitude();
                        MyCurrent_long = gps.getLongitude();

                        if (!SdestinationLocation.equalsIgnoreCase("") && callingFrom.equalsIgnoreCase("findplaceBooking")) {
                            addIcon("", new LatLng(Double.parseDouble(SdestinationLatitude), Double.parseDouble(SdestinationLongitude)), 2);
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(SdestinationLatitude), Double.parseDouble(SdestinationLongitude))).zoom(14).build();
                            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        } else {
                            addIcon("", new LatLng(MyCurrent_lat, MyCurrent_long), 2);

                        }
                        setpPointer();
                    } else {
                        enableGpsService();
                    }
                }

            } else {
                initializeMap();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }


    }

    private void OneWay() {

        IV_selectcategoryImage.setVisibility(View.VISIBLE);
        inflateuserinfo.setVisibility(VISIBLE);
        dragdropTv.setVisibility(View.GONE);
        dragdropTv.setText(getResources().getString(R.string.dragdrop_lable));
        isCameraChange = true;
        centerMArker.setVisibility(View.VISIBLE);
        Tv_PickupNow.setText(getResources().getString(R.string.ok_lable));
        Tv_PickupNow.setEnabled(true);
        book_my_ride_rideNow_layout.setVisibility(View.VISIBLE);
        fav_lyout.setVisibility(View.VISIBLE);
        back_lyout.setVisibility(View.VISIBLE);
        bookforsomeone.setVisibility(View.GONE);
        tv_setdropofflocation.setVisibility(VISIBLE);
        Rl_Back.setVisibility(View.GONE);
        Rl_destination_layout.setVisibility(View.VISIBLE);
        Rl_PickupNow.setEnabled(true);
        book_my_ride_selectcar_triptype_layout.setVisibility(View.GONE);

        params.setMargins(0, 0, 0, 0);
        book_my_ride_bottom_main_layout1.setLayoutParams(params);

        layout_home_iamge.setVisibility(View.GONE);
        layout_home_iamge2.setVisibility(View.VISIBLE);

        STrip_Type = getResources().getString(R.string.one_way_lable);
        Tv_TripType.setText(getResources().getString(R.string.oneway_textview));
        sMultipleDropStatus = "0";

        if (CheckPlayService()) {
            System.out.println("====================Muruga onCameraChangeListener  false====================");
            if (!SdestinationLocation.equalsIgnoreCase("") && callingFrom.equalsIgnoreCase("findplaceBooking")) {
                isFromFindPlace = true;
                EnableOnCameraChange();
                if (stopOverMarkerNew != null) {
                    stopOverMarkerNew.remove();
                }
            } else {
                isFromFindPlace = false;
                googleMap.setOnCameraChangeListener(null);
                setInfoWidow(STrip_Type, selectedCarType, 2);
                if (stopOverMarkerNew != null) {
                    stopOverMarkerNew.remove();
                }
            }
        } else {
            Toast.makeText(BookingPage4.this,  getResources().getString(R.string.install_googleplay_view_location), Toast.LENGTH_LONG).show();
        }

    }

    private void ReturnTrip() {
        IV_selectcategoryImage.setVisibility(View.VISIBLE);
        inflateuserinfo.setVisibility(VISIBLE);
        isRetrunOrMulti = "Return Trip";
        params.setMargins(0, 0, 0, 0);
        book_my_ride_bottom_main_layout1.setLayoutParams(params);
        isCameraChange = true;

        dragdropTv.setText(getResources().getString(R.string.dragdrop_lable));
        dragdropTv.setVisibility(View.GONE);

        centerMArker.setVisibility(View.VISIBLE);
        STrip_Type = getResources().getString(R.string.return_lable);
        Tv_TripType.setText(getResources().getString(R.string.return_textview));
        sMultipleDropStatus = "0";
        layout_home_iamge.setVisibility(View.GONE);
        layout_home_iamge2.setVisibility(View.VISIBLE);

        if (CheckPlayService()) {
            if (!SdestinationLocation.equalsIgnoreCase("") && callingFrom.equalsIgnoreCase("findplaceBooking")) {
                isFromFindPlace = true;
                EnableOnCameraChange();
                if (stopOverMarkerNew != null) {
                    stopOverMarkerNew.remove();
                }
            } else {
                isFromFindPlace = false;
                googleMap.setOnCameraChangeListener(null);
                setInfoWidow(STrip_Type, selectedCarType, 2);
                if (stopOverMarkerNew != null) {
                    stopOverMarkerNew.remove();
                }
            }
        }
        sampleapopaup();

    }

    private void MultiTrip() {
        IV_selectcategoryImage.setVisibility(View.VISIBLE);
        inflateuserinfo.setVisibility(VISIBLE);
        isRetrunOrMulti = "Stop Over";
        params.setMargins(0, 0, 0, 0);
        book_my_ride_bottom_main_layout1.setLayoutParams(params);

        dragdropTv.setText(getResources().getString(R.string.dragdrop_lable));
        dragdropTv.setVisibility(View.GONE);

        isCameraChange = true;
        centerMArker.setVisibility(View.VISIBLE);

        STrip_Type = getResources().getString(R.string.multiplestop_lable);
        Tv_PickupNow.setText(getResources().getString(R.string.ok_lable));
        Tv_PickupNow.setEnabled(true);

        book_my_ride_rideNow_layout.setVisibility(View.VISIBLE);
        fav_lyout.setVisibility(View.VISIBLE);
        back_lyout.setVisibility(View.VISIBLE);
        bookforsomeone.setVisibility(View.GONE);
        tv_setdropofflocation.setVisibility(View.VISIBLE);
        Rl_Back.setVisibility(View.GONE);
        Rl_destination_layout.setVisibility(View.VISIBLE);

        Rl_PickupNow.setEnabled(true);
        book_my_ride_selectcar_triptype_layout.setVisibility(View.GONE);
        layout_home_iamge.setVisibility(View.GONE);
        layout_home_iamge2.setVisibility(View.VISIBLE);


        if (CheckPlayService()) {
            if (!SdestinationLocation.equalsIgnoreCase("") && callingFrom.equalsIgnoreCase("findplaceBooking")) {
                isFromFindPlace = true;
                EnableOnCameraChange();
                if (stopOverMarkerNew != null) {
                    stopOverMarkerNew.remove();
                }
            } else {
                isFromFindPlace = false;
                googleMap.setOnCameraChangeListener(null);
                setInfoWidow(STrip_Type, selectedCarType, 2);
                if (stopOverMarkerNew != null) {
                    stopOverMarkerNew.remove();
                }
            }
        }

    }

    private void TrackMe() {
        params.setMargins(0, 0, 0, 0);
        book_my_ride_bottom_main_layout1.setLayoutParams(params);

        dragdropTv.setText(getResources().getString(R.string.dragdrop_lable));
        dragdropTv.setVisibility(View.GONE);
        centerMArker.setVisibility(View.GONE);


        Rl_destination_layout.setVisibility(View.VISIBLE);
        isCameraChange = true;
        /* if (!getResources().getString(R.string.type_your_destination_lable).equalsIgnoreCase(SdestinationLocation.toString()) && SdestinationLocation.toString().length() > 0) {
         */
        TrackMePopUp();
        if ("0".equalsIgnoreCase(Stracking)) {
            //adminTrackDialog();
            Stracking = "0";
        } else {
            Stracking = "0";
            Iv_Tracking.setBackground(getResources().getDrawable(R.drawable.tracking_unselect));
        }
    }

    private void BackClickHandle() {
        dragdropTv.setVisibility(View.VISIBLE);
        dragdropTv.setText(getResources().getString(R.string.lable_tripchoose));

        centerMArker.setVisibility(GONE);

        book_my_ride_selectcar_triptype_layout.setVisibility(View.VISIBLE);
        book_my_ride_rideNow_layout.setVisibility(View.GONE);
        fav_lyout.setVisibility(View.GONE);
        back_lyout.setVisibility(View.GONE);
        bookforsomeone.setVisibility(View.VISIBLE);
        tv_setdropofflocation.setVisibility(View.VISIBLE);
        layout_home_iamge.setVisibility(View.VISIBLE);
        Rl_Back.setVisibility(View.VISIBLE);
        Rl_destination_layout.setVisibility(View.GONE);
        isInfoWindowEnable = true;
        IV_selectcategoryImage.setVisibility(GONE);
        inflateuserinfo.setVisibility(GONE);
        layout_home_iamge2.setVisibility(GONE);


        book_my_ride_set_destination_edittextview.setText("");
        book_my_ride_set_destination_edittextview.setText("");
        // setInfoWidow(UserName, "", 1);
        if (stopOverMarkerNew != null) {
            stopOverMarkerNew.remove();
        }
        if (DefaultMarkerNew != null) {
            DefaultMarkerNew.remove();
        }
        addIcon(UserName.toUpperCase(), new LatLng(Double.parseDouble(SpickupLatitude), Double.parseDouble(SpickupLongitude)), 3);

        isCameraChange = false;

        CurrentLocationMethod();
        if (CheckPlayService()) {
            System.out.println("====================Muruga onCameraChangeListener  false====================");
            googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
        } else {
            Toast.makeText(BookingPage4.this,  getResources().getString(R.string.install_googleplay_view_location), Toast.LENGTH_LONG).show();
        }

        if (Rl_destination_layout.getVisibility() == GONE) {
            listview.setVisibility(GONE);
        }
    }

    private void EnableOnCameraChange() {

        isInfoWindowEnable = false;
        if (stopOverMarkerNew != null) {
            stopOverMarkerNew.remove();
        }
        if (DefaultMarkerNew != null) {
            DefaultMarkerNew.hideInfoWindow();
            DefaultMarkerNew.remove();

        }
        IV_selectcategoryImage.setVisibility(View.GONE);
        inflateuserinfo.setVisibility(GONE);
        dragdropTv.setVisibility(View.VISIBLE);
        //   centerMArker.setVisibility(View.VISIBLE);


        if (CheckPlayService()) {
            googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    return true;
                }
            });
        } else {
            Toast.makeText(BookingPage4.this,  getResources().getString(R.string.install_googleplay_view_location), Toast.LENGTH_LONG).show();
        }

        setInfoWidow(STrip_Type, selectedCarType, 2);

        System.out.println("====================Muruga onCameraChangeListener  True====================");
    }

    //-----------------------PaymentMethodList----------------
    private void PaymentMethodList(String Url) {
        spinKitView.setVisibility(View.VISIBLE);
        System.out.println("---------------LogOut Url-----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("lat", ScurrentLat);
        jsonParams.put("lon", ScurrentLan);
        jsonParams.put("code", SpaymentMode);
        jsonParams.put("mode", tripTypeMode);
        jsonParams.put("eta_amount", SetaAmount);

        mRequest = new ServiceRequest(BookingPage4.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------------PaymentMethodList-----------------" + response);
                String Sstatus = "", Sresponse = "", eta_amount = "", proceed_status = "", mode = "", error_message = "", xendit_step = "", xendittoken = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        eta_amount = object.getString("eta_amount");
                        proceed_status = object.getString("proceed_status");
                        proceed_status_global = proceed_status;
                        error_message = object.getString("error_message");
                        xendit_step = object.getString("xendit-step");
                        xendit_globalcheck = xendit_step;
                        xendittoken = object.getString("xendit-token");
                        mode = object.getString("mode");
                        mode_global = mode;
                        CURRENCYCONVERSIONKEY_KEY = object.getString("exchange-value");
                        Object intervention = object.get("payment_list");

                        if (intervention instanceof JSONArray) {
                            JSONArray jarrPaymentList = object.getJSONArray("payment_list");
                            if (jarrPaymentList.length() > 0) {
                                paymentListBooking.clear();
                                for (int i = 0; i < jarrPaymentList.length(); i++) {
                                    JSONObject jobjPayment = jarrPaymentList.getJSONObject(i);
                                    String payment_status = jobjPayment.getString("status");
                                    if (payment_status.equalsIgnoreCase("active")) {
                                        BookingPaymentListPojo bookingpayList = new BookingPaymentListPojo();
                                        bookingpayList.setName(jobjPayment.getString("name"));
                                        bookingpayList.setCode(jobjPayment.getString("code"));
                                        bookingpayList.setIcon(jobjPayment.getString("icon"));
                                        bookingpayList.setInactiveIcon(jobjPayment.getString("inactive_icon"));
                                        bookingpayList.setStatus(jobjPayment.getString("status"));
                                        paymentListBooking.add(bookingpayList);
                                    }
                                }
                            }
                        }
                        Object intervention1 = object.get("card_list");
                        if (intervention1 instanceof JSONArray) {
                            JSONArray jarrPaymentCardList = object.getJSONArray("card_list");
                            if (jarrPaymentCardList.length() > 0) {
                                paymentCardListBooking.clear();
                                for (int i = 0; i < jarrPaymentCardList.length(); i++) {
                                    BookingPaymentListPojo bookingpayList = new BookingPaymentListPojo();
                                    JSONObject jobjPayment = jarrPaymentCardList.getJSONObject(i);
                                    bookingpayList.setCard_number(jobjPayment.getString("card_number"));
                                    bookingpayList.setExp_month(jobjPayment.getString("exp_month"));
                                    bookingpayList.setExp_year(jobjPayment.getString("exp_year"));
                                    bookingpayList.setCard_type(jobjPayment.getString("card_type"));
                                    bookingpayList.setCustomer_id(jobjPayment.getString("customer_id"));
                                    bookingpayList.setCard_id(jobjPayment.getString("card_id"));
                                    bookingpayList.setCard_image(jobjPayment.getString("card_image"));
                                    paymentCardListBooking.add(bookingpayList);
                                }
                            }
                        }
                    } else {
                        Alert(getkey("action_error"), error_message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                spinKitView.setVisibility(View.GONE);
                if (Sstatus.equalsIgnoreCase("1")) {
                    if (proceed_status.equalsIgnoreCase("1")) {
                        if (mode.equalsIgnoreCase("stripe")) {
                            StripeDetailPage();
                        } else if (mode.equalsIgnoreCase("wallet")) {
                            selectedStripeCard = SpaymentMode;
                        } else if (mode.equalsIgnoreCase("xendit-card")) {
                            if (xendit_step.equalsIgnoreCase("1")) {
                                selectedStripeCard = SpaymentMode;
                            } else {
                                XenditiPayment(eta_amount);
                                selectedStripeCard = SpaymentMode;

                            }
                        }
                    } else {

                        if (mode.equalsIgnoreCase("stripe")) {
                            PaymentChoosePopUp_card(eta_amount, error_message, mode);
                        } else if (mode.equalsIgnoreCase("wallet")) {
                            PaymentChoosePopUp_wallet(eta_amount, error_message, mode);
                        } else if (mode.equalsIgnoreCase("xendit-card")) {
                            if (xendit_step.equalsIgnoreCase("1")) {
                                selectedStripeCard = SpaymentMode;
                            } else {
                                XenditiPayment(eta_amount);
                                selectedStripeCard = SpaymentMode;
                            }
                        }
                    }
                } else {
                    Alert1(getkey("action_error"), Sresponse);
                }
            }

            @Override
            public void onErrorListener() {
                spinKitView.setVisibility(View.GONE);
            }
        });
    }

    private void StripeDetailPage() {
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.stripe_paymentlist);

        stripeListView = (ListView) dialog.findViewById(R.id.card_info_card_details_listview);
        final TextView addcard_tv = (TextView) dialog.findViewById(R.id.addcard_tv);
        final TextView cancel_tv = (TextView) dialog.findViewById(R.id.cancel_tv);

        stripSmoothbar = (SmoothProgressBar) dialog.findViewById(R.id.profile_loading_progressbar1);
        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        stripeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedStripeCard = paymentCardListBooking.get(position).getCard_id();
                System.out.println("=============Muruga select Card Id " + selectedStripeCard);
                dialog.dismiss();
            }
        });

        addcard_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XenditiPayment("");

                dialog.dismiss();
            }
        });
        stripeAdapter = new StripeListVIewAdapter(BookingPage4.this, paymentCardListBooking);
        stripeListView.setAdapter(stripeAdapter);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    private void ReloadAmountWallet(String eta_amount, String error_message) {
        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim3);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.check_walletmoney_booking);

        final ImageView back_book = (ImageView) dialog.findViewById(R.id.back_book);
        final ImageView blinkImage = (ImageView) dialog.findViewById(R.id.drawer_icon);
        final TextView statusTv = (TextView) dialog.findViewById(R.id.status);
        final TextView allFareAmount = (TextView) dialog.findViewById(R.id.allfare_amount);
        final ImageView reloadAmountRl = (ImageView) dialog.findViewById(R.id.reload_layout1);
        final ListView paymentChoose = (ListView) dialog.findViewById(R.id.choose_payment_list);

        //statusTv.setText(error_message);
        allFareAmount.setText(strCurrency + " " + eta_amount);

        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        blinkImage.startAnimation(animation);
        reloadAmountRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.setbookingonProgress("1");
                Intent intent = new Intent(BookingPage4.this, Fastpayhome.class);
                intent.putExtra("frombookingpage", "1");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                dialog.dismiss();
            }
        });
        back_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.setbookingonProgress("90");
                dialog.dismiss();
            }
        });
        paymentChoose.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (paymentListBooking.size() > 0) {
                    paymentListAdapter.notifyDataSetChanged();
                    SpaymentMode = paymentListBooking.get(position).getCode();
                    paymenttype = paymentListBooking.get(position).getName();
//                    paymentname.setText(paymentListBooking.get(position).getName());
                    String paymentstatus = paymentListBooking.get(position).getStatus();
                    // selectedStripeCard=SpaymentMode;
                    System.out.println("============muruga paymenttype==============" + paymenttype + " " + SpaymentMode);
                    isCameraChange = false;
                    dialog.dismiss();
                    if (!SpaymentMode.equalsIgnoreCase("Cash") /*&& !SpaymentMode.equalsIgnoreCase("xendit-card")*/) {
                        if (!paymentstatus.equalsIgnoreCase("inactive")) {
                            if (isInternetPresent) {
                                PaymentMethodList(Iconstant.User_Booking_PaymentChoose_Url);
                            } else {
                                Alert(getResources().getString(R.string.alert), getResources().getString(R.string.alert_nointernet));
                            }
                        }
                    }
                }
            }
        });

        Eadapter = new BookingPaymentMethodAdapter(BookingPage4.this, paymentListBooking);
        paymentChoose.setAdapter(Eadapter);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void XenditiPayment(final String eta_amount) {
        xenditDialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        xenditDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        xenditDialog.setContentView(R.layout.profile_card_details_edit);

        TextView cardnumber = (TextView) xenditDialog.findViewById(R.id.cardnumber);
        cardnumber.setText(getkey("card_num"));
        TextView cardnumbers = (TextView) xenditDialog.findViewById(R.id.cardnumber);
        cardnumbers.setText(getkey("card_num"));

        TextView expire = (TextView) xenditDialog.findViewById(R.id.expire);
        expire.setText(getkey("expiry_date"));


        TextView cvvn = (TextView) xenditDialog.findViewById(R.id.cvvn);
        cvvn.setText(getkey("ccv_number"));
        TextView canee = (TextView) xenditDialog.findViewById(R.id.canee);
        canee.setText(getkey("track_your_ride_label_cancel"));

        TextView conf = (TextView) xenditDialog.findViewById(R.id.conf);
        conf.setText(getkey("confirm_lable"));


        cardNumberEt = (EditText) xenditDialog.findViewById(R.id.card_details_card_number);
        spinner_month = (Spinner) xenditDialog.findViewById(R.id.spinner2);
        spinner_year = (Spinner) xenditDialog.findViewById(R.id.spinner3);
        cardCvv = (EditText) xenditDialog.findViewById(R.id.card_details_cvv_number);
        final EditText cardHolderNameEt = (EditText) xenditDialog.findViewById(R.id.card_details_holder_name);
        cardHolderNameEt.setHint(getkey("entaer_name"));
        final RelativeLayout Cancel = (RelativeLayout) xenditDialog.findViewById(R.id.card_information_details_confirm_cancel);
        final RelativeLayout Confirm = (RelativeLayout) xenditDialog.findViewById(R.id.card_information_details_confirm);
        smoothbar = (SmoothProgressBar) xenditDialog.findViewById(R.id.profile_loading_progressbar);
        loadingTv = (TextView) xenditDialog.findViewById(R.id.loading_tv);
        yearss = (TextView) xenditDialog.findViewById(R.id.year);
        yearss.setText(getkey("year"));
        months = (TextView) xenditDialog.findViewById(R.id.month);
        months.setText(getkey("month"));
        PUBLISHABLE_KEY = session.getXenditPublicKey();
        spinner_month_and_year_process();

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                xenditDialog.dismiss();
            }
        });
        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int thisYear = Calendar.getInstance().get(Calendar.YEAR);
                int thismnth = Calendar.getInstance().get(Calendar.MONTH);
                cardNumber = cardNumberEt.getText().toString();
                if (cardNumber.length() == 0) {
                    AlertError(getkey("action_error"), getkey("valid_card_no"));

                    //erroredit(Edt1, getResources().getString(R.string.valid_card_no));
                } else if (cardNumber.length() < 16) {
                    AlertError(getkey("action_error"), getkey("valid_card_no_enter"));
                } else if (cardHolderNameEt.getText().toString().length() <= 0) {
                    AlertError(getkey("action_error"), getkey("valid_card_holder_name"));
                } else if (cardCvv.getText().toString().length() < 3) {
                    AlertError(getkey("action_error"), getkey("valid_cvv_code"));
                } else if (expMnthNUm < thismnth && expYearNum == thisYear) {
                    AlertError(getkey("action_error"), getkey("valid_validexp_mth"));
                } else if (expYearNum < thisYear) {
                    AlertError(getkey("action_error"), getkey("valid_exp_yrr"));
                } else if (expMonth.length() < 0 || expYear.length() < 0) {

                } else {
                    smoothbar.setVisibility(View.VISIBLE);
                    loadingTv.setVisibility(View.VISIBLE);
                    card_info = formCardInfo();
                    if (!selectedStripeCard.equalsIgnoreCase("xendit-card")) {
                        if (card_info.length() > 0) {
                            AddOrChangeCard(Iconstant.creditcard_stripe);
                        }
                    } else {
                        CreateXenditToken(eta_amount);
                    }
                    CloseKeyboard(cardCvv);                //CreateXenditToken(eta_amount);
                }
            }
        });


        xenditDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        xenditDialog.show();
    }

    private String formCardInfo() {

        String exp_month = expMonth;
        String exp_year = expYear;

        String card_number = cardNumberEt.getText().toString().trim();
        String cvv = cardCvv.getText().toString().trim();

        JSONObject jobjCardInfo = null;

        try {
            jobjCardInfo = new JSONObject();
            jobjCardInfo.put("card_number", card_number);
            jobjCardInfo.put("exp_month", exp_month);
            jobjCardInfo.put("exp_year", exp_year);
            jobjCardInfo.put("cvc_number", cvv);

            byte[] data = jobjCardInfo.toString().getBytes("UTF-8");
            System.out.println("CARD INFO = " + Base64.encodeToString(data, Base64.DEFAULT));
            return Base64.encodeToString(data, Base64.DEFAULT);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void AddOrChangeCard(String url) {
        smoothbar.setVisibility(View.VISIBLE);
        loadingTv.setVisibility(View.VISIBLE);
        String exp_month1 = expMonth; //spnr_exp_month.getSelectedItem().toString();
        String exp_year1 = expYear;//ed_exp_yr.getSelectedItem().toString().trim();
        String card_number = cardNumberEt.getText().toString();
        String cvv = cardCvv.getText().toString().trim();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("card_number", card_number);
        jsonParams.put("exp_month", exp_month1);
        jsonParams.put("exp_year", exp_year1);
        jsonParams.put("cvc_number", cvv);
        // jsonParams.put("card_info", card_info);
        if (card_id.length() > 0) {
            jsonParams.put("card_id", card_id);
        }
        System.out.println("=========Add or Change Card URL===========" + url);
        System.out.println("=========Add or Change Card Params===========" + jsonParams);
        stripRequest = new ServiceRequest(BookingPage4.this);
        stripRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("=========Add or Change Card Response===========" + response);
                String Sstatus = "", Sresponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        smoothbar.setVisibility(View.GONE);
                        loadingTv.setVisibility(View.GONE);
                        proceed_status_global = Sstatus;
//                        AlertCardAdded(getResources().getString(R.string.action_success), Sresponse);
                        xenditDialog.dismiss();
                    } else {
                        smoothbar.setVisibility(View.GONE);
                        loadingTv.setVisibility(View.GONE);
                        Alert1(getResources().getString(R.string.alert_label_title), Sresponse);

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

                smoothbar.setVisibility(View.GONE);
                loadingTv.setVisibility(View.GONE);
            }
        });
    }

    //--------------Alert Method-----------
    private void AlertCardAdded(String title, String alert) {
        final PkDialog mDialog = new PkDialog(BookingPage4.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void CreateXenditToken(final String etaAmount) {
        smoothbar.setVisibility(View.VISIBLE);
        xendit = new Xendit(getApplicationContext(), PUBLISHABLE_KEY);
        final boolean isMultipleUse = true;
        boolean shouldAuthenticate = true;

        Card card = new Card(cardNumber,
                expMonth,
                expYear,
                cardCvv.getText().toString());

        TokenCallback callback = new TokenCallback() {
            @Override
            public void onSuccess(Token token) {

                System.out.println("=======murugan  payment TokenId=========" + token.getId());
                if (token.getStatus().equalsIgnoreCase("VERIFIED")) {

                    if (isInternetPresent) {
                        smoothbar.setVisibility(View.GONE);
                        XenditTokenUpdate(Iconstant.XenditCard_TOKEN_UPDATE, token.getId());
                    } else {
                        Alert(getkey("action_error"), getResources().getString(R.string.alert_nointernet));
                    }
                    //createAuthenticationId(token.getId(), etaAmount);

                }
            }

            @Override
            public void onError(XenditError xenditError) {
                smoothbar.setVisibility(View.GONE);
                loadingTv.setVisibility(View.GONE);
            }
        };

        if (isMultipleUse) {
            xendit.createMultipleUseToken(card, callback);
        } else {
            Double amount = 0.0;
            amount = Double.parseDouble(etaAmount) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
            amount = (Double) Math.ceil(amount);

            System.out.println("===================amount=================" + amount);
            xendit.createSingleUseToken(card, amount, shouldAuthenticate, callback);
        }

    }


    private void XenditTokenUpdate(String Url, String tokenID) {

        dialog = new Dialog(BookingPage4.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));
        System.out.println("-------------Muruga XenditTokenUpdate Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("token_id", tokenID);

        System.out.println("-------------Muruga XenditTokenUpdate pharms----------------" + jsonParams);
        mRequest = new ServiceRequest(BookingPage4.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("-------------Muruga XenditTokenUpdate Response----------------" + response);

                String Sstatus = "", Smessage = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        xenditDialog.dismiss();
//                        AlertCardAdded(getResources().getString(R.string.action_success), Smessage);
                    } else {
                        AlertCardAdded(getResources().getString(R.string.alert_label_title), Smessage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void spinner_month_and_year_process() {
        String[] month = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        ArrayAdapter<CharSequence> monthAdapter = new ArrayAdapter<CharSequence>(BookingPage4.this, R.layout.spinner_text, month);
        monthAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        spinner_month.setAdapter(monthAdapter);
        String thisMonth = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);
        if (thisMonth.length() == 1) {
            thisMonth = "0" + thisMonth;
        }
        for (int j = 0; j <= month.length; j++) {

            if (month[j].equalsIgnoreCase(thisMonth)) {
                spinner_month.setSelection(j);
                break;
            }
        }
        spinner_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                expMonth = parent.getSelectedItem().toString();
                expMnthNUm = Integer.parseInt(expMonth);
                months.setText(expMonth);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                months.setText("month");
            }
        });
        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = thisYear; i <= thisYear + 40; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(BookingPage4.this, R.layout.spinner_text, years);
        yearAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        spinner_year.setAdapter(yearAdapter);
        spinner_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                expYear = parent.getSelectedItem().toString();
                expYearNum = Integer.parseInt(expYear);
                yearss.setText(expYear);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                yearss.setText("year");
            }
        });
    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(BookingPage4.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    //-------------------Select RideLater Dialog--------------------
    private void select_RideLater_Dialog() {

        selectedType = "1";
        bookforLater = "1";

        final Dialog dialog = new Dialog(BookingPage4.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pickup_later_new);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();


        dateChoose = (TextView) dialog.findViewById(R.id.date_tv);
        timesChoose = (TextView) dialog.findViewById(R.id.time_tv);

        final TextView confrim = (TextView) dialog.findViewById(R.id.confirm_text);
        final ImageView closeImage = (ImageView) dialog.findViewById(R.id.closeimage);
        final View selectview1 = (View) dialog.findViewById(R.id.selectview1);
        final View selectview2 = (View) dialog.findViewById(R.id.selectview2);


        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE, MMM dd");
        String dayOfWeek = simpledateformat.format(new Date());
        dateChoose.setText(dayOfWeek);

        Date currentTime1 = Calendar.getInstance().getTime();
        String aTimeStr = new SimpleDateFormat("hh:mm a").format(currentTime1);
        coupon_selectedTime = new SimpleDateFormat("hh:mm aa").format(currentTime1);
        timesChoose.setText(aTimeStr);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sTodayDate = sdf.format(new Date());
        coupon_selectedDate = sdf.format(new Date());

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, 1);

        Date d = cal.getTime();
        SimpleDateFormat mTime_Formatter = new SimpleDateFormat("HH");
        currentTime = mTime_Formatter.format(d);

        Calendar c = Calendar.getInstance();
        CurrentMinute = c.get(Calendar.MINUTE);

        Year = c.get(Calendar.YEAR);
        Month = c.get(Calendar.MONTH);
        DayOfMonth = c.get(Calendar.DATE);
        Hours = c.get(Calendar.HOUR);
        Minitues = c.get(Calendar.MINUTE);

        sSelectedDate = Year + "-" + String.format("%02d", (Month + 1)) + "-" + String.format("%02d", DayOfMonth);
        selectedTime = Hours + "";
        SelectedMinutes = Minitues;


        System.out.println("---------coupon_selectedDate-----------" + coupon_selectedDate);
        System.out.println("---------coupon_selectedTime-----------" + coupon_selectedTime);


        dateChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFromDatePicker();
                selectview1.setVisibility(View.VISIBLE);
                selectview2.setVisibility(View.GONE);
            }
        });
        timesChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectview2.setVisibility(View.VISIBLE);
                selectview1.setVisibility(View.GONE);
                showFromTimePicker();
            }
        });




        confrim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTime();

                dialog.dismiss();
            }
        });


        dialog.setCanceledOnTouchOutside(true);
    }


    private void showFromDatePicker() {
        try {
            Calendar now1 = Calendar.getInstance();
            Calendar now = Calendar.getInstance();
            now.add(Calendar.DAY_OF_MONTH, 7);
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    this,
                    now1.get(Calendar.YEAR),
                    now1.get(Calendar.MONTH),
                    now1.get(Calendar.DAY_OF_MONTH)
            );
            dpd.setAccentColor(getResources().getColor(R.color.signin_and_signup_slider_ball_blue));
            //  dpd.setAccentColor(Color.parseColor("#96ff9933"));
            dpd.setMaxDate(now);
            dpd.setMinDate(now1);
            dpd.show(getFragmentManager(), "Datepickerdialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showFromTimePicker() {
        try {
            Calendar now = Calendar.getInstance();
            TimePickerDialog tpd = TimePickerDialog.newInstance(
                    this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    false
            );
            tpd.setAccentColor(getResources().getColor(R.color.signin_and_signup_slider_ball_blue));
            tpd.show(getFragmentManager(), "Timepickerdialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Year = year;
        Month = monthOfYear;
        DayOfMonth = dayOfMonth;

        sSelectedDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        coupon_selectedDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

        System.out.println("-----sTodayDate----" + sTodayDate);
        System.out.println("-----sSelectedDate----" + sSelectedDate);


        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE,MMM dd");
        Date dates = new Date(year, monthOfYear, dayOfMonth);
        String dayOfWeek = simpledateformat.format(dates);
        dateChoose.setText(dayOfWeek);

//        showFromTimePicker();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        Hours = hourOfDay;
        Minitues = minute;
        selectedTime = hourOfDay + "";
        SelectedMinutes = minute;
        System.out.println("---------currentTime-----------" + currentTime);
        System.out.println("---------selectedTime-----------" + hourOfDay);


        System.out.println("---------CurrentMinute-----------" + CurrentMinute);
        System.out.println("---------SelectedMinutes-----------" + minute);

        Calendar calendar = Calendar.getInstance();
        calendar.set(0, 0, 0, hourOfDay, minute, 0);
        long timeInMillis = calendar.getTimeInMillis();
        java.text.DateFormat dateFormatter = new SimpleDateFormat("hh:mm a");
        Date date = new Date();
        date.setTime(timeInMillis);
        timesChoose.setText(dateFormatter.format(date));
        coupon_selectedTime = dateFormatter.format(date);

        System.out.println("---------coupon_selectedDate-----------" + coupon_selectedDate);
        System.out.println("---------coupon_selectedTime-----------" + coupon_selectedTime);


    }

    private void dateTime() {

        SimpleDateFormat mFormatter = new SimpleDateFormat("MMM/dd,hh:mm aa");

        Date date = new Date(Year, Month, DayOfMonth - 1, Hours, Minitues, 0);
        String displayTime = mFormatter.format(date);
        System.out.println("----------displayTime----------" + displayTime);

        if (selectedTime.equalsIgnoreCase("00")) {
            selectedTime = "24";
        }

        System.out.println("---------sTodayDate-----------" + sTodayDate);
        System.out.println("---------sSelectedDate-----------" + sSelectedDate);

        if (sTodayDate.equalsIgnoreCase(sSelectedDate)) {
            if (Integer.parseInt(currentTime) <= Integer.parseInt(selectedTime)) {
                if (Integer.parseInt(selectedTime) - Integer.parseInt(currentTime) == 0) {
                    Calendar c = Calendar.getInstance();
                    int CurrentMinute = c.get(Calendar.MINUTE);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    int SelectedMinutes = calendar.get(Calendar.MINUTE);
                    if (CurrentMinute <= SelectedMinutes) {
                        if (selectedType.equalsIgnoreCase("1")) {
                            Tv_PickupNow.setTextColor(Color.parseColor("#FFFFFF"));
                            Rl_PickupNow.setClickable(true);
                        }


                        selectedDate = coupon_mFormatter.format(date);
                        selectedTime = coupon_time_mFormatter.format(date);

                        dateChoose.setText(selectedDate);
                        timesChoose.setText(selectedTime);

                    } else {
                        Alert(getResources().getString(R.string.alert_label_ridelater_title), getResources().getString(R.string.alert_label_ridelater_content));
                    }
                } else {
                    if (selectedType.equalsIgnoreCase("1")) {
                        Tv_PickupNow.setTextColor(Color.parseColor("#FFFFFF"));
                        Rl_PickupNow.setClickable(true);
                    }
                    selectedDate = coupon_mFormatter.format(date);
                    selectedTime = coupon_time_mFormatter.format(date);

                    dateChoose.setText(selectedDate);
                    timesChoose.setText(selectedTime);
                    // EstimateParam_method(2);
                    Tv_PickupNow.setEnabled(true);
                    Animation animFadeIn = AnimationUtils.loadAnimation(BookingPage4.this, R.anim.fade_in);
                    Rl_ConfirmCancelLayout.startAnimation(animFadeIn);
                    // Tv_PickupLater.setText(getResources().getString(R.string.schedule_lable));
                    Rl_Back.setVisibility(View.VISIBLE);
                    bookforsomeone.setVisibility(View.VISIBLE);
                    tv_setdropofflocation.setVisibility(View.VISIBLE);
                    Rl_Bottom_layout.setVisibility(View.VISIBLE);
                }

            } else {
                Alert(getResources().getString(R.string.alert_label_ridelater_title), getResources().getString(R.string.alert_label_ridelater_content));
            }
        } else {
            //Enable and Disable RideNow Button
            if (selectedType.equalsIgnoreCase("1")) {
                Tv_PickupNow.setTextColor(Color.parseColor("#ffffff"));
                Rl_PickupNow.setClickable(true);
            }
            selectedDate = coupon_mFormatter.format(date);
            selectedTime = coupon_time_mFormatter.format(date);
            dateChoose.setText(selectedDate);
            timesChoose.setText(selectedTime);
            //EstimateParam_method(2);
            Tv_PickupNow.setEnabled(true);
            Animation animFadeIn = AnimationUtils.loadAnimation(BookingPage4.this, R.anim.fade_in);
            Rl_ConfirmCancelLayout.startAnimation(animFadeIn);

            //  Tv_PickupLater.setText(getResources().getString(R.string.schedule_lable));
            Rl_Back.setVisibility(View.VISIBLE);
            bookforsomeone.setVisibility(View.VISIBLE);
            tv_setdropofflocation.setVisibility(View.VISIBLE);
            Rl_Bottom_layout.setVisibility(View.VISIBLE);
        }
    }

    private void addIcon(String text, LatLng position, int flag) {
        MarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_markerlayout, null);

        TextView numTxt = (TextView) MarkerView.findViewById(R.id.num_txt);
        ImageView markerImage = (ImageView) MarkerView.findViewById(R.id.markerimage);
        ImageView markerimageback = (ImageView) MarkerView.findViewById(R.id.markerimageback);
        numTxt.setText(text);

        if (hideleftmarker) {
            markerimageback.setVisibility(VISIBLE);
        } else {
            markerimageback.setVisibility(GONE);
        }

        if (flag == 2) {
            numTxt.setTextColor(Color.parseColor("#0D3AA7"));
            numTxt.setBackgroundColor(getResources().getColor(R.color.transparant_color));
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 0);
            numTxt.setLayoutParams(params);
            markerImage.setImageResource(R.drawable.icn_pointer_round);
        } else if (flag == 3) {
            numTxt.setTextColor(Color.parseColor("#ffffff"));
            numTxt.setBackground(getResources().getDrawable(R.drawable.blue_bg_curve));
            markerimageback.setColorFilter(getResources().getColor(R.color.imagemarkerback));
            markerImage.setImageResource(R.drawable.icn_pointer_round);
        } else {
            markerImage.setImageResource(R.drawable.red);
        }
        stopOverMarkerNew = googleMap.addMarker(new MarkerOptions()
                .position(position)
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, MarkerView))));
    }

    private void countryCode() {
        gpsTracker = new GPSTracker(BookingPage4.this);
        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {

            double MyCurrent_lat = gpsTracker.getLatitude();
            double MyCurrent_long = gpsTracker.getLongitude();

            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {

                    String Str_getCountryCode = addresses.get(0).getCountryCode();
                    if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode);
                        CountryCodeStr = Str_countyCode;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();

        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


    private void checkrideacceptornot(String Url, final String rideid) {




        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", rideid);
        jsonParams.put("user_id", UserID);
        mRequest = new ServiceRequest(BookingPage4.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                dialogDismiss();
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject response_object = object.getJSONObject("response");
                            if (response_object.length() > 0) {

                                Object check_driver_profile_object = response_object.get("driver_profile");
                                if (check_driver_profile_object instanceof JSONObject) {
                                    JSONObject driver_profile_object = response_object.getJSONObject("driver_profile");
                                    if (driver_profile_object.length() > 0) {

                                        System.out.println("==================Ride Requet================"+response);
                                        session.setCouponCode("", "");
                                        session.setReferralCode("", "");
                                        Intent i = new Intent(getApplicationContext(), TrackRideAcceptPage.class);
                                        i.putExtra("driverID", driver_profile_object.getString("driver_id"));
                                        i.putExtra("driverName", driver_profile_object.getString("driver_name"));
                                        i.putExtra("driverImage", driver_profile_object.getString("driver_image"));
                                        i.putExtra("driverRating", driver_profile_object.getString("driver_review"));
                                        i.putExtra("driverTime", driver_profile_object.getString("min_pickup_duration"));
                                        i.putExtra("rideID", rideid);
                                        i.putExtra("driverMobile", driver_profile_object.getString("phone_number"));
                                        i.putExtra("driverCar_no", driver_profile_object.getString("vehicle_number"));
                                        i.putExtra("driverCar_model", driver_profile_object.getString("vehicle_model"));
                                        i.putExtra("flag", "1");
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);


                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogDismiss();
                }

            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void CurrentLocationMethod() {
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        gps = new GPSTracker(getApplicationContext());

        if (gps.canGetLocation() && gps.isgpsenabled()) {

            MyCurrent_lat = gps.getLatitude();
            MyCurrent_long = gps.getLongitude();

            if (mRequest != null) {
                mRequest.cancelRequest();
            }
            if (isInternetPresent) {
                System.out.println("========= Map_movingTask3=================");
                Map_movingTask asynTask = new Map_movingTask(MyCurrent_lat, MyCurrent_long);
                asynTask.execute();
            } else {
                Alert(getkey("action_error"), getResources().getString(R.string.alert_nointernet));
            }
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(MyCurrent_lat, MyCurrent_long)).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        } else {
            enableGpsService();
            //Toast.makeText(getActivity(), "GPS not Enabled !!!", Toast.LENGTH_LONG).show();
        }
    }

    private void DestLocFromFindPlace() {

        if (session.getNightMode()) {
            Iv_CenterMarker.setImageResource(R.drawable.icn_pointer_dot1);
        } else {
            Iv_CenterMarker.setImageResource(R.drawable.pickuptoofar);
        }
    }


    ///// phantom animation try

    public void phantomcaranimation(Double latitude, Double longitude) {
        sydney_us = new LatLng(latitude, longitude);
//        try {
//
//            Picasso.with(BookingPage3.this)
//                    .load(sCarImg)
//                    .into(new Target() {
//                        @Override
//                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                            String s = BitMapToString(bitmap);
//                            System.out.println("session bitmap" + s);
//                            session.setVehicle_BitmapImage(s);
//                            bmp = bitmap;
//
//                        }
//
//                        @Override
//                        public void onBitmapFailed(Drawable errorDrawable) {
//
//                        }
//
//                        @Override
//                        public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                        }
//                    });
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


//        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
//                .target(googleMap.getCameraPosition().target)
//                .zoom(17)
//                .bearing(30)
//                .tilt(45)
//                .build()));

        String requestUrl = null;
        try {
            requestUrl = "https://maps.googleapis.com/maps/api/directions/json?" +
                    "mode=driving&"
                    + "transit_routing_preference=less_driving&"
                    + "origin=" + latitude + "," + longitude + "&"
                    + "destination=" + destination_us + "&"
                    + "key=" + getResources().getString(R.string.google_directions_key);

            JsonObjectRequest req = new JsonObjectRequest(requestUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray jsonArray = response.getJSONArray("routes");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject route = jsonArray.getJSONObject(i);
                                    JSONObject poly = route.getJSONObject("overview_polyline");
                                    String polyline = poly.getString("points");
                                    polyLineList_us = decodePoly(polyline);

                                }
                                //Adjusting bounds
                                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                for (LatLng latLng : polyLineList_us) {
                                    builder.include(latLng);
                                }
                                LatLngBounds bounds = builder.build();
                                CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 2);
                                googleMap.animateCamera(mCameraUpdate);

                                polylineOptions_us = new PolylineOptions();
                                polylineOptions_us.width(0);
                                polylineOptions_us.startCap(new SquareCap());
                                polylineOptions_us.endCap(new SquareCap());
                                polylineOptions_us.jointType(ROUND);
                                polylineOptions_us.addAll(polyLineList_us);
                                greyPolyLine_us = googleMap.addPolyline(polylineOptions_us);

                                blackPolylineOptions_us = new PolylineOptions();
                                blackPolylineOptions_us.width(0);
                                blackPolylineOptions_us.startCap(new SquareCap());
                                blackPolylineOptions_us.endCap(new SquareCap());
                                blackPolylineOptions_us.jointType(ROUND);
                                blackPolyline_us = googleMap.addPolyline(blackPolylineOptions_us);

//                                googleMap.addMarker(new MarkerOptions()
//                                        .position(polyLineList_us.get(polyLineList_us.size() - 1)));

                                ValueAnimator polylineAnimator = ValueAnimator.ofInt(0, 100);
                                polylineAnimator.setDuration(1000);
                                polylineAnimator.setInterpolator(new LinearInterpolator());
                                polylineAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                    @Override
                                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                        List<LatLng> points = greyPolyLine_us.getPoints();
                                        int percentValue = (int) valueAnimator.getAnimatedValue();
                                        int size = points.size();
                                        int newPoints = (int) (size * (percentValue / 100.0f));
                                        List<LatLng> p = points.subList(0, newPoints);
                                        blackPolyline_us.setPoints(p);
                                    }
                                });
                                polylineAnimator.start();
                                marker_us = googleMap.addMarker(new MarkerOptions().position(sydney_us)
                                        .flat(true)
                                        .icon(BitmapDescriptorFactory.fromBitmap(bmp)));
                                handler_us = new Handler();
                                index_us = -1;
                                next_us = 1;
                                handler_us.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (index_us < polyLineList_us.size() - 1) {
                                            index_us++;
                                            next_us = index_us + 1;
                                        }
                                        if (index_us < polyLineList_us.size() - 1) {
                                            startPosition_us = polyLineList_us.get(index_us);
                                            endPosition_us = polyLineList_us.get(next_us);
                                        }
                                        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                                        valueAnimator.setDuration(1000);
                                        valueAnimator.setInterpolator(new LinearInterpolator());
                                        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                            @Override
                                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                v_us = valueAnimator.getAnimatedFraction();
                                                lng_us = v_us * endPosition_us.longitude + (1 - v_us)
                                                        * startPosition_us.longitude;
                                                lat_us = v_us * endPosition_us.latitude + (1 - v_us)
                                                        * startPosition_us.latitude;
                                                LatLng newPos = new LatLng(lat_us, lng_us);
                                                marker_us.setPosition(newPos);
                                                marker_us.setAnchor(0.5f, 0.5f);
                                                marker_us.setRotation(getBearing(startPosition_us, newPos));

                                            }
                                        });
                                        valueAnimator.start();
                                        handler_us.postDelayed(this, 1000);
                                    }
                                }, 1000);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e(getResources().getString(R.string.error), error.getMessage());
                }
            });


            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(req);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }


    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


    @Override
    protected void onStop() {
        super.onStop();

        System.out.println("");
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}

