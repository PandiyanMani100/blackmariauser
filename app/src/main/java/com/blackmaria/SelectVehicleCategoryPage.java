package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.InterFace.VehicleCatSelect;
import com.blackmaria.newdesigns.SelectVehicleAdapter;
import com.blackmaria.newdesigns.profileInfo.ProfileConstain_activity;
import com.blackmaria.pojo.BookingPojo1;
import com.blackmaria.pojo.Getmapview;
import com.blackmaria.pojo.HomePojo;
import com.blackmaria.pojo.NewCategoryListPojo;
import com.blackmaria.pojo.PersonPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.view.View.GONE;

/**
 * Created by user144 on 8/3/2017.
 */

public class SelectVehicleCategoryPage extends AppCompatActivity {
    //    private ExpandableHeightListView listview;
    private String Str_free_waittime = "", Str_wait_time_person = "", Str_car_img, Str_car_eta;
    private ArrayList<NewCategoryListPojo> catetorylist;
    SelectVehicleAdapter CatetoryAdapter;
    CountryPicker picker;
    private boolean isCategoryDataAvailable = false;
    private String Category_Selected_id = "";
    private ImageView cancel_layout;
    private String UserID = "", UserName = "", CategoryID = "", Gender = "";
    private Dialog dialog, dialog1;
    ArrayList<BookingPojo1> driver_list;
    private boolean driver_status = false;
    LanguageDb mhelper;
    Bitmap bitmasp ;
    private String SdestinationLatitude = "";
    private String SdestinationLongitude = "", intentcall = "";
    private String SdestinationLocation = "";
    private String SpickupLatitude, SpickupLongitude;
    private String SpickupLocation = "", Str_Page;

    private ServiceRequest mRequest;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;

    private RefreshReceiver refreshReceiver;

    //new design
    private TextView tv_pickup_address;
    private RecyclerView listview;
    private RelativeLayout rl_pickup_address_changinglayout;
    private int placeSearch_request_code = 200;


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(BookingPage1.this, BookingPage1.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(SelectVehicleCategoryPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(SelectVehicleCategoryPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_vehicle_categorypage_layout);
        mhelper=  new LanguageDb(this);
        Initialize();
        picker = CountryPicker.newInstance(getkey("select_country_lable"));
        cancel_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onBackPressed();
//                Intent intent = new Intent(SelectVehicleCategoryPage.this, BookingPage1.class);
//                // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });

        rl_pickup_address_changinglayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectVehicleCategoryPage.this, DropLocationSelect.class);
                intent.putExtra("page", "booking1");
                startActivityForResult(intent, placeSearch_request_code);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("");
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == placeSearch_request_code) {
            if (resultCode == Activity.RESULT_OK) {
                double SpickupLatitude_double = Double.parseDouble(data.getStringExtra("Selected_Latitude"));
                double SpickupLongitude_double = Double.parseDouble(data.getStringExtra("Selected_Longitude"));
                SpickupLatitude = String.valueOf(SpickupLatitude_double);
                SpickupLongitude = String.valueOf(SpickupLongitude_double);
                SpickupLocation = data.getStringExtra("Selected_Location");
                tv_pickup_address.setText(SpickupLocation);

                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", UserID);
                    jsonParams.put("lat", SpickupLatitude);
                    jsonParams.put("lon", SpickupLongitude);
                    jsonParams.put("category", CategoryID);
                    PostRequest(Iconstant.get_cabs_url, jsonParams);
                } else {
                    Alert(getkey("action_error"), getkey("alert_nointernet"));
                }


            }
        }
    }

    VehicleCatSelect vehicle_inf = new VehicleCatSelect() {
        @Override
        public void Selectvehicle(final int position) {

            if(catetorylist.get(position).getCat_type().equals("delivery") && catetorylist.get(position).getCategory_name().equals("DISPATCH"))
            {
                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SelectVehicleCategoryPage.this);
                View view = getLayoutInflater().inflate(R.layout.dispatchpopup, null);
                bottomSheetDialog.setContentView(view);
                bottomSheetDialog.setCancelable(true);
                final TextView headings = (TextView)view.findViewById(R.id.headings);
                headings.setText(catetorylist.get(position).getCategory_name().toUpperCase());
                final TextView subtitle = (TextView)view.findViewById(R.id.subtitle);
                subtitle.setText(mhelper.getvalueforkey("standard_dispatch_van_size_1_60cm_x_1_50cm_and_100cm_height_please_makesure_your_items_not_more_than_standard_size_in_total"));
                final TextView receiver_name = (TextView)view.findViewById(R.id.receiver_name);
                receiver_name.setText(mhelper.getvalueforkey("receiver_name"));
                final TextView mobile_noo = (TextView)view.findViewById(R.id.mobile_noo);
                mobile_noo.setText(mhelper.getvalueforkey("dispatchmobile_no"));
                final TextView itemqty = (TextView)view.findViewById(R.id.itemqty);
                itemqty.setText(mhelper.getvalueforkey("items_quantity"));
                final TextView items_description = (TextView)view.findViewById(R.id.items_description);
                items_description.setText(mhelper.getvalueforkey("items_description"));
                final TextView agreeeme = (TextView)view.findViewById(R.id.agreeeme);
                agreeeme.setText(mhelper.getvalueforkey("agreed_with_the_terms_and_conditions_here"));
                final TextView fullsage = (TextView)view.findViewById(R.id.fullsage);
                fullsage.setText(mhelper.getvalueforkey("probb"));

                final ImageView closedialog = (ImageView)view.findViewById(R.id.closedialog);
                closedialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                    }
                });

                final EditText itemdeshint = (EditText)view.findViewById(R.id.itemdeshint);
                itemdeshint.setHint(mhelper.getvalueforkey("eg_documents"));
                final EditText receivernameedit = (EditText)view.findViewById(R.id.receivernameedit);
                final EditText edit_mobilenumber = (EditText)view.findViewById(R.id.edit_mobilenumber);
                setupFullHeight(bottomSheetDialog);

                final Spinner chooseitem = (Spinner)view.findViewById(R.id.chooseitem);
                String[] country = { "1 item", "2 item", "3 item"};
                ArrayAdapter aa = new ArrayAdapter(SelectVehicleCategoryPage.this,android.R.layout.simple_spinner_item,country);
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Setting the ArrayAdapter data on the Spinner
                chooseitem.setAdapter(aa);


                final TextView cancel = (TextView)view.findViewById(R.id.cancel);
                final TextView submit = (TextView)view.findViewById(R.id.submit);
                submit.setText(mhelper.getvalueforkey("confirm"));
                cancel.setText(mhelper.getvalueforkey("cancel"));
                final TextView txt_country_code = (TextView)view.findViewById(R.id.txt_country_code);

                final ImageView flag = (ImageView)view.findViewById(R.id.flag);
                flag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                    }
                });

                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code, String dialCode) {
                        picker.dismiss();
                        txt_country_code.setText(dialCode);
                        String drawableName = "flag_"
                                + code.toLowerCase(Locale.ENGLISH);
                        flag.setImageResource(getResId(drawableName));
                        CloseKeyBoard();
                    }
                });

                final CheckBox chea = (CheckBox)view.findViewById(R.id.chea);

                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(receivernameedit.getText().toString().equals(""))
                        {
                            Toast.makeText(getApplicationContext(), mhelper.getvalueforkey("recmand"),Toast.LENGTH_LONG).show();
                        }
                        else if(edit_mobilenumber.getText().toString().equals(""))
                        {
                            Toast.makeText(getApplicationContext(), mhelper.getvalueforkey("mobile_numinavalid"),Toast.LENGTH_LONG).show();
                        }
                        else if(itemdeshint.getText().toString().equals(""))
                        {
                            Toast.makeText(getApplicationContext(), mhelper.getvalueforkey("itemdesempty"),Toast.LENGTH_LONG).show();
                        }
                        else if(!chea.isChecked())
                        {
                            Toast.makeText(getApplicationContext(), mhelper.getvalueforkey("agreed_wsith_the_terms_and_conditions_here"),Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            bottomSheetDialog.dismiss();
                            selectmovepage(position);
                        }
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                    }
                });

                bottomSheetDialog.show();
            }
            else  if(catetorylist.get(position).getCat_type().equals("delivery") && catetorylist.get(position).getCategory_name().equals("CITYBIKE"))
            {
                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SelectVehicleCategoryPage.this);
                View view = getLayoutInflater().inflate(R.layout.biketyy, null);
                bottomSheetDialog.setContentView(view);
                bottomSheetDialog.setCancelable(true);
                final TextView headings = (TextView)view.findViewById(R.id.headings);
                headings.setText(catetorylist.get(position).getCategory_name().toUpperCase());
                final TextView subtitle = (TextView)view.findViewById(R.id.subtitle);
                subtitle.setText(mhelper.getvalueforkey("select_your_trip"));

                final TextView itemp = (TextView)view.findViewById(R.id.itemp);
                itemp.setText(mhelper.getvalueforkey("item_parcel"));
                final TextView person = (TextView)view.findViewById(R.id.person);
                person.setText(mhelper.getvalueforkey("persons"));

                final LinearLayout sdpe = (LinearLayout)view.findViewById(R.id.sdpe);
                sdpe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                        selectmovepage(position);
                    }
                });

                final LinearLayout itepp = (LinearLayout)view.findViewById(R.id.itepp);
                itepp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                        openagain(position);
                    }
                });

                final ImageView closedialog = (ImageView)view.findViewById(R.id.closedialog);
                closedialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                    }
                });

                bottomSheetDialog.show();
            }
            else
            {
                selectmovepage(position);
            }

        }
    };

    private void selectmovepage(final int position)
    {
        if (isCategoryDataAvailable) {
            Category_Selected_id = catetorylist.get(position).getCategory_Id();
            System.out.println("=================Category_Selected_id==============" + Category_Selected_id);
            Str_car_img = catetorylist.get(position).getCat_active_img();

            String value = catetorylist.get(position).getMax_person();
            StringBuffer sBuffer = new StringBuffer();
            Pattern p = Pattern.compile("[0-9]+.[0-9]*|[0-9]*.[0-9]+|[0-9]+");
            Matcher m = p.matcher(value);
            while (m.find()) {
                sBuffer.append(m.group());
            }
            String cc= sBuffer.toString().trim();
            session.setCarCatImage(Str_car_img);
            System.out.println("===========onlyy numbers===========" + cc);

            if(cc.equals(""))
            {
                session.setMaxperson("5");
            }
            else
            {
                if (isNumeric(cc))
                {
                    System.out.println("===========contain number==========" + cc);
                    int maxp = Integer.parseInt(cc.trim());
                    if(maxp > 8)
                    {
                        session.setMaxperson("8");
                    }
                    else
                    {
                        if(maxp == 0)
                        {
                            session.setMaxperson("1");
                        }
                        else
                        {
                            session.setMaxperson(""+maxp);
                        }
                    }

                }
                else
                {
                    System.out.println("===========contain characterr==========" + cc);
                    session.setMaxperson("5");
                }
            }

            Str_car_eta = catetorylist.get(position).getCat_eta();
            Str_free_waittime = catetorylist.get(position).getStr_free_waittime();
            Str_wait_time_person = catetorylist.get(position).getStr_wait_time_person();
            session.setFreeWaitingTime(Str_free_waittime);
            session.setFreeWaitingTimeRate(Str_free_waittime, Str_wait_time_person);
            session.setSelectCatCarImage(catetorylist.get(position).getCat_active_img());

            if (catetorylist.get(position).getCat_type().equalsIgnoreCase("delivery") /*&& !catetorylist.get(position).getCat_eta().equalsIgnoreCase("Not Available")*/) {

                //PostRequestCarImage(Iconstant.BookMyRide_url, SpickupLatitude, SpickupLongitude);
                Picasso.with(SelectVehicleCategoryPage.this)
                        .load(catetorylist.get(position).getCar_image())
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                bitmasp =bitmap;
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmasp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] byteArray = stream.toByteArray();

                                try {
                                    if (catetorylist.get(position).getStrOfferType().equalsIgnoreCase("motorbike"))
                                    {
                                        AlertDelivaryMotorBike(catetorylist.get(position).getDelivery_des(),byteArray);
                                    }
                                    else
                                    {
                                        AlertDelivary(R.drawable.icn_delivery, catetorylist.get(position).getDelivery_des(),byteArray);
                                    }
                                }
                                catch
                                (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                byte[] byteArray = null;
                                if (catetorylist.get(position).getStrOfferType().equalsIgnoreCase("motorbike"))
                                {
                                    AlertDelivaryMotorBike(catetorylist.get(position).getDelivery_des(),byteArray);
                                }
                                else
                                {
                                    AlertDelivary(R.drawable.icn_delivery, catetorylist.get(position).getDelivery_des(),byteArray);
                                }
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });

            } else
            {
                //PostRequestCarImage(Iconstant.BookMyRide_url, SpickupLatitude, SpickupLongitude);
                Picasso.with(SelectVehicleCategoryPage.this)
                        .load(catetorylist.get(position).getCar_image())
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                try {
                                    bitmasp =bitmap;
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    bitmasp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                    byte[] byteArray = stream.toByteArray();
                                    if (intentcall.equalsIgnoreCase("findplaceBooking")) {

                                        Intent intent = new Intent(SelectVehicleCategoryPage.this, BookingPage3.class);
                                        intent.putExtra("pickupAddress", SpickupLocation);
                                        intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                                        intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                                        intent.putExtra("destinationAddress", SdestinationLocation);
                                        intent.putExtra("destinationLatitude", SdestinationLatitude);
                                        intent.putExtra("destinationLongitude", SdestinationLongitude);
                                        intent.putExtra("CategoryID", Category_Selected_id);
                                        intent.putExtra("driver_status", driver_status);
                                        intent.putExtra("page", "BookingPage1");
                                        intent.putExtra("intentCall", "findplaceBooking");
                                        intent.putExtra("hideleftmarker", "yes");
                                        intent.putExtra("image",byteArray);
                                        startActivity(intent);

                                        //overridePendingTransition(R.anim.enter, R.anim.exit);
                                    } else {
                                        Intent intent = new Intent(SelectVehicleCategoryPage.this, BookingPage3.class);
                                        intent.putExtra("pickupAddress", SpickupLocation);
                                        intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                                        intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                                        intent.putExtra("destinationAddress", "");
                                        intent.putExtra("destinationLatitude", "");
                                        intent.putExtra("destinationLongitude", "");
                                        intent.putExtra("CategoryID", Category_Selected_id);
                                        intent.putExtra("page", "BookingPage1");
                                        intent.putExtra("driver_status", driver_status);
                                        intent.putExtra("hideleftmarker", "yes");
                                        intent.putExtra("image",byteArray);
                                        startActivity(intent);

                                        overridePendingTransition(R.anim.enter, R.anim.exit);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                byte[] byteArray = null;
                                if (intentcall.equalsIgnoreCase("findplaceBooking")) {

                                    Intent intent = new Intent(SelectVehicleCategoryPage.this, BookingPage3.class);
                                    intent.putExtra("pickupAddress", SpickupLocation);
                                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                                    intent.putExtra("destinationAddress", SdestinationLocation);
                                    intent.putExtra("destinationLatitude", SdestinationLatitude);
                                    intent.putExtra("destinationLongitude", SdestinationLongitude);
                                    intent.putExtra("CategoryID", Category_Selected_id);
                                    intent.putExtra("driver_status", driver_status);
                                    intent.putExtra("page", "BookingPage1");
                                    intent.putExtra("intentCall", "findplaceBooking");
                                    intent.putExtra("hideleftmarker", "yes");
                                    intent.putExtra("image",byteArray);
                                    startActivity(intent);

                                    //overridePendingTransition(R.anim.enter, R.anim.exit);
                                } else {
                                    Intent intent = new Intent(SelectVehicleCategoryPage.this, BookingPage3.class);
                                    intent.putExtra("pickupAddress", SpickupLocation);
                                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                                    intent.putExtra("destinationAddress", "");
                                    intent.putExtra("destinationLatitude", "");
                                    intent.putExtra("destinationLongitude", "");
                                    intent.putExtra("CategoryID", Category_Selected_id);
                                    intent.putExtra("page", "BookingPage1");
                                    intent.putExtra("driver_status", driver_status);
                                    intent.putExtra("hideleftmarker", "yes");
                                    intent.putExtra("image",byteArray);
                                    startActivity(intent);

                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                }
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });



            }
//                    ----------------------Intent to Next Class-----------------------
        }
    }

    private void Initialize() {
        cd = new ConnectionDetector(SelectVehicleCategoryPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(SelectVehicleCategoryPage.this);
        driver_list = new ArrayList<BookingPojo1>();
        catetorylist = new ArrayList<NewCategoryListPojo>();

        TextView tv_youare = (TextView)findViewById(R.id.tv_youare);
        tv_youare.setText(getkey("action_text_currentarea_location"));

        TextView tv_selectvehi = (TextView)findViewById(R.id.tv_selectvehi);
        tv_selectvehi.setText(getkey("select_vehicle_lable"));

        HashMap<String, String> info = session.getUserDetails();
        CategoryID = info.get(SessionManager.KEY_CATEGORY);
        UserID = info.get(SessionManager.KEY_USERID);
        UserName = info.get(SessionManager.KEY_USERNAME);

        rl_pickup_address_changinglayout = findViewById(R.id.book_my_ride_pickup_address_layout1);
        tv_pickup_address = findViewById(R.id.book_my_ride_pickup_address_textView);
        catetorylist = new ArrayList<NewCategoryListPojo>();
        listview = findViewById(R.id.rc_vehicles);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        listview.setLayoutManager(linearLayoutManager);
        listview.setItemAnimator(new DefaultItemAnimator());

//        listview = (ExpandableHeightListView) findViewById(R.id.listview);
        cancel_layout = (ImageView) findViewById(R.id.cancel_layout);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        registerReceiver(refreshReceiver, intentFilter);

        Intent intent = getIntent();
        if (intent.hasExtra("intentCall")) {
            intentcall = intent.getStringExtra("intentCall");
            SpickupLocation = intent.getStringExtra("pickupAddress");
            tv_pickup_address.setText(SpickupLocation);
            SpickupLatitude = intent.getStringExtra("pickupLatitude");
            SpickupLongitude = intent.getStringExtra("pickupLongitude");
            SdestinationLocation = intent.getStringExtra("destinationAddress");
            SdestinationLatitude = intent.getStringExtra("destinationLatitude");
            SdestinationLongitude = intent.getStringExtra("destinationLongitude");
            Str_Page = intent.getStringExtra("page");
        }

        if (isInternetPresent) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", UserID);
            jsonParams.put("lat", SpickupLatitude);
            jsonParams.put("lon", SpickupLongitude);
            jsonParams.put("category", CategoryID);
            PostRequest(Iconstant.get_cabs_url, jsonParams);
        } else {
            Alert(getkey("action_error"),getkey("alert_nointernet"));
        }

    }


    //---------------------------------------------------------------
    private void PostRequest(String Url, HashMap<String, String> jsonParams) {
        dialog1 = new Dialog(SelectVehicleCategoryPage.this);
        dialog1.getWindow();
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.custom_loading);

        dialog1.setCanceledOnTouchOutside(false);
        dialog1.show();

        System.out.println("-----------BookingPage1 Url--------------" + Url);


        System.out.println("-----------BookingPage1 jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(SelectVehicleCategoryPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------BookingPage1 reponse-------------------" + response);

                String Sstatus = "", Smessage = "", Savailable_count = "", Sride_min = "", ScurrencySymbol = "", Sprofile_complete = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject jobject = object.getJSONObject("response");
                        if (jobject.length() > 0) {
                            Savailable_count = jobject.getString("available_count");
                            Sprofile_complete = jobject.getString("profile_complete");

                            Object check_driver_object = jobject.get("drivers");
                            if (check_driver_object instanceof JSONArray) {

                                JSONArray driver_array = jobject.getJSONArray("drivers");
                                if (driver_array.length() > 0) {
                                    driver_list.clear();

                                    for (int j = 0; j < driver_array.length(); j++) {
                                        JSONObject driver_object = driver_array.getJSONObject(j);

                                        BookingPojo1 pojo = new BookingPojo1();
                                        if (driver_object.has("distance")) {
                                            pojo.setDistance(driver_object.getString("distance"));
                                        }
                                        if (driver_object.has("brand_image")) {
                                            pojo.setBrand_image(driver_object.getString("brand_image"));
                                        }


                                        driver_list.add(pojo);
                                    }
                                    driver_status = true;
                                } else {
                                    driver_list.clear();
                                    driver_status = false;
                                }
                            } else {
                                driver_status = false;
                            }


                            Object check_catgory_object = jobject.get("category");

                            if (check_catgory_object instanceof JSONArray) {


                                JSONArray category_array = jobject.getJSONArray("category");

                                if (category_array.length() > 0) {

                                    catetorylist.clear();


                                    for (int m = 0; m < category_array.length(); m++) {

                                        JSONObject category_object = category_array.getJSONObject(m);
                                        NewCategoryListPojo cate_pojo = new NewCategoryListPojo();
                                        cate_pojo.setCategory_Id(category_object.getString("id"));
                                        cate_pojo.setCategory_name(category_object.getString("name"));
                                        cate_pojo.setCat_eta(category_object.getString("eta"));
                                        cate_pojo.setMax_person(category_object.getString("no_of_seats"));
                                        cate_pojo.setCat_type(category_object.getString("type"));
                                        cate_pojo.setCat_normal_img(category_object.getString("icon_normal"));
                                        cate_pojo.setCat_active_img(category_object.getString("icon_active"));
                                        cate_pojo.setCar_image(category_object.getString("icon_car_image"));
                                        cate_pojo.setOffer_type(category_object.getString("offer_type"));
                                        cate_pojo.setDelivery_des(category_object.getString("description"));
                                        cate_pojo.setStr_free_waittime(category_object.getString("free_waiting_time"));
                                        cate_pojo.setStr_wait_time_person(category_object.getString("waiting_time_per"));
                                        cate_pojo.setStrOfferType(category_object.getString("vehicle_type"));


                                        catetorylist.add(cate_pojo);

                                    }

                                    isCategoryDataAvailable = true;

                                } else {
                                    isCategoryDataAvailable = false;
                                }


                            } else {

                                isCategoryDataAvailable = false;

                            }


                            dialogDismiss1();
                        }

                    } else {
                        Smessage = object.getString("response");
                        dialogDismiss1();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss1();
                }
                dialogDismiss1();

                if (Sstatus.equalsIgnoreCase("1")) {


                    if (isCategoryDataAvailable) {

                        CatetoryAdapter = new SelectVehicleAdapter(SelectVehicleCategoryPage.this, catetorylist, vehicle_inf);
                        listview.setAdapter(CatetoryAdapter);
                    }


                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss1();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(SelectVehicleCategoryPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void AlertDelivaryMotorBike(String alert, final byte[] byteArray) {

        final Dialog dialog = new Dialog(SelectVehicleCategoryPage.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bookingmotorbike_alert);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button sendNow = (Button) dialog.findViewById(R.id.custom_dialog_library_cancel_button);

        CustomTextView userNameTv = (CustomTextView) dialog.findViewById(R.id.custom_dialog_library_message_textview);
        userNameTv.setText(alert);
        ImageView closePopup = (ImageView) dialog.findViewById(R.id.close_popup);
        closePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        sendNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (intentcall.equalsIgnoreCase("findplaceBooking")) {

                    Intent intent = new Intent(SelectVehicleCategoryPage.this, BookingPage3.class);
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                    intent.putExtra("destinationAddress", SdestinationLocation);
                    intent.putExtra("destinationLatitude", SdestinationLatitude);
                    intent.putExtra("destinationLongitude ", SdestinationLongitude);
                    intent.putExtra("CategoryID", Category_Selected_id);
                    intent.putExtra("driver_status", driver_status);
                    intent.putExtra("page", "BookingPage1");
                    intent.putExtra("hideleftmarker", "yes");
                    intent.putExtra("image",byteArray);
                    startActivity(intent);

                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Intent intent = new Intent(SelectVehicleCategoryPage.this, BookingPage3.class);
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                    intent.putExtra("destinationAddress", "");
                    intent.putExtra("destinationLatitude", "");
                    intent.putExtra("destinationLongitude", "");
                    intent.putExtra("CategoryID", Category_Selected_id);
                    intent.putExtra("page", "BookingPage1");
                    intent.putExtra("driver_status", driver_status);
                    intent.putExtra("hideleftmarker", "yes");
                    intent.putExtra("image",byteArray);
                    startActivity(intent);

                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                dialog.dismiss();

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
















    }

    //--------------Alert Method-----------
    private void AlertDelivary(int title, String alert, final byte[] byteArray) {

        final PkDialog mDialog = new PkDialog(SelectVehicleCategoryPage.this);
        mDialog.setTitleImage(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentcall.equalsIgnoreCase("findplaceBooking")) {
                    Intent intent = new Intent(SelectVehicleCategoryPage.this, BookingPage3.class);
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                    intent.putExtra("destinationAddress", SdestinationLocation);
                    intent.putExtra("destinationLatitude", SdestinationLatitude);
                    intent.putExtra("destinationLongitude ", SdestinationLongitude);
                    intent.putExtra("CategoryID", Category_Selected_id);
                    intent.putExtra("driver_status", driver_status);
                    intent.putExtra("page", "BookingPage1");
                    intent.putExtra("hideleftmarker", "yes");
                    intent.putExtra("image",byteArray);
                    startActivity(intent);

                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Intent intent = new Intent(SelectVehicleCategoryPage.this, BookingPage3.class);
                    intent.putExtra("pickupAddress", SpickupLocation);
                    intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                    intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));
                    intent.putExtra("destinationAddress", "");
                    intent.putExtra("destinationLatitude", "");
                    intent.putExtra("destinationLongitude", "");
                    intent.putExtra("CategoryID", Category_Selected_id);
                    intent.putExtra("page", "BookingPage1");
                    intent.putExtra("driver_status", driver_status);
                    intent.putExtra("hideleftmarker", "yes");
                    intent.putExtra("image",byteArray);
                    startActivity(intent);

                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss1() {
        if (dialog1 != null) {
            dialog1.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SelectVehicleCategoryPage.this, Navigation_new.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }



    //-------------------AsynTask To get the current Address----------------
    private void PostRequestCarImage(String Url, String latitude, String longitude) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);//"57c6a827cae2aa580c00002d");
        jsonParams.put("lat", latitude);
        jsonParams.put("lon", longitude);
        jsonParams.put("category", Category_Selected_id);
        jsonParams.put("mode", "oneway");
        System.out.println("--------------Book My ride jsonParams-------------------" + jsonParams);
        mRequest = new ServiceRequest(SelectVehicleCategoryPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String status = "";
                System.out.println("--------------Book My ride reponse-------------------" + response);
                try
                {
                    JSONObject object = new JSONObject(response);
                    //dialogDismiss();
                }
                catch (JSONException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                  //  dialogDismiss();
                }

            }

            @Override
            public void onErrorListener() {
               // dialogDismiss();
            }
        });
    }

    private int getResId(String drawableName) {

        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }

    private void CloseKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

       /* View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }*/
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    private void openagain(final int position)
    {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SelectVehicleCategoryPage.this);
        View view = getLayoutInflater().inflate(R.layout.dispatchpopup, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCancelable(true);
        final TextView headings = (TextView)view.findViewById(R.id.headings);
        headings.setText(catetorylist.get(position).getCategory_name().toUpperCase());
        final TextView subtitle = (TextView)view.findViewById(R.id.subtitle);
        subtitle.setText(mhelper.getvalueforkey("standard_dispatch_van_size_1_60cm_x_1_50cm_and_100cm_height_please_makesure_your_items_not_more_than_standard_size_in_total"));
        final TextView receiver_name = (TextView)view.findViewById(R.id.receiver_name);
        receiver_name.setText(mhelper.getvalueforkey("receiver_name"));
        final TextView mobile_noo = (TextView)view.findViewById(R.id.mobile_noo);
        mobile_noo.setText(mhelper.getvalueforkey("dispatchmobile_no"));
        final TextView itemqty = (TextView)view.findViewById(R.id.itemqty);
        itemqty.setText(mhelper.getvalueforkey("items_quantity"));
        final TextView items_description = (TextView)view.findViewById(R.id.items_description);
        items_description.setText(mhelper.getvalueforkey("items_description"));
        final TextView agreeeme = (TextView)view.findViewById(R.id.agreeeme);
        agreeeme.setText(mhelper.getvalueforkey("agreed_with_the_terms_and_conditions_here"));
        final TextView fullsage = (TextView)view.findViewById(R.id.fullsage);
        fullsage.setText(mhelper.getvalueforkey("probb"));

        final ImageView closedialog = (ImageView)view.findViewById(R.id.closedialog);
        closedialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });

        final EditText itemdeshint = (EditText)view.findViewById(R.id.itemdeshint);
        itemdeshint.setHint(mhelper.getvalueforkey("eg_documents"));
        final EditText receivernameedit = (EditText)view.findViewById(R.id.receivernameedit);
        final EditText edit_mobilenumber = (EditText)view.findViewById(R.id.edit_mobilenumber);
        setupFullHeight(bottomSheetDialog);

        final Spinner chooseitem = (Spinner)view.findViewById(R.id.chooseitem);
        String[] country = { "1 item", "2 item", "3 item"};
        ArrayAdapter aa = new ArrayAdapter(SelectVehicleCategoryPage.this,android.R.layout.simple_spinner_item,country);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        chooseitem.setAdapter(aa);


        final TextView cancel = (TextView)view.findViewById(R.id.cancel);
        final TextView submit = (TextView)view.findViewById(R.id.submit);
        submit.setText(mhelper.getvalueforkey("confirm"));
        cancel.setText(mhelper.getvalueforkey("cancel"));
        final TextView txt_country_code = (TextView)view.findViewById(R.id.txt_country_code);

        final ImageView flag = (ImageView)view.findViewById(R.id.flag);
        flag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                txt_country_code.setText(dialCode);
                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                flag.setImageResource(getResId(drawableName));
                CloseKeyBoard();
            }
        });

        final CheckBox chea = (CheckBox)view.findViewById(R.id.chea);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(receivernameedit.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(), mhelper.getvalueforkey("recmand"),Toast.LENGTH_LONG).show();
                }
                else if(edit_mobilenumber.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(), mhelper.getvalueforkey("mobile_numinavalid"),Toast.LENGTH_LONG).show();
                }
                else if(itemdeshint.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(), mhelper.getvalueforkey("itemdesempty"),Toast.LENGTH_LONG).show();
                }
                else if(!chea.isChecked())
                {
                    Toast.makeText(getApplicationContext(), mhelper.getvalueforkey("agreed_wsith_the_terms_and_conditions_here"),Toast.LENGTH_LONG).show();
                }
                else
                {
                    bottomSheetDialog.dismiss();
                    selectmovepage(position);
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });

        bottomSheetDialog.show();
    }
}
