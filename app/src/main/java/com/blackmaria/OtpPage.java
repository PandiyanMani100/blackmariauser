package com.blackmaria;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.Settings;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;
import com.blackmaria.pojo.loginresponse;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ChatAvailabilityCheck;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CircularImageView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.xmpp.Getlocation;
import com.blackmaria.xmpp.XmppService;
import com.devspark.appmsg.AppMsg;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

public class OtpPage extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private LinearLayout closekey;
    private LinearLayout Bt_Enter;
    private ImageView Tv_HeaderText;
    TextView changenumber,txt_go;
    RelativeLayout tv_referalltext;
    private RelativeLayout Tv_ChangeNumber;
    private SessionManager session;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private String Sphone = "", ScountryCode = "", SdeviceToken = "", SgcmId = "", ReferalStatus = "", accountExisit = "", userId = "";
    private String Sotp_Status = "", Sotp = "";
    char[] charArray;
    private ServiceRequest mRequest;

    private RelativeLayout Rl_otp;
    //    private String Spage = " ", SuserId = "";
    private String SuserId = "", Suser_name = "", Spage = " ", Suser_image = "", Sprofile_complete = "",
            Scountry_code = "", SphoneNo = "", Scategory = "",
            SsecretKey = "", gender = "", SwalletAmount = "", ScurrencyCode = "", username = "", email = "";
    String sCurrencySymbol = "";

    String gcmId = "";
    String referalCode = "";
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    private boolean isAppInfoAvailable = false;
    String sLatitude = "", sLongitude = "", app_identity_name = "", Language_code = "", profile_complete = "", userRating = "", phone_masking_status = "";
    private GPSTracker gpsTracker;
    AppInfoSessionManager appInfo_Session;

    private SessionManager sessionManager;
    String referalCodeUser = "", phoneNumberReferal = "";
    String termsCondition = "";
    PinEntryEditText Ed_pin;

    double MyCurrent_lat = 0.0;
    double MyCurrent_long = 0.0;
    Location mLocation;
    TextView mobilenumbersentto;

    private long UPDATE_INTERVAL = 15000;  /* 15 secs */
    private long FASTEST_INTERVAL = 5000; /* 5 secs */
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    Handler handler_serlocationuser;
    LanguageDb mhelper;

    //    EditText et_otpval;
    TextView Bt_Resend,verficationtext, bt_callme, tv_contentloading, tvpleasewait, tv_authenticate, tv_countreduce, tv_resendview, tv_resend, tv_callme;
    private SmoothProgressBar progressbar;
    CountDownTimer cTimer = null;

    private loginresponse loginpojo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_layout);
        mhelper= new LanguageDb(this);
        startService(new Intent(this, Getlocation.class));
        initialize();
        //FirstTimeLoginCheck();
        Bt_Enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = Ed_pin.getText().toString().trim();

                if (!"Profile".equalsIgnoreCase(Spage)) {
                    if (otp.length() == 0) {
                        AlertError(getkey("alert_label_title"), getkey("otp_label_alert_otp"));

                    } else if (!Sotp.equals(otp)) {
                        AlertError(getkey("alert_label_title"), getkey("otp_label_alert_invalid"));

                    } else {
                        cd = new ConnectionDetector(OtpPage.this);
                        isInternetPresent = cd.isConnectingToInternet();

                        if (isInternetPresent) {
                            if (getIntent().hasExtra("registeration")) {
                                progressbar.setVisibility(View.VISIBLE);
                                PostRequest(Iconstant.register_otp_url);
                            } else {
                                cancelTimer();
                                clickloginafterrespose_visible();
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        otpcheck();
                                    }
                                }, 2000);

                            }


                            System.out.println("otpregister---------" + Iconstant.register_otp_url);

                        } else {
                            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                        }
                    }
                } else {
                    if (otp.length() == 0) {
                        AlertError(getkey("alert_label_title"),getkey("otp_label_alert_otp"));
                       } else if (!Sotp.equals(otp)) {
                        AlertError(getkey("alert_label_title"),getkey("otp_label_alert_invalid"));
                        } else {
                        cd = new ConnectionDetector(OtpPage.this);
                        isInternetPresent = cd.isConnectingToInternet();

                        if (isInternetPresent) {
//                            PostRequest_mobnochange_otp(Iconstant.Edit_profile_mobileNo_url);
//                            System.out.println("otpregister---------" + Iconstant.register_otp_url);

                        } else {
                            Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                        }
                    }
                }
            }
        });

        bt_callme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getCustomerServiceNo()));
                startActivity(intent);
            }
        });
        Bt_Resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(OtpPage.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    PostRequest_resendOtp(Iconstant.register_otp_resend_url);
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }

            }
        });
//        Tv_ChangeNumber.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               /* Intent intent = new Intent(OtpPage.this, LoginPage.class);
//                startActivity(intent);*/
//                finish();
//                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//            }
//        });
//        tv_referalltext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showReferralAlert();
//                // ReferalPopUp();
//            }
//        });
//        closekey.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//                return false;
//            }
//        });

    }

    private void showReferralAlert() {
        //--------Adjusting Dialog width-----

        final Dialog referralDialog = new Dialog(OtpPage.this, R.style.DialogAnimation2);
        referralDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        referralDialog.setContentView(R.layout.alert_enter_referral_code);


        final EditText Ed_referral = (EditText) referralDialog.findViewById(R.id.edt_referral_code);
        final Button Btn_confirm = (Button) referralDialog.findViewById(R.id.btn_confirm);
        final ImageView Iv_close = (ImageView) referralDialog.findViewById(R.id.img_close);
        Ed_referral.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Ed_referral.setCursorVisible(true);
                return false;
            }
        });
        Btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referalCodeUser = Ed_referral.getText().toString().trim();


                if (referalCodeUser.length() > 0) {
                    Ed_referral.setError(null);
                    if (isInternetPresent) {
                        if (Ed_referral.getText().toString().equalsIgnoreCase("")) {
                            AlertError(getkey("alert"),getkey("login_page_alert_referal"));
                        } else {
                            // close keyboard
                            InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            mgr.hideSoftInputFromWindow(Ed_pin.getWindowToken(), 0);
                            PostRequest_referal(Iconstant.referal_url);
                            System.out.println("referal---------" + Iconstant.referal_url);

                        }
                    } else {
                        Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                    }
                    referralDialog.dismiss();
                } else {
                    AlertError(getkey("alert"), getkey("otp_page_referral_error"));

                }

            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referralDialog.dismiss();
            }
        });

        referralDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Ed_referral.setCursorVisible(false);
                tv_referalltext.setClickable(true);
                CloseKeyboardNew();
            }
        });

        Ed_referral.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(Ed_referral);
                }
                return false;
            }
        });
        tv_referalltext.setClickable(false);
        referralDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        referralDialog.show();


    }


    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initialize() {
        handler_serlocationuser = new Handler();
        loginpojo = new loginresponse();
        sessionManager = new SessionManager(OtpPage.this);
        sessionManager.setfirstimelogin("1");
        session = new SessionManager(OtpPage.this);
        cd = new ConnectionDetector(OtpPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        gpsTracker = new GPSTracker(OtpPage.this);
        appInfo_Session = new AppInfoSessionManager(OtpPage.this);
//        tv_referalltext = (RelativeLayout) findViewById(R.id.tv_referalltext);
//        closekey = (LinearLayout) findViewById(R.id.closekey);
//        ImageView Rl_drawer = (ImageView) findViewById(R.id.img_light);
//        final Animation animation = new AlphaAnimation(1, 0);
//        animation.setDuration(500);
//        animation.setInterpolator(new LinearInterpolator());
//        animation.setRepeatCount(Animation.INFINITE);
//        animation.setRepeatMode(Animation.REVERSE);
//        Rl_drawer.startAnimation(animation);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {
            sLatitude = String.valueOf(gpsTracker.getLatitude());
            sLongitude = String.valueOf(gpsTracker.getLongitude());
        } else {
            dialogDismiss();
            enableGpsService();
        }

        Intent intent = getIntent();
        if (intent.hasExtra("page")) {
            SuserId = intent.getStringExtra("userId");
            ScountryCode = intent.getStringExtra("CountryCode");
            Sphone = intent.getStringExtra("Phone");
            session.setMobileNumberUpdate(ScountryCode, Sphone);
            Spage = intent.getStringExtra("page");
            Sotp_Status = intent.getStringExtra("Otp_Status");
            Sotp = intent.getStringExtra("Otp");

        } else if (intent.hasExtra("registeration")) {
            Sotp_Status = intent.getStringExtra("Otp_Status");
            Sotp = intent.getStringExtra("Otp");
            ReferalStatus = intent.getStringExtra("ReferalStatus");
            referalCode = intent.getStringExtra("rcode");;
            Sphone = intent.getStringExtra("Phone");
            ScountryCode = intent.getStringExtra("CountryCode");
            SgcmId = intent.getStringExtra("GcmID");
            SdeviceToken = intent.getStringExtra("SdeviceToken");
            username = intent.getStringExtra("username");
            email = intent.getStringExtra("email");
            session.setMobileNumberUpdate(ScountryCode, Sphone);
        } else if (intent.hasExtra("loginresponse")) {
            SdeviceToken = getIntent().getStringExtra("SdeviceToken");
            loginpojo = (loginresponse) getIntent().getSerializableExtra("loginresponse");

            Sotp_Status = loginpojo.getOtp_status();
            Sotp = loginpojo.getOtp();
            SuserId = loginpojo.getUser_id();
            ReferalStatus = loginpojo.getReferral_staus();
            Sphone = loginpojo.getPhone_number();
            ScountryCode = loginpojo.getCountry_code();
            SgcmId = loginpojo.getKey();
            username = loginpojo.getUser_name();
            email = loginpojo.getEmail();
        }

//        et_otpval = findViewById(R.id.edt_otp_no);
//        tv_count = findViewById(R.id.tv_countreduce);
        changenumber= findViewById(R.id.changenumber);
        Bt_Enter = findViewById(R.id.lv_signup);
        txt_go = findViewById(R.id.txt_go);
        txt_go.setText(getkey("confirm"));

        TextView resendin = findViewById(R.id.resendin);
        resendin.setText(getkey("resend_in"));
        TextView secondss = findViewById(R.id.secondss);
        secondss.setText(getkey("seconds"));




        verficationtext= findViewById(R.id.verficationtext);
        verficationtext.setText(getkey("verification"));
        Bt_Resend = findViewById(R.id.tv_resend);
        Bt_Resend.setText(getkey("resend_otp"));
        bt_callme = findViewById(R.id.tv_callme);
        bt_callme.setText(getkey("call_me"));
//        tv_contentloading = findViewById(R.id.loading_content);
//        tvpleasewait = findViewById(R.id.tvpleasewait);
//        tv_authenticate = findViewById(R.id.tv_authenticate);
        tv_countreduce = findViewById(R.id.tv_countreduce);
        tv_resendview = findViewById(R.id.tv_resendview);
        mobilenumbersentto= findViewById(R.id.mobilenumbersentto);

        mobilenumbersentto.setText(ScountryCode+Sphone);

        changenumber.setText(getkey("changenumber"));
        changenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        progressbar = (SmoothProgressBar) findViewById(R.id.loading);


        Ed_pin = (PinEntryEditText) findViewById(R.id.edt_withdrawal_amount);

        otpDataMethod();
    }

    public void startTimer() {
        Bt_Resend.setVisibility(View.INVISIBLE);
        bt_callme.setVisibility(View.INVISIBLE);
        cTimer = new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                tv_countreduce.setText(String.valueOf(millisUntilFinished / 1000));
                Bt_Resend.setClickable(false);

            }

            public void onFinish() {
                tv_countreduce.setText("0");
                Bt_Resend.setClickable(true);
                Bt_Resend.setVisibility(View.VISIBLE);
                bt_callme.setVisibility(View.VISIBLE);
            }
        };
        cTimer.start();
    }

    //cancel timer
    public void cancelTimer() {
        if (cTimer != null)
            cTimer.cancel();
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }


    private void otpDataMethod() {

        if (Sotp_Status.equalsIgnoreCase("development")) {
            Ed_pin.setText(Sotp);
            startTimer();
        } else {
            startTimer();
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        startLocationUpdates();
    }


    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
          }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!checkPlayServices()) {
          }

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else
                finish();

            return false;
        }
        return true;
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(OtpPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void PostRequest_referal(String referal_url) {
        final Dialog dialog = new Dialog(OtpPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_verifying"));
        System.out.println("--------------Referal url-------------------" + referal_url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("country_code", ScountryCode);
        jsonParams.put("phone_number", Sphone);
        jsonParams.put("code", referalCodeUser);
        System.out.println("------Referal jsonParams---------" + jsonParams);

        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(referal_url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------Referal reponse-------------------" + response);
                String Sstatus = "", Smessage = "", userName = "", userImage = "", refPer = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");
//                        referalCode = referalCodeUser;
                        userName = object.getString("user_name");
                        refPer = object.getString("ref_per");
                        userImage = object.getString("user_image");

                        showVerifiedAlert(Smessage, userName, userImage, refPer);

                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    } else {
                        Smessage = object.getString("response");
                        referalCodeUser = "";
                        AlertError(getkey("alert"), Smessage);

                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Ed_pin.getWindowToken(), 0);
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void showVerifiedAlert(String message, String userName, String userImage, String rafPer) {

        //--------Adjusting Dialog width and height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 80% of the screen

        final Dialog verifiedDialog = new Dialog(OtpPage.this, R.style.DialogAnimation2);
        verifiedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifiedDialog.setContentView(R.layout.alert_verified_status);
        verifiedDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifiedDialog.getWindow().setLayout(screenWidth, screenHeight);

        final CircularImageView Iv_profile = (CircularImageView) verifiedDialog.findViewById(R.id.img_profile1);
        final TextView Tv_driverName = (TextView) verifiedDialog.findViewById(R.id.txt_driver_name);
        final TextView Tv_content1 = (TextView) verifiedDialog.findViewById(R.id.txt_content1);
        final TextView Tv_earnPercentage = (TextView) verifiedDialog.findViewById(R.id.txt_earn_percentage);
        final TextView txt_label_earn_percentage = (TextView) verifiedDialog.findViewById(R.id.txt_label_earn_percentage);
        final Button Btn_proceed = (Button) verifiedDialog.findViewById(R.id.btn_proceed);
        final ImageView Iv_close = (ImageView) verifiedDialog.findViewById(R.id.img_close);
        Btn_proceed.setText(getkey("withdrawal_page_label_proceed"));
        final TextView txt_label_verified_status = (TextView) verifiedDialog.findViewById(R.id.txt_label_verified_status);
        txt_label_verified_status.setText(getkey("verified_success"));
        final TextView subte = (TextView) verifiedDialog.findViewById(R.id.subte);
        subte.setText(getkey("referral_verified_content3"));

        final TextView txt_label_cloud_money = (TextView) verifiedDialog.findViewById(R.id.txt_label_cloud_money);
        txt_label_cloud_money.setText(getkey("referral_verified_cloud_money"));

        //  Tv_content1.setText(userName);
        Picasso.with(OtpPage.this).load(String.valueOf(userImage)).into(Iv_profile);
        Tv_driverName.setText(userName);
        txt_label_earn_percentage.setText(userName + " " + getkey("referal_info1"));
        Tv_earnPercentage.setText(rafPer + "%");
        Tv_content1.setText(userName + " " + getkey("referal_info"));
        Btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (accountExisit.equalsIgnoreCase("1")) {
                    PostRequest_referal1(Iconstant.referal_code_submit_url);
                    System.out.println("referal---------" + Iconstant.referal_url);
                    verifiedDialog.dismiss();
                } else {
                    tv_referalltext.setVisibility(View.VISIBLE);
                    verifiedDialog.dismiss();
                }


            }
        });
        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifiedDialog.dismiss();
            }
        });
        verifiedDialog.show();

    }

    private void PostRequest_referal1(String referal_url) {
        final Dialog dialog = new Dialog(OtpPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_verifying"));
        System.out.println("--------------ReferalSubmit  url-------------------" + referal_url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", SuserId);
        jsonParams.put("referral_code", referalCodeUser);
        System.out.println("------Referal jsonParams---------" + jsonParams);

        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(referal_url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------Referal reponse-------------------" + response);
                String Sstatus = "", Smessage = "", userName = "", userImage = "", refPer = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        tv_referalltext.setVisibility(View.GONE);
                        Smessage = object.getString("response");
                        Alert(getkey("action_success"), Smessage);
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    } else {
                        Smessage = object.getString("response");
                        referalCodeUser = "";
                        Alert(getkey("alert"), Smessage);
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void otpcheck() {
        session.setReferalStatus(loginpojo.getReferral_staus());
        session.setCurrency(loginpojo.getCurrency());

        if (loginpojo.getError_status().equalsIgnoreCase("0")) {

            session.createLoginSession("", SuserId, loginpojo.getUser_name(), "", loginpojo.getCountry_code(), loginpojo.getPhone_number(), "", loginpojo.getCategory(), loginpojo.getKey());
//            session.createWalletAmount(loginpojo.getCurrency() + loginpojo.get);
            session.setXmppKey(SuserId, loginpojo.getSec_key());
            if (loginpojo.getTerms_condition().equalsIgnoreCase("0")) {
                clickloginafterrespose_invisible();
                FirstTimeLoginCheck();
            } else {
                clickloginafterrespose_invisible();
                postRequest_applaunch(Iconstant.getAppInfo);
            }
        } else {
            clickloginafterrespose_invisible();
            final PkDialog mDialog = new PkDialog(OtpPage.this);
            mDialog.setDialogTitle(getkey("action_error"));
            mDialog.setDialogMessage(loginpojo.getError_message());
            mDialog.setCancelOnTouchOutside(false);
            mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();
        }

    }

    private void PostRequest(String Url) {
        cancelTimer();
        clickloginafterrespose_visible();

        SdeviceToken = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        System.out.println("--------------Otp url-------------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", email);
        jsonParams.put("user_name", username);
        jsonParams.put("phone_number", Sphone);
        jsonParams.put("country_code", ScountryCode);
        jsonParams.put("deviceToken", SdeviceToken);
        jsonParams.put("gcm_id", FirebaseInstanceId.getInstance().getToken());
        jsonParams.put("referral_code", referalCode);
        jsonParams.put("lat", sLatitude);
        jsonParams.put("lon", sLongitude);
        jsonParams.put("image", session.getUserImageName());
        System.out.println("------otp page jsonParams---------" + jsonParams);


        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Otpreponse-------------------" + response);
                progressbar.setVisibility(View.GONE);
                String Sstatus = "", Smessage = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (object.has("response")) {
                        Smessage = object.getString("response");
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {
                        SuserId = object.getString("user_id");
                        Scountry_code = object.getString("country_code");
                        SphoneNo = object.getString("phone_number");
                        SsecretKey = object.getString("sec_key");
                        gcmId = object.getString("key");
                        Sprofile_complete = object.getString("profile_complete");
                        Suser_name = object.getString("user_name");
                        Scategory = object.getString("category");
                        ScurrencyCode = object.getString("currency");
                        SwalletAmount = object.getString("wallet_amount");
                        termsCondition = object.getString("terms_condition");

                        if (object.has("referral_staus")) {
                            session.setReferalStatus(object.getString("referral_staus"));
                        }
                        if (object.has("gender")) {
                            gender = object.getString("gender");
                            session.setGender(gender);
                        }
                        try {
                            sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode);
                            session.setCurrency(sCurrencySymbol);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    System.out.println("--------SsecretKey-----------" + SsecretKey);
                    session.createLoginSession("", SuserId, Suser_name, "", Scountry_code, SphoneNo, "", Scategory, gcmId);
                    session.createWalletAmount(sCurrencySymbol + SwalletAmount);
                    session.setXmppKey(SuserId, SsecretKey);
                    if (termsCondition.equalsIgnoreCase("0")) {
                        clickloginafterrespose_invisible();
                        FirstTimeLoginCheck();
                    } else {
                        clickloginafterrespose_invisible();
                        postRequest_applaunch(Iconstant.getAppInfo);
                    }
                } else {
                    clickloginafterrespose_invisible();
                    final PkDialog mDialog = new PkDialog(OtpPage.this);
                    mDialog.setDialogTitle(getkey("action_error"));
                    mDialog.setDialogMessage(Smessage);
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });
                    mDialog.show();
                }
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Ed_pin.getWindowToken(), 0);
            }

            @Override
            public void onErrorListener() {
                progressbar.setVisibility(View.GONE);
                dialogDismiss();
            }
        });
    }

    private void clickloginafterrespose_invisible() {
        if (progressbar.getVisibility() == View.VISIBLE) {
            progressbar.setVisibility(View.INVISIBLE);
//            tv_contentloading.setVisibility(View.INVISIBLE);
//            tvpleasewait.setVisibility(View.INVISIBLE);
//            tv_authenticate.setVisibility(View.INVISIBLE);
        }
    }

    private void clickloginafterrespose_visible() {
        if (progressbar.getVisibility() == View.INVISIBLE || progressbar.getVisibility() == View.GONE) {
            progressbar.setVisibility(View.VISIBLE);
//            tv_contentloading.setVisibility(View.VISIBLE);
//            tvpleasewait.setVisibility(View.VISIBLE);
//            tv_authenticate.setVisibility(View.VISIBLE);
        }
    }

    private void resendotp_visible() {
        if (tv_countreduce.getVisibility() == View.INVISIBLE) {
            tv_countreduce.setVisibility(View.VISIBLE);
            tv_resendview.setVisibility(View.VISIBLE);
        }/*else{
            tv_countreduce.setVisibility(View.INVISIBLE);
            tv_resendview.setVisibility(View.INVISIBLE);
        }*/
    }

    private void resendotp_invisible() {
        if (tv_countreduce.getVisibility() == View.VISIBLE) {
            tv_countreduce.setVisibility(View.INVISIBLE);
            tv_resendview.setVisibility(View.INVISIBLE);
        }
    }


    private void PostRequest_resendOtp(String Url) {
        resendotp_visible();
        progressbar.setVisibility(View.VISIBLE);
//        startTimer();
        System.out.println("--------------Otp Resend url-------------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("phone_number", Sphone);
        jsonParams.put("country_code", ScountryCode);
        jsonParams.put("lat", String.valueOf(gpsTracker.getLatitude()));
        jsonParams.put("lon", String.valueOf(gpsTracker.getLongitude()));


//        jsonParams.put("deviceToken", SdeviceToken);
//        jsonParams.put("gcm_id", SgcmId);
        System.out.println("------otp v jsonParams---------" + jsonParams);


        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Otp Resend reponse-------------------" + response);


                String Sstatus = "", Smessage = "", errorStatus = "", errorMessage = "";

                String gcmId = "";

                try {
                    progressbar.setVisibility(View.INVISIBLE);
                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Sotp_Status = object.getString("otp_status");
                        Sotp = object.getString("otp");
                        errorStatus = object.getString("error_status");
                        errorMessage = object.getString("error_message");
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    if (errorStatus.equalsIgnoreCase("1")) {
//                        Bt_Resend.setVisibility(View.GONE);
                        Alert(getkey("action_error"), errorMessage);
                    } else {
                        otpDataMethod();
                    }
                } else {
                    Alert(getkey("action_error"), Smessage);
                }

//                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }



    //-----------------------------------------------------------------

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }


    private void dialogDismiss() {


        clickloginafterrespose_invisible();
    }


    private void CloseKeyboard(EditText edittext) {
        edittext.setCursorVisible(false);
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    private void postRequest_applaunch(String Url) {

        System.out.println("-------------Otp App Information Url----------------" + Url);
        HashMap<String, String> user = session.getUserDetails();
        SuserId = user.get(SessionManager.KEY_USERID);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "user");
        jsonParams.put("id", SuserId);
        jsonParams.put("lat", sLatitude);
        jsonParams.put("lon", sLongitude);
        System.out.println("-------------Otp App Information jsonParams----------------" + jsonParams);

        ServiceRequest mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Otp App Information Response----------------" + response);


                String Str_status = "", SuserImage = "", userGuide = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                sessionManager.setCustomerServiceNo(sCustomerServiceNumber);
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                                app_identity_name = info_object.getString("app_identity_name");
                              /*  sFacebookId = info_object.getString("facebook_app_id");
                                sGooglePlusId = info_object.getString("google_plus_app_id");
                                sPhoneMasking = info_object.getString("phone_masking_status");*/
                                Language_code = info_object.getString("lang_code");
                                profile_complete = info_object.getString("profile_complete");
                                phone_masking_status = info_object.getString("phone_masking_status");
                                String phantomcar = info_object.getString("phantom_car");
                                SuserImage = info_object.getString("user_image");
                                sessionManager.setPhantomcar(phantomcar);

                                termsCondition = info_object.getString("terms_condition");
                                /*server_mode = info_object.getString("server_mode");
                                site_mode = info_object.getString("site_mode");
                                site_string = info_object.getString("site_mode_string");
                                site_url = info_object.getString("site_url");*/
                                if (info_object.has("cashback_mode")) {
                                    session.setCashbackStatus(info_object.getString("cashback_mode"));
                                }

                                try {
                                    JSONArray jsonarray_emergendy_no = info_object.getJSONArray("emergencyNumbers");
                                    for (int i = 0; i <= jsonarray_emergendy_no.length() - 1; i++) {
                                        String value = jsonarray_emergendy_no.getJSONObject(i).getString("title");
                                        if (value.equalsIgnoreCase("Police")) {
                                            sessionManager.setPolice(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Fire")) {
                                            sessionManager.setFire(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Ambulance")) {
                                            sessionManager.setAmbulance(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        }
                                    }

                                } catch (Exception e) {

                                }


                                if (info_object.has("with_pincode")) {
                                    session.setSecurePin(info_object.getString("with_pincode"));
                                }
                                if (info_object.has("avg_review")) {
                                    userRating = info_object.getString("avg_review");
                                }
                                if (info_object.has("currency")) {
                                    String currency = info_object.getString("currency");
                                    session.setCurrency(currency);
                                }
                                if (info_object.has("user_guide")) {
                                    userGuide = info_object.getString("user_guide");
                                    session.setUserGuidePdf(userGuide);
                                }
                                if (info_object.has("ride_earn")) {

                                    session.setReferelStatus(info_object.getString("ride_earn"));
                                }

                                isAppInfoAvailable = true;
                            } else {
                                isAppInfoAvailable = false;
                            }

                           /* sPendingRideId= response_object.getString("pending_rideid");
                            sRatingStatus= response_object.getString("ride_status");*/

                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }
                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {

                        appInfo_Session.setAppInfo(sContact_mail, sCustomerServiceNumber, sSiteUrl, sXmppHostUrl, sHostName, sFacebookId, sGooglePlusId, "", "", "", "", "", app_identity_name, userRating, SuserImage);
                        HashMap<String, String> language = session.getLanaguage();



                        session.setXmpp(sXmppHostUrl, sHostName);
                        session.setAgent(app_identity_name);
                        session.setPhoneMaskingStatus(phone_masking_status);


                        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {
                            sLatitude = String.valueOf(gpsTracker.getLatitude());
                            sLongitude = String.valueOf(gpsTracker.getLongitude());
                        } else {
                            dialogDismiss();
                            enableGpsService();
                        }


                        if (MyCurrent_lat != 0.0) {
                            postRequest_SetUserLocation(Iconstant.setUserLocation);
                        } else {
                            removerunnable_setlocationuser();
                            handler_serlocationuser.postDelayed(runnable_setlocationuser, 2000);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
//                Toast.makeText(OtpPage.this, Iconstant.Url, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void postRequest_SetUserLocation(String Url) {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", SuserId);
        jsonParams.put("latitude", String.valueOf(MyCurrent_lat));
        jsonParams.put("longitude", String.valueOf(MyCurrent_long));


        System.out.println("-------------Otp SetUserLocation jsonParams----------------" + jsonParams);

        System.out.println("-------------Otp UserLocation Url----------------" + Url);
        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Otp UserLocation Response----------------" + response);

                String Str_status = "", sCategoryID = "", sTripProgress = "", sRideId = "", sRideStage = "";
                try {
                    JSONObject object = new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialogDismiss();

                if ("0".equalsIgnoreCase(loginpojo.getProfile_complete())) {
                    //Starting the xmpp service
                    if (!isMyServiceRunning(XmppService.class)) {
                        System.out.println("-----------OtpPage xmpp service start1---------");
                        startService(new Intent(OtpPage.this, XmppService.class));
                    }
                    ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(OtpPage.this, "available");
                    chatAvailability.postChatRequest();
//new
                    session.setUserloggedIn(true);
                    Intent intent = new Intent(OtpPage.this, Navigation_new.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                } else {

                    //Starting the xmpp service
                    if (!isMyServiceRunning(XmppService.class)) {
                        System.out.println("-----------OtpPage xmpp service start2---------");
                        startService(new Intent(OtpPage.this, XmppService.class));
                    }
                    ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(OtpPage.this, "available");
                    chatAvailability.postChatRequest();
                    Intent intent = new Intent(OtpPage.this, Navigation_new.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }

            }

            @Override
            public void onErrorListener() {

                dialogDismiss();

                if ("0".equalsIgnoreCase(loginpojo.getProfile_complete())) {
                    //Starting the xmpp service
                    if (!isMyServiceRunning(XmppService.class)) {
                        System.out.println("-----------OtpPage xmpp service start4---------");
                        startService(new Intent(OtpPage.this, XmppService.class));
                    }
                    ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(OtpPage.this, "available");
                    chatAvailability.postChatRequest();
                    Intent intent = new Intent(OtpPage.this, Navigation_new.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.putExtra("Register", "1");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


                } else {
                    //Starting the xmpp service
                    if (!isMyServiceRunning(XmppService.class)) {
                        System.out.println("-----------OtpPage xmpp service start3---------");
                        startService(new Intent(OtpPage.this, XmppService.class));
                    }
                    ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(OtpPage.this, "available");
                    chatAvailability.postChatRequest();
                    Intent intent = new Intent(OtpPage.this, Navigation_new.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
        });
    }

    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(OtpPage.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

//------------------------OnActivity Result-------------------------


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                session = new SessionManager(getApplicationContext());
                                gpsTracker = new GPSTracker(OtpPage.this);

                                HashMap<String, String> user = session.getUserDetails();
                                SuserId = user.get(SessionManager.KEY_USERID);

                                sLatitude = String.valueOf(gpsTracker.getLatitude());
                                sLongitude = String.valueOf(gpsTracker.getLongitude());
                                //  postRequest_applaunch(ServiceConstant.app_launching_url);
                                //  postRequest_AppInformation(Iconstant.app_info_url);
                                if (MyCurrent_lat != 0.0) {
                                    postRequest_SetUserLocation(Iconstant.setUserLocation);
                                } else {
                                    removerunnable_setlocationuser();
                                    handler_serlocationuser.postDelayed(runnable_setlocationuser, 2000);
                                }

                            }
                        }, 2000);
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        enableGpsService();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;


        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    Runnable runnable_setlocationuser = new Runnable() {
        @Override
        public void run() {
            if (MyCurrent_lat != 0.0) {
                postRequest_SetUserLocation(Iconstant.setUserLocation);
            } else {
                removerunnable_setlocationuser();
                handler_serlocationuser.postDelayed(runnable_setlocationuser, 2000);
            }
        }
    };


    public void removerunnable_setlocationuser() {
        if (runnable_setlocationuser != null) {
            handler_serlocationuser.removeCallbacks(runnable_setlocationuser);
        }
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }

    private void FirstTimeLoginCheck() {
        final Dialog dialog = new Dialog(OtpPage.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.firsttimelogin_alert);
        final RelativeLayout firsttime_login_terms_cbox_lyt, firsttime_login_privacy_cbox_lyt, firsttime_login_eula_lyt;

        firsttime_login_terms_cbox_lyt = (RelativeLayout) dialog.findViewById(R.id.firsttime_login_terms_cbox_lyt);
        firsttime_login_privacy_cbox_lyt = (RelativeLayout) dialog.findViewById(R.id.firsttime_login_privacy_cbox_lyt);
        firsttime_login_eula_lyt = (RelativeLayout) dialog.findViewById(R.id.firsttime_login_eula_lyt);

        final CheckBox terms = (CheckBox) dialog.findViewById(R.id.firsttime_login_terms_cbox);
        final CheckBox privacy = (CheckBox) dialog.findViewById(R.id.firsttime_login_privacy_cbox);
        final CheckBox eula = (CheckBox) dialog.findViewById(R.id.firsttime_login_eula_cbox);

        final TextView termssservice = (TextView) dialog.findViewById(R.id.termssservice);
        termssservice.setText(getkey("firsttime_login_terms"));

        final TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(getkey("firsttime_login_text3"));

        System.out.println("firsttime_login_text3--->"+getkey("firsttime_login_text3"));


        final TextView privacypolicies = (TextView) dialog.findViewById(R.id.privacypolicies);
        privacypolicies.setText(getkey("firsttime_login_privacy"));
        final TextView eulla = (TextView) dialog.findViewById(R.id.eulla);
        eulla.setText(getkey("firsttime_login_eula"));

        RelativeLayout Tv_Later = (RelativeLayout) dialog.findViewById(R.id.decline);
        Button Ok_Profilechange = (Button) dialog.findViewById(R.id.agree_button);
        Ok_Profilechange.setText(getkey("firsttime_login_agree"));
        firsttime_login_terms_cbox_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (terms.isChecked()) {
                    terms.setChecked(false);
                } else {
                    terms.setChecked(true);
                }
            }
        });
        firsttime_login_privacy_cbox_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (privacy.isChecked()) {
                    privacy.setChecked(false);
                } else {
                    privacy.setChecked(true);
                }
            }
        });
        firsttime_login_eula_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eula.isChecked()) {
                    eula.setChecked(false);
                } else {
                    eula.setChecked(true);
                }
            }
        });

        Tv_Later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                Intent intent =new Intent(OtpPage.this, LoginPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!terms.isChecked()) {
                    Alert("",getkey("action_terms_and_condition"));
                } else if (!privacy.isChecked()) {
                    Alert("", getkey("action_accept_privacy_policy"));
                } else if (!eula.isChecked()) {
                    Alert("", getkey("action_ACCEPT_EULA"));
                } else {
                    PostPrivacyPolicy(Iconstant.Privacy_policy);
                }


            }
        });
        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // remove background dim
        // dialog.getWindow().setDimAmount(0);

        dialog.show();
    }

    private void PostPrivacyPolicy(String Url) {
        final Dialog dialog1 = new Dialog(OtpPage.this);
        dialog1.getWindow();
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.custom_loading);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.show();

        TextView dialog_title = (TextView) dialog1.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_verifying"));

        System.out.println("--------------PostRequest_mobnochange_otp url-------------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("user_id", SuserId);
        jsonParams.put("confirmed", "yes");

        System.out.println("------PostRequest_mobnochange_otp page jsonParams---------" + jsonParams);

        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------PostRequest_mobnochange_otp page reponse-------------------" + response);


                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    if (dialog1 != null) {
                        dialog1.dismiss();
                    }
                    postRequest_applaunch(Iconstant.getAppInfo);
                } else {

                    final PkDialog mDialog = new PkDialog(OtpPage.this);
                    mDialog.setDialogTitle(getkey("action_error"));
                    mDialog.setDialogMessage(Smessage);
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });
                    mDialog.show();
                }

                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Ed_pin.getWindowToken(), 0);

                if (dialog1 != null) {
                    dialog1.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog1 != null) {
                    dialog1.dismiss();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        cancelTimer();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

        removerunnable_setlocationuser();

        stopService(new Intent(this, Getlocation.class));


    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(OtpPage.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
       /* InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);*/
        return true;
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(OtpPage.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    @Override
    public void onLocationChanged(Location location) {
        MyCurrent_lat = location.getLatitude();
        MyCurrent_long = location.getLongitude();
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}



