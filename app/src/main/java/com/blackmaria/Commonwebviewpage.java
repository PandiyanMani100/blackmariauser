package com.blackmaria;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.blackmaria.hockeyapp.ActivityHockeyApp;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by user144 on 2/13/2018.
 */

@SuppressLint("Registered")
public class Commonwebviewpage extends ActivityHockeyApp implements View.OnClickListener {

    private LinearLayout backImage;

    private WebView webview;
    String url = "";
    private SmoothProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_hotel_web);
        url= getIntent().getStringExtra("url");
        initialize();
    }

    private void initialize() {
        backImage = (LinearLayout) findViewById(R.id.back_button);

        webview = (WebView) findViewById(R.id.wallet_money_webview);
        progressBar = (SmoothProgressBar) findViewById(R.id.wallet_money_webview_progressbar);

        backImage.setOnClickListener(this);



        // Enable Javascript to run in WebView
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);
        // Allow Zoom in/out controls
        webview.getSettings().setBuiltInZoomControls(true);
        // Zoom out the best fit your screen
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);

        webview.setWebViewClient(new MyWebViewClient());

        webview.loadUrl(url);


    }

    @Override
    public void onClick(View v) {
        if (v == backImage) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            progressBar.setProgress(100);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(0);
            super.onPageStarted(view, url, favicon);
        }
    }

       public void setValue(int progress) {
           progressBar.setProgress(progress);

    }

    private void startWebView(String url) {
        webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            //Show loader on url load
            @Override
            public void onLoadResource(WebView view, String url) {
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
                try {
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

        });
        //Load url in webView
        webview.loadUrl(url);
    }

}
