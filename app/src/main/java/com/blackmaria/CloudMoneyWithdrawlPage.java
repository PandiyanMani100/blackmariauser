package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.WalletPaymentListAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.pojo.WalletMoneyPojo;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 5/18/2017.
 */

public class CloudMoneyWithdrawlPage extends ActivityHockeyApp {
    private RelativeLayout mainLay;
    private RelativeLayout layout1;
    private RelativeLayout imageRl;
    private ImageView topImage;
    private ImageView imageviewBackarrow;
    private TextView helpTv;
    private ImageView cloudMoneyImage;
    private TextView lastWithdrawlDate;
    private TextView avaiWithsdrawMoney;
    private TextView selectWithdraw;
    private RelativeLayout paymentLay;
    private RelativeLayout favoriteAddCardviewLayout;
    private com.github.paolorotolo.expandableheightlistview.ExpandableHeightGridView PaymentWithdrawlListGridview;
    private RelativeLayout confirmButton;
    private RelativeLayout helpLayout;
    private TextView faqHelp;
    private ImageView telephoneImage;
    private Button callUs;
    private ImageView homeImage;
    private CustomEdittext Et_enteramount;
    private String currencySymbol = "";
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private SessionManager session;
    String UserID = "";
    private String Scurrency = "", SwithdrawlAmount = "";

    private RefreshReceiver refreshReceiver;
    ArrayList<WalletMoneyPojo> userList;
    WalletPaymentListAdapter walletAdapter;
    private String SpaymentName = "";
    RelativeLayout withdarw_Rl;
    private String payPalID = "", withdrawedSuccessMoney = "";
    String enteredwithdrawAmpont = "";
    AppInfoSessionManager appInfoSessionManager;
    String strCallassist = "+6565 898914";
    final int PERMISSION_REQUEST_CODE = 111;
    private CustomTextView headtxt;
    private String minAmount, maxAmount, proceedStatus, proceedError = "";

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CloudMoneyWithdrawlPage.this, Navigation_new.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CloudMoneyWithdrawlPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(CloudMoneyWithdrawlPage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cloudmoney_withdraw);
        Initialize();
        PaymentMethod();

        homeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CloudMoneyWithdrawlPage.this, Navigation_new.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        imageviewBackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent(CloudMoneyWithdrawlPage.this, CloudMoneyHomePage.class);
                startActivity(homeIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        callUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallAssistance();
            }
        });
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payPalID = "";
                postData2();
            }
        });
        helpTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HelpPopUp();
            }
        });
        withdarw_Rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WithDrawPaymentMehtod();
            }
        });

    }


    private void Initialize() {
        appInfoSessionManager = new AppInfoSessionManager(CloudMoneyWithdrawlPage.this);
        cd = new ConnectionDetector(CloudMoneyWithdrawlPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(CloudMoneyWithdrawlPage.this);
        userList = new ArrayList<WalletMoneyPojo>();

        withdarw_Rl = (RelativeLayout) findViewById(R.id.withdraw_amount_popup);
        mainLay = (RelativeLayout) findViewById(R.id.main_lay);
        layout1 = (RelativeLayout) findViewById(R.id.layout1);
        imageRl = (RelativeLayout) findViewById(R.id.image_rl);
        topImage = (ImageView) findViewById(R.id.top_image);
        imageviewBackarrow = (ImageView) findViewById(R.id.imageview_backarrow);
        helpTv = (TextView) findViewById(R.id.help_tv);
        cloudMoneyImage = (ImageView) findViewById(R.id.cloud_money_image);
        lastWithdrawlDate = (TextView) findViewById(R.id.last_withdrawl_date);
        avaiWithsdrawMoney = (TextView) findViewById(R.id.avai_withsdraw_money);
        selectWithdraw = (TextView) findViewById(R.id.select_withdraw);
        paymentLay = (RelativeLayout) findViewById(R.id.payment_lay);
        favoriteAddCardviewLayout = (RelativeLayout) findViewById(R.id.favorite_add_cardview_layout);
        PaymentWithdrawlListGridview = (com.github.paolorotolo.expandableheightlistview.ExpandableHeightGridView) findViewById(R.id._payment_withdrawl_list_gridview);
        confirmButton = (RelativeLayout) findViewById(R.id.confirm_button);
        helpLayout = (RelativeLayout) findViewById(R.id.help_layout);
        faqHelp = (TextView) findViewById(R.id.faq_help);
        telephoneImage = (ImageView) findViewById(R.id.telephone_image);
        callUs = (Button) findViewById(R.id.call_us);
        homeImage = (ImageView) findViewById(R.id.home_image);

        ImageView Rl_drawer = (ImageView) findViewById(R.id.img_status5);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);

        // headtxt = (CustomTextView) findViewById(R.id.headtxt);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
        // headtxt.setTypeface(tf);
// -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        HashMap<String, String> app = appInfoSessionManager.getAppInfo();
        strCallassist = app.get(appInfoSessionManager.KEY_CUSTOMER_NUMBER);

        postData();
    }

    private void postData() {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);//"584aa27ccae2aa741a00002f");

        System.out.println("-------------Clodmoneywithdrawl jsonParams----------------" + jsonParams);
        if (isInternetPresent) {
            postRequestCloudMoneyWithdrawlPage(Iconstant.cloudmoney_withdrawl_url, jsonParams);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

        }
    }

    private void postData2() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                if(proceedStatus.equalsIgnoreCase("1")) {
                    if (Double.parseDouble(SwithdrawlAmount)<Double.parseDouble(minAmount) &&
                            Double.parseDouble(SwithdrawlAmount) > Double.parseDouble(maxAmount)) {
                        WIthDrawlResponsePopUP1(minAmount, "1");
                    } else if (SpaymentName.equalsIgnoreCase("")) {
                        WIthDrawlResponsePopUP1(getResources().getString(R.string.choose_withdraw_method), "2");
                    } else {

                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("user_id", UserID);//"584aa27ccae2aa741a00002f");
                        jsonParams.put("amount", SwithdrawlAmount);
                        jsonParams.put("mode", SpaymentName);
                        jsonParams.put("paypal_id", payPalID);

                        System.out.println("-------------ClodmoneywithdrawlAmount jsonParams----------------" + jsonParams);
                        if (isInternetPresent) {
                            postRequestCloudMoneyWithdrawlAmount(Iconstant.cloudmoney_withdrawl_anount, jsonParams);
                        } else {
                            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

                        }
                    }
                }else {
                    Alert(getResources().getString(R.string.action_error), proceedError);
                }
            }
        });

    }

    private void postRequestCloudMoneyWithdrawlAmount(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(CloudMoneyWithdrawlPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------cloudmoneyhome url----------------" + Url);

        mRequest = new ServiceRequest(CloudMoneyWithdrawlPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------cloudmoneywithdraw  Response----------------" + response);

                String Sstatus = "", Smessage = "", amount = "", response1 = "", mode = "", currency = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        response1 = object.getString("response");
                        mode = object.getString("mode");
                        amount = object.getString("amount");
                        currency = object.getString("currency");
                        withdrawedSuccessMoney = currency + " " + amount;
                    } else {
                        response1 = object.getString("response");
                    }
                    dialogDismiss();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogDismiss();
                }

                final String finalSstatus = Sstatus;
                final String finalResponse = response1;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (finalSstatus.equalsIgnoreCase("1")) {
                            WIthDrawlResponsePopUP(finalResponse);
                            dialogDismiss();
                        } else {
                            WIthDrawlResponsePopUP1(finalResponse, "2");
                            dialogDismiss();
                        }
                    }
                });
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });


    }


    private void postRequestCloudMoneyWithdrawlPage(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(CloudMoneyWithdrawlPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------cloudmoneyhome url----------------" + Url);

        mRequest = new ServiceRequest(CloudMoneyWithdrawlPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------cloudmoneyhome_success Response----------------" + response);

                String Sstatus = "", Smessage = "", referral_earning = "", referral_code = "", currency = "",
                        last_withdrawal_txt = "", referral_count = "", name = "", code = "", icon = "", inactive_icon = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject obj_response = object.getJSONObject("response");

                    if (obj_response.length() > 0) {



                        referral_earning = obj_response.getString("referral_earning");

                        if (obj_response.has("referral_code"))
                            referral_code = obj_response.getString("referral_code");
                        if (obj_response.has("currency"))
                            currency = obj_response.getString("currency");
                        Scurrency = currency;
                        if (obj_response.has("last_withdrawal_txt")) {
                            last_withdrawal_txt = obj_response.getString("last_withdrawal_txt");
                        }
                        minAmount = obj_response.getString("min_amount");
                        maxAmount = obj_response.getString("max_amount");

                        proceedError = obj_response.getString("proceed_error");
                        proceedStatus = obj_response.getString("proceed_status");

                        Object intervention = obj_response.get("payment");
                        if (intervention instanceof JSONArray) {
                            JSONArray refuserInfo = obj_response.getJSONArray("payment");

                            if (refuserInfo.length() > 0) {
                                userList.clear();
                                for (int i = 0; i < refuserInfo.length(); i++) {
                                    JSONObject ref_user_list_object = refuserInfo.getJSONObject(i);
                                    name = ref_user_list_object.getString("name");
                                    code = ref_user_list_object.getString("code");
                                    icon = ref_user_list_object.getString("icon");
                                    inactive_icon = ref_user_list_object.getString("inactive_icon");

                                    WalletMoneyPojo walletMoneyPojo = new WalletMoneyPojo();
                                    walletMoneyPojo.setPayment_code(code);
                                    walletMoneyPojo.setPaymnet_name(name);
                                    walletMoneyPojo.setPayment_active_img(icon);
                                    walletMoneyPojo.setPayment_normal_img(inactive_icon);
                                    userList.add(walletMoneyPojo);

                                }
                            }

                        }

                    }
                    dialogDismiss();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogDismiss();
                }

                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {
                    avaiWithsdrawMoney.setText(currency + " " + referral_earning);
                    lastWithdrawlDate.setText(getResources().getString(R.string.last_widthdraw)+ last_withdrawal_txt);
                    enteredwithdrawAmpont = avaiWithsdrawMoney.getText().toString();
                    SwithdrawlAmount = referral_earning;
                    System.out.println("==============enteredwithdrawAmpont=============" + enteredwithdrawAmpont);
                    walletAdapter = new WalletPaymentListAdapter(CloudMoneyWithdrawlPage.this, userList);
                    PaymentWithdrawlListGridview.setAdapter(walletAdapter);
                    walletAdapter.notifyDataSetChanged();

                    dialogDismiss();

                } else {

                    dialogDismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });


    }

    private void WIthDrawlResponsePopUP(String response1) {
        final Dialog dialog = new Dialog(CloudMoneyWithdrawlPage.this, R.style.DialogSlideAnim2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloudmoney_withdrawlresponse);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView closeImage = (ImageView) dialog.findViewById(R.id.img_close);
        CustomTextView withdrawResponse = (CustomTextView) dialog.findViewById(R.id.withdrawal_success_tv);
        CustomTextView InRTv = (CustomTextView) dialog.findViewById(R.id.paypal_conform);
        // ImageView home_icon = (ImageView) dialog.findViewById(R.id.home_icon);

        withdrawResponse.setText(response1.toUpperCase() + " " + withdrawedSuccessMoney);
        // InRTv.setText(withdrawedSuccessMoney);


        // Et_enteramount.setHint(getResources().getString(R.string.walletmoney_lable_rechargemoney_edittext_hint) + " " + ScurrencySymbol + Str_minimum_amt + " " + "-" + " " + ScurrencySymbol + Str_maximum_amt);
        InRTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }

        });
      /*  home_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CloudMoneyWithdrawlPage.this, Navigation_new.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                dialog.dismiss();
            }

        });*/
        dialog.show();
    }

    private void WIthDrawlResponsePopUP1(String minAmount, String flag) {
        final Dialog dialog = new Dialog(CloudMoneyWithdrawlPage.this, R.style.DialogSlideAnim2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.withdrawal_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView closeImage = (ImageView) dialog.findViewById(R.id.img_close);
        CustomTextView withdrawResponse = (CustomTextView) dialog.findViewById(R.id.min_withdrawal_tv);
        CustomTextView InRTv = (CustomTextView) dialog.findViewById(R.id.withdraw_amt);
        if (flag.equalsIgnoreCase("1")) {
            InRTv.setText(Scurrency + "" + minAmount.toUpperCase()+getResources().getString(R.string.to_space)+Scurrency + "" + maxAmount.toUpperCase());
        } else {
            withdrawResponse.setText(minAmount.toUpperCase());
            InRTv.setText("");
        }


        // Et_enteramount.setHint(getResources().getString(R.string.walletmoney_lable_rechargemoney_edittext_hint) + " " + ScurrencySymbol + Str_minimum_amt + " " + "-" + " " + ScurrencySymbol + Str_maximum_amt);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private void HelpPopUp() {
        final Dialog dialog = new Dialog(CloudMoneyWithdrawlPage.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloud_money_withdrawl_help);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.bold.ttf");
        CustomTextView custom_text1 = (CustomTextView) dialog.findViewById(R.id.custom_text1);
        CustomTextView custom_text3 = (CustomTextView) dialog.findViewById(R.id.custom_text3);
        custom_text1.setTypeface(tf);
        // custom_text3.setTypeface(tf);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView image_close = (ImageView) dialog.findViewById(R.id.image_close);
        // Et_enteramount.setHint(getResources().getString(R.string.walletmoney_lable_rechargemoney_edittext_hint) + " " + ScurrencySymbol + Str_minimum_amt + " " + "-" + " " + ScurrencySymbol + Str_maximum_amt);
        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }

        });
        dialog.show();
    }


    private void WithDrawPaymentMehtod() {
        final Dialog dialog = new Dialog(CloudMoneyWithdrawlPage.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_enter_withdrawal);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Et_enteramount = (CustomEdittext) dialog.findViewById(R.id.edt_withdrawal_amount);
        RelativeLayout payment_proceed = (RelativeLayout) dialog.findViewById(R.id.Rl_confirm);
        ImageView closeImage = (ImageView) dialog.findViewById(R.id.img_close);
        CustomTextView txt_currency = (CustomTextView) dialog.findViewById(R.id.txt_currency);
        txt_currency.setText(Scurrency);

        // Et_enteramount.setHint(getResources().getString(R.string.walletmoney_lable_rechargemoney_edittext_hint) + " " + ScurrencySymbol + Str_minimum_amt + " " + "-" + " " + ScurrencySymbol + Str_maximum_amt);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(Et_enteramount);
                dialog.dismiss();
            }

        });
        payment_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(Et_enteramount);
                enteredwithdrawAmpont = Et_enteramount.getText().toString();
                avaiWithsdrawMoney.setText(enteredwithdrawAmpont);
                SwithdrawlAmount = enteredwithdrawAmpont;
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void PaymentMethod() {
        PaymentWithdrawlListGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SpaymentName = userList.get(position).getPayment_code();


                for (int i = 0; i < userList.size(); i++) {
                    if (i == position) {
                        userList.get(i).setPayment_selected_payment_id("true");
                    } else {
                        userList.get(i).setPayment_selected_payment_id("false");
                    }
                }
                walletAdapter.notifyDataSetChanged();
                if (SpaymentName.equalsIgnoreCase("2")) {
                    PayPalMethod();
                }


            }
        });


    }

    private void PayPalMethod() {
        final Dialog dialog = new Dialog(CloudMoneyWithdrawlPage.this, R.style.DialogSlideAnim2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.email_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Et_enteramount = (CustomEdittext) dialog.findViewById(R.id.email_et);
        ImageView closeImage = (ImageView) dialog.findViewById(R.id.img_close);
        CustomTextView paypal_conform = (CustomTextView) dialog.findViewById(R.id.paypal_conform);


        // Et_enteramount.setHint(getResources().getString(R.string.walletmoney_lable_rechargemoney_edittext_hint) + " " + ScurrencySymbol + Str_minimum_amt + " " + "-" + " " + ScurrencySymbol + Str_maximum_amt);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(Et_enteramount);
                dialog.dismiss();
            }
        });
        paypal_conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(Et_enteramount);
                String payPalIdValue = Et_enteramount.getText().toString();
                payPalID = payPalIdValue;
                if (!payPalID.equalsIgnoreCase("")) {
                    if (isValidEmail(Et_enteramount.getText().toString())) {
                        {
                            postData2();
                        }
                    } else {
                        Alert("", getResources().getString(R.string.valid_paypal_alert));
                    }
                } else {
                    Alert("", getResources().getString(R.string.paypal_alert));
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        if (!TextUtils.isEmpty(target) && (android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches() || android.util.Patterns.PHONE.matcher(target).matches())) {
            return true;
        } else {

            return false;
        }
        //return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void CallAssistance() {
        System.out.println("----------------muruga sos-----------------");

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + strCallassist));
            startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + strCallassist));
            startActivity(intent);
        }

    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudMoneyWithdrawlPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + strCallassist));
                    startActivity(intent);
                }
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(CloudMoneyWithdrawlPage.this, CloudMoneyHomePage.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }

    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }
}
