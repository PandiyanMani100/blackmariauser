package com.blackmaria;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.adapter.ComplaintAdapter;
import com.blackmaria.pojo.Complaintlist_pojo;
import com.blackmaria.pojo.ComplaintsPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CircularImageView;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Complaint_Page_new extends AppCompatActivity {

    private CircularImageView Iv_user;
    private TextView Tv_user_name, Tv_user_loc, Tv_view_report, Tv_help, id_faq;
    private EditText Et_ticket_no, Et_booking_ref_no;
    CustomTextView value_reporttext, value_oprntext, value_closedtext;
    // private ExpandableHeightListView Lv_complaint_type;
    private RelativeLayout Rl_help;
    private ConstraintLayout createnew_report_layout;
    // Button Bt_reset;
    private ImageView Rl_back;
LanguageDb mhelper ;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private SessionManager session;
    private String UserID = "";

    String movetocomplaint = "0";

    private ComplaintAdapter complaintsAdapter;
    private ArrayList<ComplaintsPojo> complaint_itemList;
    private ArrayList<String> complaint_itemList1;
    private ArrayList<String> complaint_itemList2;
    private boolean isComplaintsAvailable = false;
    String userName = "", userImage = "", userLocation = "", todayDate;
    private RefreshReceiver refreshReceiver;
    private RelativeLayout compalints, open, closed;
    private String StotalTicketCount = "", SopenTicketCount = "", SclosedTicketCount = "";
    private LinearLayout complaintlist;
    private TextView  text_empty;
    private String latestTicket = "";
    private String homepage = "";
    private ArrayList<Complaintlist_pojo> complaintlist_pojos = new ArrayList<>();

    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
             if (intent.getAction().equals("com.package.ACTION_CLASS_complaint_replay_notification")) {
                if (intent.getExtras() != null) {
                    String ticketId = (String) intent.getExtras().get("ticket_id");
                    Intent intent1 = new Intent(Complaint_Page_new.this, ComplaintPageViewDetailPage.class);
                    intent1.putExtra("ticket_id", ticketId);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);//"584aa27ccae2aa741a00002f");
        if (isInternetPresent) {
            postRequestComplaintPage(Iconstant.complaint_list_url, jsonParams);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_complaint_ticket_home_constrain);
        mhelper= new LanguageDb(this);
        initialize();

        Intent intent = getIntent();

        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("mo")) {
                movetocomplaint = "1";
            }
        }

        Rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!homepage.equalsIgnoreCase("")) {
                    if (homepage.equalsIgnoreCase("Homepage")) {
                        Intent intent = new Intent(Complaint_Page_new.this, Navigation_new.class);
                        startActivity(intent);
                        onBackPressed();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                } else {
                    onBackPressed();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }

        });

        createnew_report_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Complaint_Page_new.this, CreateNew_Complaint.class);
                intent.putExtra("user_image", userImage);
                intent.putExtra("location_name", userLocation);
                intent.putExtra("user_name", userName);
                intent.putExtra("today_datetime", todayDate);
                intent.putExtra("valuesfrom", "ComplaintpageHome");
                intent.putStringArrayListExtra("complaintsubjects", complaint_itemList1);
                intent.putStringArrayListExtra("complaintoptionid", complaint_itemList2);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        compalints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Complaint_Page_new.this, ComplaintListPage.class);
                intent.putExtra("typeOfCase", "COMPLAINTS");
                intent.putExtra("cmpCounts", StotalTicketCount);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Complaint_Page_new.this, ComplaintListPage.class);
                intent.putExtra("typeOfCase", "OPEN");
                intent.putExtra("cmpCounts", SopenTicketCount);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        closed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Complaint_Page_new.this, ComplaintListPage.class);
                intent.putExtra("typeOfCase", "CLOSED");
                intent.putExtra("cmpCounts", SclosedTicketCount);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        if (getIntent().hasExtra("frompage")) {
            homepage = getIntent().getStringExtra("frompage");
        }

    }


    private void initialize() {
        cd = new ConnectionDetector(Complaint_Page_new.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(Complaint_Page_new.this);
        complaint_itemList = new ArrayList<ComplaintsPojo>();
        complaint_itemList1 = new ArrayList<String>();
        complaint_itemList2 = new ArrayList<String>();
        Iv_user = (CircularImageView) findViewById(R.id.complaint_page_user_imageview);
        Rl_back = findViewById(R.id.backbtn);
        Tv_user_loc = (TextView) findViewById(R.id.complaint_page_userlocation_textview);
        Tv_user_name = (TextView) findViewById(R.id.complaint_page_username_textview);


       TextView comm = (TextView) findViewById(R.id.comm);
        comm.setText(getkey("complaint_lable"));

        TextView compliant_text = (TextView) findViewById(R.id.compliant_text);
        compliant_text.setText("  "+getkey("create_complaint"));

        TextView opeene = (TextView) findViewById(R.id.opeene);
        opeene.setText(getkey("open_me"));

        TextView closeds = (TextView) findViewById(R.id.closeds);
        closeds.setText(getkey("closed_me"));

        compalints = (RelativeLayout) findViewById(R.id.compalints);
        open = (RelativeLayout) findViewById(R.id.open);
        closed = (RelativeLayout) findViewById(R.id.closed);
        complaintlist = findViewById(R.id.complaintlist);
        text_empty = (TextView) findViewById(R.id.text_empty);
        text_empty.setText(getkey("nocomplaints"));
        createnew_report_layout = findViewById(R.id.createnew_report_layout);
        value_reporttext = (CustomTextView) findViewById(R.id.value_reporttext);
        value_oprntext = (CustomTextView) findViewById(R.id.value_oprntext);
        value_closedtext = (CustomTextView) findViewById(R.id.value_closedtext);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/roboto-condensed.regular.ttf");
        value_closedtext.setTypeface(face, Typeface.BOLD_ITALIC);
        value_oprntext.setTypeface(face, Typeface.BOLD_ITALIC);
        value_reporttext.setTypeface(face, Typeface.BOLD_ITALIC);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);


    }

    private void postRequestComplaintPage(String Url, HashMap<String, String> jsonParams) {
        final Dialog dialog = new Dialog(Complaint_Page_new.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_loading"));

        mRequest = new ServiceRequest(Complaint_Page_new.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------ComplaintPage Response----------------" + response);

                String Sstatus = "", Scurrency_code = "", user_image = "", Scurrentbalance = "", Smessage = "", Scurrency = "", user_name = "", user_location = "", user_id = "", member_since = "", Date = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject jobject = object.getJSONObject("response");
                    StotalTicketCount = jobject.getString("total_ticket_Count");
                    SopenTicketCount = jobject.getString("open_ticket_Count");
                    SclosedTicketCount = jobject.getString("closed_ticket_Count");
                    if (StotalTicketCount.equalsIgnoreCase("0") || StotalTicketCount.equalsIgnoreCase("")) {
                        text_empty.setVisibility(View.VISIBLE);
//                        new_reply.setVisibility(View.GONE);
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (jobject.length() > 0) {
                            Date = jobject.getString("today_datetime");
                            todayDate = Date;
                            Object check_object1 = jobject.get("user_profile");
                            if (check_object1 instanceof JSONObject) {
                                JSONObject user_detail_object = jobject.getJSONObject("user_profile");
                                if (user_detail_object.length() > 0) {
                                    user_name = user_detail_object.getString("user_name");
                                    user_id = user_detail_object.getString("user_id");

                                    member_since = user_detail_object.getString("member_since");
                                    user_location = user_detail_object.getString("location_name");
                                    user_image = user_detail_object.getString("user_image");

                                    userImage = user_image;
                                    userName = user_name;
                                    userLocation = user_location;
                                    session.setcomplaintUserDetail(userName, userImage, userLocation);

                                }
                            }

                            JSONArray jsonArray_complainlist = jobject.getJSONArray("ticket_list");
                            if (jsonArray_complainlist.length() > 0) {
                                text_empty.setVisibility(View.GONE);
                                complaintlist_pojos.clear();
                                for (int i = 0; i <= jsonArray_complainlist.length() - 1; i++) {
                                    Complaintlist_pojo pojo = new Complaintlist_pojo();
                                    pojo.setRide_id(jsonArray_complainlist.getJSONObject(i).getString("ride_id"));
                                    pojo.setSubject(jsonArray_complainlist.getJSONObject(i).getString("subject"));
                                    pojo.setTicket_date(jsonArray_complainlist.getJSONObject(i).getString("ticket_date"));
                                    pojo.setTicket_number(jsonArray_complainlist.getJSONObject(i).getString("ticket_number"));
                                    pojo.setTicket_status(jsonArray_complainlist.getJSONObject(i).getString("ticket_status"));
                                    pojo.setVehicle_number(jsonArray_complainlist.getJSONObject(i).getString("vehicle_number"));
                                    pojo.setImage(jsonArray_complainlist.getJSONObject(i).getString("image"));
                                    complaintlist_pojos.add(pojo);
                                }

                                complaintlist(complaintlist_pojos);
                            } else {
                                text_empty.setVisibility(View.VISIBLE);
                            }


                            Object check_object2 = jobject.get("reply_array");
                            if (check_object2 instanceof JSONObject) {
                                JSONObject replyArray = jobject.getJSONObject("reply_array");
                                String driver_name = "", ticket = "", option_name = "", driver_image = "", reply_date = "";

                                if (replyArray.length() > 0) {
                                    driver_name = replyArray.getString("driver_name");
                                    ticket = replyArray.getString("ticket");
                                    latestTicket = ticket;
                                    option_name = replyArray.getString("option_name");
                                    driver_image = replyArray.getString("driver_image");
                                    reply_date = replyArray.getString("reply_date");
                                }
                            }


                            Object check_object = jobject.get("subject");
                            if (check_object instanceof JSONArray) {
                                JSONArray complaint_list_jsonArray = jobject.getJSONArray("subject");
                                if (complaint_list_jsonArray.length() > 0) {
                                    isComplaintsAvailable = true;
                                    complaint_itemList.clear();
                                    complaint_itemList1.clear();
                                    complaint_itemList1.add(mhelper.getvalueforkey("select_subject"));
                                    complaint_itemList2.add("0");

                                    for (int i = 0; i < complaint_list_jsonArray.length(); i++) {
                                        JSONObject earning_list_obj = complaint_list_jsonArray.getJSONObject(i);
                                        ComplaintsPojo complaintsPojo = new ComplaintsPojo();
                                        complaintsPojo.setoptionsId(earning_list_obj.getString("option_id"));
                                        complaintsPojo.setsubjectId(earning_list_obj.getString("subject_id"));
                                        complaintsPojo.setsubjectName(earning_list_obj.getString("subject_name"));
                                        complaintsPojo.setReasonStatus("0");
                                        complaint_itemList.add(complaintsPojo);
                                        complaint_itemList1.add(earning_list_obj.getString("subject_name"));
                                        complaint_itemList2.add(earning_list_obj.getString("option_id"));
                                    }
                                    isComplaintsAvailable = true;
                                } else {
                                    isComplaintsAvailable = false;
                                }
                            } else {
                                isComplaintsAvailable = false;
                            }
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                        }





                        if(movetocomplaint.equals("1"))
                        {
                            Intent intent = new Intent(Complaint_Page_new.this, CreateNew_Complaint.class);
                            intent.putExtra("user_image", userImage);
                            intent.putExtra("location_name", userLocation);
                            intent.putExtra("user_name", userName);
                            intent.putExtra("today_datetime", todayDate);
                            intent.putExtra("valuesfrom", "ComplaintpageHome");
                            intent.putStringArrayListExtra("complaintsubjects", complaint_itemList1);
                            intent.putStringArrayListExtra("complaintoptionid", complaint_itemList2);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        }

                    } else {
                        Smessage = jobject.getString("response");
                    }
                    if (dialog != null) {
                        dialog.dismiss();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    value_reporttext.setText(StotalTicketCount);
                    value_oprntext.setText(SopenTicketCount);
                    value_closedtext.setText(SclosedTicketCount);
                    Tv_user_name.setText(user_name);
                    Tv_user_loc.setText(user_location);
                    Picasso.with(Complaint_Page_new.this)
                            .load(user_image)
                            .placeholder(R.drawable.no_user_img)   // optional
                            .error(R.drawable.no_user_img)      // optional
                            .into(Iv_user);
                    complaintsAdapter = new ComplaintAdapter(Complaint_Page_new.this, complaint_itemList);

                } else {
                    Alert(getkey("action_error"), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Complaint_Page_new.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    private void complaintlist(final ArrayList<Complaintlist_pojo> complaintlist_pojos) {
        try {
            complaintlist.removeAllViews();
            for (int i = 0; i <= complaintlist_pojos.size() - 1; i++) {
                View view = getLayoutInflater().inflate(R.layout.complaintlist_contrain, null);

                ConstraintLayout constrianlayout = view.findViewById(R.id.constrianlayout);
                RoundedImageView imageView = view.findViewById(R.id.image);
                TextView vehicleno = view.findViewById(R.id.vehicleno);
                vehicleno.setText(getkey("vechile_no_caps") + complaintlist_pojos.get(i).getVehicle_number() +getkey("reply_ticket") + complaintlist_pojos.get(i).getTicket_number() + "\n" + getkey("aaaaat") + complaintlist_pojos.get(i).getTicket_date());
                TextView msgcount = view.findViewById(R.id.msgcount);

                final int finalI = i;
                constrianlayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent1 = new Intent(Complaint_Page_new.this, ComplaintPageViewDetailPage.class);
                        intent1.putExtra("ticket_id", complaintlist_pojos.get(finalI).getTicket_number());
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                });

                Picasso.with(getApplicationContext()).load(complaintlist_pojos.get(i).getImage()).placeholder(R.drawable.usericon_getpaid).error(R.drawable.usericon_getpaid).into(imageView);

                complaintlist.addView(view);
            }
        } catch (IllegalArgumentException e) {
        }
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}