package com.blackmaria;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.app.ShareCompat;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.PkDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by user144 on 5/18/2017.
 */
public class MilesHome extends FragmentActivityHockeyApp implements View.OnClickListener {

    //   ------------UI Decleration--------------------
    private TextView Tv_miles_km, helps;
    LanguageDb mhelper;
    private ImageView Iv_back, Iv_home;
    private TextView Rl_miles_list, Tv_redeem_view;
    private TextView aa;
    private TextView Tv_sharemiles_lable;
    private LinearLayout Ll_miles_share_facebook, Ll_miles_share_twitter, Ll_miles_share_googlepuls;

    //    --------------------Variable decleration-----------------
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String UserID = "";
    private String Miles_sharelink = "", Miles_shareMessage = "", Miles_shareSubject = "", Miles_shareImage = "", facebook = "", google = "";
    private boolean isdataPresent = false;

    final int PERMISSION_REQUEST_CODE = 111;
    final int GOOGLE_PLUS_SHARE_REQUEST_CODE = 222;
    private RefreshReceiver refreshReceiver;

    public static final int REQUEST_CODE_FACEBOOK_LOGIN = 111;
    public static final int REQUEST_CODE_FACEBOOK_SHARE = 112;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private Uri imageUri = null;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(MileageDetails.this, Fragment_HomePage.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(MilesHome.this, MyRideRating.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(MilesHome.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.miles_home_page_constrain);
        mhelper= new LanguageDb(this);
        initizition();
        facebookSDKInitialize();
//        Iv_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MilesHome.this, Navigation_new.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);
//            }
//        });

        Tv_redeem_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_bepartner = new Intent(MilesHome.this, MilesRedeemRewards.class);
                startActivity(intent_bepartner);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        Iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MilesHome.this, Navigation_new.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        Rl_miles_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MilesHome.this, MilesRewardsList.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

    }

    @Override
    public void onBackPressed() {
    }

    private void initizition() {

        session = new SessionManager(MilesHome.this);
        cd = new ConnectionDetector(MilesHome.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_USERID);

        Tv_miles_km = (TextView) findViewById(R.id.mylage_txt);


        Tv_redeem_view = findViewById(R.id.redeen_tv);
        Tv_redeem_view.setText(getkey("redeem_now"));
        Tv_sharemiles_lable = findViewById(R.id.share_miles_tv);
        Iv_back = (ImageView) findViewById(R.id.miles_back_img);
//        Iv_home = (ImageView) findViewById(R.id.miles_home);
        Rl_miles_list = findViewById(R.id.mils_list);
        Rl_miles_list.setText(getkey("statement"));
        Ll_miles_share_facebook = (LinearLayout) findViewById(R.id.invite_earn_fb_layout);
        Ll_miles_share_twitter = (LinearLayout) findViewById(R.id.invite_earn_twitter_layout);
        Ll_miles_share_googlepuls = (LinearLayout) findViewById(R.id.invite_earn_googleplus_layout);

        TextView miles_home_appreciate_txt= findViewById(R.id.miles_home_appreciate_txt);
        miles_home_appreciate_txt.setText(getkey("we_appreciate"));

        TextView miles_home_your_trips_txt= findViewById(R.id.miles_home_your_trips_txt);
        miles_home_your_trips_txt.setText(getkey("every_miles_of_your_trips"));

//        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/digital-7.ttf");
//        Tv_miles_km.setTypeface(face);


        Ll_miles_share_facebook.setOnClickListener(this);
        Ll_miles_share_twitter.setOnClickListener(this);
        Ll_miles_share_googlepuls.setOnClickListener(this);
        if (isInternetPresent) {
            PostRequestMilesHome(Iconstant.miles_home_url);
        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));
        }
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);


    }

    //----------Share Image and Text on Facebook Method--------

    private void shareFacebookLink(String link) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, Miles_shareMessage);
        intent.putExtra(Intent.EXTRA_TEXT, link);
        Intent pacakage2 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana");
        } else {
            intent.setPackage("");
        }

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_facebook_not_installed"), "https://play.google.com/store/apps/details?id=com.facebook.katana&hl=en");
        }
    }


    @Override
    public void onClick(View v) {

        if (isdataPresent) {

            if (v == Ll_miles_share_facebook) {

                if (isInternetPresent) {
//                    SharecashBackOnFaceBook();
//                    try {
//                        if (isFBLoggedIn()) {
//                            ShareDialog();
//                        } else {
//                            LoginManager.getInstance().logInWithPublishPermissions(
//                                    MilesHome.this,
//                                    Arrays.asList("publish_actions"));
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                    try {
                        imageUri = Uri.parse(Miles_shareImage);
                    } catch (NullPointerException e) {
                    }
                    shareFacebookLink(Miles_shareImage);
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }


            } else if (v == Ll_miles_share_twitter) {

                if (facebook.equalsIgnoreCase("1")) {

                    Uri imageUri = null;
                    try {
                        imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                                BitmapFactory.decodeResource(getResources(), R.drawable.miles_timeline_post), null, null));
                    } catch (NullPointerException e) {
                    }
                    shareTwitter(Miles_shareMessage, imageUri);
                } else {
                    Toast.makeText(MilesHome.this, getkey("alreadyusermoiles"), Toast.LENGTH_SHORT).show();
                }


            } else if (v == Ll_miles_share_googlepuls) {

                if (google.equalsIgnoreCase("1")) {
                    Uri imageUri = null;
                    try {
                        imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                                BitmapFactory.decodeResource(getResources(), R.drawable.miles_timeline_post), null, null));
                    } catch (NullPointerException e) {
                    }

//                    shareGPlus(Miles_shareMessage, imageUri);
                    shareInstagramelink(Miles_shareMessage, imageUri);

                } else {
                    Toast.makeText(MilesHome.this,getkey("alreadyusermoiles"), Toast.LENGTH_SHORT).show();
                }

            }


        } else {


            Alert(getkey("alert_label_title"), getkey("invite_earn_label_problem_server"));


        }


    }


    private void PostRequestMilesHome(String Url) {

        dialog = new Dialog(MilesHome.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------MilesHome url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);


        System.out.println("-----------MilesHome jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(MilesHome.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------MilesHome reponse-------------------" + response);

                String Sstatus = "", Stotal_distance = "", Smileage_expiry = "", Sdistance_unit = "", Smileage_timeline = "", Sfree_mileage = "",
                        Ssubject = "", Smessage = "", Surl = "", Salertmsg = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsondata = object.getJSONObject("response");

                        if (jsondata.length() > 0) {

                            Stotal_distance = jsondata.getString("total_distance");
                            Smileage_expiry = jsondata.getString("mileage_expiry");
                            Sdistance_unit = jsondata.getString("distance_unit");
                            Miles_shareImage = jsondata.getString("mileage_timeline");
                            Sfree_mileage = jsondata.getString("free_mileage");
                            Miles_shareSubject = jsondata.getString("subject");
                            Miles_shareMessage = jsondata.getString("message");
                            facebook = jsondata.getString("facebook");
                            google = jsondata.getString("google");
                            Miles_sharelink = jsondata.getString("url");

                            isdataPresent = true;

                        }

                        dialogDismiss();

                    } else {

                        Salertmsg = object.getString("response");
                        dialogDismiss();


                    }


                } catch (JSONException e) {

                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();


                }


                dialogDismiss();


                if (Sstatus.equalsIgnoreCase("1")) {

                    Tv_miles_km.setText(Stotal_distance);
                    Tv_sharemiles_lable.setText(getkey("shareusget") + " " + Sfree_mileage + " " + getkey("miles"));


                } else {

                    Alert(getkey("action_error"), Salertmsg);

                }


            }

            @Override
            public void onErrorListener() {

                dialogDismiss();

            }
        });


    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(MilesHome.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    //----------Share Image and Text on Twitter Method--------
    protected void shareTwitter(String text, Uri image) {
        boolean isAppInstalled = appInstalledOrNot("com.twitter.android");

        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_STREAM, image);
            intent.setType("image/jpeg");
            intent.setPackage("com.twitter.android");

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                Alert(getkey("alert_label_title"), getkey("invite_earn_label_twitter_not_installed"));
            }
        } else {
            Alert1(getkey("alert_label_title"),getkey("invite_earn_label_twitter_not_installed"), "https://play.google.com/store/apps/details?id=com.twitter.android");
        }
    }


    //----------Share Image and Text on googleplus Method--------
    protected void shareGPlus(String text, Uri image) {


      /*  PlusShare.Builder builder = new PlusShare.Builder(this);
        // Set call-to-action metadata.
        builder.addCallToAction(
                "CREATE_ITEM",
                Uri.parse("http://plus.google.com/pages/create"),
                "/pages/create");
        builder.setText(text);
        builder.setType("image/jpeg");
        builder.setContentUrl(Uri.parse(Miles_sharelink));
        builder.setContentDeepLinkId("/pages/",
                null, null, null);
        builder.setStream(image);
      try {
            startActivityForResult(builder.getIntent(), GOOGLE_PLUS_SHARE_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
        }*/

        Intent shareIntent = ShareCompat.IntentBuilder.from(this)
                .setText(text)
                .setType("image/jpeg")
                .setStream(image)
                .getIntent()

                .setPackage("com.google.android.apps.plus");
        try {
            startActivityForResult(shareIntent, GOOGLE_PLUS_SHARE_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            Alert(getkey("alert_label_title"), getkey("invite_earn_label_gplus_not_installed"));
//            Toast.makeText(YourActivity.this, "You haven't installed google+ on your device", Toast.LENGTH_SHORT).show();
        }
    }


    protected void shareInstagramelink(String text, Uri image) {
        boolean isAppInstalled = appInstalledOrNot("com.instagram.android");

        if (isAppInstalled) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            // intent.putExtra(Intent.EXTRA_STREAM, "");
            intent.setType("image/jpeg");
            intent.setPackage("com.instagram.android");

            // Add the URI to the Intent.
            intent.putExtra(Intent.EXTRA_STREAM, image);

            try {
                //startActivity(intent);
                startActivity(Intent.createChooser(intent, "Share to"));
            } catch (android.content.ActivityNotFoundException ex) {
                Alert1(getkey("alert_label_title"), getkey("invite_earn_label_instagram_not_installed"), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
            }
        } else {
            Alert1(getkey("alert_label_title"), getkey("invite_earn_label_instagram_not_installed"), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
        }
    }


    private void Alert1(String title, String alert, final String url) {

        final PkDialog mDialog = new PkDialog(MilesHome.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        mDialog.show();

    }


    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }


    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case GOOGLE_PLUS_SHARE_REQUEST_CODE:

                if (resultCode == RESULT_OK) {
                    PostapplyMilesHome(Iconstant.miles_rewards_apply_milageurl, "google");
                    // Google Plus Share Success
                } else {
                    // Google Plus Share Failed
                }

                break;

        }

        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PostapplyMilesHome(String Url, String media) {

        final Dialog dialog = new Dialog(MilesHome.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------MILESAPPLY URL--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("media", media);

        System.out.println("-----------MILESAPPLY jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(MilesHome.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------MilesHome reponse-------------------" + response);

                String Sstatus = "", Salertmsg = "", Smilage = "", SdistanceUnit = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        sendBroadcast();
                        //  JSONObject jsondata = object.getJSONObject("response");
                        Salertmsg = object.getString("response");
                        Smilage = object.getString("mileage");
                        SdistanceUnit = object.getString("distance_unit");
                        Tv_miles_km.setText(Smilage + " " + SdistanceUnit);
                        if (dialog != null) {
                            dialog.dismiss();
                        }

                    } else {
                        Salertmsg = object.getString("response");
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                } catch (JSONException e) {

                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
                if (dialog != null) {
                    dialog.dismiss();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    Alert(getkey("action_success"), Salertmsg);
                } else {

                    Alert(getkey("action_error"), Salertmsg);

                }
            }

            @Override
            public void onErrorListener() {

                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });


    }

    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();


        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                ShareDialog();

            }

            @Override
            public void onCancel() {
                Toast.makeText(MilesHome.this, getkey("logincancel"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(MilesHome.this, getkey("loginerror"), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void ShareDialog() {

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                System.out.println("Result = " + result.getPostId());
//                Toast.makeText(MilesHome.this, "FB Share success", Toast.LENGTH_SHORT).show();

                if (isInternetPresent) {
                    PostapplyMilesHome(Iconstant.miles_rewards_apply_milageurl, "facebook");
                } else {
                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));
                }

            }

            @Override
            public void onCancel() {
                Toast.makeText(MilesHome.this,  getkey("fb_shared"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(MilesHome.this,  getkey("fb_error"), Toast.LENGTH_SHORT).show();
            }
        }, REQUEST_CODE_FACEBOOK_SHARE);


        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(Miles_sharelink))
                .build();

        shareDialog.show(content);


    }

    private void sendBroadcast() {
        Intent local = new Intent();
        local.setAction("com.app.MilesPagePage.refreshhomePage");
        local.putExtra("refresh", "yes");
        sendBroadcast(local);
    }

    public boolean isFBLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
