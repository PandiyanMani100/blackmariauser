package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.newdesigns.Pin_activitynew;
import com.blackmaria.newdesigns.saveplus.Checkpattern;
import com.blackmaria.newdesigns.saveplus.Confirmpatern_activity;
import com.blackmaria.newdesigns.saveplus.Scanqrcode;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.volley.ServiceRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Saveplusdialog extends BottomSheetDialogFragment {

    private SessionManager session;

   LanguageDb mhelper;
    String saveplusida,codepath;

    public static Saveplusdialog newInstance() {
        return new Saveplusdialog();
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setCancelable(true);
        getDialog().setTitle("Title");

        View view = inflater.inflate(R.layout.savepluspopup, null, false);
        mhelper = new LanguageDb(getActivity());
        session=new SessionManager(getActivity());

        String currency =session.getssavedcurnecy();
        String saveplusbalance  =session.getsaveplusbalance();
         saveplusida=session.getsaveplusidd();
         codepath =session.getcodepathh();

        final TextView balance = view.findViewById(R.id.balance);

        final LinearLayout learnmore = view.findViewById(R.id.learnmore);
        balance.setText(getkey("available")+ saveplusbalance);

        final TextView saveplusid = view.findViewById(R.id.saveplusid);
        final TextView scanmycode_qr = view.findViewById(R.id.scanmycode_qr);
        scanmycode_qr.setText(getkey("scanme"));
        final TextView viewdetails = view.findViewById(R.id.viewdetails);
        viewdetails.setText(getkey("view_details"));
        saveplusid.setText(getkey("saveplus_id") +" :" + saveplusida);

        final TextView leanrmoee = view.findViewById(R.id.leanrmoee);
        leanrmoee.setText(getkey("learn_more"));

        final ImageView qrcode = (ImageView)view.findViewById(R.id.qrcode);
        Glide.with(getActivity()).load(codepath).into(qrcode);


        scanmycode_qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                startActivity(new Intent(getActivity(), Scanqrcode.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });



        learnmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
                final Dialog dialog = new Dialog(getActivity(), R.style.DialogSlideAnim3);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.newqrcoddefile);



                final LinearLayout learnmore = dialog.findViewById(R.id.learnmore);

                final TextView saveplusid = dialog.findViewById(R.id.saveplusid);
                saveplusid.setText(getkey("saveplusa"));

                final TextView lineone = dialog.findViewById(R.id.lineone);
                lineone.setText(getkey("saveplus_is_your_second_ewallet_to_accept_small_refunds_from_our_partners_where_small_returns_are_not_possible"));


                final TextView lineteo = dialog.findViewById(R.id.lineteo);
                lineteo.setText(getkey("newstring2"));

                final TextView linethree = dialog.findViewById(R.id.linethree);
                linethree.setText(getkey("string3"));

                final TextView howto = dialog.findViewById(R.id.howto);
                howto.setText(getkey("how_to"));

                final TextView make1 = dialog.findViewById(R.id.make1);
                make1.setText(getkey("string4new"));

                final TextView make2 = dialog.findViewById(R.id.make2);
                make2.setText(getkey("string5new"));

                final TextView make3 = dialog.findViewById(R.id.make3);
                make3.setText(getkey("string6"));

                final TextView make6 = dialog.findViewById(R.id.make6);
                make6.setText(getkey("saveplusa"));

                final TextView simplco = dialog.findViewById(R.id.simplco);
                simplco.setText(getkey("simin"));

                final TextView backk = dialog.findViewById(R.id.backk);
                backk.setText(getkey("back_lable"));

                learnmore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });



                // make dialog itself transparent
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                // remove background dim
                // dialog.getWindow().setDimAmount(0);
                dialog.show();
            }
        });

        ImageView closedialog = (ImageView)view.findViewById(R.id.closedialog);
        closedialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        viewdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

                HashMap<String, String> getpatterns = session.getpattern();
                String patterns = getpatterns.get(SessionManager.KEY_PATTERN);
                Intent intents;
                if (!patterns.equalsIgnoreCase(""))
                {
                    updatepattern(Iconstant.getpattern);
                } else {
                    ArrayList<String> ad = new ArrayList<String>();
                    ad.add("wallconfisss");
                    EventBus.getDefault().post(ad);
                }
            }
        });



        return view;
    }


    private void updatepattern(String Url) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> info = session.getUserDetails();
        String UserID = info.get(SessionManager.KEY_USERID);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);


        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        JSONObject jsonObject = object.getJSONObject("response");
                        if (jsonObject.has("pattern_code")) {
                            session.setpattern(jsonObject.getString("pattern_code"));
                            if(!jsonObject.getString("pattern_code").equals(""))
                            {
                                ArrayList<String> ad = new ArrayList<String>();
                                ad.add("Checkpa");
                                ad.add(codepath);
                                ad.add(saveplusida);
                                EventBus.getDefault().post(ad);
                            }
                            else
                            {
                                ArrayList<String> ad = new ArrayList<String>();
                                ad.add("confi");
                                EventBus.getDefault().post(ad);



                            }
                        }
                        else
                        {
                            ArrayList<String> ad = new ArrayList<String>();
                            ad.add("confi");
                            EventBus.getDefault().post(ad);

                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }
    public interface Communicator {
        public void message(String data);
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }


}