package com.blackmaria;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.newdesigns.Forgetpin;
import com.blackmaria.newdesigns.Registeration;
import com.blackmaria.pojo.loginresponse;
import com.blackmaria.utils.AppInfoSessionManager;
import com.blackmaria.utils.ChatAvailabilityCheck;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CountryDialCode;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.Locationservices;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.xmpp.Getlocation;
import com.blackmaria.xmpp.XmppService;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.countrycodepicker.R.drawable;
import com.devspark.appmsg.AppMsg;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

public class LoginPage extends FragmentActivityHockeyApp implements MyDialogFragment.Communicator {
    /*private String[] number = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "", "0", ""};
    private int[] imageId = {R.drawable.button1, R.drawable.button2, R.drawable.button3, R.drawable.button4,
            R.drawable.button5, R.drawable.button6, R.drawable.button7, R.drawable.button8,
            R.drawable.button9, R.drawable.button_plus, R.drawable.button0, R.drawable.button_clear};*/

    LanguageDb mhelper;
    private TextView Tv_Confirm;
    //  private GridView Gv_mobileNo;
    private EditText Et_MobileNo, Et_Pin;
    private GPSTracker gpsTracker;
    //    private TextView Tv_HeaderText;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private Dialog dialog;

    private Handler mHandler;
    private SessionManager session;
    private TextView Tv_countryCode, tv_contentloading;
    private ServiceRequest mRequest;
    private String SdeviceToken = "";
    private CustomTextView Rl_countryCode;
    CountryPicker picker;
    private String SuserId = "", ScountryCode = "", SmobNo = "", Spage = "", Sotp_status = "", Sotp = "";
    final static int otp_request_code = 100;
    private LinearLayout tv_signup;
    TextView ly_forgetpin;

    ImageView img1, img2, img3, img4, img5, img6, img7, img8, img9, img0, img_clr, img_plus,settingsicon;
    int[] imageViews = {R.id.img1, R.id.img2, R.id.img3, R.id.img4, R.id.img5, R.id.img6, R.id.img7, R.id.img8, R.id.img9, R.id.img0, R.id.img_hash};

    String number = "", enumber = "", CountryCode = "";
TextView signinn;
    private ImageView Iv_flag, Iv_status;
    private SmoothProgressBar progressbar;
ImageView currentlanguega;
    private loginresponse loginpojo;
    private LinearLayout lv_bottomlayout;
    private boolean ismobile = true;
    private boolean isAppInfoAvailable = false;
    AppInfoSessionManager appInfo_Session;
    private PkDialog mDialogwhle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_layout);
        mhelper=new LanguageDb(this);
        startService(new Intent(this, Getlocation.class));
        startService(new Intent(this, Locationservices.class));
        initialize();


        currentlanguega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialogFragment addPhotoBottomDialogFragment =
                        MyDialogFragment.newInstance();
                addPhotoBottomDialogFragment.show(getSupportFragmentManager(),
                        "add_photo_dialog_fragment");
            }
        });
        ly_forgetpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginPage.this, Forgetpin.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        Et_Pin.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ismobile = false;
                lv_bottomlayout.setVisibility(View.VISIBLE);
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Et_Pin.getWindowToken(), 0);
                return false;
            }
        });

        Et_MobileNo.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ismobile = true;
                lv_bottomlayout.setVisibility(View.VISIBLE);
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Et_MobileNo.getWindowToken(), 0);
                return false;
            }
        });


        Tv_countryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                Tv_countryCode.setText(dialCode);
                CountryCode = dialCode;
                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                Iv_flag.setImageResource(getResId(drawableName));
                CloseKeyBoard();
            }
        });

        Tv_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!"Profile".equalsIgnoreCase(Spage)) {
                    if (Et_MobileNo.getText().toString().length() == 0) {
                        AlertError(getkey("action_error"), getkey("login_page_alert_phoneNoempty"));
                    } else if (!isValidPhoneNumber(Et_MobileNo.getText().toString())) {
                        AlertError(getkey("action_error"), getkey("login_page_alert_phoneNo"));
                    } else if (Tv_countryCode.getText().toString().equalsIgnoreCase("code")) {
                        AlertError(getkey("action_error"), getkey("login_page_alert_country_code"));
                    } else if (Et_Pin.getText().toString().length() == 0) {
                        AlertError(getkey("action_error"), getkey("profile_label_alert_pincode"));
                    } else if (Et_Pin.getText().toString().trim().length() != 6) {
                        AlertError(getkey("action_error"), getkey("profile_label_alert_pincodes"));
                    } else {
                        cd = new ConnectionDetector(LoginPage.this);
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
//                            mHandler.post(dialogRunnable);

                            SdeviceToken = Settings.Secure.getString(getContentResolver(),
                                    Settings.Secure.ANDROID_ID);

                            System.out.println("login---------" + Iconstant.loginurl);
                            PostRequest(Iconstant.loginurl_new);
//                                PostRequest("http://192.168.1.251:8081/dectar/customization/blackmaria/v6/api/v1/app/check-user");//Iconstant.loginurl);


                        } else {
                            Alert(getkey("alert_nointernet"), getkey("alert_nointernet_message"));
                        }
                    }
                } else {
                    if (!isValidPhoneNumber(Et_MobileNo.getText().toString())) {
                        AlertError(getkey("action_error"), getkey("login_page_alert_phoneNo"));
                    } else if (Tv_countryCode.getText().toString().equalsIgnoreCase("code")) {
                        AlertError(getkey("action_error"), getkey("login_page_alert_country_code"));
                    } else {
                        cd = new ConnectionDetector(LoginPage.this);
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            PostRequest_mobnoChange(Iconstant.Edit_profile_mobileNo_url);
                        } else {
                            Alert(getkey("alert_nointernet"), getkey("alert_nointernet_message"));
                        }
                    }
                }

            }
        });

        gpsTracker = new GPSTracker(LoginPage.this);
        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {

            double MyCurrent_lat = gpsTracker.getLatitude();
            double MyCurrent_long = gpsTracker.getLongitude();


            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    String code = "IN";
                    String Str_getCountryCode = addresses.get(0).getCountryCode();
                    if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode);
                      //  Tv_countryCode.setText(Str_countyCode);
                        CountryCode = Str_countyCode;
                        String drawableName = "flag_"
                                + code.toLowerCase(Locale.ENGLISH);
                        //Iv_flag.setImageResource(getResId(drawableName));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }




        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    number = number + "1";
                    Et_MobileNo.setText(number);
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                } else {
                    enumber = enumber + "1";
                    Et_Pin.setText(enumber);
                    Et_Pin.setSelection(Et_Pin.getText().length());
                }

                setbackground(img1, v);

            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    number = number + "2";
                    Et_MobileNo.setText(number);
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                } else {
                    enumber = enumber + "2";
                    Et_Pin.setText(enumber);
                    Et_Pin.setSelection(Et_Pin.getText().length());
                }

                setbackground(img2, v);
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    number = number + "3";
                    Et_MobileNo.setText(number);
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                } else {
                    enumber = enumber + "3";
                    Et_Pin.setText(enumber);
                    Et_Pin.setSelection(Et_Pin.getText().length());
                }
                setbackground(img3, v);

            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    number = number + "4";
                    Et_MobileNo.setText(number);
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                } else {
                    enumber = enumber + "4";
                    Et_Pin.setText(enumber);
                    Et_Pin.setSelection(Et_Pin.getText().length());
                }
                setbackground(img4, v);
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    number = number + "5";
                    Et_MobileNo.setText(number);
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                } else {
                    enumber = enumber + "5";
                    Et_Pin.setText(enumber);
                    Et_Pin.setSelection(Et_Pin.getText().length());
                }

                setbackground(img5, v);
            }
        });
        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    number = number + "6";
                    Et_MobileNo.setText(number);
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                } else {
                    enumber = enumber + "6";
                    Et_Pin.setText(enumber);
                    Et_Pin.setSelection(Et_Pin.getText().length());
                }

                setbackground(img6, v);
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    number = number + "7";
                    Et_MobileNo.setText(number);
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                } else {
                    enumber = enumber + "7";
                    Et_Pin.setText(enumber);
                    Et_Pin.setSelection(Et_Pin.getText().length());
                }

                setbackground(img7, v);
            }
        });
        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    number = number + "8";
                    Et_MobileNo.setText(number);
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                } else {
                    enumber = enumber + "8";
                    Et_Pin.setText(enumber);
                    Et_Pin.setSelection(Et_Pin.getText().length());
                }
                setbackground(img8, v);
            }
        });
        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    number = number + "9";
                    Et_MobileNo.setText(number);
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                } else {
                    enumber = enumber + "9";
                    Et_Pin.setText(enumber);
                    Et_Pin.setSelection(Et_Pin.getText().length());
                }
                setbackground(img9, v);
            }
        });
        img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    number = number + "0";
                    Et_MobileNo.setText(number);
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                } else {
                    enumber = enumber + "0";
                    Et_Pin.setText(enumber);
                    Et_Pin.setSelection(Et_Pin.getText().length());
                }
                setbackground(img0, v);
                //   no.getText().append("0");
            }
        });
        img_clr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ismobile) {
                    String SMobileNo = Et_MobileNo.getText().toString();
                    if (SMobileNo != null && SMobileNo.length() > 0) {
                        Et_MobileNo.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                        Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                        number = Et_MobileNo.getText().toString();
                    }
                } else {
                    String SMobileNo = Et_Pin.getText().toString();
                    if (SMobileNo != null && SMobileNo.length() > 0) {
                        Et_Pin.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                        Et_Pin.setSelection(Et_Pin.getText().length());
                        enumber = Et_Pin.getText().toString();
                    }
                }
                setbackground(img_clr, v);

            }
        });

        signinn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginPage.this, Registeration.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });

        settingsicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

       /* img_clr.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Et_MobileNo.setText("");

                return false;
            }
        });*/

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, Getlocation.class));
    }

    private void initialize() {
        session = new SessionManager(LoginPage.this);
        appInfo_Session = new AppInfoSessionManager(LoginPage.this);
        cd = new ConnectionDetector(LoginPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        mHandler = new Handler();
        picker = CountryPicker.newInstance(getkey("select_country_lable"));

        currentlanguega = findViewById(R.id.currentlanguega);
        String languageToLoad  = session.getCurrentlanguage(); // your language


        tv_signup = findViewById(R.id.lv_signup);
        ly_forgetpin = findViewById(R.id.ly_forgetpin);
        ly_forgetpin.setText(getkey("forgetpin"));
        Et_Pin = findViewById(R.id.edit_pinnumnber);
        Et_Pin.setHint(getkey("edit_pinnumber"));
        TextView language= findViewById(R.id.language);
        language.setText(getkey("language"));
        TextView donthaveacc= findViewById(R.id.donthaveacc);
        donthaveacc.setText(getkey("donothaveacoount"));

        TextView signsinn = findViewById(R.id.signsinn);
        signsinn.setText(getkey("signinnn"));

        Et_MobileNo = findViewById(R.id.edt_mobile_no);
        Et_MobileNo.setHint(getkey("edit_mobilenumber"));

        Tv_countryCode = findViewById(R.id.txt_country_code);
        signinn = findViewById(R.id.signinn);
        signinn.setText(getkey("signup"));
        tv_contentloading = findViewById(R.id.loading_content);
        lv_bottomlayout = findViewById(R.id.lv_bottomlayout);
        settingsicon= findViewById(R.id.settingsicon);
        // Rl_countryCode = (CustomTextView) findViewById(R.id.login_page_country_code_layout);
        //       Gv_mobileNo = (GridView) findViewById(R.id.login_page_mobile_no_gridview);
        Tv_Confirm = (TextView) findViewById(R.id.txt_go);
        Tv_Confirm.setText(getkey("login"));
        progressbar = (SmoothProgressBar) findViewById(R.id.loading);
//        Tv_HeaderText = (TextView) findViewById(R.id.login_page_header_textview);

        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        img3 = (ImageView) findViewById(R.id.img3);
        img4 = (ImageView) findViewById(R.id.img4);
        img5 = (ImageView) findViewById(R.id.img5);
        img6 = (ImageView) findViewById(R.id.img6);
        img7 = (ImageView) findViewById(R.id.img7);
        img8 = (ImageView) findViewById(R.id.img8);
        img9 = (ImageView) findViewById(R.id.img9);
        img0 = (ImageView) findViewById(R.id.img0);
        img_clr = (ImageView) findViewById(R.id.img_hash);

        Iv_flag = (ImageView) findViewById(R.id.img_flag);
        Iv_status = (ImageView) findViewById(R.id.img_status);

        ImageView Rl_drawer = (ImageView) findViewById(R.id.drawer_icon);
       /*GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(Rl_drawer);
        Glide.with(this).load(R.drawable.alertgif).into(imageViewTarget);*/
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);


        InputMethodManager mgrs = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgrs.hideSoftInputFromWindow(Et_Pin.getWindowToken(), 0);

        // close keyboard
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(Et_MobileNo.getWindowToken(), 0);

       /* LoginPageAdapter adapter = new LoginPageAdapter(LoginPage.this, number, imageId);
        Gv_mobileNo.setAdapter(adapter);*/


        Intent intent = getIntent();
        if (intent.hasExtra("page")) {
            SuserId = intent.getStringExtra("userId");
            ScountryCode = intent.getStringExtra("countrycode");
            SmobNo = intent.getStringExtra("MobNo");
            Spage = intent.getStringExtra("page");

          //  Tv_countryCode.setText(ScountryCode);
            Et_MobileNo.setText(SmobNo);
            Et_MobileNo.setSelection(Et_MobileNo.getText().length());

        }
        gpsTracker = new GPSTracker(LoginPage.this);


    }

    private int getResId(String drawableName) {

        try {
            Class<drawable> res = drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5 || target.length() > 12) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }


    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(LoginPage.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(LoginPage.this, R.anim.shake);
        editname.startAnimation(shake);
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    //--------Handler Method------------
    Runnable dialogRunnable = new Runnable() {
        @Override
        public void run() {
            dialog = new Dialog(LoginPage.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_loading);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
    };

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        if (mDialogwhle != null) {
            mDialogwhle.dismiss();
        }
        mDialogwhle = new PkDialog(LoginPage.this);
        mDialogwhle.setDialogTitle(title);
        mDialogwhle.setDialogMessage(alert);
        mDialogwhle.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogwhle.dismiss();
                dialogDismiss();
            }
        });
        mDialogwhle.show();
    }

    private void Alertregister(String title, String alert) {
        if (mDialogwhle != null) {
            mDialogwhle.dismiss();
        }
        mDialogwhle = new PkDialog(LoginPage.this);
        mDialogwhle.setDialogTitle(title);
        mDialogwhle.setDialogMessage(alert);
        mDialogwhle.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogwhle.dismiss();
                dialogDismiss();

                Intent intent = new Intent(LoginPage.this, Registeration.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            }
        });
        mDialogwhle.show();
        getResources().getString(getResources().getIdentifier("footerLink1", "string", getPackageName()));
    }


    private void PostRequest(String Url) {
        progressbar.setVisibility(View.VISIBLE);
        tv_contentloading.setVisibility(View.GONE);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("phone_number", Et_MobileNo.getText().toString());
        jsonParams.put("country_code", Tv_countryCode.getText().toString());
        jsonParams.put("pin_number", Et_Pin.getText().toString().trim());
        jsonParams.put("deviceToken", SdeviceToken);
        jsonParams.put("gcm_id", FirebaseInstanceId.getInstance().getToken());
        jsonParams.put("lat", String.valueOf(gpsTracker.getLatitude()));
        jsonParams.put("lon", String.valueOf(gpsTracker.getLongitude()));


        System.out.println("-----------Login jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(LoginPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                loginpojo = new loginresponse();
                Log.e("registr", response);

                System.out.println("--------------Login reponse-------------------" + response);

                String Sstatus = "", gender = "", username = "", sCurrencySymbol = "", Smessage = "", email = "", ScurrencyCode = "", SreferalStatus = "", errorStatus = "", userId = "", accountExit = "", errorMessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    Type listType = new TypeToken<loginresponse>() {
                    }.getType();
                    loginpojo = new GsonBuilder().create().fromJson(object.toString(), listType);

                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (!loginpojo.getUser_image().equals("")) {
                            session.setUserImageUpdate(loginpojo.getUser_image());
                        }
                        Sotp_status = object.getString("otp_status");
                        Sotp = object.getString("otp");
                        errorStatus = object.getString("error_status");
                        errorMessage = object.getString("error_message");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    progressbar.setVisibility(View.GONE);
                    tv_contentloading.setVisibility(View.GONE);
//                    dialogDismiss();
                }

//                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {
                    if (errorStatus.equalsIgnoreCase("1")) {
                        Alert(getkey("action_error"), errorMessage);
                        progressbar.setVisibility(View.GONE);
                        tv_contentloading.setVisibility(View.GONE);
                    } else {
                        progressbar.setVisibility(View.GONE);
                        tv_contentloading.setVisibility(View.GONE);

                        //otp no need
                        session.setReferalStatus(loginpojo.getReferral_staus());
                        session.setCurrency(loginpojo.getCurrency());

                        if (loginpojo.getError_status().equalsIgnoreCase("0")) {
                            session.createLoginSession("", loginpojo.getUser_id(), loginpojo.getUser_name(), "", loginpojo.getCountry_code(), loginpojo.getPhone_number(), "", loginpojo.getCategory(), loginpojo.getKey());
                            session.setXmppKey(loginpojo.getUser_id(), loginpojo.getSec_key());
                            postRequest_applaunch(Iconstant.getAppInfo);
                        }
                    }
                } else if (Sstatus.equalsIgnoreCase("2")) {
                    progressbar.setVisibility(View.GONE);
                    tv_contentloading.setVisibility(View.GONE);

                    Alertregister(getkey("action_error"), getkey("userneedtoberesigetr"));
                } else {
                    progressbar.setVisibility(View.GONE);
                    tv_contentloading.setVisibility(View.GONE);
                    Alert(getkey("action_error"), Smessage);
                }
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Et_MobileNo.getWindowToken(), 0);
                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    //----------------------------------Mobile number change----------------------------------------------
    private void PostRequest_mobnoChange(String Url) {

        dialog = new Dialog(LoginPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("phone_number", Et_MobileNo.getText().toString());
        jsonParams.put("country_code", Tv_countryCode.getText().toString());
        jsonParams.put("user_id", SuserId);
        System.out.println("-----------Mobile number change jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(LoginPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                Log.e("registr", response);

                System.out.println("--------------Mobile number change reponse-------------------" + response);

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Sotp_status = object.getString("otp_status");
                        Sotp = object.getString("otp");
                        dialogDismiss();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    Intent intent = new Intent(LoginPage.this, OtpPage.class);
                    intent.putExtra("Otp_Status", Sotp_status);
                    intent.putExtra("Otp", Sotp);
                    intent.putExtra("Phone", Et_MobileNo.getText().toString());
                    intent.putExtra("CountryCode", Tv_countryCode.getText().toString());
                    intent.putExtra("userId", SuserId);
                    intent.putExtra("page", "Profile");
                    startActivityForResult(intent, otp_request_code);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    dialogDismiss();
                } else {
                    Alert(getkey("action_error"), Smessage);
                    dialogDismiss();
                }
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Et_MobileNo.getWindowToken(), 0);
                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    //------------------------OnActivity Result-------------------------


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case otp_request_code:

                if (requestCode == otp_request_code && resultCode == Activity.RESULT_OK && data != null) {

                    String Scountrycode = data.getStringExtra("countryCode");
                    String SmobNo = data.getStringExtra("phoneNo");
                    String Smessage = data.getStringExtra("message");
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("countryCode", Scountrycode);
                    returnIntent.putExtra("phoneNo", SmobNo);
                    returnIntent.putExtra("message", Smessage);
                    setResult(RESULT_OK, returnIntent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();

                }
                break;
        }


        super.onActivityResult(requestCode, resultCode, data);
    }


    private void CloseKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

       /* View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }*/
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
      /*  InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);*/
        return true;
    }

    @Override
    protected void onPause() {
        dialogDismiss();
        super.onPause();
    }

    @Override
    protected void onResume() {
        dialogDismiss();
        super.onResume();
        session.setnightMode(false);
    }




    private void postRequest_applaunch(String Url) {
        progressbar.setVisibility(View.VISIBLE);
        System.out.println("-------------Otp App Information Url----------------" + Url);
        HashMap<String, String> user = session.getUserDetails();
        SuserId = user.get(SessionManager.KEY_USERID);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "user");
        jsonParams.put("id", SuserId);
        jsonParams.put("lat", String.valueOf(gpsTracker.getLatitude()));
        jsonParams.put("lon", String.valueOf(gpsTracker.getLongitude()));
        System.out.println("-------------Otp App Information jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(LoginPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Otp App Information Response----------------" + response);


                String userRating = "", app_identity_name = "", termsCondition = "", Language_code = "", profile_complete = "", phone_masking_status = "", Str_status = "", SuserImage = "", userGuide = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                try {
                    progressbar.setVisibility(View.GONE);
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                session.setCustomerServiceNo(sCustomerServiceNumber);
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                                app_identity_name = info_object.getString("app_identity_name");
                              /*  sFacebookId = info_object.getString("facebook_app_id");
                                sGooglePlusId = info_object.getString("google_plus_app_id");
                                sPhoneMasking = info_object.getString("phone_masking_status");*/
                                Language_code = info_object.getString("lang_code");
                                profile_complete = info_object.getString("profile_complete");
                                phone_masking_status = info_object.getString("phone_masking_status");
                                String phantomcar = info_object.getString("phantom_car");
                                SuserImage = info_object.getString("user_image");
                                session.setPhantomcar(phantomcar);

                                termsCondition = info_object.getString("terms_condition");
                                /*server_mode = info_object.getString("server_mode");
                                site_mode = info_object.getString("site_mode");
                                site_string = info_object.getString("site_mode_string");
                                site_url = info_object.getString("site_url");*/
                                if (info_object.has("cashback_mode")) {
                                    session.setCashbackStatus(info_object.getString("cashback_mode"));
                                }

                                try {
                                    JSONArray jsonarray_emergendy_no = info_object.getJSONArray("emergencyNumbers");
                                    for (int i = 0; i <= jsonarray_emergendy_no.length() - 1; i++) {
                                        String value = jsonarray_emergendy_no.getJSONObject(i).getString("title");
                                        if (value.equalsIgnoreCase("Police")) {
                                            session.setPolice(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Fire")) {
                                            session.setFire(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Ambulance")) {
                                            session.setAmbulance(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        }
                                    }

                                } catch (Exception e) {

                                }


                                if (info_object.has("with_pincode")) {
                                    session.setSecurePin(info_object.getString("with_pincode"));
                                }
                                if (info_object.has("avg_review")) {
                                    userRating = info_object.getString("avg_review");
                                }
                                if (info_object.has("currency")) {
                                    String currency = info_object.getString("currency");
                                    session.setCurrency(currency);
                                }
                                if (info_object.has("user_guide")) {
                                    userGuide = info_object.getString("user_guide");
                                    session.setUserGuidePdf(userGuide);
                                }
                                if (info_object.has("ride_earn")) {
                                    session.setReferelStatus(info_object.getString("ride_earn"));
                                }

                                isAppInfoAvailable = true;
                            } else {
                                isAppInfoAvailable = false;
                            }

                           /* sPendingRideId= response_object.getString("pending_rideid");
                            sRatingStatus= response_object.getString("ride_status");*/

                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }
                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {

                        appInfo_Session.setAppInfo(sContact_mail, sCustomerServiceNumber, sSiteUrl, sXmppHostUrl, sHostName, sFacebookId, sGooglePlusId, "", "", "", "", "", app_identity_name, userRating, SuserImage);
                        HashMap<String, String> language = session.getLanaguage();
                        Locale locale = null;




                        session.setXmpp(sXmppHostUrl, sHostName);
                        session.setAgent(app_identity_name);
                        session.setPhoneMaskingStatus(phone_masking_status);


                        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {

                        } else {
                            dialogDismiss();
                        }


                        if (gpsTracker.getLatitude() != 0.0) {
                            postRequest_SetUserLocation(Iconstant.setUserLocation);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }

    private void postRequest_SetUserLocation(String Url) {
        progressbar.setVisibility(View.VISIBLE);
        gpsTracker = new GPSTracker(LoginPage.this);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", SuserId);
        jsonParams.put("latitude", String.valueOf(gpsTracker.getLatitude()));
        jsonParams.put("longitude", String.valueOf(gpsTracker.getLongitude()));


        System.out.println("-------------Otp SetUserLocation jsonParams----------------" + jsonParams);

        System.out.println("-------------Otp UserLocation Url----------------" + Url);
        mRequest = new ServiceRequest(LoginPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Otp UserLocation Response----------------" + response);
                progressbar.setVisibility(View.GONE);
                String Str_status = "", sCategoryID = "", sTripProgress = "", sRideId = "", sRideStage = "";
                try {
                    JSONObject object = new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialogDismiss();

                if ("0".equalsIgnoreCase(loginpojo.getProfile_complete())) {
                    //Starting the xmpp service
                    if (!isMyServiceRunning(XmppService.class)) {
                        System.out.println("-----------OtpPage xmpp service start1---------");
                        startService(new Intent(LoginPage.this, XmppService.class));
                    }
                    ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(LoginPage.this, "available");
                    chatAvailability.postChatRequest();
//new
                    session.setUserloggedIn(true);
                    Intent intent = new Intent(LoginPage.this, Navigation_new.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("image", loginpojo.getUser_image());
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                } else {

                    //Starting the xmpp service
                    if (!isMyServiceRunning(XmppService.class)) {
                        System.out.println("-----------OtpPage xmpp service start2---------");
                        startService(new Intent(LoginPage.this, XmppService.class));
                    }
                    ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(LoginPage.this, "available");
                    chatAvailability.postChatRequest();
                    Intent intent = new Intent(LoginPage.this, Navigation_new.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("image", loginpojo.getUser_image());
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }

            }

            @Override
            public void onErrorListener() {
                Toast.makeText(LoginPage.this, "Server error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }


    private void setbackground(final ImageView image, View view) {

        ImageView iv = (ImageView) view;
        final Drawable d = iv.getBackground();
        image.setBackground(getResources().getDrawable(R.drawable.rect_green));
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                image.setBackground(d.getCurrent());
            }
        }, 100);

//        for (int i = 0; i <= imageViews.length - 1; i++) {
//            final TextView img = findViewById(imageViews[i]);
//            if (image == imageViews[i]) {
//                img.setBackground(getResources().getDrawable(R.drawable.rect_green));
//                final Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        //Do something after 100ms
//                        img.setBackground(d.getCurrent());
//                    }
//                }, 100);
//            }
//        }
    }

    @Override
    public void message(String data) {
        String languageToLoad  = data; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        session.setCurrentlanguage(data);

        Intent intent = new Intent(LoginPage.this, SplashPage1.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }
}


