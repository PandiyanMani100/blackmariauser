package com.blackmaria;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.widgets.TextRCLight;
import com.devspark.appmsg.AppMsg;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

/**
 * Created by Muruganantham on 12/28/2017.
 */

@SuppressLint("Registered")
public class NewProfile_Page_CardEdit extends ActivityHockeyApp implements View.OnClickListener {
    private ServiceRequest mRequest;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private RefreshReceiver refreshReceiver;
    private SessionManager session;


    private RelativeLayout cardEditLyHeader;
    private ImageView ScanCard,cancelLayout;
    private LinearLayout myloginDialogFormWholeLayout;
    private CustomEdittext cardDetailsCardNumber;
    private RelativeLayout dateEt1;
    private Spinner spinner2;
    private TextRCLight months;
    private RelativeLayout dateEt2;
    private Spinner spinner3;
    private TextRCLight yearss;
    private CustomEdittext cardDetailsCvvNumber;
    private CustomEdittext cardDetailsHolderName;
    private RelativeLayout cardInformationDetailsConfirmCancel;
    private RelativeLayout cardInformationDetailsConfirm;
    private SmoothProgressBar profileLoadingProgressbar;
    private TextView loadingTv;
    private String expYear = "", expMonth = "", cardNumber = "", card_info = "", card_id = "",UserID="";
    private int expYearNum = 0, expMnthNUm = 0;
    private static final int MY_SCAN_REQUEST_CODE = 100;
    private String scanCardNumber = "", scanCardCVV = "", scanExpiryMonth = "", scanExpiryYear = "";

    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {

                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");


                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewProfile_Page_CardEdit.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewProfile_Page_CardEdit.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            } else if (intent.getAction().equals("com.package.ACTION_CLASS_complaint_replay_notification")) {
                if (intent.getExtras() != null) {
                    String ticketId = (String) intent.getExtras().get("ticket_id");
                    String userID = (String) intent.getExtras().get("user_id");
                    String message = (String) intent.getExtras().get("message");
                    String dateTime = (String) intent.getExtras().get("date_time");

                    Intent intent1 = new Intent(NewProfile_Page_CardEdit.this, ComplaintPageViewDetailPage.class);
                    intent1.putExtra("ticket_id", ticketId);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_card_details_edit1);
        initialize();
        OtherClickListeners();
    }



    private void initialize() {
        session = new SessionManager(NewProfile_Page_CardEdit.this);
        cd = new ConnectionDetector(NewProfile_Page_CardEdit.this);
        isInternetPresent = cd.isConnectingToInternet();
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);
        cardEditLyHeader = (RelativeLayout) findViewById(R.id.card_edit_ly_header);
        cancelLayout = (ImageView) findViewById(R.id.cancel_layout);
        myloginDialogFormWholeLayout = (LinearLayout) findViewById(R.id.mylogin_dialog_form_whole_layout);
        cardDetailsCardNumber = (CustomEdittext) findViewById(R.id.card_details_card_number);
        dateEt1 = (RelativeLayout) findViewById(R.id.date_et1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        months = (TextRCLight) findViewById(R.id.month);
        dateEt2 = (RelativeLayout) findViewById(R.id.date_et2);
        spinner3 = (Spinner) findViewById(R.id.spinner3);
        yearss = (TextRCLight) findViewById(R.id.year);
        cardDetailsCvvNumber = (CustomEdittext) findViewById(R.id.card_details_cvv_number);
        cardDetailsHolderName = (CustomEdittext) findViewById(R.id.card_details_holder_name);
        cardInformationDetailsConfirmCancel = (RelativeLayout) findViewById(R.id.card_information_details_confirm_cancel);
        cardInformationDetailsConfirm = (RelativeLayout) findViewById(R.id.card_information_details_confirm);
        profileLoadingProgressbar = (SmoothProgressBar) findViewById(R.id.profile_loading_progressbar);
        loadingTv = (TextView) findViewById(R.id.loading_tv);
        ScanCard = (ImageView) findViewById(R.id.scancard_iv);
        cancelLayout.setOnClickListener(this);
        cardInformationDetailsConfirmCancel.setOnClickListener(this);
        cardInformationDetailsConfirm.setOnClickListener(this);
        ScanCard.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == cancelLayout) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == cardInformationDetailsConfirmCancel) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == cardInformationDetailsConfirm) {
            AddCard();
        }else if (v == ScanCard) {
            ScanCardProfile();
        }
    }



    private void OtherClickListeners() {
        spinner_month_and_year_process();
    }
    private void AddCard() {

        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        int thismnth = Calendar.getInstance().get(Calendar.MONTH);
        cardNumber = cardDetailsCardNumber.getText().toString();
        if (cardNumber.length() == 0) {
            AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no));
        } else if (cardDetailsCardNumber.length() < 16) {
            AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_no_enter));
        } else if (cardDetailsHolderName.getText().toString().length() <= 0) {
            AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_card_holder_name));
        } else if (cardDetailsCvvNumber.getText().toString().length() < 3) {
            AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_cvv_code));
        } else if (expMnthNUm < thismnth && expYearNum == thisYear) {
            AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_validexp_mth));
        } else if (expYearNum < thisYear) {
            AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.valid_exp_yrr));
        } else if (expMonth.length() < 0 || expYear.length() < 0) {

        } else {
            profileLoadingProgressbar.setVisibility(View.VISIBLE);
            loadingTv.setVisibility(View.VISIBLE);
            card_info = formCardInfo();

            if (card_info.length() > 0) {
                String exp_month1 = expMonth; //spnr_exp_month.getSelectedItem().toString();
                String exp_year1 = expYear;//ed_exp_yr.getSelectedItem().toString().trim();
                String card_number = cardDetailsCardNumber.getText().toString();
                String cvv = cardDetailsCvvNumber.getText().toString().trim();
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);
                jsonParams.put("card_number", card_number);
                jsonParams.put("exp_month", exp_month1);
                jsonParams.put("exp_year", exp_year1);
                jsonParams.put("cvc_number", cvv);
                // jsonParams.put("card_info", card_info);
                if (card_id.length() > 0) {
                    jsonParams.put("card_id", card_id);
                }
                AddOrChangeCard(Iconstant.creditcard_stripe,jsonParams);
            }
            CloseKeyboard(cardDetailsCvvNumber);
            //CreateXenditToken(eta_amount);
        }
    }

    private void spinner_month_and_year_process() {


        String[] month = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        ArrayAdapter<CharSequence> monthAdapter = new ArrayAdapter<CharSequence>(NewProfile_Page_CardEdit.this, R.layout.spinner_text, month);
        monthAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        spinner2.setAdapter(monthAdapter);

        String thisMonth = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);


        if (thisMonth.length() == 1) {
            thisMonth = "0" + thisMonth;
        }


        for (int j = 0; j <= month.length; j++) {

            if (month[j].equalsIgnoreCase(thisMonth)) {
                spinner2.setSelection(j);
                break;

            }

        }
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                expMonth = parent.getSelectedItem().toString();
                expMnthNUm = Integer.parseInt(expMonth);
                months.setText(expMonth);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                months.setText("month");
            }
        });


        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);


        for (int i = thisYear; i <= thisYear + 40; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(NewProfile_Page_CardEdit.this, R.layout.spinner_text, years);
        yearAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        spinner3.setAdapter(yearAdapter);
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                expYear = parent.getSelectedItem().toString();
                expYearNum = Integer.parseInt(expYear);
                yearss.setText(expYear);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                yearss.setText("year");
            }
        });
    }
    private String formCardInfo() {

        String exp_month = expMonth;
        String exp_year = expYear;

        String card_number = cardDetailsCardNumber.getText().toString().trim();
        String cvv = cardDetailsCvvNumber.getText().toString().trim();

        JSONObject jobjCardInfo = null;

        try {
            jobjCardInfo = new JSONObject();
            jobjCardInfo.put("card_number", card_number);
            jobjCardInfo.put("exp_month", exp_month);
            jobjCardInfo.put("exp_year", exp_year);
            jobjCardInfo.put("cvc_number", cvv);

            byte[] data = jobjCardInfo.toString().getBytes("UTF-8");
            System.out.println("CARD INFO = " + Base64.encodeToString(data, Base64.DEFAULT));
            return Base64.encodeToString(data, Base64.DEFAULT);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "";

    }
    private void AddOrChangeCard(String url,  HashMap<String, String> jsonParams) {
        profileLoadingProgressbar.setVisibility(View.VISIBLE);
        loadingTv.setVisibility(View.VISIBLE);
        System.out.println("=========Add or Change Card URL===========" + url);
        System.out.println("=========Add or Change Card Params===========" + jsonParams);

        mRequest = new ServiceRequest(NewProfile_Page_CardEdit.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("=========Add or Change Card Response===========" + response);
                String Sstatus = "", Sresponse = "";

                try {
                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        profileLoadingProgressbar.setVisibility(View.GONE);
                        loadingTv.setVisibility(View.GONE);

                        AlertCardAdded(getResources().getString(R.string.action_success), Sresponse);

                    } else {

                        profileLoadingProgressbar.setVisibility(View.GONE);
                        loadingTv.setVisibility(View.GONE);
                        Alert1(getResources().getString(R.string.alert_label_title), Sresponse);

                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {

                profileLoadingProgressbar.setVisibility(View.GONE);
                loadingTv.setVisibility(View.GONE);
            }
        });

    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(NewProfile_Page_CardEdit.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();

    }

    //--------------Alert Method-----------
    private void AlertCardAdded(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewProfile_Page_CardEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent local = new Intent();
                local.setAction("com.app.PaymentListPage.refreshPage");
//                local.putExtra("refresh", "yes");
                sendBroadcast(local);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }
    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_CardEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.ok_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();


            }
        });
        mDialog.show();
    }
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
    private void ScanCardProfile() {
        Intent scanIntent = new Intent(NewProfile_Page_CardEdit.this, CardIOActivity.class);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_SCAN_EXPIRY, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false);
        scanIntent.putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, Color.GREEN);
        scanIntent.putExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false);
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }
    @Override
    public void onDestroy() {
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                scanCardNumber = scanResult.getRedactedCardNumber();

                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                    scanExpiryMonth = String.valueOf(scanResult.expiryMonth);
                    scanExpiryYear = String.valueOf(scanResult.expiryYear);
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                    scanCardCVV = String.valueOf(scanResult.cvv);
                }

                cd = new ConnectionDetector(NewProfile_Page_CardEdit.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("user_id", UserID);
                    jsonParams.put("card_number", scanCardNumber);
                    jsonParams.put("exp_month", scanExpiryMonth);
                    jsonParams.put("exp_year", scanExpiryYear);
                    jsonParams.put("cvc_number", scanCardCVV);
                    // jsonParams.put("card_info", card_info);
                    if (card_id.length() > 0) {
                        jsonParams.put("card_id", "");
                    }
                    AddOrChangeCard(Iconstant.creditcard_stripe,jsonParams);
                } else {

                    Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }


            } else {
                resultDisplayStr = "Scan was canceled.";
            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
            System.out.println("---------------resultDisplayStr---------------" + resultDisplayStr);
        }


    }
}
