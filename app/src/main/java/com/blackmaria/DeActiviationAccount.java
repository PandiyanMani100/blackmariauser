package com.blackmaria;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.blackmaria.adapter.DeActivationListStatementAdapter;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.RoundedImageView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

/**
 * Created by user144 on 8/29/2017.
 */

public class DeActiviationAccount extends ActivityHockeyApp implements View.OnClickListener {
    private ListView LvLastStatement;
    private TextView txt_total_amount;
    private TextView  RL_close_account;
    private TextView  closesummary;
LanguageDb mhelper;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "",  value = "";
    ArrayList<String> arrTitle = null;
    ArrayList<String> arrValue = null;
    private DeActivationListStatementAdapter adapter;
    private ImageView img_back;

    final Handler handler = new Handler();

    private RoundedImageView iv_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deactivationloading_constrain);
        mhelper = new LanguageDb(this);
        Iinitialize();
    }

    private void Iinitialize() {
        cd = new ConnectionDetector(DeActiviationAccount.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(DeActiviationAccount.this);
        LvLastStatement = (ListView) findViewById(R.id.Lv_last_statement);
        RL_close_account = findViewById(R.id.RL_close_account);

        closesummary = findViewById(R.id.closesummary);

        TextView ins = findViewById(R.id.ins);
        ins.setText(getkey("acccountclosecontent"));

        TextView accountsummary = findViewById(R.id.accountsummary);
        accountsummary.setText(getkey("account_summary"));

        TextView txt_label_total_amount = findViewById(R.id.txt_label_total_amount);
        txt_label_total_amount.setText(getkey("total_amount"));

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int year = calendar.get(Calendar.YEAR);
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        String month_name = month_date.format(calendar.getTime());
        closesummary.setText(getkey("close_aacount")+ currentDay + " " + month_name + " " + year);
        img_back = findViewById(R.id.img_back);
        txt_total_amount = findViewById(R.id.txt_total_amount);
        iv_user = findViewById(R.id.iv_user);
        String userImage = session.getUserImageUpdate();
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.new_no_user_img)
                .error(R.drawable.new_no_user_img)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(this).load(userImage).apply(options).into(iv_user);
        RL_close_account.setText(getkey("ccontinue_btn"));
        RL_close_account.setOnClickListener(this);
        img_back.setOnClickListener(this);
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_USERID);

        setValues();
    }

    private void setValues() {
        value = getIntent().getStringExtra("strTotalAmountValues");
        arrTitle = getIntent().getStringArrayListExtra("arrDeActTitle");
        arrValue = getIntent().getStringArrayListExtra("arrDeActValue");

        txt_total_amount.setText(value);
        adapter = new DeActivationListStatementAdapter(DeActiviationAccount.this, arrTitle, arrValue);
        LvLastStatement.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if (v == RL_close_account) {
            if (isInternetPresent) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);
                Intent i  = new Intent(DeActiviationAccount.this, Close_creditwithus.class);
                i.putExtra("amount",value);
                startActivity(i);
            } else {
                Alert(getkey("action_error"), getkey("alert_nointernet"));
            }
        } else if (v == img_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(DeActiviationAccount.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
