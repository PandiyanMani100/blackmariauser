package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Waitingtimeurl implements Serializable{

    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("id")
        private String id;

        public String getVehicle_model() {
            return vehicle_model;
        }

        public void setVehicle_model(String vehicle_model) {
            this.vehicle_model = vehicle_model;
        }

        @SerializedName("vehicle_model")
        private String vehicle_model;
        @SerializedName("name")
        private String name;
        @SerializedName("phone")
        private String phone;
        @SerializedName("vehicle_no")
        private String vehicle_no;
        @SerializedName("driver_image")
        private String driver_image;
        @SerializedName("cat_image")
        private String cat_image;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        @SerializedName("currency")
        private String currency;
        @SerializedName("waiting_time")
        Waiting_time Waiting_timeObject;




        // Getter Methods

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getPhone() {
            return phone;
        }

        public String getVehicle_no() {
            return vehicle_no;
        }

        public String getDriver_image() {
            return driver_image;
        }

        public String getCat_image() {
            return cat_image;
        }

        public Waiting_time getWaiting_time() {
            return Waiting_timeObject;
        }

        // Setter Methods

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public void setVehicle_no(String vehicle_no) {
            this.vehicle_no = vehicle_no;
        }

        public void setDriver_image(String driver_image) {
            this.driver_image = driver_image;
        }

        public void setCat_image(String cat_image) {
            this.cat_image = cat_image;
        }

        public void setWaiting_time(Waiting_time waiting_timeObject) {
            this.Waiting_timeObject = waiting_timeObject;
        }
        public class Waiting_time {
            @SerializedName("free_min")
            private String free_min;
            @SerializedName("per_min")
            private String per_min;


            // Getter Methods

            public String getFree_min() {
                return free_min;
            }

            public String getPer_min() {
                return per_min;
            }

            // Setter Methods

            public void setFree_min(String free_min) {
                this.free_min = free_min;
            }

            public void setPer_min(String per_min) {
                this.per_min = per_min;
            }
        }
    }
}


