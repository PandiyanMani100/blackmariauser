package com.blackmaria.pojo;

/**
 * Created by Prem Kumar and Anitha on 10/23/2015.
 */
public class WalletMoneyTransactionPojo {
    private String trans_type;
    private String trans_amount;
    private String title;
    private String trans_date;
    private String balance_amount;
    private String currencySymbol;
    private String strDate;
    private String refereImage;
    private String refereType;
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRefereRideId() {
        return refereRideId;
    }

    public void setRefereRideId(String refereRideId) {
        this.refereRideId = refereRideId;
    }

    private String refereRideId;
    public String getRefereType() {
        return refereType;
    }

    public void setRefereType(String refereType) {
        this.refereType = refereType;
    }

    public String getReferalUserId() {
        return referalUserId;
    }

    public void setReferalUserId(String referalUserId) {
        this.referalUserId = referalUserId;
    }

    private String referalUserId;

    public String getRefereImage() {
        return refereImage;
    }

    public void setRefereImage(String refereImage) {
        this.refereImage = refereImage;
    }














    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }


    public String getCredittype() {
        return credittype;
    }

    public void setCredittype(String credittype) {
        this.credittype = credittype;
    }

    private String credittype;
    public String getTrans_type() {
        return trans_type;
    }

    public void setTrans_type(String trans_type) {
        this.trans_type = trans_type;
    }

    public String getTrans_amount() {
        return trans_amount;
    }

    public void setTrans_amount(String trans_amount) {
        this.trans_amount = trans_amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrans_date() {
        return trans_date;
    }

    public String getTransactionTime() {
        return TransactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        TransactionTime = transactionTime;
    }

    String TransactionTime;
    public void setTrans_date(String trans_date) {
        this.trans_date = trans_date;
    }

    public String getBalance_amount() {
        return balance_amount;
    }

    public void setBalance_amount(String balance_amount) {
        this.balance_amount = balance_amount;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
}
