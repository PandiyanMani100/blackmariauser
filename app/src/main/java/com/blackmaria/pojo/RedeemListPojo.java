package com.blackmaria.pojo;

/**
 * Created by user14 on 12/19/2016.
 */

public class RedeemListPojo {

    private String check_status,distance_range,reward_id,gift_name;

    public String getCheck_status() {
        return check_status;
    }

    public void setCheck_status(String check_status) {
        this.check_status = check_status;
    }

    public String getDistance_range() {
        return distance_range;
    }

    public void setDistance_range(String distance_range) {
        this.distance_range = distance_range;
    }

    public String getGift_name() {
        return gift_name;
    }

    public void setGift_name(String gift_name) {
        this.gift_name = gift_name;
    }

    public String getReward_id() {
        return reward_id;
    }

    public void setReward_id(String reward_id) {
        this.reward_id = reward_id;
    }
}
