package com.blackmaria.pojo;

/**
 * Created by user14 on 3/14/2017.
 */

public class BookingPojo1 {
    private String distance,brand_image;


    public String getBrand_image() {
        return brand_image;
    }

    public void setBrand_image(String brand_image) {
        this.brand_image = brand_image;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
