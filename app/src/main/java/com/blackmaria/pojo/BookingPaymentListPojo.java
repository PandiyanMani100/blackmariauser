package com.blackmaria.pojo;

/**
 * Created by user144 on 12/19/2017.
 */

public class BookingPaymentListPojo {
    String name;
    String icon;
    String inactiveIcon;
    String status;
    String code;

    String card_number;
    String exp_month;
    String exp_year;
    String card_type;
    String customer_id;
    String card_id;
    String card_image;

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }


    public String getExp_month() {
        return exp_month;
    }

    public void setExp_month(String exp_month) {
        this.exp_month = exp_month;
    }


    public String getExp_year() {
        return exp_year;
    }

    public void setExp_year(String exp_year) {
        this.exp_year = exp_year;
    }


    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }


    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }


    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }


    public String getCard_image() {
        return card_image;
    }

    public void setCard_image(String card_image) {
        this.card_image = card_image;
    }





    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }




    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getInactiveIcon() {
        return inactiveIcon;
    }

    public void setInactiveIcon(String inactiveIcon) {
        this.inactiveIcon = inactiveIcon;
    }


    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
