package com.blackmaria.pojo;

import java.io.Serializable;

/**
 * Created by Prem Kumar and Anitha on 11/2/2015.
 */
public class CancelTripPojo implements Serializable
{
    private String reasonId, reason;

    public String getSetPayment_selected_cancel_id() {
        return setPayment_selected_cancel_id;
    }

    public void setSetPayment_selected_cancel_id(String setPayment_selected_cancel_id) {
        this.setPayment_selected_cancel_id = setPayment_selected_cancel_id;
    }

    String setPayment_selected_cancel_id;
    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
