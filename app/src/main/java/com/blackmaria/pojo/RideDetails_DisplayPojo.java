package com.blackmaria.pojo;

/**
 * Created by user14 on 12/14/2016.
 */

public class RideDetails_DisplayPojo {
    private String rate_title, rate_value, rate_currencySymbol;
    private String farerate_currencySymbol; private String farerate_title; private String farerate_value;
    public String getFarerate_title() {
        return farerate_title;
    }

    public void setFarerate_title(String farerate_title) {
        this.farerate_title = farerate_title;
    }



    public String getFarerate_value() {
        return farerate_value;
    }

    public void setFarerate_value(String farerate_value) {
        this.farerate_value = farerate_value;
    }



    public String getFarerate_currencySymbol() {
        return farerate_currencySymbol;
    }

    public void setFarerate_currencySymbol(String farerate_currencySymbol) {
        this.farerate_currencySymbol = farerate_currencySymbol;
    }


    public String getRate_title() {
        return rate_title;
    }

    public void setRate_title(String rate_title) {
        this.rate_title = rate_title;
    }

    public String getRate_value() {
        return rate_value;
    }

    public void setRate_value(String rate_value) {
        this.rate_value = rate_value;
    }

    public String getRate_currencySymbol() {
        return rate_currencySymbol;
    }

    public void setRate_currencySymbol(String rate_currencySymbol) {
        this.rate_currencySymbol = rate_currencySymbol;
    }
}
