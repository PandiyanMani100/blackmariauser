package com.blackmaria.pojo;

/**
 * Created by Prem Kumar and Anitha on 10/14/2015.
 */
public class RateCard_CardDisplayPojo
{
    private String rate_title, rate_value, rate_currencySymbol;

    public String getRate_title() {
        return rate_title;
    }

    public void setRate_title(String rate_title) {
        this.rate_title = rate_title;
    }

    public String getRate_value() {
        return rate_value;
    }

    public void setRate_value(String rate_value) {
        this.rate_value = rate_value;
    }

    public String getRate_currencySymbol() {
        return rate_currencySymbol;
    }

    public void setRate_currencySymbol(String rate_currencySymbol) {
        this.rate_currencySymbol = rate_currencySymbol;
    }
}
