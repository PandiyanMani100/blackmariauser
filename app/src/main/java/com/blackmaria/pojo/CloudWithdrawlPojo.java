package com.blackmaria.pojo;

/**
 * Created by user144 on 5/18/2017.
 */

public class CloudWithdrawlPojo {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    String code;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    String icon;

    public String getInactive_icon() {
        return inactive_icon;
    }

    public void setInactive_icon(String inactive_icon) {
        this.inactive_icon = inactive_icon;
    }

    String inactive_icon;
}
