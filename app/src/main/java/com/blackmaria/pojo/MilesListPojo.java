package com.blackmaria.pojo;

/**
 * Created by user129 on 5/22/2017.
 */
public class MilesListPojo {


    private String ride_id;


    private String ride_time;


    private String ride_date;


    private String ride_satus;


    private String ride_details;


    private String ride_distance;


    private String ride_refname;


    private String ride_datetime;


    public String getRide_id() {
        return ride_id;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }


    public String getRide_time() {
        return ride_time;
    }

    public void setRide_time(String ride_time) {
        this.ride_time = ride_time;
    }


    public String getRide_date() {
        return ride_date;
    }

    public void setRide_date(String ride_date) {
        this.ride_date = ride_date;
    }


    public String getRide_satus() {
        return ride_satus;
    }

    public void setRide_satus(String ride_satus) {
        this.ride_satus = ride_satus;
    }

    public String getRide_details() {
        return ride_details;
    }

    public void setRide_details(String ride_details) {
        this.ride_details = ride_details;
    }

    public String getRide_distance() {
        return ride_distance;
    }

    public void setRide_distance(String ride_distance) {
        this.ride_distance = ride_distance;
    }

    public String getRide_refname() {
        return ride_refname;
    }

    public void setRide_refname(String ride_refname) {
        this.ride_refname = ride_refname;
    }

    public String getRide_datetime() {
        return ride_datetime;
    }

    public void setRide_datetime(String ride_datetime) {
        this.ride_datetime = ride_datetime;
    }


}
