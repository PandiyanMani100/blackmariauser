package com.blackmaria.pojo;

/**
 * Created by user14 on 1/11/2017.
 */

public class EarningsPojo {
    private String date,referrer_name,ref_type,type,earning_amount;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEarning_amount() {
        return earning_amount;
    }

    public void setEarning_amount(String earning_amount) {
        this.earning_amount = earning_amount;
    }

    public String getRef_type() {
        return ref_type;
    }

    public void setRef_type(String ref_type) {
        this.ref_type = ref_type;
    }

    public String getReferrer_name() {
        return referrer_name;
    }

    public void setReferrer_name(String referrer_name) {
        this.referrer_name = referrer_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
