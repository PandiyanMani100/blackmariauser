package com.blackmaria.pojo;

/**
 * Created by Prem Kumar and Anitha on 10/30/2015.
 */
public class RideDetailPojo {
    private String carType, carNo, rideStatus, pickupaddress, pickup, dropaddress,
            rideDistance, timeTaken, waitTime, totalBill, totalPaid, couponDiscount, walletUsuage, tip_amount;
    private String currrencySymbol, rideId, displayStatus, doCancelAction, pay_status, pickupLat, pickupLong,dropLat, dropLong,distanceUnit;
    private String doTrackAction, isFavLocation,drop;


    public String getPaymentMOde() {
        return paymentMOde;
    }

    public void setPaymentMOde(String paymentMOde) {
        this.paymentMOde = paymentMOde;
    }

    private  String paymentMOde;
    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }

    public String getRideStatus() {
        return rideStatus;
    }

    public void setRideStatus(String rideStatus) {
        this.rideStatus = rideStatus;
    }


    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }


    public String getRideDistance() {
        return rideDistance;
    }

    public void setRideDistance(String rideDistance) {
        this.rideDistance = rideDistance;
    }

    public String getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(String timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(String waitTime) {
        this.waitTime = waitTime;
    }

    public String getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(String totalBill) {
        this.totalBill = totalBill;
    }

    public String getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(String totalPaid) {
        this.totalPaid = totalPaid;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getWalletUsuage() {
        return walletUsuage;
    }

    public void setWalletUsuage(String walletUsuage) {
        this.walletUsuage = walletUsuage;
    }

    public String getCurrrencySymbol() {
        return currrencySymbol;
    }

    public void setCurrrencySymbol(String currrencySymbol) {
        this.currrencySymbol = currrencySymbol;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }

    public String getDoCancelAction() {
        return doCancelAction;
    }

    public void setDoCancelAction(String doCancelAction) {
        this.doCancelAction = doCancelAction;
    }

    public String getPay_status() {
        return pay_status;
    }

    public void setPay_status(String pay_status) {
        this.pay_status = pay_status;
    }


    public String getDoTrackAction() {
        return doTrackAction;
    }

    public void setDoTrackAction(String doTrackAction) {
        this.doTrackAction = doTrackAction;
    }

    public String getIsFavLocation() {
        return isFavLocation;
    }

    public void setIsFavLocation(String isFavLocation) {
        this.isFavLocation = isFavLocation;
    }

    public String getTip_amount() {
        return tip_amount;
    }

    public void setTip_amount(String tip_amount) {
        this.tip_amount = tip_amount;
    }

    public String getDropaddress() {
        return dropaddress;
    }

    public void setDropaddress(String dropaddress) {
        this.dropaddress = dropaddress;
    }

    public String getDropLat() {
        return dropLat;
    }

    public void setPickupaddress(String pickupaddress) {
        this.pickupaddress = pickupaddress;
    }

    public String getDropLong() {
        return dropLong;
    }

    public void setDropLat(String dropLat) {
        this.dropLat = dropLat;
    }

    public String getPickupaddress() {
        return pickupaddress;
    }

    public void setDropLong(String dropLong) {
        this.dropLong = dropLong;
    }

    public String getPickupLat() {
        return pickupLat;
    }

    public void setPickupLat(String pickupLat) {
        this.pickupLat = pickupLat;
    }

    public String getPickupLong() {
        return pickupLong;
    }

    public void setPickupLong(String pickupLong) {
        this.pickupLong = pickupLong;
    }

    public String getDistanceUnit() {
        return distanceUnit;
    }

    public void setDistanceUnit(String distanceUnit) {
        this.distanceUnit = distanceUnit;
    }
    public String getDrop() {
        return drop;
    }

    public void setDrop(String drop) {
        this.drop = drop;
    }
}
