package com.blackmaria.pojo;

/**
 * Created by Prem Kumar and Anitha on 12/23/2016.
 */

public class DriverProfilePojo {
    String title, value;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
