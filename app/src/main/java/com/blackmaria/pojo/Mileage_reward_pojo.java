package com.blackmaria.pojo;

import java.io.Serializable;

public class Mileage_reward_pojo implements Serializable {
    private String distance_range;
    private String reward_id;
    private String gift_name;
    private String coupon_value;
    private String reward_type;
    private String gift_image;
    private String valid_to;
    private String valid_to_second;


    // Getter Methods

    public String getDistance_range() {
        return distance_range;
    }

    public String getReward_id() {
        return reward_id;
    }

    public String getGift_name() {
        return gift_name;
    }

    public String getCoupon_value() {
        return coupon_value;
    }

    public String getReward_type() {
        return reward_type;
    }

    public String getGift_image() {
        return gift_image;
    }

    public String getValid_to() {
        return valid_to;
    }

    public String getValid_to_second() {
        return valid_to_second;
    }

    // Setter Methods

    public void setDistance_range(String distance_range) {
        this.distance_range = distance_range;
    }

    public void setReward_id(String reward_id) {
        this.reward_id = reward_id;
    }

    public void setGift_name(String gift_name) {
        this.gift_name = gift_name;
    }

    public void setCoupon_value(String coupon_value) {
        this.coupon_value = coupon_value;
    }

    public void setReward_type(String reward_type) {
        this.reward_type = reward_type;
    }

    public void setGift_image(String gift_image) {
        this.gift_image = gift_image;
    }

    public void setValid_to(String valid_to) {
        this.valid_to = valid_to;
    }

    public void setValid_to_second(String valid_to_second) {
        this.valid_to_second = valid_to_second;
    }
}
