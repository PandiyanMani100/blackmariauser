package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Getmapview implements Serializable{

    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Category {
        @SerializedName("id")
        private String id;
        @SerializedName("name")
        private String name;
        @SerializedName("eta")
        private String eta;
        @SerializedName("no_of_seats")
        private String no_of_seats;
        @SerializedName("eta_time")
        private String eta_time;
        @SerializedName("offer_type")
        private String offer_type;
        @SerializedName("eta_unit")
        private String eta_unit;
        @SerializedName("icon_normal")
        private String icon_normal;
        @SerializedName("icon_active")
        private String icon_active;
        @SerializedName("icon_car_image")
        private String icon_car_image;


        // Getter Methods

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getEta() {
            return eta;
        }

        public String getNo_of_seats() {
            return no_of_seats;
        }

        public String getEta_time() {
            return eta_time;
        }

        public String getOffer_type() {
            return offer_type;
        }

        public String getEta_unit() {
            return eta_unit;
        }

        public String getIcon_normal() {
            return icon_normal;
        }

        public String getIcon_active() {
            return icon_active;
        }

        public String getIcon_car_image() {
            return icon_car_image;
        }

        // Setter Methods

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setEta(String eta) {
            this.eta = eta;
        }

        public void setNo_of_seats(String no_of_seats) {
            this.no_of_seats = no_of_seats;
        }

        public void setEta_time(String eta_time) {
            this.eta_time = eta_time;
        }

        public void setOffer_type(String offer_type) {
            this.offer_type = offer_type;
        }

        public void setEta_unit(String eta_unit) {
            this.eta_unit = eta_unit;
        }

        public void setIcon_normal(String icon_normal) {
            this.icon_normal = icon_normal;
        }

        public void setIcon_active(String icon_active) {
            this.icon_active = icon_active;
        }

        public void setIcon_car_image(String icon_car_image) {
            this.icon_car_image = icon_car_image;
        }
    }

    public class Drivers {
        @SerializedName("lat")
        private String lat;
        @SerializedName("lon")
        private String lon;
        @SerializedName("driver_id")
        private String driver_id;
        @SerializedName("point")
        private String point;
        @SerializedName("type")
        private String type;


        // Getter Methods

        public String getLat() {
            return lat;
        }

        public String getLon() {
            return lon;
        }

        public String getDriver_id() {
            return driver_id;
        }

        public String getPoint() {
            return point;
        }

        public String getType() {
            return type;
        }

        // Setter Methods

        public void setLat(String lat) {
            this.lat = lat;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public void setDriver_id(String driver_id) {
            this.driver_id = driver_id;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public class Payment {
        @SerializedName("name")
        private String name;
        @SerializedName("code")
        private String code;
        @SerializedName("icon")
        private String icon;
        @SerializedName("inactive_icon")
        private String inactive_icon;
        @SerializedName("status")
        private String status;


        // Getter Methods

        public String getName() {
            return name;
        }

        public String getCode() {
            return code;
        }

        public String getIcon() {
            return icon;
        }

        public String getInactive_icon() {
            return inactive_icon;
        }

        public String getStatus() {
            return status;
        }

        // Setter Methods

        public void setName(String name) {
            this.name = name;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public void setInactive_icon(String inactive_icon) {
            this.inactive_icon = inactive_icon;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public class Response {
        @SerializedName("currency")
        private String currency;

        public ArrayList<Category> getCategory() {
            return category;
        }

        public void setCategory(ArrayList<Category> category) {
            this.category = category;
        }

        public ArrayList<Drivers> getDrivers() {
            return drivers;
        }

        public void setDrivers(ArrayList<Drivers> drivers) {
            this.drivers = drivers;
        }

        public ArrayList<Payment> getPayment() {
            return payment;
        }

        public void setPayment(ArrayList<Payment> payment) {
            this.payment = payment;
        }

        @SerializedName("category")
        ArrayList<Category> category = new ArrayList<Category>();
        @SerializedName("drivers")
        ArrayList<Drivers> drivers = new ArrayList<Drivers>();
        @SerializedName("payment")
        ArrayList<Payment> payment = new ArrayList<Payment>();
        @SerializedName("selected_category")
        private String selected_category;
        @SerializedName("tracking_percentage")
        private String tracking_percentage;
        @SerializedName("profile_complete")
        private String profile_complete;
        @SerializedName("no_of_seats")
        private String no_of_seats;
        @SerializedName("basic_complete")
        private String basic_complete;
        @SerializedName("wallet_amount")
        private String wallet_amount;


        // Getter Methods

        public String getCurrency() {
            return currency;
        }

        public String getSelected_category() {
            return selected_category;
        }

        public String getTracking_percentage() {
            return tracking_percentage;
        }

        public String getProfile_complete() {
            return profile_complete;
        }

        public String getNo_of_seats() {
            return no_of_seats;
        }

        public String getBasic_complete() {
            return basic_complete;
        }

        public String getWallet_amount() {
            return wallet_amount;
        }

        // Setter Methods

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setSelected_category(String selected_category) {
            this.selected_category = selected_category;
        }

        public void setTracking_percentage(String tracking_percentage) {
            this.tracking_percentage = tracking_percentage;
        }

        public void setProfile_complete(String profile_complete) {
            this.profile_complete = profile_complete;
        }

        public void setNo_of_seats(String no_of_seats) {
            this.no_of_seats = no_of_seats;
        }

        public void setBasic_complete(String basic_complete) {
            this.basic_complete = basic_complete;
        }

        public void setWallet_amount(String wallet_amount) {
            this.wallet_amount = wallet_amount;
        }
    }
}

