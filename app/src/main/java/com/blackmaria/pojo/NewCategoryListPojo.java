package com.blackmaria.pojo;

/**
 * Created by user129 on 7/21/2017.
 */
public class NewCategoryListPojo {

    private String category_Id;

    private String category_name;

    private String cat_eta;

    private String cat_type;

    private String cat_normal_img;

    private String cat_active_img;

    private String max_person;

    private String car_image;

    private String offer_type;


    private String delivery_des;


    public String getStr_free_waittime() {
        return Str_free_waittime;
    }

    public void setStr_free_waittime(String str_free_waittime) {
        Str_free_waittime = str_free_waittime;
    }

    String Str_free_waittime;

    public String getStr_wait_time_person() {
        return Str_wait_time_person;
    }

    public void setStr_wait_time_person(String str_wait_time_person) {
        Str_wait_time_person = str_wait_time_person;
    }

    public String getStrOfferType() {
        return StrOfferType;
    }

    public void setStrOfferType(String strOfferType) {
        StrOfferType = strOfferType;
    }

    String StrOfferType;
    String Str_wait_time_person;

    public String getDelivery_des() {
        return delivery_des;
    }

    public void setDelivery_des(String delivery_des) {
        this.delivery_des = delivery_des;
    }



    public String getOffer_type() {
        return offer_type;
    }

    public void setOffer_type(String offer_type) {
        this.offer_type = offer_type;
    }



    public String getCar_image() {
        return car_image;
    }

    public void setCar_image(String car_image) {
        this.car_image = car_image;
    }



    public String getMax_person() {
        return max_person;
    }

    public void setMax_person(String max_person) {
        this.max_person = max_person;
    }




    public String getCategory_Id() {
        return category_Id;
    }

    public void setCategory_Id(String category_Id) {
        this.category_Id = category_Id;
    }



    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }



    public String getCat_eta() {
        return cat_eta;
    }

    public void setCat_eta(String cat_eta) {
        this.cat_eta = cat_eta;
    }



    public String getCat_type() {
        return cat_type;
    }

    public void setCat_type(String cat_type) {
        this.cat_type = cat_type;
    }



    public String getCat_normal_img() {
        return cat_normal_img;
    }

    public void setCat_normal_img(String cat_normal_img) {
        this.cat_normal_img = cat_normal_img;
    }



    public String getCat_active_img() {
        return cat_active_img;
    }

    public void setCat_active_img(String cat_active_img) {
        this.cat_active_img = cat_active_img;
    }




}
