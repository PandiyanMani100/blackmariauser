package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class moneytransferasuccess implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    private String response;
    @SerializedName("user_name")
    private String user_name;
    @SerializedName("country_code")
    private String country_code;
    @SerializedName("phone_number")
    private String phone_number;
    @SerializedName("user_image")
    private String user_image;
    @SerializedName("city")
    private String city;
    @SerializedName("user_status")
    private String user_status;
    @SerializedName("cross_transfer")
    private String cross_transfer;
    @SerializedName("date")
    private String date;
    @SerializedName("time")
    private String time;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    @SerializedName("from")
    private String from;
    @SerializedName("transfer_amount")
    private String transfer_amount;
    @SerializedName("transaction_number")
    private String transaction_number;
    @SerializedName("currency")
    private String currency;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public String getResponse() {
        return response;
    }


    public String getUser_image() {
        return user_image;
    }

    public String getCity() {
        return city;
    }

    public String getUser_status() {
        return user_status;
    }

    public String getCross_transfer() {
        return cross_transfer;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getTransfer_amount() {
        return transfer_amount;
    }

    public String getTransaction_number() {
        return transaction_number;
    }

    public String getCurrency() {
        return currency;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(String response) {
        this.response = response;
    }


    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public void setCross_transfer(String cross_transfer) {
        this.cross_transfer = cross_transfer;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setTransfer_amount(String transfer_amount) {
        this.transfer_amount = transfer_amount;
    }

    public void setTransaction_number(String transaction_number) {
        this.transaction_number = transaction_number;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
