package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Driver_profile implements Serializable {
    @SerializedName("driver_id")
    private String driver_id;
    @SerializedName("driver_name")
    private String driver_name;
    @SerializedName("driver_email")
    private String driver_email;
    @SerializedName("driver_image")
    private String driver_image;
    @SerializedName("driver_review")
    private String driver_review;
    @SerializedName("driver_lat")
    private String driver_lat;
    @SerializedName("driver_lon")
    private String driver_lon;
    @SerializedName("est_arrival")
    private String est_arrival;
    @SerializedName("rider_lat")
    private String rider_lat;
    @SerializedName("rider_lon")
    private String rider_lon;
    @SerializedName("min_pickup_duration")
    private String min_pickup_duration;
    @SerializedName("approx_dist")
    private String approx_dist;
    @SerializedName("est_cost")
    private String est_cost;
    @SerializedName("currency")
    private String currency;
    @SerializedName("ride_id")
    private String ride_id;
    @SerializedName("phone_number")
    private String phone_number;
    @SerializedName("vehicle_number")
    private String vehicle_number;
    @SerializedName("vehicle_model")
    private String vehicle_model;
    @SerializedName("brand_image")
    private String brand_image;
    @SerializedName("ride_status")
    private String ride_status;
    @SerializedName("pickup")
    Trackrideafteraccept_pojo.Pickup PickupObject;
    @SerializedName("drop")
    Trackrideafteraccept_pojo.Drop DropObject;

    public ArrayList<Multipath_loc> getMultipath_loc() {
        return multipath_loc;
    }

    public void setMultipath_loc(ArrayList<Multipath_loc> multipath_loc) {
        this.multipath_loc = multipath_loc;
    }

    //            Multipath_loc Multipath_locObject;
    @SerializedName("multipath_loc")
    ArrayList < Multipath_loc > multipath_loc = new ArrayList < Multipath_loc > ();

    public class Multipath_loc {
        public String getRecord_id() {
            return record_id;
        }

        public void setRecord_id(String record_id) {
            this.record_id = record_id;
        }

        public String getWait_time() {
            return wait_time;
        }

        public void setWait_time(String wait_time) {
            this.wait_time = wait_time;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public Latlong getLatlongObject() {
            return LatlongObject;
        }

        public void setLatlongObject(Latlong latlongObject) {
            LatlongObject = latlongObject;
        }
        @SerializedName("record_id")
        private String record_id;
        @SerializedName("wait_time")
        private String wait_time;
        @SerializedName("location")
        private String location;
        @SerializedName("latlong")
        Latlong LatlongObject;

        public class Latlong {
            @SerializedName("lon")
            private String lon;
            @SerializedName("lat")
            private String lat;


            // Getter Methods

            public String getLon() {
                return lon;
            }

            public String getLat() {
                return lat;
            }

            // Setter Methods

            public void setLon(String lon) {
                this.lon = lon;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }
        }

    }

    @SerializedName("mode")
    private String mode;

    @SerializedName("driver_array_view")
    ArrayList < DriverProfilePojo > driver_array_view = new ArrayList < DriverProfilePojo > ();
    public ArrayList<DriverProfilePojo> getDriver_array_view() {
        return driver_array_view;
    }

    public void setDriver_array_view(ArrayList<DriverProfilePojo> driver_array_view) {
        this.driver_array_view = driver_array_view;
    }


    public ArrayList<Trackrideafteraccept_pojo.Multistop_array> getMultistop_array() {
        return multistop_array;
    }

    public void setMultistop_array(ArrayList<Trackrideafteraccept_pojo.Multistop_array> multistop_array) {
        this.multistop_array = multistop_array;
    }
    @SerializedName("multistop_array")
    ArrayList <Trackrideafteraccept_pojo.Multistop_array> multistop_array = new ArrayList <Trackrideafteraccept_pojo.Multistop_array> ();

    @SerializedName("is_return_over")
    private String is_return_over;
    @SerializedName("return_mode")
    private String return_mode;
    @SerializedName("comp_waiting_time")
    private String comp_waiting_time;
    @SerializedName("wait_time")
    private String wait_time;
    @SerializedName("closing_status")
    private String closing_status;

    public ArrayList<Trackrideafteraccept_pojo.Next_drop_location> getNext_drop_location() {
        return next_drop_location;
    }

    public void setNext_drop_location(ArrayList<Trackrideafteraccept_pojo.Next_drop_location> next_drop_location) {
        this.next_drop_location = next_drop_location;
    }
    @SerializedName("next_drop_location")
    ArrayList <Trackrideafteraccept_pojo.Next_drop_location> next_drop_location = new ArrayList <Trackrideafteraccept_pojo.Next_drop_location> ();

    public ArrayList<Trackrideafteraccept_pojo.Drop_location_array> getDrop_location_array() {
        return drop_location_array;
    }

    public void setDrop_location_array(ArrayList<Trackrideafteraccept_pojo.Drop_location_array> drop_location_array) {
        this.drop_location_array = drop_location_array;
    }
    @SerializedName("drop_location_array")
    ArrayList <Trackrideafteraccept_pojo.Drop_location_array> drop_location_array = new ArrayList <Trackrideafteraccept_pojo.Drop_location_array> ();
    @SerializedName("category_icon")
    private String category_icon;
    @SerializedName("category_icon_map")
    private String category_icon_map;


    // Getter Methods

    public String getDriver_id() {
        return driver_id;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public String getDriver_email() {
        return driver_email;
    }

    public String getDriver_image() {
        return driver_image;
    }

    public String getDriver_review() {
        return driver_review;
    }

    public String getDriver_lat() {
        return driver_lat;
    }

    public String getDriver_lon() {
        return driver_lon;
    }

    public String getEst_arrival() {
        return est_arrival;
    }

    public String getRider_lat() {
        return rider_lat;
    }

    public String getRider_lon() {
        return rider_lon;
    }

    public String getMin_pickup_duration() {
        return min_pickup_duration;
    }

    public String getApprox_dist() {
        return approx_dist;
    }

    public String getEst_cost() {
        return est_cost;
    }

    public String getCurrency() {
        return currency;
    }

    public String getRide_id() {
        return ride_id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getVehicle_number() {
        return vehicle_number;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public String getBrand_image() {
        return brand_image;
    }

    public String getRide_status() {
        return ride_status;
    }

    public Trackrideafteraccept_pojo.Pickup getPickup() {
        return PickupObject;
    }

    public Trackrideafteraccept_pojo.Drop getDrop() {
        return DropObject;
    }


    public String getMode() {
        return mode;
    }

    public String getIs_return_over() {
        return is_return_over;
    }

    public String getReturn_mode() {
        return return_mode;
    }

    public String getComp_waiting_time() {
        return comp_waiting_time;
    }

    public String getWait_time() {
        return wait_time;
    }

    public String getClosing_status() {
        return closing_status;
    }

    public String getCategory_icon() {
        return category_icon;
    }

    public String getCategory_icon_map() {
        return category_icon_map;
    }

    // Setter Methods

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public void setDriver_email(String driver_email) {
        this.driver_email = driver_email;
    }

    public void setDriver_image(String driver_image) {
        this.driver_image = driver_image;
    }

    public void setDriver_review(String driver_review) {
        this.driver_review = driver_review;
    }

    public void setDriver_lat(String driver_lat) {
        this.driver_lat = driver_lat;
    }

    public void setDriver_lon(String driver_lon) {
        this.driver_lon = driver_lon;
    }

    public void setEst_arrival(String est_arrival) {
        this.est_arrival = est_arrival;
    }

    public void setRider_lat(String rider_lat) {
        this.rider_lat = rider_lat;
    }

    public void setRider_lon(String rider_lon) {
        this.rider_lon = rider_lon;
    }

    public void setMin_pickup_duration(String min_pickup_duration) {
        this.min_pickup_duration = min_pickup_duration;
    }

    public void setApprox_dist(String approx_dist) {
        this.approx_dist = approx_dist;
    }

    public void setEst_cost(String est_cost) {
        this.est_cost = est_cost;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public void setVehicle_number(String vehicle_number) {
        this.vehicle_number = vehicle_number;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public void setBrand_image(String brand_image) {
        this.brand_image = brand_image;
    }

    public void setRide_status(String ride_status) {
        this.ride_status = ride_status;
    }

    public void setPickup(Trackrideafteraccept_pojo.Pickup pickupObject) {
        this.PickupObject = pickupObject;
    }

    public void setDrop(Trackrideafteraccept_pojo.Drop dropObject) {
        this.DropObject = dropObject;
    }



    public void setMode(String mode) {
        this.mode = mode;
    }

    public void setIs_return_over(String is_return_over) {
        this.is_return_over = is_return_over;
    }

    public void setReturn_mode(String return_mode) {
        this.return_mode = return_mode;
    }

    public void setComp_waiting_time(String comp_waiting_time) {
        this.comp_waiting_time = comp_waiting_time;
    }

    public void setWait_time(String wait_time) {
        this.wait_time = wait_time;
    }

    public void setClosing_status(String closing_status) {
        this.closing_status = closing_status;
    }

    public void setCategory_icon(String category_icon) {
        this.category_icon = category_icon;
    }

    public void setCategory_icon_map(String category_icon_map) {
        this.category_icon_map = category_icon_map;
    }

}