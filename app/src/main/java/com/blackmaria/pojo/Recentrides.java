package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Recentrides implements Serializable{


    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }


    public class Response {

        public ArrayList<Recent_rides> getRecent_rides() {
            return recent_rides;
        }

        public void setRecent_rides(ArrayList<Recent_rides> recent_rides) {
            this.recent_rides = recent_rides;
        }

        @SerializedName("recent_rides")
        ArrayList < Recent_rides > recent_rides = new ArrayList< Recent_rides >();
        
        public class Recent_rides{
            @SerializedName("location")
            private String location;
            @SerializedName("latlong")
            Latlong LatlongObject;


            // Getter Methods 

            public String getLocation() {
                return location;
            }

            public Latlong getLatlong() {
                return LatlongObject;
            }

            // Setter Methods 

            public void setLocation(String location) {
                this.location = location;
            }

            public void setLatlong(Latlong latlongObject) {
                this.LatlongObject = latlongObject;
            }

            public class Latlong {
                @SerializedName("lon")
                private String lon;
                @SerializedName("lat")
                private String lat;


                // Getter Methods 

                public String getLon() {
                    return lon;
                }

                public String getLat() {
                    return lat;
                }

                // Setter Methods 

                public void setLon(String lon) {
                    this.lon = lon;
                }

                public void setLat(String lat) {
                    this.lat = lat;
                }
            }
        }
        
    }

}

