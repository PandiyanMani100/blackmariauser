package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Trackrideaccept_pojo implements Serializable {

    @SerializedName("status")
    private String status;

    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Review_arr{
        @SerializedName("ride_id")
        private String ride_id;
        @SerializedName("user_name")
        private String user_name;
        @SerializedName("user_image")
        private String user_image;
        @SerializedName("user_id")
        private String user_id;
        @SerializedName("date")
        private String date;
        @SerializedName("rating")
        private String rating;
        @SerializedName("comment")
        private String comment;


        // Getter Methods

        public String getRide_id() {
            return ride_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public String getUser_image() {
            return user_image;
        }

        public String getUser_id() {
            return user_id;
        }

        public String getDate() {
            return date;
        }

        public String getRating() {
            return rating;
        }

        public String getComment() {
            return comment;
        }

        // Setter Methods

        public void setRide_id(String ride_id) {
            this.ride_id = ride_id;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public void setUser_image(String user_image) {
            this.user_image = user_image;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }
    }

    public class Driver_array_view{
        @SerializedName("title")
        private String title;
        @SerializedName("value")
        private String value;


        // Getter Methods

        public String getTitle() {
            return title;
        }

        public String getValue() {
            return value;
        }

        // Setter Methods

        public void setTitle(String title) {
            this.title = title;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public class Response {
        @SerializedName("total_review_count")
        private String total_review_count;
        @SerializedName("driver_profile")
        Driver_profile Driver_profileObject;

        public ArrayList<Review_arr> getReview_arr() {
            return review_arr;
        }

        public void setReview_arr(ArrayList<Review_arr> review_arr) {
            this.review_arr = review_arr;
        }

        public ArrayList<DriverProfilePojo> getDriver_array_view() {
            return driver_array_view;
        }

        public void setDriver_array_view(ArrayList<DriverProfilePojo> driver_array_view) {
            this.driver_array_view = driver_array_view;
        }

        @SerializedName("review_arr")
        ArrayList< Review_arr > review_arr = new ArrayList < Review_arr > ();
        @SerializedName("driver_array_view")
        ArrayList < DriverProfilePojo > driver_array_view = new ArrayList < DriverProfilePojo > ();
        @SerializedName("eta_arrive")
        private String eta_arrive;


        @SerializedName("photo_video")
        private String photo_video;

        public String getPhoto_video() {
            return photo_video;
        }

        public void setPhoto_video(String photo_video) {
            this.photo_video = photo_video;
        }

        public String getPhoto_video_type() {
            return photo_video_type;
        }

        public void setPhoto_video_type(String photo_video_type) {
            this.photo_video_type = photo_video_type;
        }

        @SerializedName("photo_video_type")
        private String photo_video_type;


        // Getter Methods

        public String getTotal_review_count() {
            return total_review_count;
        }

        public Driver_profile getDriver_profile() {
            return Driver_profileObject;
        }

        public String getEta_arrive() {
            return eta_arrive;
        }

        // Setter Methods

        public void setTotal_review_count(String total_review_count) {
            this.total_review_count = total_review_count;
        }

        public void setDriver_profile(Driver_profile driver_profileObject) {
            this.Driver_profileObject = driver_profileObject;
        }

        public void setEta_arrive(String eta_arrive) {
            this.eta_arrive = eta_arrive;
        }
        public class Driver_profile {
            @SerializedName("driver_id")
            private String driver_id;
            @SerializedName("driver_name")
            private String driver_name;
            @SerializedName("driver_image")
            private String driver_image;
            @SerializedName("driver_review")
            private String driver_review;
            @SerializedName("driver_phone")
            private String driver_phone;


            // Getter Methods

            public String getDriver_id() {
                return driver_id;
            }

            public String getDriver_name() {
                return driver_name;
            }

            public String getDriver_image() {
                return driver_image;
            }

            public String getDriver_review() {
                return driver_review;
            }

            public String getDriver_phone() {
                return driver_phone;
            }

            // Setter Methods

            public void setDriver_id(String driver_id) {
                this.driver_id = driver_id;
            }

            public void setDriver_name(String driver_name) {
                this.driver_name = driver_name;
            }

            public void setDriver_image(String driver_image) {
                this.driver_image = driver_image;
            }

            public void setDriver_review(String driver_review) {
                this.driver_review = driver_review;
            }

            public void setDriver_phone(String driver_phone) {
                this.driver_phone = driver_phone;
            }
        }
    }
}


