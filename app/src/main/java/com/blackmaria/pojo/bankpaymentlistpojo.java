package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class bankpaymentlistpojo implements Serializable {
    @SerializedName("status")
    private String status;

    public ArrayList<Bank_list> getBank_list() {
        return bank_list;
    }

    public void setBank_list(ArrayList<Bank_list> bank_list) {
        this.bank_list = bank_list;
    }
    @SerializedName("bank_list")
    ArrayList<Bank_list> bank_list = new ArrayList<Bank_list>();


    public class Bank_list {
        @SerializedName("name")
        private String name;
        @SerializedName("code")
        private String code;
        @SerializedName("image")
        private String image;


        // Getter Methods

        public String getName() {
            return name;
        }

        public String getCode() {
            return code;
        }

        public String getImage() {
            return image;
        }

        // Setter Methods

        public void setName(String name) {
            this.name = name;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
    // Getter Methods

    public String getStatus() {
        return status;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

}
