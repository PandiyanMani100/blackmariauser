package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Saveplustransfer implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods 

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods 

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }


    public class Response {
        @SerializedName("trans_date")
        private String trans_date;
        @SerializedName("trans_time")
        private String trans_time;
        @SerializedName("trans_id")
        private String trans_id;

        public String getTrans_date() {
            return trans_date;
        }

        public void setTrans_date(String trans_date) {
            this.trans_date = trans_date;
        }

        public String getTrans_time() {
            return trans_time;
        }

        public void setTrans_time(String trans_time) {
            this.trans_time = trans_time;
        }

        public String getTrans_id() {
            return trans_id;
        }

        public void setTrans_id(String trans_id) {
            this.trans_id = trans_id;
        }

        public String getSaveplus_id() {
            return saveplus_id;
        }

        public void setSaveplus_id(String saveplus_id) {
            this.saveplus_id = saveplus_id;
        }

        @SerializedName("saveplus_id")
        private String saveplus_id;



        @SerializedName("currency")
        private String currency;
        @SerializedName("total_amount")
        private String total_amount;
        @SerializedName("received_amount")
        private String received_amount;
        @SerializedName("crdit_amount")
        private String crdit_amount;
        @SerializedName("receiver_details")
        Receiver_details Receiver_detailsObject;
        @SerializedName("message")
        private String message;


        // Getter Methods 

        public String getCurrency() {
            return currency;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public String getReceived_amount() {
            return received_amount;
        }

        public String getCrdit_amount() {
            return crdit_amount;
        }

        public Receiver_details getReceiver_details() {
            return Receiver_detailsObject;
        }

        public String getMessage() {
            return message;
        }

        // Setter Methods 

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public void setReceived_amount(String received_amount) {
            this.received_amount = received_amount;
        }

        public void setCrdit_amount(String crdit_amount) {
            this.crdit_amount = crdit_amount;
        }

        public void setReceiver_details(Receiver_details receiver_detailsObject) {
            this.Receiver_detailsObject = receiver_detailsObject;
        }

        public void setMessage(String message) {
            this.message = message;
        }


        public class Receiver_details {
            @SerializedName("title")
            private String title;
            @SerializedName("name")
            private String name;
            @SerializedName("image")
            private String image;
            @SerializedName("id")
            private String id;


            // Getter Methods 

            public String getTitle() {
                return title;
            }

            public String getName() {
                return name;
            }

            public String getImage() {
                return image;
            }

            public String getId() {
                return id;
            }

            // Setter Methods 

            public void setTitle(String title) {
                this.title = title;
            }

            public void setName(String name) {
                this.name = name;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }

}