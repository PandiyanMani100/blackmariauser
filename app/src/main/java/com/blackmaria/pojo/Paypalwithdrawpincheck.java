package com.blackmaria.pojo;

import java.io.Serializable;

public class Paypalwithdrawpincheck implements Serializable {
    public boolean isIspincheck() {
        return ispincheck;
    }

    public void setIspincheck(boolean ispincheck) {
        this.ispincheck = ispincheck;
    }

    private boolean ispincheck;
}
