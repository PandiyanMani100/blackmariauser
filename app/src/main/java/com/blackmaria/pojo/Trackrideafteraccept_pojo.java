package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Trackrideafteraccept_pojo implements Serializable{


    @SerializedName("status")
    private String status;

    @SerializedName("response")
    Response ResponseObject;

    public Response getResponseObject() {
        return ResponseObject;
    }

    public void setResponseObject(Response responseObject) {
        ResponseObject = responseObject;
    }

    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }


    public class Response {
        @SerializedName("ride_id")
        private String ride_id;

        @SerializedName("driver_profile")
        Driver_profile Driver_profileObject;





        // Getter Methods

        public String getRide_id() {
            return ride_id;
        }

        public Driver_profile getDriver_profile() {
            return Driver_profileObject;
        }

        // Setter Methods

        public void setRide_id(String ride_id) {
            this.ride_id = ride_id;
        }

        public void setDriver_profile(Driver_profile driver_profileObject) {
            this.Driver_profileObject = driver_profileObject;
        }


    }

    public class Pickup {
        @SerializedName("location")
        private String location;
        @SerializedName("latlong")
        Latlong LatlongObject;


        // Getter Methods

        public String getLocation() {
            return location;
        }

        public Latlong getLatlong() {
            return LatlongObject;
        }

        // Setter Methods

        public void setLocation(String location) {
            this.location = location;
        }

        public void setLatlong(Latlong latlongObject) {
            this.LatlongObject = latlongObject;
        }

        public class Latlong {
            @SerializedName("lon")
            private String lon;
            @SerializedName("lat")
            private String lat;


            // Getter Methods

            public String getLon() {
                return lon;
            }

            public String getLat() {
                return lat;
            }

            // Setter Methods

            public void setLon(String lon) {
                this.lon = lon;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }
        }
    }

    public class Drop {
        @SerializedName("location")
        private String location;
        @SerializedName("latlong")
        Latlong LatlongObject;


        // Getter Methods

        public String getLocation() {
            return location;
        }

        public Latlong getLatlong() {
            return LatlongObject;
        }

        // Setter Methods

        public void setLocation(String location) {
            this.location = location;
        }

        public void setLatlong(Latlong latlongObject) {
            this.LatlongObject = latlongObject;
        }

        public class Latlong {
            @SerializedName("lon")
            private String lon;
            @SerializedName("lat")
            private String lat;


            // Getter Methods

            public String getLon() {
                return lon;
            }

            public String getLat() {
                return lat;
            }

            // Setter Methods

            public void setLon(String lon) {
                this.lon = lon;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }


        }
    }

    public class Tracking_details{
        public String getOn_time() {
            return on_time;
        }

        public void setOn_time(String on_time) {
            this.on_time = on_time;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        @SerializedName("on_time")
        private String on_time;
        @SerializedName("location")
        private Location location;
    }

    public class Location{
        @SerializedName("lat")
        private String lat;

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }
        @SerializedName("lon")
        private String lon;
    }

    public class Multistop_array{
        @SerializedName("location")
        private String location;
        @SerializedName("is_end")
        private String is_end;

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getIs_end() {
            return is_end;
        }

        public void setIs_end(String is_end) {
            this.is_end = is_end;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        @SerializedName("mode")
        private String mode;

        public Latlong getLatlongObject() {
            return LatlongObject;
        }

        public void setLatlongObject(Latlong latlongObject) {
            LatlongObject = latlongObject;
        }
        @SerializedName("latlong")
        Latlong LatlongObject;

        public class Latlong {
            @SerializedName("lon")
            private String lon;
            @SerializedName("lat")
            private String lat;


            // Getter Methods

            public String getLon() {
                return lon;
            }

            public String getLat() {
                return lat;
            }

            // Setter Methods

            public void setLon(String lon) {
                this.lon = lon;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }
        }

    }

    public class Next_drop_location{
        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public Latlong getLatlongObject() {
            return LatlongObject;
        }

        public void setLatlongObject(Latlong latlongObject) {
            LatlongObject = latlongObject;
        }
        @SerializedName("location")
        private String location;
        @SerializedName("latlong")
        Latlong LatlongObject;

        public class Latlong {
            @SerializedName("lon")
            private String lon;
            @SerializedName("lat")
            private String lat;


            // Getter Methods

            public String getLon() {
                return lon;
            }

            public String getLat() {
                return lat;
            }

            // Setter Methods

            public void setLon(String lon) {
                this.lon = lon;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }
        }
    }
    public class Drop_location_array{
        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public Latlong getLatlongObject() {
            return LatlongObject;
        }

        public void setLatlongObject(Latlong latlongObject) {
            LatlongObject = latlongObject;
        }
        @SerializedName("location")
        private String location;
        @SerializedName("latlong")
        Latlong LatlongObject;

        public class Latlong {
            @SerializedName("lon")
            private String lon;
            @SerializedName("lat")
            private String lat;


            // Getter Methods

            public String getLon() {
                return lon;
            }

            public String getLat() {
                return lat;
            }

            // Setter Methods

            public void setLon(String lon) {
                this.lon = lon;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }
        }
    }
}







