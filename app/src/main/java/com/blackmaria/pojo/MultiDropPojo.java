package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Prem Kumar and Anitha on 8/16/2016.
 */
public class MultiDropPojo implements Serializable
{
    @SerializedName("placeName")
    private String placeName;
    @SerializedName("placeLat")
    private String placeLat;
    @SerializedName("placeLong")
    private String placeLong;
    @SerializedName("setMode")
    private String setMode;
    @SerializedName("isEnd")
    private String isEnd;
    @SerializedName("waitingTime")
    private String waitingTime;

    public String getwaitingTime() {
        return waitingTime;
    }

    public void setwaitingTimet(String waitingTime) {
        this.waitingTime = waitingTime;
    }

    public String getPlaceLat() {
        return placeLat;
    }

    public void setPlaceLat(String placeLat) {
        this.placeLat = placeLat;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceLong() {
        return placeLong;
    }

    public void setPlaceLong(String placeLong) {
        this.placeLong = placeLong;
    }


    public void setMode(String setMode) {
        this.setMode = setMode;
    }

    public String getMode() {
        return setMode;
    }

    public void setIsEnd(String isEnd) {
        this.isEnd = isEnd;
    }

    public String getIsEnd() {
        return isEnd;
    }
}
