package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class loginresponse implements Serializable{

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("terms_condition")
    private String terms_condition;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("country_code")
    private String country_code;
    @SerializedName("phone_number")
    private String phone_number;
    @SerializedName("sec_key")
    private String sec_key;
    @SerializedName("profile_complete")
    private String profile_complete;
    @SerializedName("user_name")
    private String user_name;

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    @SerializedName("user_image")
    private String user_image;
    @SerializedName("email")
    private String email;
    @SerializedName("key")
    private String key;
    @SerializedName("account_exist")
    private String account_exist;
    @SerializedName("referral_staus")
    private String referral_staus;
    @SerializedName("currency")
    private String currency;
    @SerializedName("category")
    private String category;
    @SerializedName("otp_status")
    private String otp_status;
    @SerializedName("otp")
    private String otp;
    @SerializedName("error_status")
    private String error_status;
    @SerializedName("error_message")
    private String error_message;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getTerms_condition() {
        return terms_condition;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getCountry_code() {
        return country_code;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getSec_key() {
        return sec_key;
    }

    public String getProfile_complete() {
        return profile_complete;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getEmail() {
        return email;
    }

    public String getKey() {
        return key;
    }

    public String getAccount_exist() {
        return account_exist;
    }

    public String getReferral_staus() {
        return referral_staus;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCategory() {
        return category;
    }

    public String getOtp_status() {
        return otp_status;
    }

    public String getOtp() {
        return otp;
    }

    public String getError_status() {
        return error_status;
    }

    public String getError_message() {
        return error_message;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTerms_condition(String terms_condition) {
        this.terms_condition = terms_condition;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public void setSec_key(String sec_key) {
        this.sec_key = sec_key;
    }

    public void setProfile_complete(String profile_complete) {
        this.profile_complete = profile_complete;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setAccount_exist(String account_exist) {
        this.account_exist = account_exist;
    }

    public void setReferral_staus(String referral_staus) {
        this.referral_staus = referral_staus;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setOtp_status(String otp_status) {
        this.otp_status = otp_status;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setError_status(String error_status) {
        this.error_status = error_status;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }
}
