package com.blackmaria.pojo;

/**
 * Created by CAS58 on 12/9/2016.
 */
public class RideListPojo {

    private String ride_id;

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    private String driver_name;
    private String ride_time;
    private String ride_date;
    private String pickup;
    private String ride_status;
    private String distance;
    private String ride_type;
    private String ride_category;
    private String display_status;
    private String group;
    private String datetime;

    public String getRideAmount() {
        return rideAmount;
    }

    public void setRideAmount(String rideAmount) {
        this.rideAmount = rideAmount;
    }

    String rideAmount;

    public String getRide_id() {
        return ride_id;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }

    public String getRide_time() {
        return ride_time;
    }

    public void setRide_time(String ride_time) {
        this.ride_time = ride_time;
    }

    public String getRide_date() {
        return ride_date;
    }

    public void setRide_date(String ride_date) {
        this.ride_date = ride_date;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRide_type() {
        return ride_type;
    }

    public void setRide_type(String ride_type) {
        this.ride_type = ride_type;
    }

    public String getDriverImage() {
        return setDriverImage;
    }

    public void setDriverImage(String setDriverImage) {
        this.setDriverImage = setDriverImage;
    }

    String setDriverImage;
    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public String getRide_status() {
        return ride_status;
    }

    public void setRide_status(String ride_status) {
        this.ride_status = ride_status;
    }

    public String getDisplay_status() {
        return display_status;
    }

    public void setDisplay_status(String display_status) {
        this.display_status = display_status;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getRide_category() {
        return ride_category;
    }

    public void setRide_category(String ride_category) {
        this.ride_category = ride_category;
    }
}
