package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Transactionlistsaveplus implements Serializable {

    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;
    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods 

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("currency")
        private String currency;
        @SerializedName("total_amount")
        private String total_amount;
        @SerializedName("total_transaction")
        private String total_transaction;

        public ArrayList<Trans> getTrans() {
            return trans;
        }

        public void setTrans(ArrayList<Trans> trans) {
            this.trans = trans;
        }

        @SerializedName("trans")
        ArrayList<Trans> trans = new ArrayList<Trans>();
        @SerializedName("current_page")
        private String current_page;
        @SerializedName("next_page")
        private String next_page;
        @SerializedName("perPage")
        private String perPage;

        public String getDownload_url() {
            return download_url;
        }

        public void setDownload_url(String download_url) {
            this.download_url = download_url;
        }

        @SerializedName("download_url")
        private String download_url;


        // Getter Methods 

        public String getCurrency() {
            return currency;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public String getTotal_transaction() {
            return total_transaction;
        }

        public String getCurrent_page() {
            return current_page;
        }

        public String getNext_page() {
            return next_page;
        }

        public String getPerPage() {
            return perPage;
        }

        // Setter Methods 

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public void setTotal_transaction(String total_transaction) {
            this.total_transaction = total_transaction;
        }

        public void setCurrent_page(String current_page) {
            this.current_page = current_page;
        }

        public void setNext_page(String next_page) {
            this.next_page = next_page;
        }

        public void setPerPage(String perPage) {
            this.perPage = perPage;
        }


    }

}
