package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class faqhelp_pojo implements Serializable {

    @SerializedName("status")
    private String status;

    public ArrayList<Response> getResponse() {
        return response;
    }

    public void setResponse(ArrayList<Response> response) {
        this.response = response;
    }

    @SerializedName("response")
    ArrayList < Response > response = new ArrayList< Response >();


    // Getter Methods

    public String getStatus() {
        return status;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public class Response{
        @SerializedName("name")
        private String name;
        @SerializedName("content")
        private String content;
        @SerializedName("id")
        private String id;

        public ArrayList<Question> getQuestion() {
            return question;
        }

        public void setQuestion(ArrayList<Question> question) {
            this.question = question;
        }
        @SerializedName("question")
        ArrayList < Question > question = new ArrayList < Question > ();


        // Getter Methods

        public String getName() {
            return name;
        }

        public String getContent() {
            return content;
        }

        public String getId() {
            return id;
        }

        // Setter Methods

        public void setName(String name) {
            this.name = name;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public void setId(String id) {
            this.id = id;
        }

        public class Question{
            @SerializedName("category_id")
            private String category_id;
            @SerializedName("question")
            private String question;
            @SerializedName("answer")
            private String answer;
            @SerializedName("id")
            private String id;


            // Getter Methods

            public String getCategory_id() {
                return category_id;
            }

            public String getQuestion() {
                return question;
            }

            public String getAnswer() {
                return answer;
            }

            public String getId() {
                return id;
            }

            // Setter Methods

            public void setCategory_id(String category_id) {
                this.category_id = category_id;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public void setAnswer(String answer) {
                this.answer = answer;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }
}
