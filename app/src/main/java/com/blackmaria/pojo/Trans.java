package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

public class Trans {
    @SerializedName("type")
    private String type;
    @SerializedName("amount")
    private String amount;
    @SerializedName("from")
    private String from="";
    @SerializedName("trans_date")
    private String trans_date;
    @SerializedName("trans_time")
    private String trans_time;
    @SerializedName("balance_amount")
    private String balance_amount;
    @SerializedName("date")
    private String date;


    // Getter Methods

    public String getType() {
        return type;
    }

    public String getAmount() {
        return amount;
    }

    public String getFrom() {
        return from;
    }

    public String getTrans_date() {
        return trans_date;
    }

    public String getTrans_time() {
        return trans_time;
    }

    public String getBalance_amount() {
        return balance_amount;
    }

    public String getDate() {
        return date;
    }

    // Setter Methods

    public void setType(String type) {
        this.type = type;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTrans_date(String trans_date) {
        this.trans_date = trans_date;
    }

    public void setTrans_time(String trans_time) {
        this.trans_time = trans_time;
    }

    public void setBalance_amount(String balance_amount) {
        this.balance_amount = balance_amount;
    }

    public void setDate(String date) {
        this.date = date;
    }
}