package com.blackmaria.pojo;

import java.io.Serializable;

public class Refer_friendslist implements Serializable {

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getNo_of_rides() {
        return no_of_rides;
    }

    public void setNo_of_rides(int no_of_rides) {
        this.no_of_rides = no_of_rides;
    }

    public String getReference_date() {
        return reference_date;
    }

    public void setReference_date(String reference_date) {
        this.reference_date = reference_date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String user_name;
    private int no_of_rides;
    private String reference_date;
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;
}
