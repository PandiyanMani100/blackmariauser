package com.blackmaria.pojo;

/**
 * Created by user129 on 5/19/2017.
 */
public class MilesRewardsPojo {



    private String miles_distance;

    private String reward_id;

    private String gift_name;

    private String coupon_value;

    private String gift_image;

    private String valid_to;

    private String valid_to_second;


    private String reward_type;




    public String getMiles_distance() {
        return miles_distance;
    }

    public void setMiles_distance(String miles_distance) {
        this.miles_distance = miles_distance;
    }


    public String getReward_id() {
        return reward_id;
    }

    public void setReward_id(String reward_id) {
        this.reward_id = reward_id;
    }


    public String getGift_name() {
        return gift_name;
    }

    public void setGift_name(String gift_name) {
        this.gift_name = gift_name;
    }

    public String getCoupon_value() {
        return coupon_value;
    }

    public void setCoupon_value(String coupon_value) {
        this.coupon_value = coupon_value;
    }

    public String getGift_image() {
        return gift_image;
    }

    public void setGift_image(String gift_image) {
        this.gift_image = gift_image;
    }


    public String getValid_to() {
        return valid_to;
    }

    public void setValid_to(String valid_to) {
        this.valid_to = valid_to;
    }

    public String getValid_to_second() {
        return valid_to_second;
    }

    public void setValid_to_second(String valid_to_second) {
        this.valid_to_second = valid_to_second;
    }


    public String getReward_type() {
        return reward_type;
    }

    public void setReward_type(String reward_type) {
        this.reward_type = reward_type;
    }




}
