package com.blackmaria.pojo;

import java.io.Serializable;

public class Complaintlist_pojo implements Serializable {

    private String subject;
    private String ticket_number;
    private String vehicle_number;
    private String ride_id;
    private String ticket_status;
    private String ticket_date;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image;


    // Getter Methods

    public String getSubject() {
        return subject;
    }

    public String getTicket_number() {
        return ticket_number;
    }

    public String getVehicle_number() {
        return vehicle_number;
    }

    public String getRide_id() {
        return ride_id;
    }

    public String getTicket_status() {
        return ticket_status;
    }

    public String getTicket_date() {
        return ticket_date;
    }

    // Setter Methods

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setTicket_number(String ticket_number) {
        this.ticket_number = ticket_number;
    }

    public void setVehicle_number(String vehicle_number) {
        this.vehicle_number = vehicle_number;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }

    public void setTicket_status(String ticket_status) {
        this.ticket_status = ticket_status;
    }

    public void setTicket_date(String ticket_date) {
        this.ticket_date = ticket_date;
    }
}
