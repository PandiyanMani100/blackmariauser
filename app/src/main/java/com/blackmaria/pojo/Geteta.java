package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Geteta implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;

    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }


    public class Response {
        @SerializedName("currency")
        private String currency;
        @SerializedName("eta")
        Eta EtaObject;


        // Getter Methods

        public String getCurrency() {
            return currency;
        }

        public Eta getEta() {
            return EtaObject;
        }

        // Setter Methods

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setEta(Eta etaObject) {
            this.EtaObject = etaObject;
        }


        public class Eta {
            @SerializedName("catrgory_id")
            private String catrgory_id;
            @SerializedName("catrgory_name")
            private String catrgory_name;
            @SerializedName("pickup")
            private String pickup;
            @SerializedName("drop")
            private String drop;
            @SerializedName("eta_mode")
            private String eta_mode;
            @SerializedName("eta_time")
            private String eta_time;
            @SerializedName("eta_unit")
            private String eta_unit;
            @SerializedName("amount")
            private String amount;
            @SerializedName("ride_amount")
            private String ride_amount;
            @SerializedName("phantom_car")
            private String phantom_car;
            @SerializedName("user_name")
            private String user_name;
            @SerializedName("distance")
            private String distance;
            @SerializedName("att")
            private String att;
            @SerializedName("peak_time")
            private String peak_time;
            @SerializedName("stop_one_eta")
            private String stop_one_eta;
            @SerializedName("stop_two_eta")
            private String stop_two_eta;
            @SerializedName("stop_three_eta")
            private String stop_three_eta;
            @SerializedName("stop_four_eta")
            private String stop_four_eta;
            @SerializedName("night_charge")
            private String night_charge;
            @SerializedName("total_waiting_charge")
            private String total_waiting_charge;
            @SerializedName("note")
            private String note;
            @SerializedName("icon_normal")
            private String icon_normal;
            @SerializedName("icon_active")
            private String icon_active;
            @SerializedName("icon_car_image")
            private String icon_car_image;

            public ArrayList<Summary_arr> getSummary_arr() {
                return summary_arr;
            }

            public void setSummary_arr(ArrayList<Summary_arr> summary_arr) {
                this.summary_arr = summary_arr;
            }

            @SerializedName("summary_arr")
            ArrayList<Summary_arr> summary_arr = new ArrayList<Summary_arr>();


            public class Summary_arr {
                @SerializedName("locaton")
                private String locaton;
                @SerializedName("lat")
                private String lat;
                @SerializedName("lng")
                private String lng;
                @SerializedName("distance")
                private String distance;
                @SerializedName("duration")
                private String duration;
                @SerializedName("fare")
                private String fare;
                @SerializedName("wait_time")
                private String wait_time;

                public String getWait_time_charges() {
                    return wait_time_charges;
                }

                public void setWait_time_charges(String wait_time_charges) {
                    this.wait_time_charges = wait_time_charges;
                }

                @SerializedName("wait_time_charges")
                private String wait_time_charges;


                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                @SerializedName("text")
                private String text;


                // Getter Methods 

                public String getLocaton() {
                    return locaton;
                }

                public String getLat() {
                    return lat;
                }

                public String getLng() {
                    return lng;
                }

                public String getDistance() {
                    return distance;
                }

                public String getDuration() {
                    return duration;
                }

                public String getFare() {
                    return fare;
                }

                public String getWait_time() {
                    return wait_time;
                }

                // Setter Methods 

                public void setLocaton(String locaton) {
                    this.locaton = locaton;
                }

                public void setLat(String lat) {
                    this.lat = lat;
                }

                public void setLng(String lng) {
                    this.lng = lng;
                }

                public void setDistance(String distance) {
                    this.distance = distance;
                }

                public void setDuration(String duration) {
                    this.duration = duration;
                }

                public void setFare(String fare) {
                    this.fare = fare;
                }

                public void setWait_time(String wait_time) {
                    this.wait_time = wait_time;
                }
            }
            // Getter Methods

            public String getCatrgory_id() {
                return catrgory_id;
            }

            public String getCatrgory_name() {
                return catrgory_name;
            }

            public String getPickup() {
                return pickup;
            }

            public String getDrop() {
                return drop;
            }

            public String getEta_mode() {
                return eta_mode;
            }

            public String getEta_time() {
                return eta_time;
            }

            public String getEta_unit() {
                return eta_unit;
            }

            public String getAmount() {
                return amount;
            }

            public String getRide_amount() {
                return ride_amount;
            }

            public String getPhantom_car() {
                return phantom_car;
            }

            public String getUser_name() {
                return user_name;
            }

            public String getDistance() {
                return distance;
            }

            public String getAtt() {
                return att;
            }

            public String getPeak_time() {
                return peak_time;
            }

            public String getStop_one_eta() {
                return stop_one_eta;
            }

            public String getStop_two_eta() {
                return stop_two_eta;
            }

            public String getStop_three_eta() {
                return stop_three_eta;
            }

            public String getStop_four_eta() {
                return stop_four_eta;
            }

            public String getNight_charge() {
                return night_charge;
            }

            public String getTotal_waiting_charge() {
                return total_waiting_charge;
            }

            public String getNote() {
                return note;
            }

            public String getIcon_normal() {
                return icon_normal;
            }

            public String getIcon_active() {
                return icon_active;
            }

            public String getIcon_car_image() {
                return icon_car_image;
            }

            // Setter Methods

            public void setCatrgory_id(String catrgory_id) {
                this.catrgory_id = catrgory_id;
            }

            public void setCatrgory_name(String catrgory_name) {
                this.catrgory_name = catrgory_name;
            }

            public void setPickup(String pickup) {
                this.pickup = pickup;
            }

            public void setDrop(String drop) {
                this.drop = drop;
            }

            public void setEta_mode(String eta_mode) {
                this.eta_mode = eta_mode;
            }

            public void setEta_time(String eta_time) {
                this.eta_time = eta_time;
            }

            public void setEta_unit(String eta_unit) {
                this.eta_unit = eta_unit;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public void setRide_amount(String ride_amount) {
                this.ride_amount = ride_amount;
            }

            public void setPhantom_car(String phantom_car) {
                this.phantom_car = phantom_car;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public void setAtt(String att) {
                this.att = att;
            }

            public void setPeak_time(String peak_time) {
                this.peak_time = peak_time;
            }

            public void setStop_one_eta(String stop_one_eta) {
                this.stop_one_eta = stop_one_eta;
            }

            public void setStop_two_eta(String stop_two_eta) {
                this.stop_two_eta = stop_two_eta;
            }

            public void setStop_three_eta(String stop_three_eta) {
                this.stop_three_eta = stop_three_eta;
            }

            public void setStop_four_eta(String stop_four_eta) {
                this.stop_four_eta = stop_four_eta;
            }

            public void setNight_charge(String night_charge) {
                this.night_charge = night_charge;
            }

            public void setTotal_waiting_charge(String total_waiting_charge) {
                this.total_waiting_charge = total_waiting_charge;
            }

            public void setNote(String note) {
                this.note = note;
            }

            public void setIcon_normal(String icon_normal) {
                this.icon_normal = icon_normal;
            }

            public void setIcon_active(String icon_active) {
                this.icon_active = icon_active;
            }

            public void setIcon_car_image(String icon_car_image) {
                this.icon_car_image = icon_car_image;
            }

        }
    }
}
