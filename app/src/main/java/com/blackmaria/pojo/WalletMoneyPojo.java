package com.blackmaria.pojo;

/**
 * Created by user14 on 12/8/2016.
 */

public class WalletMoneyPojo {

    private String paymnet_name, payment_code,payment_active_img,payment_normal_img,payment_selected_payment_id;

    String xenditBankName;
    String xenditBankCode;
    String xendiActiveImage;

    public String getXendiActiveImage() {
        return xendiActiveImage;
    }

    public void setXendiActiveImage(String xendiActiveImage) {
        this.xendiActiveImage = xendiActiveImage;
    }



    public String getXenditBankCode() {
        return xenditBankCode;
    }

    public void setXenditBankCode(String xenditBankCode) {
        this.xenditBankCode = xenditBankCode;
    }


    public String getXenditBankName() {
        return xenditBankName;
    }

    public void setXenditBankName(String xenditBankName) {
        this.xenditBankName = xenditBankName;
    }




    public String getPayment_code() {
        return payment_code;
    }

    public void setPayment_code(String payment_code) {
        this.payment_code = payment_code;
    }

    public String getPaymnet_name() {
        return paymnet_name;
    }

    public void setPaymnet_name(String paymnet_name) {
        this.paymnet_name = paymnet_name;
    }

    public String getPayment_active_img() {
        return payment_active_img;
    }

    public void setPayment_active_img(String payment_active_img) {
        this.payment_active_img = payment_active_img;
    }

    public String getPayment_normal_img() {
        return payment_normal_img;
    }

    public void setPayment_normal_img(String payment_normal_img) {
        this.payment_normal_img = payment_normal_img;
    }

    public String getPayment_selected_payment_id() {
        return payment_selected_payment_id;
    }

    public void setPayment_selected_payment_id(String payment_selected_payment_id) {
        this.payment_selected_payment_id = payment_selected_payment_id;
    }
}
