package com.blackmaria.pojo;

/**
 * Created by user14 on 12/22/2016.
 */

public class FareBreakupPojo {

    private String title,value,currency_status;

    public String getCurrency_status() {
        return currency_status;
    }

    public void setCurrency_status(String currency_status) {
        this.currency_status = currency_status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
