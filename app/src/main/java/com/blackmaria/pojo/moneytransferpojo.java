package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class moneytransferpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;

    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_image() {
            return user_image;
        }

        public void setUser_image(String user_image) {
            this.user_image = user_image;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        @SerializedName("user_name")
        private String user_name;
        @SerializedName("user_image")
        private String user_image;
        @SerializedName("city")
        private String city;
        @SerializedName("country_code")
        private String country_code;
        @SerializedName("phone_number")
        private String phone_number;
        @SerializedName("status")
        private String status;
        @SerializedName("user_id")
        private String user_id;
        @SerializedName("cross_border")
        private String cross_border;


        // Getter Methods

        public String getCity() {
            return city;
        }


        public String getStatus() {
            return status;
        }


        public String getCross_border() {
            return cross_border;
        }

        // Setter Methods

        public void setCity(String city) {
            this.city = city;
        }

        public void setStatus(String status) {
            this.status = status;
        }
        public void setCross_border(String cross_border) {
            this.cross_border = cross_border;
        }
    }
}


