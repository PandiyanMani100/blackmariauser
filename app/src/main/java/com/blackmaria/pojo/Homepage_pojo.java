package com.blackmaria.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Homepage_pojo implements Serializable{

    @SerializedName("status")
    private String status;
    @SerializedName("referral_staus")
    private String referral_staus;

    public ArrayList<Booking_banner_arr> getBooking_banner_arr() {
        return booking_banner_arr;
    }

    public void setBooking_banner_arr(ArrayList<Booking_banner_arr> booking_banner_arr) {
        this.booking_banner_arr = booking_banner_arr;
    }

    @SerializedName("booking_banner_arr")
    ArrayList< Booking_banner_arr > booking_banner_arr = new ArrayList < Booking_banner_arr > ();

    public class Booking_banner_arr{
        @SerializedName("name")
        private String name;
        @SerializedName("url")
        private String url;
        @SerializedName("file")
        private String file;
        @SerializedName("file_type")
        private String file_type;


        // Getter Methods

        public String getName() {
            return name;
        }

        public String getUrl() {
            return url;
        }

        public String getFile() {
            return file;
        }

        public String getFile_type() {
            return file_type;
        }

        // Setter Methods

        public void setName(String name) {
            this.name = name;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public void setFile_type(String file_type) {
            this.file_type = file_type;
        }
    }

    public String getPhoto_video_type() {
        return photo_video_type;
    }

    public void setPhoto_video_type(String photo_video_type) {
        this.photo_video_type = photo_video_type;
    }

    @SerializedName("photo_video_type")
    private String photo_video_type;


    public String getPhoto_video() {
        return photo_video;
    }

    public void setPhoto_video(String photo_video) {
        this.photo_video = photo_video;
    }

    @SerializedName("photo_video")
    private String photo_video;

    public String getQrcode_path() {
        return qrcode_path;
    }

    public void setQrcode_path(String qrcode_path) {
        this.qrcode_path = qrcode_path;
    }

    @SerializedName("qrcode_path")
    private String qrcode_path;

    @SerializedName("saveplus_min_withdrawal_amt")
    private String saveplus_min_withdrawal_amt;

    @SerializedName("saveplus_withdrawal_status")
    private String saveplus_withdrawal_status;

    @SerializedName("pattern_code")
    private String pattern_code;

    @SerializedName("pattern_code_status")
    private String pattern_code_status;

    public String getPattern_code() {
        return pattern_code;
    }

    public void setPattern_code(String pattern_code) {
        this.pattern_code = pattern_code;
    }

    public String getPattern_code_status() {
        return pattern_code_status;
    }

    public void setPattern_code_status(String pattern_code_status) {
        this.pattern_code_status = pattern_code_status;
    }

    public String getSaveplus_id() {
        return saveplus_id;
    }

    public void setSaveplus_id(String saveplus_id) {
        this.saveplus_id = saveplus_id;
    }

    @SerializedName("saveplus_id")
    private String saveplus_id;

    public String getSaveplus_min_withdrawal_amt() {
        return saveplus_min_withdrawal_amt;
    }

    public void setSaveplus_min_withdrawal_amt(String saveplus_min_withdrawal_amt) {
        this.saveplus_min_withdrawal_amt = saveplus_min_withdrawal_amt;
    }

    public String getSaveplus_withdrawal_status() {
        return saveplus_withdrawal_status;
    }

    public void setSaveplus_withdrawal_status(String saveplus_withdrawal_status) {
        this.saveplus_withdrawal_status = saveplus_withdrawal_status;
    }

    public String getSaveplus_balance() {
        return saveplus_balance;
    }

    public void setSaveplus_balance(String saveplus_balance) {
        this.saveplus_balance = saveplus_balance;
    }

    @SerializedName("saveplus_balance")
    private String saveplus_balance;

    @SerializedName("comp_stage")
    private String comp_stage;
    @SerializedName("comp_wait_time")
    private String comp_wait_time;
    @SerializedName("comp_status")
    private String comp_status;
    @SerializedName("info_badge")
    private String info_badge;
    @SerializedName("wallet_amount")
    private String wallet_amount;
    @SerializedName("bid_fare")
    private String bid_fare;

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    @SerializedName("user_image")
    private String user_image;

    public String getWith_pincode() {
        return with_pincode;
    }

    public void setWith_pincode(String with_pincode) {
        this.with_pincode = with_pincode;
    }

    @SerializedName("with_pincode")
    private String with_pincode;

    public String getBusiness_profile_status() {
        return business_profile_status;
    }

    public void setBusiness_profile_status(String business_profile_status) {
        this.business_profile_status = business_profile_status;
    }

    @SerializedName("business_profile_status")
    private String business_profile_status;

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    @SerializedName("business_name")
    private String business_name;

    public String getEmergency_contact() {
        return emergency_contact;
    }

    public void setEmergency_contact(String emergency_contact) {
        this.emergency_contact = emergency_contact;
    }

    @SerializedName("emergency_contact")
    private String emergency_contact;

    @SerializedName("ride_min")
    private String ride_min;
    @SerializedName("currencyCode")
    private String currencyCode;
    @SerializedName("on_goingride")
    private String on_goingride;
    @SerializedName("on_goingrideid")
    private String on_goingrideid;
    @SerializedName("on_going_txt")
    private String on_going_txt;
    @SerializedName("driver_request_mode")
    private String driver_request_mode;
    @SerializedName("total_ticket_Count")
    private String total_ticket_Count;
    @SerializedName("open_ticket_Count")
    private String open_ticket_Count;
    @SerializedName("closed_ticket_Count")
    private String closed_ticket_Count;
    @SerializedName("user_active")
    private String user_active;
    @SerializedName("user_location")
    private String user_location;
    @SerializedName("ride_status")
    private String ride_status;
    @SerializedName("roaming_mode")
    private String roaming_mode;
    @SerializedName("roaming_status")
    private String roaming_status;
    @SerializedName("tot_distance")
    private String tot_distance;
    @SerializedName("distance_unit")
    private String distance_unit;
    @SerializedName("profile_complete")
    private String profile_complete;
    @SerializedName("basic_complete")
    private String basic_complete;
    @SerializedName("xendit_public_key")
    private String xendit_public_key;
    @SerializedName("exchange_value")
    private String exchange_value;
    @SerializedName("payment_mode")
    private String payment_mode;
    @SerializedName("grand_fare")
    private String grand_fare;
    @SerializedName("dashboard_image")
    private String dashboard_image;
    @SerializedName("support_no")
    private String support_no;
    @SerializedName("coupen_count")
    private String coupen_count;
    @SerializedName("ticket_Count")
    private String ticket_Count;
    @SerializedName("message_count")
    private String message_count;
    @SerializedName("trip_Count")
    private String trip_Count;
    @SerializedName("available_count")
    private String available_count;


    public ArrayList<BookingPojo1> getDrivers() {
        return drivers;
    }

    public void setDrivers(ArrayList<BookingPojo1> drivers) {
        this.drivers = drivers;
    }

    @SerializedName("drivers")
    ArrayList< BookingPojo1 > drivers = new ArrayList < BookingPojo1 > ();


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public String getReferral_staus() {
        return referral_staus;
    }

    public String getComp_stage() {
        return comp_stage;
    }

    public String getComp_wait_time() {
        return comp_wait_time;
    }

    public String getComp_status() {
        return comp_status;
    }

    public String getInfo_badge() {
        return info_badge;
    }

    public String getWallet_amount() {
        return wallet_amount;
    }

    public String getBid_fare() {
        return bid_fare;
    }

    public String getRide_min() {
        return ride_min;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getOn_goingride() {
        return on_goingride;
    }

    public String getOn_goingrideid() {
        return on_goingrideid;
    }

    public String getOn_going_txt() {
        return on_going_txt;
    }

    public String getDriver_request_mode() {
        return driver_request_mode;
    }

    public String getTotal_ticket_Count() {
        return total_ticket_Count;
    }

    public String getOpen_ticket_Count() {
        return open_ticket_Count;
    }

    public String getClosed_ticket_Count() {
        return closed_ticket_Count;
    }

    public String getUser_active() {
        return user_active;
    }

    public String getUser_location() {
        return user_location;
    }

    public String getRide_status() {
        return ride_status;
    }

    public String getRoaming_mode() {
        return roaming_mode;
    }

    public String getRoaming_status() {
        return roaming_status;
    }

    public String getTot_distance() {
        return tot_distance;
    }

    public String getDistance_unit() {
        return distance_unit;
    }

    public String getProfile_complete() {
        return profile_complete;
    }

    public String getBasic_complete() {
        return basic_complete;
    }

    public String getXendit_public_key() {
        return xendit_public_key;
    }

    public String getExchange_value() {
        return exchange_value;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public String getGrand_fare() {
        return grand_fare;
    }

    public String getDashboard_image() {
        return dashboard_image;
    }

    public String getSupport_no() {
        return support_no;
    }

    public String getCoupen_count() {
        return coupen_count;
    }

    public String getTicket_Count() {
        return ticket_Count;
    }

    public String getMessage_count() {
        return message_count;
    }

    public String getTrip_Count() {
        return trip_Count;
    }

    public String getAvailable_count() {
        return available_count;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setReferral_staus(String referral_staus) {
        this.referral_staus = referral_staus;
    }

    public void setComp_stage(String comp_stage) {
        this.comp_stage = comp_stage;
    }

    public void setComp_wait_time(String comp_wait_time) {
        this.comp_wait_time = comp_wait_time;
    }

    public void setComp_status(String comp_status) {
        this.comp_status = comp_status;
    }

    public void setInfo_badge(String info_badge) {
        this.info_badge = info_badge;
    }

    public void setWallet_amount(String wallet_amount) {
        this.wallet_amount = wallet_amount;
    }

    public void setBid_fare(String bid_fare) {
        this.bid_fare = bid_fare;
    }

    public void setRide_min(String ride_min) {
        this.ride_min = ride_min;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setOn_goingride(String on_goingride) {
        this.on_goingride = on_goingride;
    }

    public void setOn_goingrideid(String on_goingrideid) {
        this.on_goingrideid = on_goingrideid;
    }

    public void setOn_going_txt(String on_going_txt) {
        this.on_going_txt = on_going_txt;
    }

    public void setDriver_request_mode(String driver_request_mode) {
        this.driver_request_mode = driver_request_mode;
    }

    public void setTotal_ticket_Count(String total_ticket_Count) {
        this.total_ticket_Count = total_ticket_Count;
    }

    public void setOpen_ticket_Count(String open_ticket_Count) {
        this.open_ticket_Count = open_ticket_Count;
    }

    public void setClosed_ticket_Count(String closed_ticket_Count) {
        this.closed_ticket_Count = closed_ticket_Count;
    }

    public void setUser_active(String user_active) {
        this.user_active = user_active;
    }

    public void setUser_location(String user_location) {
        this.user_location = user_location;
    }

    public void setRide_status(String ride_status) {
        this.ride_status = ride_status;
    }

    public void setRoaming_mode(String roaming_mode) {
        this.roaming_mode = roaming_mode;
    }

    public void setRoaming_status(String roaming_status) {
        this.roaming_status = roaming_status;
    }

    public void setTot_distance(String tot_distance) {
        this.tot_distance = tot_distance;
    }

    public void setDistance_unit(String distance_unit) {
        this.distance_unit = distance_unit;
    }

    public void setProfile_complete(String profile_complete) {
        this.profile_complete = profile_complete;
    }

    public void setBasic_complete(String basic_complete) {
        this.basic_complete = basic_complete;
    }

    public void setXendit_public_key(String xendit_public_key) {
        this.xendit_public_key = xendit_public_key;
    }

    public void setExchange_value(String exchange_value) {
        this.exchange_value = exchange_value;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public void setGrand_fare(String grand_fare) {
        this.grand_fare = grand_fare;
    }

    public void setDashboard_image(String dashboard_image) {
        this.dashboard_image = dashboard_image;
    }

    public void setSupport_no(String support_no) {
        this.support_no = support_no;
    }

    public void setCoupen_count(String coupen_count) {
        this.coupen_count = coupen_count;
    }

    public void setTicket_Count(String ticket_Count) {
        this.ticket_Count = ticket_Count;
    }

    public void setMessage_count(String message_count) {
        this.message_count = message_count;
    }

    public void setTrip_Count(String trip_Count) {
        this.trip_Count = trip_Count;
    }

    public void setAvailable_count(String available_count) {
        this.available_count = available_count;
    }

    public class Category{
        @SerializedName("id")
        private String id;
        @SerializedName("name")
        private String name;
        @SerializedName("eta")
        private String eta;
        @SerializedName("cab_count")
        private String cab_count;
        @SerializedName("no_of_seats")
        private String no_of_seats;
        @SerializedName("type")
        private String type;
        @SerializedName("offer_type")
        private String offer_type;
        @SerializedName("vehicle_type")
        private String vehicle_type;
        @SerializedName("description")
        private String description;
        @SerializedName("eta_time")
        private String eta_time;
        @SerializedName("eta_unit")
        private String eta_unit;
        @SerializedName("free_waiting_time")
        private String free_waiting_time;
        @SerializedName("waiting_time_per")
        private String waiting_time_per;
        @SerializedName("icon_normal")
        private String icon_normal;
        @SerializedName("icon_active")
        private String icon_active;
        @SerializedName("icon_car_image")
        private String icon_car_image;


        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getEta() {
            return eta;
        }

        public String getCab_count() {
            return cab_count;
        }

        public String getNo_of_seats() {
            return no_of_seats;
        }

        public String getType() {
            return type;
        }

        public String getOffer_type() {
            return offer_type;
        }

        public String getVehicle_type() {
            return vehicle_type;
        }

        public String getDescription() {
            return description;
        }

        public String getEta_time() {
            return eta_time;
        }

        public String getEta_unit() {
            return eta_unit;
        }

        public String getFree_waiting_time() {
            return free_waiting_time;
        }

        public String getWaiting_time_per() {
            return waiting_time_per;
        }

        public String getIcon_normal() {
            return icon_normal;
        }

        public String getIcon_active() {
            return icon_active;
        }

        public String getIcon_car_image() {
            return icon_car_image;
        }

        // Setter Methods

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setEta(String eta) {
            this.eta = eta;
        }

        public void setCab_count(String cab_count) {
            this.cab_count = cab_count;
        }

        public void setNo_of_seats(String no_of_seats) {
            this.no_of_seats = no_of_seats;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setOffer_type(String offer_type) {
            this.offer_type = offer_type;
        }

        public void setVehicle_type(String vehicle_type) {
            this.vehicle_type = vehicle_type;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setEta_time(String eta_time) {
            this.eta_time = eta_time;
        }

        public void setEta_unit(String eta_unit) {
            this.eta_unit = eta_unit;
        }

        public void setFree_waiting_time(String free_waiting_time) {
            this.free_waiting_time = free_waiting_time;
        }

        public void setWaiting_time_per(String waiting_time_per) {
            this.waiting_time_per = waiting_time_per;
        }

        public void setIcon_normal(String icon_normal) {
            this.icon_normal = icon_normal;
        }

        public void setIcon_active(String icon_active) {
            this.icon_active = icon_active;
        }

        public void setIcon_car_image(String icon_car_image) {
            this.icon_car_image = icon_car_image;
        }
    }
}