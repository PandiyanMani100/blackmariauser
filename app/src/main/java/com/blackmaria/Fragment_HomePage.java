package com.blackmaria;

/**
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.adapter.BookMyRide_Adapter;
import com.blackmaria.hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.pojo.HomePojo;
import com.blackmaria.pojo.MultiDropPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.CurrencySymbolConverter;
import com.blackmaria.utils.GPSTracker;
import com.blackmaria.utils.HorizontalListView;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;
import com.blackmaria.widgets.CustomEdittext;
import com.blackmaria.widgets.CustomTextView;
import com.blackmaria.widgets.PkDialog;
import com.blackmaria.xmpp.Getlocation;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import me.drakeet.materialdialog.MaterialDialog;


public class Fragment_HomePage extends FragmentActivityHockeyApp implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, RoutingListener, AdapterView.OnItemSelectedListener {

    private Context context;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private GoogleMap googleMap;
    private GoogleMap googleMap_confirm;
    private MarkerOptions marker;
    private static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
    private SessionManager session;
    GPSTracker gps;

    private CustomTextView Tv_PickupAddress, Tv_DestinationAddress;
    private ImageView Iv_Favourite, Iv_CenterMarker;
    private HorizontalListView Hlv_carTypes;
    private LinearLayout Ll_OneWay, Ll_Return, Ll_MultipleStop, Ll_Tracking;
    private CustomEdittext Et_NoOfPassenger;
    private RelativeLayout Rl_PickupLater, Rl_PickupNow;
    private CustomTextView Tv_PickupNow, Tv_PickupLater;
    private ImageView Iv_Home;
    private RelativeLayout Rl_Coupon, Rl_ReferralCode, Rl_Estimate;
    private CustomTextView Tv_TripType, Tv_CarType, Tv_CarArriveTime, Tv_FareAmount, Tv_Distance, Tv_couponCode, Tv_reffrealCode;
    private RelativeLayout Rl_PickUpLayout, Rl_ConfirmCancelLayout;
    private RelativeLayout Rl_Bottom_layout, Rl_Alert_layout;
    private TextView TV_Alert_textview;
    private ImageView Iv_OneWay, Iv_Return, Iv_MultipleStop, Iv_Tracking, currentLocation_image;
    private Spinner Passenger_Spinner;

    private ServiceRequest mRequest;
    private double Recent_lat = 0.0, Recent_long = 0.0;
    private double MyCurrent_lat = 0.0, MyCurrent_long = 0.0;
    private BookMyRide_Adapter adapter;

    String SselectedAddress = "";
    String Sselected_latitude = "", Sselected_longitude = "";

    ArrayList<HomePojo> driver_list = new ArrayList<HomePojo>();
    ArrayList<HomePojo> category_list = new ArrayList<HomePojo>();
    ArrayList<HomePojo> ratecard_list = new ArrayList<HomePojo>();
    private boolean driver_status = false;
    private boolean category_status = false;
    private boolean ratecard_status = false;
    private boolean main_response_status = false;
    String UserName = "";
    private boolean isLoading = false;
    private String ScurrencySymbol = "";
    private String UserID = "", CategoryID = "";
    private String CarAvailable = "";
    private String ScarType = "";
    private String selectedType = "";

    private int search_status = 0;
    private String SdestinationLatitude = "";
    private String SdestinationLongitude = "";
    private String SdestinationLocation = "";

    private ArrayList<MultiDropPojo> multiple_dropList;

    private int timer_request_code = 100;
    private int placeSearch_request_code = 200;
    private int multiplePlaceSearch_request_code = 300;
    private int coupon_request_code = 400;
    private int favoriteList_request_code = 500;

    private List<Polyline> polylines;
    private ArrayList<LatLng> wayPointList;
    private LatLngBounds.Builder wayPointBuilder;
    private LatLng startWayPoint, endWayPoint;
    private String sWayPointStatus = "Confirmed";
    private String[] multipleDropLatLng;


    private SimpleDateFormat mFormatter = new SimpleDateFormat("MMM/dd,hh:mm aa");
    private SimpleDateFormat coupon_mFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat coupon_time_mFormatter = new SimpleDateFormat("hh:mm aa");
    private SimpleDateFormat mTime_Formatter = new SimpleDateFormat("HH");

    //-----Declaration For Enabling Gps-------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 299;
    private String sMultipleDropStatus = "0";
    private String STrip_Type = "";
    private int SListPosition;
    private int NoOfSeats;
    ArrayList<String> passengers;
    private String StrackingPercent = "";
    MapFragment mapFragment;
    private TextView tv_apply, tv_cancel;
    //------Declaration for Coupon code-----
    private RelativeLayout referral_apply_layout, referral_loading_layout, referral_allowance_layout;
    private MaterialDialog refferal_dialog;
    private EditText referral_edittext;
    private TextView referral_allowance;
    private ImageView Iv_coupon_cancel_Icon;
    private String Str_referralCode = "";
    private String sCouponAllowanceText = "";
    private Dialog dialog;

    private String selectedDate = "", Str_driver_image = "";
    private String selectedTime = "";
    private String response_time = "";
    private String riderId = "";
    private String ScouponSource = "", ScouponCode = "";
    private String Stracking = "0";
    private String SetaAmount = "", SetaDistance = "", carCatImage = "";

    private Dialog admin_track_dialog;
    private CheckBox CB_terms;


    private RefreshReceiver refreshReceiver;


    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");

                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                       /* Intent intent1 = new Intent(Fragment_HomePage.this, Fragment_HomePage.class);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();*/
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(Fragment_HomePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(Fragment_HomePage.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            }/*else if (intent.getAction().equals("com.app.pushnotification.RideAccept")) {

                session.setCouponCode("", "");
                session.setReferralCode("", "");

                Intent i = new Intent(Fragment_HomePage.this, TrackRideAcceptPage.class);
                i.putExtra("driverID", intent.getStringExtra("driverID"));
                i.putExtra("driverName", intent.getStringExtra("driverName"));
                i.putExtra("driverImage", intent.getStringExtra("driverImage"));
                i.putExtra("driverRating", intent.getStringExtra("driverRating"));
                i.putExtra("driverTime", intent.getStringExtra("driverTime"));
                i.putExtra("rideID", intent.getStringExtra("rideID"));
                i.putExtra("driverMobile", intent.getStringExtra("driverMobile"));
                i.putExtra("driverCar_no", intent.getStringExtra("driverCar_no"));
                i.putExtra("driverCar_model", intent.getStringExtra("driverCar_model"));
                startActivity(i);
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }*/
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage_fragment);
        context = Fragment_HomePage.this;
        context.startService(new Intent(context, Getlocation.class));

        initialize();
        initializeMap();


        Tv_PickupAddress.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                search_status = 0;
                Intent intent = new Intent(Fragment_HomePage.this, LocationSearch.class);
                startActivityForResult(intent, placeSearch_request_code);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        Tv_DestinationAddress.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                search_status = 1;
                Intent intent = new Intent(Fragment_HomePage.this, DropLocationSelect.class);
                startActivityForResult(intent, placeSearch_request_code);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        Iv_Favourite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                search_status = 0;
                if (Tv_PickupAddress.getText().toString().length() > 0) {
                    Intent intent = new Intent(Fragment_HomePage.this, FavoriteList.class);
                    intent.putExtra("SelectedAddress", SselectedAddress);
                    intent.putExtra("SelectedLatitude", Sselected_latitude);
                    intent.putExtra("SelectedLongitude", Sselected_longitude);
                    startActivityForResult(intent, favoriteList_request_code);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.favorite_list_label_select_location));
                }

            }
        });


        Iv_CenterMarker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        Hlv_carTypes.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                if (category_list.size() > 0) {
                    CarAvailable = category_list.get(position).getCat_time();
                    CategoryID = category_list.get(position).getCat_id();
                    ScarType = category_list.get(position).getCat_name();

                    cd = new ConnectionDetector(Fragment_HomePage.this);
                    isInternetPresent = cd.isConnectingToInternet();

                    if (Recent_lat != 0.0) {
                        googleMap.clear();

                        if (isInternetPresent) {
                            if (mRequest != null) {
                                mRequest.cancelRequest();
                            }
                            isLoading = true;
                            PostRequest(Iconstant.BookMyRide_url, Recent_lat, Recent_long);
                        } else {
                            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                        }
                    }
                }
            }
        });
        Rl_PickupLater.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Tv_PickupLater.getText().toString().equalsIgnoreCase(getResources().getString(R.string.pickup_later_lable))) {

                    if (Tv_PickupAddress.getText().toString().length() > 0) {

                        if (!getResources().getString(R.string.type_your_destination_lable).equalsIgnoreCase(Tv_DestinationAddress.getText().toString()) && Tv_DestinationAddress.getText().toString().length() > 0) {
                            selectedType = "1";

                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(new Date());
                            calendar.add(Calendar.DAY_OF_YEAR, 7);
                            Date seventhDay = calendar.getTime();

                            new SlideDateTimePicker.Builder(getSupportFragmentManager())
                                    .setListener(listener)
                                    .setInitialDate(new Date())
                                    .setMinDate(new Date())
                                    .setMaxDate(seventhDay)
                                    //.setIs24HourTime(true)
                                    .setTheme(SlideDateTimePicker.HOLO_LIGHT)
                                    .setIndicatorColor(Color.parseColor("#F83C6F"))
                                    .build()
                                    .show();
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.home_label_invalid_drop));
                        }
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.home_label_invalid_pickUp));
                    }

                } else if (getResources().getString(R.string.cancel_lable).equalsIgnoreCase(Tv_PickupLater.getText().toString())) {

                    Rl_PickUpLayout.setVisibility(View.VISIBLE);
                    Rl_ConfirmCancelLayout.setVisibility(View.GONE);
                    Rl_PickupNow.setBackgroundColor(getResources().getColor(R.color.app_secondary_color));
                    Tv_PickupNow.setText(getResources().getString(R.string.pickup_now_lable));
                    Rl_PickupLater.setBackgroundColor(getResources().getColor(R.color.app_secondary_color));
                    Tv_PickupLater.setText(getResources().getString(R.string.pickup_later_lable));
                    currentLocation_image.setClickable(true);
                    currentLocation_image.setVisibility(View.VISIBLE);
                    Iv_Favourite.setVisibility(View.VISIBLE);

                    Tv_PickupAddress.setEnabled(true);
//                    //--------enable the map functionality---------
//                    googleMap.getUiSettings().setAllGesturesEnabled(true);
//                    googleMap_confirm.getUiSettings().setAllGesturesEnabled(true);

                    if (isInternetPresent) {
                        if (mRequest != null) {
                            mRequest.cancelRequest();
                        }

                        Rl_PickupNow.setEnabled(false);
                        Rl_PickupLater.setEnabled(false);
                        isLoading = true;
                        PostRequest(Iconstant.BookMyRide_url, Recent_lat, Recent_long);
                    } else {
                        Rl_Alert_layout.setVisibility(View.VISIBLE);
                        TV_Alert_textview.setText(getResources().getString(R.string.alert_nointernet));
                        Rl_Bottom_layout.setVisibility(View.GONE);
                    }


                   /* if (!driver_status) {
                        Tv_PickupNow.setTextColor(Color.parseColor("#848484"));
                        Rl_PickupNow.setClickable(true);

                    } else {

                        Tv_PickupNow.setTextColor(Color.parseColor("#FFFFFF"));
                        Rl_PickupNow.setClickable(true);
                    }*/
                }

            }
        });

        Rl_PickupNow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getResources().getString(R.string.pickup_now_lable).equalsIgnoreCase(Tv_PickupNow.getText().toString())) {

                    if (Tv_PickupAddress.getText().toString().length() > 0) {

                        if (!getResources().getString(R.string.type_your_destination_lable).equalsIgnoreCase(Tv_DestinationAddress.getText().toString()) && Tv_DestinationAddress.getText().toString().length() > 0) {
                            selectedType = "0";
                            //-------getting current date and time---------
                            selectedDate = coupon_mFormatter.format(new Date());
                            selectedTime = coupon_time_mFormatter.format(new Date());

                            EstimateParam_method();
/*
                            Tv_PickupAddress.setEnabled(false);
                            Rl_PickUpLayout.setVisibility(View.GONE);
                            Rl_ConfirmCancelLayout.setVisibility(View.VISIBLE);
                            Rl_PickupNow.setBackgroundColor(getResources().getColor(R.color.app_color));
                            Tv_PickupNow.setText(getResources().getString(R.string.confirm_lable));
                            Rl_PickupLater.setBackgroundColor(getResources().getColor(R.color.app_color));
                            Tv_PickupLater.setText(getResources().getString(R.string.cancel_lable));*/

                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.home_label_invalid_drop));
                        }

                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.home_label_invalid_pickUp));
                    }

                } else if (getResources().getString(R.string.confirm_lable).equalsIgnoreCase(Tv_PickupNow.getText().toString())) {
                    cd = new ConnectionDetector(Fragment_HomePage.this);
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {

                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("user_id", UserID);
                        jsonParams.put("pickup", Tv_PickupAddress.getText().toString());
                        jsonParams.put("pickup_lat", String.valueOf(Recent_lat));
                        jsonParams.put("pickup_lon", String.valueOf(Recent_long));
                        jsonParams.put("drop_loc", Tv_DestinationAddress.getText().toString());
                        jsonParams.put("drop_lat", SdestinationLatitude);
                        jsonParams.put("drop_lon", SdestinationLongitude);
                        jsonParams.put("category", CategoryID);
                        jsonParams.put("type", selectedType);
                        jsonParams.put("pickup_date", selectedDate);
                        jsonParams.put("pickup_time", selectedTime);
                        jsonParams.put("source", ScouponSource);//coupon/redeem
                        jsonParams.put("code", ScouponCode);
                        jsonParams.put("tracking", Stracking);//tracking:1    => 0/1
                        jsonParams.put("mode", STrip_Type);//: =>oneway/return/multistop
                        jsonParams.put("no_of_passenger", Passenger_Spinner.getSelectedItem().toString());
                        jsonParams.put("referal_code", Str_referralCode);
                        jsonParams.put("est_cost", SetaAmount);
                        jsonParams.put("approx_dist", SetaDistance);


                        if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(STrip_Type)) {

                            for (int i = 0; i < multiple_dropList.size(); i++) {
                                jsonParams.put("multipath_loc[" + i + "][drop_loc]", multiple_dropList.get(i).getPlaceName());
                                jsonParams.put("multipath_loc[" + i + "][drop_lon]", multiple_dropList.get(i).getPlaceLong());
                                jsonParams.put("multipath_loc[" + i + "][drop_lat]", multiple_dropList.get(i).getPlaceLat());
                            }
                        }
//                        riderId = "";
                        ConfirmRideRequest(Iconstant.confirm_ride_url, jsonParams, "");
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                }


            }
        });


        Ll_OneWay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!getResources().getString(R.string.type_your_destination_lable).equalsIgnoreCase(Tv_DestinationAddress.getText().toString()) && Tv_DestinationAddress.getText().toString().length() > 0) {
                    STrip_Type = getResources().getString(R.string.one_way_lable);
                    Tv_TripType.setText(getResources().getString(R.string.oneway_textview));
                    sMultipleDropStatus = "0";
                    Iv_OneWay.setBackground(getResources().getDrawable(R.drawable.onewayselect));
                    Iv_Return.setBackground(getResources().getDrawable(R.drawable.twoway_unselect));
                    Iv_MultipleStop.setBackground(getResources().getDrawable(R.drawable.multiplestop_unselect));
                    route();
                } else {
                    //show the alert for enter the destination address first
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.home_label_invalid_drop));
                }

            }
        });

        Ll_Return.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!getResources().getString(R.string.type_your_destination_lable).equalsIgnoreCase(Tv_DestinationAddress.getText().toString()) && Tv_DestinationAddress.getText().toString().length() > 0) {
                    STrip_Type = getResources().getString(R.string.return_lable);
                    Tv_TripType.setText(getResources().getString(R.string.return_textview));
                    sMultipleDropStatus = "0";
                    Iv_OneWay.setBackground(getResources().getDrawable(R.drawable.oneway_unselect));
                    Iv_Return.setBackground(getResources().getDrawable(R.drawable.twoway_select));
                    Iv_MultipleStop.setBackground(getResources().getDrawable(R.drawable.multiplestop_unselect));
                    route();
                } else {
                    //show the alert for enter the destination address first
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.home_label_invalid_drop));
                }
            }
        });
        Ll_MultipleStop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!getResources().getString(R.string.type_your_destination_lable).equalsIgnoreCase(Tv_DestinationAddress.getText().toString()) && Tv_DestinationAddress.getText().toString().length() > 0) {
                  /*  Intent intent = new Intent(Fragment_HomePage.this, MultipleDropLocation.class);
                    if (multiple_dropList.size() > 0) {
                        Bundle bundleObject = new Bundle();
                        bundleObject.putSerializable("MultipleDropLocation_List", multiple_dropList);
                        intent.putExtras(bundleObject);
                    }
                    startActivityForResult(intent, multiplePlaceSearch_request_code);*/
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    //show the alert for enter the destination address first
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.home_label_invalid_drop));
                }
            }
        });
        Ll_Tracking.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!getResources().getString(R.string.type_your_destination_lable).equalsIgnoreCase(Tv_DestinationAddress.getText().toString()) && Tv_DestinationAddress.getText().toString().length() > 0) {
                    if ("0".equalsIgnoreCase(Stracking)) {
                        adminTrackDialog();
                    } else {
                        Stracking = "0";
                        Iv_Tracking.setBackground(getResources().getDrawable(R.drawable.tracking_unselect));
                    }
                } else {
                    //show the alert for enter the destination address first
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.home_label_invalid_drop));
                }

            }
        });

        Rl_ReferralCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showCoupon();
            }
        });

        Rl_Coupon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("-----------coupons--------------");
                Intent intent_coupon = new Intent(Fragment_HomePage.this, CouponPage.class);
//                intent_coupon.putExtra("page", "homepage");
                startActivityForResult(intent_coupon, coupon_request_code);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        Rl_Estimate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("user_id", UserID);
                jsonParams.put("pickup", Tv_PickupAddress.getText().toString());
                jsonParams.put("pickup_lat", String.valueOf(Recent_lat));
                jsonParams.put("pickup_lon", String.valueOf(Recent_long));
                jsonParams.put("drop", Tv_DestinationAddress.getText().toString());
                jsonParams.put("drop_lat", SdestinationLatitude);
                jsonParams.put("drop_lon", SdestinationLongitude);
                jsonParams.put("category", CategoryID);
                jsonParams.put("type", selectedType);
                jsonParams.put("pickup_date", selectedDate);
                jsonParams.put("pickup_time", selectedTime);
                jsonParams.put("tracking", Stracking);//tracking:1    => 0/1
                jsonParams.put("mode", STrip_Type);//: =>oneway/return/multistop

                if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(STrip_Type)) {

                    for (int i = 0; i < multiple_dropList.size(); i++) {
                        jsonParams.put("location_arr[" + i + "][drop_loc]", multiple_dropList.get(i).getPlaceName());
                        jsonParams.put("location_arr[" + i + "][drop_lon]", multiple_dropList.get(i).getPlaceLong());
                        jsonParams.put("location_arr[" + i + "][drop_lat]", multiple_dropList.get(i).getPlaceLat());
                    }
                }
                Intent intent = new Intent(Fragment_HomePage.this, Home_RatecardPage.class);
                intent.putExtra("jsonParam", jsonParams);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        Iv_Home.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Fragment_HomePage.this, Navigation_new.class);
                startActivity(intent);

            }
        });

        currentLocation_image.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(Fragment_HomePage.this);
                isInternetPresent = cd.isConnectingToInternet();
                gps = new GPSTracker(Fragment_HomePage.this);

                if (gps.canGetLocation() && gps.isgpsenabled()) {

                    MyCurrent_lat = gps.getLatitude();
                    MyCurrent_long = gps.getLongitude();

                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }

                    // Move the camera to last position with a zoom level
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(MyCurrent_lat, MyCurrent_long)).zoom(17).build();
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                } else {
                    enableGpsService();
                    //Toast.makeText(getActivity(), "GPS not Enabled !!!", Toast.LENGTH_LONG).show();
                }
            }
        });


        OnCameraChangeListener mOnCameraChangeListener = new OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                double latitude = cameraPosition.target.latitude;
                double longitude = cameraPosition.target.longitude;

                cd = new ConnectionDetector(Fragment_HomePage.this);
                isInternetPresent = cd.isConnectingToInternet();

                Log.e("camerachange lat-->", "" + latitude);
                Log.e("on_camera_change lon-->", "" + longitude);

                if (latitude != 0.0) {
                    googleMap.clear();

                    Recent_lat = latitude;
                    Recent_long = longitude;


                    if (isInternetPresent) {
                        if (mRequest != null) {
                            mRequest.cancelRequest();
                        }

                        Rl_PickupNow.setEnabled(false);
                        Rl_PickupLater.setEnabled(false);
                        isLoading = true;
                        PostRequest(Iconstant.BookMyRide_url, latitude, longitude);
                    } else {
                        Rl_Alert_layout.setVisibility(View.VISIBLE);
                        TV_Alert_textview.setText(getResources().getString(R.string.alert_nointernet));
                        Rl_Bottom_layout.setVisibility(View.GONE);
                    }
                }
            }
        };
        if (CheckPlayService()) {
            googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
            googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String tittle = marker.getTitle();
                    return true;
                }
            });
        } else {
            //Toast.makeText(Fragment_HomePage.this, "Install Google Play service To View Location !!!", Toast.LENGTH_LONG).show();
        }

    }

    private void EstimateParam_method() {
        cd = new ConnectionDetector(Fragment_HomePage.this);
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("user_id", UserID);
            jsonParams.put("pickup", Tv_PickupAddress.getText().toString());
            jsonParams.put("pickup_lat", String.valueOf(Recent_lat));
            jsonParams.put("pickup_lon", String.valueOf(Recent_long));
            jsonParams.put("drop", Tv_DestinationAddress.getText().toString());
            jsonParams.put("drop_lat", SdestinationLatitude);
            jsonParams.put("drop_lon", SdestinationLongitude);
            jsonParams.put("category", CategoryID);
            jsonParams.put("type", selectedType);
            jsonParams.put("pickup_date", selectedDate);
            jsonParams.put("pickup_time", selectedTime);
            jsonParams.put("tracking", Stracking);//tracking:1    => 0/1
            jsonParams.put("mode", STrip_Type);//: =>oneway/return/multistop

            if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(STrip_Type)) {

                for (int i = 0; i < multiple_dropList.size(); i++) {
                    jsonParams.put("location_arr[" + i + "][drop_loc]", multiple_dropList.get(i).getPlaceName());
                    jsonParams.put("location_arr[" + i + "][drop_lon]", multiple_dropList.get(i).getPlaceLong());
                    jsonParams.put("location_arr[" + i + "][drop_lat]", multiple_dropList.get(i).getPlaceLat());
                }
            }
            PostRequest_estimate(Iconstant.estimate_price_url, jsonParams);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }


    }

    private void initialize() {
        cd = new ConnectionDetector(Fragment_HomePage.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(Fragment_HomePage.this);
        gps = new GPSTracker(Fragment_HomePage.this);
        multiple_dropList = new ArrayList<MultiDropPojo>();
        polylines = new ArrayList<Polyline>();
        wayPointList = new ArrayList<LatLng>();
        passengers = new ArrayList<String>();

        HashMap<String, String> info = session.getUserDetails();
        CategoryID = info.get(SessionManager.KEY_CATEGORY);
        UserID = info.get(SessionManager.KEY_USERID);
        UserName = info.get(SessionManager.KEY_USERNAME);

        HashMap<String, String> coupon = session.getCouponCode();
        ScouponSource = coupon.get(SessionManager.KEY_COUPON_CODE_SOURCE);
        ScouponCode = coupon.get(SessionManager.KEY_COUPON_CODE);

        HashMap<String, String> refferal = session.getReferralCode();
        Str_referralCode = refferal.get(SessionManager.KEY_REFERAL_CODE);

        session.setTimerPage("");


        Tv_PickupAddress = (CustomTextView) findViewById(R.id.book_my_ride_pickup_address_textView);
        Tv_DestinationAddress = (CustomTextView) findViewById(R.id.book_my_ride_destination_address_search_address);
        currentLocation_image = (ImageView) findViewById(R.id.book_current_location_imageview);
        Iv_Favourite = (ImageView) findViewById(R.id.book_my_ride_multiple_drop_imageview);
        Iv_CenterMarker = (ImageView) findViewById(R.id.book_my_ride_center_marker);
        Hlv_carTypes = (HorizontalListView) findViewById(R.id.book_my_ride_select_car_listview);
        Ll_OneWay = (LinearLayout) findViewById(R.id.book_my_ride_one_way_layout);
        Ll_Return = (LinearLayout) findViewById(R.id.book_my_ride_return_layout);
        Ll_MultipleStop = (LinearLayout) findViewById(R.id.book_my_ride_multiple_stop_layout);
        Ll_Tracking = (LinearLayout) findViewById(R.id.book_my_ride_tracking_layout);
        Et_NoOfPassenger = (CustomEdittext) findViewById(R.id.book_my_ride_noof_passenger_edittext);
        Iv_OneWay = (ImageView) findViewById(R.id.book_my_ride_one_way_imgview);
        Iv_Return = (ImageView) findViewById(R.id.book_my_ride_return_imgview);
        Iv_MultipleStop = (ImageView) findViewById(R.id.book_my_ride_multiple_stop_imageview);
        Iv_Tracking = (ImageView) findViewById(R.id.book_my_ride_tracking_imageview);
        Rl_PickupLater = (RelativeLayout) findViewById(R.id.book_my_ride_rideLater_layout);
        Tv_PickupLater = (CustomTextView) findViewById(R.id.book_my_ride_rideLater_textView);
        Rl_PickupNow = (RelativeLayout) findViewById(R.id.book_my_ride_rideNow_layout);
        Tv_PickupNow = (CustomTextView) findViewById(R.id.book_my_ride_rideNow_textview);
        Iv_Home = (ImageView) findViewById(R.id.book_my_ride_bottom_home_imageview);
        Rl_Coupon = (RelativeLayout) findViewById(R.id.book_my_ride_have_coupon_layout);
        Rl_ReferralCode = (RelativeLayout) findViewById(R.id.book_my_ride_rerferral_code_layout);
        Rl_Estimate = (RelativeLayout) findViewById(R.id.book_my_ride_estimate_layout);
        Tv_TripType = (CustomTextView) findViewById(R.id.book_my_ride_trip_type_textview);
        Tv_CarType = (CustomTextView) findViewById(R.id.book_my_ride_car_type_textview);
        Tv_CarArriveTime = (CustomTextView) findViewById(R.id.book_my_ride_trip_time_textview);
        Tv_FareAmount = (CustomTextView) findViewById(R.id.book_my_ride_fare_amount_textview);
        Tv_Distance = (CustomTextView) findViewById(R.id.book_my_ride_disatance_textview);

        Tv_couponCode = (CustomTextView) findViewById(R.id.book_my_ride_have_coupon_textview);
        Tv_reffrealCode = (CustomTextView) findViewById(R.id.book_my_ride_rerferral_code_textview);

        Rl_PickUpLayout = (RelativeLayout) findViewById(R.id.book_my_ride_pickupnow_bottom_layout);
        Rl_ConfirmCancelLayout = (RelativeLayout) findViewById(R.id.book_my_ride_confirm_bottom_layout);
        Rl_Bottom_layout = (RelativeLayout) findViewById(R.id.book_my_ride_bottom_layout);
        Rl_Alert_layout = (RelativeLayout) findViewById(R.id.book_my_ride_alert_layout);
        TV_Alert_textview = (TextView) findViewById(R.id.book_my_ride_alert_textView);
//        Passenger_Spinner = (Spinner) findViewById(R.id.book_my_ride_passenger_spinner);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        registerReceiver(refreshReceiver, intentFilter);

        Iv_OneWay.setBackground(getResources().getDrawable(R.drawable.onewayselect));
        Tv_TripType.setText(getResources().getString(R.string.oneway_textview));
        STrip_Type = getResources().getString(R.string.one_way_lable);

//        Passenger_Spinner.setOnItemSelectedListener(this);

//        PostRequest(Iconstant.BookMyRide_url, 13.0567237, 80.2523205);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private void initializeMap() {
        if (googleMap == null) {
            //googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.book_my_ride3_mapview)).getMap();
            mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.book_my_ride_mapview));
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap arg) {
                    loadMap(arg);
                }
            });
        }
      /*  if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.book_my_ride_mapview)).getMap();

            // check if map is created successfully or not
            if (googleMap == null) {
                //  Toast.makeText(Fragment_HomePage.this, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }
        }*/

       /* if (googleMap_confirm == null) {
            googleMap_confirm = ((MapFragment) getFragmentManager().findFragmentById(R.id.book_my_ride_mapview1)).getMap();
        }*/

        if (CheckPlayService()) {

            // Changing map type
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            // Showing / hiding your current location
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            // Enable / Disable zooming controls
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            // Enable / Disable my location button
            // Enable / Disable Compass icon
            googleMap.getUiSettings().setCompassEnabled(false);
            // Enable / Disable Rotate gesture
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);

            googleMap_confirm.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleMap_confirm.getUiSettings().setZoomControlsEnabled(false);
            googleMap_confirm.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap_confirm.getUiSettings().setCompassEnabled(false);
            googleMap_confirm.getUiSettings().setRotateGesturesEnabled(true);
            googleMap_confirm.getUiSettings().setZoomGesturesEnabled(true);
            googleMap_confirm.setMyLocationEnabled(false);

            if (gps.canGetLocation() && gps.isgpsenabled()) {
                double Dlatitude = gps.getLatitude();
                double Dlongitude = gps.getLongitude();

                MyCurrent_lat = Dlatitude;
                MyCurrent_long = Dlongitude;

                Recent_lat = Dlatitude;
                Recent_long = Dlongitude;

                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition1 = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
                googleMap_confirm.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));

            } else {

                enableGpsService();
            }
        } else {

            final PkDialog mDialog = new PkDialog(Fragment_HomePage.this);
            mDialog.setDialogTitle(getResources().getString(R.string.alert_label_title));
            mDialog.setDialogMessage(getResources().getString(R.string.action_unable_to_create_map));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();
        }


    }

    public void loadMap(GoogleMap arg) {
        googleMap = arg;
        if (CheckPlayService()) {

            // Changing map type
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            // Showing / hiding your current location
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            // Enable / Disable zooming controls
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            // Enable / Disable my location button
            // Enable / Disable Compass icon
            googleMap.getUiSettings().setCompassEnabled(false);
            // Enable / Disable Rotate gesture
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);

            googleMap_confirm.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleMap_confirm.getUiSettings().setZoomControlsEnabled(false);
            googleMap_confirm.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap_confirm.getUiSettings().setCompassEnabled(false);
            googleMap_confirm.getUiSettings().setRotateGesturesEnabled(true);
            googleMap_confirm.getUiSettings().setZoomGesturesEnabled(true);
            googleMap_confirm.setMyLocationEnabled(false);

            if (gps.canGetLocation() && gps.isgpsenabled()) {
                double Dlatitude = gps.getLatitude();
                double Dlongitude = gps.getLongitude();

                MyCurrent_lat = Dlatitude;
                MyCurrent_long = Dlongitude;

                Recent_lat = Dlatitude;
                Recent_long = Dlongitude;

                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition1 = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
                googleMap_confirm.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));

            } else {

                enableGpsService();
            }
        } else {

            final PkDialog mDialog = new PkDialog(Fragment_HomePage.this);
            mDialog.setDialogTitle(getResources().getString(R.string.alert_label_title));
            mDialog.setDialogMessage(getResources().getString(R.string.action_unable_to_create_map));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();
        }

    }

    private void adminTrackDialog() {

        admin_track_dialog = new Dialog(Fragment_HomePage.this);
        admin_track_dialog.getWindow();
        admin_track_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        admin_track_dialog.setContentView(R.layout.admin_track_alert);
        admin_track_dialog.setCanceledOnTouchOutside(true);
//        redeem_dialog.getWindow().getAttributes().windowAnimations =R.style.Animations_categories_filter;
        admin_track_dialog.show();
        admin_track_dialog.getWindow().setGravity(Gravity.CENTER);

        CustomTextView Tv_close = (CustomTextView) admin_track_dialog.findViewById(R.id.admin_track_alert_close_textview);
        RelativeLayout Rl_ok = (RelativeLayout) admin_track_dialog.findViewById(R.id.admin_track_alert_ok_layout);
        CustomTextView Tv_charge = (CustomTextView) admin_track_dialog.findViewById(R.id.admin_track_tracking_charge_textview);
        CB_terms = (CheckBox) admin_track_dialog.findViewById(R.id.admin_track_alert_home_checkbox);

        Tv_charge.setText(StrackingPercent + "% fees applied");

        Rl_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CB_terms.isChecked()) {
                    Stracking = "1";
                    Iv_Tracking.setBackground(getResources().getDrawable(R.drawable.tracking_select));
                    admin_track_dialog.dismiss();
                }

            }
        });

        Tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                admin_track_dialog.dismiss();
            }
        });

    }


    //----------------DatePicker Listener------------
    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, 1);
            Date d = cal.getTime();
            String currentTime = mTime_Formatter.format(d);
            String SselectedTime = mTime_Formatter.format(date);
            String displayTime = mFormatter.format(date);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String sTodayDate = sdf.format(new Date());
            String sSelectedDate = sdf.format(date);


            if (SselectedTime.equalsIgnoreCase("00")) {
                SselectedTime = "24";
            }

            if (sTodayDate.equalsIgnoreCase(sSelectedDate)) {
                if (Integer.parseInt(currentTime) <= Integer.parseInt(SselectedTime)) {

                    if (Integer.parseInt(SselectedTime) - Integer.parseInt(currentTime) == 0) {
                        Calendar c = Calendar.getInstance();
                        int CurrentMinute = c.get(Calendar.MINUTE);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date);
                        int SelectedMinutes = calendar.get(Calendar.MINUTE);

                        if (CurrentMinute <= SelectedMinutes) {


                            //Enable and Disable RideNow Button
                            if (selectedType.equalsIgnoreCase("1")) {
                                Tv_PickupNow.setTextColor(Color.parseColor("#FFFFFF"));
                                Rl_PickupNow.setClickable(true);
                            }

                            selectedDate = coupon_mFormatter.format(date);
                            selectedTime = coupon_time_mFormatter.format(date);


//                            //--------Disabling the map functionality---------
//                            googleMap.getUiSettings().setAllGesturesEnabled(false);
//                            googleMap_confirm.getUiSettings().setAllGesturesEnabled(false);


                            EstimateParam_method();

                        } else {
                            Alert(getResources().getString(R.string.alert_label_ridelater_title), getResources().getString(R.string.alert_label_ridelater_content));
                        }

                    } else {


                        //Enable and Disable RideNow Button
                        if (selectedType.equalsIgnoreCase("1")) {
                            Tv_PickupNow.setTextColor(Color.parseColor("#FFFFFF"));
                            Rl_PickupNow.setClickable(true);
                        }

                        selectedDate = coupon_mFormatter.format(date);
                        selectedTime = coupon_time_mFormatter.format(date);


//                        //--------Disabling the map functionality---------
//                        googleMap.getUiSettings().setAllGesturesEnabled(false);
//                        googleMap_confirm.getUiSettings().setAllGesturesEnabled(false);

                        EstimateParam_method();

                       /* Tv_PickupNow.setEnabled(false);
                        Tv_PickupAddress.setEnabled(false);
//                        favorite_layout.setEnabled(false);
                        Animation animFadeIn = AnimationUtils.loadAnimation(Fragment_HomePage.this, R.anim.fade_in);
                        Rl_ConfirmCancelLayout.startAnimation(animFadeIn);
                        Rl_PickUpLayout.setVisibility(View.GONE);
                        Rl_ConfirmCancelLayout.setVisibility(View.VISIBLE);
                        Rl_PickupNow.setBackgroundColor(getResources().getColor(R.color.app_color));
                        Tv_PickupNow.setText(getResources().getString(R.string.confirm_lable));
                        Rl_PickupLater.setBackgroundColor(getResources().getColor(R.color.app_color));
                        Tv_PickupLater.setText(getResources().getString(R.string.cancel_lable));*/


                    }

                } else {
                    Alert(getResources().getString(R.string.alert_label_ridelater_title), getResources().getString(R.string.alert_label_ridelater_content));
                }
            } else {


                //Enable and Disable RideNow Button
                if (selectedType.equalsIgnoreCase("1")) {
                    Tv_PickupNow.setTextColor(Color.parseColor("#FFFFFF"));
                    Rl_PickupNow.setClickable(true);
                }

                selectedDate = coupon_mFormatter.format(date);
                selectedTime = coupon_time_mFormatter.format(date);


//                //--------Disabling the map functionality---------
//                googleMap.getUiSettings().setAllGesturesEnabled(false);
//                googleMap_confirm.getUiSettings().setAllGesturesEnabled(false);

                EstimateParam_method();

               /* Tv_PickupAddress.setEnabled(false);
                Tv_PickupNow.setEnabled(false);
//                favorite_layout.setEnabled(false);
                Animation animFadeIn = AnimationUtils.loadAnimation(Fragment_HomePage.this, R.anim.fade_in);
                Rl_ConfirmCancelLayout.startAnimation(animFadeIn);
                Rl_PickUpLayout.setVisibility(View.GONE);
                Rl_ConfirmCancelLayout.setVisibility(View.VISIBLE);
                Rl_PickupNow.setBackgroundColor(getResources().getColor(R.color.app_color));
                Tv_PickupNow.setText(getResources().getString(R.string.confirm_lable));
                Rl_PickupLater.setBackgroundColor(getResources().getColor(R.color.app_color));
                Tv_PickupLater.setText(getResources().getString(R.string.cancel_lable));*/


            }

        }

        // Optional cancel listener
        @Override
        public void onDateTimeCancel() {
//            Toast.makeText(Fragment_HomePage.this, "Canceled", Toast.LENGTH_SHORT).show();
        }
    };


    //-------------------AsynTask To get the current Address----------------
    private void PostRequest(String Url, final double latitude, final double longitude) {
        Rl_PickupNow.setEnabled(false);
        Rl_PickupLater.setEnabled(false);
        isLoading = true;

        System.out.println("--------------Book My ride url-------------------" + Url);

        Sselected_latitude = String.valueOf(latitude);
        Sselected_longitude = String.valueOf(longitude);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);//"57c6a827cae2aa580c00002d");
        jsonParams.put("lat", String.valueOf(latitude));
        jsonParams.put("lon", String.valueOf(longitude));
        jsonParams.put("category", CategoryID);
        System.out.println("--------------Book My ride jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(Fragment_HomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Book My ride reponse-------------------" + response);
                String fail_response = "";
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.length() > 0) {
                        if (object.getString("status").equalsIgnoreCase("1")) {

                            JSONObject jobject = object.getJSONObject("response");
                            if (jobject.length() > 0) {

                                StrackingPercent = jobject.getString("tracking_percentage");
                                NoOfSeats = Integer.parseInt(jobject.getString("no_of_seats"));
                                passengers.clear();
                                for (int i = 1; i <= NoOfSeats; i++) {
                                    passengers.add(String.valueOf(i));
                                }

                                for (int i = 0; i < jobject.length(); i++) {

                                    Object check_driver_object = jobject.get("drivers");
                                    if (check_driver_object instanceof JSONArray) {

                                        JSONArray driver_array = jobject.getJSONArray("drivers");
                                        if (driver_array.length() > 0) {
                                            driver_list.clear();

                                            for (int j = 0; j < driver_array.length(); j++) {
                                                JSONObject driver_object = driver_array.getJSONObject(j);

                                                HomePojo pojo = new HomePojo();
                                                pojo.setDriver_lat(driver_object.getString("lat"));
                                                pojo.setDriver_long(driver_object.getString("lon"));

                                                driver_list.add(pojo);
                                            }
                                            driver_status = true;
                                        } else {
                                            driver_list.clear();
                                            driver_status = false;
                                        }
                                    } else {
                                        driver_status = false;
                                    }


                                    Object check_category_object = jobject.get("category");
                                    if (check_category_object instanceof JSONArray) {

                                        JSONArray cat_array = jobject.getJSONArray("category");
                                        if (cat_array.length() > 0) {
                                            category_list.clear();

                                            for (int k = 0; k < cat_array.length(); k++) {

                                                JSONObject cat_object = cat_array.getJSONObject(k);

                                                HomePojo pojo = new HomePojo();
                                                pojo.setCat_name(cat_object.getString("name"));
                                                pojo.setCat_time(cat_object.getString("eta"));
                                                pojo.setCat_id(cat_object.getString("id"));
                                                pojo.setIcon_normal(cat_object.getString("icon_normal"));
                                                pojo.setIcon_active(cat_object.getString("icon_active"));
                                                pojo.setSelected_Cat(jobject.getString("selected_category"));

                                                if (cat_object.getString("id").equals(jobject.getString("selected_category"))) {

                                                    CarAvailable = cat_object.getString("eta");
                                                    ScarType = cat_object.getString("name");
                                                    SListPosition = k;
                                                }

                                                category_list.add(pojo);
                                            }

                                            category_status = true;
                                        } else {
                                            category_list.clear();
                                            category_status = false;
                                        }
                                    } else {
                                        category_status = false;
                                    }
                                }
                            }

                            main_response_status = true;
                        } else {
                            fail_response = object.getString("response");
                            main_response_status = false;
                        }

                    }
                    dialogDismiss();

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogDismiss();
                }
                if (main_response_status) {
                    dialogDismiss();
                    Rl_Alert_layout.setVisibility(View.GONE);
                    Rl_Bottom_layout.setVisibility(View.VISIBLE);

                   /* // Creating adapter for spinner
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Fragment_HomePage.this, android.R.layout.simple_spinner_item, passengers);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    Passenger_Spinner.setAdapter(dataAdapter);
*/

                    initCustomSpinner();
                    Tv_CarType.setText(ScarType);
                    Tv_CarArriveTime.setText(CarAvailable);

                    if (driver_status) {
                        for (int i = 0; i < driver_list.size(); i++) {
                            double Dlatitude = Double.parseDouble(driver_list.get(i).getDriver_lat());
                            double Dlongitude = Double.parseDouble(driver_list.get(i).getDriver_long());

                            // create marker double Dlatitude = gps.getLatitude();
                            MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.car_map_icon));

                            // adding marker
                            googleMap.addMarker(marker);
                        }
                    } else {
                        googleMap.clear();
                    }

                    if (category_status) {

                        //Enable and Disable RideNow Button
                        if (!driver_status) {
                            Tv_PickupNow.setTextColor(Color.parseColor("#848484"));
                            Rl_PickupNow.setClickable(false);

                        } else {

                            Tv_PickupNow.setTextColor(Color.parseColor("#FFFFFF"));
                            Rl_PickupNow.setClickable(true);
                        }

                        Hlv_carTypes.setVisibility(View.VISIBLE);
                        adapter = new BookMyRide_Adapter(Fragment_HomePage.this, category_list);
                        Hlv_carTypes.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                    } else {
                        Hlv_carTypes.setVisibility(View.GONE);
                    }

                } else {
                    dialogDismiss();
                    Rl_Alert_layout.setVisibility(View.VISIBLE);
                    Rl_Bottom_layout.setVisibility(View.GONE);
                    TV_Alert_textview.setText(fail_response);
                }

                String address = getCompleteAddressString(latitude, longitude);
                Tv_PickupAddress.setText(address);
                SselectedAddress = address;

                Rl_PickupNow.setEnabled(true);
                Rl_PickupLater.setEnabled(true);

                isLoading = false;
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
                Rl_PickupNow.setEnabled(true);
                Rl_PickupLater.setEnabled(true);
                Rl_Alert_layout.setVisibility(View.VISIBLE);
                Rl_Bottom_layout.setVisibility(View.GONE);
                isLoading = false;

            }
        });
    }

//--------------------------custom spinner---------------------------------

    private void initCustomSpinner() {
        Passenger_Spinner = (Spinner) findViewById(R.id.book_my_ride_passenger_spinner);

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(Fragment_HomePage.this, passengers);
        Passenger_Spinner.setAdapter(customSpinnerAdapter);
        Passenger_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<String> asr;

        public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
            this.asr = asr;
            activity = context;
        }


        public int getCount() {
            return asr.size();
        }

        public Object getItem(int i) {
            return asr.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }


        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(Fragment_HomePage.this);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(20);
            txt.setGravity(Gravity.CENTER);
            txt.setText(asr.get(position));
            txt.setBackground(getResources().getDrawable(R.drawable.spinner_drop_down_background));
            txt.setTextColor(Color.parseColor("#000000"));
            return txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(Fragment_HomePage.this);
            txt.setGravity(Gravity.CENTER);
            txt.setPadding(10, 0, 0, 0);
            txt.setTextSize(12);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt.setText(asr.get(i));
            txt.setTextColor(Color.parseColor("#ffffff"));
            return txt;
        }

    }


    //----------------------Code for TextWatcher-------------------------
    private final TextWatcher EditorWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };


    //-----------Check Google Play Service--------
    private boolean CheckPlayService() {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(Fragment_HomePage.this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        }
        return true;
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        Fragment_HomePage.this.runOnUiThread(new Runnable() {
            public void run() {
                final Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionStatusCode, Fragment_HomePage.this, REQUEST_CODE_RECOVER_PLAY_SERVICES);
                if (dialog == null) {
                    Toast.makeText(Fragment_HomePage.this,  getResources().getString(R.string.incompitable), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    //-------------Method to get Complete Address------------
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                Log.e("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Current loction address", "Cannot get Address!");
        }
        return strAdd;
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Fragment_HomePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.ok_lable), new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(Fragment_HomePage.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(Fragment_HomePage.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((requestCode == placeSearch_request_code && resultCode == Activity.RESULT_OK && data != null)) {

            if (search_status == 0) {
                String SselectedLatitude = data.getStringExtra("Selected_Latitude");
                String SselectedLongitude = data.getStringExtra("Selected_Longitude");
                String SselectedLocation = data.getStringExtra("Selected_Location");
                if (!SselectedLatitude.equalsIgnoreCase("") && SselectedLatitude.length() > 0 && !SselectedLongitude.equalsIgnoreCase("") && SselectedLongitude.length() > 0) {
                    // Move the camera to last position with a zoom level
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(SselectedLatitude), Double.parseDouble(SselectedLongitude))).zoom(17).build();
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
                Tv_PickupAddress.setText(SselectedLocation);

                if (!getResources().getString(R.string.type_your_destination_lable).equalsIgnoreCase(Tv_DestinationAddress.getText().toString()) && Tv_DestinationAddress.getText().toString().length() > 0) {
                    Recent_lat = Double.parseDouble(SselectedLatitude);
                    Recent_long = Double.parseDouble(SselectedLongitude);
                    route();
                }

            } else {
                SdestinationLatitude = data.getStringExtra("Selected_Latitude");
                SdestinationLongitude = data.getStringExtra("Selected_Longitude");
                SdestinationLocation = data.getStringExtra("Selected_Location");

                System.out.println("-----------SdestinationLatitude----------------" + SdestinationLatitude);
                System.out.println("-----------SdestinationLongitude----------------" + SdestinationLongitude);
                System.out.println("-----------SdestinationLocation----------------" + SdestinationLocation);

                Tv_DestinationAddress.setText(SdestinationLocation);
                route();
            }
        } else if ((requestCode == multiplePlaceSearch_request_code && resultCode == Activity.RESULT_OK && data != null)) {

            String sMultipleDrop = data.getStringExtra("MultipleDropName");

            multiple_dropList.clear();
            try {
                Bundle bundleObject = data.getExtras();
                multiple_dropList = (ArrayList<MultiDropPojo>) bundleObject.getSerializable("MultipleDropLocation_List");
                System.out.println("-----------multiple_dropList size-----------" + multiple_dropList.size());
                sMultipleDropStatus = "1";
                STrip_Type = getResources().getString(R.string.multiplestop_lable);
                Tv_TripType.setText(getResources().getString(R.string.multiplestop_textview));
                Iv_OneWay.setBackground(getResources().getDrawable(R.drawable.oneway_unselect));
                Iv_Return.setBackground(getResources().getDrawable(R.drawable.twoway_unselect));
                Iv_MultipleStop.setBackground(getResources().getDrawable(R.drawable.multiplestop_select));
                route();
//                refreshLocation();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == favoriteList_request_code && resultCode == Activity.RESULT_OK && data != null) {

            String SselectedLatitude = data.getStringExtra("Selected_Latitude");
            String SselectedLongitude = data.getStringExtra("Selected_Longitude");
            if (!SselectedLatitude.equalsIgnoreCase("") && SselectedLatitude.length() > 0 && !SselectedLongitude.equalsIgnoreCase("") && SselectedLongitude.length() > 0) {
                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(SselectedLatitude), Double.parseDouble(SselectedLongitude))).zoom(17).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        } else if ((requestCode == coupon_request_code && resultCode == Activity.RESULT_OK && data != null)) {

            ScouponSource = data.getStringExtra("couponSource");
            ScouponCode = data.getStringExtra("couponCode");
            session.setCouponCode(ScouponCode, ScouponSource);

            if (ScouponCode.length() > 0 || ScouponCode != null) {
                Tv_couponCode.setText(ScouponCode);
            } else {
                Tv_couponCode.setText(getResources().getString(R.string.have_coupon_lable));
            }


        } else if (requestCode == timer_request_code && resultCode == Activity.RESULT_OK && data != null) {
            String ride_accepted = data.getStringExtra("Accepted_or_Not");
            String retry_count = data.getStringExtra("Retry_Count");

            if (retry_count.equalsIgnoreCase("1") && ride_accepted.equalsIgnoreCase("not")) {
                final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.retry_layout);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

                RelativeLayout yes_btn = (RelativeLayout) dialog.findViewById(R.id.cancel_trip_trackme_layout);
                RelativeLayout exit_yes = (RelativeLayout) dialog.findViewById(R.id.retry_trip_exit_layout);
                RelativeLayout test = (RelativeLayout) dialog.findViewById(R.id.trackdriver_car_layout);
                ImageView trackdriver_carphoto = (ImageView) dialog.findViewById(R.id.trackdriver_carphoto);

                HashMap<String, String> info = session.getCarCatImage();
                carCatImage = info.get(SessionManager.KEY_CAR_CAT_IAMGE);
                System.out.println("======carCatImage======" + carCatImage);
                if (!carCatImage.equalsIgnoreCase("")) {
                    Picasso.with(getApplicationContext()).load(String.valueOf(carCatImage)).into(trackdriver_carphoto);
                }
              /*  CustomTextView unsuccess_text=(CustomTextView)dialog.findViewById(R.id.unsuccess_text);
                unsuccess_text.setText(getResources().getString(R.string.timer_sorry)+" Mr "+(UserName.toUpperCase()));*/

                test.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();

                    }
                });

                yes_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        dialog.dismiss();

                        cd = new ConnectionDetector(Fragment_HomePage.this);
                        isInternetPresent = cd.isConnectingToInternet();

                        if (isInternetPresent) {
                            HashMap<String, String> code = session.getCouponCode();
                            String coupon = code.get(SessionManager.KEY_COUPON_CODE);

                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("user_id", UserID);
                            jsonParams.put("pickup", Tv_PickupAddress.getText().toString());
                            jsonParams.put("pickup_lat", String.valueOf(Recent_lat));
                            jsonParams.put("pickup_lon", String.valueOf(Recent_long));
                            jsonParams.put("drop_loc", Tv_DestinationAddress.getText().toString());
                            jsonParams.put("drop_lat", SdestinationLatitude);
                            jsonParams.put("drop_lon", SdestinationLongitude);
                            jsonParams.put("category", CategoryID);
                            jsonParams.put("type", selectedType);
                            jsonParams.put("pickup_date", selectedDate);
                            jsonParams.put("pickup_time", selectedTime);
                            jsonParams.put("source", ScouponSource);//coupon/redeem
                            jsonParams.put("code", ScouponCode);
                            jsonParams.put("tracking", Stracking);//tracking:1    => 0/1
                            jsonParams.put("mode", STrip_Type);//: =>oneway/return/multistop
                            jsonParams.put("no_of_passenger", Passenger_Spinner.getSelectedItem().toString());
                            jsonParams.put("referal_code", Str_referralCode);
                            jsonParams.put("est_cost", SetaAmount);
                            jsonParams.put("approx_dist", SetaDistance);

                            if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(STrip_Type)) {

                                for (int i = 0; i < multiple_dropList.size(); i++) {
                                    jsonParams.put("multipath_loc[" + i + "][drop_loc]", multiple_dropList.get(i).getPlaceName());
                                    jsonParams.put("multipath_loc[" + i + "][drop_lon]", multiple_dropList.get(i).getPlaceLong());
                                    jsonParams.put("multipath_loc[" + i + "][drop_lat]", multiple_dropList.get(i).getPlaceLat());
                                }
                            }
//                        riderId = "";
                            ConfirmRideRequest(Iconstant.confirm_ride_url, jsonParams, "2");

//                                ConfirmRideRequest(Iconstant.confirm_ride_url, coupon, coupon_selectedDate, coupon_selectedTime, selectedType, CategoryID, map_address.getText().toString(), SpickupLatitude, SpickupLongitude, "2", destination_address.getText().toString(), SdestinationLatitude, SdestinationLongitude);
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                        }


                    }
                });


                exit_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();

                        cd = new ConnectionDetector(Fragment_HomePage.this);
                        isInternetPresent = cd.isConnectingToInternet();

                        if (isInternetPresent) {
                            DeleteRideRequest(Iconstant.delete_ride_url);
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                        }


                    }
                });

                dialog.show();

                /*final PkDialog mDialog = new PkDialog(Fragment_HomePage.this);
                mDialog.setDialogTitle(getResources().getString(R.string.timer_label_alert_sorry));
                mDialog.setDialogMessage(getResources().getString(R.string.timer_label_alert_content));
                mDialog.setPositiveButton(getResources().getString(R.string.timer_label_alert_retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();

                        cd = new ConnectionDetector(Fragment_HomePage.this);
                        isInternetPresent = cd.isConnectingToInternet();

                        if (isInternetPresent) {
                            HashMap<String, String> code = session.getCouponCode();
                            String coupon = code.get(SessionManager.KEY_COUPON_CODE);

                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("user_id", UserID);
                            jsonParams.put("pickup", Tv_PickupAddress.getText().toString());
                            jsonParams.put("pickup_lat", String.valueOf(Recent_lat));
                            jsonParams.put("pickup_lon", String.valueOf(Recent_long));
                            jsonParams.put("drop_loc", Tv_DestinationAddress.getText().toString());
                            jsonParams.put("drop_lat", SdestinationLatitude);
                            jsonParams.put("drop_lon", SdestinationLongitude);
                            jsonParams.put("category", CategoryID);
                            jsonParams.put("type", selectedType);
                            jsonParams.put("pickup_date", selectedDate);
                            jsonParams.put("pickup_time", selectedTime);
                            jsonParams.put("source", ScouponSource);//coupon/redeem
                            jsonParams.put("code", ScouponCode);
                            jsonParams.put("tracking", Stracking);//tracking:1    => 0/1
                            jsonParams.put("mode", STrip_Type);//: =>oneway/return/multistop
                            jsonParams.put("no_of_passenger", Passenger_Spinner.getSelectedItem().toString());
                            jsonParams.put("referal_code", Str_referralCode);
                            jsonParams.put("est_cost", SetaAmount);
                            jsonParams.put("approx_dist", SetaDistance);

                            if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(STrip_Type)) {

                                for (int i = 0; i < multiple_dropList.size(); i++) {
                                    jsonParams.put("multipath_loc[" + i + "][drop_loc]", multiple_dropList.get(i).getPlaceName());
                                    jsonParams.put("multipath_loc[" + i + "][drop_lon]", multiple_dropList.get(i).getPlaceLong());
                                    jsonParams.put("multipath_loc[" + i + "][drop_lat]", multiple_dropList.get(i).getPlaceLat());
                                }
                            }
//                        riderId = "";
                            ConfirmRideRequest(Iconstant.confirm_ride_url, jsonParams, "2");

//                                ConfirmRideRequest(Iconstant.confirm_ride_url, coupon, coupon_selectedDate, coupon_selectedTime, selectedType, CategoryID, map_address.getText().toString(), String.valueOf(Recent_lat), String.valueOf(Recent_long), "2", destination_address.getText().toString(), SdestinationLatitude, SdestinationLongitude);
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                        }
                    }
                });
                mDialog.setNegativeButton(getResources().getString(R.string.timer_label_alert_cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();

                        cd = new ConnectionDetector(Fragment_HomePage.this);
                        isInternetPresent = cd.isConnectingToInternet();

                        if (isInternetPresent) {
                            DeleteRideRequest(Iconstant.delete_ride_url);
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                        }
                    }
                });
                mDialog.show();*/

            } else if (retry_count.equalsIgnoreCase("2") && ride_accepted.equalsIgnoreCase("not")) {
                DeleteRideRequest(Iconstant.delete_ride_url);
            } else if ((retry_count.equalsIgnoreCase("1") || retry_count.equalsIgnoreCase("2")) && ride_accepted.equalsIgnoreCase("Cancelled")) {

                riderId = "";

                //---------Hiding the bottom layout after cancel request--------
                googleMap.getUiSettings().setAllGesturesEnabled(true);

                Animation animFadeOut = AnimationUtils.loadAnimation(Fragment_HomePage.this, R.anim.fade_out);
                Rl_ConfirmCancelLayout.startAnimation(animFadeOut);
                Rl_ConfirmCancelLayout.setVisibility(View.GONE);

                Rl_PickUpLayout.setVisibility(View.VISIBLE);
                Hlv_carTypes.setVisibility(View.VISIBLE);
                Rl_PickupNow.setBackgroundColor(getResources().getColor(R.color.app_secondary_color));
                Tv_PickupNow.setText(getResources().getString(R.string.pickup_now_lable));
                Rl_PickupLater.setBackgroundColor(getResources().getColor(R.color.app_secondary_color));
                Tv_PickupLater.setText(getResources().getString(R.string.pickup_later_lable));
                Tv_PickupAddress.setEnabled(true);
                currentLocation_image.setClickable(true);
                currentLocation_image.setVisibility(View.VISIBLE);
                Iv_Favourite.setVisibility(View.VISIBLE);
//                    favorite_layout.setEnabled(true);
            }

        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    private void route() {
        double lat_decimal = Double.parseDouble(SdestinationLatitude);
        double lng_decimal = Double.parseDouble(SdestinationLongitude);
        double pick_lat_decimal = Recent_lat;
        double pick_lng_decimal = Recent_long;
        updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal);
    }

    private void updateGoogleMapTrackRide(double lat_decimal, double lng_decimal, double pick_lat_decimal, double pick_lng_decimal) {
        if (googleMap_confirm != null) {
            googleMap_confirm.clear();
        }
        LatLng dropLatLng = new LatLng(lat_decimal, lng_decimal);
        LatLng pickUpLatLng = new LatLng(pick_lat_decimal, pick_lng_decimal);

        GetNewRouteTask draw_route_asyncTask = new GetNewRouteTask();
        draw_route_asyncTask.setToAndFromLocation(pickUpLatLng, dropLatLng, "BeginTrip");
        draw_route_asyncTask.execute();

    }

    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    private class GetNewRouteTask extends AsyncTask<String, Void, String> {

        String response = "";
        private LatLng currentLocation;
        private LatLng endLocation;

        private ArrayList<LatLng> wayLatLng;

        public void setToAndFromLocation(LatLng currentLocation, LatLng endLocation, String status) {
            this.currentLocation = currentLocation;
            this.endLocation = endLocation;
            startWayPoint = currentLocation;
            endWayPoint = endLocation;
            sWayPointStatus = status;
            this.wayLatLng = addWayPointPoint(currentLocation, endLocation, multiple_dropList); //multipleDropLatLng);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... urls) {
            //Get All Route values

            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(Fragment_HomePage.this)
                    .alternativeRoutes(true)
                    .waypoints(wayLatLng)
                    .build();
            routing.execute();

            response = "Success";
            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("Success")) {
            }
        }
    }


    private ArrayList<LatLng> addWayPointPoint(LatLng start, LatLng end, ArrayList<MultiDropPojo> mMultipleDropLatLng)//  String[] mMultipleDropLatLng) {
    {
        googleMap_confirm.clear();
        wayPointList.clear();

        wayPointBuilder = new LatLngBounds.Builder();

        if (getResources().getString(R.string.return_lable).equals(STrip_Type)) {
            wayPointBuilder.include(start);
            wayPointBuilder.include(end);
            wayPointList.add(start);
            wayPointList.add(end);
            wayPointList.add(start);

        } else {
            wayPointBuilder.include(start);
            wayPointList.add(start);

            if (sMultipleDropStatus.equalsIgnoreCase("1")) {
                for (int i = 0; i < mMultipleDropLatLng.size(); i++) {

//                String sLatLng = mMultipleDropLatLng[i];
//                String[] latLng = sLatLng.split(",");

                    double Dlatitude = Double.parseDouble(mMultipleDropLatLng.get(i).getPlaceLat());
                    double Dlongitude = Double.parseDouble(mMultipleDropLatLng.get(i).getPlaceLong());

                    wayPointList.add(new LatLng(Dlatitude, Dlongitude));

                    // create marker double Dlatitude = gps.getLatitude();
                    MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.multiple_drop_pin));

                    wayPointBuilder.include(new LatLng(Dlatitude, Dlongitude));
                    googleMap_confirm.addMarker(marker);
                }
            }

            wayPointBuilder.include(end);

            wayPointList.add(end);
        }


        return wayPointList;
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        if (e != null) {
            // Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            // Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int i) {

        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        System.out.println("=========" + route.get(0).getDistanceText());
        System.out.println("=========" + route.get(0));

        polylines = new ArrayList<>();
        //add route(s) to the map.
//        for (int j = 0; i < route.size(); i++) {
        //In case of more than 5 alternative routes
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(R.color.app_secondary_color));
        polyOptions.width(10 + i * 3);
        polyOptions.addAll(route.get(0).getPoints());
        Polyline polyline = googleMap_confirm.addPolyline(polyOptions);
        polylines.add(polyline);
//        }
        googleMap_confirm.addMarker(new MarkerOptions()
                .position(startWayPoint)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.green_dot_circle)));
        googleMap_confirm.addMarker(new MarkerOptions()
                .position(endWayPoint)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.red_dot_circle)));

       /* if (wayPointBuilder != null) {
            LatLngBounds bounds = wayPointBuilder.build();
            int padding = 30; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap_confirm.moveCamera(cu);
        }*/

    }

    @Override
    public void onRoutingCancelled() {

    }

    //--------------------Estimate method----------------------------


    private void PostRequest_estimate(String Url, final HashMap<String, String> jsonParams) {
        dialog = new Dialog(Fragment_HomePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("-----------Estimate jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(Fragment_HomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Estimate reponse-------------------" + response);

                String Sstatus = "", Smessage = "", SuserName = "", Semail = "", Sgender = "", Sage = "", SstreetName = "", Sdistrict = "",
                        Sstate = "", Spincode = "", Slocality = "", sCountryCode = "", SmobNo = "", suserimage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject jsonObject = object.getJSONObject("response");
                        ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(jsonObject.getString("currency"));
                        JSONObject eta_jsonObject = jsonObject.getJSONObject("eta");
                        if (eta_jsonObject.length() > 0) {
                            SetaAmount = eta_jsonObject.getString("amount");
                            SetaDistance = eta_jsonObject.getString("distance");
                        }


                        dialogDismiss();

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {


                    if (Str_referralCode == null || Str_referralCode.length() <= 0) {
                        Tv_reffrealCode.setText(getResources().getString(R.string.have_referral_lable));
                    } else {
                        Tv_reffrealCode.setText(Str_referralCode);
                        ;
                    }
                    if (ScouponCode == null || ScouponCode.length() <= 0) {
                        Tv_couponCode.setText(getResources().getString(R.string.have_coupon_lable));
                    } else {
                        Tv_couponCode.setText(ScouponCode);
                    }

                    Tv_FareAmount.setText(ScurrencySymbol + " " + SetaAmount);
                    Tv_Distance.setText(SetaDistance);

                    Tv_PickupNow.setEnabled(false);
                    Tv_PickupAddress.setEnabled(false);
//                            favorite_layout.setEnabled(false);
                    Animation animFadeIn = AnimationUtils.loadAnimation(Fragment_HomePage.this, R.anim.fade_in);
                    Rl_ConfirmCancelLayout.startAnimation(animFadeIn);
                    Rl_PickUpLayout.setVisibility(View.GONE);
                    Rl_ConfirmCancelLayout.setVisibility(View.VISIBLE);
                    Rl_PickupNow.setBackgroundColor(getResources().getColor(R.color.app_color));
                    Tv_PickupNow.setText(getResources().getString(R.string.confirm_lable));
                    Rl_PickupLater.setBackgroundColor(getResources().getColor(R.color.app_color));
                    Tv_PickupLater.setText(getResources().getString(R.string.cancel_lable));
                    currentLocation_image.setClickable(false);
                    currentLocation_image.setVisibility(View.GONE);
                    Iv_Favourite.setVisibility(View.INVISIBLE);


                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    //----------------Referral code Method----------------------------------------------------

    private void showCoupon() {
        refferal_dialog = new MaterialDialog(Fragment_HomePage.this);
        View view = LayoutInflater.from(Fragment_HomePage.this).inflate(R.layout.coupon_code_dialog, null);

        tv_apply = (TextView) view.findViewById(R.id.referralcode_apply_textView);
        tv_cancel = (TextView) view.findViewById(R.id.referralcode_cancel_textView);
        final TextView tv_nointernet = (TextView) view.findViewById(R.id.referralcode_nointernet_textView);
        referral_edittext = (EditText) view.findViewById(R.id.referralcode_editText);
        referral_apply_layout = (RelativeLayout) view.findViewById(R.id.referralcode_apply_layout);
        referral_loading_layout = (RelativeLayout) view.findViewById(R.id.referralcode_loading_layout);
        referral_allowance_layout = (RelativeLayout) view.findViewById(R.id.referralcode_allowance_amount_layout);
        referral_allowance = (TextView) view.findViewById(R.id.referralcode_allowance_textview);
        Iv_coupon_cancel_Icon = (ImageView) view.findViewById(R.id.referralcode_cancel_imageIcon);


        tv_cancel.setVisibility(View.INVISIBLE);

        HashMap<String, String> code = session.getReferralCode();
        String referral = code.get(SessionManager.KEY_REFERAL_CODE);
        String referral_msg = code.get(SessionManager.KEY_REFERAL_CODE_MSG);


        System.out.println("-----------coupon-------------" + referral);

        if (!referral.isEmpty() && referral.length() > 0) {
            referral_edittext.setText(referral);
            referral_apply_layout.setVisibility(View.GONE);
            tv_apply.setText(getResources().getString(R.string.referralcode_label_apply));
            referral_allowance_layout.setVisibility(View.VISIBLE);
            referral_allowance.setText(referral_msg);
        } else {
            referral_apply_layout.setVisibility(View.VISIBLE);
        }
        referral_loading_layout.setVisibility(View.GONE);
        referral_edittext.addTextChangedListener(EditorWatcher);


        referral_edittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) Fragment_HomePage.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(referral_edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return false;
            }
        });

       /* tv_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                refferal_dialog.dismiss();
                Fragment_HomePage.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });*/


        Iv_coupon_cancel_Icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                refferal_dialog.dismiss();
                Fragment_HomePage.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });

        tv_apply.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (referral_edittext.length() == 0) {
                    referral_edittext.setHint(getResources().getString(R.string.referralcode_label_invalid_code));
                    referral_edittext.setHintTextColor(Color.RED);
                    Animation shake = AnimationUtils.loadAnimation(Fragment_HomePage.this, R.anim.shake);
                    referral_edittext.startAnimation(shake);
                } else {
                    cd = new ConnectionDetector(Fragment_HomePage.this);
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        tv_nointernet.setVisibility(View.INVISIBLE);
                        Fragment_HomePage.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                        if (getResources().getString(R.string.referralcode_label_apply).equalsIgnoreCase(tv_apply.getText().toString())) {
                            ReferralCodeRequest(Iconstant.add_referral_code_url, referral_edittext.getText().toString());
                        } else {
                            session.setReferralCode("", "");
                            referral_edittext.setText("");
                            tv_apply.setText(getResources().getString(R.string.referralcode_label_apply));
                            referral_allowance_layout.setVisibility(View.GONE);
                            tv_cancel.setText(getResources().getString(R.string.referralcode_label_cancel));
                        }
                    } else {
                        tv_nointernet.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
        refferal_dialog.setView(view).show();
    }

    //-------------------Coupon Code Post Request----------------

    private void ReferralCodeRequest(String Url, final String code) {
        System.out.println("--------------referral code url-------------------" + Url);

        referral_apply_layout.setVisibility(View.GONE);
        referral_loading_layout.setVisibility(View.VISIBLE);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("code", code);
        System.out.println("--------------referral code jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(Fragment_HomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------referral code reponse-------------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String status = object.getString("status");
                        String Sresponse = object.getString("response");

                        if (status.equalsIgnoreCase("1")) {
                            String Smessage = object.getString("response");

                            referral_apply_layout.setVisibility(View.GONE);
                            referral_loading_layout.setVisibility(View.GONE);

                            Str_referralCode = code;
                            session.setReferralCode(code, Smessage);
                            Tv_reffrealCode.setText(Str_referralCode);

                            referral_allowance_layout.setVisibility(View.VISIBLE);
                            referral_allowance.setText(Smessage);

                            sCouponAllowanceText = Smessage;

                            tv_apply.setText(getResources().getString(R.string.referralcode_label_remove));
                            tv_cancel.setText(getResources().getString(R.string.action_ok));
//                            tv_coupon_label.setText(Str_couponCode);
//                            tv_coupon_label.setTextColor(getResources().getColor(R.color.darkgreen_color));

                        } else {

                            Str_referralCode = "";
                            session.setReferralCode(Str_referralCode, "");
                            Tv_reffrealCode.setText(getResources().getString(R.string.have_referral_lable));

                            referral_apply_layout.setVisibility(View.VISIBLE);
                            referral_loading_layout.setVisibility(View.GONE);
                            referral_allowance_layout.setVisibility(View.GONE);

                            referral_edittext.setText("");
                            referral_edittext.setHint(getResources().getString(R.string.referralcode_label_invalid_code));
                            referral_edittext.setHintTextColor(Color.RED);
                            Animation shake = AnimationUtils.loadAnimation(Fragment_HomePage.this, R.anim.shake);
                            referral_edittext.startAnimation(shake);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                referral_apply_layout.setVisibility(View.VISIBLE);
                referral_loading_layout.setVisibility(View.GONE);
                refferal_dialog.dismiss();
            }
        });
    }

    //-------------------Confirm Ride Post Request----------------

    private void ConfirmRideRequest(String Url, final HashMap<String, String> jsonParams, final String try_value) {
        dialog = new Dialog(Fragment_HomePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView loading = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        loading.setText(getResources().getString(R.string.action_pleasewait));

        System.out.println("--------------Confirm Ride url-------------------" + Url);

        System.out.println("--------------Confirm Ride jsonParams-------------------" + jsonParams);


        mRequest = new ServiceRequest(Fragment_HomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------Confirm Ride reponse-------------------" + response);

                String selected_type = "", Sacceptance = "";
                String Str_driver_id = "", Str_driver_name = "", Str_driver_email = "", Str_driver_review = "",
                        Str_driver_lat = "", Str_driver_lon = "", Str_min_pickup_duration = "", Str_ride_id = "", Str_phone_number = "",
                        Str_vehicle_number = "", Str_vehicle_model = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String status = object.getString("status");
                        if (status.equalsIgnoreCase("1")) {
                            JSONObject response_object = object.getJSONObject("response");

                            selected_type = response_object.getString("type");
                            Sacceptance = object.getString("acceptance");
                            if (Sacceptance.equalsIgnoreCase("No")) {
                                response_time = response_object.getString("response_time");
                            }
                            riderId = response_object.getString("ride_id");


                            if (Sacceptance.equalsIgnoreCase("Yes")) {
                                JSONObject driverObject = response_object.getJSONObject("driver_profile");

                                Str_driver_id = driverObject.getString("driver_id");
                                Str_driver_name = driverObject.getString("driver_name");
                                Str_driver_email = driverObject.getString("driver_email");
                                Str_driver_image = driverObject.getString("driver_image");
                                Str_driver_review = driverObject.getString("driver_review");
                                Str_driver_lat = driverObject.getString("driver_lat");
                                Str_driver_lon = driverObject.getString("driver_lon");
                                Str_min_pickup_duration = driverObject.getString("min_pickup_duration");
                                Str_ride_id = driverObject.getString("ride_id");
                                Str_phone_number = driverObject.getString("phone_number");
                                Str_vehicle_number = driverObject.getString("vehicle_number");
                                Str_vehicle_model = driverObject.getString("vehicle_model");
                            }

                            if (selected_type.equalsIgnoreCase("1")) {

                                dialogDismiss();

                                final PkDialog mDialog = new PkDialog(Fragment_HomePage.this);
                                mDialog.setDialogTitle(getResources().getString(R.string.action_success));
                                mDialog.setDialogMessage(getResources().getString(R.string.ridenow_label_confirm_success));
                                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();

                                        session.setCouponCode("", "");
                                        session.setReferralCode("", "");
                                        //Enable and Disable RideNow Button
                                        if (!driver_status) {
                                            Tv_PickupNow.setTextColor(Color.parseColor("#848484"));
                                            Rl_PickupNow.setClickable(false);
                                        } else {
                                            Tv_PickupNow.setTextColor(Color.parseColor("#FFFFFF"));
                                            Rl_PickupNow.setClickable(true);

                                        }
                                        //---------Hiding the bottom layout after success request--------
                                        googleMap.getUiSettings().setAllGesturesEnabled(true);
                                        googleMap_confirm.getUiSettings().setAllGesturesEnabled(true);

                                        Animation animFadeOut = AnimationUtils.loadAnimation(Fragment_HomePage.this, R.anim.fade_out);
                                        Rl_ConfirmCancelLayout.startAnimation(animFadeOut);
                                        Rl_ConfirmCancelLayout.setVisibility(View.GONE);

                                        Rl_PickUpLayout.setVisibility(View.VISIBLE);
                                        Rl_PickupNow.setBackgroundColor(getResources().getColor(R.color.app_color));
                                        Tv_PickupNow.setText(getResources().getString(R.string.pickup_now_lable));
                                        Rl_PickupLater.setBackgroundColor(getResources().getColor(R.color.app_color));
                                        Tv_PickupLater.setText(getResources().getString(R.string.pickup_later_lable));
                                        currentLocation_image.setClickable(true);
                                        currentLocation_image.setVisibility(View.VISIBLE);
                                        Iv_Favourite.setVisibility(View.VISIBLE);
                                        Tv_PickupAddress.setEnabled(true);
//                                        favorite_layout.setEnabled(true);
                                    }
                                });
                                mDialog.show();

                            } else if (selected_type.equalsIgnoreCase("0")) {
                                if (Sacceptance.equalsIgnoreCase("Yes")) {
                                    dialogDismiss();

                                    if (Sacceptance.equalsIgnoreCase("Yes")) {
                                        session.setCouponCode("", "");
                                        session.setReferralCode("", "");
                                    }

                                    //Move to ride Detail page
                                    Intent i = new Intent(Fragment_HomePage.this, TrackRideAcceptPage.class);
                                    i.putExtra("driverID", Str_driver_id);
                                    i.putExtra("driverName", Str_driver_name);
                                    i.putExtra("driverImage", Str_driver_image);
                                    i.putExtra("driverRating", Str_driver_review);
                                    i.putExtra("driverTime", Str_min_pickup_duration);
                                    i.putExtra("rideID", Str_ride_id);
                                    i.putExtra("driverMobile", Str_phone_number);
                                    i.putExtra("driverCar_no", Str_vehicle_number);
                                    i.putExtra("driverCar_model", Str_vehicle_model);
                                    startActivity(i);
                                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                    finish();

                                } else {
                                    dialogDismiss();
                                    Intent intent = new Intent(Fragment_HomePage.this, TimerPage.class);
                                    intent.putExtra("Time", response_time);
                                    intent.putExtra("retry_count", try_value);
                                    intent.putExtra("ride_ID", riderId);
                                    startActivityForResult(intent, timer_request_code);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            }
                        } else {
                            String Sresponse = object.getString("response");
                            Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialogDismiss();

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    //-------------------Delete Ride Post Request----------------

    private void DeleteRideRequest(String Url) {

        dialog = new Dialog(Fragment_HomePage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView loading = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        loading.setText(getResources().getString(R.string.action_pleasewait));

        System.out.println("--------------Delete Ride url-------------------" + Url);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("ride_id", riderId);

        mRequest = new ServiceRequest(Fragment_HomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Delete Ride reponse-------------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        String status = object.getString("status");
                        String response_value = object.getString("response");
                        if (status.equalsIgnoreCase("1")) {
                            riderId = "";
                            Alert(getResources().getString(R.string.action_success), response_value);
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), response_value);
                        }


                        //---------Hiding the bottom layout after cancel request--------
                        googleMap.getUiSettings().setAllGesturesEnabled(true);

                        Animation animFadeOut = AnimationUtils.loadAnimation(Fragment_HomePage.this, R.anim.fade_out);
                        Rl_ConfirmCancelLayout.startAnimation(animFadeOut);
                        Rl_ConfirmCancelLayout.setVisibility(View.GONE);
                        Rl_PickUpLayout.setVisibility(View.VISIBLE);

                        Hlv_carTypes.setVisibility(View.VISIBLE);
                        Rl_PickupNow.setBackgroundColor(getResources().getColor(R.color.app_secondary_color));
                        Tv_PickupNow.setText(getResources().getString(R.string.pickup_now_lable));
                        Rl_PickupLater.setBackgroundColor(getResources().getColor(R.color.app_secondary_color));
                        Tv_PickupLater.setText(getResources().getString(R.string.pickup_later_lable));
                        Tv_PickupAddress.setEnabled(true);
                        currentLocation_image.setClickable(true);
                        currentLocation_image.setVisibility(View.VISIBLE);
                        Iv_Favourite.setVisibility(View.VISIBLE);
//                        favorite_layout.setEnabled(true);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
        context.stopService(new Intent(context, Getlocation.class));

    }


}
