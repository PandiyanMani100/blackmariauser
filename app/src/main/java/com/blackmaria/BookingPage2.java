package com.blackmaria;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.hockeyapp.ActivityHockeyApp;
import com.blackmaria.newdesigns.Stopoversummary;
import com.blackmaria.pojo.Geteta;
import com.blackmaria.pojo.MultiDropPojo;
import com.blackmaria.utils.ConnectionDetector;
import com.blackmaria.utils.SessionManager;
import com.blackmaria.iconstant.Iconstant;
import com.blackmaria.volley.ServiceRequest;

import com.blackmaria.widgets.PkDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;


public class BookingPage2 extends ActivityHockeyApp {

    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;

    private TextView Tv_PickUp_Address;
    //    private TextView Tv_Drop1_Approx_Km, Tv_Drop2_Approx_Km, Tv_Drop3_Approx_Km, Tv_Drop4_Approx_Km;
    private TextView Tv_Drop1_Address, Tv_Drop2_Address, Tv_Drop3_Address, Tv_Drop4_Address;
    private TextView Drop1_Lable, Drop2_Lable, Drop3_Lable, Drop4_Lable;
    private RelativeLayout Rl_Drop1, Rl_Drop2, Rl_Drop3, Rl_Drop4;
    private ImageView Iv_Drop_Off1, Iv_Drop_Off2, Iv_Drop_Off3, Iv_Drop_Off4;
    private ImageView Iv_category;
    private TextView Tv_Total_KM, Tv_Total_Min, Tv_Total_Amount, Tv_Waiting_Charge;
    private TextView Tv_Vechile_Arrive_Time, viewall;
    private ImageView Iv_Back;
    private LinearLayout booking_Tv;
    ImageView pickup_plusimagview, drop_address_add_imageview, returm_trip_drop2_add_imageview, returm_trip_drop2_add_imageview2, returm_trip_drop2_add_imageview1;
   LanguageDb mhelper;
    private String UserID = "", SwaitingTime = "", StripMode = "", Stracking = "", SCategoryImage = "", SphantomCar = "";
    private String ScategoryID = "", SselectedType = "", SselectedDate = "", SselectedTime = "";
    private String SdestinationLatitude = "", SdestinationLongitude = "", SdestinationLocation = "";
    private String SpickupLatitude = "", SpickupLongitude = "", SpickupLocation = "";
    private ArrayList<MultiDropPojo> multiDropList;
    private Dialog dialog, savedialog;
    private ServiceRequest mRequest;
    private String StotalAmount = "", fareAmount1 = "", SetaTime = "", SuserName = "", StotalDistance = "", StotalTime = "", SapproxDist1 = "", SapproxDist2 = "",
            SapproxDist3 = "", SapproxDist4 = "", SwaitingCharge = "", ScurrencyCode = "", WaitingTime = "";
    String book_ref="",user_name,gender,mobile_number,country_code;
    public static Activity BookingPage2_class;
    private RefreshReceiver refreshReceiver;
    private EditText Et_Did_location_name;
//    private ImageView backToB3;

    String multidrop1 = "", multidrop2 = "", multidrop4 = "", multidrop3 = "", multidrop1Lat = "", multidrop1Lon = "", multidrop2Lat = "", multidrop2Lon = "",
            multidrop3Lat = "", multidrop4Lat = "", multidrop4Lon = "", multidrop3Lon = "";
    private TextView pickupLable1, pickupLable2, pickupLable3, pickupLable4;
    private Geteta geteta;

    //new design
    private TextView tv_bookingtype, tv_booking_waitingfee, tv_booking_confirm;

    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {

                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(BookingPage2.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(BookingPage2.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }

            } else if (intent.getAction().equals("com.pushnotification.confirmview")) {
                JSONObject object = new JSONObject();
                try {
                    object.put("pickupAddress", SpickupLocation);
                    object.put("pickupLatitude", SpickupLatitude);
                    object.put("pickupLongitude", SpickupLongitude);
                    object.put("destinationAddress", SdestinationLocation);
                    object.put("destinationLatitude", SdestinationLatitude);
                    object.put("destinationLongitude", SdestinationLongitude);
                    object.put("CategoryID", ScategoryID);
                    object.put("pickup_date", SselectedDate);
                    object.put("pickup_time", SselectedTime);
                    object.put("type", SselectedType);
                    object.put("tracking", Stracking);
                    object.put("mode", StripMode);
                    object.put("est_cost", ScurrencyCode + " " + StotalAmount);
                    object.put("est_cost_booking", fareAmount1);
                    object.put("approx_dist", StotalDistance);
                    object.put("eta", StotalTime);
                    object.put("waitingTime", WaitingTime);
                    object.put("page", "BookingPage2");
                    object.put("MultipleDropLocation_List", multiDropList.toString());
                    sessionManager.setCurrentBooking(object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intents = new Intent(BookingPage2.this, BookingPage3.class);
                intents.putExtra("pickupAddress", SpickupLocation);
                intents.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                intents.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));

                intents.putExtra("destinationAddress", SdestinationLocation);
                intents.putExtra("destinationLatitude", SdestinationLatitude);
                intents.putExtra("destinationLongitude", SdestinationLongitude);
                intents.putExtra("CategoryID", ScategoryID);

                intents.putExtra("pickup_date", SselectedDate);
                intents.putExtra("pickup_time", SselectedTime);

                intents.putExtra("type", SselectedType);
                intents.putExtra("tracking", Stracking);
                intents.putExtra("mode", StripMode);

                if (book_ref.equalsIgnoreCase("Yes")) {
                    //jsonParams.put("book_ref", bookReference);
                    intent.putExtra("book_ref", book_ref);
                    intent.putExtra("user_name", user_name);
                    intent.putExtra("gender", gender);

                    intent.putExtra("mobile_number", mobile_number);
                    intent.putExtra("country_code", country_code);

                     }

                intents.putExtra("est_cost", ScurrencyCode + " " + StotalAmount);
                intents.putExtra("est_cost_booking", fareAmount1);

                intents.putExtra("approx_dist", StotalDistance);
                intents.putExtra("eta", StotalTime);
                intents.putExtra("waitingTime", WaitingTime);
                intents.putExtra("page", "BookingPage2");
                if (StripMode.equalsIgnoreCase("multistop")) {
                    Bundle bundleObject = new Bundle();
                    bundleObject.putSerializable("MultipleDropLocation_List", multiDropList);
                    intents.putExtras(bundleObject);
                }
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intents.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intents);
//                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bookingtwo_layout);
        mhelper = new LanguageDb(this);
        initialize();
        BookingPage2_class = BookingPage2.this;

//        backToB3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);
//            }
//        });
        viewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                Type type = new TypeToken<Geteta>() {
                }.getType();
                String json = gson.toJson(geteta, type);

                Intent intent = new Intent(BookingPage2.this, Stopoversummary.class);
                intent.putExtra("homepage", json);
                startActivity(intent);
            }
        });

        pickup_plusimagview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveFavoriteLocationDialog(SpickupLocation, String.valueOf(SpickupLatitude), String.valueOf(SpickupLongitude));
            }
        });
        drop_address_add_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("multistop".equalsIgnoreCase(StripMode)) {
                    SaveFavoriteLocationDialog(multidrop1, String.valueOf(multidrop1Lat), String.valueOf(multidrop1Lon));

                } else {
                    SaveFavoriteLocationDialog(SdestinationLocation, String.valueOf(SdestinationLatitude), String.valueOf(SdestinationLongitude));
                }
            }
        });

        returm_trip_drop2_add_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveFavoriteLocationDialog(multidrop2, String.valueOf(multidrop2Lat), String.valueOf(multidrop2Lon));
            }
        });
        returm_trip_drop2_add_imageview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveFavoriteLocationDialog(multidrop3, String.valueOf(multidrop3Lat), String.valueOf(multidrop3Lon));
            }
        });
        returm_trip_drop2_add_imageview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveFavoriteLocationDialog(multidrop4, String.valueOf(multidrop4Lat), String.valueOf(multidrop4Lon));
            }
        });

        Iv_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent book1Intent = new Intent(BookingPage2.this, Navigation_new.class);
                startActivity(book1Intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        tv_booking_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject object = new JSONObject();
                try {
                    object.put("pickupAddress", SpickupLocation);
                    object.put("pickupLatitude", SpickupLatitude);
                    object.put("pickupLongitude", SpickupLongitude);
                    object.put("destinationAddress", SdestinationLocation);
                    object.put("destinationLatitude", SdestinationLatitude);
                    object.put("destinationLongitude", SdestinationLongitude);
                    object.put("CategoryID", ScategoryID);
                    object.put("pickup_date", SselectedDate);
                    object.put("pickup_time", SselectedTime);
                    object.put("type", SselectedType);
                    object.put("tracking", Stracking);
                    object.put("mode", StripMode);
                    object.put("est_cost", ScurrencyCode + " " + StotalAmount);
                    object.put("est_cost_booking", fareAmount1);
                    object.put("approx_dist", StotalDistance);
                    object.put("eta", StotalTime);
                    object.put("waitingTime", WaitingTime);
                    object.put("page", "BookingPage2");
                    object.put("MultipleDropLocation_List", multiDropList.toString());
                    sessionManager.setCurrentBooking(object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(BookingPage2.this, BookingPage4.class);
                intent.putExtra("pickupAddress", SpickupLocation);
                intent.putExtra("pickupLatitude", String.valueOf(SpickupLatitude));
                intent.putExtra("pickupLongitude", String.valueOf(SpickupLongitude));

                intent.putExtra("destinationAddress", SdestinationLocation);
                intent.putExtra("destinationLatitude", SdestinationLatitude);
                intent.putExtra("destinationLongitude", SdestinationLongitude);
                intent.putExtra("CategoryID", ScategoryID);

                intent.putExtra("pickup_date", SselectedDate);
                intent.putExtra("pickup_time", SselectedTime);

                intent.putExtra("type", SselectedType);
                intent.putExtra("tracking", Stracking);
                intent.putExtra("mode", StripMode);
if(book_ref != null)
{
      if (book_ref.equalsIgnoreCase("Yes")) {
                    //jsonParams.put("book_ref", bookReference);
                    intent.putExtra("book_ref", book_ref);
                    intent.putExtra("user_name", user_name);
                    intent.putExtra("gender", gender);

                    intent.putExtra("mobile_number", mobile_number);
                    intent.putExtra("country_code", country_code);

                        }
}



                intent.putExtra("est_cost", ScurrencyCode + " " + StotalAmount);
                intent.putExtra("est_cost_booking", fareAmount1);

                intent.putExtra("approx_dist", StotalDistance);
                intent.putExtra("eta", StotalTime);
                intent.putExtra("waitingTime", WaitingTime);
                intent.putExtra("page", "BookingPage2");
                if (StripMode.equalsIgnoreCase("multistop")) {
                    Bundle bundleObject = new Bundle();
                    bundleObject.putSerializable("MultipleDropLocation_List", multiDropList);
                    intent.putExtras(bundleObject);
                }
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
//                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);


            }
        });
    }

    private void initialize() {

        sessionManager = new SessionManager(BookingPage2.this);
        cd = new ConnectionDetector(BookingPage2.this);
        isInternetPresent = cd.isConnectingToInternet();
        multiDropList = new ArrayList<MultiDropPojo>();

        Tv_PickUp_Address = (TextView) findViewById(R.id.booking_pickup_address_textview);

        pickup_plusimagview = (ImageView) findViewById(R.id.pickup_plusimagview);
        drop_address_add_imageview = (ImageView) findViewById(R.id.drop_address_add_imageview);

        returm_trip_drop2_add_imageview = (ImageView) findViewById(R.id.returm_trip_drop2_add_imageview);
        returm_trip_drop2_add_imageview1 = (ImageView) findViewById(R.id.returm_trip_drop2_add_imageview1);
        returm_trip_drop2_add_imageview2 = (ImageView) findViewById(R.id.returm_trip_drop3_add_imageview1);
//        backToB3 = (ImageView) findViewById(R.id.backimage_b3);

        Rl_Drop1 = (RelativeLayout) findViewById(R.id.booking_drop_off_1_layout);
        Rl_Drop2 = (RelativeLayout) findViewById(R.id.booking_drop_off_2_layout);
        Rl_Drop3 = (RelativeLayout) findViewById(R.id.booking_drop_off_3_layout);

        Rl_Drop4 = (RelativeLayout) findViewById(R.id.booking_drop_off_4_layout);

        tv_bookingtype = findViewById(R.id.booking_triptype);
        tv_booking_confirm = findViewById(R.id.booking_confirm);
        tv_booking_confirm.setText(getkey("confirm_lable"));

        TextView trips = findViewById(R.id.trips);
        trips.setText(getkey("trip_summary_lable"));

        TextView tv_lablesdistance = findViewById(R.id.tv_lablesdistance);
        tv_lablesdistance.setText(getkey("distance_lable"));

        TextView tv_lablestime = findViewById(R.id.tv_lablestime);
        tv_lablestime.setText(getkey("time_lable"));

        TextView tv_lablestrip = findViewById(R.id.tv_lablestrip);
        tv_lablestrip.setText(getkey("trip_lable"));

        TextView tv_lableswaiting = findViewById(R.id.tv_lableswaiting);
        tv_lableswaiting.setText(getkey("waitingfee"));

        TextView tv_lablestotal = findViewById(R.id.tv_lablestotal);
        tv_lablestotal.setText(getkey("totoal_fare"));
//
//        Tv_Drop1_Approx_Km = (TextView) findViewById(R.id.booking_drop_off_1_approx_km_textview);
//        Tv_Drop2_Approx_Km = (TextView) findViewById(R.id.booking_drop_off_2_approx_km_textview);
//        Tv_Drop3_Approx_Km = (TextView) findViewById(R.id.booking_drop_off_3_approx_km_textview);
//
//        Tv_Drop4_Approx_Km = (TextView) findViewById(R.id.booking_drop_off_4_approx_km_textview);

        Tv_Drop1_Address = (TextView) findViewById(R.id.booking_drop_off_1_address_textview);
        Tv_Drop2_Address = (TextView) findViewById(R.id.booking_drop_off_2_address_textview);
        Tv_Drop3_Address = (TextView) findViewById(R.id.booking_drop_off_3_address_textview);

        Tv_Drop4_Address = (TextView) findViewById(R.id.booking_drop_off_4_address_textview);

        Drop1_Lable = (TextView) findViewById(R.id.booking_drop_off_1_address_lable);
        Drop2_Lable = (TextView) findViewById(R.id.booking_drop_off_2_address_lable);
        Drop3_Lable = (TextView) findViewById(R.id.booking_drop_off_3_address_lable);

        Drop4_Lable = (TextView) findViewById(R.id.booking_drop_off_4_address_lable);

//        Iv_Drop_Off1 = (ImageView) findViewById(R.id.drop_off_imagview1);
        Iv_Drop_Off2 = (ImageView) findViewById(R.id.iv_booking_drop_off_2);
        Iv_Drop_Off3 = (ImageView) findViewById(R.id.iv_booking_drop_off_3);
        Iv_Drop_Off4 = (ImageView) findViewById(R.id.iv_booking_drop_off_4);

        Iv_category = (ImageView) findViewById(R.id.selected_catagory_imageview);

        Tv_Total_KM = (TextView) findViewById(R.id.booking_total_km_textview);
        Tv_Total_Min = (TextView) findViewById(R.id.booking_total_minitues_textview);
        Tv_Total_Amount = (TextView) findViewById(R.id.booking_total_amount_textview);
        Tv_Waiting_Charge = (TextView) findViewById(R.id.booking_waiting_charge_textview);

        Tv_Vechile_Arrive_Time = (TextView) findViewById(R.id.booking_nearest_vechile_arrival_time_textview);
        viewall = (TextView) findViewById(R.id.viewall);
//        Tv_UserName = (TextView) findViewById(R.id.booking_username_textview);
        booking_Tv = (LinearLayout) findViewById(R.id.booking_ok_textview);

        pickupLable1 = (TextView) findViewById(R.id.pickup_lable);

        pickupLable1.setText(getkey("pikcup"));
        Iv_Back = (ImageView) findViewById(R.id.booking_back_imgeview);
// -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        intentFilter.addAction("com.app.pushnotification.RideAccept");
        intentFilter.addAction("com.pushnotification.confirmview");
        registerReceiver(refreshReceiver, intentFilter);
        getDataDetails();

    }

    private void setDataDetails() {

        Tv_Total_KM.setText(StotalDistance.toString());
        if (StotalTime.equalsIgnoreCase("")) {
            Tv_Total_Min.setText(SetaTime.toString());
        } else {
            Tv_Total_Min.setText(StotalTime.toString());
        }

        Tv_Total_Amount.setText(ScurrencyCode + " " + StotalAmount.toString());
//        Tv_Waiting_Charge.setText("ADD ON WAITING CHARGES :" + ScurrencyCode + " " + SwaitingCharge.toString());
        Tv_Waiting_Charge.setText(ScurrencyCode + " " + SwaitingCharge.toString());
        tv_bookingtype.setText(StripMode);
        Picasso.with(BookingPage2.this).load(String.valueOf(SCategoryImage)).placeholder(R.drawable.green_car).into(Iv_category);

        if (SphantomCar.equalsIgnoreCase("No")) {
            Tv_Vechile_Arrive_Time.setVisibility(View.VISIBLE);
            if (!SetaTime.equalsIgnoreCase("no cabs")) {
                Tv_Vechile_Arrive_Time.setText(getkey("nearst_vechile")+" "+ SetaTime +" "+ getkey("away"));
            } else {
                Tv_Vechile_Arrive_Time.setText(getkey("no_nears_vchile"));
            }
        } else {
            Tv_Vechile_Arrive_Time.setVisibility(View.VISIBLE);
            if (!SetaTime.equalsIgnoreCase("no cabs")) {
                Tv_Vechile_Arrive_Time.setText(getkey("nearst_vechile") +" "+ SetaTime +" "+ getkey("away"));
            } else {
                Tv_Vechile_Arrive_Time.setText(getkey("no_nears_vchile"));
            }
        }
//        Tv_UserName.setText("MR." + SuserName.toString());
        Tv_PickUp_Address.setText(SpickupLocation.toString());

        if ("oneway".equalsIgnoreCase(StripMode) || "return".equalsIgnoreCase(StripMode)) {
            pickupLable1.setText(getkey("pikcup"));
            Drop1_Lable.setText(getkey("dropoff"));
            Tv_Drop1_Address.setText(SdestinationLocation.toString());
//            Tv_Drop1_Approx_Km.setText(SapproxDist1.toString());
            Tv_Waiting_Charge.setVisibility(View.VISIBLE);
            viewall.setVisibility(View.GONE);
        }

        if (!"oneway".equalsIgnoreCase(StripMode) && "return".equalsIgnoreCase(StripMode)) {
            Rl_Drop2.setVisibility(View.VISIBLE);
            Tv_Waiting_Charge.setVisibility(View.VISIBLE);
//            Tv_Drop2_Approx_Km.setVisibility(View.VISIBLE);
            Iv_Drop_Off2.setVisibility(View.VISIBLE);

            Rl_Drop4.setVisibility(View.GONE);
//            Tv_Drop4_Approx_Km.setVisibility(View.GONE);
            Iv_Drop_Off4.setVisibility(View.GONE);

            Rl_Drop3.setVisibility(View.GONE);
//            Tv_Drop3_Approx_Km.setVisibility(View.GONE);
            Iv_Drop_Off3.setVisibility(View.GONE);

            pickupLable1.setTextColor(getResources().getColor(R.color.returnpickup));
            pickupLable1.setText(getkey("pikcup"));
            Drop2_Lable.setText(getkey("pikcup"));
            Tv_Drop2_Address.setText(SpickupLocation.toString());
            viewall.setVisibility(View.GONE);
//            Tv_Drop2_Approx_Km.setText(SapproxDist2.toString());
        }
        if (StripMode.equalsIgnoreCase("multistop")) {
            Tv_Waiting_Charge.setVisibility(View.VISIBLE);
            if (multiDropList.size() == 3) {
                Rl_Drop2.setVisibility(View.VISIBLE);
//                Tv_Drop2_Approx_Km.setVisibility(View.VISIBLE);
                Iv_Drop_Off2.setVisibility(View.VISIBLE);
                Rl_Drop3.setVisibility(View.GONE);
//                Tv_Drop3_Approx_Km.setVisibility(View.VISIBLE);
                Iv_Drop_Off3.setVisibility(View.GONE);
                Rl_Drop4.setVisibility(View.GONE);
//                Tv_Drop4_Approx_Km.setVisibility(View.VISIBLE);
                Iv_Drop_Off4.setVisibility(View.GONE);
//                Iv_Drop_Off1.setImageResource(R.drawable.multi_arrow_line);
//                Iv_Drop_Off2.setImageResource(R.drawable.multi_arrow_line);
//                Iv_Drop_Off3.setImageResource(R.drawable.multi_arrow_line);
//                Iv_Drop_Off4.setImageResource(R.drawable.a);
                pickupLable1.setTextColor(getResources().getColor(R.color.returnpickup));
                pickupLable1.setText(getkey("pikcup"));
                Drop1_Lable.setText(getkey("stop_1"));
                Drop2_Lable.setText(getkey("stop_3"));
                Drop3_Lable.setText(getkey("stop_3"));
                Drop4_Lable.setText(getkey("final_drop"));

                Tv_Drop1_Address.setText(multiDropList.get(0).getPlaceName().toString());
                Tv_Drop2_Address.setText(multiDropList.get(1).getPlaceName().toString());
                Tv_Drop3_Address.setText(multiDropList.get(2).getPlaceName().toString());
                Tv_Drop4_Address.setText(SdestinationLocation.toString());


                multidrop1 = multiDropList.get(0).getPlaceName().toString();
                multidrop1Lat = multiDropList.get(0).getPlaceLat().toString();
                multidrop1Lon = multiDropList.get(0).getPlaceLong().toString();

                multidrop2 = multiDropList.get(1).getPlaceName().toString();
                multidrop2Lat = multiDropList.get(1).getPlaceLat().toString();
                multidrop2Lon = multiDropList.get(1).getPlaceLong().toString();

                multidrop3 = multiDropList.get(2).getPlaceName().toString();
                multidrop3Lat = multiDropList.get(2).getPlaceLat().toString();
                multidrop3Lon = multiDropList.get(2).getPlaceLong().toString();

                multidrop4 = SdestinationLocation.toString();
                multidrop4Lat = SdestinationLatitude;
                multidrop4Lon = SdestinationLongitude;
                viewall.setVisibility(View.VISIBLE);

//                Tv_Drop1_Approx_Km.setText(SapproxDist1.toString());
//                Tv_Drop2_Approx_Km.setText(SapproxDist2.toString());
//                Tv_Drop3_Approx_Km.setText(SapproxDist3.toString());
//                Tv_Drop4_Approx_Km.setText(SapproxDist4.toString());

//                Tv_Drop1_Approx_Km.setTextColor(getResources().getColor(R.color.yellow_color));
//                Tv_Drop2_Approx_Km.setTextColor(getResources().getColor(R.color.yellow_color));
//                Tv_Drop3_Approx_Km.setTextColor(getResources().getColor(R.color.yellow_color));
//                Tv_Drop4_Approx_Km.setTextColor(getResources().getColor(R.color.yellow_color));
            } else if (multiDropList.size() == 2) {

                Rl_Drop2.setVisibility(View.VISIBLE);
//                Tv_Drop2_Approx_Km.setVisibility(View.VISIBLE);
                Iv_Drop_Off2.setVisibility(View.VISIBLE);

                Rl_Drop3.setVisibility(View.GONE);
//                Tv_Drop3_Approx_Km.setVisibility(View.VISIBLE);
                Iv_Drop_Off3.setVisibility(View.GONE);

                Rl_Drop4.setVisibility(View.GONE);
//                Tv_Drop4_Approx_Km.setVisibility(View.GONE);
                Iv_Drop_Off4.setVisibility(View.GONE);

//                Iv_Drop_Off1.setImageResource(R.drawable.multi_arrow_line);
//                Iv_Drop_Off2.setImageResource(R.drawable.multi_arrow_line);
//                Iv_Drop_Off3.setImageResource(R.drawable.a);

                pickupLable1.setTextColor(getResources().getColor(R.color.returnpickup));
                pickupLable1.setText(getkey("pikcup"));
                Drop1_Lable.setText(getkey("stop_1"));
                Drop2_Lable.setText(getkey("stop_2"));
                Drop3_Lable.setText(getkey("final_drop"));

                Tv_Drop1_Address.setText(multiDropList.get(0).getPlaceName().toString());
                Tv_Drop2_Address.setText(multiDropList.get(1).getPlaceName().toString());
                Tv_Drop3_Address.setText(SdestinationLocation.toString());

                multidrop1 = multiDropList.get(0).getPlaceName().toString();
                multidrop1Lat = multiDropList.get(0).getPlaceLat().toString();
                multidrop1Lon = multiDropList.get(0).getPlaceLong().toString();

                multidrop2 = multiDropList.get(1).getPlaceName().toString();
                multidrop2Lat = multiDropList.get(1).getPlaceLat().toString();
                multidrop2Lon = multiDropList.get(1).getPlaceLong().toString();

                multidrop3 = SdestinationLocation.toString();
                multidrop3Lat = SdestinationLatitude;
                multidrop3Lon = SdestinationLongitude;
                viewall.setVisibility(View.VISIBLE);
//
//                Tv_Drop1_Approx_Km.setText(SapproxDist1.toString());
//                Tv_Drop2_Approx_Km.setText(SapproxDist2.toString());
//                Tv_Drop3_Approx_Km.setText(SapproxDist3.toString());
//
//                Tv_Drop1_Approx_Km.setTextColor(getResources().getColor(R.color.yellow_color));
//                Tv_Drop2_Approx_Km.setTextColor(getResources().getColor(R.color.yellow_color));
//                Tv_Drop3_Approx_Km.setTextColor(getResources().getColor(R.color.yellow_color));

            } else if (multiDropList.size() == 1) {

                Rl_Drop2.setVisibility(View.VISIBLE);
//                Tv_Drop2_Approx_Km.setVisibility(View.VISIBLE);
                Iv_Drop_Off2.setVisibility(View.VISIBLE);

//                Iv_Drop_Off1.setImageResource(R.drawable.multi_arrow_line);
//                Iv_Drop_Off2.setImageResource(R.drawable.a);

                Rl_Drop3.setVisibility(View.GONE);
//                Tv_Drop3_Approx_Km.setVisibility(View.GONE);
                Iv_Drop_Off3.setVisibility(View.GONE);

                Rl_Drop4.setVisibility(View.GONE);
//                Tv_Drop4_Approx_Km.setVisibility(View.GONE);
                Iv_Drop_Off4.setVisibility(View.GONE);
//
                Drop1_Lable.setText("stop 1");
                Drop2_Lable.setText("drop off");

                multidrop1 = multiDropList.get(0).getPlaceName().toString();
                multidrop1Lat = multiDropList.get(0).getPlaceLat().toString();
                multidrop1Lon = multiDropList.get(0).getPlaceLong().toString();

                Tv_Drop1_Address.setText(multiDropList.get(0).getPlaceName().toString());
                Tv_Drop2_Address.setText(SdestinationLocation.toString());
                viewall.setVisibility(View.GONE);
//                Tv_Drop1_Approx_Km.setText(SapproxDist1.toString());
//                Tv_Drop2_Approx_Km.setText(SapproxDist2.toString());

//                Tv_Drop1_Approx_Km.setTextColor(getResources().getColor(R.color.yellow_color));
//                Tv_Drop2_Approx_Km.setTextColor(getResources().getColor(R.color.yellow_color));
            }
        }

    }

    private void getDataDetails() {
        try {

            HashMap<String, String> info = sessionManager.getUserDetails();
            UserID = info.get(SessionManager.KEY_USERID);

            Intent intent = getIntent();
            SpickupLocation = intent.getStringExtra("pickupAddress");
            SpickupLatitude = intent.getStringExtra("pickupLatitude");
            SpickupLongitude = intent.getStringExtra("pickupLongitude");

            multidrop2 = intent.getStringExtra("pickupAddress");
            multidrop2Lat = intent.getStringExtra("pickupLatitude");
            multidrop2Lon = intent.getStringExtra("pickupLongitude");

            SdestinationLocation = intent.getStringExtra("destinationAddress");
            SdestinationLatitude = intent.getStringExtra("destinationLatitude");
            SdestinationLongitude = intent.getStringExtra("destinationLongitude");
            SselectedDate = intent.getStringExtra("pickup_date");
            SselectedTime = intent.getStringExtra("pickup_time");
            ScategoryID = intent.getStringExtra("CategoryID");
            SselectedType = intent.getStringExtra("type");
            Stracking = intent.getStringExtra("tracking");
            StripMode = intent.getStringExtra("mode");
            WaitingTime = intent.getStringExtra("waitingTime");

            if (intent.hasExtra("book_ref")) {
                book_ref = intent.getStringExtra("book_ref");
                user_name = intent.getStringExtra("user_name");
                gender = intent.getStringExtra("gender");
                mobile_number = intent.getStringExtra("mobile_number");
                country_code = intent.getStringExtra("country_code");
            }

            System.out.println("=============SpickupLocation==============" + SpickupLocation);
            System.out.println("=============SdestinationLocation==============" + SdestinationLocation);
            if (intent.hasExtra("waiting_time")) {
                SwaitingTime = intent.getStringExtra("waiting_time");
            }
            if (intent.hasExtra("MultipleDropLocation_List")) {
                try {
                    Bundle bundleObject = getIntent().getExtras();
                    multiDropList = (ArrayList<MultiDropPojo>) bundleObject.getSerializable("MultipleDropLocation_List");
                    for (int i = 0; i <= multiDropList.size() - 1; i++) {
                        if (i == 0) {
                            multidrop1 = multiDropList.get(i).getPlaceName();
                            multidrop1Lat = multiDropList.get(i).getPlaceLat();
                            multidrop1Lon = multiDropList.get(i).getPlaceLong();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isInternetPresent) {
            postDataDetails();

        } else {
            Alert(getkey("action_error"), getkey("alert_nointernet"));
        }


    }


    private void postDataDetails() {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("pickup", SpickupLocation);
        jsonParams.put("pickup_lat", SpickupLatitude);
        jsonParams.put("pickup_lon", SpickupLongitude);
        jsonParams.put("drop", SdestinationLocation);
        jsonParams.put("drop_lat", SdestinationLatitude);
        jsonParams.put("drop_lon", SdestinationLongitude);
        jsonParams.put("mode", StripMode);
        jsonParams.put("tracking", Stracking);
        jsonParams.put("category", ScategoryID);
        jsonParams.put("type", SselectedType);
        jsonParams.put("pickup_date", SselectedDate);
        jsonParams.put("pickup_time", SselectedTime);

        if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(StripMode)) {
            for (int i = 0; i < multiDropList.size(); i++) {
                jsonParams.put("location_arr[" + i + "][drop_loc]", multiDropList.get(i).getPlaceName());
                jsonParams.put("location_arr[" + i + "][drop_lon]", multiDropList.get(i).getPlaceLong());
                jsonParams.put("location_arr[" + i + "][drop_lat]", multiDropList.get(i).getPlaceLat());
                jsonParams.put("location_arr[" + i + "][wait_time]", multiDropList.get(i).getwaitingTime());
            }
        } else {
            if (!WaitingTime.equalsIgnoreCase("")) {
                if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(StripMode) || getResources().getString(R.string.return_lable).equalsIgnoreCase(StripMode)) {
                    jsonParams.put("wait_time", WaitingTime);
                }
            } else {
                if (getResources().getString(R.string.multiplestop_lable).equalsIgnoreCase(StripMode) || getResources().getString(R.string.return_lable).equalsIgnoreCase(StripMode)) {
                    jsonParams.put("wait_time", SwaitingTime);
                }
            }
        }

        PostRequest_getDetails(Iconstant.estimate_price_url, jsonParams);


    }


    private void PostRequest_getDetails(String Url, HashMap<String, String> jsonParams) {

        dialog = new Dialog(BookingPage2.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView loading = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        loading.setText(getkey("action_pleasewait"));
        System.out.println("--------------estimate_price_url----------------" + Url);
        System.out.println("--------------estimate_price_url- jsonParams---------------" + jsonParams);

        mRequest = new ServiceRequest(BookingPage2.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String status = "";
                geteta = new Geteta();
                try {
                    System.out.println("--------------estimate_price_url- jsonObject---------------" + response);
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");
                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");

                            Type listType = new TypeToken<Geteta>() {
                            }.getType();
                            geteta = new GsonBuilder().create().fromJson(object.toString(), listType);

//                            ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(jsonObject.getString("currency"));
                            ScurrencyCode = jsonObject.getString("currency");
                            JSONObject eta_jsonObject = jsonObject.getJSONObject("eta");
                            if (eta_jsonObject.length() > 0) {
                                StotalAmount = eta_jsonObject.getString("amount");
                                SetaTime = eta_jsonObject.getString("eta_time");
                                SuserName = eta_jsonObject.getString("user_name");
                                fareAmount1 = eta_jsonObject.getString("ride_amount");
                                StotalDistance = eta_jsonObject.getString("distance");
                                StotalTime = eta_jsonObject.getString("att");

                                SapproxDist1 = eta_jsonObject.getString("stop_one_eta");
                                SapproxDist2 = eta_jsonObject.getString("stop_two_eta");
                                SapproxDist3 = eta_jsonObject.getString("stop_three_eta");
                                SapproxDist4 = eta_jsonObject.getString("stop_four_eta");

                                SwaitingCharge = eta_jsonObject.getString("total_waiting_charge");
                                SCategoryImage = eta_jsonObject.getString("icon_normal");
                                SphantomCar = eta_jsonObject.getString("phantom_car");

                            }
                        }
                    }

                    if (status.equalsIgnoreCase("1")) {
                        setDataDetails();
                        dialogDismiss();

                    }

                    dialogDismiss();

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialogDismiss();

            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    public void SaveFavoriteLocationDialog(final String location, final String lat, final String lon) {


        savedialog = new Dialog(BookingPage2.this, R.style.DialogSlideAnim3);
        savedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        savedialog.setContentView(R.layout.save_favourite_dialog_constrain);

//        final CustomTextView Tv_Dig_saveLoaction_address = (CustomTextView) savedialog.findViewById(R.id.save_fav_location_address);
//        final RelativeLayout Btn_Dig_save = (RelativeLayout) savedialog.findViewById(R.id.save_fav_bottom_layout);
//        final ImageView image_close = (ImageView) savedialog.findViewById(R.id.image_close);

        final TextView Tv_Dig_saveLoaction_address = savedialog.findViewById(R.id.save_fav_location_address);
        final TextView Btn_Dig_save = savedialog.findViewById(R.id.save_fav_bottom_layout);
        final ImageView iv_close = (ImageView) savedialog.findViewById(R.id.image_close);

        Et_Did_location_name = (EditText) savedialog.findViewById(R.id.fav_loc_name);
        final TextView myreferalcode =(TextView) savedialog.findViewById(R.id.myreferalcode);
        myreferalcode.setText(getkey("favourites"));
        final TextView fav_location_name_lable =(TextView) savedialog.findViewById(R.id.fav_location_name_lable);
        fav_location_name_lable.setText(getkey("enter_place_name"));
        Et_Did_location_name.setHint(getkey("eg_cafe"));
        Btn_Dig_save.setText(getkey("add_favourites"));

        Tv_Dig_saveLoaction_address.setText(location);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savedialog.dismiss();
            }
        });
        Btn_Dig_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Btn_Dig_save.getWindowToken(), 0);

                cd = new ConnectionDetector(BookingPage2.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {


                    if (Et_Did_location_name.getText().toString().length() == 0) {


                        erroredit(Et_Did_location_name, getkey("profile_label_alert_favelocartionname"));


                    } else {

                        postRequest_FavoriteSave(Iconstant.favoritelist_add_url, location, lat, lon);

                    }


                } else {


                    Alert(getkey("alert_label_title"), getkey("alert_nointernet"));

                }


            }
        });


        // make dialog itself transparent
        savedialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // remove background dim
        savedialog.getWindow().setDimAmount(0);

        savedialog.show();


    }


    //-----------------------Favourite Save Post Request-----------------
    private void postRequest_FavoriteSave(String Url, final String favLocation, final String favLat, final String favLon) {
        dialog = new Dialog(BookingPage2.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getkey("action_saving"));


        System.out.println("-------------Favourite Save Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", UserID);
        jsonParams.put("title", Et_Did_location_name.getText().toString());
        jsonParams.put("latitude", favLat);
        jsonParams.put("longitude", favLon);
        jsonParams.put("address", favLocation);

        System.out.println("-------------Favourite Save jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(BookingPage2.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Favourite Save Response----------------" + response);
                String Sstatus = "", Smessage = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Intent local = new Intent();
                        local.setAction("com.favoriteList.refresh");
                        sendBroadcast(local);

                        final PkDialog mDialog = new PkDialog(BookingPage2.this);
                        mDialog.setDialogTitle(getkey("action_success"));
                        mDialog.setDialogMessage(Smessage);
                        mDialog.setPositiveButton(getkey("action_ok"), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                savedialog.dismiss();
                                mDialog.dismiss();
                            }
                        });
                        mDialog.show();

                    } else {
                        Alert(getkey("alert_label_title"), Smessage);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(BookingPage2.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(BookingPage2.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getkey("alert_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent book1Intent = new Intent(BookingPage2.this, Navigation_new.class);
        //book1Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(book1Intent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    private String getkey(String key)
    {
        return mhelper.getvalueforkey(key);
    }

}
